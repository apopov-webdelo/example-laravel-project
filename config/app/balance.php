<?php
/*
 * Configuration for deals
 */

return [
    'default_balance_values' => [
        'amount'      => env('DEFAULT_BALANCE_AMOUNT', 0),
        'turnover'    => 0,
        'escrow'      => 0,
        'commission'  => env('DEFAULT_BALANCE_COMMISSION', 80), // 1%
        'deals_count' => 0,
    ],

    'commissions' => [
        'pay_out' => [

            'btc' => [
                'commission' => env('PAYOUT_COMMISSION_BTC', 0.0001), //float crypto amount! Not in satoshi!
                'min_amount' => env('PAYOUT_MIN_AMOUNT_BTC', 0.0002), //float crypto amount! Not in satoshi!
            ],

            'eth' => [
                'commission' => env('PAYOUT_COMMISSION_ETH', 0.001), //float crypto amount! Not in satoshi!
                'min_amount' => env('PAYOUT_MIN_AMOUNT_ETH', 0.002), //float crypto amount! Not in satoshi!
            ],
        ],

        'pay_in' => [

            'btc' => [
                'commission' => env('PAYIN_COMMISSION_BTC', 0.0000), //float crypto amount! Not in satoshi!
                'min_amount' => env('PAYIN_MIN_AMOUNT_BTC', 0.0002), //float crypto amount! Not in satoshi!
            ],

            'eth' => [
                'commission' => env('PAYIN_COMMISSION_ETH', 0.000), //float crypto amount! Not in satoshi!
                'min_amount' => env('PAYIN_MIN_AMOUNT_ETH', 0.002), //float crypto amount! Not in satoshi!
            ],
        ],
    ],

];
