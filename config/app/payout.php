<?php
/*
 * Configuration for PayOut module
 */

return [
    'processing_deadline_time' => env('PAYOUT_PROCESSING_DEADLINE_TIME', 5), // Value in minutes
];
