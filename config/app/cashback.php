<?php
/*
 * Configuration for deals
 */

return [
    'deals' => [
        'quantity'   => env('DEAL_CASHBACK_DEAL_QUANTITY', 5),
        'commission' => env('DEAL_CASHBACK_COMMISSION', 5),
    ],
];
