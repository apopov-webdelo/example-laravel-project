<?php
/*
 * Configuration for blockchain gates
 */

return [
    // NodeJS gate to stellar blockchain system
    'stellar' => [
        'host'     => env('BLOCKCHAIN_STELLAR_HOST', '127.0.0.1'),
        'port'     => env('BLOCKCHAIN_STELLAR_PORT', '80'),
        'protocol' => env('BLOCKCHAIN_STELLAR_PROTOCOL', 'http'), // 'http' or 'https'

        'private_key' => env('BLOCKCHAIN_STELLAR_KEY', 'SecretPrivateKeyForBlockchainGate'),
    ]
];
