<?php
/*
 * Configuration for deals
 */

return [
    'default_rate' => env('USER_DEFAULT_RATE', 100)
];
