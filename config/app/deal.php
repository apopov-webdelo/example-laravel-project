<?php
/*
 * Configuration for deals
 */

return [

    'min_crypto_amount' => env('DEAL_MIN_CRYPTO_AMOUNT', 0.00001), // minimal crypto amount for deal

    'dispute_deadline' => env('DEAL_DISPUTE_DEADLINE', 30), // value in minutes!

    'dispute_admin_email'        => env('DEAL_DISPUTE_ADMIN_EMAIL', 'croptown@risex.net'), // value in minutes!

    // min canceled deals user has to calculate deals_cancellation_percent
    'cancel_min_limit'           => env('DEAL_CANCEL_MIN_LIMIT', 3),

    // min deals user has to calculate deals_cancellation_percent
    'cancel_total_limit'         => env('DEAL_CANCEL_TOTAL_LIMIT', 10),

    // status timeouts for alerts to go (in minutes)
    'verifying_deadline_time'    => env('DEAL_VERIFYING_DEADLINE', 3),
    'cancellation_deadline_time' => env('DEAL_CANCELLATION_DEADLINE', 3),
    'finishing_deadline_time'    => env('DEAL_FINISHING_DEADLINE', 3),
];
