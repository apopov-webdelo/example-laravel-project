<?php

return [
    // Max possible price for Ad (potentially can save price * 1000 in coins)
    'max_price_int' => env('MAX_PRICE_INT', 999999999999),

    // min Ad time limits (minutes)
    'time_min' => env('AD_TIME_MIN', 15),

    // max Ad time limit (minutes)
    'time_max' => env('AD_TIME_MAX', 90),

    // all Ad time limit periods (minutes)
    'time_periods' => env('AD_TIME_PERIODS', '15,30,60,90'),
];
