<?php

return [
    'default_driver' => env('PARTNERSHIP_DEFAULT_PROGRAM_DRIVER', 'first_line'),

    'first_line' => [
        'program_driver' => \App\Services\Partnership\Commission\Strategies\Partnership1stLineCommissionStrategy::class,

        'percentage_stage_1'  => env('PARTNERSHIP_FIRST_LINE_STAGE_1_PERCENT', 25),
        'percentage_stage_2'  => env('PARTNERSHIP_FIRST_LINE_STAGE_2_PERCENT', 12),
        'percentage_stage_3'  => env('PARTNERSHIP_FIRST_LINE_STAGE_3_PERCENT', 6),
    ],

    'second_line' => [
        'program_driver' => \App\Services\Partnership\Commission\Strategies\Partnership2ndLineCommissionStrategy::class,

        'percentage_stage_1'  => env('PARTNERSHIP_SECOND_LINE_STAGE_1_PERCENT', 10),
        'percentage_stage_2'  => env('PARTNERSHIP_SECOND_LINE_STAGE_1_PERCENT', 5),
        'percentage_stage_3'  => env('PARTNERSHIP_SECOND_LINE_STAGE_1_PERCENT', 2),
    ],
];
