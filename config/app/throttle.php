<?php
/*
 * Configuration for throttling
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Global api rate limiting
    |--------------------------------------------------------------------------
    */
    'default_api'          => 'throttle:' . env('THROTTLE_DEFAULT_API', '60,1'),

    /*
    |--------------------------------------------------------------------------
    | security route api rate limiting
    |--------------------------------------------------------------------------
    */
    'profile_security_api' => 'throttle:' . env('THROTTLE_PROFILE_SECURITY_API', '10,2'),

    /*
    |--------------------------------------------------------------------------
    | mailing routes api rate limiting
    |--------------------------------------------------------------------------
    */
    'mailing_api' => 'throttle:' . env('THROTTLE_PROFILE_MAILING_API', '3,1'),
];
