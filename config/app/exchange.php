<?php
/*
 * Configuration for deals
 */

return [
    'crypto' => [
        'default_market' => env('EXCHANGE_CRYPTO_DEFAULT_DRIVER', 'average'),

        /* Markets Engines */
        'markets'        => [

            'average' => [
                'driver'         => 'AverageCryptoExchange',
                'cache_lifetime' => env('EXCHANGE_CRYPTO_AVERAGE_DRIVER_CACHE_LIFETIME', 14400), // 10 days
            ],

            'bitfinex' => [
                'driver'         => 'Bitfinex',
                'cache_lifetime' => env('EXCHANGE_CRYPTO_BITFINEX_DRIVER_CACHE_LIFETIME', 14400), // 10 days
            ],

            'cryptonator' => [
                'driver'         => 'Cryptonator',
                'cache_lifetime' => env('EXCHANGE_CRYPTO_CRYPTONATOR_DRIVER_CACHE_LIFETIME', 14400), // 10 days
            ],
        ],
    ],

    'fiat' => [
        'default_market' => env('EXCHANGE_FIAT_DEFAULT_DRIVER', 'average'),

        /* Markets Engines */
        'markets'        => [

            'average'           => [
                'driver'         => \App\Services\Exchange\Currency\AverageFiatExchange::class,
                'cache_lifetime' => env('EXCHANGE_FIAT_AVERAGE_DRIVER_CACHE_LIFETIME', 1440), // 1 days
                'excluded_pairs' => [],
            ],

            // exchangeratesapi.io
            'ratesapi'          => [
                'driver'         => \App\Services\Exchange\Currency\RatesApiExchange::class,
                'cache_lifetime' => env('EXCHANGE_FIAT_EXCHANGERATESAPI_DRIVER_CACHE_LIFETIME', 1440), // 1 days
                'excluded_pairs' => ['USD/BYN', 'USD/UAH'],
            ],

            // https://openexchangerates.org [Free plan - 1000 req/month]
            'openexchangerates' => [
                'driver'         => \App\Services\Exchange\Currency\OpenExchangeRatesApi::class,
                'cache_lifetime' => env('EXCHANGE_FIAT_OPENEXCHANGERATES_DRIVER_CACHE_LIFETIME', 1440), // 1 days
                'excluded_pairs' => [],
                'api_key'        => env('EXCHANGE_FIAT_OPENEXCHANGERATES_DRIVER_API_KEY'),
                'base'           => \App\Models\Directory\FiatConstants::USD, // free plan API is limited to this base
                'batch_updates'  => true, // utilizes batch cron updates to reduce requests number
            ],

            // @deprecated | no longer works with our IPs
            // free.currencyconverterapi.com
            // 'freecc'   => [
            //     'driver'         => \App\Services\Exchange\Currency\FreeConverterApiExchange::class,
            //     'cache_lifetime' => env('EXCHANGE_FIAT_FREECC_DRIVER_CACHE_LIFETIME', 14400), // 10 days
            //     'excluded_pairs' => [],
            // ],
        ],
    ],
];
