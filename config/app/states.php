<?php

return [
    'front' => [
        'main'    => env('FRONT_MAIN_STATE', 'https://risex.net'),
        'chat'    => env('FRONT_CHAT_STATE', 'https://risex.net/messages/user/:user_id'),
        'deal'    => env('FRONT_DEAL_STATE', 'https://risex.net/deal/:deal_id'),
        'reviews' => env('FRONT_REVIEWS_STATE', 'https://risex.net/profile/:user_id/reviews'),
        'profile' => env('FRONT_PROFILE_STATE', 'https://risex.net/profile/:user_id'),
        'account' => env('FRONT_ACCOUNT_STATE', 'https://risex.net/account'),
    ],
    'admin' => [
        'main' => env('ADMIN_MAIN_STATE', 'https://admin.risex.net'),
        'deal' => env('ADMIN_DEAL_STATE', 'https://admin.risex.net/deals/:deal_id')
    ]
];
