<?php

return [
    // default board for sending jira events
    'board' => env('JIRA_LOG_BOARD', 'OPS'),
];
