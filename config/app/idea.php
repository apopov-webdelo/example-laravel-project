<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/26/18
 * Time: 14:26
 */

return [
    'email' => env('IDEA_RECEIVER_EMAIL', 'd.cercel@webdelo.org')
];
