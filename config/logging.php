<?php

use Monolog\Handler\StreamHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver'   => 'stack',
            'channels' => ['single'],
        ],

        'single' => [
            'driver'     => 'single',
            'path'       => storage_path('logs/laravel.log'),
            'level'      => 'debug',
            'permission' => 0664,
        ],

        'test' => [
            'driver'     => 'single',
            'path'       => storage_path('logs/test.log'),
            'level'      => 'debug',
            'permission' => 0664,
        ],

        'partnership' => [
            'driver'     => 'daily',
            'path'       => storage_path('logs/partnership.log'),
            'level'      => 'debug',
            'permission' => 0664,
        ],

        'cashback' => [
            'driver'     => 'daily',
            'path'       => storage_path('logs/cashback.log'),
            'level'      => 'info',
            'permission' => 0664,
        ],

        'stellar' => [
            'driver'     => 'single',
            'path'       => storage_path('logs/stellar.log'),
            'level'      => 'debug',
            'permission' => 0664,
        ],

        'daily' => [
            'driver'     => 'daily',
            'path'       => storage_path('logs/laravel.log'),
            'level'      => 'debug',
            'days'       => 7,
            'permission' => 0664,
        ],

        'slack' => [
            'driver'   => 'slack',
            'url'      => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji'    => ':boom:',
            'level'    => 'critical',
        ],

        'telegram_emergency' => [
            'driver'      => 'custom',
            'via'         => \App\Utils\Loggers\TelegramLogger::class,
            'name'        => 'Emergency',
            'token'       => env('TELEGRAM_EMERGENCY_TOKEN', ''),
            'channel'     => env('TELEGRAM_EMERGENCY_CHANNEL', ''),
            'date_format' => 'd-m-Y H:i:s',
            'timeout'     => 5,
        ],

        'telegram_deals' => [
            'driver'      => 'custom',
            'via'         => \App\Utils\Loggers\TelegramLogger::class,
            'name'        => 'Deals',
            'token'       => env('TELEGRAM_DEAL_BOT_TOKEN', ''),
            'channel'     => env('TELEGRAM_DEAL_BOT_CHANNEL', ''),
            'date_format' => 'd-m-Y H:i:s',
            'timeout'     => 5,
        ],

        'jira' => [
            'driver' => 'custom',
            'via'    => \App\Utils\Loggers\JiraLogger::class,
            'level'  => 'critical',
        ],

        'stderr' => [
            'driver'  => 'monolog',
            'handler' => StreamHandler::class,
            'with'    => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level'  => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level'  => 'debug',
        ],

        'gelf' => [
            'driver' => 'custom',
            'via'    => \App\Utils\Loggers\GelfLogger::class,
            'host'   => '127.0.0.1',
            'port'   => 12201,
        ],
    ],

];
