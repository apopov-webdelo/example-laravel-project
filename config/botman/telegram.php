<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Telegram Token
    |--------------------------------------------------------------------------
    |
    | Your Telegram bot token you received after creating
    | the chatbot through Telegram.
    |
    */
    'token' => env('TELEGRAM_TOKEN'),

    'commands' => [
        '/start' => 'First meeting and available commands',
        '/help'  => 'Available commands',
        '/myid'  => 'You will receive your personal Telegram ID'
    ]
];
