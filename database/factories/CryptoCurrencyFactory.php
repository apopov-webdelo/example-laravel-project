<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Directory\CryptoCurrency::class, function (Faker $faker) {
    
    return [
        'title' => $faker->randomElement(['Ethereum', 'XRP', 'Litecoin', 'Stellar', 'Monero']),
        'code' => $faker->randomElement(['CC1', 'CC2', 'CC3', 'CC4', 'CC5']),
        'accuracy' => 8,
        'min_balance' => $faker->numberBetween(100, 999),
        'created_at' => now(),
        'updated_at' => null,
    ];
});
