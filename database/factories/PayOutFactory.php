<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Balance\PayOut::class, function (Faker $faker) {

    $min = \App\Models\Directory\CryptoCurrencyConstants::ONE_BTC / 10;
    $max = \App\Models\Directory\CryptoCurrencyConstants::ONE_BTC * 10;
    $amount = $faker->numberBetween($min, $max);
    $commission = $amount * 0.01;

    $user = factory(\App\Models\User\User::class)->create();

    return [
        'status'         => $faker->randomElement(['pending', 'processed', 'canceled']),
        'user_id'        => $user,
        'crypto_code'    => factory(\App\Models\Directory\CryptoCurrency::class)->create()->getCode(),
        'amount'         => $amount,
        'commission'     => $commission,
        'wallet_id'      => $faker->word,
        'transaction_id' => factory(\App\Models\Balance\Transaction::class)->create([
            'amount'    => $amount,
            'author_id' => $user,
        ]),
    ];
});
