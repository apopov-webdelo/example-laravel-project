<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User\Note::class, function (Faker $faker) {
    /** @var \App\Models\User\User $author */
    $author = \App\Makers\User\UserMaker::init()->create();
    /** @var \App\Models\User\User $recipient */
    $recipient = \App\Makers\User\UserMaker::init()->create();

    return [
        'author_id'     => $author->id,
        'recipient_id'  => $recipient->id,
        'note'          => $faker->text('200'),
    ];
});
