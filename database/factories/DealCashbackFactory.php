<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\DealCashback\DealCashback::class, function (Faker $faker, $options = []) {

    if (array_key_exists('user_id', $options)) {
        $user_id = $options['user_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $user = \App\Makers\User\UserMaker::init()->create();
        $user_id = $user->id;
    }

    $allCryptoCurrency = \App\Models\Directory\CryptoCurrency::all('id');

    return [
        'user_id'            => $user_id,
        'status'             => $faker->randomElement(['in_progress', 'break', 'pending', 'done', 'canceled']),
        'commission'         => config('app.cashback.deals.commission'),
        'crypto_currency_id' => $faker->randomElement($allCryptoCurrency),
    ];
});
