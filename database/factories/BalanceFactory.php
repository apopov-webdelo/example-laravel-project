<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Balance\Balance::class, function (Faker $faker) {

    /** @var \App\Models\User\User $user */
    $user = factory(\App\Models\User\User::class)->create();

    /** @var \App\Models\Directory\CryptoCurrency $cryptoCurrency */
    $cryptoCurrency = \App\Models\Directory\CryptoCurrency::all()->random(1)->first();

    $tenBtc = \App\Models\Directory\CryptoCurrencyConstants::TEN_BTC;
    $twentyBtc = $tenBtc*2;

    return [
        'user_id' => $user->id,
        'crypto_currency_id' => $cryptoCurrency->id,
        'amount' => $faker->numberBetween($tenBtc, $twentyBtc),
        'turnover' => $faker->numberBetween($tenBtc, $twentyBtc),
        'escrow' => $faker->numberBetween($tenBtc/10, $tenBtc/5),
        'commission' => $faker->numberBetween(0, 14),
        'deals_count' => $faker->numberBetween(1, 100000)
    ];
});
