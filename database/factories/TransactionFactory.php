<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Balance\Transaction::class, function (Faker $faker) {

    $min = \App\Models\Directory\CryptoCurrencyConstants::ONE_BTC / 10;
    $max = \App\Models\Directory\CryptoCurrencyConstants::ONE_BTC * 10;
    $amount = $faker->numberBetween($min, $max);

    return [
        'author_id'   => factory(\App\Models\User\User::class)->create(),
        'amount'      => $amount,
        'reason_id'   => \App\Models\Balance\TransactionReason::get(['id'])
                                                              ->random(1)
                                                              ->first()->id,
        'description' => $faker->text(100),
    ];
});
