<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User\Reputation::class, function (Faker $faker) {
    /** @var \App\Models\User\User $user */
    $user = factory(\App\Models\User\User::class)->create();

    return [
        'user_id'        => $user->id,
        'positive_count' => $faker->numberBetween(1, 20),
        'negative_count' => $faker->numberBetween(1, 30),
        'rate'           => $faker->numberBetween(0, 100), //TODO: need calculate rate according pos/neg counts!
    ];
});
