<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Deal\Deal::class, function (Faker $faker, $options = []) {

    if (array_key_exists('ad_id', $options)) {
        $ad_id = $options['ad_id'];
        $ad_conditions = $faker->sentence(20);
    } else {
        /** @var \App\Models\Ad\Ad $ad */
        $ad = \App\Makers\AdMaker::init()->create();
        $ad_id = $ad->id;
        $ad_conditions = $ad->conditions;
    }

    if (array_key_exists('author_id', $options)) {
        $author_id = $options['author_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $user = \App\Makers\User\UserMaker::init()->create();
        $author_id = $user->id;
    }

    $ad = \App\Models\Ad\Ad::find($ad_id)->first();
    $currency = $ad->currency;
    $currencyCrypto = $ad->cryptoCurrency;

    if (array_key_exists('fiat_amount', $options)) {
        $crypto_amount = currencyToCoins($options['fiat_amount'] / $ad->price, $currencyCrypto);
    } else {
        $crypto_amount = currencyToCoins($faker->numberBetween(1, 20), $currencyCrypto);
    }
    $commission = $crypto_amount * (1/100);

    return [
        'ad_id'         => $ad_id,
        'author_id'     => $author_id,
        'price'         => $ad->price,
        'fiat_amount'   => currencyToCoins($faker->numberBetween(100, 1000), $currency),
        'crypto_amount' => $crypto_amount,
        'commission'    => $commission,
        'time'          => $faker->numberBetween(900, 1200),
        'conditions'    => $ad_conditions,
    ];
});
