<?php

use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\PaymentSystem;
use Faker\Generator as Faker;

$factory->define(\App\Models\Ad\Ad::class, function (Faker $faker, $options = []) {

    if (array_key_exists('author_id', $options)) {
        $author_id = $options['author_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $user = \App\Makers\User\UserMaker::init()->create();
        $author_id = $user->id;
    }

    $allCryptoCurrency = CryptoCurrency::all('id');
    $allCountry = Country::all('id');
    $allPaysystems = PaymentSystem::all('id');

    $currency = \App\Models\Directory\Currency::query()->get()->random(1)->first();

    $max = currencyToCoins($faker->numberBetween(300, 500), $currency);

    return [
        'author_id'                     => $author_id,
        'payment_system_id'             => $faker->randomElement($allPaysystems),
        'notes'                         => $faker->sentence(),
        'country_id'                    => $faker->randomElement($allCountry),
        'is_sale'                       => $faker->boolean,
        'currency_id'                   => $currency,
        'crypto_currency_id'            => $faker->randomElement($allCryptoCurrency),
        'price'                         => currencyToCoins($faker->numberBetween(900, 1200), $currency),
        'min'                           => currencyToCoins($faker->numberBetween(100, 200), $currency),
        'max'                           => $max,
        'original_max'                  => $max,
        'turnover'                      => $faker->numberBetween(0, 2000),
        'min_deal_finished_count'       => $faker->numberBetween(0, 14),
        'deal_cancellation_max_percent' => $faker->numberBetween(0, 100),
        'time'                          => $faker->randomElement(explode(',', config('app.ad.time_periods'))),
        'liquidity_required'            => $faker->boolean,
        'email_confirm_required'        => $faker->boolean,
        'phone_confirm_required'        => $faker->boolean,
        'trust_required'                => $faker->boolean,
        'tor_denied'                    => $faker->boolean,
        'review_rate'                   => $faker->numberBetween(0, 100),
        'is_active'                     => $faker->boolean,
        'conditions'                    => $faker->sentence(10),
    ];
});
