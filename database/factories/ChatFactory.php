<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Chat\Chat::class, function (Faker $faker, $options = []) {

    if (array_key_exists('author_id', $options)) {
        $author_id = $options['author_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $user = \App\Makers\User\UserMaker::init()->create();
        $author_id = $user->id;
    }

    return [
        'title'     => $faker->text('50'),
        'author_id' => $author_id,
    ];
});
