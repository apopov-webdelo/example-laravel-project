<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User\Review::class, function (Faker $faker, $options = []) {


    if (array_key_exists('author_id', $options)) {
        $author_id = $options['author_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $author_id = \App\Makers\User\UserMaker::init()->create();
    }

    if (array_key_exists('recipient_id', $options)) {
        $recipient_id = $options['recipient_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $recipient_id = \App\Makers\User\UserMaker::init()->create();
    }

    if (array_key_exists('deal_id', $options)) {
        $deal_id = $options['deal_id'];
    } else {
        /** @var \App\Models\Deal\Deal $deal */
        $deal_id = \App\Makers\DealMaker::init()->create();
    }

    return [
        'author_id'    => $author_id,
        'recipient_id' => $recipient_id,
        'deal_id'      => $deal_id,
        'rate'         => $faker->numberBetween(0, 4),
        'message'      => $faker->text('200'),
        'trust'        => $faker->randomElement(['positive', 'neutral', 'negative']),
    ];
});
