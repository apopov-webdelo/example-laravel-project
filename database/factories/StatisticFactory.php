<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User\Statistic::class, function (Faker $faker) {

    /** @var \App\Models\User\User $user */
    $user = factory(\App\Models\User\User::class)->create();

    return [
        'user_id'              => $user->id,
        'total_turnover'       => $faker->numberBetween(0, 800),
        'deals_count'          => $faker->numberBetween(0, 1000),
        'deals_finished_count' => $faker->numberBetween(0, 100),
        'deals_canceled_count' => $faker->numberBetween(0, 50),
        'deals_disputed_count' => $faker->numberBetween(0, 10),
        'deals_paid_count'     => $faker->numberBetween(0, 30),
        'deals_cancellation_percent' => $faker->boolean,
    ];
});
