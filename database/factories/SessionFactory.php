<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Session\Session::class, function (Faker $faker, $options = []) {

    if (array_key_exists('sessionable_id', $options)) {
        $user = $options['sessionable_id'];
    } else {
        /** @var \App\Models\User\User $user */
        $user = factory(\App\Models\User\User::class)->create();
    }

    /** @var \App\Models\Directory\Country $country */
    $country = \App\Models\Directory\Country::all()->random(1)->first();

    return [
        'sessionable_id'   => $user->id,
        'sessionable_type' => get_class($user),
        'ip'               => $faker->ipv4,
        'country'          => $country->title,
        'city'             => $faker->city,
        'type'             => $faker->randomElement(['sign-in', 'sign-out']),
        'status'           => $faker->randomElement(['good', 'bad']),
    ];
});
