<?php

use Faker\Generator as Faker;

$factory->define(App\Models\User\User::class, function (Faker $faker) {
    static $password;

    return [
        'login'           => microtime(),
        'email'           => rand(1000, 9999).$faker->unique()->safeEmail,
        'phone'           => $faker->unique()->phoneNumber,
        'password'        => $password ? : $password = bcrypt('secret'),
        // 'api_token' => str_random(60),
        'remember_token'  => str_random(10),
        'user_locked_at'  => null,
        'user_locked_to'  => null,
        'trade_locked_at' => null,
        'trade_locked_to' => null,
        'created_at'      => now(),
        'updated_at'      => null,
        'appeal'          => $faker->text,
        'last_seen'       => $faker->dateTimeThisDecade,
        'role_id'         => \App\Models\Role\RoleConstants::CLIENT,
    ];
});
