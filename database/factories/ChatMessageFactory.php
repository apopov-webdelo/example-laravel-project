<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Chat\Message::class, function (Faker $faker, $options = []) {

    if (array_key_exists('chat_id', $options)) {
        $chat = $options['chat_id'];
        if (is_numeric($chat)) {
            $chat = \App\Models\Chat\Chat::find($chat);
        }
    } else {
        /** @var \App\Models\Chat\Chat $chat */
        $chat = factory(\App\Models\Chat\Chat::class)->create();
    }

    if (array_key_exists('author_id', $options)) {
        $author = $options['author_id'];
        $chat->recipients()->attach($options['author_id']);
    } else {
        $chat->recipients()->attach(factory(\App\Models\User\User::class)->create());
        $chat->recipients()->attach(factory(\App\Models\User\User::class)->create());
        $author = $chat->recipients->random(1)->first();
    }

    return [
        'message'     => $faker->text('200'),
        'chat_id'     => $chat,
        'author_id'   => $author,
        'author_type' => get_class($author),
    ];
});
