<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User\Security::class, function (Faker $faker) {

    /** @var \App\Models\User\User $user */
    $user = factory(\App\Models\User\User::class)->create();

    $statuses = \App\Models\User\Google2faConstants::STATUSES;

    return [
        'user_id'                   => $user->id,
        'email_confirmed'           => $faker->boolean,
        'email_access_token'        => $faker->md5,
        'email_inactive'            => $faker->email,
        'email_confirm_token'       => $faker->md5,
        'phone_confirmed'           => $faker->boolean,
        'phone_access_token'        => $faker->md5,
        'phone_inactive'            => $faker->boolean,
        'phone_confirm_token'       => $faker->md5,
        'google2fa_status'          => $faker->randomElement($statuses),
        'telegram_id_confirmed'     => $faker->boolean,
        'telegram_id_confirm_token' => $faker->md5,
    ];
});
