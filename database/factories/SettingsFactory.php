<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User\Settings::class, function (Faker $faker) {

    /** @var \App\Models\User\User $user */
    $user = factory(\App\Models\User\User::class)->create();

    /** @var \App\Models\Directory\Country $country */
    $country = \App\Models\Directory\Country::all()->random(1)->first();

    /** @var \App\Models\Directory\Language $language */
    $language = \App\Models\Directory\Language::all()->random(1)->first();

    return [
        'user_id'                        => $user->id,
        'email_notification_required'    => $faker->boolean,
        'phone_notification_required'    => $faker->boolean,
        'telegram_notification_required' => $faker->boolean,
        'deal_notification_required'     => $faker->boolean,
        'message_notification_required'  => $faker->boolean,
        'push_notification_required'     => $faker->boolean,
        'country_id'                     => $country->id,
        'language_id'                    => $language->id,
        'deal_cancellation_max_percent'  => $faker->numberBetween(0, 30),
    ];
});
