<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePayOutTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('payout_transactions', function (Blueprint $table) {
                $table->increments('id');
                $table->enum('status', ['pending', 'processed', 'canceled'])->index();
                $table->integer('user_id')->unsigned()->nullable();
                $table->string('crypto_code');
                $table->bigInteger('amount');
                $table->bigInteger('commission');
                $table->string('wallet_id')->comment('Blockchain wallet for withdraw');
                $table->string('transaction_id')
                      ->unique()
                      ->nullable()
                      ->comment('Transaction ID in blockchain');
                $table->timestamps();
            });

            Schema::table('payout_transactions', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('payout_transactions');
        });
    }
}
