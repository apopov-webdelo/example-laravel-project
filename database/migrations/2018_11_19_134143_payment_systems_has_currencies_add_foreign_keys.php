<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentSystemsHasCurrenciesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('payment_systems_has_currencies', function (Blueprint $table) {
                $table->unsignedInteger('payment_system_id')->change();
                $table->unsignedInteger('currency_id')->change();
                $table->unsignedInteger('country_id')->change();
                $table->unsignedInteger('author_id')->change();

                $table->foreign('payment_system_id')->references('id')->on('payment_systems')->onDelete('cascade');
                $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade');
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
                $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('payment_systems_has_currencies', function (Blueprint $table) {
                $table->dropForeign('payment_systems_has_currencies_payment_system_id_foreign');
                $table->dropForeign('payment_systems_has_currencies_currency_id_foreign');
                $table->dropForeign('payment_systems_has_currencies_country_id_foreign');
                $table->dropForeign('payment_systems_has_currencies_author_id_foreign');
            });

            Schema::table('payment_systems_has_currencies', function (Blueprint $table) {
                $table->integer('payment_system_id')->change();
                $table->integer('currency_id')->change();
                $table->integer('country_id')->change();
                $table->integer('author_id')->change();

                $table->dropIndex('payment_systems_has_currencies_payment_system_id_foreign');
                $table->dropIndex('payment_systems_has_currencies_currency_id_foreign');
                $table->dropIndex('payment_systems_has_currencies_country_id_foreign');
                $table->dropIndex('payment_systems_has_currencies_author_id_foreign');
            });
        });
    }
}
