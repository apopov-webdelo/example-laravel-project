<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecurityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('security_logs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('sessionable_id');
                $table->string('sessionable_type', 200);
                $table->string('ip', 20);
                $table->string('country', 50);
                $table->string('city', 50);
                $table->string('os', 50);
                $table->string('browser', 50);
                $table->string('type', 30);
                $table->boolean('status');
                $table->string('status_desc', 30)->nullable();
                $table->timestamp('created_at')->useCurrent();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('security_logs');
        });
    }
}
