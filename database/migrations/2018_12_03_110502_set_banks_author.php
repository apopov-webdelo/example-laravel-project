<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetBanksAuthor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banks', function (Blueprint $table) {
            $table->dropForeign('banks_author_id_foreign');
            $table->dropIndex('banks_author_id_foreign');
        });
        $admin = App\Models\Admin\Admin::where('login', 'root')->first();
        if ($admin->id) {
            DB::table('banks')->update(['author_id' => $admin->id]);
        }
        Schema::table('banks', function (Blueprint $table) {
            $table->foreign('author_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->update(['author_id' => null]);
    }
}
