<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewDealStatusesChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Deal\DealStatus::find(1)->fill(['title' => 'Verified'])->save();
        \App\Models\Deal\DealStatus::find(7)->fill(['title' => 'Verification'])->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Deal\DealStatus::find(7)->fill(['title' => 'Verified'])->save();
        \App\Models\Deal\DealStatus::find(1)->fill(['title' => 'Verification'])->save();
    }
}
