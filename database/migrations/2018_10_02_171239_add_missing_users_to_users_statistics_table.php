<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddMissingUsersToUsersStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {

            $users = DB::table('users')
                ->select('id')
                ->whereRaw('`id` NOT IN (SELECT `users_statistics`.`user_id` FROM `users_statistics`)')
                ->get();

            foreach ($users as $user) {
                DB::table('users_statistics')->insert(['user_id' => $user->id]);
            }
        });
    }

    /**
     * Reverse the migrations is impossible
     *
     * @return void
     */
    public function down()
    {
        // Reverse the migrations is impossible
    }
}
