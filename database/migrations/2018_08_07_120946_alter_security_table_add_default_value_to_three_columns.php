<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSecurityTableAddDefaultValueToThreeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('security', function (Blueprint $table) {
            $table->boolean('email_confirmed')->default(0)->change();
            $table->boolean('phone_confirmed')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('security', function (Blueprint $table) {
            //
        });
    }
}
