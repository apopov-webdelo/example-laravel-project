<?php

use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AuthorIdAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('banks', function (Blueprint $table) {
                $table->unsignedInteger('author_id')->nullable()->change();
            });

            Schema::table('banks', function (Blueprint $table) {
                foreach (Bank::all() as $bank) {
                    $bank->author_id = null;
                    $bank->save();
                }

                $table->foreign('author_id')->references('id')->on('users');
            });

            Schema::table('countries', function (Blueprint $table) {
                $table->unsignedInteger('author_id')->nullable()->change();
            });

            Schema::table('countries', function (Blueprint $table) {
                foreach (Country::all() as $country) {
                    $country->author_id = null;
                    $country->save();
                }

                $table->foreign('author_id')->references('id')->on('users');
            });


            Schema::table('images', function (Blueprint $table) {
                $table->unsignedInteger('author_id')->nullable()->change();
            });

            Schema::table('images', function (Blueprint $table) {
                $table->foreign('author_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('banks', function (Blueprint $table) {
                $table->dropForeign('banks_author_id_foreign');
                $table->dropIndex('banks_author_id_foreign');
            });

            Schema::table('countries', function (Blueprint $table) {
                $table->dropForeign('countries_author_id_foreign');
                $table->dropIndex('countries_author_id_foreign');
            });

            Schema::table('images', function (Blueprint $table) {
                $table->dropForeign('images_author_id_foreign');
                $table->dropIndex('images_author_id_foreign');
            });
        });
    }
}
