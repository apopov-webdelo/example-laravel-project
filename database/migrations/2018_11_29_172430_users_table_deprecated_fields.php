<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersTableDeprecatedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('remember_token');
                $table->dropColumn('api_token');
            });

            Schema::table('users', function (Blueprint $table) {
                $table->string('remember_token', 100)
                      ->comment('deprecated, model uses Passport tokens')
                      ->nullable()
                      ->after('trade_locked_to');

                $table->string('api_token', 255)
                      ->comment('deprecated, model uses Passport tokens')
                      ->nullable()
                      ->after('remember_token');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('remember_token');
                $table->dropColumn('api_token');
            });

            Schema::table('users', function (Blueprint $table) {
                $table->string('remember_token', 100)
                      ->comment('')
                      ->nullable()
                      ->after('trade_locked_to');

                $table->string('api_token', 255)
                      ->comment('')
                      ->nullable()
                      ->after('remember_token');
            });
        });
    }
}
