<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDealsCashbackTableAddColumnCryptoCurrencyId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('deals_cashback', function (Blueprint $table) {
                $table->unsignedInteger('crypto_currency_id')->after('commission');
                $table->foreign('crypto_currency_id')
                    ->references('id')
                    ->on('crypto_currencies');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('deals_cashback', function (Blueprint $table) {
                $table->dropColumn('crypto_currency_id');
            });
        });
    }
}
