<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModelKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('model_bc_keys', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('model');
                $table->unsignedInteger('model_id');
                $table->string('alias', 20);
                $table->string('key');
                $table->timestamps();
                $table->softDeletes();

                $table->unique(['model', 'model_id', 'alias']);
                $table->index('deleted_at');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('model_bc_keys');
        });
    }
}
