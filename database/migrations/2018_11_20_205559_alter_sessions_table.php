<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Session\Session;

class AlterSessionsTable extends Migration
{
    /**
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('sessions', function (Blueprint $table) {
                $table->dropForeign('sessions_user_id_foreign');
            });
            Schema::table('sessions', function (Blueprint $table) {
                $table->unsignedInteger('user_id')->change();
                $table->index('user_id', 'idx_sessions_user_id');
            });

            Schema::table('sessions', function (Blueprint $table) {
                $table->string('sessionable_type', 200)->after('id');
                $table->integer('sessionable_id')->after('id');
            });
            DB::statement('UPDATE `sessions` SET `sessionable_type` = "App\\\Models\\\\User\\\User", `sessionable_id` = `sessions`.`user_id`');
            Schema::table('sessions', function (Blueprint $table) {
                $table->dropColumn('user_id');
            });
        });
    }

    /**
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('sessions', function (Blueprint $table) {
                $table->integer('user_id')->after('id');
            });
            DB::statement('UPDATE `sessions` SET `user_id` = `sessions`.`sessionable_id`');
            Schema::table('sessions', function (Blueprint $table) {
                $table->dropColumn(['sessionable_type', 'sessionable_id']);
            });

            Schema::table('sessions', function (Blueprint $table) {
                $table->dropIndex('idx_sessions_user_id');
                $table->unsignedInteger('user_id')->change();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }
}
