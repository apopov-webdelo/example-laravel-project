<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChatMessagesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('chat_messages', function (Blueprint $table) {
                $table->dropIndex('fk_chat_id_to_chats_idx');
                $table->dropIndex('fk_author_id_to_users_idx');

                $table->unsignedInteger('chat_id')->change();
                $table->unsignedInteger('author_id')->change();

                $table->foreign('chat_id')->references('id')->on('chat');
                $table->foreign('author_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('chat_messages', function (Blueprint $table) {
                $table->dropForeign('chat_messages_author_id_foreign');
                $table->dropForeign('chat_messages_chat_id_foreign');
            });

            Schema::table('chat_messages', function (Blueprint $table) {
                $table->integer('chat_id')->change();
                $table->integer('author_id')->change();

                $table->index('chat_id', 'fk_chat_id_to_chats_idx');
                $table->index('author_id', 'fk_author_id_to_users_idx');
            });
        });
    }
}
