<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TransactionsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('transactions', function (Blueprint $table) {
                $table->dropIndex('remitter_balance');
                $table->dropIndex('transactions_receiver_balance_id_foreign');

                $table->unsignedInteger('author_id')->change();
                $table->unsignedInteger('reason_id')->change();
                $table->unsignedInteger('remitter_balance_id')->change();
                $table->unsignedInteger('receiver_balance_id')->change();

                $table->foreign('author_id')->references('id')->on('users');
                $table->foreign('reason_id')->references('id')->on('transactions_reasons');
                $table->foreign('remitter_balance_id')->references('id')->on('balance');
                $table->foreign('receiver_balance_id')->references('id')->on('balance');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('transactions', function (Blueprint $table) {
                $table->dropForeign('transactions_author_id_foreign');
                $table->dropForeign('transactions_reason_id_foreign');
                $table->dropForeign('transactions_receiver_balance_id_foreign');
                $table->dropForeign('transactions_remitter_balance_id_foreign');
            });

            Schema::table('transactions', function (Blueprint $table) {
                $table->integer('author_id')->change();
                $table->integer('reason_id')->change();
                $table->integer('remitter_balance_id')->change();
                $table->integer('receiver_balance_id')->change();

                $table->index('remitter_balance_id', 'remitter_balance');
                $table->index('receiver_balance_id', 'transactions_receiver_balance_id_foreign');
            });
        });
    }
}
