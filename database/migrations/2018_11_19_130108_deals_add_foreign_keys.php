<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DealsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('deals', function (Blueprint $table) {
                Schema::disableForeignKeyConstraints();
                DB::table('deals')->truncate();

                $table->dropIndex('idx_deals_ad_id');
                $table->dropIndex('idx_deals_buyer_id');

                $table->unsignedInteger('ad_id')->change();
                $table->unsignedInteger('author_id')->change();

                $table->foreign('ad_id')->references('id')->on('ads');
                $table->foreign('author_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('deals', function (Blueprint $table) {
                $table->dropForeign('deals_ad_id_foreign');
                $table->dropForeign('deals_author_id_foreign');
            });

            Schema::table('deals', function (Blueprint $table) {
                $table->unsignedInteger('ad_id')->change();
                $table->unsignedInteger('author_id')->change();

                $table->index('ad_id', 'idx_deals_ad_id');
                $table->index('author_id', 'idx_deals_buyer_id');
            });
        });
    }
}
