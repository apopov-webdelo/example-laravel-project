<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDealColumnsUserStatistic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_statistics', function (Blueprint $table) {
            $table->renameColumn('deal_cancellation_percent', 'deals_cancellation_percent');
            $table->renameColumn('deal_finished_count', 'deals_finished_count');
            $table->renameColumn('deal_canceled_count', 'deals_canceled_count');
            $table->renameColumn('deal_disputed_count', 'deals_disputed_count');
            $table->renameColumn('deal_paid_count', 'deals_paid_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_statistics', function (Blueprint $table) {
            $table->renameColumn('deals_cancellation_percent', 'deal_cancellation_percent');
            $table->renameColumn('deals_finished_count', 'deal_finished_count');
            $table->renameColumn('deals_canceled_count', 'deal_canceled_count');
            $table->renameColumn('deals_disputed_count', 'deal_disputed_count');
            $table->renameColumn('deals_paid_count', 'deal_paid_count');
        });
    }
}
