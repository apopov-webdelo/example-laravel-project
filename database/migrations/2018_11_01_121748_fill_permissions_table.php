<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('permissions')->insert([
            ['name' => 'Ads', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Personal messages view', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Deal moderation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Show users', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'User activation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'User deactivation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'User start trading', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'User stop trading', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Balance view', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Sessions view', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Reset user password', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Deactivate google2fa', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Set email', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Currency settings', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Operators list view', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Operator creation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Operator update', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Operator delete', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Operator activation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Operator deactivation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Roles list view', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Role creation', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Role update', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Role delete', 'guard_name' => 'api', 'created_at' => now()],
            ['name' => 'Permissions list view', 'guard_name' => 'api', 'created_at' => now()],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('permissions')->truncate();
    }
}
