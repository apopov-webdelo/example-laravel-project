<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdsUpdateReviewRateField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('ads', function (Blueprint $table) {
                $table->dropColumn('review_rate');
            });

            Schema::table('ads', function (Blueprint $table) {
                $table->integer('review_rate')->after('deleted_at')->default(0);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('ads', function (Blueprint $table) {
                $table->dropColumn('review_rate');
            });

            Schema::table('ads', function (Blueprint $table) {
                $table->integer('review_rate')->after('deleted_at')->nullable();
            });
        });
    }
}
