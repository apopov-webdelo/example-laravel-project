<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SettingsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('settings', function (Blueprint $table) {
                DB::table('settings')->truncate();

                $table->dropIndex('fk_user_to_users_idx');
                $table->dropIndex('settings_country_id_index');
                $table->dropIndex('settings_language_id_index');

                $table->unsignedInteger('user_id')->change();
                $table->unsignedInteger('country_id')->default(1)->change();
                $table->unsignedInteger('language_id')->default(1)->change();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('country_id')->references('id')
                                             ->on('countries')->onDelete('cascade');
                $table->foreign('language_id')->references('id')
                                              ->on('languages')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('settings', function (Blueprint $table) {
                $table->dropForeign('settings_country_id_foreign');
                $table->dropForeign('settings_language_id_foreign');
                $table->dropForeign('settings_user_id_foreign');
            });

            Schema::table('settings', function (Blueprint $table) {
                $table->integer('user_id')->change();
                $table->integer('country_id')->change();
                $table->integer('language_id')->change();

                $table->index('user_id', 'fk_user_to_users_idx');
                $table->index('country_id', 'settings_country_id_index');
                $table->index('language_id', 'settings_language_id_index');
            });
        });
    }
}
