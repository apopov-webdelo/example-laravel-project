<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdsHasStatusesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('ads_has_statuses', function (Blueprint $table) {
                DB::table('ads_has_statuses')->truncate();

                $table->dropIndex('ads_has_statuses_ad_id_index');
                $table->dropIndex('ads_has_statuses_ad_id_status_id_index');

                $table->unsignedInteger('ad_id')->change();
                $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');

                $table->unsignedInteger('status_id')->change();
                $table->foreign('status_id')->references('id')->on('ads_statuses')->onDelete('cascade');

                $table->unsignedInteger('author_id')->change();
                $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('ads_has_statuses', function (Blueprint $table) {
                $table->dropForeign('ads_has_statuses_ad_id_foreign');
                $table->dropForeign('ads_has_statuses_author_id_foreign');
                $table->dropForeign('ads_has_statuses_status_id_foreign');
            });

            Schema::table('ads_has_statuses', function (Blueprint $table) {
                $table->dropIndex('ads_has_statuses_ad_id_foreign');
                $table->dropIndex('ads_has_statuses_status_id_foreign');
                $table->dropIndex('ads_has_statuses_author_id_foreign');
            });

            Schema::table('ads_has_statuses', function (Blueprint $table) {
                $table->integer('ad_id')->change();
                $table->integer('status_id')->change();
                $table->integer('author_id')->change();

                $table->index('ad_id', 'ads_has_statuses_ad_id_index');
                $table->index('status_id', 'ads_has_statuses_ad_id_status_id_index');
            });
        });
    }
}
