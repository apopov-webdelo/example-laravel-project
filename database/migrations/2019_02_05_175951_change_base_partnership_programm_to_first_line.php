<?php

use Illuminate\Database\Migrations\Migration;

class ChangeBasePartnershipProgrammToFirstLine extends Migration
{
    private $oldValue = 'base';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            $affected = DB::update(
                'update partnership_programs '.
                'set driver = \''.config('app.partnership.default_driver').'\' '.
                'where driver = \''.$this->oldValue.'\''
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            $affected = DB::update(
                'update partnership_programs '.
                'set driver = \''.$this->oldValue.'\' '.
                'where driver = \''.config('app.partnership.default_driver').'\''
            );
        });
    }
}
