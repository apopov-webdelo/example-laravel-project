<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdsDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function (Blueprint $table){
           $table->boolean('liquidity_required')->default(0)->change();
           $table->boolean('email_confirm_required')->default(0)->change();
           $table->boolean('phone_confirm_required')->default(0)->change();
           $table->boolean('trust_required')->default(0)->change();
           $table->boolean('is_active')->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
