<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DealsHasReviewsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('deals_has_reviews', function (Blueprint $table) {
                Schema::disableForeignKeyConstraints();
                DB::table('deals_has_reviews')->truncate();

                $table->dropIndex('fk_deals_has_reviews_reviews1_idx');
                $table->dropIndex('fk_deals_has_reviews_deals1_idx');

                $table->unsignedInteger('deal_id')->change();
                $table->unsignedInteger('review_id')->change();

                $table->foreign('deal_id')->references('id')->on('deals')->onDelete('cascade');
                $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('deals_has_reviews', function (Blueprint $table) {
                $table->dropForeign('deals_has_reviews_deal_id_foreign');
                $table->dropForeign('deals_has_reviews_review_id_foreign');
            });

            Schema::table('deals_has_reviews', function (Blueprint $table) {
                $table->unsignedInteger('deal_id')->change();
                $table->unsignedInteger('review_id')->change();

                $table->index('deal_id', 'fk_deals_has_reviews_reviews1_idx');
                $table->index('review_id', 'fk_deals_has_reviews_deals1_idx');
            });
        });
    }
}
