<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnAtSecurityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('security', function (Blueprint $table) {
            if (Schema::hasColumn('security', 'fa_confirmed')) {
                $table->dropColumn('fa_confirmed');
            }
            if (Schema::hasColumn('security', 'fa_token')) {
                $table->dropColumn('fa_token');
            }
            if (!Schema::hasColumn('security', 'google2fa_confirmed')) {
                $table->tinyInteger('google2fa_confirmed')->nullable();
            }
        });
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'google2fa_secret')) {
                $table->text('google2fa_secret')->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('security', function (Blueprint $table) {
            if (!Schema::hasColumn('security', 'fa_confirmed')) {
                $table->tinyInteger('fa_confirmed')->nullable();
            }
            if (!Schema::hasColumn('security', 'fa_token')) {
                $table->text('fa_token')->nullable();
            }
            if (Schema::hasColumn('security', 'google2fa_confirmed')) {
                $table->dropColumn('google2fa_confirmed');
            }
        });
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'google2fa_secret')) {
                $table->text('google2fa_secret')->nullable();
            }
        });
    }
}
