<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultToUsersStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_statistics', function (Blueprint $table) {
            $table->integer('total_turnover')->default(0)->change();
            $table->integer('deals_cancellation_percent')->default(0)->change();
            $table->integer('deals_finished_count')->default(0)->change();
            $table->integer('deals_canceled_count')->default(0)->change();
            $table->integer('deals_disputed_count')->default(0)->change();
            $table->integer('deals_paid_count')->default(0)->change();
            $table->integer('deals_count')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_statistics', function (Blueprint $table) {
            //
        });
    }
}
