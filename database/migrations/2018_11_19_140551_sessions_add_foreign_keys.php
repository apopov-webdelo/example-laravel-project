<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SessionsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('sessions', function (Blueprint $table) {
                $table->dropIndex('idx_sessions_user_id');

                $table->unsignedInteger('user_id')->change();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('sessions', function (Blueprint $table) {
                $table->dropForeign('sessions_user_id_foreign');
            });

            Schema::table('sessions', function (Blueprint $table) {
                $table->unsignedInteger('user_id')->change();
                $table->index('user_id', 'idx_sessions_user_id');
            });
        });
    }
}
