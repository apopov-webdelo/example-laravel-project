<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddMissingUsersToReputationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            // Add Existing Users to Reputation Table
            $users = DB::table('users')
                ->select('id')
                ->whereRaw('`id` NOT IN (SELECT `reputation`.`user_id` FROM `reputation`)')
                ->get();

            foreach ($users as $user) {
                DB::table('reputation')->insert(['user_id' => $user->id]);
            }
            
            // Remove Nonexistent Users from Reputation Table
            DB::table('reputation')
            ->whereRaw('`user_id` NOT IN (SELECT `users`.`id` FROM `users`)')
            ->delete();
        });
    }

    /**
     * Reverse the migrations is impossible
     *
     * @return void
     */
    public function down()
    {
        // Reverse the migrations is impossible
    }
}
