<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdsHasStatusesTableAddIpAndAuthorIdColumn extends Migration
{
    /**
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function (){
            if (false == Schema::hasColumns('ads_has_statuses', ['ip', 'author_id'])) {
                Schema::table('ads_has_statuses', function (Blueprint $table) {
                    $table->integer('author_id')->default(\App\Factories\RobotFactory::ROBOT_USER_ID)->after('status_id');
                    $table->ipAddress('ip')->after('author_id')->nullable();
                });
            };
        });
    }

    /**
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function (){
            if (Schema::hasColumns('ads_has_statuses', ['ip', 'author_id'])) {
                Schema::table('ads_has_statuses', function (Blueprint $table) {
                    $table->dropColumn(['ip', 'author_id']);
                });
            };
        });

    }
}
