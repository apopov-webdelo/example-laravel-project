<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdsHasBankAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('ads_has_banks', function (Blueprint $table) {
                $table->unsignedInteger('ad_id')->change();
                $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');

                $table->unsignedInteger('bank_id')->change();
                $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');

                $table->unsignedInteger('author_id')->change();
                $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('ads_has_banks', function (Blueprint $table) {
                $table->dropForeign('ads_has_banks_ad_id_foreign');
                $table->dropForeign('ads_has_banks_author_id_foreign');
                $table->dropForeign('ads_has_banks_bank_id_foreign');
            });

            Schema::table('ads_has_banks', function (Blueprint $table) {
                $table->integer('ad_id')->change();
                $table->integer('bank_id')->change();
                $table->integer('author_id')->change();
            });
        });
    }
}
