<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balance', function (Blueprint $table){
            $table->renameColumn('quility', 'turnover');
            $table->timestamp('updated_at')->useCurrent();;
            $table->integer('commission')->length(2)->default(1)->change();
            $table->index(['user_id', 'crypto_currency_id'], 'user_currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance', function (Blueprint $table){
            $table->renameColumn('turnover', 'quility');
            $table->dropColumn( 'update_at');
            $table->decimal('commission')->length(21)->change();
            $table->dropIndex('user_currency');
        });
    }
}
