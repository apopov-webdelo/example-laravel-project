<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRootUserForAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = DB::table('admins')->insertGetId(
            [
                'name' => 'Root',
                'login' => 'root',
                'email' => 'root@risex.net',
                'password' => bcrypt('3fhva4hdf56hs'),
                'api_token' => str_random(60),
                'created_at' => now()
            ]
        );
        DB::table('admins_security')->insert([
           [
               'admin_id'         => $id,
               'google2fa_status' => 'deactivated'
           ]
        ]);
        $permissions = [];
        foreach (\App\Models\Permission\Permission::all() as $permission) {
            $permissions[] = [
                'permission_id' => $permission->id,
                'model_type'    => \App\Models\Admin\Admin::class,
                'model_id'      => $id,
            ];
        }
        DB::table('model_has_permissions')->insert($permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('admins_security')
            ->whereRaw(' `admin_id` = (SELECT `id` FROM `admins` WHERE `login`="root" ) ')
            ->delete();

        DB::table('model_has_permissions')
            ->whereRaw(' 
                `model_id` = (SELECT `id` FROM `admins` WHERE `login`="root" ) 
                AND `model_type`="App\\\Models\\\Admin\\\Admin" 
            ')->delete();

        DB::table('admins')->where('login', '=', 'root')->delete();
    }
}
