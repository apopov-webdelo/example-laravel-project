<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealCashbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('deals_cashback', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->enum('status', ['in_progress', 'break', 'pending', 'done', 'canceled'])->index();
                $table->integer('commission');
                $table->string('transaction_id')->nullable()->unique()->comment('Transaction ID in blockchain');
                $table->timestamps();
                $table->softDeletes();
            });
            Schema::create('deals_cashback_has_deals', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('deal_id');
                $table->integer('deals_cashback_id');
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('deals_cashback');
            Schema::dropIfExists('deals_cashback_has_deals');
        });
    }
}
