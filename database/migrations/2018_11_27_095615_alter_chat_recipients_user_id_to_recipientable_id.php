<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChatRecipientsUserIdToRecipientableId extends Migration
{
    /**
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            // RECIPIENTS MIGRATION
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->dropForeign('chat_recipients_user_id_foreign');
            });
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->integer('recipientable_id')->after('chat_id');
                $table->string('recipientable_type')->nullable()->default('NULL')->after('recipientable_id');
            });
            DB::statement('
                UPDATE `chat_recipients` 
                SET `recipientable_id` = `user_id`, 
                    `recipientable_type` = "App\\\Models\\\\User\\\User"');
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->dropColumn('user_id');
            });

            // CHAT MIGRATION
            Schema::table('chat', function (Blueprint $table) {
                $table->dropForeign('chat_author_id_foreign');
                $table->string('author_type')->nullable()->default('NULL')->after('author_id');
            });
            DB::statement('UPDATE `chat` SET `author_type` = "App\\\Models\\\\User\\\User"');
            // MESSAGES MIGRATION
            Schema::table('chat_messages', function (Blueprint $table) {
                $table->dropForeign('chat_messages_author_id_foreign');
                $table->string('author_type')->nullable()->default('NULL')->after('author_id');
            });
            DB::statement('UPDATE `chat_messages` SET `author_type` = "App\\\Models\\\\User\\\User"');
        });
    }

    /**
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            // RECIPIENTS MIGRATION
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->unsignedInteger('user_id')->after('chat_id');
            });
            DB::statement('
                UPDATE `chat_recipients` 
                SET `user_id`=`recipientable_id`');
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->dropColumn('recipientable_id');
                $table->dropColumn('recipientable_type');
            });
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('users');
            });

            // CHAT MIGRATION
            Schema::table('chat', function (Blueprint $table) {
                $table->dropColumn('author_type');
                $table->foreign('author_id')->references('id')->on('users');
            });

            // MESSAGES MIGRATION
            Schema::table('chat_messages', function (Blueprint $table) {
                $table->dropColumn('author_type');
                $table->foreign('author_id')->references('id')->on('users');
            });
        });
    }
}
