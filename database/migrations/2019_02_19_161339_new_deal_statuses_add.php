<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewDealStatusesAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals_statuses', function (Blueprint $table) {
            $table->timestamps();
        });

        \App\Models\Deal\DealStatus::insert([
            [
                'id' => 7,
                'title' => 'Verified',
            ],
            [
                'id' => 8,
                'title' => 'Cancellation',
            ],
            [
                'id' => 9,
                'title' => 'Finishing',
            ],
        ]);

        \App\Models\Deal\DealStatus::find(1)->fill(['title' => 'Verification'])->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Deal\DealStatus::destroy([7, 8, 9]);

        \App\Models\Deal\DealStatus::find(1)->fill(['title' => 'In Progress'])->save();

        Schema::table('deals_statuses', function (Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at']);
        });
    }
}
