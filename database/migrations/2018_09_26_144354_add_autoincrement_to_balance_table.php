<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddAutoincrementToBalanceTable extends Migration
{
    /**
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            DB::raw('
                ALTER TABLE transactions
                    DROP FOREIGN KEY transactions_receiver_balance_id_foreign,
                    MODIFY receiver_balance_id INT(11) UNSIGNED;
                
                ALTER TABLE transactions
                    DROP FOREIGN KEY transactions_remitter_balance_id_foreign,
                    MODIFY remitter_balance_id INT(11) UNSIGNED;
                
                
                ALTER TABLE balance MODIFY id INT(11) UNSIGNED AUTO_INCREMENT;
                
                
                ALTER TABLE transactions
                    ADD CONSTRAINT transactions_receiver_balance_id_foreign FOREIGN KEY (receiver_balance_id)
                          REFERENCES balance (id);
                
                ALTER TABLE transactions
                    ADD CONSTRAINT transactions_remitter_balance_id_foreign FOREIGN KEY (remitter_balance_id)
                          REFERENCES balance (id);
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // no need to make down migration
    }
}
