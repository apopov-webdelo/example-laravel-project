<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('model_wallets', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('model');
                $table->unsignedInteger('model_id');
                $table->string('alias', 20);
                $table->string('wallet_id');
                $table->timestamps();
                $table->softDeletes();

                $table->unique(['model', 'model_id', 'alias']);
                $table->index('deleted_at');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('model_wallets');
        });
    }
}
