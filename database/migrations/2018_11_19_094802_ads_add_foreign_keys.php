<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdsAddForeignKeys extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('ads', function (Blueprint $table) {
                $table->dropIndex('idx_ads_user_id');
                $table->unsignedInteger('author_id')->change();
                $table->foreign('author_id')->references('id')->on('users');

                $table->dropIndex('fk_payment_system_to_payment_systems_idx');
                $table->unsignedInteger('payment_system_id')->change();
                $table->foreign('payment_system_id')->references('id')->on('payment_systems');

                $table->dropIndex('fk_country_id_to_countries_idx');
                $table->unsignedInteger('country_id')->change();
                $table->foreign('country_id')->references('id')->on('countries');

                $table->dropIndex('fk_currency_to_currencies_idx');
                $table->unsignedInteger('currency_id')->change();
                $table->foreign('currency_id')->references('id')->on('currencies');

                $table->dropIndex('fk_crypto_currency_to_currencies_idx');
                $table->unsignedInteger('crypto_currency_id')->change();
                $table->foreign('crypto_currency_id')->references('id')->on('crypto_currencies');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('ads', function (Blueprint $table) {
                $table->dropForeign('ads_author_id_foreign');
                $table->dropForeign('ads_country_id_foreign');
                $table->dropForeign('ads_crypto_currency_id_foreign');
                $table->dropForeign('ads_currency_id_foreign');
                $table->dropForeign('ads_payment_system_id_foreign');
            });

            Schema::table('ads', function (Blueprint $table) {
                $table->integer('payment_system_id')->change();
                $table->integer('country_id')->change();
                $table->integer('currency_id')->change();
                $table->integer('crypto_currency_id')->change();

                $table->index('author_id', 'idx_ads_user_id');
                $table->index('payment_system_id', 'fk_payment_system_to_payment_systems_idx');
                $table->index('country_id', 'fk_country_id_to_countries_idx');
                $table->index('currency_id', 'fk_currency_to_currencies_idx');
                $table->index('crypto_currency_id', 'fk_crypto_currency_to_currencies_idx');
            });
        });
    }
}
