<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('users', function (Blueprint $table) {
                $table->unsignedInteger('role_id')->change();
                $table->foreign('role_id')->references('id')->on('roles');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('users', function (Blueprint $table) {
                $table->dropForeign('users_role_id_foreign');
                $table->dropIndex('users_role_id_foreign');
            });

            Schema::table('users', function (Blueprint $table) {
                $table->integer('role_id')->change();
            });
        });
    }
}
