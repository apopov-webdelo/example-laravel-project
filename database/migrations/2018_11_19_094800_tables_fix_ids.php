<?php

use App\Services\User\RegistrationService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablesFixIds extends Migration
{
    protected $tables = [
        'users',
        'payment_systems',
        'countries',
        'currencies',
        'crypto_currencies',
        'chat',
        'chat_messages',
        'chat_recipients',
        'deals_statuses',
        'balance_locked',
        'settings',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            $this->truncateAll();

            foreach ($this->tables as $table_name) {
                Schema::table($table_name, function (Blueprint $table) {
                    $table->increments('id')->change();
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->tables as $table_name) {
                Schema::table($table_name, function (Blueprint $table) {
                    $table->increments('id')->change();
                });
            }
        });
    }

    private function truncateAll()
    {
        $arTables = [
            'ads',
            'ads_has_banks',
            'ads_has_statuses',
            'balance',
            'balance_locked',
            'chat',
            'chat_has_deals',
            'chat_messages',
            'chat_recipients',
            'deals',
            'deals_has_reviews',
            'deals_has_statuses',
            'failed_jobs',
            'images',
            'jobs',
            'reputation',
            'reviews',
            'security',
            'sessions',
            'settings',
            'transactions',
            'transactions_balances_states',
            'users',
            'users_notes',
            'users_statistics',
        ];

        Schema::disableForeignKeyConstraints();

        foreach ($arTables as $table) {
            DB::table($table)->truncate();
        }
    }
}
