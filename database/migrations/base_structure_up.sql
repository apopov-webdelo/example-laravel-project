# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: crypto
# Generation Time: 2018-06-07 09:30:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ads`;

CREATE TABLE `ads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `payment_system_id` int(11) NOT NULL,
  `notes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `is_sale` tinyint(1) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL,
  `crypto_currency_id` int(11) NOT NULL,
  `price` decimal(21,2) NOT NULL,
  `min` int(11) unsigned NOT NULL,
  `max` int(11) unsigned NOT NULL,
  `userquility` decimal(21,8) NOT NULL,
  `time` int(11) unsigned NOT NULL,
  `liquidity_required` tinyint(1) unsigned NOT NULL,
  `email_confirm_required` tinyint(1) unsigned NOT NULL,
  `phone_confirm_required` tinyint(1) unsigned NOT NULL,
  `trust_required` tinyint(1) unsigned NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL,
  `conditions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `review_rate` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ads_user_id` (`user_id`),
  KEY `fk_crypto_currency_to_currencies_idx` (`crypto_currency_id`),
  KEY `fk_currency_to_currencies_idx` (`currency_id`),
  KEY `fk_payment_system_to_payment_systems_idx` (`payment_system_id`),
  KEY `fk_country_id_to_countries_idx` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ads` WRITE;
/*!40000 ALTER TABLE `ads` DISABLE KEYS */;

INSERT INTO `ads` (`id`, `user_id`, `payment_system_id`, `notes`, `country_id`, `is_sale`, `currency_id`, `crypto_currency_id`, `price`, `min`, `max`, `userquility`, `time`, `liquidity_required`, `email_confirm_required`, `phone_confirm_required`, `trust_required`, `is_active`, `conditions`, `created_at`, `review_rate`)
VALUES
	(1,1,1,NULL,1,1,1,2,1000.00,100,100000,8.00000000,0,1,0,1,1,1,NULL,'2018-05-29 15:04:43',90);

/*!40000 ALTER TABLE `ads` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table balance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `balance`;

CREATE TABLE `balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `crypto_currency_id` int(11) NOT NULL,
  `balance` decimal(21,8) NOT NULL,
  `quility` decimal(21,8) NOT NULL,
  `escrow` decimal(21,8) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `commission` decimal(21,8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_balance_user_id` (`user_id`),
  KEY `fk_crypto_currency_to_currencies_idx` (`crypto_currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `balance` WRITE;
/*!40000 ALTER TABLE `balance` DISABLE KEYS */;

INSERT INTO `balance` (`id`, `user_id`, `crypto_currency_id`, `balance`, `quility`, `escrow`, `created_at`, `commission`)
VALUES
	(1,1,1,10.00000000,20.00000000,0.00000000,'2018-05-29 15:59:52',1.00000000),
	(2,1,2,10.00000000,220.00000000,0.00000000,'2018-05-29 15:59:56',1.00000000),
	(3,2,1,10.00000000,20.00000000,0.00000000,'2018-05-30 08:57:07',1.00000000),
	(4,2,2,10.00000000,20.00000000,0.00000000,'2018-05-30 08:57:15',1.00000000);

/*!40000 ALTER TABLE `balance` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table balance_locked
# ------------------------------------------------------------

DROP TABLE IF EXISTS `balance_locked`;

CREATE TABLE `balance_locked` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_id` int(11) DEFAULT NULL,
  `amount` decimal(21,8) DEFAULT NULL,
  `expiration_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_balance_id_to_balance_idx` (`balance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table chat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chat`;

CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_chat_user_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;

INSERT INTO `chat` (`id`, `title`, `created_at`, `author_id`)
VALUES
	(1,'Проверка чата','2018-05-31 10:17:51',1);

/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table chat_has_deals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chat_has_deals`;

CREATE TABLE `chat_has_deals` (
  `chat_id` int(10) unsigned NOT NULL,
  `deal_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chat_id`,`deal_id`),
  KEY `fk_chat_has_deals_deals1_idx` (`deal_id`),
  KEY `fk_chat_has_deals_chat1_idx` (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `chat_has_deals` WRITE;
/*!40000 ALTER TABLE `chat_has_deals` DISABLE KEYS */;

INSERT INTO `chat_has_deals` (`chat_id`, `deal_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'2018-05-31 16:00:41',NULL);

/*!40000 ALTER TABLE `chat_has_deals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table chat_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chat_messages`;

CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text,
  `chat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chat_id_to_chats_idx` (`chat_id`),
  KEY `fk_author_id_to_users_idx` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `chat_messages` WRITE;
/*!40000 ALTER TABLE `chat_messages` DISABLE KEYS */;

INSERT INTO `chat_messages` (`id`, `message`, `chat_id`, `created_at`, `updated_at`, `author_id`)
VALUES
	(1,'Hello!\n',1,'2018-05-31 11:02:07',NULL,1),
	(2,'Hi there!',1,'2018-05-31 11:03:05',NULL,2);

/*!40000 ALTER TABLE `chat_messages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table chat_recipients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chat_recipients`;

CREATE TABLE `chat_recipients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chat_id_to_chat_idx` (`chat_id`),
  KEY `fk_user_id_to_users_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `chat_recipients` WRITE;
/*!40000 ALTER TABLE `chat_recipients` DISABLE KEYS */;

INSERT INTO `chat_recipients` (`id`, `chat_id`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2018-05-31 10:18:11',NULL),
	(2,1,2,'2018-05-31 10:18:28',NULL);

/*!40000 ALTER TABLE `chat_recipients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `iso` int(11) DEFAULT NULL,
  `alpha2` char(2) DEFAULT NULL,
  `alpha3` char(3) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `title`, `iso`, `alpha2`, `alpha3`, `created_at`)
VALUES
	(1,'Russia',134343,'RU','RUS','2018-05-30 15:54:53');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table crypto_currencies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `crypto_currencies`;

CREATE TABLE `crypto_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `min_balance` decimal(7,4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `crypto_currencies` WRITE;
/*!40000 ALTER TABLE `crypto_currencies` DISABLE KEYS */;

INSERT INTO `crypto_currencies` (`id`, `title`, `code`, `min_balance`, `created_at`, `updated_at`)
VALUES
	(1,'Bitcoin','btc',0.0001,'2018-05-11 19:32:21',NULL),
	(2,'Etherium','eth',0.0010,'2018-05-11 19:32:21',NULL);

/*!40000 ALTER TABLE `crypto_currencies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table currencies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currencies`;

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;

INSERT INTO `currencies` (`id`, `title`, `code`, `created_at`, `updated_at`)
VALUES
	(1,'Рубль','RUB','2018-05-29 15:54:20',NULL),
	(2,'US Dollar','USD','2018-05-29 15:54:21',NULL);

/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deals`;

CREATE TABLE `deals` (
  `id` int(10) unsigned NOT NULL,
  `ad_id` int(10) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `price` decimal(21,2) NOT NULL,
  `fiat_amount` decimal(21,2) NOT NULL,
  `crypto_amount` decimal(21,8) NOT NULL,
  `commission` decimal(21,8) DEFAULT NULL,
  `time` int(11) unsigned NOT NULL,
  `conditions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_deals_ad_id` (`ad_id`),
  KEY `idx_deals_buyer_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `deals` WRITE;
/*!40000 ALTER TABLE `deals` DISABLE KEYS */;

INSERT INTO `deals` (`id`, `ad_id`, `author_id`, `price`, `fiat_amount`, `crypto_amount`, `commission`, `time`, `conditions`, `created_at`)
VALUES
	(1,1,2,1000.00,100.00,1.00000000,0.00020000,0,NULL,'2018-05-31 14:03:43');

/*!40000 ALTER TABLE `deals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deals_has_reviews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deals_has_reviews`;

CREATE TABLE `deals_has_reviews` (
  `deal_id` int(10) unsigned NOT NULL,
  `review_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`deal_id`,`review_id`),
  KEY `fk_deals_has_reviews_reviews1_idx` (`review_id`),
  KEY `fk_deals_has_reviews_deals1_idx` (`deal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `deals_has_reviews` WRITE;
/*!40000 ALTER TABLE `deals_has_reviews` DISABLE KEYS */;

INSERT INTO `deals_has_reviews` (`deal_id`, `review_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `deals_has_reviews` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deals_has_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deals_has_statuses`;

CREATE TABLE `deals_has_statuses` (
  `deal_id` int(10) unsigned NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`deal_id`,`status_id`),
  KEY `fk_deals_has_deals_statuses_deals_statuses1_idx` (`status_id`),
  KEY `fk_deals_has_deals_statuses_deals1_idx` (`deal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `deals_has_statuses` WRITE;
/*!40000 ALTER TABLE `deals_has_statuses` DISABLE KEYS */;

INSERT INTO `deals_has_statuses` (`deal_id`, `status_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'2018-05-31 14:11:29',NULL);

/*!40000 ALTER TABLE `deals_has_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deals_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deals_statuses`;

CREATE TABLE `deals_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `deals_statuses` WRITE;
/*!40000 ALTER TABLE `deals_statuses` DISABLE KEYS */;

INSERT INTO `deals_statuses` (`id`, `title`)
VALUES
	(1,'In process'),
	(2,'Part'),
	(3,'Finished'),
	(4,'Autocanceled'),
	(5,'Canceled'),
	(6,'In dispute');

/*!40000 ALTER TABLE `deals_statuses` ENABLE KEYS */;
UNLOCK TABLES;

# Dump of table payment_systems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_systems`;

CREATE TABLE `payment_systems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `payment_systems` WRITE;
/*!40000 ALTER TABLE `payment_systems` DISABLE KEYS */;

INSERT INTO `payment_systems` (`id`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'QIWI','2018-05-30 15:54:21',NULL);

/*!40000 ALTER TABLE `payment_systems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reputation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reputation`;

CREATE TABLE `reputation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `positive_count` int(11) unsigned NOT NULL DEFAULT '0',
  `negative_count` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_reputation_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `reputation` WRITE;
/*!40000 ALTER TABLE `reputation` DISABLE KEYS */;

INSERT INTO `reputation` (`id`, `user_id`, `positive_count`, `negative_count`, `created_at`, `updated_at`)
VALUES
	(1,1,2,1,'2018-05-30 11:17:17',NULL),
	(2,2,0,0,'2018-06-05 15:49:01','2018-06-05 15:49:01'),
	(3,10,0,0,'2018-06-05 15:50:58','2018-06-05 15:50:58'),
	(4,11,0,0,'2018-06-05 15:54:49','2018-06-05 15:54:49'),
	(5,12,0,0,'2018-06-05 15:57:09','2018-06-05 15:57:09'),
	(6,13,0,0,'2018-06-05 15:58:17','2018-06-05 15:58:17');

/*!40000 ALTER TABLE `reputation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reviews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reviews`;

CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL,
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` int(11) unsigned NOT NULL,
  `trust` enum('positive','neutral','negative') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_user_id` int(10) unsigned NOT NULL,
  `to_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_reviews_from_user_id` (`from_user_id`),
  KEY `idx_reviews_to_user_id` (`to_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;

INSERT INTO `reviews` (`id`, `msg`, `rate`, `trust`, `created_at`, `from_user_id`, `to_user_id`)
VALUES
	(1,NULL,4,'positive','2018-05-29 17:57:27',1,2);

/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table security
# ------------------------------------------------------------

DROP TABLE IF EXISTS `security`;

CREATE TABLE `security` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `email_confirmed` tinyint(1) DEFAULT NULL,
  `email_access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_inactive` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_confirm_token` varchar(255) DEFAULT NULL,
  `fa_confirmed` tinyint(1) DEFAULT NULL,
  `fa_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_confirmed` tinyint(4) DEFAULT NULL,
  `phone_access_token` varchar(255) DEFAULT NULL,
  `phone_inactive` varchar(255) DEFAULT NULL,
  `phone_confirm_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_security_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `security` WRITE;
/*!40000 ALTER TABLE `security` DISABLE KEYS */;

INSERT INTO `security` (`id`, `user_id`, `email_confirmed`, `email_access_token`, `email_inactive`, `email_confirm_token`, `fa_confirmed`, `fa_token`, `phone`, `phone_confirmed`, `phone_access_token`, `phone_inactive`, `phone_confirm_token`, `created_at`, `updated_at`)
VALUES
	(1,1,NULL,NULL,NULL,NULL,NULL,NULL,'+37367377178',NULL,NULL,NULL,NULL,'2018-06-05 18:33:40',NULL),
	(2,2,NULL,NULL,NULL,NULL,NULL,NULL,'+37367377178',NULL,NULL,NULL,NULL,'2018-06-05 15:49:01','2018-06-05 15:49:01'),
	(3,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-05 15:50:58','2018-06-05 15:50:58'),
	(4,11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-05 15:54:49','2018-06-05 15:54:49'),
	(5,12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-05 15:57:09','2018-06-05 15:57:09'),
	(6,13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-06-05 15:58:17','2018-06-05 15:58:17');

/*!40000 ALTER TABLE `security` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime NOT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_sessions_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `user_id`, `ip`, `country`, `city`, `time`, `type`, `status`, `created_at`)
VALUES
	(1,1,'127.0.0.0','United States','New Haven','2018-06-05 13:31:09','in','good','2018-06-05 16:31:09'),
	(2,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 13:31:51','in','good','2018-06-05 16:31:51'),
	(3,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 13:37:35','in','bad','2018-06-05 16:37:35'),
	(4,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 13:38:25','in','bad','2018-06-05 16:38:25'),
	(5,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 13:38:31','in','good','2018-06-05 16:38:31'),
	(6,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 13:39:19','out','good','2018-06-05 16:39:19'),
	(7,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:29:58','in','good','2018-06-05 17:29:58'),
	(8,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:30:14','in','good','2018-06-05 17:30:14'),
	(9,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:30:30','out','good','2018-06-05 17:30:30'),
	(10,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:42:41','in','good','2018-06-05 17:42:41'),
	(11,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:48:05','in','good','2018-06-05 17:48:05'),
	(12,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:51:17','in','good','2018-06-05 17:51:17'),
	(13,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:51:42','in','good','2018-06-05 17:51:42'),
	(14,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:52:53','in','good','2018-06-05 17:52:53'),
	(15,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 14:53:34','in','good','2018-06-05 17:53:34'),
	(16,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 15:00:12','in','good','2018-06-05 18:00:12'),
	(17,1,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-05 15:05:36','in','good','2018-06-05 18:05:36'),
	(18,2,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-06 10:27:09','in','good','2018-06-06 13:27:09'),
	(19,13,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-06 10:27:31','in','bad','2018-06-06 13:27:31'),
	(20,13,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-06 10:27:39','in','bad','2018-06-06 13:27:39'),
	(21,2,'188.212.28.233','Republic of Moldova','Chisinau','2018-06-06 10:30:30','in','good','2018-06-06 13:30:30');

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email_notification_required` tinyint(1) NOT NULL DEFAULT '0',
  `phone_notification_required` tinyint(1) NOT NULL DEFAULT '0',
  `telegram_notification_required` tinyint(1) NOT NULL DEFAULT '0',
  `deal_notification_required` tinyint(1) NOT NULL DEFAULT '0',
  `message_notification_required` tinyint(1) NOT NULL DEFAULT '0',
  `push_notification_required` tinyint(1) NOT NULL DEFAULT '0',
  `telegram_id` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_to_users_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `user_id`, `email_notification_required`, `phone_notification_required`, `telegram_notification_required`, `deal_notification_required`, `message_notification_required`, `push_notification_required`, `telegram_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,0,1,1,1,'33242342342','2018-05-30 13:59:09','2018-06-05 10:25:23'),
	(2,2,1,1,0,1,1,1,NULL,'2018-06-05 15:49:01','2018-06-05 15:49:01'),
	(3,NULL,0,0,0,0,0,0,NULL,'2018-06-05 15:50:58','2018-06-05 15:50:58'),
	(4,NULL,0,0,0,0,0,0,NULL,'2018-06-05 15:54:49','2018-06-05 15:54:49'),
	(5,NULL,0,0,0,0,0,0,NULL,'2018-06-05 15:57:09','2018-06-05 15:57:09'),
	(6,13,0,0,0,0,0,0,NULL,'2018-06-05 15:58:17','2018-06-05 15:58:17');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_locked_at` timestamp NULL DEFAULT NULL,
  `user_locked_to` timestamp NULL DEFAULT NULL,
  `trade_locked_at` timestamp NULL DEFAULT NULL,
  `trade_locked_to` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `api_token` varchar(255) DEFAULT NULL,
  `one_signal_player_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_idx_users_login` (`login`),
  UNIQUE KEY `unq_idx_users_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `login`, `password`, `email`, `created_at`, `updated_at`, `user_locked_at`, `user_locked_to`, `trade_locked_at`, `trade_locked_to`, `remember_token`, `api_token`, `one_signal_player_id`)
VALUES
	(1,'RainXC','$2y$10$qR7kBUcB8M1MVkD45ZuSe.cWZ3ffGNIZoAUCmO3NAAcOOXjF6FqxG','d.cercel@webdelo.org','2018-05-28 15:08:52','2018-06-05 15:05:35',NULL,NULL,NULL,NULL,'px1ow37dHq6dzpVHq741WwQdHCMSg874nl5cMByxKPYdk3oeB1JV80U3RF45','ESgyPSvpbVdmSjGWO2mdmefG7hTgvBTFlk5r0U4JY8e1l3m92jGv1KFOCVNI','3c8f60cf-94f5-426d-b129-9303749a38de'),
	(2,'RainXC2','$2y$10$qR7kBUcB8M1MVkD45ZuSe.cWZ3ffGNIZoAUCmO3NAAcOOXjF6FqxG','d.cercel2@webdelo.org','2018-05-28 15:08:52','2018-06-06 10:30:30',NULL,NULL,NULL,NULL,'px1ow37dHq6dzpVHq741WwQdHCMSg874nl5cMByxKPYdk3oeB1JV80U3RF45','qSrs62zBxnZHWBgZZHcN3I53fKE7Hxth3rIz3ITbjtkG3b6WuIBQo4gOcI79','3c8f60cf-94f5-426d-b129-9303749a38de'),
	(3,'RainXC3','$2y$10$qR7kBUcB8M1MVkD45ZuSe.cWZ3ffGNIZoAUCmO3NAAcOOXjF6FqxG','d.cercel3@webdelo.org','2018-05-28 15:08:52','2018-05-29 08:23:46',NULL,NULL,NULL,NULL,'px1ow37dHq6dzpVHq741WwQdHCMSg874nl5cMByxKPYdk3oeB1JV80U3RF45','IGJc3z4HIROYIGXHq40vS3kLx7ZjKhNRi3e0MyypT8Duokb1z2Q4dspHUKSR',NULL),
	(9,'quisquam','$2y$10$RJKiP3J.8tgcx2.abOvNzu7Sv2ybu5LAV19pEKuFfBjKzAfIvnuDe','rose43@example.org','2018-06-05 15:49:01','2018-06-05 15:49:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,'quisquam2','$2y$10$A7sBmkjojw4Uz1EnmFvhcOWVLeF8EnxshhECfika4OgLpWh020W3a','rose343@example.org','2018-06-05 15:50:58','2018-06-05 15:50:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,'quisquam4','$2y$10$eqhlnOHeVJqSpRE3ripq2ufW9H7YrrIl2GUQqDKumMevl3xABOeDG','rose3343@example.org','2018-06-05 15:54:49','2018-06-05 15:54:49',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,'quisquam5','$2y$10$Em/PBVUMeExwOqU6JripieuZS/XfuVsMQFiw71Z0BOQHbVghOaMQG','rainxc@example.org','2018-06-05 15:57:09','2018-06-05 15:57:09',NULL,NULL,NULL,NULL,NULL,'AudBOpGvnNcPx4lUy13CZ0WVULXPrCsS1QcJytWqWUB41FuGmdzG5bqatpz1',NULL),
	(13,'test','$2y$10$EHEWDWp.59nap5P0nkay3OjjGeF2/iom31mQ.5p8yB0hUzP99VBd.','test@example.org','2018-06-05 15:58:17','2018-06-05 15:58:17',NULL,NULL,NULL,NULL,NULL,'CU1SVHGwJ3qArYt3pkjDxV5wzDsxGEanNMVMvfDIMTzOFX8pgb7LemV7v7fM',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
