<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMoneyAmountTypeInAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_reasons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reason');
        });

        $this->insertReason(['deal', 'transfer', 'blockchain', 'exchange', 'code', 'commission']);

        if (Schema::hasColumn('transactions', 'type'))
        {
            Schema::table('transactions', function (Blueprint $table)
            {
                $table->dropColumn('type');
            });
        }

        if (Schema::hasColumn('transactions', 'reason'))
        {
            Schema::table('transactions', function (Blueprint $table)
            {
                $table->dropColumn('reason');
            });
        }

        Schema::table('transactions_balances_states', function (Blueprint $table) {
            $table->bigInteger('amount')->default(0)->change();
        });

        Schema::table('balance_locked', function (Blueprint $table) {
            $table->bigInteger('amount')->default(0)->change();
        });

        Schema::table('crypto_currencies', function (Blueprint $table) {
            $table->bigInteger('min_balance')->default(0)->change();
        });
        Schema::table('balance', function (Blueprint $table) {
            $table->bigInteger('amount')->default(0)->change();
            $table->bigInteger('turnover')->default(0)->change();
            $table->bigInteger('escrow')->default(0)->change();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->bigInteger('amount')->default(0)->change();
            $table->integer('reason_id')->default('2')->after('amount');
        });
    }

    /**
     * @param array $reasons
     * @return $this
     */
    protected function insertReason(array $reasons)
    {
        foreach ($reasons as $reason)
            DB::table('transactions_reasons')->insert(
                array(
                    'reason' => $reason,
                )
            );
        return $this;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance', function (Blueprint $table) {
            $table->float('amount')->default(0)->change();
            $table->float('turnover')->default(0)->change();
            $table->float('escrow')->default(0)->change();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->float('amount')->default(0)->change();
            $table->enum('type', ['withdraw', 'deposit']);
            $table->enum('reason', ['deal', 'transfer', 'blockchain', 'exchange', 'code', 'commission']);
            $table->dropColumn('reason_id');
        });

        Schema::table('transactions_balances_states', function (Blueprint $table) {
            $table->float('amount')->default(0)->change();
        });

        Schema::table('balance_locked', function (Blueprint $table) {
            $table->float('amount')->default(0)->change();
        });

        Schema::table('crypto_currencies', function (Blueprint $table) {
            $table->float('min_balance')->default(0)->change();
        });

        Schema::drop('transactions_reasons');
    }
}
