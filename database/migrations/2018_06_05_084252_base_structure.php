<?php

use Illuminate\Database\Migrations\Migration;

class BaseStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->run('base_structure_up.sql');
    }

    /**
     * @param string $filename
     * @return string
     */
    private function run(string $filename)
    {
        $base = config('database.connections.mysql.database');
        $host = config('database.connections.mysql.host');
        $port = config('database.connections.mysql.port');
        $user = config('database.connections.mysql.username');
        $pass = config('database.connections.mysql.password');
        return exec("cd database/migrations/; mysql -u $user -h $host -P $port -p$pass $base < $filename");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->run('base_structure_down.sql');
    }
}
