<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableAdsAddSoftDeletes extends Migration
{
    /**
     * @throws Exception
     */
    public function up()
    {
        DB::beginTransaction();

        try {

            Schema::table('ads', function (Blueprint $table) {
                $table->softDeletes()->after('updated_at');
            });

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
    }

    /**
     *
     */
    public function down()
    {
        DB::beginTransaction();

        try {

            Schema::table('ads', function (Blueprint $table) {
                $table->dropColumn('deleted_at');
            });

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
    }
}
