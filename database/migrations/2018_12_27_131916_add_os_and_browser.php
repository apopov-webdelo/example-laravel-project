<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOsAndBrowser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('sessions', function (Blueprint $table) {
                $table->string('os', 50)->nullable()->after('city');
                $table->string('browser', 50)->nullable()->after('os');
                $table->dropColumn('time');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('sessions', function (Blueprint $table) {
                $table->dropColumn('os');
                $table->dropColumn('browser');
                $table->dateTime('time')->after('city');
            });
        });
    }
}
