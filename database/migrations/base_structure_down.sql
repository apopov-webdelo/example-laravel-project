# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: crypto
# Generation Time: 2018-06-04 05:02:47 +0000
# ************************************************************


DROP TABLE IF EXISTS `ads`;

DROP TABLE IF EXISTS `balance`;

DROP TABLE IF EXISTS `balance_locked`;

DROP TABLE IF EXISTS `chat`;

DROP TABLE IF EXISTS `chat_has_deals`;

DROP TABLE IF EXISTS `chat_messages`;

DROP TABLE IF EXISTS `chat_recipients`;

DROP TABLE IF EXISTS `countries`;

DROP TABLE IF EXISTS `crypto_currencies`;

DROP TABLE IF EXISTS `currencies`;

DROP TABLE IF EXISTS `deals`;

DROP TABLE IF EXISTS `deals_has_statuses`;

DROP TABLE IF EXISTS `deals_has_reviews`;

DROP TABLE IF EXISTS `deals_statuses`;

DROP TABLE IF EXISTS `migrations`;

DROP TABLE IF EXISTS `payment_systems`;

DROP TABLE IF EXISTS `reputation`;

DROP TABLE IF EXISTS `reviews`;

DROP TABLE IF EXISTS `security`;

DROP TABLE IF EXISTS `sessions`;

DROP TABLE IF EXISTS `settings`;

DROP TABLE IF EXISTS `users`;