<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterImagesTableAddAuthorType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropForeign('images_author_id_foreign');
            $table->string('author_type')->nullable()->default('NULL')->after('author_id');
        });
        DB::statement('UPDATE `images` SET `author_type` = "App\\\Models\\\\User\\\User"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->foreign('images_author_id_foreign')->references('id')->on('users');
            $table->dropColumn('author_type');
        });
    }
}
