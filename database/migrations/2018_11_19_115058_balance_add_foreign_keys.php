<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BalanceAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('balance', function (Blueprint $table) {
                $table->dropIndex('idx_balance_user_id');
                $table->dropIndex('fk_crypto_currency_to_currencies_idx');

                $table->foreign('user_id')->references('id')->on('users');

                $table->unsignedInteger('crypto_currency_id')->change();
                $table->foreign('crypto_currency_id')->references('id')->on('crypto_currencies');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('balance', function (Blueprint $table) {
                $table->dropForeign('balance_crypto_currency_id_foreign');
                $table->dropForeign('balance_user_id_foreign');
            });

            Schema::table('balance', function (Blueprint $table) {
                $table->integer('crypto_currency_id')->change();

                $table->index('user_id', 'idx_balance_user_id');
                $table->index('crypto_currency_id', 'fk_crypto_currency_to_currencies_idx');
            });
        });
    }
}
