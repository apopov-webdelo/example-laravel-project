<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersStatisticsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('users_statistics', function (Blueprint $table) {
                DB::table('users_statistics')->truncate();
                $table->unsignedInteger('user_id')->change();
                $table->foreign('user_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('users_statistics', function (Blueprint $table) {
                $table->dropForeign('users_statistics_user_id_foreign');
                $table->dropIndex('users_statistics_user_id_foreign');
            });

            Schema::table('users_statistics', function (Blueprint $table) {
                $table->unsignedInteger('user_id')->change();
            });
        });
    }
}
