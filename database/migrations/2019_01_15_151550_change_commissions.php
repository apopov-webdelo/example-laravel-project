<?php

use App\Models\Directory\CommissionConstants;
use Illuminate\Database\Migrations\Migration;

class ChangeCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach (\App\Models\Ad\Ad::all() as $ad) {
                $ad->commission_percent = CommissionConstants::ONE_PERCENT;
                $ad->save();
            }

            foreach (\App\Models\Balance\Balance::all() as $balance) {
                $balance->commission = CommissionConstants::ONE_PERCENT;
                $balance->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach (\App\Models\Ad\Ad::all() as $ad) {
                $ad->commission_percent = 1;
                $ad->save();
            }

            foreach (\App\Models\Balance\Balance::all() as $balance) {
                $balance->commission = 1;
                $balance->save();
            }
        });
    }
}
