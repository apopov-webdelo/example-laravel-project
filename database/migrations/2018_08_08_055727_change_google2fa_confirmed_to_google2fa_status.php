<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGoogle2faConfirmedToGoogle2faStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('security', function (Blueprint $table) {
            $table->dropColumn('google2fa_confirmed');
            $table->enum('google2fa_status', ['deactivated', 'activated' , 'paused']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('security', function (Blueprint $table) {
            $table->dropColumn('google2fa_status');
            $table->boolean('google2fa_confirmed');
        });
    }
}
