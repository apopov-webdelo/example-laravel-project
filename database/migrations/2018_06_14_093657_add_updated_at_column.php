<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedAtColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->addTimestampColumn('chat')
             ->addTimestampColumn('ads')
             ->addTimestampColumn('deals')
             ->addTimestampColumn('reviews');
    }

    /**
     * @param string $table
     * @return $this
     */
    protected function addTimestampColumn(string $table)
    {
        Schema::table($table, function (Blueprint $table){
            $table->addColumn('timestamp', 'updated_at')->after('created_at')->default(now());
        });
        return $this;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->removeTimestamlColumn('chat')
             ->removeTimestamlColumn('ads')
             ->removeTimestamlColumn('deals')
             ->removeTimestamlColumn('reviews');
    }

    protected function removeTimestamlColumn(string $table)
    {
        Schema::table($table, function (Blueprint $table){
            $table->dropColumn(['updated_at']);
        });
        return $this;
    }
}
