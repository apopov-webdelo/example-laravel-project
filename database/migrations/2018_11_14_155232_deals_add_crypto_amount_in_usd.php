<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DealsAddCryptoAmountInUsd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('deals', function (Blueprint $table) {
                $table->bigInteger('crypto_amount_in_usd')->after('crypto_amount')->default(0);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('deals', function (Blueprint $table) {
                $table->dropColumn('crypto_amount_in_usd');
            });
        });
    }
}
