<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReviewsAddForeignKeys extends Migration
{
    // https://stackoverflow.com/questions/33140860/laravel-5-1-unknown-database-type-enum-requested
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('reviews', function (Blueprint $table) {
                Schema::disableForeignKeyConstraints();
                DB::table('reviews')->truncate();

                $table->unsignedInteger('author_id')->change();
                $table->unsignedInteger('recipient_id')->change();
                $table->unsignedInteger('deal_id')->change();

                $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('recipient_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('deal_id')->references('id')->on('deals')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('reviews', function (Blueprint $table) {
                $table->dropForeign('reviews_author_id_foreign');
                $table->dropForeign('reviews_deal_id_foreign');
                $table->dropForeign('reviews_recipient_id_foreign');
            });

            Schema::table('reviews', function (Blueprint $table) {
                $table->integer('author_id')->change();
                $table->integer('recipient_id')->change();
                $table->integer('deal_id')->change();

                $table->dropIndex('reviews_author_id_foreign');
                $table->dropIndex('reviews_recipient_id_foreign');
                $table->dropIndex('reviews_deal_id_foreign');
            });
        });
    }
}
