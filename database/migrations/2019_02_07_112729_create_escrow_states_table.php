<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEscrowStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('escrow_states', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('balance_id');
                $table->unsignedBigInteger('escrow_before');
                $table->unsignedBigInteger('escrow');
                $table->unsignedBigInteger('escrow_after');
                $table->unsignedBigInteger('balance_before');
                $table->unsignedBigInteger('balance_after');
                $table->timestamps();

                $table->foreign('balance_id')->references('id')->on('balance');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('escrow_states');
        });
    }
}
