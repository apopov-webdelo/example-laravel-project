<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DealsHasStatusesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('deals_has_statuses', function (Blueprint $table) {
                Schema::disableForeignKeyConstraints();
                DB::table('deals_has_statuses')->truncate();

                $table->dropIndex('fk_deals_has_deals_statuses_deals_statuses1_idx');
                $table->dropIndex('fk_deals_has_deals_statuses_deals1_idx');

                $table->unsignedInteger('deal_id')->change();
                $table->unsignedInteger('status_id')->change();

                $table->foreign('deal_id')->references('id')->on('deals')->onDelete('cascade');
                $table->foreign('status_id')->references('id')->on('deals_statuses')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('deals_has_statuses', function (Blueprint $table) {
                $table->dropForeign('deals_has_statuses_deal_id_foreign');
                $table->dropForeign('deals_has_statuses_status_id_foreign');
            });

            Schema::table('deals_has_statuses', function (Blueprint $table) {
                $table->unsignedInteger('deal_id')->change();
                $table->unsignedInteger('status_id')->change();

                $table->index('deal_id', 'fk_deals_has_deals_statuses_deals1_idx');
                $table->index('status_id', 'fk_deals_has_deals_statuses_deals_statuses1_idx');
            });
        });
    }
}
