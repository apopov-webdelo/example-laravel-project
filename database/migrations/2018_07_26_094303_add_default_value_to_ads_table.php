<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToAdsTable extends Migration
{
    /**
     * @throws Exception
     */
    public function up()
    {
        DB::beginTransaction();

        try {

            Schema::table('ads', function (Blueprint $table) {
                $table->bigInteger('original_max')->default(0)->change();
                $table->bigInteger('min')->change();
                $table->bigInteger('max')->change();
            });

            \DB::statement('UPDATE `ads` SET `original_max` = `max`');

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
    }

    /**
     * @throws Exception
     */
    public function down()
    {
        DB::beginTransaction();

        try {

            Schema::table('ads', function (Blueprint $table) {
                $table->integer('original_max')->change();
                $table->integer('max')->change();
                $table->integer('min')->change();
            });

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

    }
}
