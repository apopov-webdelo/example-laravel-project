<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChatRecipientsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->dropIndex('fk_chat_id_to_chat_idx');
                $table->dropIndex('fk_user_id_to_users_idx');

                $table->unsignedInteger('chat_id')->change();
                $table->unsignedInteger('user_id')->change();

                $table->foreign('chat_id')->references('id')->on('chat');
                $table->foreign('user_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->dropForeign('chat_recipients_chat_id_foreign');
                $table->dropForeign('chat_recipients_user_id_foreign');
            });

            Schema::table('chat_recipients', function (Blueprint $table) {
                $table->integer('chat_id')->change();
                $table->integer('user_id')->change();

                $table->index('chat_id', 'fk_chat_id_to_chat_idx');
                $table->index('user_id', 'fk_user_id_to_users_idx');
            });
        });
    }
}
