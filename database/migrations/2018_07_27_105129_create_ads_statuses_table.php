<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsStatusesTable extends Migration
{
    /**
     * @throws Exception
     */
    public function up()
    {
        DB::beginTransaction();

        try {
            if (Schema::hasTable('ads_statuses') == false) {
                Schema::create('ads_statuses', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('title');
                    $table->timestamps();
                });
            }

            if (Schema::hasTable('ads_has_statuses') == false) {
                Schema::create('ads_has_statuses', function (Blueprint $table) {
                    $table->integer('ad_id');
                    $table->integer('status_id');
                    $table->timestamps();

                    $table->index('ad_id');
                    $table->index(['ad_id', 'status_id']);
                });

                $data = [
                    ['title' => 'Active'],
                    ['title' => 'Blocked'],
                ];

                DB::table('ads_statuses')->insert($data);

                foreach (\App\Models\Ad\Ad::all(['id', 'is_active']) as $ad) {
                    $data = [
                        'ad_id'     => $ad->id,
                        'status_id' => $ad->is_active
                            ? \App\Models\Ad\AdStatusConstants::ACTIVE
                            : \App\Models\Ad\AdStatusConstants::BLOCKED,
                    ];
                    DB::table('ads_has_statuses')->insert($data);
                }
            }

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
    }

    /**
     * @throws Exception
     */
    public function down()
    {
        DB::beginTransaction();

        try {

            Schema::dropIfExists('ads_has_statuses');
            Schema::dropIfExists('ads_statuses');

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
    }
}
