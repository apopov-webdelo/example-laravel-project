<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class AddFieldToCountryTableForDefaultCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function (){
            Schema::table('countries', function (Blueprint $table) {
                $table->integer('currency_id')->default(1)->after('title');
            });
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function (){
            Schema::table('countries', function (Blueprint $table) {
                $table->dropColumn('currency_id');
            });
        });
    }
}
