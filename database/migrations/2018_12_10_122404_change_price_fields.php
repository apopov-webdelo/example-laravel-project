<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->unsignedBigInteger('price')->change();
            $table->unsignedBigInteger('min')->change();
            $table->unsignedBigInteger('max')->change();
            $table->unsignedBigInteger('original_max')->change();
            $table->unsignedBigInteger('turnover')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->BigInteger('price')->change();
            $table->BigInteger('min')->change();
            $table->BigInteger('max')->change();
            $table->BigInteger('original_max')->change();
            $table->BigInteger('turnover')->change();
        });
    }
}
