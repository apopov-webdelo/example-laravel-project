<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersStatisticsTableAddReviewsCoefficientColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_statistics', function (Blueprint $table) {
            $table->integer('reviews_coefficient')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_statistics', function (Blueprint $table) {
            $table->dropColumn('reviews_coefficient');
        });
    }
}
