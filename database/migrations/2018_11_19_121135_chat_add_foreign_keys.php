<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChatAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('chat', function (Blueprint $table) {
                $table->dropIndex('idx_chat_user_id');

                $table->foreign('author_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('chat', function (Blueprint $table) {
                $table->dropForeign('chat_author_id_foreign');
                $table->index('author_id', 'idx_chat_user_id');
            });
        });
    }
}
