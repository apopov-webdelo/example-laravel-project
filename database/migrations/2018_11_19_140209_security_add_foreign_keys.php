<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SecurityAddForeignKeys extends Migration
{
    // https://stackoverflow.com/questions/33140860/laravel-5-1-unknown-database-type-enum-requested
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('security', function (Blueprint $table) {
                $table->dropIndex('idx_security_user_id');

                $table->unsignedInteger('user_id')->change();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('security', function (Blueprint $table) {
                $table->dropForeign('security_user_id_foreign');
            });

            Schema::table('security', function (Blueprint $table) {
                $table->unsignedInteger('user_id')->change();
                $table->index('user_id', 'idx_security_user_id');
            });
        });
    }
}
