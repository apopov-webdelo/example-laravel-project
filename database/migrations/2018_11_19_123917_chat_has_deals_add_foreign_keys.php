<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChatHasDealsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('chat_has_deals', function (Blueprint $table) {
                $table->dropIndex('fk_chat_has_deals_deals1_idx');
                $table->dropIndex('fk_chat_has_deals_chat1_idx');

                $table->foreign('chat_id')->references('id')->on('chat')->onDelete('cascade');
                $table->foreign('deal_id')->references('id')->on('deals')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('chat_has_deals', function (Blueprint $table) {
                $table->dropForeign('chat_has_deals_chat_id_foreign');
                $table->dropForeign('chat_has_deals_deal_id_foreign');

                $table->index('deal_id', 'fk_chat_has_deals_deals1_idx');
                $table->index('chat_id', 'fk_chat_has_deals_chat1_idx');
            });
        });
    }
}
