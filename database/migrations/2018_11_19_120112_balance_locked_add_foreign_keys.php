<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BalanceLockedAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('balance_locked', function (Blueprint $table) {
                $table->dropIndex('fk_balance_id_to_balance_idx');

                $table->unsignedInteger('balance_id')->change();
                $table->foreign('balance_id')->references('id')->on('balance')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('balance_locked', function (Blueprint $table) {
                $table->dropForeign('balance_locked_balance_id_foreign');
            });

            Schema::table('balance_locked', function (Blueprint $table) {
                $table->integer('balance_id')->change();
                $table->index('balance_id', 'fk_balance_id_to_balance_idx');
            });
        });
    }
}
