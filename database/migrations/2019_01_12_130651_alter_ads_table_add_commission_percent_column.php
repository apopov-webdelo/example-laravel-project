<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class AlterAdsTableAddCommissionPercentColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('ads', function (Blueprint $table) {
                $table->integer('commission_percent')->nullable();
            });
            foreach (\App\Models\Ad\Ad::all() as $ad) {
                $ad->commission_percent = $ad->author->getBalance($ad->cryptoCurrency)->commission;
                $ad->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->dropColumn('commission_percent');
        });
    }
}
