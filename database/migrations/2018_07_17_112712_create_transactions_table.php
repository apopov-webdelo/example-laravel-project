<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transactions');

        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id');
            $table->float('amount');
            $table->enum('type', ['deposit', 'withdraw']);
            $table->float('balance_amount');
            $table->integer('remitter_balance_id');
            $table->integer('receiver_balance_id');
            $table->enum('reason', ['deal', 'transfer', 'blockchain', 'exchange', 'code', 'commission']);
            $table->string('description');
            $table->timestamps();

            // create indexes
            $table->index('remitter_balance_id', 'remitter_balance');
            $table->index('reason', 'reason');

            // foreign keys
            $table->foreign('remitter_balance_id')
                  ->references('id')->on('balance')
                  ->onDelete('cascade');

            $table->foreign('receiver_balance_id')
                  ->references('id')->on('balance')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
