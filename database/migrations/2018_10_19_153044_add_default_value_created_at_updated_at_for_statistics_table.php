<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddDefaultValueCreatedAtUpdatedAtForStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('users_statistics', function (Blueprint $table) {
                // Drop columns with default values NULL
                $table->dropTimestamps();
            });
            Schema::table('users_statistics', function (Blueprint $table) {
                // Add columns with default values CURRENT_TIMESTAMP
                $table->addColumn('timestamp', 'created_at')->after('id')->useCurrent();
                $table->addColumn('timestamp', 'updated_at')->after('created_at')->useCurrent();
            });
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('users_statistics', function (Blueprint $table) {
                // Drop columns with default values NULL
                $table->dropTimestamps();
            });
            Schema::table('users_statistics', function (Blueprint $table) {
                $table->addColumn('timestamp', 'created_at')->after('id')->nullable();
                $table->addColumn('timestamp', 'updated_at')->after('created_at')->nullable();
            });
        });
    }
}
