<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdAutoincrementToDealsHasStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals_has_statuses', function (Blueprint $table) {
            $table->dropPrimary(['deal_id', 'status_id']);
        });
        Schema::table('deals_has_statuses', function (Blueprint $table) {
            $table->increments('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals_has_statuses', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->primary('deal_id');
            $table->primary('status_id');
        });
    }
}
