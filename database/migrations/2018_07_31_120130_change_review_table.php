<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class ChangeReviewTable extends Migration
{
    /**
     * ChangeReviewTable constructor.
     */
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            DB::beginTransaction();
            try {

                $table->renameColumn('msg', 'message');
                $table->dropColumn(['from_user_id', 'to_user_id']);
                $table->integer('author_id')->after('id');
                $table->integer('recipient_id')->after('author_id');

            } catch (Exception $exception) {
                DB::rollBack();
                throw $exception;
            }
            DB::commit();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            DB::beginTransaction();
            try {

                $table->renameColumn('message', 'msg');
                $table->dropColumn(['author_id', 'recipient_id']);
                $table->integer('from_user_id')->after('id');
                $table->integer('to_user_id')->after('from_user_id');

            } catch (Exception $exception) {
                DB::rollBack();
                throw $exception;
            }
            DB::commit();
        });
    }
}
