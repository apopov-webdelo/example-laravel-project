<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UserStatisticsAddColumnAverageFinishDealTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('users_statistics', function (Blueprint $table) {
                $table->integer('average_finish_deal_time')
                      ->default(0)
                      ->comment('Time in seconds');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('users_statistics', function (Blueprint $table) {
                $table->dropColumn('average_finish_deal_time');
            });
        });
    }
}
