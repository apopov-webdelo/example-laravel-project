<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePayinTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('payin_transactions', function (Blueprint $table) {
                $table->increments('id');
                $table->string('transaction_id')->unique()->comment('Transaction ID in blockchain');
                $table->enum('status', ['pending', 'processed', 'failed'])->index();
                $table->integer('user_id')->unsigned()->nullable();
                $table->string('crypto_code');
                $table->bigInteger('amount');
                $table->timestamps();
            });

            Schema::table('payin_transactions', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('payin_transactions');
        });
    }
}
