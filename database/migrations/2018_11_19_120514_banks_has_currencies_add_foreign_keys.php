<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BanksHasCurrenciesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('banks_has_currencies', function (Blueprint $table) {
                $table->unsignedInteger('bank_id')->change();
                $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');

                $table->unsignedInteger('currency_id')->change();
                $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade');

                $table->unsignedInteger('country_id')->change();
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

                $table->unsignedInteger('author_id')->change();
                $table->foreign('author_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('banks_has_currencies', function (Blueprint $table) {
                $table->dropForeign('banks_has_currencies_author_id_foreign');
                $table->dropForeign('banks_has_currencies_bank_id_foreign');
                $table->dropForeign('banks_has_currencies_country_id_foreign');
                $table->dropForeign('banks_has_currencies_currency_id_foreign');
            });

            Schema::table('banks_has_currencies', function (Blueprint $table) {
                $table->integer('bank_id')->change();
                $table->integer('currency_id')->change();
                $table->integer('country_id')->change();
                $table->integer('author_id')->change();
            });
        });
    }
}
