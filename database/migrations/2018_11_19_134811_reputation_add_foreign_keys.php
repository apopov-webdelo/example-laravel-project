<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReputationAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('reputation', function (Blueprint $table) {
                $table->dropIndex('idx_reputation_user_id');

                $table->unsignedInteger('user_id')->change();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('reputation', function (Blueprint $table) {
                $table->dropForeign('reputation_user_id_foreign');
            });

            Schema::table('reputation', function (Blueprint $table) {
                $table->unsignedInteger('user_id')->change();
                $table->index('user_id', 'idx_reputation_user_id');
            });
        });
    }
}
