<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class ChangeReviewMessageColumnTypeToText extends Migration
{
    /**
     * ChangeReviewTable constructor.
     */
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::transaction(function (){
            Schema::table('reviews', function (Blueprint $table) {
                $table->text('message')->nullable()->change();;
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function (){
            Schema::table('reviews', function (Blueprint $table) {
                $table->string('message')->nullable()->change();;
            });
        });
    }
}
