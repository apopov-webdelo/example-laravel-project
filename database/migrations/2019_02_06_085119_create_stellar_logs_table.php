<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStellarLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('stellar_logs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('type', 4);
                $table->string('method', 10);
                $table->string('route');
                $table->smallInteger('response_code');
                $table->text('request');
                $table->text('response');
                $table->dateTime('created_at');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('stellar_logs');
        });
    }
}
