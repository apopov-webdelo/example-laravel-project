<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTelegramFieldsToSecurityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('security', function (Blueprint $table) {
            $table->tinyInteger('telegram_id_confirmed')->default(0);
            $table->string('telegram_id_confirm_token', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('security', function (Blueprint $table) {
            $table->dropColumn('telegram_id_confirmed');
            $table->dropColumn('telegram_id_confirm_token');
        });
    }
}
