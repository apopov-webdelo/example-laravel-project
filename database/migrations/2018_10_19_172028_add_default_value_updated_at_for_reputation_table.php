<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueUpdatedAtForReputationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('reputation', function (Blueprint $table) {
                // Drop columns with default values NULL
                $table->dropColumn('updated_at');
            });
            Schema::table('reputation', function (Blueprint $table) {
                // Add columns with default values CURRENT_TIMESTAMP
                $table->addColumn('timestamp', 'updated_at')->after('created_at')->useCurrent();
            });
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('reputation', function (Blueprint $table) {
                // Drop columns with default values NULL
                $table->dropColumn('updated_at');
            });
            Schema::table('reputation', function (Blueprint $table) {
                // Add columns with default values NULL
                $table->addColumn('timestamp', 'updated_at')->after('created_at')->nullable();
            });
        });
    }
}
