<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddDefaultPartnershipProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            DB::table('partnership_programs_statuses')->insert([
                ['id' => 1, 'title' => 'Active', 'color' => 'success'],
                ['id' => 2, 'title' => 'Blocked', 'color' => 'danger']
            ]);
            DB::table('partnership_programs')->insert([
                [
                    'id'        => 1,
                    'title'     => 'Base partnership program',
                    'status_id' => 1,
                    'driver'    => config('app.partnership.default_driver')
                ]
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            DB::table('partnership_programs_statuses')->truncate();
            DB::table('partnership_programs')->truncate();
        });
    }
}
