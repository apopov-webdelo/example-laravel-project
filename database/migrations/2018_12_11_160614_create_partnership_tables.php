<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePartnershipTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('partnership_programs_statuses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('color');
            });
            Schema::create('partnership_programs', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->unsignedInteger('status_id');
                $table->string('driver', 1000);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();

                $table->foreign('status_id')->references('id')->on('partnership_programs_statuses');
            });
            Schema::create('partners', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('partnership_program_id');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();

                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('partnership_program_id')->references('id')->on('partnership_programs');
            });
            Schema::create('referrals', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('partner_id');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();

                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('partner_id')->references('id')->on('partners');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::dropIfExists('partnership_programs_statuses');
            Schema::dropIfExists('partnership_programs');
            Schema::dropIfExists('partners');
            Schema::dropIfExists('referrals');
        });
    }
}
