<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterDealsTableChangeAmountTypeToBigint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('deals', function (Blueprint $table) {
                  $table->bigInteger('crypto_amount')->default(0)->change();
                  $table->bigInteger('fiat_amount')->default(0)->change();
                  $table->bigInteger('commission')->default(0)->change();
                  $table->bigInteger('price')->default(0)->change();
            });

            Schema::table('ads', function (Blueprint $table) {
                $table->bigInteger('price')->default(0)->change();
                $table->bigInteger('turnover')->default(0)->change();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('deals', function (Blueprint $table) {
                $table->decimal('crypto_amount', 21)->default(0)->change();
                $table->decimal('fiat_amount', 21)->default(0)->change();
                $table->decimal('commission', 21)->default(0)->change();
                $table->decimal('price', 21)->default(0)->change();
            });

            Schema::table('ads', function (Blueprint $table) {
                $table->decimal('price', 21)->default(0)->change();
                $table->decimal('turnover', 21)->default(0)->change();
            });
        });
    }
}
