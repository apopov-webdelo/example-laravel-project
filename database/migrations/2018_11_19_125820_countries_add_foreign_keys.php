<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CountriesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('countries', function (Blueprint $table) {
                $table->unsignedInteger('currency_id')->change();
                $table->foreign('currency_id')->references('id')->on('currencies');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('countries', function (Blueprint $table) {
                $table->dropForeign('countries_currency_id_foreign');
            });

            Schema::table('countries', function (Blueprint $table) {
                $table->integer('currency_id')->change();
            });
        });
    }
}
