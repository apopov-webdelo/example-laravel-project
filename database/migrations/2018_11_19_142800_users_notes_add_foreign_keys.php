<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersNotesAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('users_notes', function (Blueprint $table) {
                $table->unsignedInteger('author_id')->change();
                $table->unsignedInteger('recipient_id')->change();

                $table->foreign('author_id')->references('id')->on('users');
                $table->foreign('recipient_id')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::table('users_notes', function (Blueprint $table) {
                $table->dropForeign('users_notes_author_id_foreign');
                $table->dropForeign('users_notes_recipient_id_foreign');

                $table->dropIndex('users_notes_author_id_foreign');
                $table->dropIndex('users_notes_recipient_id_foreign');
            });

            Schema::table('users_notes', function (Blueprint $table) {
                $table->unsignedInteger('author_id')->change();
                $table->unsignedInteger('recipient_id')->change();
            });
        });
    }
}
