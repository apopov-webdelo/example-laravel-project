<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesSeeder extends Seeder
{
    /**
     * @var string
     */
    private $table = 'countries';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ( DB::table($this->table)->where('id', 1)->doesntExist() ) {
            DB::table($this->table)->insert([ 'id' => 1, 'title' => 'Россия', 'iso' => '134343', 'alpha2' => 'RU', 'alpha3' => 'RUS', 'created_at' => now() ]);
        }
    }
}
