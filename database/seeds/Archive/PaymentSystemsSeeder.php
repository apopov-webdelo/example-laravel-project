<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentSystemsSeeder extends Seeder
{
    /**
     * @var string $table
     */
    private $table = 'payment_systems';

    /**
     * @var int $rubbleId
     */
    private $rubbleId = 1;

    /**
     * @var int $authorId
     */
    private $authorId = 1;

    /**
     * @var int $russiaId
     */
    private $russiaId = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentSystems = collect([
            [ 'id' => 1, 'title' => 'QIWI', 'created_at' => now() ],
            [ 'id' => 2, 'title' => 'Яндекс.Деньги', 'created_at' => now() ],
            [ 'id' => 3, 'title' => 'Банки', 'created_at' => now() ],
            [ 'id' => 4, 'title' => 'WebMoney', 'created_at' => now() ],
            [ 'id' => 5, 'title' => 'Золотая корона', 'created_at' => now() ],
        ]);

        foreach ($paymentSystems as $paymentSystem) {
            $currency = [
                'payment_system_id' => $paymentSystem['id'],
                'currency_id'       => $this->rubbleId,
                'country_id'        => $this->russiaId,
                'created_at'        => now(),
                'author_id'         => $this->authorId
            ];
            if ( DB::table($this->table)->where('id', $paymentSystem['id'])->doesntExist() ) {
                DB::table($this->table)->insert($paymentSystem);
            }
            if ( 
                DB::table($this->table . '_has_currencies')
                    ->where('payment_system_id', $paymentSystem['id'])
                    ->where('currency_id', $this->rubbleId)
                    ->where('country_id', $this->russiaId)
                    ->doesntExist()
            ) {
                DB::table($this->table . '_has_currencies')->insert($currency);
            }
        }
    }
}
