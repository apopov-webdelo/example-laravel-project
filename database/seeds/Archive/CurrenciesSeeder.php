<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesSeeder extends Seeder
{
    private $table = 'currencies';


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = collect([
            collect([ 'id' => 1, 'title' => 'Рубль', 'code' => 'RUB', 'created_at' => now() ]),
        ]);

        foreach ($currencies as $currency) {
            if ( DB::table($this->table)->where('id', $currency['id'])->doesntExist() ) {
                DB::table($this->table)->insert($currency);
            }
        }
    }
}
