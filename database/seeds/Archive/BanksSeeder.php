<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BanksSeeder extends Seeder
{
    /**
     * @var string $table
     */
    private $table = 'banks';

    /**
     * @var int $roboUserId
     */
    private $roboUserId = 1;

    /**
     * @var int $rubbleId
     */
    private $rubbleId = 1;

    /**
     * @var int $russiaId
     */
    private $russiaId = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = collect([
            [ 'id' => 1, 'title'  => 'Альфа-Банк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 2, 'title'  => 'СберБанк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 3, 'title'  => 'Тинькофф Банк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 4, 'title'  => 'ПромСвязьБанк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 5, 'title'  => 'ВТБ', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 6, 'title'  => 'ГазпромБанк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 7, 'title'  => 'РоссельхозБанк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 8, 'title'  => 'Московский Кредитный Банк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 9, 'title'  => 'Открытие Банк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 10, 'title' => 'РайффайзенБанк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 11, 'title' => 'БинБанк', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 12, 'title' => 'Русский Стандарт', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 13, 'title' => 'Другие банки РФ', 'created_at' => now(), 'author_id' => $this->roboUserId ],
            [ 'id' => 14, 'title' => 'c2c', 'created_at' => now(), 'author_id' => $this->roboUserId ],
        ]);

        foreach ($banks as $bank) {
            $currency = [
                'bank_id'     => $bank['id'],
                'currency_id' => $this->rubbleId,
                'country_id'  => $this->russiaId,
                'created_at'  => now(),
                'author_id'   => $this->roboUserId
            ];
            if (DB::table($this->table)->where('id', $bank['id'])->doesntExist()) {
                DB::table($this->table)->insert($bank);
            }

            if (DB::table($this->table.'_has_currencies')
                    ->where('bank_id', $bank['id'])
                    ->where('currency_id', $this->rubbleId)
                    ->where('country_id', $this->russiaId)
                    ->doesntExist()
            ) {
                DB::table($this->table.'_has_currencies')->insert($currency);
            }
        }
    }
}
