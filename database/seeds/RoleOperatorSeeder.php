<?php

use Illuminate\Database\Seeder;
use \App\Models\Role\RoleConstants;

class RoleOperatorSeeder extends Seeder
{
    /**
     * @var string
     */
    private $table = 'roles';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->updateOrInsert(
            ['id' => RoleConstants::OPERATOR],
            [
                'id' => RoleConstants::OPERATOR,
                'name' => 'Operator',
                'guard_name' => 'api',
                'created_at' => now()
            ]
        );
    }
}
