<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * @var string $table
     */
    private $table = 'roles';

    /**
     * @var int $roboUserId
     */
    private $roboUserId = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = collect([
            [ 'id' => 1, 'name'  => 'Client', 'created_at' => now(), 'guard_name' => 'api' ],
            [ 'id' => 2, 'name'  => 'Admin', 'created_at' => now(), 'guard_name' => 'api' ]
        ]);

        foreach ($roles as $role) {
            if (DB::table($this->table)->where('id', $role['id'])->doesntExist()) {
                DB::table($this->table)->insert($role);
            }
        }
    }
}
