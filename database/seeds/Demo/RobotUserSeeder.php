<?php

use App\Services\User\RegistrationService;
use Illuminate\Database\Seeder;

class RobotUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt(str_random(60));

        app(RegistrationService::class)
            ->store([
                'login'                 => 'robot',
                'email'                 => 'robot@risex.net',
                'password'              => $password,
                'password_confirmation' => $password,
            ]);

        $robot = \App\Facades\Robot::user();

        $cryptos = \App\Models\Directory\CryptoCurrency::all();
        $cryptos->each(function ($crypto) use ($robot) {
            $balance = $robot->getBalance($crypto);
            $balance->amount = \App\Models\Directory\CryptoCurrencyConstants::ONE_BTC * 100000000;
            $balance->save();
        });
    }
}
