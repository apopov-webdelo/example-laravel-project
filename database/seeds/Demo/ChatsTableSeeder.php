<?php

use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class ChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {
            $users = User::all('id')->random(2);
            $user = $users->first();
            $recipient = $users->last();

            $chat = factory(Chat::class)->create([
                'author_id' => $user,
            ]);

            $message = factory(Message::class)->create([
                'chat_id'   => $chat,
                'author_id' => $user,
            ]);

            $chat->recipients()->attach($recipient);
        }
    }
}
