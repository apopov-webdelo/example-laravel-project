<?php

use Illuminate\Database\Seeder;

/**
 * Seed demo data to database
 *
 * Class DemoDatabaseSeeder
 */
class DemoDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TruncateTablesSeeder::class);
        $this->call(CleanUpImagesTable::class);
        $this->call(RobotUserSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(DealsTableSeeder::class);
        $this->call(ChatsTableSeeder::class);
        $this->call(DealChatsTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
    }
}
