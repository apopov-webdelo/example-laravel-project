<?php

use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class DealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            $deal = \App\Makers\DealMaker::init([
                'author_id' => User::all('id')->random(1)->first(),
                'ad_id'     => Ad::all('id')->random(1)->first(),
            ])->create();

            $deal->statuses()->attach(array_random([
                DealStatusConstants::VERIFIED,
                DealStatusConstants::PAID,
                DealStatusConstants::IN_DISPUTE,
                DealStatusConstants::CANCELED,
                DealStatusConstants::FINISHED,
            ]));
        }
    }
}
