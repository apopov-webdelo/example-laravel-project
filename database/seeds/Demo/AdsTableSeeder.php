<?php

use App\Models\Ad\Ad;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            \App\Makers\AdMaker::init([
                'author_id' => User::all('id')->random(1)->first()
            ])->create();
        }
    }
}
