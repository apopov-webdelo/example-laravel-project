<?php

use Illuminate\Database\Seeder;

class CleanUpImagesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::transaction(function () {
            \App\Models\Image\Image::query()->where('imageable_type', '!=', \App\Models\Directory\Bank::class)
                                   ->chunk(30, function ($image) {
                                       $image->each->delete();
                                   });
        });
    }
}
