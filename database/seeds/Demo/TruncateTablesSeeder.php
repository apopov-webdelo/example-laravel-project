<?php

use Illuminate\Database\Seeder;

/**
 * Truncate users and all related tables
 *
 * Class TruncateTablesSeeder
 */
class TruncateTablesSeeder extends Seeder
{
    /**
     * List of tables that are saved
     * @var array
     */
    protected $saveTables = [
        'admins',
        'admins_security',
        'ads_statuses',
        'banks',
        'banks_has_currencies',
        'countries',
        'crypto_currencies',
        'currencies',
        'deals_statuses',
        'languages',
        'migrations',
        'oauth_clients',
        'oauth_personal_access_clients',
        'partnership_programs',
        'partnership_programs_statuses',
        'payment_systems',
        'payment_systems_has_currencies',
        'permissions',
        'roles',
        'role_has_permissions',
        'transactions_reasons',
        'images',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tables as $table) {
            if (!in_array($table, $this->saveTables)) {
                DB::table($table)->truncate();
            }
        }
    }
}
