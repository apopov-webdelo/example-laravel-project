<?php

use App\Services\User\RegistrationService;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        static $password;

        $faker = app(Faker::class);

        for ($i = 0; $i < 30; $i++) {
            app(RegistrationService::class)->store([
                'login' => 'seed' . date('dmY_') . $i,
                'email' => 'seedmail' . date('dmY_') . $i . '@risex.net',
                'phone' => $faker->unique()->phoneNumber,
                'password' => $password ?: $password = bcrypt('secret'),
                'api_token' => str_random(60),
                'remember_token' => str_random(10),
                'user_locked_at' => null,
                'user_locked_to' => null,
                'trade_locked_at' => null,
                'trade_locked_to' => null,
                'created_at' => now(),
                'updated_at' => null,
                'appeal' => $faker->text,
                'last_seen' => $faker->dateTimeThisDecade,
                'role_id' => \App\Models\Role\RoleConstants::CLIENT,
            ]);
        }
    }
}
