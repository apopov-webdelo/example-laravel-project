<?php

use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Deal\Deal;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class DealChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            /* @var Deal $deal */
            $deal = Deal::all()->random(1)->first();

            $storeChatService = app(\App\Services\Chat\StoreChatService::class);

            $members = $deal->getDealMembers()->unique();

            $chat = $storeChatService->store($deal);

            foreach ($members as $user) {
                factory(Message::class)->create([
                    'chat_id'   => $chat,
                    'author_id' => $user,
                ]);
            }
        }
    }
}
