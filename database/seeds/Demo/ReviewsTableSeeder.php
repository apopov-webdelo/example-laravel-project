<?php

use App\Models\Deal\Deal;
use App\Models\User\Review;
use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {
            $deal = Deal::all()->random(1)->first();
            $author = $deal->author_id;
            $recepient = $deal->ad->author_id;

            \App\Makers\User\ReviewMaker::init([
                'author_id'    => $author,
                'recipient_id' => $recepient,
                'deal_id'      => $deal,
            ])->create();
        }
    }
}
