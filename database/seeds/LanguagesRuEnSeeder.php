<?php

use Illuminate\Database\Seeder;

/**
 * Class LanguagesRuEnSeeder
 */
class LanguagesRuEnSeeder extends Seeder
{
    /**
     * @var string
     */
    private $table = 'languages';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = collect([
            ['id' => 1, 'title' => 'Русский', 'created_at' => now()],
            ['id' => 2, 'title' => 'English', 'created_at' => now()]
        ]);

        foreach ($languages as $language) {
            DB::table($this->table)->updateOrInsert(['id'=>$language['id']], $language);
        }
    }
}
