<?php

use Illuminate\Database\Seeder;

/**
 * Seed demo data to database
 *
 * Class DemoDatabaseSeeder
 */
class FreshDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TruncateTablesSeeder::class);
        $this->call(CleanUpImagesTable::class);
        $this->call(RobotUserSeeder::class);
    }
}
