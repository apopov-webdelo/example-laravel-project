<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 16:21
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'transaction' => [

        'commission' => [
            'default' => 'Комиссия по пользователю №:user_id',
            'deal'    => 'Комиссия по сделке №:deal_id',
            'payout'  => 'Комиссия за вывод средств по заявке №:payout_id',
        ],

        'deal' => [
            'default' => 'Окончательный трансфер по сделке №:deal_id',
        ],

        'deposit' => 'Пополнение баланса через блокчейн',

        'pay_in' => 'Пополнение баланса через блокчейн',

        'withdraw' => 'Вывод средств через блокчейн',

        'pay_out' => 'Вывод средств через блокчейн на кошелёк :wallet',

        'cashback' => 'Начисление за кешбек №:cashback_id за сделки :deal_list',

        'partnership' => 'Начисление по партнерской программе',
    ],

    'balance' => [
        'check' => [
            'not_enough' => 'Недостаточно криптовалюты для этой операции',
        ],
        'find' => [
            'not_exists' => 'Баланс не существует для указанной криптовалюты'
        ],
    ],

    'payout' => [
        'min_amount_error' => 'Сумма вывода (:amount) меньше минимального лимита (:min_amount)!',
    ],
];
