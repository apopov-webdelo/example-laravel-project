<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 16:21
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'error' => [
        'shortage_balance'      => 'У продавца недостаточно средств для осуществления сделки!',
        'not_found_closed_deal' => 'Не найдено ни одной закрытой сделки.',
        'cant_cancel'           => 'Сейчас нельзя отменить сделку.',
    ],
];
