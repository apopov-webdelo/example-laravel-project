<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Security logs constants
    |--------------------------------------------------------------------------
    */

    'password_changed' => 'изменен пароль',
    'password_change_fail_2fa' => 'ошибка проверки 2fa',

    'email_confirmation_start' => 'начата процедура подтверждения email',

    'user_locked'       => 'заблокирован',
    'user_block_trade'  => 'заблокирована торговля',
    'user_unlocked'     => 'разблокирован',
    'user_unlock_trade' => 'разблокирована торговля',

    'password_is_reset' => 'пароль сброшен',

    '2fa_connect'    => '2FA подключено',
    '2fa_disconnect' => '2FA отключено',
    '2fa_enable'     => '2FA активно',
    '2fa_disable'    => '2FA неактивно',
    '2fa_deactivate' => '2FA деактивировано',

    'phone_add'             => 'телефон добавлен',
    'phone_add_confirm'     => 'телефон подтвержден',
    'phone_delete'          => 'телефон помечен на удаление',
    'phone_delete_complete' => 'телефон удален',

    'telegramm_add'         => 'telegram добавлен',
    'telegramm_add_confirm' => 'telegram подтвержден',

    'email_change'         => 'email изменен',
    'email_confirm_change' => 'подтверждение нового email',
    'email_confirm'        => 'email подтвержден',

    'phone_change'         => 'телефон изменен',
    'phone_confirm_change' => 'подтверждение нового телефона',
    'phone_confirm'        => 'телефон подтвержден',
];
