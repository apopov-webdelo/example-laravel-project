<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute должен быть выбран.',
    'active_url'           => ':attribute невалидный URL.',
    'after'                => ':attribute должен быть датой после :date.',
    'after_or_equal'       => ':attribute должен быть датой после либо равен дате :date.',
    'alpha'                => ':attribute должен содержать только символы.',
    'alpha_dash'           => ':attribute должен содержать только символы, цифры и тире.',
    'alpha_num'            => ':attribute должен содержать только символы и цифры.',
    'array'                => ':attribute должен быть массивом.',
    'before'               => ':attribute должен быть датой до :date.',
    'before_or_equal'      => ':attribute должен быть датой до или равен дате :date.',
    'between'              => [
        'numeric' => ':attribute должна быть между :min и :max.',
        'file'    => ':attribute должна быть между :min и :max килобайт.',
        'string'  => ':attribute должна быть между :min и :max символов.',
        'array'   => ':attribute должна быть между :min и :max элементов.',
    ],
    'boolean'              => ':attribute поле должно быть либо false, либо true.',
    'confirmed'            => ':attribute подтверждение не совпадает',
    'date'                 => ':attribute невалидная дата.',
    'date_format'          => ':attribute не совпадает с форматом :format.',
    'different'            => ':attribute и :other должны отличаться.',
    'digits'               => ':attribute должен быть :digits числами.',
    'digits_between'       => ':attribute должен быть между :min и :max числами.',
    'dimensions'           => ':attribute невалидный размер изображения.',
    'distinct'             => ':attribute поле хранит дубликат значения',
    'email'                => ':attribute должен быть валидным e-mail адресом.',
    'exists'               => 'Выбранное значение :attribute не верно',
    'file'                 => ':attribute должно быть файлом.',
    'filled'               => ':attribute должно хранить значение.',
    'gt'                   => [
        'numeric' => ':attribute должно быть больше чем :value.',
        'file'    => ':attribute должно быть больше чем :value килобайт.',
        'string'  => ':attribute должно быть больше чем :value символов.',
        'array'   => ':attribute должно быть больше чем :value элементов.',
    ],
    'gte'                  => [
        'numeric' => ':attribute должна быть больше либо равна :value.',
        'file'    => ':attribute должна быть больше либо равна :value килобайт.',
        'string'  => ':attribute должна быть больше либо равна :value символов.',
        'array'   => ':attribute должно хранить :value элементов или больше.',
    ],
    'image'                => ':attribute должно быть изображением.',
    'in'                   => 'Выбранное значение :attribute невалидно.',
    'in_array'             => ':attribute поле отсутствует в :other.',
    'integer'              => ':attribute должно быть integer.',
    'ip'                   => ':attribute должно быть валидным IP адресом.',
    'ipv4'                 => ':attribute должно быть валидным IPv4 адресом.',
    'ipv6'                 => ':attribute должно быть валидным IPv6 адресом.',
    'json'                 => ':attribute должно быть валидной JSON строкой.',
    'lt'                   => [
        'numeric' => ':attribute должно быть меньше чем :value.',
        'file'    => ':attribute должно быть меньше чем :value килобайт.',
        'string'  => ':attribute должно быть меньше чем :value символов.',
        'array'   => ':attribute должно содержать меньше чем :value элементов.',
    ],
    'lte'                  => [
        'numeric' => ':attribute должно быть меньше либо равно :value.',
        'file'    => ':attribute должно быть меньше либо равно :value килобайт.',
        'string'  => ':attribute должно быть меньше либо равно :value символов.',
        'array'   => ':attribute должно содержать меньше либо равно :value элементов.',
    ],
    'max'                  => [
        'numeric' => ':attribute не может быть больше чем :max.',
        'file'    => ':attribute не может быть больше чем :max килобайт.',
        'string'  => ':attribute не может быть больше чем :max символов.',
        'array'   => ':attribute не может содержать больше :max элементов.',
    ],
    'mimes'                => ':attribute должен быть типа: :values.',
    'mimetypes'            => ':attribute должен быть типа: :values.',
    'min'                  => [
        'numeric' => ':attribute должно быть как минимум :min.',
        'file'    => ':attribute должно быть как минимум :min килобайт.',
        'string'  => ':attribute должно быть как минимум :min символов.',
        'array'   => ':attribute должно содержать как минимум :min элементов.',
    ],
    'not_in'               => 'Выбранный :attribute невалиден.',
    'not_regex'            => ':attribute формат не валиден.',
    'numeric'              => ':attribute должно быть числом.',
    'present'              => ':attribute должно фигурировать в данных.',
    'regex'                => ':attribute формат не верен.',
    'required'             => ':attribute поле обязательное.',
    'required_if'          => ':attribute поле обязательное когда :other имеет значение :value.',
    'required_unless'      => ':attribute поле обязательно когда значения :other не :values.',
    'required_with'        => ':attribute поле обязательно когда :values присутствуют в данных.',
    'required_with_all'    => ':attribute поле обязательно когда :values присутствуют в данных.',
    'required_without'     => ':attribute поле обязательно когда значения :values не присутствуют.',
    'required_without_all' => ':attribute поле обязательно когда значение :values не присутствуют.',
    'same'                 => ':attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => ':attribute должно быть :size.',
        'file'    => ':attribute должно быть :size килобайт.',
        'string'  => ':attribute должно быть :size символов.',
        'array'   => ':attribute должно содержать :size элементов.',
    ],
    'string'               => ':attribute должно быть строкой.',
    'timezone'             => ':attribute должно быть валидной таймзоной.',
    'unique'               => ':attribute должно быть уникальным.',
    'uploaded'             => ':attribute ошибка загрузки.',
    'url'                  => ':attribute формат невалидный.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */


    'auth' => [
        'wrong_login_password' => 'Не верный логин или пароль',
        'banned'               => 'Ваш аккаунт заблокирован',
        'google2fa_required'   => 'Требуется код двухфакторной авторизации',
    ],

    'custom_errors' => [
        'token'                 => [
            'required' => 'Токен обязательное поле',
        ],
        'login'                 => [
            'required' => 'Логин обязательное поле',
            'string'   => 'Логин должен быть строкой',
            'max'      => 'Логин не должен быть больше 255 символов',
            'unique'   => 'Логин должен быть уникальным',
        ],
        'email'                 => [
            'required' => 'E-mail обязательное поле',
            'string'   => 'E-mail должен быть строкой',
            'email'    => 'E-mail должен быть валидным e-mail',
            'max'      => 'E-mail не должен быть больше 255 символов',
            'unique'   => 'E-mail должен быть уникальным',
        ],
        'password'              => [
            'required'  => 'Пароль обязательное поле',
            'string'    => 'Пароль должен быть строкой',
            'confirmed' => 'Пароль должен быть подтвержденным',
            'min'       => 'Пароль должен быть не меньше 7 символов',
            'max'       => 'Пароль не должен быть больше 45 символов',
            'regex'     => 'Пароль должен содержать минимум одну заглавную букву и одну цифру',
        ],
        'password_confirmation' => [
            'required' => 'Подтверждение пароля обязательное поле',
            'string'   => 'Подтверждение пароля должен быть строкой',
            'same'     => 'Подтверждение пароля должно совпадать с полем пароль',
        ],
    ],

    'attributes' => [],

    'tor_denied' => 'Тор браузер запрещен',


    'phone_already_exsits'    => 'Телефон уже существует',
    'phone_doesnt_exsits'     => 'Нет записи о телефоне',
    'telegram_already_exsits' => 'Телеграм уже существует',

    'email_free_required' => 'Выбранный e-mail уже занят',
    'phone_free_required' => 'Выбранный номер телефона уже занят',

    'ad' => [
        'unique_ad'          => 'Такое объявление уже существует',
        'limit_less_balance' => 'Максимальный лимит выше текущего баланса',
        'price_unknown'      => 'Не задана цена',
    ],

    'google2fa' => [
        'connected' => 'Google 2fa уже включена',
    ],

    'deal' => [
        'ad_is_protected' => 'Нужен ваш публичный ключ для этого объявления.',
        'ad_is_not_active' => 'Объявление не активно сейчас',
        'author_is_ad_author' => 'Автор объявления не может быть автором сделки.',
        'balance_is_to_small' => 'Баланс продавца слишком маленький для сделки.',
        'email_required' => 'Ожидается подтвержденный e-mail.',
        'phone_required' => 'Ожидается подтвержденный номер телефона.',
        'trust_required' => 'Между продавцом и покупателем ожидается доверие.',
        'blacklist_detected' => 'Вы в черном списке пользователя.',
        'blacklist_detected_owner' => 'Пользователь в вашем черном списке.',
        'turnover_required' => 'Ваш оборот слишком маленький для этой сделки.',
        'invalid_amount_period' => 'Неверная сумма покупки.',
        'invalid_price' => 'Цена уже была изменена.',
        'invalid_dealer_balance' => 'Неверный баланс продавца.',
        'invalid_reputation' => 'Ваша репутация меньше требуемой.',
        'min_deal_finished_count' => 'Вы завершили недостаточно сделок, чтобы оформить эту',
        'is_not_deadline_time' => 'Время сделки ещё не истекло',
        'invalid_buyer_deal_cancellation_max_percent' => 'У покупателя слишком высокий процент отмены сделок.',
        'crypto_amount_less_min_limit' => 'Крипто сумма меньше минимального лимита. Мин. лимит :limit :code',
    ],

    'user' => [
        'email_diff' => 'E-mail должен быть другим.',
        'phone_diff' => 'Номер телефона должен быть другим.',

        'google2fa_doesnt_match' => 'Google 2fa код не совпадает.',
        'password_doesnt_match'  => 'Пароль не совпадает.',

        'email_access_token'       => 'E-mail код доступа не совпадает.',
        'email_confirmation_token' => 'E-mail код подтверждения не совпадает.',

        'phone_access_token'       => 'Код доступа телефона не совпадает.',
        'phone_confirmation_token' => 'Код подтверждения телефона не сопадает.',

        'telegram_confirmation_token' => 'Код подтверждения телеграма не совпадает.',
    ],

    'partnership' => [
        'already_partner' => 'Вы уже зарегистрированы как партнер'
    ]

];
