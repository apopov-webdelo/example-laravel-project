<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен состоять минимум из 6 символов и совпадать с полем подтверждения пароля.',
    'reset'    => 'Ваш пароль был сброшен!',
    'sent'     => 'Мы отправили на ваш e-mail пароль для сброса пароля!',
    'token'    => 'Этот токен для сброса пароля не верен.',
    'user'     => "Мы не можем найти пользователя с таким e-mail",

    'password_updated_admin' => [
        'subject'      => 'Ваш пароль изменен администратором',
        'reason'       => 'Причина: ',
        'new_password' => 'Новый пароль: ',
    ],

    'password_reset_request' => [
        'subject' => 'Уведомление о сбросе пароля',
        'text1'   => 'Вы получили это письмо поскольку запросили сброс пароля для вашего аккаунта.',
        'action'  => 'Сбросить пароль',
        'text2'   => 'Если вы не запрашивали сброс пароля, то дальнейшие действия не требуются.',
    ],

];
