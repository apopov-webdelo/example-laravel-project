<?php

$standardAction = 'Детали';
$standardFooter = 'Спасибо за использование нашего ресурса!';

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    'hello' => 'Здравствуйте!',

    'partnership' => [
        'partner-register' => [
            'subject' => 'Вы стали партнером на сервисе '.config('app.name'),
            'content' => 'Рады приветствовать вас как партнера! Зарабатывайте вместе с нами!',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
        'referral-register' => [
            'subject' => 'Был зарегистрирован ваш реферал на сервисе '.config('app.name'),
            'content' => '
                Ему был присвоен ID=:referral_id. 
                Чтобы заработать больше, приглашайте активных трейдеров.
            ',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
        'partner-award' => [
            'subject' => 'Прибыль за сделку вашего реферала на сервисе '.config('app.name'),
            'content' => 'Вы получаете :commission :commissionType за сделку вашего реферала',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ]
    ],

    'deal' => [
        'placed' => [
            'ad_author' => [
                'subject' => 'Была оформлена сделка',
                'content' => 'Была оформлена сделка по вашему объявлению №:adId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'canceled' => [
            'seller' => [
                'subject' => 'Сделка №:dealId была отменена',
                'content' => 'Была отменена сделка #:dealId для вашего объявления #:adId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'finished' => [
            'buyer' => [
                'subject' => 'Сделка №:dealId была завершена',
                'content' => 'Была завершена сделка №:dealId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'paid' => [
            'seller' => [
                'subject' => 'Сделка №:dealId была оплачена',
                'content' => 'Has been paid deal #:dealId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'disputed' => [
            'participant' => [
                'subject' => 'По сделке №:dealId открыт спор',
                'content' => ':login открыл спор по сделке #:dealId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ]
        ],
    ],

    'chat' => [
        'placed' => [
            'subject' => 'Начат диалог между :recipientString',
            'content' => 'Был открыт диалог между :recipientString',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
        'message-placed' => [
            'recipient' => [
                'subject' => 'Вы получили сообщение от :login',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
    ],

    'user' => [
        'registration-message' => [
            'subject' => 'Вы успешно зарегистрировались на сервисе '.config('app.name'),
            'content' => 'Добро пожаловать на Risex! ',
            'description' => '
                Мы любители криптовалют, энтузиасты! Поэтому мы рады приветствовать вас 
                на портале безопасных сделок с криптовалютами.
            ',
            'action'  => 'Подтвердить свою учетную запись',
            'footer'  => $standardFooter,
        ],

        'email-access' => [
            'subject' => 'Изменение e-mail на сервисе '.config('app.name'),
            'content' => 'Вы начали процесс изменения e-mail.',
            'action'  => 'Продолжить изменение e-mail',
            'footer'  => $standardFooter,
        ],
        'email-confirmation' => [
            'subject' => 'Подтверждение e-mail на сервисе '.config('app.name'),
            'content' => 'Вы запросили подтверждение e-mail.',
            'action'  => 'Подтвердить e-mail',
            'footer'  => $standardFooter,
        ],
        'phone-access' => [
            'content' => 'Чтобы подтвердить изменение номера телефона, пожалуйста, используйте код: :accessToken'
        ],
        'phone-confirmation' => [
            'content' => 'Чтобы подтвердить новый номер телефона, пожалуйста, используйте код: :confirmToken'
        ],
        'telegram-confirmation' => [
            'content' => 'Чтобы подтвердить свой телеграм, пожалуйста, используйте код: :confirmToken'
        ],

        'login-success' => [
            'subject' => 'Успешная авторизация',
            'content' => ':login, успешно авторизовался на сайте '.config('app.name'),
            'action'  => "Время вход :time. Вход из :country, :city. \r\n
                          Если вы не знаете об этом входе, пожалуйста, как можно скорее измените пароль.",
            'footer'  => $standardFooter,
        ],

        'login-error' => [
            'subject' => 'Ошибка авторизации',
            'content' => ':login, произошла ошибка во время авторизации на сайте '.config('app.name'),
            'action'  => "Время вход :time. Вход из :country, :city. \r\n
                          Если вы не знаете об этом входе, пожалуйста, как можно скорее измените пароль.",
            'footer'  => $standardFooter,
        ],

        'review-laced' => [
            'subject' => 'Был оставлен новый отзыв',
            'content' => ':author написал отзыв для вас.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'locked' => [
            'subject' => 'Ваша учетная запись заблокирована на '.config('app.name'),
            'content' => 'Внимание! Администрация заблокировала вашу учетную запись из-за подозрительной активности.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'unlocked' => [
            'subject' => 'Ваша учетная запись разблокирована на '.config('app.name'),
            'content' => 'Подздравляем! Ваша учетная запись была разблокирована. Вы можете возобновить торговлю.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'trade-locked' => [
            'subject' => 'Заблокирована возможность торговать на '.config('app.name'),
            'content' => 'Внимание! Администрация заблокировала для вас возможность вести торговлю.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'trade-unlocked' => [
            'subject' => 'Разблокирована возможность торговать на '.config('app.name'),
            'content' => 'Подздравляем! Вы можете возобновить торговлю.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
    ],

    'idea' => [
        'subject' => 'Посетитель поделился идеей для '.config('app.name'),
        'content' => ':name, хочет поделиться с Вами идеей: ',
        'description' => ":text",
        'footer'  => 'Почта: :email, логин: :login',
    ],

    'support' => [
        'api_access_request' => [
            'subject' => 'Посетитель хочет доступ к API '.config('app.name'),
            'content' => ':name, хочет доступ к API: ',
            'description' => ":text",
            'footer'  => 'Почта: :email, логин: :login',
        ]
    ],

];
