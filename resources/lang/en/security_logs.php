<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Security logs constants
    |--------------------------------------------------------------------------
    */

    'password_changed' => 'password changed',
    'password_change_fail_2fa' => '2fa validation error',

    'email_confirmation_start' => 'email confirmation started',

    'user_locked'       => 'locked',
    'user_block_trade'  => 'trade locked',
    'user_unlocked'     => 'unlocked',
    'user_unlock_trade' => 'trade unlocked',

    'password_is_reset' => 'password is reset',

    '2fa_connect'    => '2FA connect',
    '2fa_disconnect' => '2FA disconnect',
    '2fa_enable'     => '2FA enable',
    '2fa_disable'    => '2FA disable',
    '2fa_deactivate' => '2FA deactivate',

    'phone_add'             => 'phone add',
    'phone_add_confirm'     => 'phone add confirm',
    'phone_delete'          => 'phone marked to delete',
    'phone_delete_complete' => 'phone deleted',

    'telegramm_add'         => 'telegram added',
    'telegramm_add_confirm' => 'telegram confirmed',

    'email_change'         => 'email changed',
    'email_confirm_change' => 'confirm new email',
    'email_confirm'        => 'email confirmed',

    'phone_change'         => 'phone changed',
    'phone_confirm_change' => 'confirm new phone',
    'phone_confirm'        => 'phone confirmed',
];
