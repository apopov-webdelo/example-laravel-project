<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 16:21
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'error' => [
        'shortage_balance'      => 'The seller has insufficient funds!',
        'not_found_closed_deal' => 'No closed deals found.',
        'cant_cancel'           => 'You cannot cancel deal at this moment.',
    ],
];
