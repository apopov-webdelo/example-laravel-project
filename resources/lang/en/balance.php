<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 16:21
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'transaction' => [

        'commission' => [
            'default' => 'Commission by user ID :user_id',
            'deal'    => 'Commission by deal ID :deal_id',
            'payout'  => 'Commission by payout. Request #:payout_id',
        ],

        'deal' => [
            'default' => 'Final transfer by deal ID :deal_id',
        ],

        'deposit' => 'Deposit through blockchain transaction',

        'pay_in' => 'Deposit through blockchain',

        'withdraw' => 'Withdraw through blockchain transaction',

        'pay_out' => 'Withdraw funds through blockchain to wallet :wallet',

        'cashback' => 'Deposit cashback #:cashback_id for deals :deal_list',

        'partnership' => 'Deposit by partnership programm',
    ],

    'balance' => [
        'check' => [
            'not_enough' => 'Not enough crypto currency for this transaction',
        ],
        'find' => [
            'not_exists' => 'Balance does not exist for this crypto currency'
        ],
    ],

    'payout' => [
        'min_amount_error' => 'Amount (:amount) less than minimal payout limit (:min_amount)!',
    ],
];
