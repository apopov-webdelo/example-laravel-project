<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'gt'                   => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte'                  => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'lt'                   => [
        'numeric' => 'The :attribute must be less than :value.',
        'file'    => 'The :attribute must be less than :value kilobytes.',
        'string'  => 'The :attribute must be less than :value characters.',
        'array'   => 'The :attribute must have less than :value items.',
    ],
    'lte'                  => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file'    => 'The :attribute must be less than or equal :value kilobytes.',
        'string'  => 'The :attribute must be less than or equal :value characters.',
        'array'   => 'The :attribute must not have more than :value items.',
    ],
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'auth' => [
        'wrong_login_password' => 'Wrong login or password',
        'banned'               => 'Your account is banned',
        'google2fa_required'   => 'Google 2FA code is required',
    ],

    'custom_errors' => [
        'token'                 => [
            'required' => 'Token is required',
        ],
        'login'                 => [
            'required' => 'Login is required',
            'string'   => 'Login should be a string',
            'max'      => 'Login could not have more than 255 characters',
            'unique'   => 'Login should be unique',
        ],
        'email'                 => [
            'required' => 'E-mail is required',
            'string'   => 'E-mail should be a string',
            'email'    => 'E-mail should be a valid email',
            'max'      => 'E-mail should not have more than 255 characters',
            'unique'   => 'E-mail should be unique',
        ],
        'password'              => [
            'required'  => 'Password is required',
            'string'    => 'Password should be a string',
            'confirmed' => 'Password should be confirmed',
            'min'       => 'Password should have more than 7 characters',
            'max'       => 'Password should not have more than 45 characters',
            'regex'     => 'Password should contain an upper case character and a digit',
        ],
        'password_confirmation' => [
            'required' => 'Password confirmation is required',
            'string'   => 'Password confirmation should be a string',
            'same'     => 'Password confirmation should match to password',
        ],
    ],

    'attributes' => [],

    'tor_denied' => 'Tor browser usage is denied',


    'phone_already_exsits'    => 'Phone is already exists',
    'phone_doesnt_exsits'     => 'Phone doesn\'t exist',
    'telegram_already_exsits' => 'Telegram is already exists',

    'ad_is_not_active'           => 'Ad is not active now',
    'deal_author_is_ad_author'   => 'Deal author should not be an author of ad',
    'balance_is_to_small'        => 'Seller\'s balance is too small for deal',
    'deal_email_required'        => 'Confirmed email is required for dealer',
    'deal_phone_required'        => 'Confirmed phone is required for dealer',
    'deal_trust_required'        => 'Trust between dealer and offer is required',
    'deal_blacklist_detected'    => 'You are in the blacklist',
    'deal_turnover_required'     => 'Your turnover is too small for this deal',
    'deal_invalid_amount_period' => 'Invalid amount',
    'deal_invalid_price'         => 'Price has been changed',

    'email_free_required' => 'Chosen email is already occupied',
    'phone_free_required' => 'Chosen phone is already occupied',

    'ad' => [
        'unique_ad'          => 'The same advertisement is exists',
        'limit_less_balance' => 'Max limit is more than current balance',
        'price_unknown'      => 'Price unknown',
    ],

    'google2fa' => [
        'connected' => 'Google 2Fa is already connected',
    ],

    'deal' => [
        'ad_is_protected' => 'Your public key is needed for this ad.',
        'ad_is_not_active' => 'Ad is not active now',
        'author_is_ad_author' => 'Deal author should not be an author of ad',
        'balance_is_to_small' => 'Seller\'s balance is too small for deal',
        'email_required' => 'Confirmed email is required for dealer',
        'phone_required' => 'Confirmed phone is required for dealer',
        'trust_required' => 'Trust between dealer and offer is required',
        'blacklist_detected' => 'You are in the blacklist',
        'blacklist_detected_owner' => 'User is in your blacklist.',
        'turnover_required' => 'Your turnover is too small for this deal',
        'invalid_amount_period' => 'Invalid amount',
        'invalid_price' => 'Price has been changed',
        'invalid_dealer_balance' => 'Invalid dealer balance',
        'invalid_reputation' => 'Your reputation less then required',
        'min_deal_finished_count' => 'You have finished not enough deals',
        'is_not_deadline_time' => 'The deal time has not yet expired',
        'invalid_buyer_deal_cancellation_max_percent' => 'Your deal cancellation coefficient is more than required by dealer',
        'crypto_amount_less_min_limit' => 'Crypto amount less than min. limit. Min limit is :limit :code',
    ],

    'user' => [
        'email_diff' => 'Email should be different',
        'phone_diff' => 'Phone should be different',

        'google2fa_doesnt_match' => 'Google 2fa code doesn\'t match',
        'password_doesnt_match'  => 'Password doesn\'t match',

        'email_access_token'       => 'Email access token doesn\'t match',
        'email_confirmation_token' => 'Email confirmation token doesn\'t match',

        'phone_access_token'          => 'Phone access token doesn\'t match',
        'phone_confirmation_token'    => 'Phone confirmation token doesn\'t match',
        'telegram_confirmation_token' => 'Telegram confirmation token doesn\'t match',
    ],

];
