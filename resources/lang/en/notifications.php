<?php

$standardAction = 'Details';
$standardFooter = 'Thank you for using our application!';

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    'hello' => 'Hello!',

    'partnership' => [
        'partner-register' => [
            'subject' => 'Вы стали партнером на сервисе '.config('app.name'),
            'content' => 'Рады приветсвовать вас как партнера! Зарабатывайте вместе с нами!',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
        'referral-register' => [
            'subject' => 'Был зарегистрирован ваш реферал на сервисе '.config('app.name'),
            'content' => 'Чтобы заработать больше, вы должны приглашать активных трейдеров',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
        'partner-award' => [
            'subject' => 'Прибыль за сделку вашего реферала на сервисе '.config('app.name'),
            'content' => 'Вы получаете :commission :commissionType за сделку вашего реферала',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ]
    ],

    'deal' => [
        'placed' => [
            'ad_author' => [
                'subject' => 'Deal has been placed',
                'content' => 'Has been placed a deal for your advertisement #:adId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'canceled' => [
            'seller' => [
                'subject' => 'Deal #:dealId has been canceled',
                'content' => 'Has been canceled deal #:dealId for your advertisement #:adId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'finished' => [
            'buyer' => [
                'subject' => 'Deal #:dealId has been finished',
                'content' => 'Has been finished deal #:dealId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'paid' => [
            'seller' => [
                'subject' => 'Deal #:dealId has been paid',
                'content' => 'Has been paid deal #:dealId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'disputed' => [
            'participant' => [
                'subject' => 'Deal #:dealId has been disputed',
                'content' => ':login opened a dispute for deal #:dealId',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
    ],

    'chat' => [
        'placed' => [
            'participant' => [
                'subject' => 'Started dialog with :recipientString',
                'content' => 'Has been opened a dialog with :recipientString',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
        'message-placed' => [
            'recipient' => [
                'subject' => 'You received new message from :login',
                'action'  => $standardAction,
                'footer'  => $standardFooter,
            ],
        ],
    ],

    'user' => [
        'registration-message' => [
            'subject' => 'Вы успешно зарегистрировались на сервисе '.config('app.name'),
            'content' => 'Добро пожаловать на Risex! ',
            'description' => '
                Мы любители криптовалют, энтузиасты! Поэтому мы рады приветствовать вас 
                на портале безопасных сделок с криптовалютами.
            ',
            'action'  => 'Подтвердить свою учетную запись',
            'footer'  => $standardFooter,
        ],

        'email-access' => [
            'subject' => 'Email access code',
            'content' => 'You just started email change process. To change email you should copy next code.',
            'action'  => 'Code: :accessToken',
            'footer'  => $standardFooter,
        ],
        'email-confirmation' => [
            'subject' => 'Email confirmation code',
            'content' => 'Your email has been successfully changed. To confirm new email you should copy next code.',
            'action'  => 'Code: :confirmToken',
            'footer'  => $standardFooter,
        ],
        'phone-access' => [
            'content' => 'To confirm phone number changing copy this code: :confirmToken'
        ],
        'phone-confirmation' => [
            'content' => 'To confirm new phone number copy this code: :confirmToken'
        ],
        'telegram-confirmation' => [
            'content' => 'To confirm telegram, please copy this code: :confirmToken'
        ],

        'login-success' => [
            'subject' => 'Login success',
            'content' => ':login, successfully signed in on ersec',
            'action'  => "Time is :time. Geolocation is :country, :city. \r\n
                          If you do not know about this action, please change your password as soon as possible.",
            'footer'  => $standardFooter,
        ],

        'login-error' => [
            'subject' => 'Login success',
            'content' => ':login, successfully signed in on ersec',
            'action'  => "Time is :time. Geolocation is :country, :city. \r\n
                          If you do not know about this action, please change your password as soon as possible.",
            'footer'  => $standardFooter,
        ],

        'review-laced' => [
            'subject' => 'Has been placed new review',
            'content' => ':author wrote a review for you.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'locked' => [
            'subject' => 'Ваша учетная запись заблокирована на '.config('app.name'),
            'content' => 'Внимание! Администрация заблокировала вашу учетную запись из-за подозрительной активности.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'unlocked' => [
            'subject' => 'Ваша учетная запись разблокирована на '.config('app.name'),
            'content' => 'Подздравляем! Ваша учетная запись была разблокирована. Вы можете возобновить торговлю.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'trade-locked' => [
            'subject' => 'Заблокирована возможность торговать на '.config('app.name'),
            'content' => 'Внимание! Администрация заблокировала для вас возможность вести торговлю.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],

        'trade-unlocked' => [
            'subject' => 'Разблокирована возможность торговать на '.config('app.name'),
            'content' => 'Подздравляем! Вы можете возобновить торговлю.',
            'action'  => $standardAction,
            'footer'  => $standardFooter,
        ],
    ],

];
