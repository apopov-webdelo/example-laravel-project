<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/16/18
 * Time: 10:35
 */

namespace App\Makers;

use App\Exceptions\Maker\UnexpectedClassException;
use Illuminate\Database\Eloquent\Model;

/**
 * Abstract base class Maker for simple models creating
 *
 * @package database\makers
 */
abstract class Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class;

    /**
     * Model will be instances using factory
     *
     * @var Model
     */
    protected $model;

    /**
     * Default model fields
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Maker constructor.
     * @param array $attributes
     * @throws UnexpectedClassException
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
        $this->instanceModel();
    }

    /**
     * Set attributes to model
     *
     * Important! Its will be set using fill() method!
     *
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes = [])
    {
        $this->model->fill($attributes);
        return $this;
    }

    /**
     * Instant default model using factory
     *
     * @return $this
     * @throws UnexpectedClassException
     */
    protected function instanceModel()
    {

        if (empty((string)$this->class)) {
            throw new UnexpectedClassException('Class value is empty!');
        }

        return $this->setModel(factory($this->class)->make($this->attributes))
                    ->boot();
    }

    /**
     * Set concrete model to maker
     *
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Return created model
     *
     * @return Model
     */
    public function make()
    {
        return $this->model;
    }

    /**
     * Method could be redefine in derived classes
     *
     * @return $this
     */
    protected function boot()
    {
        return $this;
    }

    /**
     * Store in DB and return model
     *
     * @return Model
     */
    public function create()
    {
        $this->model->save();
        return $this->model;
    }

    /**
     * Instance new maker with attributes
     *
     * @param array $attributes
     * @return Maker
     */
    public static function init(array $attributes = [])
    {
        return app(get_called_class(), ['attributes' => $attributes]);
    }
}