<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/16/18
 * Time: 12:36
 */

namespace App\Makers;

use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use Carbon\Carbon;

/**
 * Maker class for fake Deal creating
 *
 * @package database\makers
 * @property Deal $model
 * @method static DealMaker init(array $attributes = [])
 * @method Deal create()
 * @method Deal make()
 */
class DealMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Deal::class;

    /**
     * @return Maker|void
     * @throws \Exception
     */
    public function boot()
    {
        if (!isset($this->attributes['author_id'])) {
            $this->author(UserMaker::init()->tradeAvailable()->active()->create());
        }

        $this->verified();
    }

    /**
     * Set concrete Ad
     *
     * @param Ad $ad
     * @return $this
     */
    public function ad(Ad $ad)
    {
        $this->model->ad()->save($ad);
        return $this;
    }

    /**
     * Set concrete author
     *
     * @param User $user
     * @return $this
     */
    public function author(User $user)
    {
        $this->model->author()->associate($user);
        return $this;
    }

    /**
     * Set concrete price
     *
     * @param int $value
     * @return $this
     */
    public function price(int $value)
    {
        $this->model->price = $value;
        return $this;
    }

    /**
     * Set deal status by statusId
     *
     * @param int         $statusId
     * @param Carbon|null $createdDate
     *
     * @return $this
     * @throws \Exception
     */
    public function status(int $statusId, Carbon $createdDate = null)
    {
        if (not_in_array($statusId, DealStatusConstants::STATUSES)) {
            throw new \Exception('Unexpected deal statusId '.$statusId.'!');
        }
        $status = DealStatus::findOrFail($statusId);
        $this->model->save();
        $attr = ['created_at' => $createdDate ?? now()];
        $this->model->statuses()->attach($status, $attr);
        return $this;
    }

    /**
     * Set status In Process
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function verified()
    {
        return $this->status(DealStatusConstants::VERIFIED);
    }

    /**
     * Set status In Process
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function paid()
    {
        return $this->status(DealStatusConstants::PAID);
    }

    /**
     * Set status Cancellation
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function cancellation()
    {
        return $this->status(DealStatusConstants::CANCELLATION);
    }

    /**
     * Set status Canceled
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function canceled()
    {
        return $this->status(DealStatusConstants::CANCELED);
    }

    /**
     * Set status Autocanceled
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function autoCanceled()
    {
        return $this->status(DealStatusConstants::AUTOCANCELED);
    }

    /**
     * Set status In Dispute
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function inDispute()
    {
        return $this->status(DealStatusConstants::IN_DISPUTE);
    }

    /**
     * Set status Finishing
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function finishing()
    {
        return $this->status(DealStatusConstants::FINISHING);
    }

    /**
     * Set status Finished
     *
     * @return DealMaker
     *
     * @throws \Exception
     */
    public function finished()
    {
        return $this->status(DealStatusConstants::FINISHED);
    }
}