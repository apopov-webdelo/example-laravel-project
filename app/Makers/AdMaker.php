<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/16/18
 * Time: 11:38
 */

namespace App\Makers;

use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\Ad\AdStatus;
use App\Models\Ad\AdStatusConstants;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;

/**
 * Maker class for fake Ad creating
 *
 * @package database\makers
 * @property Ad $model
 * @method static AdMaker init(array $attributes = [])
 * @method Ad create()
 * @method Ad make()
 */
class AdMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Ad::class;

    public function boot()
    {
        if (!isset($this->attributes['is_active'])) {
            $this->active();
        }
        
        if (!isset($this->attributes['is_sale'])) {
            $this->sale();
        }
        
        if (!isset($this->attributes['author_id'])) {
            $this->author(UserMaker::init()->tradeAvailable()->create());
        }
        
        if (!isset($this->attributes['review_rate'])) {
            $this->reviewRate(30);
        }
        
        if (!isset($this->attributes['turnover'])) {
            $this->turnover(0);
        }
        
        if (!isset($this->attributes['min_deal_finished_count'])) {
            $this->minDealFinish(0);
        }

        return $this;
    }

    /**
     * Set ad author
     *
     * @param User $user
     * @return $this
     */
    public function author(User $user)
    {
        $this->model->author()->associate($user);
        return $this;
    }

    /**
     * Set concrete payment system
     *
     * @param PaymentSystem $paymentSystem
     * @return $this
     */
    public function paymentSystem(PaymentSystem $paymentSystem)
    {
        $this->model->paymentSystem()->associate($paymentSystem);
        return $this;
    }

    /**
     * Set concrete notes
     *
     * @param string $text
     * @return $this
     */
    public function notes(string $text)
    {
        $this->model->notes = $text;
        return $this;
    }

    /**
     * Set concrete country
     *
     * @param Country $country
     * @return $this
     */
    public function country(Country $country)
    {
        $this->model->country()->associate($country);
        return $this;
    }

    /**
     * Set sale type (default = for sale)
     *
     * @param bool $type
     * @return $this
     */
    public function sale(bool $type = true)
    {
        $this->model->is_sale = $type;
        return $this;
    }

    /**
     * Set concrete currency
     *
     * @param Currency $currency
     * @return $this
     */
    public function currency(Currency $currency)
    {
        $this->model->currency()->associate($currency);
        return $this;
    }

    /**
     * Set concrete crypto currency
     *
     * @param CryptoCurrency $currency
     * @return $this
     */
    public function cryptoCurrency(CryptoCurrency $currency)
    {
        $this->model->cryptoCurrency()->associate($currency);
        return $this;
    }

    /**
     * Set concrete price
     *
     * @param int $value
     * @return $this
     */
    public function price(int $value)
    {
        $this->model->price = $value;
        return $this;
    }

    /**
     * Set concrete limits
     *
     * @param int $min
     * @param int $max
     * @param int|null $originalMax
     * @return $this
     */
    public function limits(int $min, int $max, int $originalMax = null)
    {
        $this->model->min = $min;
        $this->model->max = $max;
        $this->model->original_max = $originalMax ?? $max;
        return $this;
    }

    /**
     * Set concrete turnover
     *
     * @param int $value
     * @return $this
     */
    public function turnover(int $value)
    {
        $this->model->turnover = $value;
        return $this;
    }

    /**
     * Set concrete time
     *
     * @param int $value
     * @return $this
     */
    public function time(int $value)
    {
        $this->model->time = $value;
        return $this;
    }

    /**
     * Set concrete liquidity
     *
     * @param bool $required
     * @return $this
     */
    public function liquidity(bool $required = true)
    {
        $this->model->liquidity_required = $required;
        return $this;
    }

    /**
     * Set concrete email confirmation property
     *
     * @param bool $required
     * @return $this
     */
    public function emailConfirmation(bool $required = true)
    {
        $this->model->email_confirm_required = $required;
        return $this;
    }

    /**
     * Set concrete phone confirmation property
     *
     * @param bool $required
     * @return $this
     */
    public function phoneConfirmation(bool $required = true)
    {
        $this->model->phone_confirm_required = $required;
        return $this;
    }

    /**
     * Set concrete trust confirmation
     *
     * @param bool $required
     * @return $this
     */
    public function trust(bool $required = true)
    {
        $this->model->trust_required = $required;
        return $this;
    }

    /**
     * Mark ad as active
     *
     * @return $this
     */
    public function active()
    {
        return $this->status(AdStatusConstants::ACTIVE);
    }

    /**
     * Set concrete status
     *
     * @param AdStatus|int $status
     * @return $this
     */
    public function status($status)
    {
        if (false == $status instanceof AdStatus) {
            $status = app(AdStatus::class)->findOrFail($status);
        }
        $this->model->save();
        $this->model->statuses()->attach($status, [], false);
        $this->model->is_active = ($status->id == AdStatusConstants::ACTIVE);
        return $this;
    }

    /**
     * Set ad bank by id
     *
     * @param int|array $banksId
     * @param int|null $userId
     * @return $this
     */
    public function bank($banksId, $userId = null)
    {
        $userId = $userId ?? $this->model->author()->id;

        $banksId = is_array($banksId) ? $banksId : [$banksId];

        $this->model->save();
        foreach ($banksId as $bankId) {
            $bank = Bank::findOrFail($bankId);
            $this->model->banks()->attach($bank, ['author_id' => $userId]);
        }

        return $this;
    }

    /**
     * Mark ad as blocked
     *
     * @return $this
     */
    public function blocked()
    {
        return $this->status(AdStatusConstants::BLOCKED);
    }

    /**
     * Alias for blocked() method
     *
     * @return $this
     */
    public function inactive()
    {
        return $this->blocked();
    }

    /**
     * Set concrete conditions
     *
     * @param string $text
     * @return $this
     */
    public function conditions(string $text)
    {
        $this->model->conditions = $text;
        return $this;
    }

    /**
     * Set concrete review rate
     *
     * @param int $rate
     * @return $this
     */
    public function reviewRate(int $rate)
    {
        $this->model->review_rate = $rate;
        return $this;
    }

    /**
     * Block ad for tor access
     *
     * @return $this
     */
    public function torDenied()
    {
        $this->model->tor_denied = true;
        return $this;
    }

    /**
     * Allow ad for tor access
     *
     * @return $this
     */
    public function torAllowed()
    {
        $this->model->tor_denied = false;
        return $this;
    }

    /**
     * Set concrete min deal finished count
     *
     * @param int $countDeals
     * @return $this
     */
    public function minDealFinish(int $countDeals)
    {
        $this->model->min_deal_finished_count = $countDeals;
        return $this;
    }
}