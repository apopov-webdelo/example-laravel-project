<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/21/18
 * Time: 07:22
 */

namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\User\Security;
use App\Models\User\User;

/**
 * Maker class for fake Security creating
 *
 * @package database\makers
 * @property Security $model
 * @method static SecurityMaker init(array $attributes = [])
 * @method Security create()
 * @method Security make()
 */
class SecurityMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Security::class;

    /**
     * Associate reputation with User model
     *
     * @param User $user
     * @return $this
     */
    public function user(User $user)
    {
        $this->model->user()->associate($user);
        return $this;
    }

    /** Set email confirmed
     *
     * @return $this
     */
    public function emailConfirmed()
    {
        $this->model->email_confirmed = true;
        return $this;
    }

    /** Set email unconfirmed
     *
     * @return $this
     */
    public function emailNotConfirmed()
    {
        $this->model->email_confirmed = false;
        return $this;
    }
}
