<?php

namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\Session\Session;
use App\Models\User\User;

/**
 * Maker class for fake Session creating
 *
 * @package database\makers
 * @property Session $model
 * @method static SessionMaker init(array $attributes = [])
 * @method Session create()
 * @method Session make()
 */
class SessionMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Session::class;

    /**
     * Associate Session with User model
     *
     * @param User $user
     * @return $this
     */
    public function sessionable(User $user)
    {
        $this->model->sessionable()->associate($user);
        return $this;
    }
}
