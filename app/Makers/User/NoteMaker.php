<?php
namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\User\Note;
use App\Models\User\User;

/**
 * Maker class for fake Note creating
 *
 * @package database\makers
 * @property Note $model
 * @method static NoteMaker init(array $attributes = [])
 * @method Note create()
 * @method Note make()
 */
class NoteMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Note::class;

    /**
     * Associate Note Author with User model
     *
     * @param User $user
     * @return $this
     */
    public function author(User $user)
    {
        $this->model->save();
        $this->model->author()->associate($user);
        return $this;
    }

    /**
     * Associate Note Recipient with User model
     *
     * @param User $user
     * @return $this
     */
    public function recipient(User $user)
    {
        $this->model->save();
        $this->model->recipient()->associate($user);
        return $this;
    }
}