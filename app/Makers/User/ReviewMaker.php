<?php
namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\Deal\Deal;
use App\Models\User\Review;
use App\Models\User\User;

/**
 * Maker class for fake Review creating
 *
 * @package database\makers
 * @property Review $model
 * @method static ReviewMaker init(array $attributes = [])
 * @method Review create()
 * @method Review make()
 */
class ReviewMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Review::class;

    /**
     * Associate Review Author with User model
     *
     * @param User $user
     * @return $this
     */
    public function author(User $user)
    {
        $this->model->save();
        $this->model->author()->associate($user);
        return $this;
    }

    /**
     * Associate Review Recipient with User model
     *
     * @param User $user
     * @return $this
     */
    public function recipient(User $user)
    {
        $this->model->save();
        $this->model->recipient()->associate($user);
        return $this;
    }

    /**
     * Associate Review Deal with Deal model
     *
     * @param Deal $deal
     * @return $this
     */
    public function deal(Deal $deal)
    {
        $this->model->save();
        $this->model->deal()->associate($deal);
        return $this;
    }

    /**
     * Set rate value
     *
     * @param int $rate
     * @return $this
     */
    public function rate(int $rate)
    {
        $this->model->rate = $rate;
        return $this;
    }

    /**
     * Set trust
     *
     * @param string $trust
     *
     * @return $this
     */
    public function trust(string $trust)
    {
        $this->model->trust = $trust;
        return $this;
    }
}