<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/20/18
 * Time: 14:43
 */

namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\User\Reputation;
use App\Models\User\User;

/**
 * Maker class for fake Reputation creating
 *
 * @package database\makers
 * @property Reputation $model
 * @method static ReputationMaker init(array $attributes = [])
 * @method Reputation create()
 * @method Reputation make()
 */
class ReputationMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Reputation::class;

    /**
     * Associate reputation with User model
     *
     * @param User $user
     * @return $this
     */
    public function user(User $user)
    {
        $this->model->user()->associate($user);
        return $this;
    }

    /**
     * Set rate value
     *
     * @param int $rate
     * @return $this
     */
    public function rate(int $rate)
    {
        $this->model->rate = $rate;
        return $this;
    }

    /**
     * Set positive count
     *
     * @param int $dealsCount
     * @return $this
     */
    public function positiveCount(int $dealsCount)
    {
        $this->model->positive_count = $dealsCount;
        return $this;
    }

    /**
     * Set negative count
     *
     * @param int $dealsCount
     * @return $this
     */
    public function negativeCount(int $dealsCount)
    {
        $this->model->negative_count = $dealsCount;
        return $this;
    }
}