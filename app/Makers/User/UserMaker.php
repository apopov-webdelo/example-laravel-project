<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/16/18
 * Time: 10:35
 */

namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;

/**
 * Maker class for fake User creating
 *
 * @package database\makers
 * @property User $model
 * @method static UserMaker init(array $attributes = [])
 * @method User create()
 * @method User make()
 */
class UserMaker extends Maker
{
    const STATE_ACTIVE   = 'active';

    const STATE_INACTIVE = 'inactive';

    const TRADE_AVAILABLE = 'available';

    const TRADE_LOCKED = 'locked';

    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = User::class;

    /**
     * @return $this
     */
    protected function boot()
    {
        $user = $this->create();
        ReputationMaker::init()->user($user)->rate(90)->create();
        SecurityMaker::init()->user($user)->emailConfirmed()->create();
        SettingsMaker::init()->user($user)->create();
        StatisticMaker::init()->user($user)->create();

        foreach (CryptoCurrency::all() as $currency) {
            factory(Balance::class)->create([
                'user_id'            => $user->id,
                'crypto_currency_id' => $currency->id,
            ]);
        }

        return $this;
    }

    /**
     * Set fields to be unlocked
     *
     * @return UserMaker
     */
    public function active()
    {
        return $this->state(self::STATE_ACTIVE);
    }

    /**
     * Set fields to lock or unlock user
     *
     * @param string $state
     * @return $this
     */
    public function state(string $state = self::STATE_ACTIVE)
    {
        if ($state == self::STATE_INACTIVE) {
            $this->model->user_locked_at = now()->subDay();
            $this->model->user_locked_to = now()->addDay();
        } else {
            $this->model->user_locked_at = now()->subDays(2);
            $this->model->user_locked_to = now()->subDay();
        }

        return $this;
    }

    /**
     * Set fields to be locked
     *
     * @return UserMaker
     */
    public function inactive()
    {
        return $this->state(self::STATE_INACTIVE);
    }

    /**
     * Set field for UNLOCK trading operations
     *
     * @return UserMaker
     */
    public function tradeAvailable()
    {
        return $this->tradeStatus(self::TRADE_AVAILABLE);
    }

    /**
     * Set field for lock or unlock trading operations
     *
     * @param string $state
     * @return $this
     */
    public function tradeStatus(string $state = self::TRADE_AVAILABLE)
    {
        if ($state == self::TRADE_AVAILABLE) {
            $this->model->trade_locked_at = now()->subDays(2);
            $this->model->trade_locked_to = now()->subDay();
        } else {
            $this->model->trade_locked_at = now()->subDay();
            $this->model->trade_locked_to = now()->addDay();
        }

        return $this;
    }

    /**
     * Set field for LOCK trading operations
     *
     * @return UserMaker
     */
    public function tradeLock()
    {
        return $this->tradeStatus(self::TRADE_LOCKED);
    }

    /**
     * Set concrete phone number
     *
     * @param string $phone
     * @return $this
     */
    public function phone(string $phone)
    {
        $this->model->phone = $phone;
        return $this;
    }

    /**
     * Set concrete login
     *
     * @param string $login
     * @return $this
     */
    public function login(string $login)
    {
        $this->model->login = $login;
        return $this;
    }

    /**
     * Set concrete telegramID
     *
     * @param string $telegramId
     * @return $this
     */
    public function telegram(string $telegramId)
    {
        $this->model->telegram_id = $telegramId;
        return $this;
    }

    /**
     * Set concrete appeal
     *
     * @param string $text
     * @return $this
     */
    public function appeal(string $text)
    {
        $this->model->appeal = $text;
        return $this;
    }

    /**
     * Set concrete token
     *
     * @param string $token
     * @return $this
     */
    public function token(string $token)
    {
        $this->model->token = $token;
        return $this;
    }

    /**
     * Set concrete google 2fa secret code
     *
     * @param string $secret
     * @return $this
     */
    public function google2fa(string $secret)
    {
        $this->model->google2fa_secret = $secret;
        return $this;
    }
}