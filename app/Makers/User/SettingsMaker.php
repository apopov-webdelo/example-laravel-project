<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/21/18
 * Time: 07:22
 */

namespace App\Makers\User;

use App\Makers\Maker;
use App\Models\User\Settings;
use App\Models\User\User;

/**
 * Maker class for fake Settings creating
 *
 * @package database\makers
 * @property Settings $model
 * @method static SettingsMaker init(array $attributes = [])
 * @method Settings create()
 * @method Settings make()
 */
class SettingsMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Settings::class;

    /**
     * Associate Settings with User model
     *
     * @param User $user
     * @return $this
     */
    public function user(User $user)
    {
        $this->model->user()->associate($user);
        return $this;
    }
}
