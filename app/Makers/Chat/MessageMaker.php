<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/21/18
 * Time: 13:24
 */

namespace App\Makers\Chat;

use App\Makers\Maker;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\User\User;

/**
 * Maker class for fake Message creating
 *
 * @package database\makers
 * @property Message $model
 * @method static MessageMaker init(array $attributes = [])
 * @method Message create()
 * @method Message make()
 */
class MessageMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Message::class;

    /**
     * Associate message with User model
     *
     * @param User $user
     * @return $this
     */
    public function author(User $user)
    {
        $this->model->author()->associate($user);
        $this->model->save();
        return $this;
    }

    /**
     * Relate message with concrete chat
     *
     * @param Chat $chat
     * @return $this
     */
    public function chat(Chat $chat)
    {
        $this->model->save();
        $this->model->chat()->save($chat);
        return $this;
    }
}