<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/21/18
 * Time: 13:24
 */

namespace App\Makers\Chat;

use App\Makers\Maker;
use App\Models\Chat\Chat;
use App\Models\User\User;

/**
 * Maker class for fake Chat creating
 *
 * @package database\makers
 * @property Chat $model
 * @method static ChatMaker init(array $attributes = [])
 * @method Chat create()
 * @method Chat make()
 */
class ChatMaker extends Maker
{
    /**
     * Class for model using this Maker
     *
     * @var string
     */
    protected $class = Chat::class;

    /**
     * @var Chat
     */
    protected $chat;

    protected function boot()
    {
        $chat = $this->create();
        $this->chat = $chat;
    }

    /**
     * @param User $user
     * @param User $recipient
     *
     * @return $this
     */
    public function withMessages(User $user, User $recipient)
    {
        $data = [
            'chat_id' => $this->chat->id,
            'author_id' => $user,
        ];

        MessageMaker::init($data)->create();
        MessageMaker::init($data)->create();

        $data = [
            'chat_id' => $this->chat->id,
            'author_id' => $recipient,
        ];

        MessageMaker::init($data)->create();
        MessageMaker::init($data)->create();

        return $this;
    }

    /**
     * @param User $recipient
     *
     * @return $this
     */
    public function withRecepient(User $recipient)
    {
        $this->chat->recipients()->attach($recipient);

        return $this;
    }

    /**
     * Associate message with User model
     *
     * @param User $user
     *
     * @return $this
     */
    public function author(User $user)
    {
        $this->model->author()->associate($user);

        return $this;
    }

    /**
     * Set concrete title
     *
     * @param string $text
     *
     * @return $this
     */
    public function title(string $text)
    {
        $this->model->title = $text;

        return $this;
    }
}