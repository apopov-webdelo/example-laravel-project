<?php

namespace App\Console;

use App\Jobs\Ad\DeactivateAd;
use App\Jobs\Auth\RevokeExpiredTokens;
use App\Jobs\Balance\PayOut\CheckPayOutProcessingDeadline;
use App\Jobs\Deal\AutocancelDeal;
use App\Jobs\Deal\Status\CheckDealCancellationStatusDeadline;
use App\Jobs\Deal\Status\CheckDealFinishingStatusDeadline;
use App\Jobs\Deal\Status\CheckDealVerifyingStatusDeadline;
use App\Jobs\Exchange\CryptoRatesUpdate;
use App\Jobs\Exchange\FiatRatesBatchUpdate;
use App\Jobs\Exchange\FiatRatesUpdate;
use App\Utils\User\RobotBinding;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // bind robot user for cron operations
        if (isNotTesting()) {
            RobotBinding::bind();
        }

        $schedule->command('telescope:prune --hours=24')->hourly();

        $schedule->job(DeactivateAd::class)->everyMinute();

        $schedule->job(CryptoRatesUpdate::class)->everyTenMinutes();
        $schedule->job(FiatRatesUpdate::class)->everyTenMinutes();
        $schedule->job(FiatRatesBatchUpdate::class)->hourly(); // 24 x 30 = 720 requests (of 1000 from free plan)
        $schedule->job(AutocancelDeal::class)->everyMinute();
        $schedule->job(RevokeExpiredTokens::class)->everyMinute();
        $schedule->job(CheckPayOutProcessingDeadline::class)->everyMinute();
        $schedule->job(CheckDealVerifyingStatusDeadline::class)->everyMinute();
        $schedule->job(CheckDealCancellationStatusDeadline::class)->everyMinute();
        $schedule->job(CheckDealFinishingStatusDeadline::class)->everyMinute();

        // todo revoked токены хорошо бы перемещать периодически в другую таблицу (истории токенов),
        // todo чтобы горячая таблица авторизации не разрасталась сильно
        // $schedule->job(MoveExpiredTokens::class)->everyHour();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
