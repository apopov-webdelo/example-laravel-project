<?php

namespace App\Console\Commands\Balance;

use App\Console\Commands\Traits\User;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Jobs\Blockchain\CompareBalanceJob;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Repositories\User\UserRepo;
use Illuminate\Console\Command;

class VerifyBalance extends Command
{
    use User;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "
        balance:verify
        {?--crypto=btc : Crypto currency code: btc, eth etc.}
        {?--login= : target user login (optional if --userId is specified) }
        {?--userId= : target user id (optional if --login is specified) }
        {--queue : Put in queue} 
    ";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify user crypto balance agains Stellar Box information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UserRepo           $userRepo
     * @param CryptoCurrencyRepo $cryptoCurrencyRepo
     *
     * @return mixed
     */
    public function handle(UserRepo $userRepo, CryptoCurrencyRepo $cryptoCurrencyRepo)
    {
        try {
            $this->validateOptionsInput();
            $this->setQueueOption();

            $user = $this->getUser($userRepo);
            $crypto = $this->getCryptoCurrency($cryptoCurrencyRepo);

            CompareBalanceJob::dispatch($user, $crypto);

        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function validateOptionsInput()
    {
        $this->validateUserOptionsInput();
    }

    /**
     * Set command executing options
     */
    private function setQueueOption()
    {
        if ($this->option('queue') === false) {
            app('queue')->setDefaultDriver('sync');
        }
    }

    /**
     * @param CryptoCurrencyRepo $repo
     *
     * @return CryptoCurrencyContract
     */
    private function getCryptoCurrency(CryptoCurrencyRepo $repo)
    {
        $code = $this->option('crypto');

        return $repo->getByCode($code ?? CryptoCurrencyConstants::BTC_CODE);
    }
}
