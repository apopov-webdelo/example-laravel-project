<?php

namespace App\Console\Commands\Balance;

use App\Console\Commands\Traits\User;
use App\Facades\Robot;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Repositories\User\UserRepo;
use App\Services\Balance\TransactionService;
use Illuminate\Console\Command;

/**
 * Manually deposit cryptocurrency to users balance
 *
 * Syntax:
 * php artisan balance:deposit --crypto={btc} --amount={amount} --login={login}|--userId={id} --coins [--description]
 *
 * Class DepositCrypto
 *
 * @package App\Console\Commands\Balance
 */
class DepositCrypto extends Command
{
    use User;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "
        balance:deposit 
        {?--crypto=btc : Crypto currency code: btc, eth etc.} 
        {?--amount=1 : Amount to deposit in satoshis or currency (1 BTC is default) }
        {?--coins : Amount requested in satoshis }
        {?--login= : target user login (optional if --userId is specified) }
        {?--userId= : target user id (optional if --login is specified) }
        {?--description=Added by console command : reason }
    ";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Manually deposit cryptocurrency to user's balance.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TransactionService $service
     * @param CryptoCurrencyRepo $cryptoCurrencyRepo
     * @param UserRepo           $userRepo
     *
     * @return bool
     */
    public function handle(TransactionService $service, CryptoCurrencyRepo $cryptoCurrencyRepo, UserRepo $userRepo)
    {
        try {
            $this->validateOptionsInput();

            $cryptoCurrency = $this->getCryptoCurrency($cryptoCurrencyRepo);
            $amount = $this->getAmount($cryptoCurrency);
            $user = $this->getUser($userRepo);

            $description = $this->getDepositDescription();
            if ($description) {
                $service->setDescription($description);
            }

            $service->reasonTransfer()->transfer($amount, Robot::user(), $user, $cryptoCurrency);

            $balance = $user->getBalance($cryptoCurrency);
            $balanceInCrypto = currencyFromCoins($balance->amount, $cryptoCurrency);

            $this->info(
                "New balance is {$balance->amount} satoshis, {$balanceInCrypto} in {$cryptoCurrency->getCode()}"
            );
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return false;
        }


        return true;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function validateOptionsInput()
    {
        $this->validateUserOptionsInput();
    }

    /**
     * @param CryptoCurrencyRepo $repo
     *
     * @return CryptoCurrency
     */
    private function getCryptoCurrency(CryptoCurrencyRepo $repo)
    {
        $code = $this->option('crypto');

        return $repo->getByCode($code ?? CryptoCurrencyConstants::BTC_CODE);
    }



    /**
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return int
     */
    private function getAmount(CryptoCurrency $cryptoCurrency)
    {
        $amount = $this->option('amount');
        $isCoins = $this->option('coins');

        if ($isCoins === false) {
            $amount = currencyToCoins($amount, $cryptoCurrency);
        }

        return $amount;
    }

    /**
     * @return array|null|string
     */
    public function getDepositDescription()
    {
        $text = $this->option('description');

        return $text;
    }
}
