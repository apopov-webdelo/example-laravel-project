<?php

namespace App\Console\Commands\Database;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeedDemo extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbdemo:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed demo data to database from DemoDatabaseSeeder.php';

    /**
     * The connection resolver instance.
     *
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * Create a new database seed command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->resolver = app('db');
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->getLaravel()->environment() === 'production') {
            $this->error('Only runs in local environment!');

            return;
        }

        if (! $this->confirmToProceed()) {
            return;
        }

        $this->resolver->setDefaultConnection($this->getDatabase());

        Model::unguarded(function () {
            $this->getSeeder()->__invoke();
        });

        $this->info('Demo database seeding completed successfully.');
    }

    /**
     * Get a seeder instance from the container.
     *
     * @return \Illuminate\Database\Seeder
     */
    protected function getSeeder()
    {
        $class = $this->laravel->make('DemoDatabaseSeeder');

        return $class->setContainer($this->laravel)->setCommand($this);
    }

    /**
     * Get the name of the database connection to use.
     *
     * @return string
     */
    protected function getDatabase()
    {
        return $this->laravel['config']['database.default'];
    }
}
