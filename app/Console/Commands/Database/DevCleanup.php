<?php

namespace App\Console\Commands\Database;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Eloquent\Model;

class DevCleanup extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbdemo:fresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all user data from database [only when local env]';

    /**
     * The connection resolver instance.
     *
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->resolver = app('db');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $this->checkEnv();

            if (!$this->confirmToProceed()) {
                return;
            }

            $this->resolver->setDefaultConnection($this->getDatabase());

            Model::unguarded(function () {
                $this->getSeeder()->__invoke();
            });

            $this->info('Demo database refreshed successfully.');

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        return;
    }

    /**
     * @throws \Exception
     */
    protected function checkEnv()
    {
        if (app('env') !== 'local') {
            throw new \Exception('Only possible in local env!');
        }
    }

    /**
     * Get a seeder instance from the container.
     *
     * @return \Illuminate\Database\Seeder
     */
    protected function getSeeder()
    {
        $class = $this->laravel->make('FreshDatabaseSeeder');

        return $class->setContainer($this->laravel)->setCommand($this);
    }

    /**
     * Get the name of the database connection to use.
     *
     * @return string
     */
    protected function getDatabase()
    {
        return $this->laravel['config']['database.default'];
    }
}
