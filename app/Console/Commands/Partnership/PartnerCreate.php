<?php

namespace App\Console\Commands\Partnership;

use App\Console\Commands\Traits\User;
use App\Contracts\Partnership\PartnerRegisterContract;
use App\Contracts\Partnership\PartnershipProgramContract;
use App\Repositories\Partnership\ProgramRepoContract;
use App\Repositories\User\UserRepo;
use Illuminate\Console\Command;

/**
 * Class PartnerCreate
 *
 * @package App\Console\Commands\Partnership
 */
class PartnerCreate extends Command
{
    use User;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "
        partner:create 
        {?--login= : target user login (optional if --userId is specified) }
        {?--userId= : target user id (optional if --login is specified) }
        {?--programDriver= : partnership program driver if need to customize it }
    ";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register user like partner';

    /**
     * @var ProgramRepoContract
     */
    protected $programRepo;

    /**
     * @var PartnerRegisterContract
     */
    protected $partnerRegisterService;

    /**
     * @var UserRepo
     */
    protected $userRepo;

    /**
     * PartnerCreate constructor.
     *
     * @param ProgramRepoContract     $programRepo
     * @param PartnerRegisterContract $partnerRegisterService
     * @param UserRepo                $userRepo
     */
    public function __construct(
        ProgramRepoContract $programRepo,
        PartnerRegisterContract $partnerRegisterService,
        UserRepo $userRepo
    ) {
        parent::__construct();
        $this->programRepo = $programRepo;
        $this->partnerRegisterService = $partnerRegisterService;
        $this->userRepo = $userRepo;
    }


    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $this->validateOptionsInput();

            $partner = $this->partnerRegisterService->register($this->getUser($this->userRepo), $this->getProgram());

            $this->info(
                "Successfully created new partner."
            );
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function validateOptionsInput()
    {
        $this->validateUserOptionsInput();
    }

    /**
     * Retrieve partnership program
     *
     * @return PartnershipProgramContract
     */
    protected function getProgram(): PartnershipProgramContract
    {
        $driver = $this->option('programDriver');
        return $driver
            ? $this->programRepo->getByDriver($driver)
            : $this->programRepo->getDefaultProgram();
    }
}
