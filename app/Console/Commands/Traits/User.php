<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-27
 * Time: 11:12
 */

namespace App\Console\Commands\Traits;

use App\Repositories\User\UserRepo;

/**
 * Trait provide methods for working with User
 *
 * @package App\Console\Commands\Traits
 */
trait User
{
    /**
     * Validate User options
     *
     * @return void
     * @throws \Exception
     */
    protected function validateUserOptionsInput()
    {
        if (!$this->option('login') && !$this->option('userId')) {
            throw new \Exception('Please specify --login or --userId');
        }
    }

    /**
     * Retrieve User object according arguments
     *
     * @param UserRepo $userRepo
     *
     * @return bool|\App\Models\User\User
     * @throws \Exception
     */
    protected function getUser(UserRepo $userRepo)
    {
        $user = false;
        $login = $this->option('login');
        $id = $this->option('userId');

        if ($id) {
            $user = $userRepo->filterById($id)->take()->first();
        }

        if ($login) {
            $user = $userRepo->filterByLogin($login)->take()->first();
        }

        if (!$user) {
            throw new \Exception('Target user not found');
        }

        return $user;
    }
}
