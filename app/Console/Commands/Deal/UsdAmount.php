<?php

namespace App\Console\Commands\Deal;

use App\Jobs\Deal\UpdateDealCryptoAmountInUsd;
use App\Models\Deal\Deal;
use App\Models\User\User;
use Illuminate\Console\Command;

class UsdAmount extends Command
{
    /**
     * @var array
     */
    protected $log = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deal:usd_amount 
                            {--queue : Put all jobs in queue} 
                            {--force : Force recalc for all deals }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fills crypto_amount_in_usd in deals where crypto_amount_in_usd=0';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setOptions();

        $dealQuery = $this->getQuery();

        $dealCount = $dealQuery->count();

        $bar = $this->output->createProgressBar($dealCount);

        $bar->start();

        $dealQuery->chunkById(100, function ($deals) use ($bar) {
            foreach ($deals as $deal) {
                $this->performTask($deal);

                $bar->advance();
            }
        });

        $bar->finish();

        $this->dumpLog();

        $this->line('');
        $this->info('Done!');

        return true;
    }

    /**
     * @param Deal $deal
     */
    private function performTask(Deal $deal)
    {
        try {
            UpdateDealCryptoAmountInUsd::dispatch($deal);
            $this->log[] = "Dispatched update job for deal id = {$deal->id}";
        } catch (\Exception $e) {
            $this->line('');
            $this->error($e->getMessage());
            $this->line($e->getFile());
            $this->line($e->getLine());
            die;
        }
    }

    /**
     * @return Deal|\Illuminate\Database\Eloquent\Builder
     */
    private function getQuery()
    {
        return $this->option('force')
            ? Deal::query()
            : Deal::where('crypto_amount_in_usd', 0);
    }

    /**
     * Set command executing options
     */
    private function setOptions()
    {
        if ($this->option('queue') === false) {
            app('queue')->setDefaultDriver('sync');
        }
    }

    /**
     * Dump all log after operation
     */
    private function dumpLog()
    {
        if (count($this->log)) {
            $this->line('');

            foreach ($this->log as $line) {
                $this->line($line);
            }
        }
    }

}
