<?php

namespace App\Broadcasting;

use App\Models\Ad\Ad;
use App\Models\User\User;
use Log;

class AdChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function join(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }
}
