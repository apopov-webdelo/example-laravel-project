<?php

namespace App\Broadcasting;

use App\Models\User\User;

class AuthenticatedChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     *
     * @return int
     */
    public function join(User $user)
    {
        return (bool)$user->id;
    }
}
