<?php

namespace App\Broadcasting;

use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\User\User;

class DealChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Ad   $ad
     * @param Deal $deal
     *
     * @return bool
     */
    public function join(User $user, Deal $deal)
    {
        return $deal->isUserDealMember($user);
    }
}
