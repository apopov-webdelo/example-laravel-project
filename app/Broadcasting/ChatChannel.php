<?php

namespace App\Broadcasting;

use App\Models\Chat\Chat;
use App\Models\User\User;

class ChatChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param User    $user
     * @param Chat $chat
     *
     * @return bool
     */
    public function join(User $user, Chat $chat)
    {
        return $chat->isUserChatMember($user);
    }
}
