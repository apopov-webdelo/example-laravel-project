<?php

namespace App\Broadcasting;

use App\Models\User\User;

class BalanceChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param User $user
     * @param User $balanceOwner
     *
     * @return bool
     */
    public function join(User $user, User $balanceOwner)
    {
        return $user->id == $balanceOwner->id;
    }
}
