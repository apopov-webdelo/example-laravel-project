<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/31/18
 * Time: 08:02
 */

namespace App\Factories;

use App\Contracts\Exchange\Fiat\FiatFactoryContract;
use App\Contracts\Exchange\Fiat\ExchangeContract;
use App\Exceptions\Exchange\ExchangeException;

class FiatExchangeFactory implements FiatFactoryContract
{
    /**
     * Key for config data loading
     *
     * @var string
     */
    protected $configKey = 'app.exchange.fiat';

    /**
     * Return instanced market engine
     *
     * @param string|null $driver
     * @return ExchangeContract
     *
     * @throws ExchangeException
     */
    public function market(string $driver = null): ExchangeContract
    {
        $driver = $driver ?? $this->getDefaultDriver();
        return $this->instanceDriver($driver);
    }

    /**
     * Return default market engine alias
     *
     * @return string
     */
    protected function getDefaultDriver(): string
    {
        return $this->getConfig()['default_market'];
    }

    /**
     * @return array
     */
    protected function getConfig(): array
    {
        return config($this->configKey);
    }

    /**
     * Instance engine object using Container
     *
     * @param string $alias
     * @return \Illuminate\Foundation\Application|mixed
     *
     * @throws ExchangeException
     */
    protected function instanceDriver(string $alias)
    {
        $driver = app($this->getConfig()['markets'][$alias]['driver']);
        if ($driver instanceof ExchangeContract) {
            return $driver;
        }

        throw new ExchangeException('Fiat Exchange driver "'.$alias.'" not implement '.ExchangeContract::class.'!');
    }

    /**
     * Dynamically call the default driver instance.
     *
     * @param  string  $method
     * @param  array   $parameters
     *
     * @return mixed
     *
     * @throws ExchangeException
     */
    public function __call($method, $parameters)
    {
        return $this->market()->$method(...$parameters);
    }
}