<?php

namespace App\Factories;

use App\Contracts\Facade\EmergencyAlertFactoryContract;
use App\Jobs\Utils\LogToChannel;

class EmergencyAlertFactory implements EmergencyAlertFactoryContract
{
    /**
     * Valid channels for this facade
     */
    const VALID_CHANNELS = [
        'telegram_emergency',
        'jira',
        'slack',
    ];

    /**
     * @var string
     */
    private $channels = ['telegram_emergency'];

    /**
     * Set current channel(s)
     *
     * @param mixed $names
     *
     * @return EmergencyAlertFactoryContract
     */
    public function channel($names): EmergencyAlertFactoryContract
    {
        $this->channels = is_array($names) ? $names : [$names];

        return $this;
    }

    /**
     * Send message to selected channels
     *
     * @param string $message
     * @param array  $data
     *
     * @return EmergencyAlertFactoryContract
     */
    public function send(string $message, array $data = []): EmergencyAlertFactoryContract
    {
        collect($this->channels)->each(function ($channel) use ($message, $data) {
            $this->sendMessage($channel, $message, $data);
        });

        return $this;
    }

    /**
     * @param        $channel
     * @param string $message
     * @param array  $data
     */
    protected function sendMessage($channel, string $message, array $data): void
    {
        if ($this->channelInvalid($channel)) {
            return;
        }

        dispatch(app(LogToChannel::class, [
            'channel' => $channel,
            'message' => $message,
            'data'    => $data,
        ]));
    }

    /**
     * @param $channel
     *
     * @return bool
     */
    private function channelInvalid($channel)
    {
        return !collect(static::VALID_CHANNELS)->contains($channel);
    }
}
