<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:21
 */

namespace App\Factories;


use App\Contracts\RobotFactoryContract;
use App\Models\User\User;

/**
 * Class RobotFactory
 * @package App\Repositories\User
 */
class RobotFactory implements RobotFactoryContract
{
    /**
     * ID for robot user in the system
     */
    const ROBOT_USER_ID = 1;

    /**
     * @var User
     */
    protected $user;

    /**
     * RobotFactory constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function user() : User
    {
        return $this->user->findOrFail(self::ROBOT_USER_ID);
    }
}