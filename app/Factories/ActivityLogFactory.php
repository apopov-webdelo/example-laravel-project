<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/31/18
 * Time: 08:02
 */

namespace App\Factories;

use App\Contracts\ActivityLog\ActivityLogServiceContract;
use App\Contracts\ActivityLog\FactoryContract;

class ActivityLogFactory implements FactoryContract
{
    /**
     * @param string|null $name
     *
     * @return ActivityLogServiceContract
     */
    public function guard(string $name = null) : ActivityLogServiceContract
    {
        $guard = $name ?? $this->getDefaultGuard();
        return $this->instanceDriver($guard);
    }

    /**
     * Return default market engine alias
     *
     * @return string
     */
    protected function getDefaultGuard() : string
    {
        return 'api';
    }

    /**
     * @param string $guard
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    protected function instanceDriver(string $guard)
    {
        return app(ActivityLogServiceContract::class, ['user' => auth()->guard($guard)->user()]);
    }

    /**
     * Dynamically call the default driver instance.
     *
     * @param $method
     * @param $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->guard()->$method(...$parameters);
    }
}