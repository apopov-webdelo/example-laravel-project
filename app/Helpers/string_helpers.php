<?php
/**
 * Created by PhpStorm.
 * User: d.cercel
 * Date: 11/23/18
 * Time: 09:20
 */

if (! function_exists('replace')) {
    /**
     * Replace aliases within values in string
     *
     * @param string $string
     * @param array  $variables
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function replace(string $string = '', array $variables = [])
    {
        if (sizeof($variables) > 0) {
            foreach ($variables as $key => $value) {
                $string = str_replace(':'.$key, $value, $string);
            }
        }
        return $string;
    }
}
