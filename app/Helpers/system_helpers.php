<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/2/18
 * Time: 13:49
 */

if (! function_exists('isTesting')) {
    /**
     * Check is session starts through UnitTest environment
     *
     * @return bool
     */
    function isTesting()
    {
        return env('APP_ENV') == 'testing';
    }
}

if (! function_exists('isNotTesting')) {
    /**
     * Check is session starts through UnitTest environment
     *
     * @return bool
     */
    function isNotTesting()
    {
        return false == isTesting();
    }
}
