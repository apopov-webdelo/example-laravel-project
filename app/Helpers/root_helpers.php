<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:38
 */

if (! function_exists('root')) {
    /**
     * @return \App\Models\Admin\Admin|\Illuminate\Database\Eloquent\Model
     */
    function root() : \App\Models\Admin\Admin
    {
        /** @var \App\Repositories\Admin\AdminRepo $repo */
        $repo = app(\App\Repositories\Admin\AdminRepo::class);
        return $repo->filterByLogin('root')->take()->first();
    }
}