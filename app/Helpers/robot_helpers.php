<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:38
 */

if (! function_exists('robot')) {
    /**
     * @return \App\Models\User\User
     */
    function robot() : \App\Models\User\User
    {
        return \App\Facades\Robot::user();
    }
}

if (! function_exists('bindRobot')) {
    /**
     * Bind RobotUser to AuthenticatedContract
     *
     * @param \Illuminate\Contracts\Container\Container|null $app
     *
     * @return \Illuminate\Contracts\Container\Container
     */
    function bindRobot($app = null) : \Illuminate\Contracts\Container\Container
    {
        return \App\Utils\User\RobotBinding::bind($app);
    }
}
