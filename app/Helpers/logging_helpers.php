<?php

if (!function_exists('toJira')) {
    /**
     * Send message to jira
     *
     * @param string $message
     * @param array  $data (name => value (only scalar values))
     */
    function toJira(string $message, array $data = [])
    {
        \App\Facades\EmergencyAlert::channel('jira')->send($message, $data);
    }
}

if (!function_exists('toSlack')) {
    /**
     * Send message to slack
     *
     * @param string $message
     * @param array  $data (name => value (only scalar values))
     */
    function toSlack(string $message, array $data = [])
    {
        \App\Facades\EmergencyAlert::channel('slack')->send($message, $data);
    }
}

if (!function_exists('toTelegram')) {
    /**
     * Send message to Telegram
     *
     * @param string $message
     * @param array  $data (name => value (only scalar values))
     */
    function toTelegram(string $message, array $data = [])
    {
        \App\Facades\EmergencyAlert::channel('telegram_emergency')->send($message, $data);
    }
}

if (!function_exists('toTelegramDeal')) {
    /**
     * Send message to Telegram
     *
     * @param string $message
     * @param array  $data (name => value (only scalar values))
     */
    function toTelegramDeal(string $message, array $data = [])
    {
        dispatch(
            (new \App\Jobs\Utils\LogToChannel('telegram_deals', $message, $data))->setLevel('info')
        );
    }
}
