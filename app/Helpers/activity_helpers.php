<?php
/**
 * Created by PhpStorm.
 * User: d.cercel
 * Date: 11/23/18
 * Time: 09:20
 */

if (! function_exists('activity')) {
    /**
     * @param string|null $message
     * @param string|null $type
     *
     * @return mixed
     */
    function activity(string $message = null, string $type = null)
    {
        if ($message) {
            return \App\Facades\ActivityLog::info(
                $message,
                $type
            );
        } else {
            return \App\Facades\ActivityLog::guard('api');
        }
    }
}

if (! function_exists('activity_admin')) {
    /**
     * @param string|null $message
     * @param string|null $type
     *
     * @return mixed
     */
    function activity_admin(string $message = null, string $type = null)
    {
        if ($message) {
            return \App\Facades\ActivityLog::guard('api-admin')->info(
                $message,
                $type
            );
        } else {
            return \App\Facades\ActivityLog::guard('api-admin');
        }
    }
}