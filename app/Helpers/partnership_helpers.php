<?php

if (! function_exists('is_partner')) {
    /**
     * Check is object instance of Partner contract
     *
     * @param object $object
     *
     * @return bool
     */
    function is_partner($object): bool
    {
        return $object instanceof \App\Contracts\Partnership\PartnerContract;
    }
}

if (! function_exists('is_user_partner')) {
    /**
     * Check is user partner in partnership program
     *
     * @param \App\Models\User\User $user
     *
     * @return bool
     */
    function is_user_partner(\App\Models\User\User $user): bool
    {
        /** @var \App\Contracts\Partnership\PartnerRepoContract $repo */
        $repo = app(\App\Contracts\Partnership\PartnerRepoContract::class);
        return $repo->filterByUser($user)->take()->exists();
    }
}

if (! function_exists('is_referral')) {
    /**
     * Check is object instance of Referral contract
     *
     * @param object $object
     *
     * @return bool
     */
    function is_referral($object): bool
    {
        return $object instanceof \App\Contracts\Partnership\ReferralContract;
    }
}

if (! function_exists('is_user_referral')) {
    /**
     * Check is user referral in partnership program
     *
     * @param \App\Models\User\User $user
     *
     * @return bool
     */
    function is_user_referral(\App\Models\User\User $user): bool
    {
        /** @var \App\Contracts\Partnership\ReferralRepoContract $repo */
        $repo = app(\App\Contracts\Partnership\ReferralRepoContract::class);
        return $repo->filterByUser($user)->take()->exists();
    }
}

if (! function_exists('get_referral')) {
    /**
     * Retrieve instance referral object by User model
     *
     * @param \App\Models\User\User $user
     *
     * @return \App\Contracts\Partnership\ReferralContract
     * @throws \App\Exceptions\Partnership\NotFoundReferralPartnershipException
     */
    function get_referral(\App\Models\User\User $user): \App\Contracts\Partnership\ReferralContract
    {
        /** @var \App\Contracts\Partnership\ReferralRepoContract $repo */
        $repo = app(\App\Contracts\Partnership\ReferralRepoContract::class);
        $referral = $repo->filterByUser($user)->take()->first();
        if ($referral instanceof \App\Contracts\Partnership\ReferralContract) {
            return $referral;
        }

        throw new \App\Exceptions\Partnership\NotFoundReferralPartnershipException(
            'User with ID='.$user->id.' is not referral!'
        );
    }
}

if (! function_exists('get_partner')) {
    /**
     * Retrieve partner object by User model
     * @param \App\Models\User\User $user
     *
     * @return \App\Contracts\Partnership\PartnerContract
     * @throws \App\Exceptions\Partnership\NotFoundPartnerPartnershipException
     */
    function get_partner(\App\Models\User\User $user): \App\Contracts\Partnership\PartnerContract
    {
        /** @var \App\Contracts\Partnership\PartnerRepoContract $repo */
        $repo = app(\App\Contracts\Partnership\PartnerRepoContract::class);
        $partner = $repo->filterByUser($user)->take()->first();
        if ($partner instanceof \App\Contracts\Partnership\PartnerContract) {
            return $partner;
        }

        throw new \App\Exceptions\Partnership\NotFoundPartnerPartnershipException(
            'User with ID='.$user->id.' is not partner!'
        );
    }
}
