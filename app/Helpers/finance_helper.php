<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/14/18
 * Time: 16:49
 */

if (!function_exists('currencyFromCoins')) {
    /**
     * Convert amount from coins to float value according currency's accuracy
     *
     * @param int                                      $amount
     * @param \App\Contracts\Currency\CurrencyContract $currency
     * @param bool                                     $stringFormat Returned value will be converted to string type
     *
     * @return float|string
     */
    function currencyFromCoins(
        int $amount,
        \App\Contracts\Currency\CurrencyContract $currency,
        bool $stringFormat = true
    ) {
        return \App\Utils\CurrencyConvertor::fromCoins($amount, $currency, $stringFormat);
    }
}

if (!function_exists('currencyToCoins')) {
    /**
     * Convert currency from float to coins value according currency's accuracy
     *
     * @param float                                    $amount
     * @param \App\Contracts\Currency\CurrencyContract $currency
     *
     * @return int
     */
    function currencyToCoins(float $amount, \App\Contracts\Currency\CurrencyContract $currency)
    {
        return \App\Utils\CurrencyConvertor::toCoins($amount, $currency);
    }
}

if (!function_exists('floatToString')) {
    /**
     * Converts float number to string with correct decimals
     *
     * @param float $amount
     * @param int   $accuracy
     *
     * @return int
     */
    function floatToString(float $amount, int $accuracy)
    {
        return rtrim(number_format($amount, $accuracy), '.0');
    }
}
