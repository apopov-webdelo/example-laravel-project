<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:38
 */

if (! function_exists('not_in_array')) {
    /**
     * Check is element is not in array
     *
     * That function return reverse value for function in_array
     *
     * @param $needle
     * @param array $haystack
     * @param bool $strict
     * @return bool
     */
    function not_in_array($needle, array $haystack, $strict = false) : bool
    {
        return !in_array($needle, $haystack, $strict);
    }
}