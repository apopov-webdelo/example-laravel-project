<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/14/18
 * Time: 16:49
 */

if (! function_exists('isValidTimezone')) {
    /**
     * Check if a string is a valid timezone
     *
     * timezone_identifiers_list() requires PHP >= 5.2
     *
     * @param string $timezone
     * @return bool
     */
    function isValidTimezone(string $timezone)
    {
        return in_array($timezone, timezone_identifiers_list());
    }
}
