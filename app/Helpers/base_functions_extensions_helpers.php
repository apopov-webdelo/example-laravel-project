<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:38
 */

if (! function_exists('not_isset')) {
    /**
     * Check is variable not isset. Analog call to !isset()
     *
     * @param $var
     * @return bool
     */
    function not_isset($var) : bool
    {
        return !isset($var);
    }
}