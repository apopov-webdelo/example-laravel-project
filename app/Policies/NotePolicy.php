<?php

namespace App\Policies;

use App\Models\Ad\Ad;
use App\Models\User\Note;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AdPolicy
 * @package App\Policies
 */
class NotePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Note $note
     *
     * @return mixed
     */
    public function destroy(User $user, Note $note)
    {
        return $note->isAuthor($user);
    }
}
