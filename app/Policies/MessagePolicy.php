<?php

namespace App\Policies;

use App\Models\Chat\Chat;
use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Utils\User\Blacklist;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * @var Blacklist
     */
    protected $blacklist;

    /**
     * MessagePolicy constructor.
     *
     * @param Blacklist $blacklist
     */
    public function __construct(Blacklist $blacklist)
    {
        $this->blacklist = $blacklist;
    }

    /**
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function index(User $user, Chat $chat)
    {
        return $chat->isUserChatMember($user);
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     */
    public function storeToDeal(User $user, Deal $deal)
    {
        return $deal->isUserDealMember($user);
    }

    /**
     * @param User $user
     * @param User $recipient
     * @return bool
     */
    public function storeToUser(User $user, User $recipient)
    {
        return $this->blacklist->isClean($user, $recipient);
    }

    /**
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function storeToChat(User $user, Chat $chat)
    {
        //TODO: Complete final logic
        return true;
    }
}
