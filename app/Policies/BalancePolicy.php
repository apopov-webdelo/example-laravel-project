<?php

namespace App\Policies;

use App\Contracts\AuthenticatedContract;
use App\Models\Balance\Balance;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class BalancePolicy
 *
 * @package App\Policies
 */
class BalancePolicy
{
    use HandlesAuthorization;

    /**
     * @param AuthenticatedContract $user
     * @param Balance               $balance
     *
     * @return mixed
     */
    public function wallet(AuthenticatedContract $user, Balance $balance)
    {
        return $user->id === $balance->user_id;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function createEmergencyWallet(AuthenticatedContract $user)
    {
        return $user->isBlocked() === false;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function addEmergencyWallet(AuthenticatedContract $user)
    {
        return $user->isBlocked() === false;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function emergencyWithdraw(AuthenticatedContract $user)
    {
        return $user->isBlocked() === false;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function withdraw(AuthenticatedContract $user)
    {
        return $user->isBlocked() === false;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function amount(AuthenticatedContract $user)
    {
        return $user->isBlocked() === false;
    }

    /**
     * @param AuthenticatedContract $user
     * @param Balance               $balance
     *
     * @return bool
     */
    public function transactions(AuthenticatedContract $user, Balance $balance)
    {
        return $user->id == $balance->user_id;
    }
}
