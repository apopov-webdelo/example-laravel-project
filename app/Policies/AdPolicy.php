<?php

namespace App\Policies;

use App\Models\Ad\Ad;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AdPolicy
 * @package App\Policies
 */
class AdPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     *
     * @return bool
     */
    public function store(User $user)
    {
        return $user->isTradeAvailable();
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function update(User $user, Ad $ad)
    {
        return $user->isTradeAvailable() && $ad->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function deals(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function destroy(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function details(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function deactivate(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function activate(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    public function statistics(User $user, Ad $ad)
    {
        return $ad->isAuthor($user);
    }
}
