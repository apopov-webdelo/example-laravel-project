<?php

namespace App\Policies;

use App\Models\Deal\Deal;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DealPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->isTradeAvailable();
    }

    /**
     * @param User $user
     * @param Deal $deal
     *
     * @return bool
     */
    public function storeReview(User $user, Deal $deal)
    {
        return $deal->isUserDealMember($user);
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     */
    public function details(User $user, Deal $deal)
    {
        return $deal->isUserDealMember($user);
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     */
    public function pay(User $user, Deal $deal)
    {
        return $user->id == $deal->getBuyer()->id;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     */
    public function dispute(User $user, Deal $deal)
    {
        return $deal->isUserDealMember($user);
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     */
    public function finishing(User $user, Deal $deal)
    {
        return $user->id == $deal->getSeller()->id;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     */
    public function cancellation(User $user, Deal $deal)
    {
        return $user->id == $deal->getBuyer()->id;
    }
}
