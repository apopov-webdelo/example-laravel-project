<?php

namespace App\Policies;

use App\Models\User\Review;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Review $review
     * @return bool
     */
    public function update(User $user, Review $review)
    {
        return $review->isAuthor($user);
    }

    /**
     * @param User $user
     * @param Review $review
     * @return bool
     */
    public function destroy(User $user, Review $review)
    {
        return $review->isAuthor($user);
    }
}
