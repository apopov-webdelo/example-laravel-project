<?php

namespace App\Notifications\User;

use App\Models\Chat\Chat;
use App\Notifications\Traits\Lang;
use App\Notifications\Traits\Recipients;
use App\Services\SmsAero\SmsAeroChannel;
use App\Services\SmsAero\SmsAeroMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

/**
 * Class ChatPlacedNotification
 *
 * @package App\Notifications\User
 */
class ChatPlacedNotification extends Notification
{
    use Queueable, Recipients, Lang;

    /**
     * ChatPlacedNotification constructor.
     *
     * @param Chat $chat
     */
    public function __construct(Chat $chat)
    {
        $this->setChat($chat)->setLangRoot('notifications.chat.placed');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) : array
    {
        $via = collect([]);
        if ($notifiable->settings->email_notification_required
            && $notifiable->security->email_confirmed
        ) {
            $via->push('mail');
        }
        if ($notifiable->settings->phone_notification_required
            && $notifiable->phone
            && $notifiable->security->phone_confirmed
        ) {
            $via->push(SmsAeroChannel::class);
        }
        if ($notifiable->settings->telegram_notification_required && $notifiable->telegram_id) {
            $via->push(TelegramChannel::class);
        }
        if ($notifiable->settings->push_notification_required && $notifiable->one_signal_player_id) {
            $via->push(OneSignalChannel::class);
        }

        return $via->all();
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable) : MailMessage
    {
        return (new MailMessage)
            ->subject($this->subject([ 'recipientString' => $this->getRecipientsByReceiverString($notifiable) ]))
            ->line($this->content([ 'recipientString' => $this->getRecipientsByReceiverString($notifiable) ]))
            ->action($this->action(), url(replace(config('app.states.front.chat'), ['user_id' => $notifiable->id])))
            ->line($this->footer());
    }

    /**
     * @param $notifiable
     *
     * @return SmsAeroMessage
     * @throws \Exception
     */
    public function toSmsAero($notifiable) : SmsAeroMessage
    {
        return (new SmsAeroMessage())
            ->content($this->content([ 'recipientString' => $this->getRecipientsByReceiverString($notifiable) ]));
    }

    /**
     * @param $notifiable
     *
     * @return TelegramMessage
     * @throws \Exception
     */
    public function toTelegram($notifiable) : TelegramMessage
    {
        return (new TelegramMessage())
            ->content($this->content([ 'recipientString' => $this->getRecipientsByReceiverString($notifiable) ])) // Markdown supported.
            ->button($this->action(), url(replace(config('app.states.front.chat'), ['user_id' => $notifiable->id]))); // Inline Button
    }

    /**
     * @param $notifiable
     *
     * @return OneSignalMessage
     * @throws \Exception
     */
    public function toOneSignal($notifiable) : OneSignalMessage
    {
        $state = ($this->chat->deal->isEmpty())
            ? replace(config('app.states.front.chat'), ['user_id' => $notifiable->id])
            : replace(config('app.states.front.deal'), ['deal_id' => $this->chat->deal->first()->id]);

        return OneSignalMessage::create()
            ->subject($this->content([ 'recipientString' => $this->getRecipientsByReceiverString($notifiable) ]))
            ->body($this->action())
            ->url($state)
            ->webButton(
                OneSignalWebButton::create('link-1')
                    ->text($this->action())
                    ->url($state)
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
