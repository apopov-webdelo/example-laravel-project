<?php

namespace App\Notifications\Chat;

use App\Models\Chat\Message;
use App\Notifications\Traits\Lang;
use App\Notifications\Traits\Recipients;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class ChatMessagePlacedToRecipientNotification extends Notification
{
    use Queueable, Recipients, Lang;

    /**
     * @var Message
     */
    private $message;


    /**
     * ChatMessagePlacedToRecipientNotification constructor.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->setMessage($message)
             ->setChat($message->chat)
             ->setLangRoot('notifications.chat.message-placed.recipient');
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    private function setMessage(Message $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = collect([]);
        if ($notifiable->settings->push_notification_required && $notifiable->settings->message_notification_required) {
            $via->push(OneSignalChannel::class);
        }

        return $via->all();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return OneSignalMessage
     * @throws \Exception
     */
    public function toOneSignal($notifiable)
    {
        $state = ($this->message->chat->deal->isEmpty())
            ? replace(config('app.states.front.chat'), ['user_id' => $notifiable->id])
            : replace(config('app.states.front.deal'), ['deal_id' => $this->message->chat->deal->first()->id]);

        return OneSignalMessage::create()
            ->subject($this->subject([ 'login' => $this->message->author->login ]))
            ->body($this->action())
            ->url($state)
            ->webButton(
                OneSignalWebButton::create('link-1')
                    ->text($this->action())
                    ->url($state)
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
