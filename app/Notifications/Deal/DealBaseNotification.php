<?php

namespace App\Notifications\Deal;

use App\Models\Deal\Deal;
use App\Notifications\Traits\Lang;
use App\Services\SmsAero\SmsAeroChannel;
use App\Services\SmsAero\SmsAeroMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

abstract class DealBaseNotification extends Notification
{
    use Queueable, Lang;

    /**
     * @var Deal $deal
     */
    protected $deal;

    /**
     * @var string $stateUrl
     */
    protected $stateUrl;

    /**
     * DealPlacedToAdAuthorNotification constructor.
     *
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal     = $deal;
        $this->stateUrl = replace(config('app.states.front.deal'), ['deal_id' => $this->deal->id]);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = collect([]);
        if ($notifiable->settings->email_notification_required
            && $notifiable->security->email_confirmed
        ) {
            $via->push('mail');
        }
        if ($notifiable->settings->phone_notification_required
            && $notifiable->phone
            && $notifiable->security->phone_confirmed
        ) {
            $via->push(SmsAeroChannel::class);
        }
        if ($notifiable->settings->telegram_notification_required && $notifiable->telegram_id) {
            $via->push(TelegramChannel::class);
        }
        if ($notifiable->settings->push_notification_required && $notifiable->one_signal_player_id) {
            $via->push(OneSignalChannel::class);
        }

        return $via->all();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->subject([ 'dealId' => $this->deal->id ]))
            ->line($this->content([ 'dealId' => $this->deal->id, 'adId' => $this->deal->ad->id ]))
            ->action($this->action(), $this->stateUrl)
            ->line($this->footer());
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SmsAeroMessage
     * @throws \Exception
     */
    public function toSmsAero($notifiable)
    {
        return (new SmsAeroMessage())
            ->content(
                $this->content([
                    'dealId' => $this->deal->id,
                    'adId' => $this->deal->ad->id
                ])
            );
    }

    /**
     * Get the Telegram representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TelegramMessage
     * @throws \Exception
     */
    public function toTelegram($notifiable)
    {
        return (new TelegramMessage())
            ->content(
                $this->content([
                    'dealId' => $this->deal->id,
                    'adId' => $this->deal->ad->id
                ])
            )
            ->button($this->action(), $this->stateUrl);
    }

    /**
     * Get the Push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return OneSignalMessage
     * @throws \Exception
     */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->subject($this->content([ 'dealId' => $this->deal->id, 'adId' => $this->deal->ad->id ]))
            ->body($this->action())
            ->url($this->stateUrl)
            ->webButton(
                OneSignalWebButton::create('link-1')
                    ->text($this->action())
                    ->url($this->stateUrl)
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     * @throws \Exception
     */
    public function toArray($notifiable)
    {
        return [
            'subject' => $this->subject([ 'dealId' => $this->deal->id ]),
            'content' => $this->content([ 'dealId' => $this->deal->id, 'adId' => $this->deal->ad->id ]),
            'action'  => $this->action(),
            'footer'  => $this->footer(),
        ];
    }
}
