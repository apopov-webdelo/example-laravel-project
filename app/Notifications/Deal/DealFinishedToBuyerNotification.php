<?php

namespace App\Notifications\Deal;

use App\Models\Deal\Deal;
use App\Notifications\Traits\Lang;
use App\Services\SmsAero\SmsAeroChannel;
use App\Services\SmsAero\SmsAeroMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class DealFinishedToBuyerNotification extends DealBaseNotification
{
    use Queueable;

    /**
     * DealPlacedToAdAuthorNotification constructor.
     *
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        parent::__construct($deal);
        $this->setLangRoot('notifications.deal.finished.buyer');
    }
}
