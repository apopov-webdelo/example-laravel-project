<?php

namespace App\Notifications\Deal;

use App\Models\Deal\Deal;
use App\Notifications\Traits\Lang;
use App\Services\SmsAero\SmsAeroChannel;
use App\Services\SmsAero\SmsAeroMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class DealPlacedToAdAuthorNotification extends DealBaseNotification
{
    use Queueable;

    /**
     * DealPlacedToAdAuthorNotification constructor.
     *
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        parent::__construct($deal);
        $this->setLangRoot('notifications.deal.placed.ad_author');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->subject())
                    ->line($this->content([ 'adId' => $this->deal->ad->id ]))
                    ->action($this->action(), $this->stateUrl)
                    ->line($this->footer());
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SmsAeroMessage
     * @throws \Exception
     */
    public function toSmsAero($notifiable)
    {
        return (new SmsAeroMessage())->content($this->content([ 'adId' => $this->deal->ad->id ]));
    }

    /**
     * Get the Telegram representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TelegramMessage
     * @throws \Exception
     */
    public function toTelegram($notifiable)
    {
        return (new TelegramMessage())
                    ->content($this->content([ 'adId' => $this->deal->ad->id ])) // Markdown supported.
                    ->button($this->action(), $this->stateUrl); // Inline Button
    }

    /**
     * Get the Push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return OneSignalMessage
     * @throws \Exception
     */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->subject($this->content([ 'adId' => $this->deal->ad->id ]))
            ->body($this->action())
            ->url($this->stateUrl)
            ->webButton(
                OneSignalWebButton::create('link-1')
                    ->text($this->action())
                    ->url($this->stateUrl)
            );
    }
}
