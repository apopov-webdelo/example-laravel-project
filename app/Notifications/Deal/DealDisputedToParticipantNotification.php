<?php

namespace App\Notifications\Deal;

use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Notifications\Traits\Lang;
use App\Services\SmsAero\SmsAeroChannel;
use App\Services\SmsAero\SmsAeroMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class DealDisputedToParticipantNotification extends DealBaseNotification
{
    use Queueable;

    /**
     * @var User $user
     */
    public $user;

    /**
     * DealDisputedToParticipantNotification constructor.
     *
     * @param Deal $deal
     * @param User $user
     */
    public function __construct(Deal $deal, User $user)
    {
        parent::__construct($deal);
        $this->setLangRoot('notifications.deal.disputed.participant');
        $this->user = $user;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->subject([ 'dealId' => $this->deal->id ]))
            ->line($this->content([ 'dealId' => $this->deal->id, 'login' => $this->user->login ]))
            ->action($this->action(), $this->stateUrl)
            ->line($this->footer());
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SmsAeroMessage
     * @throws \Exception
     */
    public function toSmsAero($notifiable)
    {
        return (new SmsAeroMessage())->content(
            $this->content([ 'dealId' => $this->deal->id, 'login' => $this->user->login ])
        );
    }

    /**
     * Get the Telegram representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TelegramMessage
     * @throws \Exception
     */
    public function toTelegram($notifiable)
    {
        return (new TelegramMessage())
            ->content($this->content(
                [ 'dealId' => $this->deal->id, 'login' => $this->user->login ]
            )) // Markdown supported.
            ->button($this->action(), url(config('app.url'))); // Inline Button
    }

    /**
     * Get the Push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return OneSignalMessage
     * @throws \Exception
     */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->subject($this->content([ 'dealId' => $this->deal->id, 'login' => $this->user->login ]))
            ->body($this->action())
            ->url($this->stateUrl)
            ->webButton(
                OneSignalWebButton::create('link-1')
                    ->text($this->action())
                    ->url($this->stateUrl)
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     * @throws \Exception
     */
    public function toArray($notifiable)
    {
        return [
            'subject' => $this->subject([ 'dealId' => $this->deal->id ]),
            'content' => $this->content([ 'dealId' => $this->deal->id, 'login' => $this->user->login ]),
            'action'  => $this->action(),
            'footer'  => $this->footer(),
        ];
    }
}
