<?php

namespace App\Notifications\Support;

use App\Http\Requests\Support\ApiAccessRequest;
use App\Notifications\Traits\Lang;
use BotMan\BotMan\Interfaces\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class ApiAccessRequestNotification
 *
 * @package  App\Notifications\Support
 */
class ApiAccessRequestNotification extends Notification implements ShouldQueue
{
    use Queueable, Lang;

    /**
     * @var ApiAccessRequest
     */
    protected $request;

    /**
     * ApiAccessRequestNotification constructor.
     *
     * @param ApiAccessRequest $request
     */
    public function __construct(ApiAccessRequest $request)
    {
        $this->request = $request;
        $this->setLangRoot('notifications.support.api_access_request');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->subject())
            ->line($this->content(['name' => $this->request->name]))
            ->line($this->description(['text' => $this->request->text]))
            ->line($this->footer([
                'email' => $this->request->email,
                'login' => $this->request->login,
            ]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->request->all();
    }
}
