<?php

namespace App\Notifications\Partnership;

use App\Models\Partnership\Partner;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

class PartnerRegistrationNotification extends PartnerBaseNotification implements ShouldQueue
{
    use Queueable, Lang;

    /**
     * Create a new notification instance.
     *
     * @param Partner $partner
     *
     * @return void
     */
    public function __construct(Partner $partner)
    {
        parent::__construct($partner);
        $this->setLangRoot('notifications.partnership.partner-register');
    }
}
