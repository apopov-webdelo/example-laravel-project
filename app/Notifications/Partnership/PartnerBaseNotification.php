<?php

namespace App\Notifications\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Models\Partnership\Partner;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PartnerBaseNotification extends Notification implements ShouldQueue
{
    use Queueable, Lang;

    /**
     * @var Partner $partner
     */
    protected $partner;

    /**
     * Create a new notification instance.
     *
     * @param PartnerContract $partner
     *
     * @return void
     */
    public function __construct(PartnerContract $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->subject())
                    ->line($this->content())
                    ->line($this->footer());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
