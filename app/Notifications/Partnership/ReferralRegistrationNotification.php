<?php

namespace App\Notifications\Partnership;

use App\Models\Partnership\Referral;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReferralRegistrationNotification extends PartnerBaseNotification implements ShouldQueue
{
    use Queueable, Lang;

    /**
     * @var Referral $referral
     */
    private $referral;

    /**
     * Create a new notification instance.
     *
     * @param Referral $referral
     *
     * @return void
     */
    public function __construct(Referral $referral)
    {
        parent::__construct($referral->getPartner());
        $this->setLangRoot('notifications.partnership.referral-register');
        $this->referral = $referral;
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject($this->subject())
            ->line($this->content(['referral_id' => $this->referral->id]))
            ->line($this->footer());
    }
}
