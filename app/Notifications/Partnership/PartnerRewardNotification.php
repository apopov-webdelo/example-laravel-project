<?php

namespace App\Notifications\Partnership;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Models\Partnership\Partner;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class PartnerRewardNotification extends PartnerBaseNotification
{
    use Queueable, Lang;

    /**
     * @var PartnershipCommissionStorageContract $commission
     */
    private $commissionStorage;

    /**
     * Create a new notification instance.
     *
     * @param PartnershipCommissionStorageContract $commissionStorage
     *
     * @return void
     */
    public function __construct(PartnershipCommissionStorageContract $commissionStorage)
    {
        parent::__construct($commissionStorage->partner());
        $this->setLangRoot('notifications.partnership.partner-award');
        $this->commissionStorage = $commissionStorage;
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        $crypto = $this->commissionStorage->cryptoCurrency();
        $amount = currencyFromCoins($this->commissionStorage->amount(), $crypto);

        return (new MailMessage)
            ->line($this->subject())
            ->line($this->content([
                'commission'     => $amount,
                'commissionType' => mb_strtoupper($crypto->getCode()),
            ]))
            ->line($this->footer());
    }
}
