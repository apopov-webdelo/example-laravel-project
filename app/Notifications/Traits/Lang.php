<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/20/18
 * Time: 11:08
 */
namespace App\Notifications\Traits;


trait Lang
{
    /**
     * @var string $langRoot
     */
    private $langRoot = '';

    /**
     * @param string $langRoot
     *
     * @return $this
     */
    protected function setLangRoot(string $langRoot)
    {
        $this->langRoot = $langRoot;
        return $this;
    }

    /**
     * @param array $params
     *
     * @return array|null|string
     * @throws \Exception
     */
    public function subject(array $params = [])
    {
        $this->isLangRootSettled();
        return __($this->langRoot.'.subject', $params);
    }

    /**
     * @param array $params
     *
     * @return array|null|string
     * @throws \Exception
     */
    public function content(array $params = [])
    {
        $this->isLangRootSettled();
        return __($this->langRoot.'.content', $params);
    }

    /**
     * @param array $params
     *
     * @return array|null|string
     * @throws \Exception
     */
    public function description(array $params = [])
    {
        $this->isLangRootSettled();
        return __($this->langRoot.'.description', $params);
    }

    /**
     * @param array $params
     *
     * @return array|null|string
     * @throws \Exception
     */
    public function action(array $params = [])
    {
        $this->isLangRootSettled();
        return __($this->langRoot.'.action', $params);
    }

    /**
     * @param array $params
     *
     * @return array|null|string
     * @throws \Exception
     */
    public function footer(array $params = [])
    {
        $this->isLangRootSettled();
        return __($this->langRoot.'.footer', $params);
    }

    /**
     * @throws \Exception
     */
    private function isLangRootSettled()
    {
        if (! $this->langRoot) {
            throw new \Exception('You should define $langRoot property by setLangRoot(string $langRoot) before using trait Lang ');
        }
    }
}