<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/7/18
 * Time: 11:37
 */

namespace App\Notifications\Traits;
use App\Models\Chat\Chat;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait Recipients
 *
 * @package App\Notifications\Traits
 */
trait Recipients
{
    /**
     * @var string
     */
    private $recipientString = '';

    /**
     * @var Chat
     */
    private $chat;

    /**
     * @param User $user
     *
     * @return string
     * @throws \Exception
     */
    private function getRecipientsByReceiverString(User $user) : string
    {
        if (!$this->recipientString) {
            $this->recipientString = $this->getRecipientsByReceiver($user)->count() > 1
                ? substr($this->getRecipientsByReceiver($user)->pluck('login')->implode(','), 0, -1)
                : $this->getRecipientsByReceiver($user)->pluck('login')->implode(',');
        }

        return $this->recipientString;
    }

    /**
     * @param User $user
     *
     * @return Collection
     * @throws \Exception
     */
    private function getRecipientsByReceiver(User $user) : Collection
    {
        if (!$this->chat) {
            throw new \Exception('For Recipients trait required to set chat by setChat(Chat $chat) method');
        }

        return $this->chat->recipients->where('id', '!=', $user->id);
    }

    /**
     * @param Chat $chat
     *
     * @return $this
     */
    private function setChat(Chat $chat)
    {
        $this->chat = $chat;

        return $this;
    }
}