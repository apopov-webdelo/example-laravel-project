<?php

namespace App\Notifications\User;

use App\Contracts\GeoLocatorContract;
use App\Events\User\LoginError;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LoginErrorNotification extends Notification
{
    use Queueable, Lang;
    /**
     * @var LoginError
     */
    protected $event;

    /**
     * Create a new notification instance.
     *
     * @param LoginError $event
     */
    public function __construct(LoginError $event)
    {
        $this->setLangRoot('notifications.user.login-error');
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->security->email_confirmed ? ['mail'] : [];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        $locator = app(GeoLocatorContract::class);

        return (new MailMessage)
            ->subject($this->subject())
            ->line($this->content(['login' => $notifiable->login]))
            ->line($this->action([
                'time'    => now(),
                'IP'      => $this->event->sessionStorage->ip(),
                'country' => $locator->getCountry($this->event->sessionStorage->ip()),
                'city'    => $locator->getCity($this->event->sessionStorage->ip()),
            ]))
            ->line($this->footer());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
