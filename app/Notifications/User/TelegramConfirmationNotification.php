<?php

namespace App\Notifications\User;

use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramConfirmationNotification extends Notification
{
    use Queueable, Lang;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setLangRoot('notifications.user.telegram-confirmation');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    /**
     * @param $notifiable
     *
     * @return TelegramMessage
     * @throws \Exception
     */
    public function toTelegram($notifiable) : TelegramMessage
    {
        return (new TelegramMessage())->content($this->content([ 'confirmToken' => $notifiable->security->telegram_id_confirm_token ])); // Markdown supported.
    }
}
