<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class PasswordResetedNotification
 *
 * @package App\Notifications\User
 */
class PasswordResetedNotification extends Notification
{
    use Queueable;

    /**
     * @var string
     */
    private $reason;
    /**
     * @var string
     */
    private $password;

    /**
     * PasswordResetedNotification constructor.
     *
     * @param string $reason
     * @param string $password
     */
    public function __construct(string $reason, string $password)
    {
        $this->reason = $reason;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('passwords.password_updated_admin.subject'))
            ->line(__('passwords.password_updated_admin.subject'))
            ->line(__('passwords.password_updated_admin.reason') . $this->reason)
            ->line(__('passwords.password_updated_admin.new_password') . $this->password);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
