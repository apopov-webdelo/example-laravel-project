<?php

namespace App\Notifications\User;

use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegistrationNotification extends Notification
{
    use Queueable, Lang;

    /**
     * RegistrationNotification constructor.
     */
    public function __construct()
    {
        $this->setLangRoot('notifications.user.registration-message');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->subject())
                    ->line($this->content())
                    ->line($this->description())
                    ->action(
                        $this->action(),
                        config('app.email_confirm_state').$notifiable->security->email_confirm_token
                    );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
