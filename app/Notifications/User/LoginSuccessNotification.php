<?php

namespace App\Notifications\User;

use App\Contracts\GeoLocatorContract;
use App\Events\User\LoginSuccess;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LoginSuccessNotification extends Notification
{
    use Queueable, Lang;

    /**
     * @var LoginSuccess $event
     */
    public $event;

    /**
     * LoginSuccessNotification constructor.
     *
     * @param LoginSuccess $event
     */
    public function __construct(LoginSuccess $event)
    {
        $this->event = $event;
        $this->setLangRoot('notifications.user.login-success');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->security->email_confirmed ? ['mail'] : [];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        $locator = app(GeoLocatorContract::class);

        return (new MailMessage)
                    ->subject($this->subject())
                    ->line($this->content([ 'login' => $notifiable->login ]))
                    ->line($this->action([
                        'time'    => now(),
                        'country' => $locator->getCountry($this->event->sessionStorage->ip()),
                        'city'    => $locator->getCity($this->event->sessionStorage->ip()),
                    ]))
                    ->line($this->footer());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $notifiable->toArray();
    }
}
