<?php

namespace App\Notifications\User;

use App\Notifications\Traits\Lang;
use App\Services\SmsAero\SmsAeroChannel;
use App\Services\SmsAero\SmsAeroMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class PhoneConfirmationNotification extends Notification
{
    use Queueable, Lang;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setLangRoot('notifications.user.phone-access');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsAeroChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SmsAeroMessage
     * @throws \Exception
     */
    public function toSmsAero($notifiable)
    {
        return (new SmsAeroMessage)->content($this->content([ 'confirmToken' => $notifiable->security->phone_confirm_token ]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
