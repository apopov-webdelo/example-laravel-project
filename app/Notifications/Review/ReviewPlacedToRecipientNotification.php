<?php

namespace App\Notifications\User;

use App\Models\User\Review;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReviewPlacedToRecipientNotification extends Notification
{
    use Queueable, Lang;

    /**
     * @var Review $review
     */
    private $review;

    /**
     * ReviewPlacedToRecipientNotification constructor.
     *
     * @param Review $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
        $this->setLangRoot('notifications.user.review-placed');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->security->email_confirmed ? ['mail'] : [];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->subject())
                    ->line($this->content([ 'author' => $this->review->author->login ]))
                    ->line($this->action(), replace(config('app.states.front.reviews'), ['user_id' => $notifiable->id]))
                    ->line($this->footer());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
