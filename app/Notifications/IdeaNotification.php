<?php

namespace App\Notifications;

use App\Contracts\Services\Idea\IdeaStorageContract;
use App\Notifications\Traits\Lang;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class IdeaNotification
 *
 * @package App\Notifications
 * @resource Idea
 */
class IdeaNotification extends Notification
{
    use Queueable, Lang;

    /**
     * @var IdeaStorageContract $storage
     */
    private $storage;

    /**
     * IdeaNotification constructor.
     *
     * @param IdeaStorageContract $storage
     */
    public function __construct(IdeaStorageContract $storage)
    {
        $this->storage = $storage;
        $this->setLangRoot('notifications.idea');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     *
     * @return MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->subject())
                    ->line($this->content(['name' => $this->storage->getName()]))
                    ->line($this->description(['text'  => $this->storage->getText()]))
                    ->line($this->footer([
                        'email' => $this->storage->getEmail(),
                        'login' => $this->storage->getLogin(),
                    ]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
