<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;

class StartConversation extends Conversation
{
    protected $firstname;

    protected $email;

    public function askFirstname()
    {
        $this->ask('Hello! What is your firstname?', function(Answer $answer) {
            $this->firstname = $answer->getText();

            $this->say('Nice to meet you '.$this->firstname)
                 ->showCommands();
        });
    }

    public function showCommands()
    {
        $commandsString = '';
        collect(config('botman.telegram.commands'))->map(function ($title, $command) use (&$commandsString) {
            $commandsString .= $command . ' - '. $title . "\n";
        });

        $this->say("Here are available commands: \n". $commandsString);
    }

    public function run()
    {
        $this->askFirstname();
    }
}