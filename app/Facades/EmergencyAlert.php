<?php
namespace App\Facades;

use App\Contracts\Facade\EmergencyAlertFactoryContract;
use Illuminate\Support\Facades\Facade;

/**
 * Facade to send emergency logs to jira, slack, telegram etc.
 *
 * @method static EmergencyAlertFactoryContract channel($name)
 * @method static EmergencyAlertFactoryContract send(string $message, array $data = [])
 */
class EmergencyAlert extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return EmergencyAlertFactoryContract::class;
    }
}
