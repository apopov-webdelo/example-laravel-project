<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/13/18
 * Time: 11:50
 */

namespace App\Facades;

use App\Contracts\Exchange\Crypto\FactoryContract;
use App\Contracts\Exchange\Crypto\MarketContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @method static MarketContract market(string $driver = null)
 * @method static Collection currencies()
 * @method static Collection cryptoCurrencies()
 * @method static float rate($from, $to, bool $cache = true)
 * @method static MarketContract refresh($currency = null)
 * @method static float convert(float $sum, $from, $to, int $accuracy = 0, bool $cache = true)
 */
class CryptoExchange extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return FactoryContract::class;
    }
}
