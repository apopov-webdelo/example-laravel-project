<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:19
 */

namespace App\Facades;

use App\Contracts\GeoLocatorContract;
use Illuminate\Support\Facades\Facade;

/**
 * @method static string getIp()
 * @method static string getCountryISO(string $ip = null)
 * @method static string getCountryAlpha2(string $ip = null)
 * @method static string getCountryAlpha3(string $ip = null)
 * @method static string getCountry(string $ip = null)
 * @method static string getCity(string $ip = null)
 * @method static string getState(string $ip = null)
 * @method static string getStateCode(string $ip = null)
 * @method static string getPostalCode(string $ip = null)
 * @method static string getTimezone(string $ip = null)
 * @method static string getContinent(string $ip = null)
 * @method static string getCurrency(string $ip = null)
 * @method static bool   isDefault(string $ip = null)
 */
class GeoLocator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return GeoLocatorContract::class;
    }
}