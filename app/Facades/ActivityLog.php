<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/13/18
 * Time: 11:50
 */

namespace App\Facades;

use App\Contracts\ActivityLog\FactoryContract;
use Illuminate\Support\Facades\Facade;

/**
 * Facade to log users activity
 *
 * @method static guard(string $name)
 * @method static emergency(string $message, string $type)
 * @method static alert(string $message, string $type)
 * @method static critical(string $message, string $type)
 * @method static error(string $message, string $type)
 * @method static warning(string $message, string $type)
 * @method static notice(string $message, string $type)
 * @method static info(string $message, string $type)
 * @method static debug(string $message, string $type)
 */
class ActivityLog extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return FactoryContract::class;
    }
}