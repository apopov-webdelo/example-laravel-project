<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/13/18
 * Time: 11:50
 */

namespace App\Facades;

use App\Contracts\Exchange\Fiat\FiatFactoryContract;
use App\Contracts\Exchange\Fiat\ExchangeContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @method static ExchangeContract market(string $driver = null)
 * @method static Collection currencies()
 * @method static float rate(string $currencyCode)
 * @method static ExchangeContract refresh()
 * @method static float convert(float $sum, string $currencyFrom, string $currencyTo)
 */
class Exchange extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return FiatFactoryContract::class;
    }
}
