<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:19
 */

namespace App\Facades;

use App\Contracts\RobotFactoryContract;
use App\Models\User\User;
use Illuminate\Support\Facades\Facade;

/**
 * @method static User user()
 */
class Robot extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return RobotFactoryContract::class;
    }
}