<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/27/18
 * Time: 13:56
 */

namespace App\Utils;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Utils for convert JsonResources and arrays to JSON structure for test validation
 *
 * @package App\Utils
 */
class Json
{
    /**
     *
     * @var bool $isDataSettled
     */
    static private $isDataSettled = false;

    /**
     * Convert JsonResources and arrays to JSON structure for test validation
     *
     * @param JsonResource|array $resource
     *
     * @return array
     */
    public static function structure($resource) : array
    {
        self::$isDataSettled = false;
        if ($resource instanceof ResourceCollection) {
            return self::parseStructure($resource);
        }

        self::$isDataSettled = true;
        return ['data'=>self::parseStructure($resource)];
    }

    /**
     * @param $resource
     * @return array
     */
    protected static function parseStructure($resource) : array
    {
        $items = $resource instanceof JsonResource
            ? $resource->toArray([])
            : $resource;

        $structure = [];
        foreach ($items as $key => $item) {
            if ($item instanceof ResourceCollection) {
                $structure[self::getStructureKey($key)]['*'] = self::parseStructure($item->first());
            } elseif ($item instanceof JsonResource || is_array($item)) {
                $structure[$key] = self::parseStructure($item);
            } elseif (is_string($key)) {
                $structure[] = $key;
            }
        }

        return $structure;
    }

    private static function getStructureKey($key)
    {
        if (self::$isDataSettled) {
            return $key;
        }

        self::$isDataSettled = true;
        return 'data';
    }

    /**
     * Convert JsonResources and arrays to data array for JSON test validation
     *
     * @param JsonResource|array $resource
     *
     * @return array
     */
    public static function data($resource) : array
    {
        //TODO: Complete assert by json content
        return $resource instanceof ResourceCollection
            ? self::parseData($resource)
            : ['data' => self::parseData($resource)];
    }

    /**
     * @param $resource
     * @return array
     */
    protected static function parseData($resource) : array
    {
        $items = ($resource instanceof JsonResource || $resource instanceof ResourceCollection)
            ? $resource->jsonSerialize()
            : $resource;

        foreach ($items as $key => $item) {
            if ($item instanceof JsonResource || is_array($item) || $item instanceof ResourceCollection) {
                $items[$key] = self::parseData($item);
            }
        }

        return $items;
    }
}
