<?php
namespace App\Utils\TorChecker;


interface INodeList
{

	function getIPs(): array;

}