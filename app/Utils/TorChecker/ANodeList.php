<?php
namespace App\Utils\TorChecker;


abstract class ANodeList implements INodeList
{

	/**
	 * @var array
	 */
	protected $list = [];



	/**
	 * @return array
	 */
	public function getIPs(): array
	{
		return $this->list;
	}

}