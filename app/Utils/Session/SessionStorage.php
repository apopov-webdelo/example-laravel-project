<?php

namespace App\Utils\Session;

use App\Contracts\GeoLocatorContract;
use App\Contracts\Utils\Session\SessionStorageContract;
use Jenssegers\Agent\Agent;

/**
 * Request info storage object
 *
 * Class SessionStorage
 *
 * @package App\Utils\Session
 */
class SessionStorage implements SessionStorageContract
{
    /**
     * @var string
     */
    protected $ip;
    /**
     * @var string
     */
    protected $os;
    /**
     * @var string
     */
    protected $browser;

    /**
     * SessionStorage constructor.
     *
     * @param GeoLocatorContract $locator
     * @param Agent              $agent
     */
    public function __construct(GeoLocatorContract $locator, Agent $agent)
    {
        $this->ip = $locator->getIp();
        $this->os = $agent->platform();
        $this->browser = $agent->browser();
    }

    /**
     * Client ip
     *
     * @return string
     */
    public function ip(): string
    {
        return $this->ip;
    }

    /**
     * Client OS
     *
     * @return string
     */
    public function os(): string
    {
        return $this->os;
    }

    /**
     * Client browser
     *
     * @return string
     */
    public function browser(): string
    {
        return $this->browser;
    }
}
