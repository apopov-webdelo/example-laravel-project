<?php

namespace App\Utils\Filters;

use App\Http\Resources\Client\Directory\PaymentSystem\PaymentSystemByCountryResource;
use App\Models\Directory\Country;
use App\Models\Directory\PaymentSystem;
use Illuminate\Support\Collection;

class PaymentSystemByCountryFilter
{
    /**
     * @var Collection
     */
    protected $paymentSystems;
    /**
     * @var Country
     */
    protected $country;

    public function __construct(Collection $paymentSystems, Country $country)
    {
        $this->paymentSystems = $paymentSystems;
        $this->country = $country;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke()
    {
        $payments = $this->paymentSystems->map(function (PaymentSystem $payment) {
            $payment->currenciesByCountry = $payment->currenciesByCountry($this->country->id)->get();

            return $payment;
        });

        return PaymentSystemByCountryResource::collection($payments);
    }
}
