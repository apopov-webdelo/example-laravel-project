<?php

namespace App\Utils\Loggers;

use App\Utils\Loggers\Handlers\JiraHandler;
use Monolog\Logger;

class JiraLogger
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $handler = app(JiraHandler::class);

        return new Logger('main', [$handler]);
    }
}