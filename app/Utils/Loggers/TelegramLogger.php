<?php

namespace App\Utils\Loggers;

use App\Utils\Loggers\Handlers\TelegramHandler;
use Monolog\Logger;

class TelegramLogger
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array $config
     *
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $handler = new TelegramHandler($config);

        $log = new Logger($config['name']);
        $log->pushHandler($handler);

        return $log;
    }
}