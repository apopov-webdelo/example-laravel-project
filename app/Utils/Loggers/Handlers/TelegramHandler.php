<?php

namespace App\Utils\Loggers\Handlers;

use App\Jobs\Utils\TelegramSendMessageJob;
use Monolog\Handler\AbstractProcessingHandler;

/**
 * Telegram Handler For Monolog
 */
class TelegramHandler extends AbstractProcessingHandler
{
    /**
     * @var array
     */
    protected $config;

    /**
     * getting token a channel name from Telegram Handler Object.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        parent::__construct();
    }

    /**
     * format the log to send
     *
     * @param $record [] log data
     *
     * @return void
     */
    public function write(array $record)
    {
        TelegramSendMessageJob::dispatch($this->config, $record);
    }
}
