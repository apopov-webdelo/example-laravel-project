<?php

namespace App\Utils\Loggers\Handlers;

use Illuminate\Support\Collection;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\Issue\IssueService;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class JiraHandler extends AbstractProcessingHandler
{
    /**
     * @var IssueField
     */
    protected $jiraIssue;

    /**
     * @var IssueService
     */
    protected $jiraService;

    /**
     * JiraHandler constructor.
     *
     * @param IssueField   $jiraIssue
     * @param IssueService $jiraService
     * @param int          $level
     * @param bool         $bubble
     */
    public function __construct(
        IssueField $jiraIssue,
        IssueService $jiraService,
        $level = Logger::DEBUG,
        $bubble = true
    ) {
        parent::__construct($level, $bubble);
        $this->jiraIssue = $jiraIssue;
        $this->jiraService = $jiraService;
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     *
     * @return void
     * @throws \JiraRestApi\JiraException
     * @throws \JsonMapper_Exception
     */
    protected function write(array $record)
    {
        $data = collect($record['context']);

        $this->jiraIssue->setProjectKey(config('app.jira.board'))
                        ->setPriorityName("Highest")
                        ->setIssueType("Bug")
                        ->setSummary($record['message']);

        if ($data->count()) {
            $this->jiraIssue->setDescription($this->formatDescription($data));
        }

        $this->jiraService->create($this->jiraIssue);
    }

    /**
     * @param Collection $data
     *
     * @return mixed
     */
    protected function formatDescription(Collection $data)
    {
        $data->each(function ($item, $key) use (&$description) {
            $description .= '|' . $key . '|' . $item . '| ';
        });

        return $description;
    }
}
