<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/4/18
 * Time: 17:31
 */

namespace App\Utils\Loggers;

use Monolog\Logger;
use Monolog\Handler\GelfHandler;
use Gelf\Publisher;
use Gelf\Transport\UdpTransport;

class GelfLogger
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $handler = new GelfHandler(new Publisher(new UdpTransport($config['host'], $config['port'])));
        return new Logger('main', [$handler]);
    }
}