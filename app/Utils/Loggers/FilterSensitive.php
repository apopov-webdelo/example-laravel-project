<?php

namespace App\Utils\Loggers;

use Psr\Http\Message\StreamInterface;

/**
 * Class FilterSensitive
 *
 * @package App\Utils\Loggers
 */
class FilterSensitive
{
    /**
     * @var string
     */
    protected $hidden = 'key';

    /**
     * @var string
     */
    const STUB = '***secret***';

    /**
     * @param mixed $data
     *
     * @return array
     */
    public function filter($data)
    {
        if (is_array($data)) {
            return collect($data)->transform(function ($item, $key) {
                if (strpos($key, $this->hidden) !== false) {
                    return static::STUB;
                }
                return $item;
            })->toArray();
        }

        return $this->adapt($data);
    }

    /**
     * @param $data
     *
     * @return mixed|StreamInterface
     */
    private function adapt($data)
    {
        if (is_object($data) && is_a($data, StreamInterface::class)) {
            /* @var StreamInterface $data */
            return json_decode($data->getContents(), true);
        }

        return $data;
    }
}
