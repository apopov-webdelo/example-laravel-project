<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/17/18
 * Time: 15:32
 */

namespace App\Utils;

use App\Contracts\Currency\CurrencyContract;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;

class CurrencyConvertor
{
    /**
     * Convert crypto amount to coins value according cryptoCurrency accuracy
     *
     * @param float                                    $amount
     * @param CurrencyContract|Currency|CryptoCurrency $currency
     *
     * @return int
     */
    public static function toCoins(float $amount, CurrencyContract $currency): int
    {
        return self::toCoinsByAccuracy($amount, $currency->accuracy);
    }

    /**
     * Convert crypto amount to coins value
     *
     * @param float $amount
     * @param int   $accuracy
     *
     * @return int
     */
    public static function toCoinsByAccuracy(float $amount, int $accuracy = 8): int
    {
        return (int)round($amount * pow(10, $accuracy));
    }

    /**
     * Convert amount from coins to float value according cryptoCurrency accuracy
     *
     * @param int                                      $amount
     * @param CurrencyContract|Currency|CryptoCurrency $currency
     * @param bool                                     $string
     *
     * @return float|string
     */
    public static function fromCoins(int $amount, CurrencyContract $currency, bool $string = true)
    {
        return self::fromCoinsByAccuracy($amount, $currency->accuracy, $string);
    }

    /**
     * Convert amount from coins to float value
     *
     * @param int  $amount
     * @param int  $accuracy
     * @param bool $string If returned param type need convert to string
     *
     * @return float|string
     */
    public static function fromCoinsByAccuracy(int $amount, int $accuracy = 8, bool $string = true)
    {
        $value = $amount / pow(10, $accuracy);

        return $string ? self::cutZeros(number_format($value, $accuracy, '.', '')) : $value;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected static function cutZeros(string $value)
    {
        $data = explode('.', $value);
        $after_point = rtrim($data[1], "0");

        return strlen($after_point) === 0
            ? $data[0]
            : $data[0] . '.' . $after_point;
    }
}
