<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/8/18
 * Time: 15:42
 */

namespace App\Utils\User;
use App\Models\User\Review;
use App\Models\User\User;
use App\Repositories\User\ReviewRepo;

/**
 * Blacklist utils check if users is in blacklist (if somebody has blocked another)
 *
 * @package App\Utils\User
 */
class Blacklist
{

    /**
     * @var ReviewRepo
     */
    private $reviewRepo;

    /**
     * Blacklist constructor.
     *
     * @param ReviewRepo $reviewRepo
     */
    public function __construct(ReviewRepo $reviewRepo)
    {
        $this->reviewRepo = $reviewRepo;
    }

    /**
     * Check if users is clean for business
     *
     * @param User $firstUser
     * @param User $secondUser
     * @return bool
     */
    public function isClean(User $firstUser, User $secondUser) : bool
    {
        return $this->isUserClean($firstUser, $secondUser) && $this->isUserClean($secondUser, $firstUser);
    }

    /**
     * Check is subject user did not block object user
     *
     * @param User $subject
     * @param User $object
     * @return bool
     */
    private function isUserClean(User $subject, User $object) : bool
    {
        return $this->reviewRepo
            ->filterByAuthor($subject)
            ->filterByTrust(Review::TRUST_NEGATIVE)
            ->filterByRecipient($object)
            ->take()->get()->isEmpty();
    }

    /**
     * Check if users is blocked
     *
     * @param User $firstUser
     * @param User $secondUser
     * @return bool
     */
    public function isBlocked(User $firstUser, User $secondUser) : bool
    {
        return !$this->isClean($firstUser, $secondUser);
    }
}