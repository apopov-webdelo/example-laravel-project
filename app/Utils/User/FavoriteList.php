<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/8/18
 * Time: 15:42
 */

namespace App\Utils\User;

use App\Models\User\User;
use App\Repositories\User\ReviewRepo;

/**
 * Blacklist utils check if users is in blacklist (if somebody has blocked another)
 *
 * @package App\Utils\User
 */
class FavoriteList
{

    /**
     * @var ReviewRepo
     */
    private $reviewRepo;

    /**
     * Blacklist constructor.
     *
     * @param ReviewRepo $reviewRepo
     */
    public function __construct(ReviewRepo $reviewRepo)
    {
        $this->reviewRepo = $reviewRepo;
    }

    /**
     * Check if users is clean for business
     *
     * @param User $firstUser
     * @param User $secondUser
     * @return bool
     */
    public function isNotFavorite(User $firstUser, User $secondUser) : bool
    {
        return $this->isUserNotFavorite($firstUser, $secondUser) && $this->isUserNotFavorite($secondUser, $firstUser);
    }

    /**
     * Check is subject user did not block object user
     *
     * @param User $subject
     * @param User $object
     * @return bool
     */
    private function isUserNotFavorite(User $subject, User $object) : bool
    {
        return $this->reviewRepo
                    ->filterByAuthor($subject)
                    ->filterByRecipient($object)
                    ->filterByRate(4)
                    ->take()->get()->isEmpty();
    }

    /**
     * Check if users is favorite
     *
     * @param User $firstUser
     * @param User $secondUser
     *
     * @return bool
     */
    public function isFavorite(User $firstUser, User $secondUser) : bool
    {
        return ! $this->isNotFavorite($firstUser, $secondUser);
    }

    /**
     * @param User $subject
     * @param User $object
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList(User $subject, User $object)
    {
        return $this->reviewRepo
            ->filterByAuthor($subject)
            ->filterByRecipient($object)
            ->filterByRate(4)
            ->take()->get();
    }
}