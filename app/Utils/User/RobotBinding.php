<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/2/18
 * Time: 15:33
 */

namespace App\Utils\User;

use App\Contracts\AuthenticatedContract;
use App\Facades\Robot;
use Illuminate\Contracts\Container\Container;

class RobotBinding
{
    /**
     * Bind RobotUser to AuthenticatedContract
     *
     * @param Container|null $app
     *
     * @return Container
     */
    public static function bind(Container $app = null)
    {
        /** @var Container $app */
        $app = $app ?? app();

        $app->bind(AuthenticatedContract::class, function () {
            return Robot::user();
        });

        return $app;
    }
}
