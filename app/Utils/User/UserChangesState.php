<?php

namespace App\Utils\User;

use App\Models\User\UserChanges;

class UserChangesState
{
    /**
     * @var UserChanges
     */
    protected $state;

    public function __construct(?UserChanges $state)
    {
        $this->state = $state;
    }

    /**
     * @return bool
     */
    public function isOAuthRegistration()
    {
        return $this->state ? true : false;
    }

    /**
     * @return bool
     */
    public function canSetLogin()
    {
        return $this->state
            ? $this->state->login_changed_at ? false : true
            : false;
    }

    /**
     * @return bool
     */
    public function canSetEmail()
    {
        return $this->state
            ? $this->state->email_changed_at ? false : true
            : false;
    }
}
