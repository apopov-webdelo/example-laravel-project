<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/2/18
 * Time: 16:48
 */

namespace App\Services\GeoIP;


use App\Contracts\GeoLocatorContract;
use Torann\GeoIP\Facades\GeoIP;

/**
 * Class TorannGeoIP
 * @package App\Services\GeoIP
 */
class TorannGeoIP implements GeoLocatorContract
{
    /**
     * Return client remote IP
     *
     * @return string
     */
    public function getIp() : string
    {
        return GeoIP::getClientIP();
    }

    /**
     * Return country name
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountry(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->country;
    }

    /**
     * Return country code in ISO format
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountryISO(string $ip = null) : string
    {
        return '';
    }

    /**
     * Return country code in Alpha2 format
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountryAlpha2(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->iso_code;
    }

    /**
     * Return country code in Alpha3 format
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountryAlpha3(string $ip = null) : string
    {
        return '';
    }

    /**
     * Return city name
     *
     * @param string|null $ip
     * @return string
     */
    public function getCity(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->city;
    }

    /**
     * Return state name
     *
     * @param string|null $ip
     * @return string
     */
    public function getState(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->state_name;
    }

    /**
     * Return state code
     *
     * @param string|null $ip
     * @return string
     */
    public function getStateCode(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->state;
    }

    /**
     * Return ZIP code (postal code)
     *
     * @param string|null $ip
     * @return string
     */
    public function getPostalCode(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->postal_code;
    }

    /**
     * Return timezone
     *
     * @param string|null $ip
     * @return string
     */
    public function getTimezone(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->timezone;
    }

    /**
     * Return continent code
     *
     * @param string|null $ip
     * @return string
     */
    public function getContinent(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->continent;
    }

    /**
     * Return currency code
     *
     * @param string|null $ip
     * @return string
     */
    public function getCurrency(string $ip = null) : string
    {
        return GeoIP::getLocation($ip)->currency;
    }

    /**
     * Check is that value is default (IP was not found in database)
     *
     * @param string|null $ip
     * @return bool
     */
    public function isDefault(string $ip = null) : bool
    {
        return (bool)GeoIP::getLocation($ip)->default;
    }
}