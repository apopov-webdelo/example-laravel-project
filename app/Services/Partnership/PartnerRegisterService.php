<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 13:43
 */

namespace App\Services\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\PartnerRegisterContract;
use App\Contracts\Partnership\PartnershipProgramContract;
use App\Events\Partnership\PartnerRegistered;
use App\Exceptions\Partnership\PartnerAlreadyRegisteredException;
use App\Models\Partnership\Partner;
use App\Models\User\User;
use App\Repositories\Partnership\ProgramRepoContract;

/**
 * Class PartnerRegisterService
 *
 * @package App\Services\Partnership
 */
class PartnerRegisterService implements PartnerRegisterContract
{
    /**
     * @var string
     */
    protected $event = PartnerRegistered::class;

    /**
     * @var ProgramRepoContract
     */
    protected $programRepo;

    /**
     * PartnerRegisterService constructor.
     *
     * @param ProgramRepoContract $programRepo
     */
    public function __construct(ProgramRepoContract $programRepo)
    {
        $this->programRepo = $programRepo;
    }

    /**
     * {@inheritdoc}
     *
     * @param User $user
     * @param PartnershipProgramContract $programContract
     *
     * @return PartnerContract
     * @throws PartnerAlreadyRegisteredException
     * @throws \Exception
     */
    public function register(User $user, PartnershipProgramContract $programContract = null): PartnerContract
    {
        if (is_user_partner($user)) {
            throw new PartnerAlreadyRegisteredException(__('validation.partnership.already_partner'));
        }

        $partner = new Partner();
        $partner->user()->associate($user);
        $partner->partnershipProgram()->associate(
            $programContract
                ? $this->programRepo->getByDriver($programContract->getDriverAlias())
                : $this->programRepo->getDefaultProgram()
        );

        if ($partner->save()) {
            event(app($this->event, ['partner' => $partner]));
            return $partner;
        }
        throw new \Exception('Unexpected error during Partner model saving!');
    }
}
