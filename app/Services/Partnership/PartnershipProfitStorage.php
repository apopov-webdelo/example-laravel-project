<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/12/18
 * Time: 15:06
 */

namespace App\Services\Partnership;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Partnership\PartnershipProfitStorageContract;
use App\Exceptions\Partnership\InvalidProfitAmountPartnershipException;

/**
 * Class PartnershipProfitStorage
 *
 * @package App\Services\Partnership
 */
class PartnershipProfitStorage implements PartnershipProfitStorageContract
{
    /**
     * @var int $amount
     */
    private $amount;

    /**
     * @var CryptoCurrencyContract $cryptoCurrency
     */
    private $cryptoCurrency;

    /**
     * PartnershipProfitStorage constructor.
     *
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @throws InvalidProfitAmountPartnershipException
     */
    public function __construct(int $amount, CryptoCurrencyContract $cryptoCurrency)
    {
        $this->setAmount($amount)->setCryptoCurrency($cryptoCurrency);
    }

    /**
     * Check and set amount
     *
     * @param int $amount
     *
     * @return $this
     * @throws InvalidProfitAmountPartnershipException
     */
    protected function setAmount(int $amount)
    {
        if ($amount < 0) {
            throw new InvalidProfitAmountPartnershipException(
                'Amount for profit must be positive number!'
            );
        }
        $this->amount = $amount;
        return $this;
    }

    /**
     * Set Crypto currency
     *
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return $this
     */
    protected function setCryptoCurrency(CryptoCurrencyContract $cryptoCurrency)
    {
        $this->cryptoCurrency = $cryptoCurrency;
        return $this;
    }



    /**
     * Retrieve amount
     *
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * Retrieve crypto currency
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract
    {
        return $this->cryptoCurrency;
    }
}