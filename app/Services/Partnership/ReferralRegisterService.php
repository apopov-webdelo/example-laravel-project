<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 13:45
 */

namespace App\Services\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\ReferralContract;
use App\Contracts\Partnership\ReferralRegisterContract;
use App\Events\Partnership\ReferralRegistered;
use App\Exceptions\Partnership\ReferralAlreadyRegisteredException;
use App\Models\Partnership\Referral;
use App\Models\User\User;

/**
 * Class ReferralRegisterService
 *
 * @package App\Services\Partnership
 */
class ReferralRegisterService implements ReferralRegisterContract
{
    /**
     * @var string $event
     */
    protected $event = ReferralRegistered::class;

    /**
     * @param User    $user
     * @param PartnerContract $partner
     *
     * @return ReferralContract
     * @throws ReferralAlreadyRegisteredException
     * @throws \Exception
     */
    public function register(User $user, PartnerContract $partner): ReferralContract
    {
        if (is_user_referral($user)) {
            throw new ReferralAlreadyRegisteredException('User is already referral');
        }

        $referral = new Referral();
        $referral->user()->associate($user);
        $referral->partner()->associate($partner);

        if ($referral->save()) {
            event(app($this->event, ['referral' => $referral]));
            return $referral;
        }
        throw new \Exception('Unexpected error during saving Referral model!');
    }
}
