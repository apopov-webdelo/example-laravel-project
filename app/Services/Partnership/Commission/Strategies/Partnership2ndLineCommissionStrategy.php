<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 13:46
 */

namespace App\Services\Partnership\Commission\Strategies;

use App\Contracts\Partnership\Partnership2ndLineStrategyContract;

/**
 * Service for calculation second line commissions
 *
 * @package App\Services\Partnership
 */
class Partnership2ndLineCommissionStrategy extends Partnership1stLineCommissionStrategy implements
    Partnership2ndLineStrategyContract
{
    /**
     * Path to default config
     *
     * @var string
     */
    protected $configPath = 'app.partnership.second_line';
}
