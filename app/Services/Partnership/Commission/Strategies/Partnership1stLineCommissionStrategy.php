<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 13:46
 */

namespace App\Services\Partnership\Commission\Strategies;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Contracts\Partnership\PartnershipCommissionStrategyContract;
use App\Contracts\Partnership\PartnershipEntityContract;
use App\Contracts\Partnership\ReferralContract;
use App\Models\Directory\CommissionConstants;
use App\Services\Partnership\Commission\PartnershipCommission;

/**
 * Class PartnershipBaseCommissionStrategy
 *
 * @package App\Services\Partnership
 */
class Partnership1stLineCommissionStrategy implements PartnershipCommissionStrategyContract
{
    const DAYS_PER_PERIOD = 180;

    /**
     * Path to default config
     *
     * @var string
     */
    protected $configPath = 'app.partnership.first_line';

    /**
     * Config array
     *
     * @var array
     */
    protected $config;

    /**
     * @var PartnershipEntityContract
     */
    protected $entity;

    /**
     * @var ReferralContract
     */
    protected $referral;

    /**
     * PartnershipBaseCommissionStrategy constructor.
     *
     * @param array|null $config
     */
    public function __construct(array $config = null)
    {
        $this->setConfig($config);
    }

    /**
     * Set custom config
     *
     * @param array $config
     *
     * @return Partnership1stLineCommissionStrategy
     */
    public function setConfig(array $config = null): self
    {
        $this->config = $config ?? config($this->configPath);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param PartnershipEntityContract $entity
     * @param ReferralContract          $referral
     *
     * @return PartnershipCommissionStorageContract
     * @throws \App\Exceptions\Partnership\InvalidCommissionAmountPartnershipException
     */
    public function getCommission(
        PartnershipEntityContract $entity,
        ReferralContract $referral
    ): PartnershipCommissionStorageContract {
        $this->entity   = $entity;
        $this->referral = $referral;

        return $this->getCommissionStorage($this->getCommissionAmount());
    }

    /**
     * Get commission for entity
     *
     * @return float|int
     */
    protected function getCommissionAmount()
    {
        return $this->entity->entityProfit()->amount()
            * $this->getPartnersPercent()
            / CommissionConstants::ONE_PERCENT;
    }

    /**
     * Calculate current commission percentage for partner
     *
     * @return int
     */
    protected function getPartnersPercent(): int
    {
        $configKey = 'percentage_'.$this->getPartnerStageKey();
        return (int)$this->config[$configKey];
    }

    /**
     * Return partner stage key
     *
     * @return string
     */
    protected function getPartnerStageKey(): string
    {
        $referralAge = $this->referral->getRegistrationDate()->diffInDays(now());
        if ($referralAge <= self::DAYS_PER_PERIOD) {
            $stageKey = 'stage_1';
        } elseif ($referralAge <= self::DAYS_PER_PERIOD * 2) {
            $stageKey = 'stage_2';
        } else {
            $stageKey = 'stage_3';
        }
        return $stageKey;
    }

    /**
     * Retrieve instant commission storage object
     *
     * @param $commission
     *
     * @return PartnershipCommissionStorageContract
     * @throws \App\Exceptions\Partnership\InvalidCommissionAmountPartnershipException
     */
    protected function getCommissionStorage($commission) : PartnershipCommissionStorageContract
    {
        return new PartnershipCommission(
            $commission,
            $this->entity->entityProfit()->cryptoCurrency(),
            $this->entity,
            $this->referral
        );
    }
}
