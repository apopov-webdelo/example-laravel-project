<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-12
 * Time: 13:59
 */

namespace App\Services\Partnership\Commission;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Contracts\Partnership\PartnershipEntityContract;
use App\Contracts\Partnership\ReferralContract;
use App\Exceptions\Partnership\InvalidCommissionAmountPartnershipException;

/**
 * Class PartnershipCommission
 *
 * @package App\Services\Partnership
 */
class PartnershipCommission implements PartnershipCommissionStorageContract
{
    /**
     * Commission amount
     *
     * @var int
     */
    protected $amount;

    /**
     * Commission crypto type
     *
     * @var CryptoCurrencyContract
     */
    protected $cryptoCurrency;

    /**
     * Entity is related with that commission
     *
     * @var PartnershipEntityContract
     */
    protected $entity;

    /**
     * Referral is related with that commission
     *
     * @var ReferralContract
     */
    protected $referral;

    /**
     * PartnershipCommission constructor.
     *
     * @param int                       $amount
     * @param CryptoCurrencyContract    $cryptoCurrency
     * @param PartnershipEntityContract $entity
     * @param ReferralContract          $referral
     *
     * @throws InvalidCommissionAmountPartnershipException
     */
    public function __construct(
        int $amount,
        CryptoCurrencyContract $cryptoCurrency,
        PartnershipEntityContract $entity,
        ReferralContract $referral
    ) {
        $this->setAmount($amount);
        $this->cryptoCurrency = $cryptoCurrency;
        $this->entity         = $entity;
        $this->referral       = $referral;
    }

    /**
     * Check and set amount
     *
     * @param int $amount
     *
     * @return $this
     * @throws InvalidCommissionAmountPartnershipException
     */
    public function setAmount(int $amount)
    {
        if ($amount < 0) {
            throw new InvalidCommissionAmountPartnershipException(
                'Commission amount must be positive number!'
            );
        }
        $this->amount = $amount;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract
    {
        return $this->cryptoCurrency;
    }

    /**
     * {@inheritdoc}
     *
     * @return PartnershipEntityContract
     */
    public function entity(): PartnershipEntityContract
    {
        return $this->entity;
    }

    /**
     * {@inheritdoc}
     *
     * @return PartnerContract
     */
    public function partner(): PartnerContract
    {
        return $this->referral->getPartner();
    }

    /**
     * {@inheritdoc}
     *
     * @return ReferralContract
     */
    public function referral(): ReferralContract
    {
        return $this->referral;
    }
}
