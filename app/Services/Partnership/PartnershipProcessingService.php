<?php

namespace App\Services\Partnership;

use App\Contracts\Partnership\Partnership2ndLineStrategyContract;
use App\Contracts\Partnership\PartnershipCommissionStrategyContract;
use App\Contracts\Partnership\PartnershipEntityContract;
use App\Contracts\Partnership\PartnershipPayoutStrategyContract;
use App\Contracts\Partnership\PartnershipProcessingContract;
use App\Contracts\Partnership\ReferralContract;
use App\Events\Partnership\EntityProcessed;
use App\Models\Partnership\Referral;
use App\Services\Partnership\Traits\PartnershipLogger;

/**
 * Class PartnershipProgramProcessingService process base partnership program logic
 *
 * @package App\Services\Partnership
 */
class PartnershipProcessingService implements PartnershipProcessingContract
{
    use PartnershipLogger;

    /**
     * Event will be fired after successfully entity processing
     *
     * @var string
     */
    protected $event = EntityProcessed::class;

    /**
     * Payout strategy service
     *
     * @var PartnershipPayoutStrategyContract
     */
    protected $payoutService;

    /**
     * @var PartnershipEntityContract
     */
    protected $entity;

    /**
     * Service for calculation 2nd line commission
     *
     * @var PartnershipCommissionStrategyContract
     */
    protected $secondLineCommissionStrategy;

    /**
     * PartnershipProcessingService constructor.
     *
     * @param PartnershipPayoutStrategyContract  $payoutService
     * @param Partnership2ndLineStrategyContract $secondLineCommissionStrategy
     */
    public function __construct(
        PartnershipPayoutStrategyContract $payoutService,
        Partnership2ndLineStrategyContract $secondLineCommissionStrategy
    ) {
        $this->payoutService = $payoutService;
        $this->secondLineCommissionStrategy = $secondLineCommissionStrategy;
    }


    /**
     * {@inheritdoc}
     *
     * @param PartnershipEntityContract $entity
     *
     * @return bool
     *
     * @throws \App\Exceptions\Partnership\NotFoundReferralPartnershipException
     */
    public function process(PartnershipEntityContract $entity): bool
    {
        $this->entity = $entity;

        /** @var Referral $referral */
        foreach ($this->entity->entityReferrals() as $referral) {
            $this->processReferral($referral);
        }

        event(app($this->event, ['entity' => $entity]));

        $this->logProcessing();

        return true;
    }

    /**
     * Process referral, calculate commission and check 2-nd partnership line
     *
     * @param ReferralContract $referral
     *
     * @return $this
     * @throws \App\Exceptions\Partnership\NotFoundReferralPartnershipException
     */
    protected function processReferral($referral)
    {
        if (is_referral($referral)) {
            $this->payCommissionToPartner($referral);
            $this->processSecondLine($referral->getPartner());
        }

        return $this;
    }

    /**
     * Pay commission to partner
     *
     * @param ReferralContract                           $referral
     * @param PartnershipCommissionStrategyContract|null $commissionStrategy
     *
     * @return $this
     */
    protected function payCommissionToPartner(
        ReferralContract $referral,
        PartnershipCommissionStrategyContract $commissionStrategy = null
    ) {
        $commissionService = $commissionStrategy ?? $referral->getPartner()
                                                             ->getPartnershipProgram()
                                                             ->getCommissionDriver();

        $commission = $commissionService->getCommission($this->entity, $referral);
        $this->payoutService->pay($commission);

        return $this;
    }

    /**
     * Check is partner referral and process second partnership line
     *
     * @param \App\Contracts\Partnership\PartnerContract $partner
     *
     * @return $this
     * @throws \App\Exceptions\Partnership\NotFoundReferralPartnershipException
     */
    private function processSecondLine(\App\Contracts\Partnership\PartnerContract $partner)
    {

        if (is_user_referral($partner->getUser())) {
            $referral = get_referral($partner->getUser());
            $this->payCommissionToPartner($referral, $this->secondLineCommissionStrategy);
        }

        return $this;
    }

    /**
     * Log entity processing
     *
     * @return $this
     */
    protected function logProcessing()
    {
        $this->logger()->info(
            $this->entity->entityTitle() . ' was processed in partnership.',
            ['entity' => $this->entity]
        );

        return $this;
    }
}
