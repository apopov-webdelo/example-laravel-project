<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-12
 * Time: 09:22
 */

namespace App\Services\Partnership\Traits;

use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

/**
 * Trait PartnershipLogger
 *
 * @package App\Services\Partnership\Traits
 */
trait PartnershipLogger
{
    /**
     * Return logger object for partnership log channel
     *
     * @return LoggerInterface
     */
    protected function logger(): LoggerInterface
    {
        return Log::channel('partnership');
    }
}
