<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-13
 * Time: 10:06
 */

namespace App\Services\Partnership\Payout;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Contracts\Partnership\PartnershipPayoutStrategyContract;
use App\Events\Partnership\PartnerRewarded;
use App\Exceptions\Partnership\FailedPayoutPartnershipException;
use App\Facades\Robot;
use App\Models\Balance\Transaction;
use App\Services\Balance\TransactionService;
use App\Services\Partnership\Traits\PartnershipLogger;

/**
 * Class BalanceTransactionPayoutService
 *
 * Pay partner commission using usual balance transactions.
 *
 * @package App\Services\Partnership\Payout
 */
class BalanceTransactionPayoutService implements PartnershipPayoutStrategyContract
{
    use PartnershipLogger;

    /**
     * Event will be fired after successfully entity processing
     *
     * @var string
     */
    protected $event = PartnerRewarded::class;

    /**
     * @var string
     */
    protected $description = 'balance.transaction.partnership';

    /**
     * @var TransactionService
     */
    private $transferService;

    /**
     * @var PartnershipCommissionStorageContract
     */
    private $commissionStorage;

    /**
     * BalanceTransactionPayoutService constructor.
     *
     * @param TransactionService $transferService
     */
    public function __construct(TransactionService $transferService)
    {
        $this->transferService = $transferService;
    }

    /**
     * {@inheritdoc}
     *
     * @param PartnershipCommissionStorageContract $commissionStorage
     *
     * @return bool
     * @throws FailedPayoutPartnershipException
     */
    public function pay(PartnershipCommissionStorageContract $commissionStorage): bool
    {
        $this->commissionStorage = $commissionStorage;

        try {
            $transaction = $this->transferService
                                ->setDescription(__($this->description))
                                ->transfer(
                                    $commissionStorage->amount(),
                                    Robot::user(),
                                    $commissionStorage->partner()->getUser(),
                                    $commissionStorage->cryptoCurrency()
                                );

            event(app($this->event, ['commissionStorage' => $commissionStorage]));

            $this->logSuccess($transaction);

            return true;
        } catch (\Exception $e) {
            $this->logger()->critical(
                'Fail payout to partner! Error:' . $e->getMessage(),
                [
                    'commissionStorage' => $commissionStorage,
                    'exception'         => $e,
                ]
            );
            throw new FailedPayoutPartnershipException('Fail payout to partner! Error:' . $e->getMessage());
        }
    }

    /**
     * Log successfully payout for debug actions
     *
     * @param Transaction $transaction
     *
     * @return $this
     */
    protected function logSuccess(Transaction $transaction)
    {
        $partnerId = $this->commissionStorage->partner()->getUser()->id;
        $this->logger()->info(
            'Successfully payout for partner ID=' . $partnerId . '.' .
            'Commission: ' . $this->commissionStorage->amount() . '.' .
            'Referral ID: ' . $this->commissionStorage->referral()->getUser()->id . '.' .
            'Entity: ' . $this->commissionStorage->entity()->entityTitle() . '.' .
            'Reason: ' . $this->commissionStorage->entity()->entityDescription() . '.' .
            'Transaction ID: ' . $transaction->id . '.'
        );

        return $this;
    }
}
