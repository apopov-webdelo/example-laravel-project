<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-26
 * Time: 11:37
 */

namespace App\Services\Partnership\Entities;

use App\Contracts\Partnership\PartnershipEntityContract;
use App\Contracts\Partnership\PartnershipProfitStorageContract;
use App\Models\Deal\Deal;
use App\Services\Partnership\PartnershipProfitStorage;
use App\Services\Traits\Dealable;

class DealEntity implements PartnershipEntityContract
{
    use Dealable;

    const TYPE = 'deal';

    /**
     * DealEntity constructor.
     *
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->setDeal($deal);
    }

    /**
     * {@inheritdoc}
     *
     * @return PartnershipProfitStorageContract
     */
    public function entityProfit(): PartnershipProfitStorageContract
    {
        return app(PartnershipProfitStorage::class, [
            'amount'         => $this->deal->commission,
            'cryptoCurrency' => $this->deal->cryptoCurrency
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function entityType(): string
    {
        return self::TYPE;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function entityId(): string
    {
        return $this->deal->id;
    }


    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function entityTitle(): string
    {
        return "Deal-{$this->deal->id}";
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function entityDescription(): string
    {
        $deal = $this->deal;
        return "Deal-{$deal->id}-".
                "{$deal->crypto_amount}{$deal->cryptoCurrency->title}".
                "-to-{$deal->ad->price}{$deal->ad->currency->title}";
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function entityReferrals(): array
    {
        $result = [
            $this->deal->getOffer(),
        ];

        $result = array_filter($result, function ($user) {
            return is_user_referral($user);
        });

        return array_map(function ($user) {
            return get_referral($user);
        }, $result);
    }
}
