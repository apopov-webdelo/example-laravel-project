<?php

namespace App\Services\SmsAero;

use App\Services\SmsAero\Exceptions\InvalidConfiguration;
use Illuminate\Support\ServiceProvider;

class SmsAeroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(SmsAeroChannel::class)
            ->needs(SmsAeroClient::class)
            ->give(function () {
                $config = config('services.smsaero');
                if (is_null($config)) {
                    throw InvalidConfiguration::configurationNotSet();
                }

                return new SmsAeroClient(
                    $config['email'],
                    $config['token'],
                    $config['sign']
                );
            });
    }
}
