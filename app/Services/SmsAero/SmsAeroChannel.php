<?php

namespace App\Services\SmsAero;

use DomainException;
use \Illuminate\Notifications\Notification;
use App\Services\SmsAero\Exceptions\CouldNotSendNotification;

class SmsAeroChannel
{
    /**
     * @var SmsAeroClient
     */
    protected $smsaero;

    /**
     * SmsAeroChannel constructor.
     *
     * @param SmsAeroClient $smsaero
     */
    public function __construct(SmsAeroClient $smsaero)
    {
        $this->smsaero = $smsaero;
    }

    /**
     * @param              $notifiable
     * @param Notification $notification
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('smsaero')) {
            throw CouldNotSendNotification::missingTo();
        }

        $message = $notification->toSmsAero($notifiable);

        if (is_string($message)) {
            $message = new SmsAeroMessage($message);
        }

        try {
            $response = $this->smsaero->send($to, trim($message->content), config('services.smsaero.channel'));

            return $response;
        } catch (DomainException $exception) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($exception);
        }
    }
}
