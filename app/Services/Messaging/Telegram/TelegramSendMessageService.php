<?php

namespace App\Services\Messaging\Telegram;

use App\Contracts\Services\Messaging\Telegram\TelegramSendMessageContract;

class TelegramSendMessageService extends AbstractTelegramService implements TelegramSendMessageContract
{
    /**
     * This service's endpoint | must be set in child class
     *
     * @var string
     */
    protected $endpoint = '/SendMessage';

    /**
     * This service's method | must be set in child class
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Send message thru telegram api
     *
     * @param array $record
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(array $record): bool
    {
        return $this->processRequest([
            'form_params' => [
                'text'    => $this->formatMessage($record),
                'chat_id' => $this->config['channel'],
            ],
            'timeout'     => $this->config['timeout'],
            'http_errors' => false,
        ]);
    }
}
