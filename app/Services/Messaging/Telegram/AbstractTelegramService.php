<?php

namespace App\Services\Messaging\Telegram;

use GuzzleHttp\ClientInterface;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

abstract class AbstractTelegramService
{
    /**
     * Bot api url
     */
    const BOT_API_URL = 'https://api.telegram.org/bot';

    /**
     * @var array
     */
    protected $config;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * This service's endpoint | must be set in child class
     *
     * @var string
     */
    protected $endpoint;

    /**
     * This service's method | must be set in child class
     *
     * @var string
     */
    protected $method;

    /**
     * AbstractTelegramService constructor.
     *
     * @param array           $config
     * @param ClientInterface $client
     *
     * @throws \Exception
     */
    public function __construct(array $config, ClientInterface $client)
    {
        $this->config = $config;
        $this->client = $client;

        $this->validateParams();
    }

    /**
     * @param array $options
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function processRequest(array $options)
    {
        try {
            $response = $this->client->request($this->method, $this->getUrl(), $options);
            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    $this->logCritical('unexpected response: ' . $response->getBody()->getContents());
                }

                if ($json->ok === true) {
                    return true;
                }
            }

            $this->logCritical("NOT OK response: {$code} | " . $response->getBody()->getContents());
        } catch (\Exception $e) {
            $this->logCritical('bad response: ' . $e->getMessage());
        }

        return false;
    }

    /**
     * make emoji for log events
     *
     * @return array
     *
     */
    protected function emojiMap()
    {
        return [
            Logger::DEBUG     => '🚧',
            Logger::INFO      => '‍🗨',
            Logger::NOTICE    => '🕵',
            Logger::WARNING   => '⚡️',
            Logger::ERROR     => '🚨',
            Logger::CRITICAL  => '🤒',
            Logger::ALERT     => '👀',
            Logger::EMERGENCY => '🤕',
        ];
    }

    /**
     * return emoji for given level
     *
     * @param $level
     *
     * @return string
     */
    protected function getEmoji($level)
    {
        $levelEmojiMap = $this->emojiMap();

        return $levelEmojiMap[$level];
    }

    /**
     * @return string
     */
    protected function getUrl(): string
    {
        return static::BOT_API_URL . $this->config['token'] . $this->endpoint;
    }

    /**
     * @param array $record
     *
     * @return string
     */
    protected function formatMessage(array $record)
    {
        $data = collect($record['context']);
        $data->each(function ($item, $key) use (&$description) {
            $description .= $key . ': ' . $item . PHP_EOL;
        });

        $date = date($this->config['date_format']);
        $message = $this->getEmoji($record['level']) . $date . ' ' . $record['channel'] . '.' . $record['level_name']
            . PHP_EOL . ' ' . $record['message']
            . PHP_EOL . $description;

        return $message;
    }

    /**
     * @param string $string
     */
    protected function logCritical(string $string)
    {
        Log::critical(class_basename($this) . ' ' . $string);
    }

    /**
     * @throws \Exception
     */
    private function validateParams()
    {
        if (!$this->endpoint) {
            throw new \Exception(class_basename($this) . ': $endpoint must be set');
        }

        if (!$this->method) {
            throw new \Exception(class_basename($this) . ': $method must be set');
        }
    }
}
