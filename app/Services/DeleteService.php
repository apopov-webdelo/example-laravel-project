<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 12.11.2018
 * Time: 13:37
 */

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DeleteService
 * @package App\Services
 */
abstract class DeleteService
{
    /**
     * @var Model $model
     */
    protected $model;

    /**
     * @param Model $model
     *
     * @return DeleteService
     * @throws \Exception
     */
    public function delete(Model $model) : self
    {
        $this->model = $model;
        $this->model->delete();
        $this->fireEvent();
        return $this;
    }

    /**
     * @return mixed
     */
    abstract protected function fireEvent();
}
