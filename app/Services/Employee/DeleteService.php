<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 12.11.2018
 * Time: 15:41
 */

namespace App\Services\Employee;

use App\Events\Employee\EmployeeDeleted;

/**
 * Class DeleteService
 *
 * @package App\Services\Employee
 */
class DeleteService extends \App\Services\DeleteService
{
    /**
     *
     */
    protected function fireEvent()
    {
        event(app(EmployeeDeleted::class, ['admin' => $this->model]));
    }
}
