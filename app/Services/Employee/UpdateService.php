<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 12.11.2018
 * Time: 13:52
 */

namespace App\Services\Employee;

use App\Contracts\Services\User\SyncPermissionsServiceContract;
use App\Events\Employee\EmployeeUpdated;
use App\Models\Admin\Admin;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateService
 *
 * @package App\Services\Employee
 */
class UpdateService extends \App\Services\UpdateService
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $data
     * @return Model
     */
    public function update(array $data) : Model
    {
        /**
         * @var Admin $model
         */
        $model = parent::update($data);

        /**
         * @var SyncPermissionsServiceContract $service
         */
        $service = app(SyncPermissionsServiceContract::class, ['admin'=>$model]);
        $service->syncPermissions($data['permissions']);

        return $model;
    }

    /**
     * @return mixed|void
     */
    protected function fireEvent()
    {
        event(app(EmployeeUpdated::class, ['admin' => $this->model]));
    }
}
