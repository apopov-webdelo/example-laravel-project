<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 06.11.2018
 * Time: 16:56
 */

namespace App\Services\Employee;

use App\Contracts\Services\User\SyncPermissionsServiceContract;
use App\Events\Employee\EmployeeRegistered;
use App\Models\Admin\Admin;
use App\Models\Admin\Security;
use App\Models\Role\Role;
use App\Models\User\User;

/**
 * Class RegistrationService
 * @package App\Services\Employee
 */
class RegistrationService extends \App\Services\RegistrationService
{
    /**
     * @var string
     */
    private $password;

    public function __construct()
    {
    }

    /**
     * @param array $data
     * @return User
     */
    public function store(array $data) : \Illuminate\Foundation\Auth\User
    {
        $this->userStore($data)
             ->securityStore()
             ->syncPermissions($data['permissions'])
             ->fireEvent();

        return $this->user;
    }

    /**
     * @param array $data
     * @return RegistrationService
     */
    protected function userStore(array $data) : self
    {
        $this->password = str_random(10);

        $this->user = Admin::create([
            'name'      => $data['name'],
            'login'     => $data['login'],
            'email'     => $data['email'],
            'phone'     => $data['phone'],
            'password'  => bcrypt($this->password),
            'api_token' => str_random(60)
        ]);

        return $this;
    }

    /**
     * @param Role $role
     *
     * @return RegistrationService
     */
    protected function assignRole(Role $role) : self
    {
        $this->user->assignRole($role);
        return $this;
    }

    /**
     * @return RegistrationService
     */
    protected function securityStore() : self
    {
        /** @var Security $model */
        $model = app(Security::class);
        $model->admin()->associate($this->user);
        $model->save();
        return $this;
    }

    /**
     * @param array $permissions
     * @return RegistrationService
     */
    protected function syncPermissions(array $permissions) : self
    {
        /**
         * @var SyncPermissionsServiceContract $service
         */
        $service = app(SyncPermissionsServiceContract::class, ['admin'=>$this->user]);
        $service->syncPermissions($permissions);
        return $this;
    }

    /**
     *
     */
    protected function fireEvent()
    {
        event(app(EmployeeRegistered::class, ['admin' => $this->user, 'password' => $this->password]));
    }
}
