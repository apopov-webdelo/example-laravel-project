<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/19/18
 * Time: 13:18
 */

namespace App\Services\Images;

use App\Contracts\ImageableContract;
use App\Contracts\Images\ImageContract;
use App\Contracts\Images\ImageUploadStorageServiceContract;
use App\Models\Image\Image;
use Illuminate\Http\UploadedFile;

class ImageUploadStorageService implements ImageUploadStorageServiceContract
{
    /**
     * @var UploadedFile $file
     */
    private $file;

    /**
     * @param UploadedFile $file
     *
     * @return ImageUploadStorageServiceContract
     */
    public function setImage(UploadedFile $file): ImageUploadStorageServiceContract
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getImage(): UploadedFile
    {
        return $this->file;
    }

    /**
     * @param ImageableContract $object
     * @param ImageContract     $image
     *
     * @return bool
     */
    public function uploadFor(ImageContract $image, ImageableContract $object): bool
    {
        return (bool)$this->getImage()->storeAs(
            Image::IMAGES_LOCATION,
            $image->getFilenameById(),
            'public'
        );
    }
}