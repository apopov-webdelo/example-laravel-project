<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/19/18
 * Time: 13:18
 */

namespace App\Services\Images;

use App\Contracts\AuthenticatedContract;
use App\Contracts\ImageableContract;
use App\Contracts\Images\ImageContract;
use App\Contracts\Images\ImageUploadServiceContract;
use App\Contracts\Images\ImageUploadStorageServiceContract;
use App\Models\Image\Image;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ImageService
 *
 * @package App\Services\Images
 */
class ImageUploadService implements ImageUploadServiceContract
{
    /**
     * @param ImageableContract|Model     $object
     * @param ImageUploadStorageServiceContract $imageStorage
     *
     * @return ImageContract
     */
    public function upload(ImageableContract $object, ImageUploadStorageServiceContract $imageStorage): ImageContract
    {
        $file  = $imageStorage->getImage();
        /** @var Image $image */
        $image = new Image([
            'title'    => $file->getClientOriginalName(),
            'filename' => $file->getClientOriginalName(),
            'mime'     => $file->getMimeType(),
            'ext'      => $file->getClientOriginalExtension(),
            'size'     => $file->getSize(),
        ]);
        $image->author()->associate(app(AuthenticatedContract::class));
        $image->imageable()->associate($object);
        $image->save();

        if ($image->id) {
            $imageStorage->uploadFor($image, $object);
        }

        return $image;
    }
}
