<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 14:27
 */

namespace App\Services;

use App\Contracts\AuthenticatedContract;
use App\Services\Traits\Eventable;
use App\Services\Traits\Userable;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseService
 * @package App\Services
 */
abstract class BaseService
{
    use Eventable, Userable;

    /**
     * Model class for instance for service logic
     *
     * @var string Model class for service using
     */
    protected $modelClass;

    /**
     * Instance of service using model
     *
     * @var Model
     */
    protected $model;

    /**
     * @var Container
     */
    protected $app;

    /**
     * BaseService constructor for all services.
     *
     * @param AuthenticatedContract $user
     * @param Container $container
     */
    public function __construct(AuthenticatedContract $user, Container $container)
    {
        if ($this->modelClass) {
            $this->setModel(new $this->modelClass);
        }

        $this->app = $container;
        $this->setUser($user);
    }

    /**
     * Set model to class variable
     *
     * @param Model $model
     * @return self
     */
    protected function setModel(Model $model): self
    {
        $this->model = $model;
        return $this;
    }
}
