<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 06.11.2018
 * Time: 13:27
 */

namespace App\Services;

use App\Contracts\AuthenticatedContract;
use App\Contracts\PasswordResetContract;
use App\Events\User\PasswordReseted;
use App\Models\User\SecurityLogConstants;
use App\Services\Traits\Userable;
use App\Traits\Service\HasSecurityLogs;
use Illuminate\Support\Facades\Log;

/**
 * Class PasswordResetService
 * @package App\Services\User\Security
 */
class PasswordResetService implements PasswordResetContract
{
    use Userable, HasSecurityLogs;

    /**
     * @var string $password
     */
    private $password;

    /**
     * Reset password for user
     *
     * @param string $reason
     * @return $this
     */
    public function reset(string $reason): PasswordResetContract
    {
        $this->resetPassword();

        $class = get_class($this->user);
        Log::info("Password was updated for {$class}:{$this->user->getId()} by administrator. Reason: {$reason}");

        $event = app(PasswordReseted::class, [
            'user'      => $this->user,
            'reason'    => $reason,
            'password'  => $this->password
        ]);

        event($event);

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::PASSWORD_IS_RESET);

        return $this;
    }

    /**
     * @return $this
     */
    private function resetPassword()
    {
        $this->password = str_random(10);
        $this->user->setPassword($this->password);
        $this->user->invalidateAllTokens();

        return $this;
    }
}
