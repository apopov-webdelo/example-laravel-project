<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/22/18
 * Time: 14:55
 */

namespace App\Services\ActivityLog;

use App\Contracts\ActivityLog\ActivityLoggable;
use App\Contracts\ActivityLog\ActivityLogServiceContract;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Admin\Admin;
use App\Models\User\User;

/**
 * Class ActivityLogService
 *
 * @package App\Services\ActivityLog
 */
class ActivityLogService implements ActivityLogServiceContract
{
    /**
     * @var ActivityLoggable|User|Admin
     */
    private $user;

    /**
     * ActivityLogService constructor. Depencies
     *
     * @param ActivityLoggable $user
     */
    public function __construct(ActivityLoggable $user)
    {
        $this->user = $user;
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function alert(string $message, string $type)
    {
        return $this->log($message, $type, 'alert');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function critical(string $message, string $type)
    {
        return $this->log($message, $type, 'critical');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function debug(string $message, string $type)
    {
        return $this->log($message, $type, 'debug');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function emergency(string $message, string $type)
    {
        return $this->log($message, $type, 'emergency');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function error(string $message, string $type)
    {
        return $this->log($message, $type, 'error');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function info(string $message, string $type)
    {
        return $this->log($message, $type, 'info');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function notice(string $message, string $type)
    {
        return $this->log($message, $type, 'notice');
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return ActivityLog
     */
    public function warning(string $message, string $type)
    {
        return $this->log($message, $type, 'warning');
    }

    /**
     * @param string $message
     * @param string $type
     * @param string $level
     *
     * @return ActivityLog
     */
    public function log(string $message, string $type, string $level = 'info')
    {
        $model = new ActivityLog();
        $model->fill([
            'message'         => $message,
            'type'            => $type,
            'level'           => $level,
            'authorable_id'   => $this->user->id,
            'authorable_type' => get_class($this->user),
        ])->save();

        return $model;
    }
}
