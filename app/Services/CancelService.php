<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/20/18
 * Time: 13:54
 */

namespace App\Services;


use App\Services\Traits\Cancellable;
use Illuminate\Database\Eloquent\Model;

abstract class CancelService extends BaseService
{
    use Cancellable;

    /**
     * @var int
     */
    protected $statusId;

    /**
     * @var string
     */
    protected $statusModel;

    /**
     * Return cancel status object
     *
     * @return Model
     */
    protected function getCancelStatus() : Model
    {
        return $this->app->make($this->statusModel)::findOrFail($this->statusId);
    }
}