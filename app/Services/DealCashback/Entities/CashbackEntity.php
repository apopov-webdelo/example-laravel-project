<?php

namespace App\Services\DealCashback\Entities;

use App\Contracts\DealCashback\CashbackEntityContract;
use App\Contracts\DealCashback\DealCashbackContract;
use App\Models\DealCashback\DealCashback;

class CashbackEntity implements CashbackEntityContract
{
    const TYPE = 'cashback';

    /**
     * @var DealCashback
     */
    protected $cashback;

    /**
     * CashbackEntity constructor.
     *
     * @param DealCashback $cashback
     */
    public function __construct(DealCashback $cashback)
    {
        $this->cashback = $cashback;
    }

    /**
     * Retrieve entity type
     *
     * That value must be similar for all entities in that classname
     *
     * @return string
     */
    public function entityType(): string
    {
        return self::TYPE;
    }

    /**
     * Retrieve entity ID code
     *
     * @return string
     */
    public function entityId(): string
    {
        return $this->cashback->id;
    }

    /**
     * Retrieve entity title
     *
     * That value is unique for concrete entity
     *
     * @return string
     */
    public function entityTitle(): string
    {
        return "Cashback-{$this->entityId()}";
    }

    /**
     * Retrieve entity description
     *
     * That value is unique for concrete entity
     *
     * @return string
     */
    public function entityDescription(): string
    {
        return "Cashback-{$this->entityId()}-".
            "{$this->cashback->crypto_currency_id}".
            "-to-{$this->cashback->commission}";
    }
}
