<?php

namespace App\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackContract;
use App\Contracts\DealCashback\DealCashbackDoneServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Exceptions\DealCashback\DealCashbackException;
use App\Exceptions\DealCashback\ExistingsTransactionIdCashbackException;
use App\Exceptions\DealCashback\UnexpectedCashbackStatusException;
use App\Models\DealCashback\DealCashback;

/**
 * Class DealCashbackDoneService
 *
 * @package App\Services\DealCashback
 */
class DealCashbackDoneService implements DealCashbackDoneServiceContract
{
    /**
     * @var DealCashbackRepoContract
     */
    protected $cashbackRepo;

    /**
     * DealCashbackDoneService constructor.
     *
     * @param DealCashbackRepoContract $cashbackRepo
     */
    public function __construct(DealCashbackRepoContract $cashbackRepo)
    {
        $this->cashbackRepo = $cashbackRepo;
    }

    /**
     * Cancel cashback model
     *
     * @param DealCashbackContract $cashback
     *
     * @param string               $transactionId
     *
     * @return bool
     *
     * @throws DealCashbackException
     */
    public function done(DealCashbackContract $cashback, string $transactionId): bool
    {
        $this->checkTransactionId($transactionId);

        $this->checkStatus($cashback);

        $cashback->setStatus(DealCashback::STATUS_DONE);
        $cashback->setTransactionId($transactionId);

        return true;
    }

    /**
     * @param string $transactionId
     *
     * @return $this
     * @throws ExistingsTransactionIdCashbackException
     */
    private function checkTransactionId(string $transactionId)
    {
        if ($this->cashbackRepo->isExists($transactionId)) {
            throw new ExistingsTransactionIdCashbackException(
                'Cachback with same transaction ID exists! (ID: ' . $transactionId . ')'
            );
        }

        return $this;
    }

    /**
     * @param DealCashbackContract $cashback
     *
     * @return $this
     * @throws UnexpectedCashbackStatusException
     */
    private function checkStatus(DealCashbackContract $cashback)
    {
        if ($cashback->isPending()) {
            return $this;
        }

        throw new UnexpectedCashbackStatusException(
            'Unexpected status! Current status is "' . $cashback->status() . '".
            Expected "' . DealCashback::STATUS_PENDING . '"'
        );
    }
}
