<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 21:06
 */

namespace App\Services\DealCashback;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\DealCashback\CashbackEntityContract;
use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Models\DealCashback\DealCashback;
use App\Models\User\User;
use App\Services\DealCashback\Entities\CashbackEntity;
use Illuminate\Support\Collection;

class DealCashbackCommission implements DealCashbackCommissionStorageContract
{
    /**
     * @var DealCashback
     */
    protected $cashback;

    /**
     * @var CashbackEntityContract
     */
    protected $entity;

    /**
     * DealCashbackCommission constructor.
     *
     * @param DealCashback $cashback
     */
    public function __construct(DealCashback $cashback)
    {
        $this->cashback = $cashback;
        $this->entity = app(CashbackEntity::class, ['cashback' => $cashback]);
    }

    /**
     * @inheritdoc
     *
     * @return mixed
     */
    public function percent()
    {
        return $this->cashback->commission;
    }

    /**
     * @inheritdoc
     *
     * @return int
     */
    public function amount(): int
    {
        return (int)ceil(
            $this->cashback->deals()->sum('commission') * config('app.cashback.deals.commission') / 100
        );
    }

    /**
     * @inheritdoc
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract
    {
        return $this->cashback->deals()->last()->cryptoCurrency;
    }

    /**
     * Cashback receiving user
     *
     * @return User
     */
    public function user(): User
    {
        return $this->cashback->recipient();
    }

    /**
     * Retrieve entity that is reason for partner reward
     *
     * @return CashbackEntityContract
     */
    public function entity(): CashbackEntityContract
    {
        return $this->entity;
    }

    /**
     * All deals of cashback
     *
     * @return Collection
     */
    public function deals(): Collection
    {
        return $this->cashback->deals();
    }
}
