<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/22/19
 * Time: 12:31
 */
namespace App\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackProcessServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Events\DealCashback\DealCashbackPendingState;
use App\Models\Deal\Deal;
use App\Models\DealCashback\DealCashback;
use App\Models\User\User;
use Illuminate\Support\Facades\Log;

class DealCashbackProcessService implements DealCashbackProcessServiceContract
{
    /**
     * @var string
     */
    protected $event = DealCashbackPendingState::class;

    /**
     * @var DealCashbackRepoContract
     */
    private $repo;

    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var Log
     */
    private $log;

    /**
     * DealCashbackProcessService constructor.
     *
     * @param DealCashbackRepoContract $repo
     * @param Log                      $log
     */
    public function __construct(
        DealCashbackRepoContract $repo,
        Log $log
    ) {
        $this->repo = $repo;
        $this->log  = $log::channel('cashback');
    }

    /**
     * @param Deal $deal
     *
     * @return bool
     * @throws \Exception
     */
    public function process(Deal $deal): bool
    {
        $this->deal = $deal;
        foreach ($this->deal->getDealMembers() as $participant) {
            $this->processParticipant($participant);
        }

        return true;
    }

    /**
     * @param User $user
     *
     * @throws \Exception
     */
    private function processParticipant(User $user)
    {
        /** @var DealCashback $cashback */
        $cashback = $this->repo->getInProgressOrCreate($user, $this->deal->getCryptoCurrency());
        $cashback->dealsRelation()->attach($this->deal);
        if ($cashback->isReadyToPay()) {
            $result = $cashback->setStatus(DealCashback::STATUS_PENDING);

            if ($result) {
                $this->log->info("Cashback #{$cashback->id} processed by deal #{$this->deal->id}");
                event(app($this->event, ['cashback' => $cashback]));
            }
        }
        $cashback->save();
    }
}
