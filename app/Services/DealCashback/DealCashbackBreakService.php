<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/22/19
 * Time: 13:58
 */

namespace App\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackBreakServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Models\Deal\Deal;
use App\Models\DealCashback\DealCashback;
use App\Models\User\User;
use Illuminate\Support\Facades\Log;

/**
 * Class DealCashbackBreakService
 *
 * @package App\Services\DealCashback
 */
class DealCashbackBreakService implements DealCashbackBreakServiceContract
{
    /**
     * @var DealCashbackRepoContract
     */
    private $repo;

    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var Log
     */
    private $log;

    /**
     * DealCashbackBreakService constructor.
     *
     * @param DealCashbackRepoContract $repo
     * @param Log                      $log
     */
    public function __construct(
        DealCashbackRepoContract $repo,
        Log $log
    ) {
        $this->repo = $repo;
        $this->log  = $log::channel('cashback');
    }

    /**
     * @param Deal $deal
     *
     * @return bool
     * @throws \Exception
     */
    public function break(Deal $deal): bool
    {
        $this->deal = $deal;
        foreach ($this->deal->getDealMembers() as $participant) {
            $this->processParticipant($participant);
        }
        return true;
    }

    /**
     * @param User $user
     *
     * @throws \Exception
     */
    private function processParticipant(User $user)
    {
        /** @var DealCashback $cashback */
        $cashback = $this->repo
                         ->filterByRecipient($user)
                         ->filterByCryptoCurrency($this->deal->getCryptoCurrency())
                         ->filterByStatus(DealCashback::STATUS_IN_PROGRESS)
                         ->take()
                         ->first();

        if ($cashback) {
            $cashback->dealsRelation()->attach($this->deal);
            $cashback->setStatus(DealCashback::STATUS_BREAK);
            $this->log->info("Cashback #{$cashback->id} break by deal #{$this->deal->id}");
        }
    }
}
