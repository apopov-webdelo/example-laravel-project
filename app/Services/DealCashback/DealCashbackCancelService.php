<?php

namespace App\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackCancelServiceContract;
use App\Contracts\DealCashback\DealCashbackContract;
use App\Exceptions\DealCashback\UnexpectedCashbackStatusException;
use App\Models\DealCashback\DealCashback;

/**
 * Class DealCashbackCancelService
 *
 * @package App\Services\DealCashback
 */
class DealCashbackCancelService implements DealCashbackCancelServiceContract
{
    /**
     * Cancel cashback model
     *
     * @param DealCashbackContract|DealCashback $cashback
     *
     * @return bool
     * @throws \Exception
     */
    public function cancel(DealCashbackContract $cashback): bool
    {
        $this->checkStatus($cashback);

        $cashback->setStatus(DealCashback::STATUS_CANCELED);

        return true;
    }

    /**
     * @param DealCashbackContract $cashback
     *
     * @return $this
     * @throws UnexpectedCashbackStatusException
     */
    private function checkStatus(DealCashbackContract $cashback)
    {
        if ($cashback->isCanceled()) {
            throw new UnexpectedCashbackStatusException(
                'Unexpected status! Current status is "' . $cashback->status() . '".
            Expected "' . DealCashback::STATUS_PENDING . '"'
            );
        }

        return $this;
    }
}
