<?php

namespace App\Services\Deal;

use App\Contracts\Services\Deal\VerifyDealServiceContract;
use App\Events\Deal\DealVerified;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class VerifyDealService
 *
 * @method VerifyDealService setModel(Model $model)
 * @method VerifyDealService checkDealStatus()
 * @method VerifyDealService addStatus(int $statusId)
 * @method VerifyDealService fireEvent($event)
 * @property Deal $model
 * @package App\Services\Deal
 */
class VerifyDealService extends DealService implements VerifyDealServiceContract
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::VERIFIED;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::VERIFICATION,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealVerified::class;

    /**
     * VerifyDealService constructor.
     *
     * @param Container  $container
     * @param DealStatus $dealStatus
     */
    public function __construct(Container $container, DealStatus $dealStatus)
    {
        parent::__construct($container, $dealStatus);
    }

    /**
     * Register verified deal in backend service
     *
     * @param Deal $deal
     *
     * @return Deal
     * @throws \Exception
     */
    public function verified(Deal $deal): Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);
            $this->setModel($deal)
                 ->checkDealStatus()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent($this->eventClass);
            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
