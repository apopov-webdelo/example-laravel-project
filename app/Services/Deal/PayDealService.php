<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/28/18
 * Time: 14:59
 */

namespace App\Services\Deal;

use App\Events\Deal\DealPaid;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class PayDealService
 *
 * @package App\Services\Deal
 * @method PayDealService setModel(Model $model) : self
 */
class PayDealService extends DealService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::PAID;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::VERIFIED,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealPaid::class;

    /**
     * Set Deal to pay status
     *
     * @param Deal $deal
     *
     * @return Deal
     *
     * @throws \Exception
     */
    public function pay(Deal $deal): Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);
            $this->setModel($deal)
                 ->checkDealStatus()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent($this->eventClass);
            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
