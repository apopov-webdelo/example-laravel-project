<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/9/18
 * Time: 15:58
 */

namespace App\Services\Deal;

use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Facades\Robot;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Directory\CryptoCurrency;
use App\Services\BaseService;
use Illuminate\Contracts\Container\Container;

/**
 * Class DealService
 *
 * @package App\Services\Deal
 * @property Deal $model
 */
abstract class DealService extends BaseService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [];

    /**
     * @var DealStatus
     */
    protected $dealStatus;

    /**
     * PayDealService constructor.
     *
     * @param Container  $container
     * @param DealStatus $dealStatus
     */
    public function __construct(
        Container $container,
        DealStatus $dealStatus
    ) {
        parent::__construct(Robot::user(), $container);
        $this->dealStatus = $dealStatus;
    }

    /**
     * @return DealService
     * @throws UnexpectedDealStatusException
     */
    protected function checkDealStatus()
    {
        $currentStatusId = $this->getDeal()->getCurrentStatus()->id;
        if (in_array($currentStatusId, $this->currentStatuses)) {
            return $this;
        }
        $statuses = implode(',', $this->currentStatuses);
        throw new UnexpectedDealStatusException(
            'Unexpected deal status ID=' . $currentStatusId . '! Expected statuses is (' . $statuses . ').'
        );
    }

    /**
     * Attach status for deal
     *
     * @param int $statusId
     *
     * @return self
     */
    protected function addStatus(int $statusId)
    {
        /** @var DealStatus $status */
        $status = $this->dealStatus->findOrFail($statusId);
        $this->model->statuses()->attach($status);

        return $this;
    }

    /**
     * Return Deal model
     *
     * @return Deal
     */
    protected function getDeal(): Deal
    {
        return $this->model;
    }

    /**
     * Return CryptoCurrency for Deal model
     *
     * @return CryptoCurrency
     */
    protected function getCryptoCurrency(): CryptoCurrency
    {
        return $this->getDeal()->ad->cryptoCurrency;
    }
}
