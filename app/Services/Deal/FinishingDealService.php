<?php

namespace App\Services\Deal;

use App\Contracts\Services\Deal\FinishingDealServiceContract;
use App\Events\Deal\DealFinishing;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class FinishingDealService
 *
 * @package App\Services\Deal
 * @method FinishingDealService setModel(Model $model) : \App\Services\BaseService
 * @method FinishingDealService checkDealStatus()
 */
class FinishingDealService extends DealService implements FinishingDealServiceContract
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::FINISHING;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::PAID,
        DealStatusConstants::IN_DISPUTE,
        DealStatusConstants::VERIFIED,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealFinishing::class;

    /**
     * Register finishing deal in backend service
     *
     * @param Deal $deal
     *
     * @return Deal
     * @throws \Exception
     */
    public function finishing(Deal $deal): Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);

            $this->setModel($deal)
                 ->checkDealStatus()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent($this->eventClass);

            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
