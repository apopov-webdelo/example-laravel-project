<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:11
 */

namespace App\Services\Deal;

use App\Contracts\AuthenticatedContract;
use App\Events\Deal\DealPlaced;
use App\Events\Deal\DealPlacedForStellar;
use App\Exceptions\Balance\BalanceAmountException;
use App\Exceptions\Deal\DealException;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\CommissionConstants;
use App\Services\Ad\DeactivateAdService;
use App\Services\Balance\EscrowService;
use App\Services\StoreService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Facades\DB;

/**
 * Class DealService
 * @package App\Services\Deal
 * @property Deal $model
 */
class StoreDealService extends StoreService
{
    /**
     * First deal status ID will be set after storing
     *
     * @var int
     */
    protected $statusId = DealStatusConstants::VERIFICATION;

    /**
     * @var string
     */
    protected $modelClass = Deal::class;

    /**
     * @var string
     */
    protected $event = DealPlaced::class;

    /**
     * @var string
     */
    protected $eventForStellar = DealPlacedForStellar::class;

    /**
     * @var DealStatus
     */
    protected $dealStatus;

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * @var EscrowService
     */
    protected $escrowService;

    /**
     * @var DeactivateAdService
     */
    protected $deactivateAdService;

    /**
     * @param AuthenticatedContract $user
     * @param Container $container
     * @param DealStatus $dealStatus
     * @param Ad $ad
     * @param EscrowService $escrowService
     * @param DeactivateAdService $deactivateAdService
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        DealStatus $dealStatus,
        Ad $ad,
        EscrowService $escrowService,
        DeactivateAdService $deactivateAdService
    ) {
        parent::__construct($user, $container);

        $this->dealStatus          = $dealStatus;
        $this->ad                  = $ad;
        $this->escrowService       = $escrowService;
        $this->deactivateAdService = $deactivateAdService;
    }

    /**
     * Store new deal according data
     *
     * @param array $data
     * @return Deal
     * @throws \Exception
     */
    public function store(array $data): Deal
    {
        $ad = $this->getAd($data['ad_id']);

        $data['price']         = $ad->price;
        $data['time']          = $ad->time;
        $data['conditions']    = $ad->conditions;
        $data['fiat_amount']   = currencyToCoins($data['fiat_amount'], $ad->currency);
        $data['crypto_amount'] = currencyToCoins($data['fiat_amount'] / $ad->price, $ad->cryptoCurrency);
        $data['commission']    = (int)ceil(
            $data['crypto_amount'] * $ad->accuracyCommissionPercent / CommissionConstants::ONE_PERCENT
        );

        //TODO: подумать о блокировке изменения цены в объявлении на момент осуществления сделки

        DB::beginTransaction();

        try {
            $this->setStoreEventClass($this->event)
                 ->storeModel($data, false)
                 ->addStatus()
                 ->updateSellerBalance()
                 ->savePublicKey($data)
                 ->sendToStellar();

            DB::commit();

            return $this->fireEvent($this->event)->model;
        } catch (BalanceAmountException $exception) {
            DB::rollBack();
            throw new DealException(__('deal.error.shortage_balance'), $exception->getCode());
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $data
     *
     * @return $this
     */
    protected function savePublicKey($data)
    {
        if (isset($data['public_key'])) {
            $this->model->saveBcKey($data['public_key'], 'offer');
        }

        return $this;
    }

    /**
     * Return instant Ad object
     *
     * @param int $adId
     * @return Ad
     */
    protected function getAd(int $adId): Ad
    {
        return $this->ad->findOrFail($adId);
    }

    /**
     * Attach status for stored deal
     *
     * @return StoreDealService
     */
    protected function addStatus(): self
    {
        $status = $this->dealStatus->findOrFail($this->statusId);
        $this->model->statuses()->attach($status);
        return $this;
    }

    /**
     * Update seller balance after new deal storing
     *
     * @return StoreDealService
     * @throws \Exception
     */
    protected function updateSellerBalance(): self
    {
        /** @var Deal $deal */
        $deal = $this->model;

        $this->escrowService->increase(
            $deal->getEscrowAmount(),
            $deal->getSeller(),
            $deal->getCryptoCurrency()
        );

        return $this;
    }

    /**
     * Fire sync event to place deal to stellar
     *
     * @return $this
     * @throws \ReflectionException
     */
    protected function sendToStellar()
    {
        $this->fireEvent($this->eventForStellar);

        return $this;
    }
}
