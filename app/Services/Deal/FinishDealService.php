<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/28/18
 * Time: 14:16
 */

namespace App\Services\Deal;

use App\Events\Deal\DealFinished;
use App\Models\Balance\TransactionConstants;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Services\Balance\CommissionService;
use App\Services\Balance\EscrowService;
use App\Services\Balance\TransactionService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class FinishDealService
 *
 * @package App\Services\Deal
 * @method FinishDealService setModel(Model $model)
 * @method FinishDealService checkDealStatus()
 * @method FinishDealService addStatus(int $statusId)
 * @method FinishDealService fireEvent($event)
 * @property Deal $model
 */
class FinishDealService extends DealService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::FINISHED;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::PAID,
        DealStatusConstants::IN_DISPUTE,
        DealStatusConstants::VERIFIED,
        DealStatusConstants::FINISHING,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealFinished::class;

    /**
     * @var EscrowService
     */
    protected $escrowService;

    /**
     * @var TransactionService
     */
    protected $transactionService;

    /**
     * @var CommissionService
     */
    protected $commissionService;

    /**
     * @param Container          $container
     * @param DealStatus         $dealStatus
     * @param EscrowService      $escrowService
     * @param TransactionService $transactionService
     * @param CommissionService  $commissionService
     */
    public function __construct(
        Container $container,
        DealStatus $dealStatus,
        EscrowService $escrowService,
        TransactionService $transactionService,
        CommissionService $commissionService
    ) {
        parent::__construct($container, $dealStatus);

        $this->escrowService = $escrowService;
        $this->transactionService = $transactionService;
        $this->commissionService = $commissionService;
    }

    /**
     * Finish deal (balance, status, event)
     *
     * @param Deal $deal
     *
     * @return Deal
     * @throws \Exception
     */
    public function finish(Deal $deal): Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);
            $this->setModel($deal)
                 ->checkDealStatus()
                 ->makeTransactions()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent($this->eventClass);
            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @return self
     * @throws \Exception
     */
    protected function makeTransactions(): self
    {
        $deal = $this->getDeal();
        $seller = $deal->getSeller();
        $offer = $deal->getOffer();
        $buyer = $deal->getBuyer();
        $cryptoCurrency = $deal->getCryptoCurrency();

        /* Escrow Decrease */
        $escrow = $deal->getEscrowAmount();
        $this->escrowService->decrease($escrow, $seller, $cryptoCurrency);
        /* /Escrow Decrease */

        /* Transaction Seller-Buyer */
        $description = __('balance.transaction.deal.default', ['deal_id' => $deal->id]);
        $this->transactionService
            ->setDescription($description)
            ->setReason(TransactionConstants::REASON_DEAL)
            ->transfer($deal->crypto_amount, $seller, $buyer, $cryptoCurrency);
        /* /Transaction Seller-Buyer */

        /* Commission */
        $description = __('balance.transaction.commission.deal', ['deal_id' => $deal->id]);
        $this->commissionService
            ->setUser(robot())
            ->setDescription($description)
            ->charge($deal->commission, $offer, $cryptoCurrency);

        /* /Commission */

        return $this;
    }
}
