<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/20/18
 * Time: 13:51
 */

namespace App\Services\Deal;

use App\Exceptions\Deal\DealException;
use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Models\Deal\DealStatusConstants;
use App\Rules\Deal\DealDeadlineRule;

/**
 * Class AutocancelDealService
 * @package App\Services\Deal
 */
class AutocancelDealService extends CancellationDealService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::AUTOCANCELED;

    /**
     * @return AutocancelDealService|CancellationDealService
     *
     * @throws DealException
     * @throws UnexpectedDealStatusException
     */
    protected function checkDealStatus()
    {
        /** @var DealDeadlineRule $deadlineRule */
        $deadlineRule = app(DealDeadlineRule::class, ['deal' => $this->model]);
        if ($deadlineRule->check()) {
            return parent::checkDealStatus();
        }

        throw new DealException($deadlineRule->message());
    }

    /**
     * @return array|null|string
     */
    protected function check()
    {
        return __('validation.deal.is_not_deadline_time');
    }
}
