<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/20/18
 * Time: 13:51
 */

namespace App\Services\Deal;

use App\Events\Deal\DealCanceled;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Services\Balance\EscrowService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class CancelDealService
 *
 * @package App\Services\Deal
 * @method CancelDealService setModel(Model $model) : \App\Services\BaseService
 * @method CancelDealService checkDealStatus()
 */
class CancelDealService extends DealService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::CANCELED;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::PAID,
        DealStatusConstants::IN_DISPUTE,
        DealStatusConstants::VERIFIED,
        DealStatusConstants::CANCELLATION,
        DealStatusConstants::AUTOCANCELED,
        DealStatusConstants::VERIFICATION,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealCanceled::class;

    /**
     * @var EscrowService
     */
    protected $escrowService;

    /**
     * @param Container     $container
     * @param DealStatus    $dealStatus
     * @param EscrowService $escrowService
     */
    public function __construct(
        Container $container,
        DealStatus $dealStatus,
        EscrowService $escrowService
    ) {
        parent::__construct($container, $dealStatus);
        $this->escrowService = $escrowService;
    }

    /**
     * Cancel deal
     *
     * @param Deal $deal
     *
     * @return Deal
     * @throws \Exception
     */
    public function cancel(Deal $deal): Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);

            $this->setModel($deal)
                 ->checkDealStatus()
                 ->updateSellerBalance()
                 ->updateAd()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent($this->eventClass);

            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @return self
     * @throws \Exception
     */
    protected function updateSellerBalance(): self
    {
        $deal = $this->getDeal();
        $seller = $deal->getSeller();
        $sum = $deal->getEscrowAmount();

        $this->escrowService->decrease($sum, $seller, $this->getCryptoCurrency());

        return $this;
    }

    /**
     * @return self
     */
    protected function updateAd(): self
    {
        /** @var Ad $ad */
        $deal = $this->getDeal();
        $ad = $deal->ad;

        if ($ad->isSale()) {
            $this->updateAdMax();
        } elseif ($ad->liquidity_required) {
            $this->updateAdMax();
        }

        return $this;
    }

    /**
     * @return CancelDealService
     */
    protected function updateAdMax(): self
    {
        /** @var Deal $deal */
        $deal = $this->getDeal();
        /** @var Ad $ad */
        $ad = $deal->ad;

        if (($ad->max + $deal->fiat_amount) > $ad->original_max) {
            $ad->max = $ad->original_max;
        } else {
            $ad->max += $deal->fiat_amount;
        }

        $ad->save();

        return $this;
    }
}
