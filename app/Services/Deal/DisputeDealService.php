<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/28/18
 * Time: 15:08
 */

namespace App\Services\Deal;

use App\Events\Deal\DealDisputed;
use App\Exceptions\Deal\DealPaidDeadlineException;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class DisputeDealService
 * @package App\Services\Deal
 * @method $this setModel(Model $model) : self
 * @method $this checkDealStatus() : self
 */
class DisputeDealService extends DealService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::IN_DISPUTE;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::PAID,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealDisputed::class;

    /**
     * Set deal status to In Dispute
     *
     * @param Deal $deal
     *
     * @return Deal
     *
     * @throws \Exception
     */
    public function dispute(Deal $deal) : Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);
            $this->setModel($deal)
                 ->checkDealStatus()
                 ->checkDeadline()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent(app(DealDisputed::class, ['deal'=>$this->getDeal(), 'user'=>$this->user]));
            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Check is request was sent according deadline.
     *
     * @return DisputeDealService
     * @throws DealPaidDeadlineException
     */
    protected function checkDeadline() : self
    {
        /** @var Carbon $paidTime */
        $paidTime = $this->getDeal()->getCurrentStatus()->pivot->created_at;
        $deadline = config('app.deal.dispute_deadline');

        $spentTime = $paidTime->diffInMinutes(now());
        if ($spentTime < $deadline) {
            throw new DealPaidDeadlineException(
                'Deal could be disputed after '.$deadline.' minutes after payment!'.
                $spentTime.' minutes have passed now.'
            );
        }
        return $this;
    }
}
