<?php

namespace App\Services\Deal;

use App\Contracts\Services\Deal\CancellationDealServiceContract;
use App\Events\Deal\DealCancellation;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class CancellationDealService
 *
 * @package App\Services\Deal
 * @method CancellationDealService setModel(Model $model) : \App\Services\BaseService
 * @method CancellationDealService checkDealStatus()
 */
class CancellationDealService extends DealService implements CancellationDealServiceContract
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $nextStatusId = DealStatusConstants::CANCELLATION;

    /**
     * Valid deal statuses for applying payment
     *
     * @var array
     */
    protected $currentStatuses = [
        DealStatusConstants::PAID,
        DealStatusConstants::IN_DISPUTE,
        DealStatusConstants::VERIFIED,
    ];

    /**
     * Will be fire after successfully action
     *
     * @var string
     */
    protected $eventClass = DealCancellation::class;

    /**
     * Register cancellation of deal in backend service
     *
     * @param Deal $deal
     *
     * @return Deal
     * @throws \Exception
     */
    public function cancellation(Deal $deal): Deal
    {
        try {
            DB::beginTransaction();
            $deal = Deal::query()->lockForUpdate()->findOrFail($deal->id);

            $this->setModel($deal)
                 ->checkDealStatus()
                 ->addStatus($this->nextStatusId)
                 ->fireEvent($this->eventClass);

            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
