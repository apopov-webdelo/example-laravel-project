<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/4/18
 * Time: 16:36
 */

namespace App\Services\Traits;

use App\Contracts\AuthenticatedContract;
use App\Models\Admin\Admin;
use App\Models\User\User;

/**
 * Trait Userable for services and classes worked with User objects
 *
 * @package App\Services\Traits
 */
trait Userable
{
    /**
     * User will be related with all actions
     *
     * @var User|Admin
     */
    protected $user;

    /**
     * Set user in service object. All actions will be processed by that author
     *
     * @param User|Admin|AuthenticatedContract $user
     * @return self
     */
    public function setUser(AuthenticatedContract $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User|Admin
     */
    public function getUser()
    {
        return $this->user;
    }
}
