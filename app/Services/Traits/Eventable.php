<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/4/18
 * Time: 16:03
 */

namespace App\Services\Traits;

use Illuminate\Contracts\Container\Container;

/**
 * Trait Eventable
 * @package App\Services\Traits
 */
trait Eventable
{
    /**
     * Fire events after successfully storing
     *
     * @param string|object $event
     * @param string|null $bingingAlias
     * @return self
     *
     * @throws \ReflectionException
     * @throws \Exception
     */
    protected function fireEvent($event, $bingingAlias = null) : self
    {
        /** @var Container $container */
        $container = $this->app;
        if (is_string($event)) {
            if ($this->model === null) {
                throw new \Exception('Empty model for firing event!');
            }
            $bingingAlias = $bingingAlias ?? $this->getBindingAliasByEvent(get_class($this->model));
            event($container->make($event, [$bingingAlias => $this->model]));
        } else {
            event($event);
        }
        return $this;
    }

    /**
     * @param string $event
     * @return string
     * @throws \ReflectionException
     */
    protected function getBindingAliasByEvent(string $event) : string
    {
        return camel_case((new \ReflectionClass($event))->getShortName());
    }
}
