<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 14:48
 */

namespace App\Services\Traits;

/**
 * Trait Cancellable
 * @package App\Services\Traits
 */
trait Cancellable
{
    /**
     * Class name for event which will be fired after successfully actions
     *
     * @var string
     */
    protected $cancelEventClass;

    /**
     * Set cancel status to model
     *
     * @return self
     */
    public function cancelModel() : self
    {
        $this->model->statuses()->attach($this->statusModel::findOrFail($this->statusId));

        if ($this->cancelEventClass)
            $this->fireEvent($this->cancelEventClass);
        return $this;
    }

    /**
     * Set event class name for cancelModel method
     *
     * @param string $eventClass
     * @return self
     */
    protected function setCancelEvent(string $eventClass) : self
    {
        $this->cancelEventClass = $eventClass;
        return $this;
    }
}