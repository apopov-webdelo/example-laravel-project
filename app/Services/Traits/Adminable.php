<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/4/18
 * Time: 16:36
 */

namespace App\Services\Traits;

use App\Models\Admin\Admin;

/**
 * Trait Adminable for services and classes worked with Admin objects
 *
 * @package App\Services\Traits
 */
trait Adminable
{
    /**
     * Admin will be related with all actions
     *
     * @var Admin
     */
    protected $user;

    /**
     * Set user in service object. All actions will be processed by that author
     *
     * @param Admin $user
     * @return self
     */
    public function setAdmin(Admin $user)
    {
        $this->user = $user;

        return $this;
    }
}
