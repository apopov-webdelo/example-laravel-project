<?php

namespace App\Services\Traits;

use App\Contracts\AuthenticatedContract;

/**
 * Trait Storable
 *
 * @package App\Services\Traits
 */
trait Storable
{
    /**
     * Class name for event which will be fired after successfully actions
     *
     * @var string
     */
    protected $storeEventClass;

    /**
     * Store new model according transmitted data
     *
     * @param array $data
     * @param bool  $withEvent
     *
     * @return self
     * @throws \Exception
     */
    public function storeModel(array $data, bool $withEvent = true): self
    {
        // must reset model on chain calls
        $this->setModel(new $this->modelClass);

        $this->model->fill($data);

        if ($this->user instanceof AuthenticatedContract) {
            $this->model->author_id = $this->user->id;
        }

        if ($this->model->save()) {
            if (isset($this->storeEventClass) && $withEvent) {
                $this->fireEvent($this->storeEventClass);
            }

            return $this;
        }
        throw new \Exception('Unexpected error during saving ' . $this->modelClass . ' model!');
    }

    /**
     * Set event class
     *
     * @param string $eventClass
     *
     * @return self
     */
    protected function setStoreEventClass(string $eventClass): self
    {
        $this->storeEventClass = $eventClass;

        return $this;
    }
}