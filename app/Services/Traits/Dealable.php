<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/4/18
 * Time: 16:36
 */

namespace App\Services\Traits;

use App\Models\Deal\Deal;

/**
 * Trait Dealable for services and classes worked with Deal objects
 *
 * @package App\Services\Traits
 */
trait Dealable
{
    /**
     * Deal object
     *
     * @var Deal
     */
    protected $deal;

    /**
     * Set deal object in service object
     *
     * @param Deal $deal
     *
     * @return $this
     */
    public function setDeal(Deal $deal)
    {
        $this->deal = $deal;
        return $this;
    }
}
