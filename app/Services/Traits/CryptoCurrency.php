<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/14/18
 * Time: 12:04
 */

namespace App\Services\Traits;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Repositories\Directory\CryptoCurrencyRepo;

/**
 * Trait CryptoCurrency
 *
 * Provide functional for working with CryptoCurrency inside another classes
 *
 * @package App\Services\Traits
 */
trait CryptoCurrency
{
    /**
     * @var \App\Models\Directory\CryptoCurrency
     */
    protected $cryptoCurrency;

    /**
     * @var CryptoCurrencyContract
     */
    protected $cryptoCurrencyContract;

    /**
     * Set CryptoCurrencyContract in service object
     *
     * @param CryptoCurrencyContract $cryptoCurrencyContract
     *
     * @return $this
     */
    public function setCryptoCurrency(CryptoCurrencyContract $cryptoCurrencyContract)
    {
        $this->cryptoCurrencyContract = $cryptoCurrencyContract;

        /** @var CryptoCurrencyRepo $repo */
        $repo = app(CryptoCurrencyRepo::class);
        $this->cryptoCurrency = $repo->getByCode($cryptoCurrencyContract->getCode());
        return $this;
    }

    /**
     * @return \App\Models\Directory\CryptoCurrency
     */
    public function getCryptoCurrency()
    {
        return $this->cryptoCurrency;
    }
}
