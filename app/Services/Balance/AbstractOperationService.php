<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/29/18
 * Time: 11:40
 */

namespace App\Services\Balance;

/**
 * Class AbstractOperationService
 *
 * @package App\Services\Balance
 */
abstract class AbstractOperationService
{
    /**
     * @var string
     */
    protected $descriptionLangAlias;

    /**
     * Service for transactions
     *
     * @var TransactionService
     */
    protected $transactionService;

    /**
     * WithdrawService constructor.
     *
     * @param TransactionService $transactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Return default description for withdraw transactions
     *
     * @return string
     */
    protected function getDefaultDescription(): string
    {
        return empty($this->descriptionLangAlias) ? '' : __($this->descriptionLangAlias);
    }
}
