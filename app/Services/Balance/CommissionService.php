<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/6/18
 * Time: 12:51
 */

namespace App\Services\Balance;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\Balance\Transaction;
use App\Models\Balance\TransactionConstants;
use App\Models\User\User;
use App\Services\Traits\Userable;

/**
 * Class CommissionService
 * @package App\Services\Balance
 */
class CommissionService
{
    use Userable;

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var TransactionService
     */
    protected $transactionService;

    /**
     * @param TransactionService $transactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Set custom description for commission transaction
     *
     * @param string $description
     * @return CommissionService
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Charge commission from user
     *
     * @param int                    $sum
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return Transaction
     * @throws \Exception
     */
    public function charge(int $sum, User $user, CryptoCurrencyContract $cryptoCurrency): Transaction
    {
        if ($user->id == robot()->id) {
            throw new \Exception('Could not use commission service for Robot User!');
        }

        $this->setUser($user);

        return $this->transactionService
                    ->setUser(robot())
                    ->setReason(TransactionConstants::REASON_COMMISSION)
                    ->setDescription($this->getDescription())
                    ->transfer($sum, $user, robot(), $cryptoCurrency);
    }

    /**
     * Generate final description for commission transaction
     *
     * @return string
     */
    protected function getDescription(): string
    {
        return empty($this->description)
            ? __('balance.transaction.commission.default', ['user_id' => $this->user->id])
            : $this->description;
    }
}
