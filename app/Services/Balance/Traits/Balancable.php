<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/4/18
 * Time: 11:54
 */

namespace App\Services\Balance\Traits;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;

/**
 * Trait Balancable
 * @package App\Services\Balance\Traits
 * @property User $user
 */
trait Balancable
{
    /**
     * Cryptocurrency of balance operation
     *
     * @var CryptoCurrency
     */
    protected $cryptoCurrency;

    /**
     * Sum of balance operation
     *
     * @var int
     */
    protected $sum;

    /**
     * Escrow before operation
     *
     * @var int
     */
    protected $escrowBefore;

    /**
     * Balance before operation
     *
     * @var int
     */
    protected $balanceBefore;

    /**
     * @var Balance[]
     */
    private $balance;

    /**
     * Return CryptoCurrency object by ID
     *
     * @param int $cryptoCurrencyId
     * @return CryptoCurrency
     */
    protected function getCryptoCurrency(int $cryptoCurrencyId) : CryptoCurrency
    {
        return app(CryptoCurrency::class)::findOrFail($cryptoCurrencyId);
    }

    /**
     * Set operation crypto currency
     *
     * @param CryptoCurrencyContract $cryptoCurrency
     * @return self
     */
    protected function setCryptoCurrency(CryptoCurrencyContract $cryptoCurrency) : self
    {
        if (! $cryptoCurrency instanceof CryptoCurrency) {
            /** @var CryptoCurrencyRepo $repo */
            $repo = app(CryptoCurrencyRepo::class);
            $cryptoCurrency = $repo->getByCode($cryptoCurrency->getCode());
        }
        $this->cryptoCurrency = $cryptoCurrency;
        return $this;
    }

    /**
     * Set operation sum
     *
     * @param int $sum
     * @return self
     */
    protected function setSum(int $sum) : self
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * Return balance object in crypto currency for user
     *
     * @return Balance
     */
    protected function getBalance() : Balance
    {
        $cacheKey = $this->user->id.'-'.$this->cryptoCurrency->id;
        if (empty($this->balance[$cacheKey])) {
            $this->balance[$cacheKey] = $this->user->getBalance($this->cryptoCurrency);
        }
        return $this->balance[$cacheKey];
    }

    /**
     * Set balance model to private variable
     *
     * @param Balance $balance
     * @return self
     */
    protected function setBalance(Balance $balance) : self
    {
        $cacheKey = $balance->user->id.'-'.$balance->cryptoCurrency->id;
        $this->balance[$cacheKey] = $balance;
        return $this;
    }

    /**
     * Refresh balance model in local variable (cache)
     *
     * @return self
     */
    protected function refreshBalance() : self
    {
        $this->balance = [];
        return $this;
    }
}
