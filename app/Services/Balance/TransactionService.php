<?php

namespace App\Services\Balance;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Events\Balance\TransactionExecuted;
use App\Models\Balance\Balance;
use App\Models\Balance\Transaction;
use App\Models\Balance\TransactionBalanceState;
use App\Models\Balance\TransactionConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Services\StoreService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Service for creation transaction between user's balance
 *
 * @package App\Services\Balance
 * @property Transaction $model
 * @method TransactionService storeModel(array $data, bool $withEvent = true)
 */
class TransactionService extends StoreService
{
    /**
     * Model class for instance for service logic
     *
     * @var string
     */
    protected $modelClass = Transaction::class;

    /**
     * Event will be fire after successfully action
     *
     * @var string
     */
    protected $storeEventClass = TransactionExecuted::class;

    /**
     * Reason for transaction operation
     *
     * @var int
     */
    protected $reasonId = TransactionConstants::REASON_TRANSFER;

    /**
     * @var float
     */
    protected $sum;

    /**
     * @var CryptoCurrency
     */
    protected $cryptoCurrency;

    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * @var TransactionBalanceState
     */
    protected $transactionBalanceState;

    /**
     * @var BalanceService
     */
    protected $balanceService;

    /**
     * Transaction description
     *
     * @var string
     */
    protected $description = '';

    /**
     * @var CryptoCurrencyRepo
     */
    protected $cryptoCurrencyRepo;

    /**
     * TransactionService constructor.
     *
     * @param AuthenticatedContract   $user
     * @param Container               $container
     * @param TransactionBalanceState $transactionBalanceState
     * @param BalanceService          $balanceService
     * @param CryptoCurrencyRepo      $cryptoCurrencyRepo
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        TransactionBalanceState $transactionBalanceState,
        BalanceService $balanceService,
        CryptoCurrencyRepo $cryptoCurrencyRepo
    ) {
        $this->transactionBalanceState = $transactionBalanceState;
        $this->balanceService = $balanceService;
        $this->cryptoCurrencyRepo = $cryptoCurrencyRepo;
        parent::__construct($user, $container);
    }

    /**
     * Set DEAL reason for transaction
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function reasonDeal(): self
    {
        return $this->setReason(TransactionConstants::REASON_DEAL);
    }

    /**
     * Set reason for transaction
     *
     * @param int $reasonId
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function setReason(int $reasonId): self
    {
        if (collect(TransactionConstants::REASONS)->contains($reasonId)) {
            $this->reasonId = $reasonId;

            return $this;
        }
        $reasons = implode(',', TransactionConstants::REASONS);
        throw new \Exception(
            'Unexpected reason ID "' . $reasonId . '" for transaction! Reason list: ' . $reasons . ' .'
        );
    }

    /**
     * Set TRANSFER reason for transaction
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function reasonTransfer(): self
    {
        return $this->setReason(TransactionConstants::REASON_TRANSFER);
    }

    /**
     * Set BLOCKCHAIN reason for transaction
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function reasonBlockchain(): self
    {
        return $this->setReason(TransactionConstants::REASON_BLOCKCHAIN);
    }

    /**
     * Set EXCHANGE reason for transaction
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function reasonExchange(): self
    {
        return $this->setReason(TransactionConstants::REASON_EXCHANGE);
    }

    /**
     * Set CODE reason for transaction
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function reasonCode(): self
    {
        return $this->setReason(TransactionConstants::REASON_CODE);
    }

    /**
     * Set COMMISSION reason for transaction
     *
     * @return TransactionService
     * @throws \Exception
     */
    public function reasonCommission(): self
    {
        return $this->setReason(TransactionConstants::REASON_COMMISSION);
    }

    /**
     * Make transaction in crypto between users
     *
     * @param int                    $sum
     * @param User                   $remitter
     * @param User                   $receiver
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return Transaction
     * @throws \Exception
     */
    public function transfer(
        int $sum,
        User $remitter,
        User $receiver,
        CryptoCurrencyContract $cryptoCurrency
    ): Transaction {
        try {
            DB::beginTransaction();

            $this->setSum($sum)->setCryptoCurrency($cryptoCurrency);

            $remitterBalance = $remitter->getBalance($this->cryptoCurrency, $lockForUpdate = true);
            $receiverBalance = $receiver->getBalance($this->cryptoCurrency, $lockForUpdate = true);

            $data = [
                'amount'      => $this->sum,
                'reason_id'   => $this->reasonId,
                'description' => $this->description,

                'remitter_balance_id' => $remitterBalance->id,
                'receiver_balance_id' => $receiverBalance->id,
            ];

            $this->storeModel($data)
                 ->storeBalanceState($remitterBalance)
                 ->storeBalanceState($receiverBalance)
                 ->updateBalance($remitter, false)
                 ->updateBalance($receiver);

            DB::commit();

            return $this->model;
        } catch (\Exception $exception) {
            DB::rollBack();

            toSlack('TransactionService error: ' . $exception->getMessage(), $exception->getTrace());

            throw $exception;
        }
    }

    /**
     * @param int $sum
     *
     * @return TransactionService
     */
    protected function setSum(int $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return TransactionService
     */
    protected function setCryptoCurrency(CryptoCurrencyContract $cryptoCurrency): self
    {
        $this->cryptoCurrency = $this->cryptoCurrencyRepo->getByCode($cryptoCurrency->getCode());

        return $this;
    }

    /**
     * Create balance state record
     *
     * @param Balance $balance
     *
     * @return TransactionService
     * @throws \Exception
     */
    protected function storeBalanceState(Balance $balance): self
    {
        $data = [
            'transaction_id' => $this->model->id,
            'balance_id'     => $balance->id,
            'amount'         => $balance->amount,
        ];

        if ($this->transactionBalanceState::create($data) instanceof Model) {
            return $this;
        }

        throw new \Exception('Unexpected error during saving transaction balance state!');
    }

    /**
     * Update user balance
     *
     * @param User $user
     * @param bool $deposit
     *
     * @return TransactionService
     * @throws \Exception
     */
    protected function updateBalance(User $user, bool $deposit = true): self
    {
        $method = $deposit ? 'deposit' : 'withdraw';

        if ($this->balanceService->$method($this->sum, $user, $this->cryptoCurrency) instanceof Balance) {
            return $this;
        }

        throw new \Exception('Unexpected error during balance editing. (Operation "' . $method . '")');
    }

    /**
     * Set transaction description
     *
     * @param string $description
     *
     * @return TransactionService
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
