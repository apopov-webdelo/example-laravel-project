<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-02-13
 * Time: 14:41
 */

namespace App\Services\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutCancelServiceContract;
use App\Contracts\Balance\PayOut\PayOutContract;
use App\Events\Balance\PayOut\PayOutCanceled;
use App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException;
use App\Models\Balance\PayOut;
use App\Services\Balance\EscrowService;
use Illuminate\Support\Facades\DB;

/**
 * Class PayOutCancelService
 *
 * @package App\Services\Balance\PayOut
 */
class PayOutCancelService implements PayOutCancelServiceContract
{
    /**
     * Event will be fired after successfully cancellation
     *
     * @var string
     */
    protected $event = PayOutCanceled::class;

    /**
     * Cancellation reason
     *
     * @var string
     */
    private $reason;

    /**
     * Processed PayOut
     *
     * @var PayOutContract
     */
    private $payOut;

    /**
     * @var EscrowService
     */
    private $escrowService;

    /**
     * PayOutCancelService constructor.
     *
     * @param EscrowService $escrowService
     */
    public function __construct(EscrowService $escrowService)
    {
        $this->escrowService = $escrowService;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $reason
     *
     * @return PayOutCancelServiceContract
     */
    public function reason(string $reason): PayOutCancelServiceContract
    {
        $this->reason = $reason;
    }

    /**
     * {@inheritdoc}
     *
     * @param PayOutContract $payOut
     *
     * @return PayOutContract
     * @throws \Exception
     * @throws UnexpectedStatusPayOutException
     */
    public function cancel(PayOutContract $payOut): PayOutContract
    {
        try {
            DB::beginTransaction();

            $this->setPayOut($payOut)
                 ->checkStatus()
                 ->decreaseEscrow()
                 ->setStatus()
                 ->fireEvent();

            DB::commit();

            return $this->payOut;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Set PayOut to private field
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     */
    private function setPayOut(PayOutContract $payOut)
    {
        $this->payOut = $payOut;
        return $this;
    }

    /**
     * Check is current status is valid
     *
     * @return $this
     * @throws UnexpectedStatusPayOutException
     */
    private function checkStatus()
    {
        $validStatus = PayOut::STATUS_PENDING;
        if ($this->payOut->status() == $validStatus) {
            return $this;
        }

        throw new UnexpectedStatusPayOutException(
            "Unexpected status '{$this->payOut->status()}'! Expected '{$validStatus}'."
        );
    }

    /**
     * Decrease escrow according PayOut due amount
     *
     * @return $this
     * @throws \Exception
     */
    private function decreaseEscrow()
    {
        $this->escrowService->decrease(
            $this->payOut->due(),
            $this->payOut->user(),
            $this->payOut->cryptoCurrency()
        );

        return $this;
    }

    /**
     * Set cancelled status for PayOut
     *
     * @return $this
     */
    private function setStatus()
    {
        $this->payOut->setCanceledStatus();
        return $this;
    }

    /**
     * Fire event
     *
     * @return $this
     */
    private function fireEvent()
    {
        event(app($this->event, ['payOut' => $this->payOut]));
        return $this;
    }
}
