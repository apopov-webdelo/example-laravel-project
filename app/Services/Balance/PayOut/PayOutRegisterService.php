<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-21
 * Time: 15:42
 */

namespace App\Services\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutCommissionServiceContract;
use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Balance\PayOut\PayOutRegisterServiceContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\PayOutRepoContract;
use App\Events\Balance\PayOut\PayOutRegistered;
use App\Exceptions\Balance\PayOut\InsufficientBalanceAmountForPayOutException;
use App\Exceptions\Balance\PayOut\TooSmallAmountForPayOutException;
use App\Models\User\User;
use App\Services\Balance\EscrowService;
use App\Services\Traits\Userable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class PayOutRegisterService
 *
 * @package App\Services\Balance\PayOut
 */
class PayOutRegisterService implements PayOutRegisterServiceContract
{
    use Userable;

    /**
     * Event will be fired after successfully registration
     *
     * @var string
     */
    private $eventClass = PayOutRegistered::class;

    /**
     * @var PayOutRepoContract
     */
    private $payOutRepo;

    /**
     * @var Log
     */
    private $logger;

    /**
     * @var EscrowService
     */
    private $escrowService;

    /**
     * @var PayOutCommissionServiceContract
     */
    private $commissionService;

    /**
     * Detect is amount include commission or not
     *
     * @var bool
     */
    private $commissionIncluded;

    /**
     * PayOutRegisterService constructor.
     *
     * @param PayOutRepoContract              $payOutRepo
     * @param Log                             $logger
     * @param EscrowService                   $escrowService
     * @param PayOutCommissionServiceContract $commissionService
     */
    public function __construct(
        PayOutRepoContract $payOutRepo,
        Log $logger,
        EscrowService $escrowService,
        PayOutCommissionServiceContract $commissionService
    ) {
        $this->payOutRepo = $payOutRepo;
        $this->logger = $logger;
        $this->escrowService = $escrowService;
        $this->commissionService = $commissionService;
    }

    /**
     * {@inheritdoc}
     *
     * @param bool $isIncluded
     *
     * @return PayOutRegisterServiceContract
     */
    public function commissionIncluded(bool $isIncluded): PayOutRegisterServiceContract
    {
        $this->commissionIncluded = $isIncluded;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId
     *
     * @return PayOutContract
     *
     * @throws InsufficientBalanceAmountForPayOutException
     * @throws TooSmallAmountForPayOutException
     * @throws \Exception
     */
    public function register(
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency,
        string $walletId
    ): PayOutContract {

        DB::beginTransaction();
        try {
            $this->setUser($user)
                 ->checkBalance($amount, $cryptoCurrency);

            list($payOutAmount, $payOutCommission) = $this->calculateAmountAndCommission($amount, $cryptoCurrency);

            $payOut = $this->payOutRepo->store(
                $user,
                $payOutAmount,
                $payOutCommission,
                $cryptoCurrency,
                $walletId
            );

            $this->increaseEscrow($payOut)->fireEvent($payOut)->log($payOut);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
        return $payOut;
    }

    /**
     * Check is user have needed balance for payout request register
     *
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return $this
     *
     * @throws InsufficientBalanceAmountForPayOutException
     * @throws TooSmallAmountForPayOutException
     */
    private function checkBalance(int $amount, CryptoCurrencyContract $cryptoCurrency)
    {
        if ($this->user->getBalance($cryptoCurrency, true)->amount < $amount) {
            throw new InsufficientBalanceAmountForPayOutException('Insufficient balance amount for payout request!');
        }

        $key       = 'app.balance.commissions.pay_out.'.$cryptoCurrency->getCode().'.min_amount';
        $minAmount = currencyToCoins(config($key), $cryptoCurrency);
        if ($amount < $minAmount) {
            throw new TooSmallAmountForPayOutException(
                __('balance.payout.min_amount_error', [
                        'amount'     => currencyFromCoins($amount, $cryptoCurrency),
                        'min_amount' => currencyFromCoins($minAmount, $cryptoCurrency),
                    ])
            );
        }

        return $this;
    }

    /**
     * Calculate payout amount and commission
     *
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return array
     */
    private function calculateAmountAndCommission(int $amount, CryptoCurrencyContract $cryptoCurrency): array
    {
        $commission = $this->commissionService->calculateCommission($amount, $this->user, $cryptoCurrency);
        $amount = $this->commissionIncluded ? ($amount-$commission) : $amount;
        return [$amount, $commission];
    }

    /**
     * Fire event for successfully PayOut registration
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     */
    private function fireEvent(PayOutContract $payOut)
    {
        event(app($this->eventClass, ['payOut' => $payOut]));
        return $this;
    }

    /**
     * Log success
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     */
    private function log(PayOutContract $payOut)
    {
        $this->logger::info(
            'PayOut request (ID = '.$payOut->id().') was successfully registered.
             User ID = '. $this->user->getId().'.'
        );
        return $this;
    }

    /**
     * Increase user's escrow account
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     * @throws \Exception
     */
    private function increaseEscrow(PayOutContract $payOut)
    {
        $this->escrowService->increase(
            $payOut->due(),
            $payOut->user(),
            $payOut->cryptoCurrency()
        );

        return $this;
    }
}
