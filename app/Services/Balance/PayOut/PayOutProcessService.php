<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-21
 * Time: 16:36
 */

namespace App\Services\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Balance\PayOut\PayOutProcessServiceContract;
use App\Contracts\Balance\WithdrawServiceContract;
use App\Contracts\Repositories\PayOutRepoContract;
use App\Events\Balance\PayOut\PayOutFailed;
use App\Events\Balance\PayOut\PayOutProcessed;
use App\Exceptions\Balance\PayOut\NotUniqueTransactionIdPayOutException;
use App\Services\Balance\CommissionService;
use App\Services\Balance\EscrowService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class PayOutProcessService
 *
 * @package App\Services\Balance\PayOut
 */
class PayOutProcessService implements PayOutProcessServiceContract
{
    /**
     * Lang pattern for commission transaction by PayOut
     *
     * @var string
     */
    protected $commissionLangPath = 'balance.transaction.commission.payout';

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $eventClass = PayOutProcessed::class;

    /**
     * Event will be fired after failed processing
     *
     * @var string
     */
    private $failedEventClass = PayOutFailed::class;

    /**
     * @var PayOutContract
     */
    private $payOut;

    /**
     * @var EscrowService
     */
    private $escrowService;

    /**
     * @var WithdrawServiceContract
     */
    private $withdrawService;

    /**
     * @var CommissionService
     */
    private $commissionService;
    /**
     * @var Log
     */
    private $log;

    /**
     * Transaction ID in blockchain
     *
     * @var string
     */
    private $transactionId;

    /**
     * @var PayOutRepoContract
     */
    private $payOutRepo;

    /**
     * PayOutProcessService constructor.
     *
     * @param EscrowService           $escrowService
     * @param WithdrawServiceContract $withdrawService
     * @param CommissionService       $commissionService
     * @param Log                     $log
     * @param PayOutRepoContract      $payOutRepo
     */
    public function __construct(
        EscrowService $escrowService,
        WithdrawServiceContract $withdrawService,
        CommissionService $commissionService,
        Log $log,
        PayOutRepoContract $payOutRepo
    ) {
        $this->escrowService     = $escrowService;
        $this->withdrawService   = $withdrawService;
        $this->commissionService = $commissionService;
        $this->log               = $log;
        $this->payOutRepo        = $payOutRepo;
    }


    /**
     * {@inheritdoc}
     *
     * @param PayOutContract $payOut
     * @param string         $transactionId
     *
     * @return PayOutContract
     * @throws \Exception
     * @throws NotUniqueTransactionIdPayOutException
     */
    public function process(PayOutContract $payOut, string $transactionId): PayOutContract
    {
        try {
            DB::beginTransaction();

            $this->setPayOut($payOut)
                 ->setTransactionId($transactionId)
                 ->decreaseEscrow()
                 ->withdrawBalance()
                 ->withdrawCommission()
                 ->updatePayOut()
                 ->logSuccess()
                 ->fireEvent();

            DB::commit();

            return $this->payOut;
        } catch (\Exception $exception) {
            $this->logFail($exception)
                 ->fireFailed();
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Set PayOut to private field
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     */
    private function setPayOut(PayOutContract $payOut)
    {
        $this->payOut = $payOut;
        return $this;
    }

    /**
     * Set transaction ID to private field
     *
     * @param string $transactionId
     *
     * @return $this
     * @throws NotUniqueTransactionIdPayOutException
     */
    private function setTransactionId(string $transactionId)
    {
        $this->transactionId = $transactionId;
        $this->checkTransactionId();
        return $this;
    }

    /**
     * Check is transaction ID is unique
     *
     * @throws NotUniqueTransactionIdPayOutException
     */
    private function checkTransactionId()
    {
        if ($this->payOutRepo->filterByTransactionId($this->transactionId)->take()->exists()) {
            throw new NotUniqueTransactionIdPayOutException(
                "Transaction ID ({$this->transactionId}) for PayOut with ID = {$this->payOut->id()} is not unique!"
            );
        }
    }

    /**
     * Decrease payout total sum from escrow account
     *
     * @return $this
     * @throws \Exception
     */
    private function decreaseEscrow()
    {
        $this->escrowService->decrease(
            $this->payOut->due(),
            $this->payOut->user(),
            $this->payOut->cryptoCurrency()
        );
        return $this;
    }

    /**
     * Withdraw amount sum
     *
     * @return $this
     * @throws \Exception
     */
    private function withdrawBalance()
    {
        $transData = [
            'payout_id' => $this->payOut->id(),
            'wallet'    => $this->payOut->walletId(),
        ];

        $this->withdrawService
            ->description(__('balance.transaction.pay_out', $transData))
            ->withdraw(
                $this->payOut->user(),
                $this->payOut->amount(),
                $this->payOut->cryptoCurrency()
            );
        return $this;
    }

    /**
     * Withdraw commission sum
     *
     * @return $this
     * @throws \Exception
     */
    private function withdrawCommission()
    {
        $this->commissionService
            ->setDescription($this->getCommissionDescription())
            ->charge(
                $this->payOut->commission(),
                $this->payOut->user(),
                $this->payOut->cryptoCurrency()
            );
        return $this;
    }

    /**
     * Retrieve description for commission transaction
     *
     * @return string
     */
    private function getCommissionDescription(): string
    {
        return __($this->commissionLangPath, ['payout_id' => $this->payOut->id()]);
    }

    /**
     * Set status PROCESSED for PayOut request
     *
     * @return $this
     */
    private function updatePayOut()
    {
        $this->payOut->setTransactionId($this->transactionId);
        $this->payOut->setProcessedStatus();
        return $this;
    }

    /**
     * Log that PayOut request was processed
     *
     * @return $this
     */
    private function logSuccess()
    {
        $this->log::info('PayOut transaction with ID = '. $this->payOut->id().'was processed.');
        return $this;
    }

    /**
     * Fire success processing event
     *
     * @return $this
     */
    private function fireEvent()
    {
        event(app($this->eventClass, ['payOut' => $this->payOut]));
        return $this;
    }

    /**
     * Log failed processing
     *
     * @param \Exception $exception
     *
     * @return $this
     */
    private function logFail(\Exception $exception)
    {
        $this->log::emergency(
            'Fail during processing PayOut request with ID = '. $this->payOut->id().'!',
            [
                $exception->getMessage()
            ]
        );
        return $this;
    }

    /**
     * Fire failed processing event
     *
     * @return $this
     */
    private function fireFailed()
    {
        event(app($this->failedEventClass, ['payOut' => $this->payOut]));
        return $this;
    }
}
