<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-23
 * Time: 16:30
 */

namespace App\Services\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutCommissionServiceContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

/**
 * Class PayOutCommissionService
 *
 * @package App\Services\Balance\PayOut
 */
class PayOutFixedCommissionService implements PayOutCommissionServiceContract
{
    /**
     * Config data
     *
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $configPath = 'app.balance.commissions.pay_out';

    /**
     * @param array $config
     *
     * @return PayOutFixedCommissionService
     */
    public function setConfig(array $config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param int                    $amount
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param bool                   $commissionIncluded
     *
     * @return int
     *
     * @throws \Exception
     */
    public function calculateCommission(
        int $amount,
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        bool $commissionIncluded = true
    ): int {
        return $this->getCommission($user, $cryptoCurrency);
    }

    /**
     * Retrieve commission for crypto
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return int
     * @throws \Exception
     */
    protected function getCommission(User $user, CryptoCurrencyContract $cryptoCurrency): int
    {
        /*
         * Commission could be customized considering User and Crypto type
         */
        $config     = $this->getConfig();
        $cryptoCode = $cryptoCurrency->getCode();

        if (isset($config[$cryptoCode]['commission'])) {
            return currencyToCoins($config[$cryptoCode]['commission'], $cryptoCurrency);
        }

        throw new \Exception('Could not get payout commission for crypto "'.$cryptoCode.'"');
    }

    /**
     * Retrieve config for PayOut
     *
     * @return array
     */
    protected function getConfig(): array
    {
        return $this->config ?? config($this->configPath);
    }
}
