<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 17:28
 */

namespace App\Services\Balance\PayIn;

use App\Contracts\Balance\DepositServiceContract;
use App\Contracts\Balance\PayIn\PayInProcessServiceContract;
use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Events\Balance\PayIn\PayInTransactionFailed;
use App\Events\Balance\PayIn\PayInTransactionProcessed;
use App\Exceptions\Balance\PayIn\UnexpectedStatusPayInException;
use App\Models\Balance\PayInTransaction;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

/**
 * Class PayInProcessService implement logic for balance transaction by PayInTransaction request
 *
 * @package App\Services\Balance\PayIn
 */
class PayInProcessService implements PayInProcessServiceContract
{
    protected $event = PayInTransactionProcessed::class;

    /**
     * @var PayInTransactionContract
     */
    private $payInTransaction;

    /**
     * @var DepositServiceContract
     */
    private $depositService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PayInProcessService constructor.
     *
     * @param DepositServiceContract $depositService
     * @param Log                    $logger
     */
    public function __construct(DepositServiceContract $depositService, Log $logger)
    {
        $this->depositService = $depositService;
        $this->logger = $logger::channel();
    }


    /**
     * {@inheritdoc}
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @return PayInTransactionContract
     * @throws UnexpectedStatusPayInException
     * @throws \Exception
     */
    public function process(PayInTransactionContract $payInTransaction): PayInTransactionContract
    {
        try {
            $this->setPayInTransaction($payInTransaction);

            /* TODO: Блокировать таблицу или строку PayInTransaction на время обработки для иссключения параллельной обработки */

            $this->depositService->description($this->getPayInDescription())
                                 ->deposit($payInTransaction);

            $payInTransaction->setProcessedStatus();
            $this->logSuccess()->fireSuccessEvent();
            return $payInTransaction;
        } catch (UnexpectedStatusPayInException $exception) {
            $this->logFail($exception);
            throw $exception;
        } catch (\Exception $exception) {
            $this->payInTransaction->setFailedStatus();
            $this->logFail($exception)->fireFailedEvent();
            throw $exception;
        } finally {
            /* TODO: Разблокировать таблицу */
        }
    }

    /**
     * Check and set PayInTransaction to private field
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @throws UnexpectedStatusPayInException
     */
    private function setPayInTransaction(PayInTransactionContract $payInTransaction)
    {
        $this->payInTransaction = $payInTransaction;
        $this->checkStatus($payInTransaction);
    }

    /**
     * Check is status valid for processing
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @return $this
     * @throws UnexpectedStatusPayInException
     */
    private function checkStatus(PayInTransactionContract $payInTransaction)
    {
        if ($payInTransaction->isPending()) {
            return $this;
        }

        throw new UnexpectedStatusPayInException(
            'Unexpected status! Current status is "'.$payInTransaction->status().'".
            Expected "'.PayInTransaction::STATUS_PENDING.'"'
        );
    }

    /**
     * Retrieve description by lang pattern
     *
     * @return string
     */
    private function getPayInDescription(): string
    {
        return __(
            'balance.transaction.pay_in',
            ['transaction_id' => $this->payInTransaction->transactionId()]
        );
    }

    /**
     * Fire processed event
     *
     * @return $this
     */
    private function fireSuccessEvent()
    {
        return $this->fireEvent(PayInTransactionProcessed::class);
    }

    /**
     * Fire needed event class
     *
     * @param string $eventClass
     *
     * @return $this
     */
    private function fireEvent(string $eventClass)
    {
        event(app($eventClass, ['payInTransaction' => $this->payInTransaction]));
        return $this;
    }

    /**
     * Fire processed event
     *
     * @return $this
     */
    private function fireFailedEvent()
    {
        return $this->fireEvent(PayInTransactionFailed::class);
    }

    /**
     * Log success
     *
     * @return PayInProcessService
     */
    private function logSuccess()
    {
        return $this->log(
            'PayIn Transaction #'. $this->payInTransaction->transactionId().' was processed successfully.',
            'info'
        );
    }

    /**
     * Log needed info with required type
     *
     * @param $text
     * @param $type
     * @param \Exception $exception
     *
     * @return $this
     */
    private function log($text, $type, \Exception $exception = null)
    {
        $context = [
            'transaction_id' => $this->payInTransaction->transactionId(),
            'user_id'        => $this->payInTransaction->recipient()->getId(),
            'amount'         => $this->payInTransaction->amount(),
            'crypto_code'    => $this->payInTransaction->cryptoCurrency()->getCode(),
        ];

        if ($exception) {
            $context['errorMessage'] = $exception->getMessage();
        }

        $this->logger->$type($text, $context);
        return $this;
    }

    /**
     * Log failed processing
     *
     * @param \Exception $exception
     *
     * @return PayInProcessService
     */
    private function logFail(\Exception $exception)
    {
        return $this->log(
            'Processing for PayIn Transaction #'. $this->payInTransaction->transactionId().' fallen down!',
            'warning',
            $exception
        );
    }
}
