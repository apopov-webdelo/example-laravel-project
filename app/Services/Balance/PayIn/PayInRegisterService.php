<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 12:22
 */

namespace App\Services\Balance\PayIn;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\PayInTransactionRepoContract;
use App\Events\Balance\PayIn\PayInTransactionStored;
use App\Exceptions\Balance\PayIn\AmountPayInException;
use App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException;
use App\Models\Balance\PayIn\PayInRegisterServiceContract;
use App\Models\Balance\PayInTransaction;
use App\Models\User\User;

/**
 * Class PayInService
 *
 * @package App\Services\Balance
 */
class PayInRegisterService implements PayInRegisterServiceContract
{
    /**
     * @var string
     */
    protected $event = PayInTransactionStored::class;

    /**
     * @var PayInTransactionRepoContract
     */
    private $payInTransactionRepo;

    /**
     * PayInService constructor.
     *
     * @param PayInTransactionRepoContract $payInTransactionRepo
     */
    public function __construct(PayInTransactionRepoContract $payInTransactionRepo)
    {
        $this->payInTransactionRepo = $payInTransactionRepo;
    }

    /**
     * {@inheritdoc}
     *
     * @param string                 $transactionId
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return PayInTransactionContract
     *
     * @throws AmountPayInException
     * @throws ExistingsTransactionIdPayInException
     */
    public function register(
        string $transactionId,
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency
    ): PayInTransactionContract {
        $this->checkAmount($amount)->checkTransactionId($transactionId);

        $payInTransaction = $this->payInTransactionRepo->store(
            $transactionId,
            $user,
            $cryptoCurrency,
            $amount
        );

        $this->fireEvent($payInTransaction);
        return $payInTransaction;
    }

    /**
     * Check is transaction ID is valid
     *
     * @param string $transactionId
     *
     * @return $this
     * @throws ExistingsTransactionIdPayInException
     */
    private function checkTransactionId(string $transactionId)
    {
        if ($this->payInTransactionRepo->isExists($transactionId)) {
            throw new ExistingsTransactionIdPayInException(
                'Transaction with same ID is exists! (TransactionId = '.$transactionId.')'
            );
        }
        return $this;
    }

    /**
     * Check is amount valid
     *
     * @param int $amount
     *
     * @return $this
     * @throws AmountPayInException
     */
    private function checkAmount(int $amount)
    {
        if ($amount > 0) {
            return $this;
        }

        throw new AmountPayInException('Amount must be positive number!');
    }

    /**
     * Fire event for successfully action
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @return $this
     */
    private function fireEvent(PayInTransactionContract $payInTransaction)
    {
        event(app($this->event, ['payInTransaction' => $payInTransaction]));
        return $this;
    }
}
