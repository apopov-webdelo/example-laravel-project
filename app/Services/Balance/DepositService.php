<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/29/18
 * Time: 10:42
 */

namespace App\Services\Balance;

use App\Contracts\Balance\DepositServiceContract;
use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Facades\Robot;
use App\Models\Balance\Transaction;

class DepositService extends AbstractOperationService implements DepositServiceContract
{
    /**
     * @var string
     */
    protected $descriptionLangAlias = 'balance.transaction.deposit';

    /**
     * Custom description
     *
     * @var string
     */
    protected $description;

    /**
     * {@inheritdoc}
     *
     * @param string $description
     *
     * @return WithdrawService
     */
    public function description(string $description): DepositServiceContract
    {
        $this->description = $description;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @return Transaction
     * @throws \Exception
     */
    public function deposit(PayInTransactionContract $payInTransaction): Transaction
    {
        return $this->transactionService
            ->setDescription($this->description ?? $this->getDefaultDescription())
            ->transfer(
                $payInTransaction->amount(),
                Robot::user(),
                $payInTransaction->recipient(),
                $payInTransaction->cryptoCurrency()
            );
    }
}
