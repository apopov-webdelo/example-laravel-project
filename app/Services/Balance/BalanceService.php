<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/3/18
 * Time: 12:49
 */

namespace App\Services\Balance;

use App\Events\Balance\BalanceReplenished;
use App\Events\Balance\BalanceWrittenOff;
use App\Events\Balance\CryptoAmountChanged;
use App\Events\Balance\TotalFiatAmountChanged;
use App\Exceptions\Balance\BalanceAmountException;
use App\Models\Balance\Balance as BalanceModel;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Services\Balance\Traits\Balancable;
use App\Services\BaseService;

/**
 * Class Balance
 *
 * WARNING!!! That service could NOT be used directly!
 * Use ONLY in TransactionService and EscrowService!
 * Directly using broke transactions history!
 *
 * @package App\Services\Balance
 */
class BalanceService extends BaseService
{
    use Balancable;

    /**
     * @var string
     */
    protected $withdrawEventClass = BalanceWrittenOff::class;

    /**
     * @var string
     */
    protected $depositEventClass = BalanceReplenished::class;

    /**
     * Events will be fired for all actions
     *
     * WARNING: That events get User model in constructor
     *
     * @var array
     */
    protected $userEvents = [
        CryptoAmountChanged::class,
        TotalFiatAmountChanged::class,
    ];

    /**
     * Detect is current operation deposit type
     *
     * @var string
     */
    private $depositOperation = true;

    /**
     * Withdraw sum to user balance in needed crypto currency (Using CryptoCurrencyId param)
     *
     * @param int  $sum
     * @param User $user
     * @param int  $cryptoCurrencyId
     *
     * @return BalanceModel
     * @throws BalanceAmountException
     * @throws \Exception
     */
    public function withdrawByCryptoCurrencyId(int $sum, User $user, int $cryptoCurrencyId) : BalanceModel
    {
        return $this->withdraw($sum, $user, $this->getCryptoCurrency($cryptoCurrencyId));
    }

    /**
     * Return CryptoCurrency object by ID
     *
     * @param int $cryptoCurrencyId
     *
     * @return CryptoCurrency
     */
    protected function getCryptoCurrency(int $cryptoCurrencyId) : CryptoCurrency
    {
        return app(CryptoCurrency::class)::findOrFail($cryptoCurrencyId);
    }

    /**
     * Withdraw sum from user balance in needed crypto currency
     *
     * @param int            $sum
     * @param User           $user
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return BalanceModel
     * @throws BalanceAmountException
     * @throws \Exception
     */
    public function withdraw(int $sum, User $user, CryptoCurrency $cryptoCurrency) : BalanceModel
    {
        return $this->setSum($sum)
                    ->setCryptoCurrency($cryptoCurrency)
                    ->setUser($user)
                    ->isAmountEnough()
                    ->setWithdraw()
                    ->editBalance()
                    ->fireEvent(app($this->withdrawEventClass, ['balance' => $this->getBalance()]))
                    ->fireUserEvents()
                    ->getBalance();
    }

    /**
     * Check if current balance amount enough for withdraw operation
     *
     * @return BalanceService
     * @throws BalanceAmountException
     */
    protected function isAmountEnough() : self
    {
        if ($this->sum > $this->getBalance()->amount) {
            throw new BalanceAmountException('Balance amount is less than withdraw sum!');
        }
        return $this;
    }

    /**
     * Set operation type like DEPOSIT
     *
     * @return self
     */
    protected function setDeposit(): self
    {
        $this->depositOperation = true;
        return $this;
    }

    /**
     * Set operation type like WITHDRAW
     *
     * @return self
     */
    protected function setWithdraw(): self
    {
        $this->depositOperation = false;
        return $this;
    }

    /**
     * Edit balance according service sum and operation type
     *
     * @return BalanceService
     * @throws \Exception
     */
    protected function editBalance(): self
    {

        $this->getBalance()->amount += $this->isDeposit()
            ? $this->sum
            : $this->sum * (-1);

        if ($this->getBalance()->save()) {
            return $this;
        }

        throw new \Exception('Unexpected error during balance updating!');
    }

    /**
     * Fire all user events list
     *
     * @return BalanceService
     */
    protected function fireUserEvents(): self
    {
        foreach ($this->userEvents as $event) {
            event($this->app->make($event, ['user' => $this->user]));
        }
        return $this;
    }

    /**
     * Deposit sum to user balance in needed crypto currency (Using CryptoCurrencyId param)
     *
     * @param int  $sum
     * @param User $user
     * @param int  $cryptoCurrencyId
     *
     * @return BalanceModel
     * @throws \Exception
     */
    public function depositByCryptoCurrencyId(int $sum, User $user, int $cryptoCurrencyId): BalanceModel
    {
        return $this->deposit($sum, $user, $this->getCryptoCurrency($cryptoCurrencyId));
    }

    /**
     * Deposit sum to user balance in needed crypto currency
     *
     * @param int            $sum
     * @param User           $user
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return BalanceModel
     * @throws \Exception
     */
    public function deposit(int $sum, User $user, CryptoCurrency $cryptoCurrency): BalanceModel
    {
        return $this->setCryptoCurrency($cryptoCurrency)
                    ->setSum($sum)
                    ->setUser($user)
                    ->setDeposit()
                    ->editBalance()
                    ->fireEvent(app($this->depositEventClass, ['balance' => $this->getBalance()]))
                    ->fireUserEvents()
                    ->getBalance();
    }

    /**
     * Check is current operation deposit
     *
     * @return bool
     */
    protected function isDeposit(): bool
    {
        return $this->depositOperation;
    }

    /**
     * Check is current operation withdraw
     *
     * @return bool
     */
    protected function isWithdraw(): bool
    {
        return $this->depositOperation == false;
    }
}
