<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/25/19
 * Time: 19:06
 */

namespace App\Services\Balance\Admin\Dashboard;

use App\Contracts\Balance\Admin\Dashboard\TotalSummaryStorageContract;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Balance\TransactionRepo;

/**
 * Class TotalSummaryStorage
 *
 * @package App\Services\Balance\Admin\Dashboard
 */
class TotalSummaryStorage implements TotalSummaryStorageContract
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Balance
     */
    private $balance;

    /**
     * @var TransactionRepo
     */
    private $withdrawRepo;

    /**
     * @var TransactionRepo
     */
    private $depositRepo;

    /**
     * TotalSummaryStorage constructor.
     *
     * @param User            $user
     * @param Balance         $balance
     * @param TransactionRepo $withdrawRepo
     * @param TransactionRepo $depositRepo
     */
    public function __construct(
        User $user,
        Balance $balance,
        TransactionRepo $withdrawRepo,
        TransactionRepo $depositRepo
    ) {
        $this->user           = $user;
        $this->balance        = $balance;
        $this->withdrawRepo   = $withdrawRepo;
        $this->depositRepo    = $depositRepo;
    }

    /**
     * @return TransactionRepo
     */
    public function deposits(): TransactionRepo
    {
        return $this->depositRepo->filterReceiverBalance($this->balance);
    }

    /**
     * @return TransactionRepo
     */
    public function withdraws(): TransactionRepo
    {
        return $this->withdrawRepo->filterRemitterBalance($this->balance);
    }

    /**
     * @return User
     */
    public function user(): User
    {
        return $this->user;
    }

    /**
     * @return CryptoCurrency
     */
    public function cryptoCurrency(): CryptoCurrency
    {
        return $this->balance->cryptoCurrency;
    }
}