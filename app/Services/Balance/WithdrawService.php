<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/29/18
 * Time: 11:14
 */

namespace App\Services\Balance;

use App\Contracts\Balance\WithdrawServiceContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Facades\Robot;
use App\Models\Balance\Transaction;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;

/**
 * Class WithdrawService
 *
 * Make withdraw transaction for decrease user's balance
 *
 * @package App\Services\Balance
 */
class WithdrawService extends AbstractOperationService implements WithdrawServiceContract
{
    /**
     * @var string
     */
    protected $descriptionLangAlias = 'balance.transaction.withdraw';

    /**
     * Custom description
     *
     * @var string
     */
    protected $description;

    /**
     * {@inheritdoc}
     *
     * @param string $description
     *
     * @return WithdrawService
     */
    public function description(string $description): WithdrawServiceContract
    {
        $this->description = $description;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param User           $user
     * @param int            $amount
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return Transaction
     * @throws \Exception
     */
    public function withdraw(
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency
    ): Transaction {
        return $this->transactionService
                    ->setDescription($this->description ?? $this->getDefaultDescription())
                    ->transfer(
                        $amount,
                        $user,
                        Robot::user(),
                        $cryptoCurrency
                    );
    }
}
