<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/4/18
 * Time: 11:29
 */

namespace App\Services\Balance;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Events\Balance\EscrowDecreased;
use App\Events\Balance\EscrowIncreased;
use App\Models\Balance\Balance as BalanceModel;
use App\Models\Balance\EscrowState;
use App\Models\User\User;
use App\Services\Balance\Traits\Balancable;
use App\Services\Traits\Eventable;
use App\Services\Traits\Userable;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Escrow
 *
 * @package App\Services\Balance
 */
class EscrowService
{
    use Balancable, Eventable, Userable;

    /**
     * @var string
     */
    protected $increaseEventClass = EscrowIncreased::class;

    /**
     * @var string
     */
    protected $decreasedEventClass = EscrowDecreased::class;

    /**
     * @var BalanceService
     */
    protected $balanceService;

    /**
     * @var Container
     */
    protected $app;

    /**
     * @var EscrowState
     */
    protected $escrowState;

    /**
     * @param BalanceService $balanceService
     * @param EscrowState    $transactionEscrowState
     * @param Container      $container
     */
    public function __construct(
        BalanceService $balanceService,
        EscrowState $transactionEscrowState,
        Container $container
    ) {
        $this->balanceService = $balanceService;
        $this->escrowState = $transactionEscrowState;
        $this->app = $container;
    }

    /**
     * Increase escrow and withdraw sum from balance amount by User and CryptoCurrency
     *
     * @param int                    $sum
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return BalanceModel
     * @throws \Exception
     */
    public function increase(int $sum, User $user, CryptoCurrencyContract $cryptoCurrency): BalanceModel
    {
        $this->setCryptoCurrency($cryptoCurrency)
             ->setUser($user)
             ->rememberStates();

        return $this->increaseByBalance($sum, $this->getBalance());
    }

    /**
     * Decrease escrow and withdraw sum from balance amount by User and CryptoCurrency
     *
     * @param int                    $sum
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return BalanceModel
     * @throws \Exception
     */
    public function decrease(int $sum, User $user, CryptoCurrencyContract $cryptoCurrency): BalanceModel
    {
        $this->setCryptoCurrency($cryptoCurrency)
             ->setUser($user)
             ->rememberStates();

        return $this->decreaseByBalance($sum, $this->getBalance());
    }

    /**
     * Increase escrow and withdraw sum from balance amount
     *
     * @param int          $sum
     * @param BalanceModel $balance
     *
     * @return BalanceModel
     * @throws \Exception
     */
    protected function increaseByBalance(int $sum, BalanceModel $balance): BalanceModel
    {
        $this->setSum($sum)->setBalance($balance);

        DB::beginTransaction();

        try {
            $this->balanceService->withdraw($this->sum, $this->getBalance()->user, $this->cryptoCurrency);
            $this->changeEscrowValue();
            $this->storeEscrowState();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

        return $this->fireEvent($this->getEvent($this->increaseEventClass))
                    ->getBalance();
    }

    /**
     * Decrease escrow and withdraw sum from balance amount
     *
     * @param int          $sum
     * @param BalanceModel $balance
     *
     * @return BalanceModel
     * @throws \Exception
     */
    protected function decreaseByBalance(int $sum, BalanceModel $balance): BalanceModel
    {
        $this->setSum($sum)->setBalance($balance);

        DB::beginTransaction();

        try {
            $this->balanceService->deposit($this->sum, $this->getBalance()->user, $this->cryptoCurrency);
            $this->changeEscrowValue(false);
            $this->storeEscrowState();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

        return $this->fireEvent($this->getEvent($this->decreasedEventClass))
                    ->getBalance();
    }

    /**
     * Create escrow state record
     *
     * @return EscrowService
     * @throws \Exception
     */
    protected function storeEscrowState(): self
    {
        $balance = $this->getBalance()->refresh();

        $data = [
            'balance_id'     => $balance->id,
            'escrow_before'  => $this->escrowBefore,
            'escrow'         => $this->sum,
            'escrow_after'   => $balance->escrow,
            'balance_before' => $this->balanceBefore,
            'balance_after'  => $balance->amount,
        ];

        if ($this->escrowState::create($data) instanceof Model) {
            return $this;
        }

        throw new \Exception('Unexpected error during saving escrow state!');
    }

    /**
     * Change escrow value in balance model according operation type (increase or decrease)
     *
     * @param bool $isIncrease
     *
     * @return EscrowService
     * @throws \Exception
     */
    protected function changeEscrowValue(bool $isIncrease = true): self
    {
        $balance = $this->getBalance()->refresh();

        $balance->escrow += $isIncrease
            ? $this->sum
            : $this->sum * (-1);

        if ($balance->escrow < 0) {
            toSlack('Unexpected escrow. Less than 0!', [
                'status'         => 'Escrow decrease aborted.',
                'user'           => $this->user->id,
                'escrow_before'  => $this->escrowBefore,
                'escrow'         => $this->sum,
                'escrow_after'   => $balance->escrow,
                'balance_before' => $this->balanceBefore,
                'balance_after'  => $balance->amount,
            ]);
            throw new \Exception('Unexpected escrow. Less than 0!');
        }

        if ($balance->save()) {
            return $this;
        }

        throw new \Exception('Unexpected error during saving balance model!');
    }

    /**
     * Return instance of event object
     *
     * @param string $class
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    protected function getEvent(string $class)
    {
        return app($class, ['balance' => $this->getBalance()]);
    }

    /**
     * Remember state before changes
     */
    private function rememberStates(): self
    {
        $this->escrowBefore = $this->getBalance()->escrow;
        $this->balanceBefore = $this->getBalance()->amount;

        return $this;
    }
}
