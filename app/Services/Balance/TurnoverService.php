<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/5/18
 * Time: 10:39
 */

namespace App\Services\Balance;

use App\Events\Balance\TurnoverDecreased;
use App\Events\Balance\TurnoverIncreased;
use App\Models\Balance\Balance;
use App\Models\Deal\Deal;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Services\Balance\Traits\Balancable;
use App\Services\Traits\Eventable;
use App\Services\Traits\Userable;
use Illuminate\Contracts\Container\Container;

class TurnoverService
{
    use Balancable, Eventable, Userable;

    /**
     * @var Container
     */
    protected $app;

    /**
     * @var string
     */
    protected $increaseEventClass = TurnoverIncreased::class;

    /**
     * @var string
     */
    protected $decreaseEventClass = TurnoverDecreased::class;

    /**
     * BaseService constructor for all services.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->app = $container;
    }

    /**
     * Increase turnover for user balance
     *
     * @param int            $sum
     * @param User           $user
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return Balance
     * @throws \Exception
     */
    public function increase(int $sum, User $user, CryptoCurrency $cryptoCurrency): Balance
    {
        return $this->setUser($user)
                    ->setCryptoCurrency($cryptoCurrency)
                    ->setSum($sum)
                    ->refreshBalance()
                    ->editTurnover();
    }

    /**
     * Edit turnover field in balance
     *
     * @param bool $isIncrease
     * @return Balance
     * @throws \Exception
     */
    protected function editTurnover(bool $isIncrease = true): Balance
    {
        $balance = $this->getBalance();
        $balance->turnover += $isIncrease
            ? $this->sum
            : $this->sum * (-1);

        if ($balance->save()) {
            $eventClass = $isIncrease
                ? $this->increaseEventClass
                : $this->decreaseEventClass;
            $this->fireEvent(app($eventClass, ['balance' => $balance]));
            return $balance;
        }
        throw new \Exception('Unexpected error during balance saving!');
    }

    /**
     * Edit turnover field in balance according deal data
     *
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function increaseByDeal(Deal $deal): Bool
    {
        $crypto = $deal->ad->cryptoCurrency;
        $sum    = $deal->crypto_amount;
        $this->increase($sum, $deal->getBuyer(), $crypto);
        $this->increase($sum, $deal->getSeller(), $crypto);
        return true;
    }
}
