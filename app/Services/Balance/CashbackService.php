<?php

namespace App\Services\Balance;

use App\Contracts\Balance\CashbackServiceContract;
use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Facades\Robot;
use App\Models\Balance\Transaction;

class CashbackService extends AbstractOperationService implements CashbackServiceContract
{
    /**
     * @var string
     */
    protected $descriptionLangAlias = 'balance.transaction.cashback';

    /**
     * Custom description
     *
     * @var string
     */
    protected $description;

    /**
     * {@inheritdoc}
     *
     * @param string $description
     *
     * @return CashbackServiceContract
     */
    public function description(string $description): CashbackServiceContract
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Increase user's balance
     *
     * @param DealCashbackCommissionStorageContract $commissionStorage
     *
     * @return Transaction
     * @throws \Exception
     */
    public function cashback(DealCashbackCommissionStorageContract $commissionStorage): Transaction
    {
        return $this->transactionService
            ->setDescription($this->description ?? $this->getDefaultDescription())
            ->transfer(
                $commissionStorage->amount(),
                Robot::user(),
                $commissionStorage->user(),
                $commissionStorage->cryptoCurrency()
            );
    }
}
