<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/6/18
 * Time: 16:11
 */

namespace App\Services\Ad;

use App\Models\Ad\Ad;
use App\Models\Ad\AdStatusConstants;

/**
 * Class CancelAdService
 * @package App\Services\Ad
 */
class ActivateAdService extends StateAdService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $statusId = AdStatusConstants::ACTIVE;

    /**
     * Current status expected in model
     *
     * @var int
     */
    protected $currentStatusId = AdStatusConstants::BLOCKED;

    /**
     * Deactivate transmitted ad
     *
     * @param Ad $ad
     * @return Ad
     * @throws \Exception
     */
    public function activate(Ad $ad) : Ad
    {
        return $this->setModel($ad)->changeState();
    }
}