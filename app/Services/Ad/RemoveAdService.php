<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/26/18
 * Time: 13:48
 */

namespace App\Services\Ad;

use App\Contracts\AuthenticatedContract;
use App\Events\Ad\AdDeleted;
use App\Models\Ad\Ad;
use App\Services\BaseService;
use Illuminate\Contracts\Container\Container;

/**
 * Class RemoveAdService
 * @package App\Services\Ad
 * @property Ad $model
 */
class RemoveAdService extends BaseService
{

    /**
     * Event will be fired after successfully action
     *
     * @var string
     */
    protected $event = AdDeleted::class;

    /**
     * @var DeactivateAdService
     */
    protected $deactivateAdService;

    /**
     * @param AuthenticatedContract $user
     * @param Container $container
     * @param DeactivateAdService $deactivateAdService
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        DeactivateAdService $deactivateAdService
    ) {
        parent::__construct($user, $container);
        $this->deactivateAdService = $deactivateAdService;
    }

    /**
     * Remove ad using soft delete
     *
     * @param Ad $ad
     * @return Ad
     * @throws \Exception
     */
    public function destroy(Ad $ad) : Ad
    {
        $this->setModel($ad);

        if ($ad->is_active) {
            $this->deactivateAdService->deactivate($ad);
        }

        if ($ad->delete()) {
            $this->fireEvent($this->event);
            return $ad;
        }

        throw new \Exception('Unexpected error during Ad model deleting!');
    }
}