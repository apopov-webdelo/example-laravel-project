<?php

namespace App\Services\Ad;

use App\Services\BaseService;

abstract class AbstractUpdateAdService extends BaseService
{
    /**
     * Adapt data for update
     *
     * @var array
     */
    protected $data = [];

    /**
     * Events needed after data analyzing
     *
     * @var array
     */
    protected $neededEvents = [];

    /**
     * Fire all needed events
     *
     * @return UpdateAdService
     * @throws \ReflectionException
     */
    protected function fireEvents(): self
    {
        foreach ($this->neededEvents as $event) {
            $this->fireEvent($event);
        }

        return $this;
    }

    /**
     * Convert fiat to coins if has value in request
     *
     * @param $code
     *
     * @return int|mixed
     */
    protected function adaptPrice($code)
    {
        if (isset($this->data[$code])) {
            return currencyToCoins($this->data[$code], $this->model->currency);
        } else {
            return $this->model->{$code};
        }
    }
}
