<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/8/18
 * Time: 07:00
 */

namespace App\Services\Ad;

use App\Events\Ad\AdLimitChanged;
use App\Events\Ad\AdPriceChanged;
use App\Events\Ad\AdUpdated;
use App\Models\Ad\Ad;
use App\Models\Directory\CommissionConstants;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateAdService
 *
 * @package App\Services\Ad
 * @property Ad $model
 * @method UpdateAdService setModel(Model $model) : self
 */
class UpdateAdService extends AbstractUpdateAdService
{
    /**
     * Will be fire after successfully updating
     *
     * @var array
     */
    protected $events = [
        'update' => AdUpdated::class,
        'price'  => AdPriceChanged::class,
        'limit'  => AdLimitChanged::class,
    ];

    /**
     * That fields could not be updated!
     *
     * @var array
     */
    protected $protectedFields = [
        'currency_id',
        'crypto_currency_id',
        'payment_system_id',
        'country_id',
        'is_sale',
    ];

    /**
     * Update Ad model according data array
     *
     * @param Ad    $ad
     * @param array $data
     *
     * @return Ad
     *
     * @throws \Exception
     */
    public function update(Ad $ad = null, array $data = []): Ad
    {
        return $this->setData($ad ?? $this->model, $data)
                    ->stateUpdate()
                    ->checkNeededEvents()
                    ->updateAd()
                    ->model;
    }

    /**
     * Set data for ad updating
     *
     * @param Ad    $ad
     * @param array $data
     *
     * @return UpdateAdService
     *
     * @throws \Exception
     */
    public function setData(Ad $ad, array $data): self
    {
        if (empty($this->model)) {
            if ($ad instanceof Ad) {
                $this->setModel($ad);
            } else {
                throw new \Exception('Ad model was not set in service!');
            }
        }

        if (false == empty($data)) {
            $this->data = $data;
            $this->adaptData();
        }

        return $this;
    }

    /**
     * Remove protected fields from data array, convert prices to coins
     *
     * @return UpdateAdService
     */
    protected function adaptData(): self
    {
        foreach ($this->protectedFields as $key) {
            unset($this->data[$key]);
        }

        foreach (['min', 'max', 'price'] as $code) {
            $this->data[$code] = $this->adaptPrice($code);
        }

        $this->data['original_max'] = $this->data['max'];

        $this->data['is_active'] = $this->data['is_active'] ?? $this->model->is_active;

        if (isset($this->data['commission_percent'])) {
            $percent = str_replace(',', '.', $this->data['commission_percent']);
            $this->data['commission_percent'] = (float)$percent * CommissionConstants::ONE_PERCENT;
        }

        return $this;
    }


    /**
     * @return UpdateAdService
     */
    protected function stateUpdate(): self
    {
        // check if active state will be changed
        if (isset($this->data['is_active']) && $this->data['is_active'] != $this->model->is_active) {
            if ($this->data['is_active'] == 1) {
                $service = app(ActivateAdService::class);
                $method = 'activate';
            } else {
                $service = app(DeactivateAdService::class);
                $method = 'deactivate';
            }

            $service->setUser($this->user)->$method($this->model);
        }

        return $this;
    }

    /**
     * Check what events must to be fired
     *
     * @return UpdateAdService
     */
    protected function checkNeededEvents(): self
    {
        // set base update event
        $this->neededEvents[] = $this->events['update'];

        // check if active state will be changed
        if ($this->data['is_active'] != $this->model->is_active) {
            $this->neededEvents[] = $this->data['is_active'] == 1
                ? $this->events['activate']
                : $this->events['deactivate'];
        }

        // check if price will be changed
        if ($this->data['price'] != $this->model->price) {
            $this->neededEvents[] = $this->events['price'];
        }

        // check if limits (min OR max) will be changed
        if (isset($this->data['min']) || isset($this->data['max'])) {
            if ($this->data['min'] != $this->model->min || $this->data['max'] != $this->model->max) {
                $this->neededEvents[] = $this->events['limit'];
            }
        }

        return $this;
    }

    /**
     * Update add model and fire needed events
     *
     * @return UpdateAdService
     * @throws \Exception
     */
    protected function updateAd(): self
    {
        if ($this->model->fill($this->data)->save()) {
            $this->setBanks()
                 ->fireEvents();

            return $this;
        }

        throw new \Exception('Unexpected error during model saving!');
    }

    /**
     * Attach banks list to created Ad
     *
     * @return $this
     */
    protected function setBanks(): self
    {
        if (isset($this->data['banks'])) {
            $this->model->banks()->detach($this->model->banks->pluck('id'));
            $this->model->banks()->attach($this->data['banks'], ['author_id' => $this->user->id]);
        }

        return $this;
    }
}
