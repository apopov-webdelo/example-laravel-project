<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/6/18
 * Time: 16:11
 */

namespace App\Services\Ad;

use App\Events\Ad\AdDeactivated;
use App\Models\Ad\Ad;
use App\Models\Ad\AdStatusConstants;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CancelAdService
 * @package App\Services\Ad
 * @method self setModel(Model $model)
 */
class DeactivateAdService extends StateAdService
{
    /**
     * Status will be set
     *
     * @var int
     */
    protected $statusId = AdStatusConstants::BLOCKED;

    /**
     * Current status expected in model
     *
     * @var int
     */
    protected $currentStatusId = AdStatusConstants::ACTIVE;

    /**
     * Will be fire after cancelation
     *
     * @var string
     */
    protected $eventClass = AdDeactivated::class;

    /**
     * Deactivate transmitted ad
     *
     * @param Ad $ad
     * @return Ad
     * @throws \Exception
     *
     */
    public function deactivate(Ad $ad): Ad
    {
        return $this->setModel($ad)->changeState();
    }
}
