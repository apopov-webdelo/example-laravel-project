<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/27/18
 * Time: 12:36
 */

namespace App\Services\Ad;

use App\Events\Ad\AdActivated;
use App\Exceptions\Ad\UnexpectedAdStatusException;
use App\Models\Ad\Ad;
use App\Models\Ad\AdStatus;
use App\Models\Ad\AdStatusConstants;
use App\Models\User\User;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class StateAdService
 * @package App\Services\Ad
 * @property Ad $model
 * @method self setModel(Model $model)
 * @property User $user
 */
abstract class StateAdService extends BaseService
{

    /**
     * Status will be set
     *
     * @var int
     */
    protected $statusId;

    /**
     * Current status expected in model
     *
     * @var int
     */
    protected $currentStatusId;

    /**
     * Will be fire after cancelation
     *
     * @var string
     */
    protected $eventClass = AdActivated::class;

    /**
     * @var string
     */
    protected $statusModel = AdStatus::class;

    /**
     * Change state to another (active/inactive) for ad model
     *
     * @return Ad
     * @throws \Exception
     */
    protected function changeState() : Ad
    {
        DB::beginTransaction();

        try {
            $this->checkCurrentStatus();

            $this->model->statuses()->attach(
                $this->statusModel::findOrFail($this->statusId),
                [
                    'ip'        => request('ip'),
                    'author_id' => $this->user->id,
                ]
            );

            $this->model->is_active = $this->statusId == AdStatusConstants::ACTIVE;
            $this->model->save();

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

        return $this->fireEvent($this->eventClass)->model;
    }

    /**
     * Check current status
     *
     * @return self
     * @throws \Exception
     */
    protected function checkCurrentStatus() : self
    {
        $modelStatus = $this->model->getCurrentStatus()->id;
        if ($modelStatus == $this->currentStatusId) {
            return $this;
        }

        throw new UnexpectedAdStatusException(
            'Unexpected current status (ID = '.$modelStatus.')!'.
            'Expected status ID = '.$this->currentStatusId.'.'
        );
    }
}
