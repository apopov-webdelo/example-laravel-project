<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 12:34
 */

namespace App\Services\Ad;

use App\Contracts\AuthenticatedContract;
use App\Events\Ad\AdPlaced;
use App\Exceptions\AccessException;
use App\Models\Ad\Ad;
use App\Models\Ad\AdStatus;
use App\Models\Ad\AdStatusConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Services\StoreService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Response;

/**
 * Class StoreAdService
 * @package App\Services\Ad
 * @property Ad $model
 */
class StoreAdService extends StoreService
{
    /**
     * @var string
     */
    protected $modelClass = Ad::class;

    /**
     * @var string
     */
    protected $storeEventClass = AdPlaced::class;

    /**
     * @var AdStatus
     */
    protected $status;

    /**
     * Data from store request
     *
     * @var array
     */
    protected $data;

    /**
     * @param AuthenticatedContract $user
     * @param Container             $container
     * @param AdStatus              $status
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        AdStatus $status
    ) {
        parent::__construct($user, $container);
        $this->status = $status;
    }

    /**
     * Store new Ad according transmitted data
     *
     * @param array $data
     * @return Ad
     *
     * @throws AccessException
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function store(array $data): Ad
    {
        if ($this->user->cant('store', Ad::class)) {
            throw new AccessException('User have not right for Ads creating!', Response::HTTP_FORBIDDEN);
        }

        return $this->setData($data)
                    ->adaptPrices()
                    ->storeModel($this->data, false)
                    ->setStatus()
                    ->setBanks()
                    ->setCommissionPercent()
                    ->fireEvent($this->storeEventClass)
                    ->savePublicKey()
                    ->model;
    }

    /**
     * Save private key for Ad if supplied
     *
     * @return $this
     */
    protected function savePublicKey()
    {
        if (isset($this->data['public_key'])) {
            $this->model->saveBcKey($this->data['public_key'], 'owner');
        }

        return $this;
    }

    /**
     * Set data to property
     *
     * @param array $data
     *
     * @return $this
     */
    protected function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Attach active status to object
     *
     * @return $this
     */
    protected function setStatus(): self
    {
        $status = $this->status->findOrFail(AdStatusConstants::ACTIVE);
        $this->model->statuses()->attach($status);

        return $this;
    }

    /**
     * @return $this
     */
    protected function setCommissionPercent()
    {
        $this->model->commission_percent = $this->getUser()->getBalance($this->model->cryptoCurrency)->commission;
        $this->model->save();

        return $this;
    }

    /**
     * Attach banks list to created Ad
     *
     * @return $this
     */
    protected function setBanks(): self
    {
        if (isset($this->data['banks'])) {
            $this->model->banks()->attach($this->data['banks'], ['author_id' => $this->user->id]);
        }

        return $this;
    }

    /**
     * Convert fiat prices to coins
     *
     * @return StoreAdService
     */
    private function adaptPrices(): self
    {
        if (isset($this->data['turnover'])) {
            $crypto = CryptoCurrency::find($this->data['crypto_currency_id']);
            $this->data['turnover'] = currencyToCoins($this->data['turnover'], $crypto);
        }

        $currency = Currency::find($this->data['currency_id']);
        $this->data['price'] = currencyToCoins($this->data['price'], $currency);
        $this->data['min'] = currencyToCoins($this->data['min'], $currency);
        $this->data['max'] = currencyToCoins($this->data['max'], $currency);
        $this->data['original_max'] = $this->data['max'];

        return $this;
    }
}
