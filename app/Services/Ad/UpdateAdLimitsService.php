<?php

namespace App\Services\Ad;

use App\Events\Ad\AdLimitChanged;
use App\Models\Ad\Ad;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateAdLimitsService
 *
 * @package App\Services\Ad
 * @property Ad $model
 * @method UpdateAdService setModel(Model $model) : self
 */
class UpdateAdLimitsService extends AbstractUpdateAdService
{
    /**
     * Events needed after data analyzing
     *
     * @var array
     */
    protected $neededEvents = [
        'limit' => AdLimitChanged::class,
    ];

    /**
     * Update Ad model according data array
     *
     * @param Ad    $ad
     * @param array $data
     *
     * @return Ad
     *
     * @throws \Exception
     */
    public function update(Ad $ad = null, array $data = []): Ad
    {
        return $this->setData($ad ?? $this->model, $data)
                    ->updateAd()
                    ->model;
    }

    /**
     * Set data for ad updating
     *
     * @param Ad    $ad
     * @param array $data
     *
     * @return UpdateAdLimitsService
     *
     * @throws \Exception
     */
    public function setData(Ad $ad, array $data): self
    {
        if (empty($this->model)) {
            if ($ad instanceof Ad) {
                $this->setModel($ad);
            } else {
                throw new \Exception('Ad model was not set in service!');
            }
        }

        if (false == empty($data)) {
            $this->data['max'] = $data['max'];
            $this->adaptData();
        }

        return $this;
    }

    /**
     * convert max to coins
     *
     * @return UpdateAdLimitsService
     */
    protected function adaptData(): self
    {
        $this->data['max'] = $this->adaptPrice('max');

        return $this;
    }

    /**
     * Update add model and fire needed events
     *
     * @return UpdateAdLimitsService
     * @throws \Exception
     */
    protected function updateAd(): self
    {
        if ($this->model->fill($this->data)->save()) {
            $this->fireEvents();

            return $this;
        }

        throw new \Exception('Unexpected error during model saving!');
    }
}
