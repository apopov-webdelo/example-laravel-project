<?php

namespace App\Services\User;

use App\Models\User\User;
use App\Models\User\UserChanges;

/**
 * Class SocialRegistrationService
 *
 * @package App\Services\User
 */
class SocialRegistrationService extends \App\Services\RegistrationService
{
    /**
     * @var RegistrationService
     */
    protected $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function store(array $data): \Illuminate\Foundation\Auth\User
    {
        $this->user = $this->registrationService->store($data);

        $this->storeChangebleData($this->user);

        return $this->user;
    }

    /**
     * @return mixed
     */
    protected function fireEvent()
    {
        return true;
    }

    /**
     * @param User $user
     */
    protected function storeChangebleData(User $user)
    {
        app(UserChanges::class)->user()
                               ->associate($user)
                               ->save();
    }
}
