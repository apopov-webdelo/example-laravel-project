<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 14:41
 */

namespace App\Services\User\Note;


use App\Contracts\AuthenticatedContract;
use App\Models\User\Note;
use App\Models\Session\Session;
use App\Models\User\User;


class NoteService
{
    /**
     * @var User $_user
     */
    protected $_user;

    /**
     * @var User $_author
     */
    protected $_author;

    /**
     * NoteService constructor.
     *
     * @param User                  $user
     * @param AuthenticatedContract $author
     */
    public function __construct(User $user, AuthenticatedContract $author)
    {
        $this->_user   = $user;
        $this->_author = $author;
    }

    public function store(string $note)
    {
        return $this->isNoteExists()
            ? $this->_update($note)
            : $this->_store($note);
    }

    /**
     * @return bool
     */
    private function isNoteExists() : bool
    {
        return (bool)$this->getNote();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    private function getNote()
    {
        return $this->_user->notesIncome()->where('author_id', $this->_author->id)->first();
    }

    /**
     * @param string $value
     *
     * @return Note
     */
    private function _store(string $value) : Note
    {
        /** @var Note $note */
        $note = new Note();
        $note->author()->associate($this->_author);
        $note->recipient()->associate($this->_user);

        $note->setValue($value);

        return $note;
    }

    /**
     * @param string $note
     *
     * @return Note
     */
    private function _update(string $note) : Note
    {
        return $this->getNote()->setValue($note);
    }
}