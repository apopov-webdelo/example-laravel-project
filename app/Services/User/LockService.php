<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 13:08
 */

namespace App\Services\User;

use App\Contracts\LockableContract;
use App\Contracts\Services\User\LockServiceContract;
use App\Events\User\Locked;
use App\Models\User\SecurityLogConstants;
use App\Services\BaseService;
use App\Traits\Service\HasSecurityLogs;
use Carbon\Carbon;

/**
 * Class LockService
 * @package App\Services\User
 */
class LockService extends BaseService implements LockServiceContract
{
    use HasSecurityLogs;

    /**
     * @param LockableContract $user
     * @param int              $days
     *
     * @return LockServiceContract
     */
    public function lock(LockableContract $user, int $days): LockServiceContract
    {
        $days = $days ? Carbon::now()->addDays($days) : null;
        if ($user->block(Carbon::now(), $days)) {
            event(app(Locked::class, [ 'user' => $user ]));
        }

        $this->fireBadSecurityLog($this->user, SecurityLogConstants::USER_LOCKED);

        return $this;
    }
}
