<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 14:41
 */

namespace App\Services\User\Session;

use App\Contracts\AuthenticatedContract;
use App\Contracts\GeoLocatorContract;
use App\Contracts\Utils\Session\SessionStorageContract;
use App\Models\Session\Session;
use App\Models\User\User;

class SessionLogService
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var \Torann\GeoIP\GeoIP|\Torann\GeoIP\Location
     */
    protected $locator;

    /**
     * @var SessionStorageContract
     */
    protected $sessionStorage;

    /**
     * SessionService constructor.
     *
     * @param AuthenticatedContract  $user
     * @param SessionStorageContract $sessionStorage
     * @param GeoLocatorContract     $geoLocator
     */
    public function __construct(
        AuthenticatedContract $user,
        SessionStorageContract $sessionStorage,
        GeoLocatorContract $geoLocator
    ) {
        $this->user = $user;
        $this->locator = $geoLocator;
        $this->sessionStorage = $sessionStorage;
    }

    /**
     * @return Session
     */
    public function inGood()
    {
        return $this->addRecord('sign-in', 'good');
    }

    /**
     * @return Session
     */
    public function inProblem()
    {
        return $this->addRecord('sign-in', 'problem');
    }

    /**
     * @return Session
     */
    public function outGood()
    {
        return $this->addRecord('sign-out', 'good');
    }

    /**
     * @return Session
     */
    public function outProblem()
    {
        return $this->addRecord('sign-out', 'problem');
    }

    /**
     * @param string $type
     * @param string $status
     *
     * @return Session
     */
    private function addRecord(string $type, string $status)
    {
        return Session::create([
            'sessionable_id'   => $this->user->id,
            'sessionable_type' => get_class($this->user),
            'ip'               => $this->sessionStorage->ip(),
            'country'          => $this->locator->getCountry($this->sessionStorage->ip()),
            'city'             => $this->locator->getCity($this->sessionStorage->ip()),
            'os'               => $this->sessionStorage->os(),
            'browser'          => $this->sessionStorage->browser(),
            'type'             => $type,
            'status'           => $status,
        ]);
    }
}
