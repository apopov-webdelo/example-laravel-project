<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/12/19
 * Time: 09:13
 */

namespace App\Services\User\Statistics;

use App\Contracts\Services\User\Statistics\UpdateReviewsCoefficientStorageContract;
use App\Contracts\Services\User\UpdateReviewsCoefficientServiceContract;
use App\Models\User\Statistic;

/**
 * Class UpdateReviewsCoefficientService
 *
 * @package App\Services\User\Statistics
 */
class UpdateReviewsCoefficientService implements UpdateReviewsCoefficientServiceContract
{
    /**
     * @var Statistic
     */
    protected $model;

    /**
     * @var UpdateReviewsCoefficientStorageContract
     */
    protected $storage;

    /**
     * UpdateReviewsCoefficientService constructor.
     *
     * @param UpdateReviewsCoefficientStorageContract $storage
     */
    public function __construct(UpdateReviewsCoefficientStorageContract $storage)
    {
        $this->model   = $storage->model();
        $this->storage = $storage;
    }

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function update(): bool
    {
        $this->model->reviews_coefficient = $this->getUserReviewsCoefficient();
        $this->model->save();

        return true;
    }

    private function getUserReviewsCoefficient()
    {
        if ($this->isReviewCoefficientAvailable()) {
            $coef = number_format(
                ($this->getPositiveCoefficient() - $this->getNeutralCoefficient() - $this->getNegativeCoefficient()),
                8
            );

            return ($coef > 0) ? intval($coef * 100000000) : 0;
        } else {
            return 0;
        }
    }

    protected function isReviewCoefficientAvailable()
    {
        return $this->storage->neutralCount() + $this->storage->negativeCount() + $this->storage->positiveCount() >= 10;
    }

    /**
     * @return float
     */
    protected function getPositiveCoefficient()
    {
        return $this->storage->positiveCount() / $this->storage->dealsCount();
    }

    /**
     * @return float
     */
    protected function getNeutralCoefficient()
    {
        return $this->storage->neutralCount() / $this->storage->dealsCount();
    }

    /**
     * @return float
     */
    protected function getNegativeCoefficient()
    {
        return ($this->storage->negativeCount() / $this->storage->dealsCount()) * 10;
    }
}
