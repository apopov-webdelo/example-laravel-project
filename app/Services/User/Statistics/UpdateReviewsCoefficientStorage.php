<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/12/19
 * Time: 09:13
 */

namespace App\Services\User\Statistics;

use App\Contracts\Services\User\Statistics\UpdateReviewsCoefficientStorageContract;
use App\Models\User\Statistic;
use App\Repositories\Deal\DealRepo;
use App\Repositories\User\ReviewRepo;

/**
 * Class UpdateReviewsCoefficientService
 *
 * @package App\Services\User\Statistics
 */
class UpdateReviewsCoefficientStorage implements UpdateReviewsCoefficientStorageContract
{
    /**
     * @var Statistic
     */
    protected $model;

    /**
     * @var DealRepo
     */
    protected $dealRepo;

    /**
     * @var ReviewRepo
     */
    protected $reviewRepo;

    /**
     * @var int
     */
    protected $dealsQuantity;

    /**
     * @var int
     */
    protected $positiveQuantity;

    /**
     * @var int
     */
    protected $neutralQuantity;

    /**
     * @var int
     */
    protected $negativeQuantity;

    /**
     * UpdateReviewsCoefficientService constructor.
     *
     * @param Statistic  $model
     * @param DealRepo   $dealRepo
     * @param ReviewRepo $reviewRepo
     */
    public function __construct(Statistic $model, DealRepo $dealRepo, ReviewRepo $reviewRepo)
    {
        $this->model = $model;
        $this->dealRepo = $dealRepo;
        $this->reviewRepo = $reviewRepo;
    }

    /**
     * @return int
     */
    public function positiveCount(): int
    {
        return ($this->positiveQuantity)
            ? $this->positiveQuantity
            : (int)$this->reviewRepo
                ->reset()
                ->filterPublicPositive($this->model->user)
                ->take()
                ->count();
    }

    /**
     * @return int
     */
    public function negativeCount(): int
    {
        return ($this->negativeQuantity)
            ? $this->negativeQuantity
            : (int)$this->reviewRepo
                ->reset()
                ->filterPublicNegative($this->model->user)
                ->take()
                ->count();
    }

    /**
     * @return int
     */
    public function neutralCount(): int
    {
        return ($this->neutralQuantity)
            ? $this->neutralQuantity
            : (int)$this->reviewRepo
                ->reset()
                ->filterPublicNeutral($this->model->user)
                ->take()
                ->count();
    }

    /**
     * @return int
     */
    public function dealsCount(): int
    {
        return ($this->dealsQuantity)
            ? $this->dealsQuantity
            : $this->dealRepo->filterByAdAuthor($this->model->user)->take()->count();
    }

    /**
     * @return Statistic
     */
    public function model(): Statistic
    {
        return $this->model;
    }
}
