<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/4/18
 * Time: 16:55
 */

namespace App\Services\User\Statistics;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\User\Statistics\UserStatisticsContract;
use App\Exceptions\Statistics\UserWasNotSetException;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use Illuminate\Support\Facades\Log;

/**
 * Class UserStatisticsService
 *
 * @package App\Services\User\Statistics
 */
class UserStatisticsService implements UserStatisticsContract
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var DealRepo
     */
    protected $dealRepo;

    /**
     * Flag show if model was saved
     *
     * @var bool
     */
    protected $isSaved = false;

    /**
     * UserStatisticsService constructor.
     *
     * @param AuthenticatedContract|null $user
     * @param DealRepo                   $dealRepo
     */
    public function __construct(?AuthenticatedContract $user, DealRepo $dealRepo)
    {
        if ($user) {
            $this->setUser($user);
        }
        $this->dealRepo = $dealRepo;
    }

    /**
     * {@inheritdoc}
     *
     * @param User $user
     *
     * @return UserStatisticsContract
     */
    public function setUser(User $user): UserStatisticsContract
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsContract
     *
     * @throws UserWasNotSetException
     */
    public function update(): bool
    {
        return $this->checkUser()
                    ->resetSavedFlag()
                    ->updateFinishedDealsCount()
                    ->updateCanceledDealsCount()
                    ->updateDisputedDealsCount()
                    ->updatePaidDealsCount()
                    ->updateTotalDealsCount()
                    ->updateCancellationPercent()
                    ->save();
    }

    /**
     * Check is user was set
     *
     * @return $this
     *
     * @throws UserWasNotSetException
     */
    protected function checkUser()
    {
        if ($this->user instanceof User) {
            return $this;
        }
        throw new UserWasNotSetException('Was not set user in statistics service!');
    }

    /**
     * Reset saved flag
     *
     * @return $this
     */
    protected function resetSavedFlag()
    {
        $this->isSaved = false;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsService
     *
     * @throws UserWasNotSetException
     */
    public function updateFinishedDealsCount(): UserStatisticsContract
    {
        $this->resetSavedFlag()->checkUser();
        // Log::debug('Finished');

        return $this->updateDealsCount(DealStatusConstants::FINISHED, 'deals_finished_count');
    }

    /**
     * Update deals by statistics
     *
     * @param int    $statusId
     * @param string $field
     *
     * @return UserStatisticsService
     */
    protected function updateDealsCount(int $statusId, string $field): self
    {
        return $this->updateField($this->getDealsCount($statusId), $field);
    }

    /**
     * Set value to statistics model
     *
     * @param        $value
     * @param string $field
     *
     * @return UserStatisticsService
     */
    protected function updateField($value, string $field): self
    {
        $this->user->statistic->$field = $value;

        return $this;
    }

    /**
     * Return count deals for user
     *
     * @param int $statusId
     *
     * @return int
     */
    protected function getDealsCount(int $statusId)
    {
        return $this->resetRepoAndSetUser()
                    ->filterByStatusId($statusId)
                    ->take()->count();
    }

    /**
     * Return count deals for user
     *
     * @param int $statusId
     *
     * @return int
     */
    protected function getDealsCountCanceled(int $statusId)
    {
        return $this->resetRepoAndSetUser()
                    ->filterByStatusId($statusId)
                    ->filterByBuyer($this->user)
                    ->take()->count();
    }

    /**
     * Return clean repo with filter by member
     *
     * @return DealRepo
     */
    protected function resetRepoAndSetUser(): DealRepo
    {
        return $this->dealRepo->reset()->filterByDealMember($this->user);
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsService
     *
     * @throws UserWasNotSetException
     */
    public function updateCanceledDealsCount(): UserStatisticsContract
    {
        $this->checkUser();
        // Log::debug('Canceled');

        return $this->updateField($this->getDealsCountCanceled(DealStatusConstants::CANCELED), 'deals_canceled_count');
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsService
     *
     * @throws UserWasNotSetException
     */
    public function updateDisputedDealsCount(): UserStatisticsContract
    {
        $this->resetSavedFlag()->checkUser();
        // Log::debug('Disputed');

        return $this->updateDealsCount(DealStatusConstants::IN_DISPUTE, 'deals_disputed_count');
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsService
     *
     * @throws UserWasNotSetException
     */
    public function updatePaidDealsCount(): UserStatisticsContract
    {
        $this->resetSavedFlag()->checkUser();
        // Log::debug('Paid');

        return $this->updateDealsCount(DealStatusConstants::PAID, 'deals_paid_count');
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsContract
     *
     * @throws UserWasNotSetException
     */
    public function updateTotalDealsCount(): UserStatisticsContract
    {
        $this->resetSavedFlag()->checkUser();
        // Log::debug('TotalDeals');
        $this->updateField($this->getTotalDeals(), 'deals_count');

        return $this;
    }

    /**
     * Calculate total deals
     *
     * @return int
     */
    protected function getTotalDeals(): int
    {
        $sum = $this->getDealsCountCanceled(DealStatusConstants::CANCELED)
            + $this->getDealsCount(DealStatusConstants::IN_DISPUTE)
            + $this->getDealsCount(DealStatusConstants::PAID)
            + $this->getDealsCount(DealStatusConstants::FINISHED);

        return (int)$sum;
    }

    /**
     * {@inheritdoc}
     *
     * @return UserStatisticsContract
     *
     * @throws UserWasNotSetException
     */
    public function updateCancellationPercent(): UserStatisticsContract
    {
        $canceledDeals = $this->getDealsCountCanceled(DealStatusConstants::CANCELED);
        $totalDeals = $this->getTotalDeals();

        if ($canceledDeals > config('app.deal.cancel_min_limit')
            || $totalDeals > config('app.deal.cancel_total_limit')) {
            $this->resetSavedFlag()->checkUser();
            $percent = $canceledDeals / $totalDeals * 100;
            $this->updateField(ceil($percent), 'deals_cancellation_percent');
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function save(): bool
    {
        if ($this->user->statistic->save()) {
            $this->isSaved = true;

            return true;
        }
        Log::warning('Unexpected error during saving statistics for user ID=' . $this->user->id . '.');

        return false;
    }

    /**
     * Save statistics model if not saved
     */
    public function __destruct()
    {
        if ($this->isSaved) {
            $this->save();
        }
    }
}
