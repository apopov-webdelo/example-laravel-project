<?php

namespace App\Services\User;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\User\UserChangesServiceContract;
use App\Models\User\UserChanges;
use App\Services\BaseService;
use App\Utils\User\UserChangesState;
use Illuminate\Contracts\Container\Container;

/**
 * Class UserChangesService
 *
 * @package App\Services\User
 */
class UserChangesService extends BaseService implements UserChangesServiceContract
{
    /**
     * Model class for instance for service logic
     *
     * @var string Model class for service using
     */
    protected $modelClass = UserChanges::class;

    /**
     * @var UserChangesState
     */
    protected $state;

    /**
     * @var UserChanges
     */
    protected $userChanges;

    public function __construct(AuthenticatedContract $user, Container $container)
    {
        parent::__construct($user, $container);

        $this->refreshState();

        $this->userChanges = $user->changesAvailable;
    }

    /**
     * @param string $login
     *
     * @return bool
     */
    public function loginChange(string $login): bool
    {
        if ($this->state->canSetLogin()) {
            $this->user->login = $login;
            $this->user->save();

            $this->userChanges->login_changed_at = now();
            $this->userChanges->save();

            $this->refreshState();

            return true;
        }

        return false;
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public function emailChange(string $email): bool
    {
        if ($this->state->canSetEmail()) {
            $this->user->email = $email;
            $this->user->save();

            $this->userChanges->email_changed_at = now();
            $this->userChanges->save();

            $this->refreshState();

            return true;
        }

        return false;
    }

    /**
     *
     */
    private function refreshState()
    {
        $this->state = app(UserChangesState::class, ['state' => $this->user->fresh()->changesAvailable]);
    }
}
