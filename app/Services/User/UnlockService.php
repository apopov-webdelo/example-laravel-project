<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:00
 */

namespace App\Services\User;

use App\Contracts\LockableContract;
use App\Contracts\Services\User\UnlockServiceContract;
use App\Events\User\Unlocked;
use App\Models\User\SecurityLogConstants;
use App\Services\BaseService;
use App\Traits\Service\HasSecurityLogs;

/**
 * Class UnlockService
 * @package App\Services\User
 */
class UnlockService extends BaseService implements UnlockServiceContract
{
    use HasSecurityLogs;

    /**
     * Unlock user
     *
     * @param LockableContract $user
     *
     * @return UnlockServiceContract
     */
    public function unlock(LockableContract $user): UnlockServiceContract
    {
        if ($user->unlock()) {
            event(app(Unlocked::class, [ 'user' => $user ]));
        }

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::USER_UNLOCKED);

        return $this;
    }
}
