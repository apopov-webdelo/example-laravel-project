<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:00
 */

namespace App\Services\User;

use App\Contracts\Services\User\UnlockTradeServiceContract;
use App\Contracts\TradeLockableContract;
use App\Events\User\TradeUnlocked;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Services\BaseService;
use App\Traits\Service\HasSecurityLogs;

/**
 * Class UnlockService
 * @package App\Services\User
 */
class UnlockTradeService extends BaseService implements UnlockTradeServiceContract
{
    use HasSecurityLogs;

    /**
     * Unlock user trade
     *
     * @param User|TradeLockableContract $user
     * @return UnlockTradeServiceContract
     */
    public function unlock(TradeLockableContract $user): UnlockTradeServiceContract
    {
        if ($user->unlockTrade()) {
            event(app(TradeUnlocked::class, ['user' => $user]));
        }

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::USER_UNLOCK_TRADE);

        return $this;
    }
}
