<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 13:08
 */

namespace App\Services\User;

use App\Contracts\Services\User\LockTradeServiceContract;
use App\Contracts\TradeLockableContract;
use App\Events\User\TradeLocked;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Services\BaseService;
use App\Traits\Service\HasSecurityLogs;
use Carbon\Carbon;

/**
 * Class LockTradeService
 * @package App\Services\User
 */
class LockTradeService extends BaseService implements LockTradeServiceContract
{
    use HasSecurityLogs;

    /**
     * Lock user trade
     *
     * @param User|TradeLockableContract $user
     * @param int                        $days
     *
     * @return LockTradeServiceContract
     */
    public function lock(TradeLockableContract $user, int $days): LockTradeServiceContract
    {
        if ($user->blockTrade($days ? Carbon::now()->addDays($days) : null)) {
            event(app(TradeLocked::class, ['user' => $user]));
        }

        $this->fireBadSecurityLog($this->user, SecurityLogConstants::USER_LOCK_TRADE);

        return $this;
    }
}
