<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:13
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Exceptions\User\Google2FaAlreadyConnectedException;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Models\User\Google2faConstants;
use App\Traits\Service\HasSecurityLogs;

class Google2FaService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract
     */
    private $user;

    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    private $google2fa;

    /**
     * Google2FaService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
        $this->google2fa = app('pragmarx.google2fa');
    }

    /**
     * @return array
     * @throws Google2FaAlreadyConnectedException
     */
    public function connect()
    {
        if ($this->user->isGoogle2faConnected() && !$this->user->isGoogle2faDeactivated()) {
            throw new Google2FaAlreadyConnectedException();
        }

        $this->user->google2fa_secret = $this->google2fa->generateSecretKey();
        $this->user->save();

        $QRCodeImage = $this->google2fa->getQRCodeInline(
            config('app.name'),
            $this->user->login,
            $this->user->google2fa_secret
        );

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TWOFA_CONNECT);

        return [
            'QRCodeImage'      => $QRCodeImage,
            'google2fa_secret' => $this->user->google2fa_secret
        ];
    }

    /**
     * @return Google2FaService
     */
    public function disconnect()
    {
        $this->user->disableMutator()->google2fa_secret = '';
        if ($this->user->save()) {
            $this->deactivate();
        }

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TWOFA_DISCONNECT);

        return $this;
    }

    /**
     * @return $this
     */
    public function enable()
    {
        $this->user->security->google2fa_status = Google2faConstants::STATUS_ACTIVATED;
        $this->user->security->save();

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TWOFA_ENABLE);

        return $this;
    }

    /**
     * @return $this
     */
    public function disable()
    {
        $this->user->security->google2fa_status = Google2faConstants::STATUS_PAUSED;
        $this->user->security->save();

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TWOFA_DISABLE);

        return $this;
    }

    /**
     * @return $this
     */
    public function deactivate()
    {
        $this->user->security->google2fa_status = Google2faConstants::STATUS_DEACTIVATED;
        $this->user->security->save();

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TWOFA_DEACTIVATE);

        return $this;
    }
}