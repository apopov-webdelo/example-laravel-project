<?php

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Contracts\GeoLocatorContract;
use App\Contracts\Utils\Session\SessionStorageContract;
use App\Models\User\SecurityLog;
use App\Models\User\User;

class SecurityLogService
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var \Torann\GeoIP\GeoIP|\Torann\GeoIP\Location
     */
    protected $locator;

    /**
     * @var SessionStorageContract
     */
    protected $sessionStorage;

    /**
     * SessionService constructor.
     *
     * @param AuthenticatedContract  $user
     * @param SessionStorageContract $sessionStorage
     * @param GeoLocatorContract     $geoLocator
     */
    public function __construct(
        AuthenticatedContract $user,
        SessionStorageContract $sessionStorage,
        GeoLocatorContract $geoLocator
    ) {
        $this->user = $user;
        $this->locator = $geoLocator;
        $this->sessionStorage = $sessionStorage;
    }

    /**
     * @param string      $type
     * @param bool        $status
     * @param string|null $statusDesc
     *
     * @return SecurityLog
     */
    public function addRecord(string $type, bool $status, ?string $statusDesc = null)
    {
        return SecurityLog::create([
            'sessionable_id'   => $this->user->id,
            'sessionable_type' => get_class($this->user),
            'ip'               => $this->sessionStorage->ip(),
            'country'          => $this->locator->getCountry($this->sessionStorage->ip()),
            'city'             => $this->locator->getCity($this->sessionStorage->ip()),
            'os'               => $this->sessionStorage->os(),
            'browser'          => $this->sessionStorage->browser(),
            'type'             => $type,
            'status'           => $status,
            'status_desc'      => $statusDesc,
        ]);
    }
}
