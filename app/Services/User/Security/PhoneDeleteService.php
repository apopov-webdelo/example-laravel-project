<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/27/18
 * Time: 10:25
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Events\User\PhoneConfirmationRequested;
use App\Exceptions\User\PhoneAlreadyExistsException;
use App\Exceptions\User\PhoneDoesntExistException;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;

/**
 * Class PhoneAddService
 *
 * @package App\Services\User\Security
 */
class PhoneDeleteService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * @var string $event
     */
    private $event = PhoneConfirmationRequested::class;

    /**
     * PhoneAddService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * @return bool
     * @throws PhoneDoesntExistException
     */
    public function delete()
    {
        if (!$this->user->phone) {
            throw new PhoneDoesntExistException('Phone doesn\'t exist');
        }

        $this->user
            ->security
            ->fill(['phone_confirm_token' => str_random(5)])
            ->save();

        event(app($this->event, ['user'=>$this->user]));

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::PHONE_DELETE);

        return true;
    }

    /**
     * @return $this
     */
    public function complete()
    {
        if ($this->user->fill(['phone'=>''])->save()) {
            $this->user
                ->security
                ->fill([
                    'phone_confirm_token' => null,
                    'phone_confirmed' => false
                ])->save();
        }

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::PHONE_DELETE_COMPLETE);

        return $this;
    }
}
