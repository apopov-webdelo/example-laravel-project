<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:12
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Events\User\EmailAccessRequested;
use App\Events\User\EmailConfirmationRequested;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;

class EmailService extends BaseContactService
{
    /**
     * @var string $field
     */
    protected $field = 'email';

    /**
     * @var string $changeEvent
     */
    protected $changeEvent = EmailAccessRequested::class;

    /**
     * @var string $confirmChangeEvent
     */
    protected $confirmChangeEvent = EmailConfirmationRequested::class;

    /**
     * EmailService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        parent::__construct($user);
    }

    protected function getAccessToken()
    {
        return str_random(20);
    }

    protected function getConfirmToken()
    {
        return str_random(20);
    }

    /**
     * Security log constant
     *
     * @return mixed
     */
    protected function getSecurityLogChange()
    {
        return SecurityLogConstants::EMAIL_CHANGE;
    }

    /**
     * Security log constant
     *
     * @return mixed
     */
    protected function getSecurityLogConfirmChange()
    {
        return SecurityLogConstants::EMAIL_CONFIRM_CHANGE;
    }

    /**
     * Security log constant
     *
     * @return mixed
     */
    protected function getSecurityLogConfirm()
    {
        return SecurityLogConstants::EMAIL_CONFIRM;
    }
}
