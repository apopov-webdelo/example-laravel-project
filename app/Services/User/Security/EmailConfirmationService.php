<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:12
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Events\User\EmailConfirmationRequested;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;

class EmailConfirmationService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract|User $user
     */
    protected $user;

    /**
     * @var string $confirmationRequestedEvent
     */
    protected $confirmationRequestedEvent = EmailConfirmationRequested::class;

    /**
     * EmailService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * @return $this
     */
    public function startProcess()
    {
        $this->user
            ->security
            ->fill([
                'email_confirmed'     => false,
                'email_confirm_token' => $this->getConfirmToken(),
            ])
            ->save();
        event(app($this->confirmationRequestedEvent, ['user' => $this->user]));

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::EMAIL_CONFIRMATION_START);

        return $this;
    }

    /**
     * @return string
     */
    protected function getConfirmToken()
    {
        return str_random(20);
    }
}
