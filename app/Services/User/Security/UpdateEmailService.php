<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/20/19
 * Time: 19:18
 */

namespace App\Services\User\Security;

use App\Contracts\Services\User\UpdateEmailServiceContract;
use App\Contracts\Services\User\UserChangesServiceContract;
use App\Events\User\EmailAccessRequested;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;
use App\Utils\User\UserChangesState;

/**
 * Class UpdateEmailService
 *
 * @package App\Services\User\Security
 */
class UpdateEmailService implements UpdateEmailServiceContract
{
    use HasSecurityLogs;

    /**
     * @var User
     */
    public $user;

    /**
     * @var UserChangesState
     */
    private $state;

    /**
     * UpdateEmailService constructor.
     *
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user  = $user;
        $this->state = app(UserChangesState::class, ['state' => $this->user->changesAvailable]);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public function update(string $email): bool
    {
        if ($this->state->isOAuthRegistration()) {
            $result = $this->updateBySocialService($email);
        } else {
            $result = $this->updateNative($email);
        }

        if ($result) {
            $this->user->security->email_access_token = $this->getAccessToken();
            $this->user->security->save();

            $this->fireEvent();
        }

        return $result;
    }

    protected function fireEvent()
    {
        event(app(EmailAccessRequested::class, ['user' => $this->user]));
    }

    /**
     * @param string $email
     *
     * @return mixed
     */
    private function updateBySocialService(string $email)
    {
        $service = app(UserChangesServiceContract::class);
        $result  = $service->emailChange($email);

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::EMAIL_CHANGE);

        return $result;
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    private function updateNative(string $email)
    {
        $this->user->email = $email;
        $this->user->save();

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::EMAIL_CHANGE);

        return true;
    }

    /**
     * @return string
     */
    private function getAccessToken()
    {
        return str_random(5);
    }
}
