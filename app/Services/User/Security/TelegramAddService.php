<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/27/18
 * Time: 10:25
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Events\User\TelegramAdded;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;

/**
 * Class PhoneAddService
 *
 * @package App\Services\User\Security
 */
class TelegramAddService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * @var string $event
     */
    private $event = TelegramAdded::class;

    /**
     * PhoneAddService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * @param string $telegramId
     *
     * @return bool
     */
    public function add(string $telegramId)
    {
        if ($this->user->fill([ 'telegram_id' => $telegramId ])->save()) {
            $this->user
                ->security
                ->fill(['telegram_id_confirm_token' => str_random(5)])
                ->save();

            event(app($this->event, ['user'=>$this->user]));

            $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TELEGRAMM_ADD);

            return true;
        }
        return false;
    }

    /**
     * Contact confirmation
     *
     * @return $this
     */
    public function confirm()
    {
        $this->user
            ->security
            ->fill([
                'telegram_id_confirm_token' => null,
                'telegram_id_confirmed'     => true
            ])->save();

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::TELEGRAMM_ADD_CONFIRM);

        return $this;
    }
}
