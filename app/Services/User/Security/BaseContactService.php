<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/25/18
 * Time: 10:43
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;

abstract class BaseContactService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract|User $user
     */
    protected $user;

    /**
     * @var string $field
     */
    protected $field = 'email';

    /**
     * @var string $changeEvent
     */
    protected $changeEvent = '';

    /**
     * @var string $confirmChangeEvent
     */
    protected $confirmChangeEvent = '';

    /**
     * @var string $fieldInactive
     */
    protected $fieldInactive = '';

    /**
     * @var string $fieldAccessToken
     */
    protected $fieldAccessToken = '';

    /**
     * @var string $fieldConfirmToken
     */
    protected $fieldConfirmToken = '';

    /**
     * @var string $fieldConfirmed
     */
    protected $fieldConfirmed = '';

    /**
     * Security log constant
     *
     * @return mixed
     */
    abstract protected function getSecurityLogChange();

    /**
     * Security log constant
     *
     * @return mixed
     */
    abstract protected function getSecurityLogConfirmChange();

    /**
     * Security log constant
     *
     * @return mixed
     */
    abstract protected function getSecurityLogConfirm();

    /**
     * EmailService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user              = $user;
        $this->fieldInactive     = $this->field . '_inactive';
        $this->fieldAccessToken  = $this->field . '_access_token';
        $this->fieldConfirmToken = $this->field . '_confirm_token';
        $this->fieldConfirmed    = $this->field . '_confirmed';
    }

    public function change(string $field)
    {
        if ($this->user->security->{$this->fieldConfirmed}) {
            return $this->changeConfirmed($field);
        }

        return $this->changeNotConfirmed($field);
    }

    /**
     * @param string $field
     *
     * @return $this
     */
    private function changeNotConfirmed(string $field)
    {
        return $this->updateEmailField($field);
    }

    /**
     * Send access code to old contact through notification
     *
     * @param string $field
     *
     * @return $this
     */
    public function changeConfirmed(string $field)
    {
        $this->user
            ->security
            ->fill(
                [
                    $this->fieldInactive    => $field,
                    $this->fieldAccessToken => $this->getAccessToken(),
                ]
            )
            ->save();

        event(app($this->changeEvent, ['user' => $this->user]));

        $this->fireGoodSecurityLog($this->user, $this->getSecurityLogChange());

        return $this;
    }

    /**
     * @return string
     */
    protected function getAccessToken()
    {
        return str_random(5);
    }

    /**
     * Send confirmation code to new contact through notification
     *
     * @return $this
     */
    public function confirmChange()
    {
        return $this->updateEmailField($this->user->security->{$this->fieldInactive});
    }

    /**
     * @param string $field
     *
     * @return BaseContactService
     */
    private function updateEmailField(string $field)
    {
        $this->user
            ->fill([$this->field => $field])
            ->save();

        return $this->confirmUpdateEmail();
    }

    private function confirmUpdateEmail()
    {
        $this->user
            ->security
            ->fill(
                [
                    $this->fieldInactive     => null,
                    $this->fieldAccessToken  => null,
                    $this->fieldConfirmed    => false,
                    $this->fieldConfirmToken => $this->getConfirmToken(),
                ]
            )
            ->save();

        event(app($this->confirmChangeEvent, ['user' => $this->user]));

        $this->fireGoodSecurityLog($this->user, $this->getSecurityLogConfirmChange());

        return $this;
    }

    /**
     * @return string
     */
    protected function getConfirmToken()
    {
        return str_random(5);
    }

    /**
     * Contact confirmation
     *
     * @return $this
     */
    public function confirm()
    {
        $this->user
            ->security
            ->fill(
                [
                    $this->fieldConfirmToken => null,
                    $this->fieldConfirmed    => true,
                ]
            )
            ->save();

        $this->fireGoodSecurityLog($this->user, $this->getSecurityLogConfirm());

        return $this;
    }
}
