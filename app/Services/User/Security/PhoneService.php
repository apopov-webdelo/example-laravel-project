<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:12
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Events\User\PhoneAccessRequested;
use App\Events\User\PhoneConfirmationRequested;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;

class PhoneService extends BaseContactService
{
    /**
     * @var string $field
     */
    protected $field = 'phone';

    /**
     * @var string $changeEvent
     */
    protected $changeEvent = PhoneAccessRequested::class;

    /**
     * @var string $confirmChangeEvent
     */
    protected $confirmChangeEvent = PhoneConfirmationRequested::class;

    /**
     * EmailService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        parent::__construct($user);
    }

    /**
     * Security log constant
     *
     * @return mixed
     */
    protected function getSecurityLogChange()
    {
        return SecurityLogConstants::PHONE_CHANGE;
    }

    /**
     * Security log constant
     *
     * @return mixed
     */
    protected function getSecurityLogConfirmChange()
    {
        return SecurityLogConstants::PHONE_CONFIRM_CHANGE;
    }

    /**
     * Security log constant
     *
     * @return mixed
     */
    protected function getSecurityLogConfirm()
    {
        return SecurityLogConstants::PHONE_CONFIRM;
    }
}
