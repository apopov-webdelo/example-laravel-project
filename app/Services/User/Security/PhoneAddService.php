<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/27/18
 * Time: 10:25
 */

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Events\User\PhoneConfirmationRequested;
use App\Exceptions\User\PhoneAlreadyExistsException;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;

/**
 * Class PhoneAddService
 *
 * @package App\Services\User\Security
 */
class PhoneAddService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * @var string $event
     */
    private $event = PhoneConfirmationRequested::class;

    /**
     * PhoneAddService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * @param string $phone
     *
     * @return mixed
     * @throws PhoneAlreadyExistsException
     */
    public function add(string $phone)
    {
        if ($this->user->phone && $this->user->security->phone_confirmed) {
            throw new PhoneAlreadyExistsException('You can\'t set phone number. Phone is already exists.');
        }

        if ($this->user->fill(['phone' => $phone])->save()) {
            $this->user
                ->security
                ->fill(['phone_confirm_token' => str_random(5)])
                ->save();

            event(app($this->event, ['user'=>$this->user]));

            $this->fireGoodSecurityLog($this->user, SecurityLogConstants::PHONE_ADD);

            return true;
        }
        return false;
    }

    /**
     * @return $this
     */
    public function confirm()
    {
        $this->user
            ->security
            ->fill([
                'phone_confirm_token' => null,
                'phone_confirmed'     => true
            ])->save();

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::PHONE_ADD_CONFIRM);

        return $this;
    }
}