<?php

namespace App\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Models\User\SecurityLogConstants;
use App\Models\User\User;
use App\Traits\Service\HasSecurityLogs;

/**
 * Class ChangePasswordService
 *
 * @package App\Services\User\Security
 */
class ChangePasswordService
{
    use HasSecurityLogs;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * ChangePasswordService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * change password and log success
     *
     * @param string $password
     *
     * @throws \Exception
     */
    public function change(string $password)
    {
        $this->user->invalidateAllTokens();

        $this->user->setPassword($password);

        $this->fireGoodSecurityLog($this->user, SecurityLogConstants::PASSWORD_CHANGED);
    }

    /**
     * log fail of password change with 2fa
     */
    public function logFail2fa()
    {
        $this->fireBadSecurityLog($this->user, SecurityLogConstants::PASSWORD_CHANGE_FAIL_2FA);
    }
}
