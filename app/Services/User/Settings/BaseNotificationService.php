<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 12:10
 */

namespace App\Services\User\Settings;


use App\Contracts\AuthenticatedContract;
use App\Models\User\Settings;
use App\Models\User\User;

/**
 * Class BaseNotificationService
 *
 * @package App\Services\User\Settings
 */
abstract class BaseNotificationService
{
    /**
     * @return string
     */
    abstract protected function getNotificationOption() : string;

    /**
     * @var Settings
     */
    private $_settings;

    /**
     * EmailNotificationsService constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->_settings = $user->settings;
    }

    /**
     * @return bool
     */
    public function enable() : bool
    {
        $this->_settings[$this->getNotificationOption()] = true;
        $this->_settings->save();

        return true;
    }

    /**
     * @return bool
     */
    public function disable() : bool
    {
        $this->_settings[$this->getNotificationOption()] = false;
        $this->_settings->save();

        return true;
    }
}