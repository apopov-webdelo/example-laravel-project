<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 12:10
 */

namespace App\Services\User\Settings;


use App\Contracts\AuthenticatedContract;

/**
 * Class EmailNotificationService
 *
 * @package App\Services\User\Settings
 */
class EmailNotificationService extends BaseNotificationService
{
    /**
     * EmailNotificationService constructor.
     *
     * @param AuthenticatedContract $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        parent::__construct($user);
    }

    /**
     * @return string
     */
    protected function getNotificationOption() : string
    {
        return 'email_notification_required';
    }
}