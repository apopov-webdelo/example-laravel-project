<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 12:10
 */

namespace App\Services\User\Settings;


use App\Contracts\AuthenticatedContract;

/**
 * Class PushNotificationService
 *
 * @package App\Services\User\Settings
 */
class PushNotificationService extends BaseNotificationService
{
    /**
     * PushNotificationService constructor.
     *
     * @param AuthenticatedContract $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        parent::__construct($user);
    }

    /**
     * @return string
     */
    protected function getNotificationOption() : string
    {
        return 'push_notification_required';
    }
}