<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 12:10
 */

namespace App\Services\User\Settings;


use App\Contracts\AuthenticatedContract;

/**
 * Class TelegramNotificationService
 *
 * @package App\Services\User\Settings
 */
class TelegramNotificationService extends BaseNotificationService
{
    /**
     * TelegramNotificationService constructor.
     *
     * @param AuthenticatedContract $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        parent::__construct($user);
    }

    protected function getNotificationOption() : string
    {
        return 'telegram_notification_required';
    }
}