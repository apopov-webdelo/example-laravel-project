<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 12.11.2018
 * Time: 13:43
 */

namespace App\Services\User;

use App\Contracts\Services\User\SyncPermissionsServiceContract;
use App\Events\User\PermissionsSynchronized;
use App\Models\Admin\Admin;

/**
 * Class SyncPermissionsService
 * @package App\Services
 */
class SyncPermissionsService implements SyncPermissionsServiceContract
{
    /**
     * @var Admin $admin
     */
    private $admin;

    /**
     * SyncPermissionsService constructor.
     *
     * @param Admin $admin
     */
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }

    /**
     * @param array $permissions
     * @return $this
     */
    public function syncPermissions(array $permissions)
    {
        $this->admin->syncPermissions(collect($permissions)->pluck('id'));
        event(app(PermissionsSynchronized::class, ['admin' => $this->admin]));
        return $this;
    }
}
