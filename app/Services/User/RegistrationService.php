<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 18:24
 */

namespace App\Services\User;

use App\Events\User\Registered;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\Reputation;
use App\Models\User\Security;
use App\Models\User\Settings;
use App\Models\User\Statistic;
use App\Models\User\User;

/**
 * Class RegistrationService
 * @package App\Services\User
 */
class RegistrationService extends \App\Services\RegistrationService
{
    /**
     * @param array $data
     * @return User
     */
    public function store(array $data) : \Illuminate\Foundation\Auth\User
    {
        $this->userStore($data)
            ->reputationStore()
            ->securityStore()
            ->settingsStore()
            ->statisticStore()
            ->balanceStore()
            ->fireEvent();

        return $this->user;
    }

    /**
     * @param array $data
     * @return RegistrationService
     */
    protected function userStore(array $data) : self
    {
        $createData = [
            'login'     => $data['login'],
            'password'  => bcrypt($data['password'] ?? str_random(10)),
        ];

        if (array_key_exists('email', $data)) {
            $createData['email'] = $data['email'];
        }

        if (array_key_exists('provider', $data)) {
            $createData['provider'] = $data['provider'];
        }

        if (array_key_exists('provider_id', $data)) {
            $createData['provider_id'] = $data['provider_id'];
        }

        $this->user = User::create($createData);

        return $this;
    }

    /**
     * @return RegistrationService
     */
    protected function statisticStore() : self
    {
        $this->associate(Statistic::class);
        return $this;
    }

    /**
     * @return RegistrationService
     */
    protected function balanceStore() : self
    {
        /** @var Balance $balance */
        /** @var array $defaultData */
        $defaultData = config('app.balance.default_balance_values');
        foreach (CryptoCurrency::all() as $cryptoCurrency) {
            $balance = app(Balance::class)->fill($defaultData);
            $balance->user()->associate($this->user);
            $balance->crypto_currency_id = $cryptoCurrency->id;
            $balance->save();
        }
        return $this;
    }

    /**
     * @return RegistrationService
     */
    protected function reputationStore() : self
    {
        /** @var Reputation $model */
        $model = app(Reputation::class);
        $model->rate = config('app.user.default_rate');
        $model->user()->associate($this->user)->save();
        return $this;
    }

    /**
     * @return RegistrationService
     */
    protected function securityStore() : self
    {
        /** @var Security $security */
        $security = app(Security::class);
        $security->email_confirm_token = str_random(20);
        $security->user()->associate($this->user);
        $security->save();

        return $this;
    }

    /**
     * @return RegistrationService
     */
    protected function settingsStore() : self
    {
        $this->associate(Settings::class);
        return $this;
    }

    /**
     * @param string $class
     * @return mixed
     */
    protected function associate(string $class)
    {
        $model = app($class);
        $model->user()->associate($this->user);
        return $model->save();
    }

    /**
     *
     */
    protected function fireEvent()
    {
        event(app(Registered::class, ['user' => $this->user]));
    }
}
