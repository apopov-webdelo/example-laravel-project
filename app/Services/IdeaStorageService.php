<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/26/18
 * Time: 13:00
 */

namespace App\Services;

use App\Contracts\Services\Idea\IdeaStorageContract;

/**
 * Class IdeaStorageService
 *
 * @package App\Services
 */
class IdeaStorageService implements IdeaStorageContract
{
    /**
     * @var string $name
     */
    private $name = '';

    /**
     * @var string $login
     */
    private $login = '';

    /**
     * @var string $email
     */
    private $email = '';

    /**
     * @var string $text
     */
    private $text = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $name
     *
     * @return IdeaStorageService
     */
    public function setName(string $name): IdeaStorageContract
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $email
     *
     * @return IdeaStorageContract
     */
    public function setEmail(string $email): IdeaStorageContract
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param string $login
     *
     * @return IdeaStorageContract
     */
    public function setLogin(string $login = ''): IdeaStorageContract
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @param string $text
     *
     * @return IdeaStorageContract
     */
    public function setText(string $text): IdeaStorageContract
    {
        $this->text = $text;
        return $this;
    }
}
