<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/22/19
 * Time: 17:16
 */
namespace App\Services\Review;

use App\Contracts\Services\Review\ResetingWithBlockReviewServiceContract;
use App\Events\Review\ReviewUpdated;
use App\Models\User\Review;
use App\Models\User\ReviewConstants;
use App\Services\Traits\Eventable;
use Illuminate\Contracts\Container\Container;

/**
 * Class ResetingReviewService
 *
 * @package App\Services\Review
 */
class ResetingWithBlockReviewService implements ResetingWithBlockReviewServiceContract
{
    use Eventable;

    /**
     * @var string
     */
    private $event = ReviewUpdated::class;

    /**
     * @var Container
     */
    private $app;

    /**
     * ResetingReviewService constructor.
     *
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * {@inheritdoc}
     *
     * @param Review $review
     *
     * @return bool
     * @throws \Throwable
     */
    public function resetWithBlock(Review $review): bool
    {
        $review->trust = ReviewConstants::TRUST_NEGATIVE;
        $review->rate  = ReviewConstants::RATE_NEGATIVE_HIDDEN;

        if ($review->saveOrFail()) {
            $this->fireEvent(app($this->event, ['review' => $review]));
        }
        return true;
    }
}
