<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/7/18
 * Time: 09:28
 */

namespace App\Services\Review;

use App\Contracts\AuthenticatedContract;
use App\Events\Review\ReviewPlaced;
use App\Exceptions\Review\ReviewException;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\Review;
use App\Models\User\ReviewConstants;
use App\Models\User\User;
use App\Repositories\User\ReviewRepo;
use App\Services\BaseService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;

/**
 * Service for storing reviews
 *
 * @package App\Services\Review
 * @method $this setModel(Model $model): self
 * @property Review $model
 */
class ReviewService extends BaseService
{
    /**
     * Will be fired after successfully review placed
     *
     * @var string
     */
    protected $event = ReviewPlaced::class;

    /**
     * Deal on which review based
     *
     * @var Deal
     */
    protected $deal;

    /**
     * Review data array
     *
     * @var array
     */
    protected $data;

    /**
     * Recipient of review
     *
     * @var User
     */
    protected $recipient;

    /**
     * @var ReviewRepo
     */
    protected $reviewRepo;

    /**
     * @param AuthenticatedContract $user
     * @param Container $container
     * @param ReviewRepo $reviewRepo
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        ReviewRepo $reviewRepo
    ) {
        parent::__construct($user, $container);
        $this->reviewRepo = $reviewRepo;
    }

    /**
     * Store or update review by deal to to user
     *
     * @param Deal  $deal
     * @param array $data
     *
     * @return Review
     * @throws ReviewException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function store(Deal $deal, array $data): Review
    {
        $this->deal      = $deal;
        $this->data      = array_merge($data, ['deal_id' => $deal->id]);
        $this->recipient = $deal->getAnotherMember($this->user);

        return $this->checkAuthor()
                    ->checkStatus()
                    ->checkTrust()
                    ->checkRate()
                    ->checkMessage()
                    ->setModel($this->updateOrCreateReviewModel())
                    ->fireEvent($this->event)
                    ->model;
    }

    /**
     * Check is review author is valid
     *
     * @return ReviewService
     * @throws ReviewException
     */
    public function checkAuthor(): self
    {
        if ($this->deal->isUserNotDealMember($this->user)) {
            throw new ReviewException('Review author must be deal member!');
        }

        if ($this->user->id == $this->recipient->id) {
            throw new ReviewException('Review author and recipient must be different users!');
        }

        return $this;
    }

    /**
     * Check is status is valid
     *
     * @return ReviewService
     * @throws ReviewException
     */
    public function checkStatus(): self
    {
        if (collect(DealStatusConstants::ACTIVE_STATUSES)->search($this->deal->status->id)) {
            throw new ReviewException('Review status should be finished or canceled or auto-canceled!');
        }

        return $this;
    }

    /**
     * Check if trust is valid
     *
     * @return $this
     *
     * @throws ReviewException
     */
    protected function checkTrust(): self
    {
        $trust = (string)$this->data['trust'];

        if (in_array($trust, ReviewConstants::TRUSTS)) {
            return $this;
        }

        $trusts = implode(',', ReviewConstants::TRUSTS);
        throw new ReviewException('Unexpected trust value: '.$trust.'! Variants: '.$trusts);
    }

    /**
     * Check if rate is valid
     *
     * @return ReviewService
     *
     * @throws ReviewException
     */
    protected function checkRate(): self
    {
        $rate = (int)$this->data['rate'];
        $trust = $this->data['trust'];

        if (in_array($rate, ReviewConstants::RATES[$this->data['trust']])) {
            return $this;
        }

        $rates = implode(',', ReviewConstants::RATES[$this->data['trust']]);
        throw new ReviewException(
            'Unexpected rate value: '.$rate.'! Variants for trust "'.$trust.'": '.$rates
        );
    }

    /**
     * Check if message is valid according rate value
     *
     * @return ReviewService
     *
     * @throws ReviewException
     */
    protected function checkMessage(): self
    {
        if ($this->isRateRequireMessage()) {
            $message = (string)$this->data['message'];
            if (strlen($message) < 10 && strlen($message) > 1000) {
                throw new ReviewException(
                    'Review message must contains 10-1000 symbols!',
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
        } else {
            if (! isset($this->data['message'])) {
                $this->data['message'] = '';
            }
        }
        return $this;
    }

    /**
     * Check condition for message validation
     *
     * @return bool
     */
    protected function isRateRequireMessage(): bool
    {
        return $this->data['rate'] == 0;
    }

    /**
     * Store review model or update if exists
     *
     * @return Review
     * @throws \Exception
     */
    protected function updateOrCreateReviewModel(): Review
    {
        $review = $this->reviewRepo->filterByRecipient($this->recipient)
                                   ->filterByAuthor($this->user)
                                   ->take()->get()->first();

        return $review ? $this->updateReview($review) : $this->storeReview();
    }

    /**
     * Update review model by data array
     *
     * @param Review $review
     *
     * @return Review
     * @throws \Exception
     */
    protected function updateReview(Review $review): Review
    {
        if ($review->fill($this->data)->save()) {
            return $review;
        }

        throw new \Exception('Unexpected errors during review model saving!');
    }

    /**
     * Store new review model
     *
     * @return Review
     * @throws \Exception
     */
    protected function storeReview(): Review
    {
        /** @var Review $review */
        $review = new Review($this->data);
        $review->author()->associate($this->user);
        $review->recipient()->associate($this->deal->getAnotherMember($this->user));

        if ($review->save()) {
            return $review;
        }

        throw new \Exception('Unexpected errors during review model saving!');
    }
}
