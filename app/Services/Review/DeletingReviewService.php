<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/22/19
 * Time: 17:10
 */
namespace App\Services\Review;

use App\Contracts\Services\Review\DeletingReviewServiceContract;
use App\Events\Review\ReviewDeleted;
use App\Models\User\Review;
use App\Services\Traits\Eventable;
use Illuminate\Contracts\Container\Container;

/**
 * Class DeletingReviewService
 *
 * @package App\Services\Review
 */
class DeletingReviewService implements DeletingReviewServiceContract
{
    use Eventable;

    /**
     * @var string
     */
    private $event = ReviewDeleted::class;

    /**
     * ResetingReviewService constructor.
     *
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * {@inheritdoc}
     *
     * @param Review $review
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(Review $review): bool
    {
        if ($review->delete()) {
            $this->fireEvent(app($this->event, ['review' => $review]));
        }
        return true;
    }
}