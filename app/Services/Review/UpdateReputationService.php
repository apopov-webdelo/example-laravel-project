<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/13/18
 * Time: 06:39
 */

namespace App\Services\Review;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\User\Statistics\UpdateReviewsCoefficientStorageContract;
use App\Contracts\Services\User\UpdateReviewsCoefficientServiceContract;
use App\Events\User\ReputationChanged;
use App\Models\User\Reputation;
use App\Models\User\Review;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use App\Repositories\User\ReviewRepo;
use App\Services\BaseService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Model;

/**
 * Service class for user reputation recounting and updating
 *
 * @package App\Services\Review
 * @property User $model
 * @method UpdateReputationService setModel(Model $model) : self
 *
 */
class UpdateReputationService extends BaseService
{

    /**
     * Event will be fired after successfully reputation updating
     *
     * @var string
     */
    protected $event = ReputationChanged::class;

    /**
     * @var ReviewRepo
     */
    protected $reviewRepo;

    /**
     * @var DealRepo
     */
    protected $dealRepo;

    /**
     * UpdateReputationService constructor.
     *
     * @param AuthenticatedContract $user
     * @param Container             $container
     * @param ReviewRepo            $reviewRepo
     * @param DealRepo              $dealRepo
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        ReviewRepo $reviewRepo,
        DealRepo $dealRepo
    ) {
        parent::__construct($user, $container);
        $this->setModel($this->getUser());
        $this->reviewRepo = $reviewRepo;
        $this->dealRepo   = $dealRepo;
    }

    /**
     * @param Review $review
     *
     * @return User
     * @throws \ReflectionException
     */
    public function updateByReview(Review $review): User
    {
        return $this->update($review->recipient);
    }

    /**
     * @param User $user
     *
     * @return User
     * @throws \ReflectionException
     */
    public function update(User $user): User
    {
        return $this->setModel($user)
                    ->updateReputation()
                    ->updateStatistics()
                    ->fireEvent($this->event)
            ->model;
    }

    /**
     * @return UpdateReputationService
     */
    protected function updateReputation(): self
    {
        $reputation                 = $this->model->reputation;
        $reputation->positive_count = $this->getUserPositiveReviewsCount();
        $reputation->neutral_count  = $this->getUserNeutralReviewsCount();
        $reputation->negative_count = $this->getUserNegativeReviewsCount();
        $reputation->rate           = $this->calculateRate($reputation);
        $reputation->save();
        return $this;
    }

    /**
     * @return UpdateReputationService
     */
    protected function updateStatistics(): self
    {
        /** @var UpdateReviewsCoefficientServiceContract $service */
        $storage = app(UpdateReviewsCoefficientStorageContract::class, ['model' => $this->model->statistic]);
        $service = app(UpdateReviewsCoefficientServiceContract::class, ['storage' => $storage]);
        $service->update();

        return $this;
    }

    /**
     * @return int
     */
    protected function getUserPositiveReviewsCount(): int
    {
        return (int)$this->reviewRepo
                         ->reset()
                         ->filterPublicPositive($this->model)
                         ->take()
                         ->count();
    }

    /**
     * @return int
     */
    protected function getUserNegativeReviewsCount(): int
    {
        return (int)$this->reviewRepo
                         ->reset()
                         ->filterPublicNegative($this->model)
                         ->take()
                         ->count();
    }

    /**
     * @return int
     */
    protected function getUserNeutralReviewsCount(): int
    {
        return (int)$this->reviewRepo
                         ->reset()
                         ->filterPublicNeutral($this->model)
                         ->take()
                         ->count();
    }

    /**
     * Calculate rate value according reviews stats
     *
     * @param Reputation $reputation
     *
     * @return int
     */
    protected function calculateRate(Reputation $reputation)
    {
        $total = $reputation->negative_count + $reputation->positive_count;
        if ($total === 0) {
            return config('app.user.default_rate');
        }
        $rate = ($reputation->positive_count / $total) * 100;

        return round($rate);
    }
}
