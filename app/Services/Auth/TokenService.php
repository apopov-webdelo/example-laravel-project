<?php

namespace App\Services\Auth;

use App\Contracts\Services\Auth\TokenServiceContract;
use App\Services\Traits\Userable;
use DateInterval;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Laravel\Passport\Token;

/**
 * Class TokenService
 *
 * @package App\Services\Auth
 */
class TokenService implements TokenServiceContract
{
    use Userable;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container, Request $request)
    {
        $this->request   = $request;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     *
     * @return string
     * @throws \Exception
     */
    public function createNew(): string
    {
        $this->revokeForDevice();

        $token = $this->addNew();

        $this->updateExpireOnNew();

        return $token;
    }

    /**
     * @inheritdoc
     */
    public function revokeForDevice(): void
    {
        $this->user->tokens()
                   ->where('name', $this->getDeviceName())
                   ->where('revoked', false)
                   ->get()
                   ->each(function ($token) {
                       /* @var Token $token */
                       $token->revoke();
                   });
    }

    /**
     * @inheritdoc
     */
    public function revokeAll(): void
    {
        $this->user->tokens()
                   ->where('revoked', false)
                   ->get()
                   ->each(function ($token) {
                       /* @var Token $token */
                       $token->revoke();
                   });
    }

    /**
     * @return Token
     */
    protected function getActive()
    {
        return $this->user->tokens()
                          ->where('revoked', false)
                          ->get()
                          ->first();
    }

    /**
     * @return string
     */
    protected function getDeviceName()
    {
        return md5($this->getUserAgent() . $this->getUserIp());
    }

    /**
     * @return string
     */
    protected function getUserAgent()
    {
        return $this->request->header('User-Agent');
    }

    /**
     * @return string
     */
    protected function getUserIp()
    {
        return $this->request->ip();
    }

    /**
     * Add new token for device
     *
     * @return string
     */
    protected function addNew()
    {
        return $this->user->createToken($this->getDeviceName())->accessToken;
    }

    /**
     * Set token expire by project rules on login/or generate
     *
     * @throws \Exception
     */
    protected function updateExpireOnNew()
    {
        $token = $this->getActive();

        $minutes = $this->request->get('remember_me')
            ? config('auth.token_valid_time_remember')
            : config('auth.token_valid_time');

        $time = $this->getExpireTime($minutes);

        $this->updateExpire($token, $time);
    }

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    public function updateExpireOnVisit(): void
    {
        $token = $this->getActive();

        if ($token) {
            $minutes = config('auth.token_valid_time');

            $time = $this->getExpireTime($minutes);

            if ($token->expires_at <= $time) {
                $this->updateExpire($token, $time);
            }
        }
    }

    /**
     * Set token expire in minutes
     *
     * @param Token  $token
     * @param Carbon $time
     *
     * @throws \Exception
     */
    protected function updateExpire(Token $token, Carbon $time)
    {
        /* @var Carbon $token ->expires_at */
        $token->expires_at = $time;
        $token->save();
    }

    /**
     * @param string $minutes
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function getExpireTime(string $minutes)
    {
        return now()->add(new DateInterval($minutes));
    }
}
