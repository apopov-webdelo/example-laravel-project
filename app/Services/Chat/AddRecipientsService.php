<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:11
 */

namespace App\Services\Chat;

use App\Events\Chat\RecipientsAttached;
use App\Models\Admin\Admin;
use App\Models\Chat\Chat;
use App\Models\User\User;
use App\Services\BaseService;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;

/**
 * Class AddRecipientsServiceService for adding members to chat
 *
 * @package App\Services\Chat
 */
class AddRecipientsService extends BaseService
{
    /**
     * Event class will be fired after recipients attaching to chat
     *
     * @var string
     */
    protected $event = RecipientsAttached::class;

    /**
     * Add recipients to transmitted chat
     *
     * @param Chat            $chat
     * @param Authenticatable ...$users
     *
     * @return bool
     */
    protected function add(Chat $chat, Authenticatable ...$users): bool
    {
        foreach ($users as $user) {
            if ($user instanceof User) {
                $chat->recipients()->attach($user->id);
            }
            if ($user instanceof Admin) {
                $chat->adminRecipients()->attach($user->id);
            }
        }

        return true;
    }

    /**
     * Add list of users to chat
     *
     * @param Chat $chat
     * @param Collection $users
     * @return bool
     * @throws \ReflectionException
     */
    public function addCollection(Chat $chat, Collection $users): bool
    {
        foreach ($users as $user) {
            $this->add($chat, $user);
        }

        $this->setModel($chat)->fireEvent($this->event);

        return true;
    }
}