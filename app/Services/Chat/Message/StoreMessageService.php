<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 14:26
 */

namespace App\Services\Chat\Message;

use App\Contracts\AuthenticatedContract;
use App\Events\Chat\MessagePlaced;
use App\Exceptions\Chat\ChatException;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Repositories\Chat\ChatRepo;
use App\Services\Chat\AddRecipientsService;
use App\Services\Chat\StoreChatService;
use App\Services\StoreService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MessageService
 * @package App\Services\Chat
 */
abstract class StoreMessageService extends StoreService
{
    /**
     * @var string
     */
    protected $modelClass = Message::class;

    /**
     * @var string
     */
    protected $event = MessagePlaced::class;

    /**
     * @var User
     */
    protected $recipient;

    /**
     * @var StoreChatService
     */
    protected $storeChatService;

    /**
     * @var ChatRepo
     */
    protected $chatRepo;

    /**
     * @var AddRecipientsService
     */
    protected $addRecipientsService;

    /**
     * MessageService constructor.
     *
     * @param AuthenticatedContract $user
     * @param Container $container
     * @param StoreChatService $storeChatService
     * @param ChatRepo $chatRepo
     * @param AddRecipientsService $addRecipientsService
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        StoreChatService $storeChatService,
        ChatRepo $chatRepo,
        AddRecipientsService $addRecipientsService
    ) {
        $this->storeChatService     = $storeChatService->setUser($user);
        $this->chatRepo             = $chatRepo;
        $this->addRecipientsService = $addRecipientsService;

        parent::__construct($user, $container);
    }

    /**
     * Set recipient in class param
     *
     * @param User $user
     * @return StoreMessageService
     */
    protected function setRecipient(User $user): self
    {
        $this->recipient = $user;
        return $this;
    }

    /**
     * Check if author is blocked by recipient and throw ChatException
     *
     * @param Collection|null $users
     * @return StoreMessageService
     * @throws ChatException
     */
    protected function checkUsersInBlacklistAndThrowException(Collection $users = null): self
    {
        if ($users instanceof Collection) {
            if ($users->first()->isNotInBlacklist($users->get(1))) {
                return $this;
            }
        }

        if ($this->recipient) {
            if ($this->recipient->isNotInBlacklist($this->user)) {
                return $this;
            }
        }

        throw new ChatException('Users are in blacklist!', 403);
    }

    /**
     * Store new users chat and return chat object
     *
     * @param Collection $recipients
     * @param Deal|null $deal
     * @return Chat
     * @throws \Exception
     */
    protected function storeChat(Collection $recipients, Deal $deal = null): Chat
    {
        $chat = $this->storeChatService->store($deal);
        if ($this->addRecipientsService->addCollection($chat, $recipients)) {
            return $chat;
        }
        throw new \Exception('Unexpected error during adding recipients to chat room!');
    }

    /**
     * Store new message and fire event
     *
     * @param Chat $chat
     * @param string $message
     * @param bool $withEvent
     * @return Message|Model
     * @throws \Exception
     */
    protected function storeMessage(Chat $chat, string $message, $withEvent = true): Message
    {
        return $this->setStoreEventClass($this->event)->storeModel([
            'message'     => $message,
            'chat_id'     => $chat->id,
            'author_id'   => $this->user->id,
            'author_type' => get_class($this->user),
        ], $withEvent)->model;
    }
}
