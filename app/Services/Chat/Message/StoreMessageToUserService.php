<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 14:26
 */

namespace App\Services\Chat\Message;

use App\Contracts\Chat\StoreMessageToUserServiceContract;
use App\Exceptions\Chat\ChatException;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\User\User;
use Illuminate\Support\Collection;

/**
 * Class MessageService
 * @package App\Services\Chat
 */
class StoreMessageToUserService extends StoreMessageService implements StoreMessageToUserServiceContract
{
    /**
     * Save message to private chat with user and create new chat if not exists
     *
     * @param User   $recipient
     * @param string $message
     *
     * @return Message
     * @throws ChatException
     * @throws \Exception
     */
    public function store(User $recipient, string $message): Message
    {
        $this->setRecipient($recipient)->checkUsersInBlacklistAndThrowException();

        if ($recipient->id == $this->user->id) {
            throw new ChatException('Recipient can not be identical author!', 403);
        }

        /** @var Chat $chat */
        $chat = $this->getChatBetweenUsers() ?? $this->storeChat(new Collection([$this->user, $this->recipient]));

        return $this->storeMessage($chat, $message);
    }

    /**
     * Return chat object between users or false if not exists
     *
     * @return Chat|null
     */
    protected function getChatBetweenUsers()
    {
        return $this->chatRepo->getChatBetweenUsers($this->user, $this->recipient);
    }
}