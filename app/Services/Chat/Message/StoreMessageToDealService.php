<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 14:26
 */

namespace App\Services\Chat\Message;

use App\Contracts\Chat\StoreMessageToDealServiceContract;
use App\Exceptions\Chat\ChatException;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Deal\Deal;

/**
 * Class MessageService
 * @package App\Services\Chat
 */
class StoreMessageToDealService extends StoreMessageService implements StoreMessageToDealServiceContract
{
    /**
     * Save message to deal chat and create new chat if not exists
     *
     * @param Deal $deal
     * @param string $message
     *
     * @return Message
     *
     * @throws ChatException
     * @throws \Exception
     */
    public function store(Deal $deal, string $message) : Message
    {
        $this->checkUsersInBlacklistAndThrowException($deal->getDealMembers());

        if ($deal->isUserNotDealMember($this->user)) {
            throw new ChatException('User is not member in transmitted deal!', 403);
        }

        /** @var Chat $chat */
        $chat = $this->getDealChat($deal) ?? $this->storeChat($deal->getDealMembers(), $deal);

        return $this->storeMessage($chat, $message);
    }

    /**
     * Return deal's chat if exists
     *
     * @param Deal $deal
     * @return Chat|null
     */
    protected function getDealChat(Deal $deal)
    {
        return $this->chatRepo->getDealChat($deal);
    }
}