<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 13:11
 */

namespace App\Services\Chat;

use App\Contracts\AuthenticatedContract;
use App\Events\Chat\ChatPlaced;
use App\Events\Chat\RecipientsAttached;
use App\Models\Chat\Chat;
use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Services\BaseService;
use App\Services\Traits\Storable;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Collection;

/**
 * Class StoreChatService for creating new chats and messages
 *
 * @package App\Services\Chat
 */
class StoreChatService extends BaseService
{
    use Storable;

    /**
     * @var string
     */
    protected $modelClass = Chat::class;

    /**
     * Event will be fired after successfully action
     *
     * @var string
     */
    protected $event = ChatPlaced::class;

    /**
     * @param AuthenticatedContract $user
     * @param Container $container
     */
    public function __construct(AuthenticatedContract $user, Container $container)
    {
        parent::__construct($user, $container);
        $this->setStoreEventClass($this->event);
    }

    /**
     * Store new chat
     *
     * @param Deal|null $deal
     * @param string|null $title
     * @param bool $withEvent
     * @return Chat
     * @throws \Exception
     */
    public function store(Deal $deal = null, string $title = null, $withEvent = true): Chat
    {
        /** @var Chat $chat */
        $title = $title ?? $this->getDefaultTitle($deal);
        $chat = $this->storeModel([
            'title'       => $title,
            'author_id'   => $this->user->id,
            'author_type' => get_class($this->user),
        ], $withEvent = false)->model;
        if ($deal) {
            $chat->deals()->sync($deal);
        }

        $this->fireEvent($this->storeEventClass);

        return $chat;
    }

    /**
     * Return default chat title according lang-file in EN locale
     *
     * @param Deal|null $deal
     * @return string
     */
    protected function getDefaultTitle(Deal $deal = null): string
    {
        return $deal
            ? __('chat.title.default.deal', ['deal_id' => $deal->id], 'en')
            : __('chat.title.default.user', ['user_id' => $this->user->id], 'en');
    }
}