<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 14:26
 */

namespace App\Services\Chat\Admin;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Chat\StoreMessageToDealServiceContract;
use App\Events\Chat\MessagePlaced;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Deal\Deal;
use App\Repositories\Chat\ChatRepo;
use App\Services\BaseService;
use App\Services\Chat\AddRecipientsService;
use App\Services\Chat\StoreChatService;
use App\Services\Traits\Storable;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class MessageService
 * @package App\Services\Chat
 */
class StoreMessageToDealService extends BaseService implements StoreMessageToDealServiceContract
{
    use Storable;

    /**
     * @var string
     */
    protected $modelClass = Message::class;

    /**
     * @var ChatRepo $chatRepo
     */
    private $chatRepo;

    /**
     * @var StoreChatService $storeChatService
     */
    private $storeChatService;

    /**
     * @var string $event
     */
    private $event = MessagePlaced::class;

    /**
     * @var AddRecipientsService $addRecipientsService
     */
    private $addRecipientsService;


    /**
     * StoreMessageToDealService constructor.
     *
     * @param AuthenticatedContract $user
     * @param Container             $container
     * @param ChatRepo              $chatRepo
     * @param StoreChatService      $storeChatService
     * @param AddRecipientsService  $addRecipientsService
     */
    public function __construct(
        AuthenticatedContract $user,
        Container $container,
        ChatRepo $chatRepo,
        StoreChatService $storeChatService,
        AddRecipientsService $addRecipientsService
    ) {
        parent::__construct($user, $container);
        $this->chatRepo             = $chatRepo;
        $this->storeChatService     = $storeChatService;
        $this->addRecipientsService = $addRecipientsService;
    }

    /**
     * Save message to deal chat and create new chat if not exists
     *
     * @param Deal   $deal
     * @param string $message
     *
     * @return Message
     * @throws \Exception
     */
    public function store(Deal $deal, string $message) : Message
    {
        /** @var Chat $chat */
        $chat = $this->getDealChat($deal) ?? $this->storeChat($deal->getDealMembers(), $deal);

        return $this->storeMessage($chat, $message);
    }

    /**
     * Store new users chat and return chat object
     *
     * @param Collection $recipients
     * @param Deal|null $deal
     * @return Chat
     * @throws \Exception
     */
    protected function storeChat(Collection $recipients, Deal $deal = null): Chat
    {
        $chat = $this->storeChatService->store($deal);
        if ($this->addRecipientsService->addCollection($chat, $recipients)) {
            return $chat;
        }
        throw new \Exception('Unexpected error during adding recipients to chat room!');
    }

    /**
     * Return deal's chat if exists
     *
     * @param Deal $deal
     * @return Chat|null
     */
    protected function getDealChat(Deal $deal)
    {
        return $this->chatRepo->getDealChat($deal);
    }

    /**
     * Store new message and fire event
     *
     * @param Chat $chat
     * @param string $message
     * @param bool $withEvent
     * @return Message|Model
     * @throws \Exception
     */
    protected function storeMessage(Chat $chat, string $message, $withEvent = true): Message
    {
        return $this->setStoreEventClass($this->event)->storeModel([
            'message'     => $message,
            'chat_id'     => $chat->id,
            'author_id'   => $this->user->id,
            'author_type' => get_class($this->user),
        ], $withEvent)->model;
    }
}
