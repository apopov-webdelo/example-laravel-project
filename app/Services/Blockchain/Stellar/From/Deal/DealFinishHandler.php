<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 09:33
 */

namespace App\Services\Blockchain\Stellar\From\Deal;

use App\Contracts\Services\Blockchain\From\Deal\DealFinishContract;
use App\Events\Blockchain\From\Deal\DealFinished;
use App\Facades\Robot;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Deal\CancelDealService;
use App\Services\Deal\FinishDealService;
use App\Services\Traits\Dealable;

/**
 * Class DealFinishHandler
 *
 * @package App\Services\Blockchain\Stellar
 */
class DealFinishHandler extends AbstractHandler implements DealFinishContract
{
    use ControlSum, Dealable, Eventable;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealFinished::class;

    /**
     * Service for deal cancellation
     *
     * @var CancelDealService
     */
    protected $finishDealService;

    /**
     * DealFinishHandler constructor.
     *
     * @param FinishDealService $finishDealService
     */
    public function __construct(FinishDealService $finishDealService)
    {
        $this->finishDealService = $finishDealService;
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function finish(Deal $deal): bool
    {
        $this->setDeal($deal)->checkControlSum();

        $this->processRequest()->fireEvent($this->event, $this->deal);
        return true;
    }

    /**
     * Execute logic for request from blockchain
     *
     * @return $this
     * @throws \Exception
     */
    protected function processRequest()
    {
        $this->finishDealService->setUser(Robot::user())->finish($this->deal);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->deal->id,
            $this->controlKey,
        ];
    }
}
