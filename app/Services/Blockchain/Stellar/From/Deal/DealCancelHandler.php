<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 09:33
 */

namespace App\Services\Blockchain\Stellar\From\Deal;

use App\Contracts\Services\Blockchain\From\Deal\DealCancelContract;
use App\Events\Blockchain\From\Deal\DealCanceled;
use App\Facades\Robot;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Deal\CancelDealService;
use App\Services\Traits\Dealable;

/**
 * Class DealCancelHandler
 *
 * @package App\Services\Blockchain\Stellar
 */
class DealCancelHandler extends AbstractHandler implements DealCancelContract
{
    use ControlSum, Dealable, Eventable;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealCanceled::class;

    /**
     * Service for deal cancellation
     *
     * @var CancelDealService
     */
    protected $cancelDealService;

    /**
     * DealCancelHandler constructor.
     *
     * @param CancelDealService $cancelDealService
     */
    public function __construct(CancelDealService $cancelDealService)
    {
        $this->cancelDealService = $cancelDealService;
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function cancel(Deal $deal): bool
    {
        $this->setDeal($deal)->checkControlSum();

        $this->processRequest()->fireEvent($this->event, $this->deal);
        return true;
    }

    /**
     * Execute logic for request from blockchain
     *
     * @return $this
     * @throws \Exception
     */
    protected function processRequest()
    {
        $this->cancelDealService->setUser(Robot::user())->cancel($this->deal);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->deal->id,
            $this->controlKey,
        ];
    }
}
