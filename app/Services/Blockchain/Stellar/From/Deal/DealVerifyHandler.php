<?php

namespace App\Services\Blockchain\Stellar\From\Deal;

use App\Contracts\Services\Blockchain\From\Deal\DealVerifyContract;
use App\Contracts\Services\Deal\VerifyDealServiceContract;
use App\Events\Blockchain\From\Deal\DealVerified;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Traits\Dealable;

/**
 * Class DealVerifyHandler
 *
 * @package App\Services\Blockchain\Stellar\From\Deal
 */
class DealVerifyHandler extends AbstractHandler implements DealVerifyContract
{
    use ControlSum, Dealable, Eventable;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealVerified::class;

    /**
     * @var VerifyDealServiceContract
     */
    protected $verifyDealService;

    /**
     * DealFinishHandler constructor.
     *
     * @param VerifyDealServiceContract $verifyDealService
     */
    public function __construct(VerifyDealServiceContract $verifyDealService)
    {
        $this->verifyDealService = $verifyDealService;
    }

    /**
     * Register verified deal in blockchain gate
     *
     * @param Deal $deal
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function verified(Deal $deal): bool
    {
        $this->setDeal($deal)->checkControlSum();

        $this->processRequest()->fireEvent($this->event, $this->deal);

        return true;
    }

    /**
     * Execute logic for request from blockchain
     *
     * @return $this
     * @throws \Exception
     */
    protected function processRequest()
    {
        $this->verifyDealService->verified($this->deal);

        return $this;
    }

    /**
     * Return array with element for control-sum generation
     *
     * Warning! You do not include private key! It will be added automatically!
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->deal->id,
            $this->controlKey,
        ];
    }
}
