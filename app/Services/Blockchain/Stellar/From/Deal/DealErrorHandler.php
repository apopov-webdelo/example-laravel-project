<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 09:33
 */

namespace App\Services\Blockchain\Stellar\From\Deal;

use App\Contracts\Services\Blockchain\From\Deal\DealCreationErrorContract;
use App\Events\Blockchain\From\Deal\DealError;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Exceptions\Deal\DealException;
use App\Facades\Robot;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Blockchain\Stellar\Traits\ReasonableError;
use App\Services\Deal\CancelDealService;
use App\Services\Traits\Dealable;

/**
 * Class DealErrorHandler
 *
 * @package App\Services\Blockchain\Stellar
 */
class DealErrorHandler extends AbstractHandler implements DealCreationErrorContract
{
    use ControlSum, Dealable, ReasonableError, Eventable;

    /**
     * @var CancelDealService
     */
    protected $cancelDealService;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealError::class;

    /**
     * DealErrorHandler constructor.
     *
     * @param CancelDealService $cancelDealService
     */
    public function __construct(CancelDealService $cancelDealService)
    {
        $this->cancelDealService = $cancelDealService;
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal   $deal
     * @param string $error
     * @param string $reason
     *
     * @return bool
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function error(Deal $deal, string $error, string $reason): bool
    {
        $this->setDeal($deal)
             ->setError($error)
             ->setReason($reason)
             ->checkControlSum();

        $this->processRequest()->fireEvent($this->event, $this->deal);

        $this->logCritical(__CLASS__, [
            'deal'   => $deal->id,
            'error'  => $error,
            'reason' => $reason,
        ]);

        return true;
    }

    /**
     * Execute logic for request from blockchain
     *
     * @return $this
     * @throws \Exception
     */
    protected function processRequest()
    {
        try {
            $this->cancelDealService->setUser(Robot::user())->cancel($this->deal);

            return $this;
        } catch (DealException $exception) {
            throw new StellarException($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->deal->id,
            $this->error,
            $this->reason,
        ];
    }
}
