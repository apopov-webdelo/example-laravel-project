<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 09:28
 */

namespace App\Services\Blockchain\Stellar\From\Deal;

use App\Contracts\Services\Blockchain\From\Deal\DealCancelContract;
use App\Contracts\Services\Blockchain\From\Deal\DealCreationErrorContract;
use App\Contracts\Services\Blockchain\From\Deal\DealFinishContract;
use App\Contracts\Services\Blockchain\From\Deal\DealVerifyContract;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\Traits\ControlSum;

/**
 * Class DealService
 *
 * @package App\Services\Blockchain\Stellar\From\Deal
 */
class DealService implements DealCancelContract, DealFinishContract, DealCreationErrorContract, DealVerifyContract
{
    use ControlSum;

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \ReflectionException
     */
    public function cancel(Deal $deal): bool
    {
        /** @var DealCancelHandler $handler */
        $handler = $this->getHandler(DealCancelHandler::class);
        return $handler->cancel($deal);
    }

    /**
     * Retrieve instant handler
     *
     * @param string $class
     *
     * @return mixed
     */
    protected function getHandler(string $class)
    {
        $handler = app($class);
        return $handler->controlSum($this->controlSum, $this->controlKey);
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \ReflectionException
     */
    public function finish(Deal $deal): bool
    {
        /** @var DealFinishHandler $handler */
        $handler = $this->getHandler(DealFinishHandler::class);
        return $handler->finish($deal);
    }

    /**
     * Register verified deal in blockchain gate
     *
     * @param Deal $deal
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function verified(Deal $deal): bool
    {
        /** @var DealVerifyHandler $handler */
        $handler = $this->getHandler(DealVerifyHandler::class);
        return $handler->verified($deal);
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal   $deal
     * @param string $error
     * @param string $reason
     *
     * @return bool
     * @throws \ReflectionException
     */
    public function error(Deal $deal, string $error, string $reason): bool
    {
        /** @var DealErrorHandler $handler */
        $handler = $this->getHandler(DealErrorHandler::class);
        return $handler->error($deal, $error, $reason);
    }
}
