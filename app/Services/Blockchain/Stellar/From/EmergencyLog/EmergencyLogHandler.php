<?php

namespace App\Services\Blockchain\Stellar\From\EmergencyLog;

use App\Contracts\Services\Blockchain\From\EmergencyLog\EmergencyLogHandlerContract;
use App\Facades\EmergencyAlert;
use App\Http\Requests\Blockchain\EmergencyLog\EmergencyLogRequest;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Timestamp;

class EmergencyLogHandler extends AbstractHandler implements EmergencyLogHandlerContract
{
    use Timestamp;

    /**
     * @var string
     */
    private $channel;

    /**
     * Set channel
     *
     * @param string $name
     *
     * @return mixed
     */
    public function channel(string $name)
    {
        $this->channel = $name;

        return $this;
    }

    /**
     * Send log message for request
     *
     * @param EmergencyLogRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function send(EmergencyLogRequest $request)
    {
        $this->setTimestamp($request->timestamp)
             ->controlSum($request->hash)
             ->checkControlSum();

        EmergencyAlert::channel($this->channel)->send('Stellar emergency', $request->all());

        return $this;
    }

    /**
     * Return array with element for control-sum generation
     *
     * Warning! You do not include private key! It will be added automatically!
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [];
    }
}
