<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/28/18
 * Time: 15:33
 */

namespace App\Services\Blockchain\Stellar\From\Transaction;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\From\Transaction\DepositContract;
use App\Events\Blockchain\From\Transaction\BlockchainDeposit;
use App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException;
use App\Models\Balance\PayIn\PayInRegisterServiceContract;
use App\Models\User\User;

/**
 * Class DepositHandler
 *
 * Check request and increase user's balance
 *
 * @package App\Services\Blockchain\Stellar\From\Transaction
 */
class DepositHandler extends TransactionHandler implements DepositContract
{
    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = BlockchainDeposit::class;

    /**
     * Service for register PayInTransaction requests
     *
     * @var PayInRegisterServiceContract
     */
    private $payInRegisterService;

    /**
     * DepositHandler constructor.
     *
     * @param PayInRegisterServiceContract $payInRegisterService
     */
    public function __construct(PayInRegisterServiceContract $payInRegisterService)
    {
        $this->payInRegisterService = $payInRegisterService;
    }

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                  $amount
     * @param string                  $balanceAmount
     * @param string                 $transactionId
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \Exception
     */
    public function deposit(
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        string $amount,
        string $balanceAmount,
        string $transactionId
    ): bool {
        $this->setUser($user)
             ->setCryptoCurrency($cryptoCurrency)
             ->setAmount($amount)
             ->setBalanceAmount($balanceAmount)
             ->setTransactionId($transactionId)
             ->checkControlSum();

        $this->processRequest()->fireEvent();

        return true;
    }

    /**
     * Execute logic for request
     *
     * @return $this
     * @throws \Exception
     */
    protected function processRequest()
    {
        try {
            $this->payInRegisterService->register(
                $this->transactionId,
                $this->user,
                currencyToCoins($this->amount, $this->cryptoCurrency),
                $this->cryptoCurrency
            );
        } catch (ExistingsTransactionIdPayInException $exception) {
            $this->logDoubleRequest();
        }

        return $this;
    }

    /**
     * Log that was double PayIn request
     *
     * @return $this
     */
    private function logDoubleRequest()
    {
        $this->initLog()->logger->notice(
            'Got double PayInTransaction request from blockchain gate with ID '. $this->transactionId.'!',
            [
                'transaction_id' => $this->transactionId,
                'user_id'        => $this->user->getId(),
                'crypto_code'    => $this->cryptoCurrency->getCode(),
                'amount'         => currencyToCoins($this->amount, $this->cryptoCurrency),
            ]
        );
        return $this;
    }
}
