<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/28/18
 * Time: 15:33
 */

namespace App\Services\Blockchain\Stellar\From\Transaction;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\From\Transaction\WithdrawContract;
use App\Events\Blockchain\From\Transaction\BlockchainWithdraw;
use App\Exceptions\Blockchain\Stellar\UnexpectedWithdrawDataStellarException;
use App\Models\User\User;
use App\Repositories\Balance\PayOutRepo;
use App\Services\Blockchain\Stellar\Traits\PayOut;

/**
 * Class WithdrawHandler
 *
 * Check request and decrease user's balance
 *
 * @package App\Services\Blockchain\Stellar\From\Transaction
 */
class WithdrawHandler extends TransactionHandler implements WithdrawContract
{
    use PayOut;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = BlockchainWithdraw::class;

    /**
     * @var PayOutContract
     */
    private $payOut;

    /**
     * @var PayOutRepo
     */
    private $payOutRepo;

    /**
     * WithdrawHandler constructor.
     *
     * @param PayOutRepo $payOutRepo
     */
    public function __construct(PayOutRepo $payOutRepo)
    {
        $this->payOutRepo = $payOutRepo;
    }

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param float                  $amount
     * @param float                  $balanceAmount
     * @param string                 $transactionId
     * @param int                    $payOutId
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \Exception
     */
    public function withdraw(
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        string $amount,
        string $balanceAmount,
        string $transactionId,
        int $payOutId
    ): bool {
        $this->setUser($user)
             ->setCryptoCurrency($cryptoCurrency)
             ->setAmount($amount)
             ->setBalanceAmount($balanceAmount)
             ->setTransactionId($transactionId)
             ->setPayOutId($payOutId)
             ->checkControlSum();

        $this->processRequest()->fireEvent();

        return true;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->transactionId,
            $this->user->id,
            $this->amount,
            $this->cryptoCurrency->getCode(),
            $this->balanceAmount,
            $this->payOutId,
        ];
    }

    /**
     * Execute logic for request
     *
     * @return $this
     * @throws \Exception
     */
    protected function processRequest()
    {
        $this->setPayOut()
             ->checkRequestDataAccordingPayout();

        return $this;
    }

    /**
     * Set payout to prived field
     *
     * @return $this
     * @throws \Exception
     */
    private function setPayOut()
    {
        $this->payOut = $this->getPayOut();

        if ($this->payOut instanceof PayOutContract) {
            return $this;
        }

        throw new \Exception('PayOut request model with ID = ' . $this->payOutId . ' do not exists!');
    }

    /**
     * Check incoming request is according PayOut data
     *
     * @return $this
     * @throws UnexpectedWithdrawDataStellarException
     * @throws \Exception
     */
    private function checkRequestDataAccordingPayout()
    {
        $errors = [];
        $payout = $this->payOut;

        $this->checkAndSetError($payout->user()->getId(), $this->user->getId(), 'user_id', $errors);

        $this->checkAndSetError(
            $payout->amount(),
            currencyToCoins($this->amount, $this->cryptoCurrency),
            'amount',
            $errors
        );

        $this->checkAndSetError(
            $payout->cryptoCurrency()->getCode(),
            $this->cryptoCurrency->getCode(),
            'crypto_code',
            $errors
        );

        if ($errors) {
            $this->initLog()
                ->logger->critical('Unexpected data got from stellar for PayOut processing!', [$errors]);

            throw new UnexpectedWithdrawDataStellarException(
                'Unexpected data got from stellar for PayOut processing! (' . serialize($errors) . ')'
            );
        }

        return $this;
    }

    /**
     * @param $expected
     * @param $received
     * @param $errorKey
     * @param $errors
     *
     * @return $this
     */
    private function checkAndSetError($expected, $received, $errorKey, &$errors)
    {
        if ($expected !== $received) {
            $errors[$errorKey] = 'Unexpected ' . $errorKey . ' value! Expected: ' . $expected . ' -> Received: ' . $received . '.';
        }

        return $this;
    }

    /**
     * Fire transaction event
     *
     * @return $this
     */
    protected function fireEvent()
    {
        if ($this->event) {
            // todo all this data is not needed anymore! я удалял но видимо где-то на мержах и рефакторинге это вернулось, проверка баланса уже в другой логике
            $event = app()->make($this->event, [
                'user'           => $this->user,
                'amount'         => currencyToCoins($this->amount, $this->cryptoCurrency),
                'commission'     => $this->payOut->commission(),
                'cryptoCurrency' => $this->cryptoCurrency,
                // from Stellar, expected amount after transaction
                'balanceAmount'  => currencyToCoins($this->balanceAmount, $this->cryptoCurrency),
                // real amount after transaction
                'realAmount'     => $this->user->getBalance($this->cryptoCurrency)->amount,
                'transactionId'  => $this->transactionId,
                'payOutId'       => $this->payOutId,
            ]);

            event($event);
        }

        return $this;
    }
}
