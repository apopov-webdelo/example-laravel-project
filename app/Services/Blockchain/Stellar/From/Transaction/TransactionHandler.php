<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/28/18
 * Time: 15:33
 */

namespace App\Services\Blockchain\Stellar\From\Transaction;

use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\AmountFloat;
use App\Services\Blockchain\Stellar\Traits\BalanceAmountFloat;
use App\Services\Blockchain\Stellar\Traits\TransactionId;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

/**
 * Class TransactionHandler
 *
 * Base methods for all transactions handlers
 *
 * @package App\Services\Blockchain\Stellar\From\Transaction
 */
abstract class TransactionHandler extends AbstractHandler
{
    use Userable, CryptoCurrency, AmountFloat, BalanceAmountFloat, TransactionId;

    /**
     * Event will be fired after successfully processing
     *
     * NOTICE: Need to be override in derived class!
     *
     * @var string
     */
    protected $event;

    /**
     * Fire transaction event
     *
     * @return $this
     */
    protected function fireEvent()
    {
        if ($this->event) {
            $event = app()->make($this->event, ['user' => $this->user,]);

            event($event);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->transactionId,
            $this->user->id,
            $this->amount,
            $this->cryptoCurrency->getCode(),
            $this->balanceAmount,
        ];
    }
}
