<?php

namespace App\Services\Blockchain\Stellar\From\Transaction;

use App\Contracts\Balance\PayOut\PayOutCancelServiceContract;
use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Services\Blockchain\From\Transaction\WithdrawErrorContract;
use App\Events\Blockchain\From\Transaction\BlockchainWithdrawError;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Blockchain\Stellar\Traits\ReasonableError;
use App\Services\Traits\Userable;

/**
 * Class WithdrawErrorHandler
 *
 * @package App\Services\Blockchain\Stellar\From\Wallet
 */
class WithdrawErrorHandler extends AbstractHandler implements WithdrawErrorContract
{
    use ControlSum, ReasonableError, Eventable, Userable;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = BlockchainWithdrawError::class;

    /**
     * @var PayOutContract
     */
    private $payOut;

    /**
     * @var PayOutCancelServiceContract
     */
    private $payOutCancelService;

    /**
     * WithdrawErrorHandler constructor.
     *
     * @param PayOutCancelServiceContract $payOutCancelService
     */
    public function __construct(PayOutCancelServiceContract $payOutCancelService)
    {
        $this->payOutCancelService = $payOutCancelService;
    }

    /**
     * {@inheritdoc}
     *
     * @param PayOutContract $payOut
     * @param string         $error
     * @param string         $reason
     *
     * @return bool
     * @throws \App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function error(PayOutContract $payOut, string $error, string $reason): bool
    {
        $this->setPayOut($payOut)
             ->setError($error)
             ->setReason($reason)
             ->checkControlSum();

        $this->processRequest()->fireEvent($this->event, $this->user);

        return true;
    }

    /**
     * Set PayOut to private field
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     */
    private function setPayOut(PayOutContract $payOut)
    {
        $this->payOut = $payOut;
        return $this;
    }

    /**
     * Execute logic for request from blockchain
     *
     * @return $this
     * @throws \App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException
     */
    protected function processRequest()
    {
        $this->payOutCancelService->reason($this->reason)->cancel($this->payOut);

        $this->logCritical(__CLASS__, [
            'user'   => $this->user->id,
            'error'  => $this->error,
            'reason' => $this->reason,
        ]);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->error,
            $this->reason,
        ];
    }
}
