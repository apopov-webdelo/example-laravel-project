<?php

namespace App\Services\Blockchain\Stellar\From\User;

use App\Contracts\DealCashback\DealCashbackCancelServiceContract;
use App\Contracts\DealCashback\DealCashbackContract;
use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackCancelContract;
use App\Events\DealCashback\DealCashbackCancelState;
use App\Http\Requests\Blockchain\DealCashback\CancelRequest;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\DealCashback;
use App\Services\Blockchain\Stellar\Traits\ReasonableError;

/**
 * Class ProcessDealCashbackCancel
 *
 * Handle stellar cancel cashback transaction
 *
 * @package App\Services\Blockchain\Stellar\From\User
 */
class ProcessDealCashbackCancel extends AbstractHandler implements ProcessDealCashbackCancelContract
{
    use ReasonableError, DealCashback;

    /**
     * Event to fire on cancel
     *
     * @var string
     */
    protected $event = DealCashbackCancelState::class;

    /**
     * @var DealCashbackCancelServiceContract
     */
    protected $cancelService;

    /**
     * @var CancelRequest
     */
    protected $request;

    /**
     * ProcessDealCashbackCancel constructor.
     *
     * @param DealCashbackCancelServiceContract $cancelService
     * @param CancelRequest                     $request
     */
    public function __construct(DealCashbackCancelServiceContract $cancelService, CancelRequest $request)
    {
        $this->cancelService = $cancelService;
        $this->request = $request;

        $this->setError($request->error)
             ->setReason($request->reason)
             ->controlSum($request->hash);
    }

    /**
     * Action on cashback transfer cancel from blockchain
     *
     * @param DealCashbackContract $cashback
     *
     * @return ProcessDealCashbackCancelContract
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function cancel(DealCashbackContract $cashback): ProcessDealCashbackCancelContract
    {
        $this->setCashback($cashback)
             ->checkControlSum();

        $this->cancelService->cancel($this->cashback);

        $this->logCritical(__CLASS__, [
            'cashback' => $this->cashback,
            'error'    => $this->request->error,
            'reason'   => $this->request->reason,
        ]);

        event(app(DealCashbackCancelState::class, [
            'cashback' => $this->cashback,
            'error'    => $this->request->error,
            'reason'   => $this->request->reason,
        ]));

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->cashback->id,
            $this->error,
            $this->reason,
        ];
    }
}
