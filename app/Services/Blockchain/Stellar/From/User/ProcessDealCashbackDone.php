<?php

namespace App\Services\Blockchain\Stellar\From\User;

use App\Contracts\DealCashback\DealCashbackContract;
use App\Contracts\DealCashback\DealCashbackDoneServiceContract;
use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackDoneContract;
use App\Events\DealCashback\DealCashbackDoneState;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Exceptions\DealCashback\DealCashbackException;
use App\Http\Requests\Blockchain\DealCashback\CancelRequest;
use App\Http\Requests\Blockchain\DealCashback\DoneRequest;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\DealCashback;
use App\Services\Blockchain\Stellar\Traits\Timestamp;
use App\Services\Blockchain\Stellar\Traits\TransactionId;

/**
 * Class ProcessDealCashbackDone
 *
 * Handle stellar done cashback transaction
 *
 * @package App\Services\Blockchain\Stellar\From\User
 */
class ProcessDealCashbackDone extends AbstractHandler implements ProcessDealCashbackDoneContract
{
    use DealCashback, TransactionId, Timestamp;

    /**
     * Event to fire on done
     *
     * @var string
     */
    protected $event = DealCashbackDoneState::class;

    /**
     * @var DealCashbackDoneServiceContract
     */
    protected $service;

    /**
     * @var CancelRequest
     */
    protected $request;

    /**
     * ProcessDealCashbackDone constructor.
     *
     * @param DealCashbackDoneServiceContract $service
     * @param DoneRequest                     $request
     */
    public function __construct(DealCashbackDoneServiceContract $service, DoneRequest $request)
    {
        $this->service = $service;
        $this->request = $request;

        $this->setTransactionId($request->transaction_id)
             ->setTimestamp($request->timestamp)
             ->controlSum($request->hash);
    }

    /**
     * Action on cashback transfer cancel from blockchain
     *
     * @param DealCashbackContract $cashback
     *
     * @return ProcessDealCashbackDoneContract
     * @throws StellarException
     * @throws DealCashbackException
     */
    public function done(DealCashbackContract $cashback): ProcessDealCashbackDoneContract
    {
        $this->setCashback($cashback)
             ->checkControlSum();

        $this->service->done($cashback, $this->request->transaction_id);

        event(app(DealCashbackDoneState::class, ['cashback' => $this->cashback]));

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->getCashback()->id,
            $this->transactionId,
        ];
    }
}
