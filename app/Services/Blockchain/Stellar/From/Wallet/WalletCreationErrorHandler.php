<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/30/18
 * Time: 15:24
 */

namespace App\Services\Blockchain\Stellar\From\Wallet;

use App\Contracts\Services\Blockchain\From\Wallet\WalletCreationErrorContract;
use App\Events\Blockchain\From\Wallet\WalletCreationError;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\From\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Blockchain\Stellar\Traits\ReasonableError;
use App\Services\Traits\Userable;

/**
 * Class WalletCreationErrorHandler
 *
 * @package App\Services\Blockchain\Stellar\From\Wallet
 */
class WalletCreationErrorHandler extends AbstractHandler implements WalletCreationErrorContract
{
    use ControlSum, ReasonableError, Eventable, Userable;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = WalletCreationError::class;

    /**
     * {@inheritdoc}
     *
     * @param User   $user
     * @param string $error
     * @param string $reason
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function error(User $user, string $error, string $reason): bool
    {
        $this->setUser($user)
             ->setError($error)
             ->setReason($reason)
             ->checkControlSum();

        $this->processRequest()->fireEvent($this->event, $this->user);

        return true;
    }

    /**
     * Execute logic for request from blockchain
     *
     * @return $this
     */
    protected function processRequest()
    {
        $this->user->blockTrade();

        $this->logCritical(__CLASS__, [
            'user'   => $this->user->id,
            'error'  => $this->error,
            'reason' => $this->reason,
        ]);

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->error,
            $this->reason,
        ];
    }
}
