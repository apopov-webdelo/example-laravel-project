<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/29/18
 * Time: 12:08
 */

namespace App\Services\Blockchain\Stellar\From\Wallet;

use App\Contracts\Services\Blockchain\From\Wallet\WalletCreationErrorContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\Logging;

/**
 * Class TransactionService
 *
 * @package App\Services\Blockchain\Stellar\From\Transaction
 */
class WalletService implements WalletCreationErrorContract
{
    use ControlSum, Logging;

    /**
     * {@inheritdoc}
     *
     * @param User   $user
     * @param string $error
     * @param string $reason
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function error(User $user, string $error, string $reason): bool
    {
        /** @var WalletCreationErrorHandler $handler */
        $handler = app(WalletCreationErrorHandler::class);
        return $handler->controlSum($this->controlSum)->error($user, $error, $reason);
    }
}
