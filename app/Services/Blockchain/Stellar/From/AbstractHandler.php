<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 13:49
 */

namespace App\Services\Blockchain\Stellar\From;

use App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException;
use App\Services\Blockchain\Stellar\Traits\ControlSum;
use App\Services\Blockchain\Stellar\Traits\GateSecretKey;
use App\Services\Blockchain\Stellar\Traits\Logging;

/**
 * Class AbstractHandler
 *
 * @package App\Services\Blockchain\Stellar
 */
abstract class AbstractHandler
{
    use GateSecretKey, ControlSum, Logging;

    /**
     * Check is control sum is valid accourding request data
     *
     * @return $this
     * @throws InvalidControlSumStellarException
     */
    protected function checkControlSum()
    {
        $validControlSum = $this->generateControlSum();
        if ($this->controlSum == $validControlSum) {
            return $this;
        }

        $this->initLog()->logger::warning(
            'Incorrect control sum for blockchain handler ' . get_class($this) . '!' .
            'Valid: ' . $validControlSum . '. Got: ' . $this->controlSum . '.'
        );

        throw new InvalidControlSumStellarException('Unexpected hash for that request data!');
    }
}
