<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:06
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait Walletable
 *
 * @package App\Services\Blockchain\Stellar
 */
trait Wallet
{
    /**
     * @var string
     */
    protected $walletId;

    /**
     * Set wallet Id
     *
     * @param null|string $walletId
     *
     * @return $this
     */
    public function setWallet(?string $walletId)
    {
        $this->walletId = $walletId;
        return $this;
    }
}
