<?php

namespace App\Services\Blockchain\Stellar\Traits;

use App\Contracts\DealCashback\DealCashbackContract;
use App\Models\DealCashback\DealCashback as DealCashbackModel;

/**
 * Trait DealCashback for working with cashback in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait DealCashback
{
    /**
     * @var DealCashbackModel|DealCashbackContract
     */
    protected $cashback;

    /**
     * Set cashback value
     *
     * @param DealCashbackModel|DealCashbackContract $cashback
     *
     * @return $this
     */
    public function setCashback(DealCashbackContract $cashback)
    {
        $this->cashback = $cashback;

        return $this;
    }

    /**
     * @return DealCashbackModel|DealCashbackContract
     */
    public function getCashback()
    {
        return $this->cashback;
    }
}
