<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 14:36
 */

namespace App\Services\Blockchain\Stellar\Traits;

use App\Exceptions\Event\UndefinedEventException;

/**
 * Trait Eventable
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait Eventable
{
    /**
     * Fire events after successfully storing
     *
     * @param      $event
     * @param      $payload
     * @param null $bindingAlias
     *
     * @return self
     * @throws \ReflectionException
     * @throws UndefinedEventException
     */
    protected function fireEvent($event, $payload, $bindingAlias = null): self
    {
        if (!$event) {
            throw new UndefinedEventException('Must define $event in ' . static::class);
        }

        if (is_string($event)) {
            $bindingAlias = $bindingAlias ?? $this->getBindingAliasByEvent(get_class($payload));
            event(app()->make($event, [$bindingAlias => $payload]));
        } else {
            event($event);
        }

        return $this;
    }

    /**
     * @param string $event
     *
     * @return string
     * @throws \ReflectionException
     */
    protected function getBindingAliasByEvent(string $event): string
    {
        return camel_case((new \ReflectionClass($event))->getShortName());
    }
}
