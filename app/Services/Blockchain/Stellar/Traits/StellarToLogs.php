<?php

namespace App\Services\Blockchain\Stellar\Traits;

use App\Events\Blockchain\StellarLogEvent;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * Saving all To request to Stellar and all responses in DB
 *
 * Trait StellarToLogs
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait StellarToLogs
{
    /**
     * @var ResponseInterface|null
     */
    protected $stellarResponse;

    /**
     * @var RequestException|null
     */
    protected $stellarException;

    /**
     * @param string $method
     * @param string $route
     * @param array  $request
     */
    protected function writeStellarLog(string $method, string $route, array $request)
    {
        event(app(StellarLogEvent::class, [
            'type'          => 'to',
            'method'        => $method,
            'route'         => $route,
            'request'       => serialize($request),
            'response'      => $this->getStellarResponseContent(),
            'response_code' => $this->getStellarResponseCode(),
        ]));
    }

    /**
     * @param ResponseInterface $stellarResponse
     */
    protected function setStellarLogResponse(ResponseInterface $stellarResponse)
    {
        $this->stellarResponse = $stellarResponse;
    }

    /**
     * @param GuzzleException $stellarException
     */
    protected function setStellarLogException(GuzzleException $stellarException)
    {
        $this->stellarException = $stellarException;
    }

    /**
     * @return string
     */
    private function getStellarResponseContent()
    {
        if ($this->stellarResponse) {
            return serialize(json_decode($this->stellarResponse->getBody(), true));
        }

        if ($this->stellarException) {
            return serialize([$this->stellarException->getMessage()]);
        }

        return '-';
    }

    /**
     * @return int|mixed
     */
    private function getStellarResponseCode()
    {
        if ($this->stellarResponse) {
            return $this->stellarResponse->getStatusCode();
        }

        if ($this->stellarException) {
            return $this->stellarException->getCode();
        }

        return Response::HTTP_UNPROCESSABLE_ENTITY;
    }
}
