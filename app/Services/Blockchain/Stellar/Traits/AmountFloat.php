<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:08
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait Amountable for working with amount in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait AmountFloat
{
    /**
     * @var float|string
     */
    protected $amount;

    /**
     * Set amount value
     *
     * @param float|string $amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float|string
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
