<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:08
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait Amountable for working with amount in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait BalanceAmount
{
    /**
     * Total due on balance in blockchain after transaction
     *
     * @var int
     */
    protected $balanceAmount;

    /**
     * Set balance amount
     *
     * @param int $amount
     *
     * @return $this
     */
    public function setBalanceAmount(int $amount)
    {
        $this->balanceAmount = $amount;

        return $this;
    }

    /**
     * @return int
     */
    public function getBalanceAmount()
    {
        return $this->balanceAmount;
    }
}
