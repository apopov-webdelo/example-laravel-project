<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:08
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait Commission for working with commission in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait Commission
{
    /**
     * @var int|float
     */
    protected $commission;

    /**
     * Set commission value
     *
     * @param int|float $commission
     *
     * @return $this
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
        return $this;
    }

    /**
     * @return int|float
     */
    public function getCommission()
    {
        return $this->commission;
    }
}
