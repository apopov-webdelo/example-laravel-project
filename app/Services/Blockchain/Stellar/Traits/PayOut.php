<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:08
 */

namespace App\Services\Blockchain\Stellar\Traits;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Repositories\PayOutRepoContract;

/**
 * Trait PayOut for working with payOut in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait PayOut
{
    /**
     * @var int
     */
    protected $payOutId;

    /**
     * Set payOut value
     *
     * @param int $payOutId
     *
     * @return $this
     */
    public function setPayOutId(int $payOutId)
    {
        $this->payOutId = $payOutId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPayOutId()
    {
        return $this->payOutId;
    }

    /**
     * Retrieve payOutObject
     *
     * @return PayOutContract
     * @throws \Exception
     */
    public function getPayOut(): PayOutContract
    {
        /** @var PayOutRepoContract $repo */
        $repo = app(PayOutRepoContract::class);
        $payOut = $repo->findById($this->payOutId);

        if ($payOut instanceof PayOutContract) {
            return $payOut;
        }

        throw new \Exception('PayOut request model with ID = '.$this->payOutId.' do not exists!');
    }
}
