<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:08
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * todo temporary object
 * Trait Amountable for working with amount in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait BalanceAmountFloat
{
    /**
     * Total due on balance in blockchain after transaction
     *
     * @var float|string
     */
    protected $balanceAmount;

    /**
     * Set balance amount
     *
     * @param float|string $amount
     *
     * @return $this
     */
    public function setBalanceAmount($amount)
    {
        $this->balanceAmount = $amount;

        return $this;
    }

    /**
     * @return float|string
     */
    public function getBalanceAmount()
    {
        return $this->balanceAmount;
    }
}
