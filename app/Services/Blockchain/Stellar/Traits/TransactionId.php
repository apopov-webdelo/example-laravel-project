<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-17
 * Time: 11:51
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait for working with Transaction ID in blockchain
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait TransactionId
{

    /**
     * Blockchain transaction ID
     *
     * @var string
     */
    protected $transactionId;

    /**
     * Set value for transaction ID
     *
     * @param string $transactionId
     *
     * @return $this
     */
    public function setTransactionId(string $transactionId)
    {
        $this->transactionId = $transactionId;
        return $this;
    }

    /**
     * Retrieve transaction Id in blockchain
     *
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transactionId;
    }
}