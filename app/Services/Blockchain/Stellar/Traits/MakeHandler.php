<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 15:09
 */

namespace App\Services\Blockchain\Stellar\Traits;

use GuzzleHttp\ClientInterface;
use Illuminate\Contracts\Container\Container;

/**
 * Trait MakeHandler
 *
 * Provide method for quick instant handlers
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait MakeHandler
{
    /**
     * Driver for handling http requests
     *
     * @var ClientInterface
     */
    protected $http;

    /**
     * IoC container implementation
     *
     * @var Container
     */
    protected $app;

    /**
     * WalletService constructor.
     *
     * @param ClientInterface $http
     * @param Container       $app
     */
    public function __construct(ClientInterface $http, Container $app)
    {
        $this->http = $http;
        $this->app  = $app;
    }

    /**
     * Instant and retrieve with all dependencies standart handlers with AbstractStellarHandler base class
     *
     * @param string $class
     *
     * @return mixed
     */
    protected function getHandler(string $class)
    {
        return $this->app->make($class, ['http' => $this->http]);
    }
}