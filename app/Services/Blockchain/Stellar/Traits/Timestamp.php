<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/19/18
 * Time: 15:49
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait Timestamp for generation timestamp value for control sum generation
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait Timestamp
{
    /**
     * Timestamp for unique control sum
     *
     * @var int
     */
    protected $timestamp;

    /**
     * Generate timestamp
     *
     * @return $this
     */
    protected function generateTimestamp()
    {
        $this->timestamp = time();
        return $this;
    }

    /**
     * @param int $timestamp
     *
     * @return $this
     */
    protected function setTimestamp(int $timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
