<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/14/18
 * Time: 11:29
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait provide method for interface App\Contracts\Services\Blockchain\From\ControlSumContract
 *
 * @package App\Services\Blockchain\Stellar
 */
trait ControlSum
{
    /**
     * Hash sum built accourding request data
     *
     * @var string
     */
    protected $controlSum;

    /**
     * Unique key (usual use timestamp)
     *
     * @var string
     */
    protected $controlKey;

    /**
     * Set control sum for that request
     *
     * @param string      $hash
     * @param string|null $controlKey Usual using timestamp like additional key for control sum
     *
     * @return $this
     */
    public function controlSum(string $hash, string $controlKey = null)
    {
        $this->controlSum = $hash;
        $this->controlKey = $controlKey;
        return $this;
    }
}
