<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:20
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait PrivateKey
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait PrivateKey
{
    /**
     * @var string
     */
    protected $privateKey;

    /**
     * Set private key
     *
     * @param string|null $key
     *
     * @return $this
     */
    public function setPrivateKey(?string $key)
    {
        $this->privateKey = $key;
        return $this;
    }
}