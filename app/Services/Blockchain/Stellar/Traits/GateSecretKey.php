<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 13:52
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait GateSecretKey
 *
 * Provide method that retrieve secret key for control sum generation for blockchain gate requests.
 *
 * @package App\Services\Blockchain\Stellar
 */
trait GateSecretKey
{
    /**
     * Generate control sum for request
     * hash: getControlData() + timestamp(by default) + private_key(by default)
     *
     * @return string
     */
    protected function generateControlSum(): string
    {
        $elements = $this->getControlData();

        if (isset($this->timestamp)) {
            $elements[] = $this->timestamp;
        }

        $elements[] = $this->getPrivateKey();

        return md5(implode('-', $elements));
    }

    /**
     * Return array with element for control-sum generation
     *
     * Warning! You do not include private key! It will be added automatically!
     *
     * @return array
     */
    abstract protected function getControlData(): array;

    /**
     * Return current private key for blockchain gate control sum generation
     *
     * @return string
     */
    protected function getPrivateKey(): string
    {
        return config('app.blockchain.stellar.private_key');
    }
}
