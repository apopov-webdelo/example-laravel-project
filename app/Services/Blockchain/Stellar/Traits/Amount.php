<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/22/18
 * Time: 14:08
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait Amountable for working with amount in object
 *
 * @package App\Services\Blockchain\Stellar
 */
trait Amount
{
    /**
     * @var int
     */
    protected $amount;

    /**
     * Set amount value
     *
     * @param int $amount
     *
     * @return $this
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
