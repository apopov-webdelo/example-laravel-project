<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/19/18
 * Time: 11:56
 */

namespace App\Services\Blockchain\Stellar\Traits;

use App\Exceptions\Blockchain\Stellar\InvalidKeyStellarException;

trait CheckKey
{
    /**
     * Check is key valid
     *
     * @param string $type
     * @param string $value
     *
     * @return $this
     * @throws InvalidKeyStellarException
     */
    protected function checkKey(string $type, string $value)
    {
        if (empty($value)) {
            $message = ucfirst($type).' key can\'t be empty string!';
            throw new InvalidKeyStellarException($message);
        }
        return $this;
    }
}
