<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/23/18
 * Time: 16:06
 */

namespace App\Services\Blockchain\Stellar\Traits;

/**
 * Trait ReasonableError
 *
 * @package App\Services\Blockchain\Stellar\Traits
 */
trait ReasonableError
{
    /**
     * Error title
     *
     * @var string
     */
    protected $error;

    /**
     * Reason or error description
     *
     * @var string
     */
    protected $reason;

    /**
     * Set error value
     *
     * @param string $title
     *
     * @return self
     */
    protected function setError(string $title) : self
    {
        $this->error = $title;
        return $this;
    }

    /**
     * Set reason or error description
     *
     * @param string $description
     *
     * @return $this
     */
    protected function setReason(string $description)
    {
        $this->reason = $description;
        return $this;
    }
}
