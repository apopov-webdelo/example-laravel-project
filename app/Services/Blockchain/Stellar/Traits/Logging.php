<?php

namespace App\Services\Blockchain\Stellar\Traits;

use App\Utils\Loggers\FilterSensitive;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

trait Logging
{
    /**
     * Channel for logging
     *
     * @var string
     */
    protected $logChannel = 'stellar';

    /**
     * Log facade could be used in handlers
     *
     * @var Log
     */
    protected $logger;

    /**
     * @var FilterSensitive
     */
    protected $filterService;

    /**
     * Init logger
     *
     * @return $this
     */
    protected function initLog()
    {
        if (!$this->logger) {
            $this->logger = Log::channel($this->logChannel);
        }

        return $this;
    }

    /**
     * Log response using log driver
     *
     * @param string            $method
     * @param string            $url
     * @param array             $body
     * @param ResponseInterface $response
     *
     * @return $this
     */
    protected function logResponse(string $method, string $url, array $body, ResponseInterface $response)
    {
        $this->initLog()
            ->logger
            ->info(
                $method . ' | ' . $url . ' | Code: ' . $response->getStatusCode(),
                [
                    'payload'  => $this->filterLog($body),
                    'response' => $this->filterLog($response->getBody()),
                ]
            );

        return $this;
    }

    /**
     * Log requests errors
     *
     * @param string     $method
     * @param string     $url
     * @param array      $body
     * @param \Exception $exception
     *
     * @return $this
     */
    protected function logError(string $method, string $url, array $body, \Exception $exception)
    {
        $this->initLog()
            ->logger
            ->error(
                $method . ' | ' . $url . ' | Code: ' . $exception->getCode(),
                [
                    'payload' => $this->filterLog($body),
                    'error'   => $exception->getMessage(),
                ]
            );

        return $this;
    }

    /**
     * Log critical errors
     *
     * @param string $where
     * @param array  $data
     *
     * @return $this
     */
    protected function logCritical(string $where, array $data)
    {
        $data = $this->filterLog($data);

        $this->initLog()
            ->logger
            ->critical(
                $where,
                $data
            );

        return $this;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function filterLog($data)
    {
        if (!$this->filterService) {
            $this->filterService = app(FilterSensitive::class);
        }

        return $this->filterService->filter($data);
    }
}
