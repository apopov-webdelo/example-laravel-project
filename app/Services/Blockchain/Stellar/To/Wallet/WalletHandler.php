<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 17:14
 */

namespace App\Services\Blockchain\Stellar\To\Wallet;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

/**
 * Class WalletHandler
 *
 * Make request and retrieve wallet ID in needed crypto in blockchain for user
 *
 * @package App\Services\Blockchain\Stellar
 */
class WalletHandler extends AbstractHandler
{
    use Userable,
        CryptoCurrency;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/wallet';

    /**
     * Retrieve wallet ID from blockchain gate
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return string
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function getWallet(User $user, CryptoCurrencyContract $cryptoCurrency): string
    {
        $this->setUser($user)->setCryptoCurrency($cryptoCurrency);

        $data = $this->processRequest();

        return $data->wallet_id;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'     => $this->user->id,
            'crypto_type' => $this->cryptoCurrency->getCode(),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->cryptoCurrency->getCode(),
        ];
    }
}
