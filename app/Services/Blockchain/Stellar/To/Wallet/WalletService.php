<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 16:57
 */

namespace App\Services\Blockchain\Stellar\To\Wallet;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\To\Wallet\AddEmergencyWalletContract;
use App\Contracts\Services\Blockchain\To\Wallet\CreateEmergencyWalletContract;
use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\Traits\MakeHandler;
use GuzzleHttp\ClientInterface;
use Illuminate\Contracts\Container\Container;

/**
 * Class WalletService
 *
 * @package App\Services\Blockchain\Stellar
 */
class WalletService implements WalletContract, CreateEmergencyWalletContract, AddEmergencyWalletContract
{
    use MakeHandler;

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return string
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function getWallet(User $user, CryptoCurrencyContract $cryptoCurrency): string
    {
        /** @var WalletHandler $handler */
        $handler = $this->getHandler(WalletHandler::class);
        return $handler->getWallet($user, $cryptoCurrency);
    }

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return float
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function getAmount(User $user, CryptoCurrencyContract $cryptoCurrency): float
    {
        /** @var AmountHandler $handler */
        $handler = $this->getHandler(AmountHandler::class);
        return $handler->getAmount($user, $cryptoCurrency);
    }

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function addEmergencyWallet(
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        string $walletId
    ): bool {
        /** @var AddEmergencyWalletHandler $handler */
        $handler = $this->getHandler(AddEmergencyWalletHandler::class);
        return $handler->addEmergencyWallet($user, $cryptoCurrency, $walletId);
    }

    /**
     * {@inheritdoc}
     *
     * @param User   $user
     * @param string $publicKey
     *
     * @return string
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function createEmergencyWallet(
        User $user,
        string $publicKey
    ): string {
        /** @var CreateEmergencyWalletHandler $handler */
        $handler = $this->getHandler(CreateEmergencyWalletHandler::class);
        return $handler->createEmergencyWallet($user, $publicKey);
    }
}
