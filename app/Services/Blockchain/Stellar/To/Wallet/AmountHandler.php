<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 17:14
 */

namespace App\Services\Blockchain\Stellar\To\Wallet;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

/**
 * Class AmountHandler
 *
 * Make request and retrieve current amount in blockchain wallet
 *
 * @package App\Services\Blockchain\Stellar
 */
class AmountHandler extends AbstractHandler
{
    use Userable,
        CryptoCurrency;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/amount';

    /**
     * Retrieve current amount from blockchain gate
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return float
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function getAmount(User $user, CryptoCurrencyContract $cryptoCurrency): float
    {
        $this->setUser($user)->setCryptoCurrency($cryptoCurrency);

        $data = $this->processRequest();

        return (float)$data->amount;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'     => $this->user->id,
            'crypto_type' => $this->cryptoCurrency->getCode(),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->cryptoCurrency->getCode(),
        ];
    }
}
