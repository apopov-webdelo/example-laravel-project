<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 17:14
 */

namespace App\Services\Blockchain\Stellar\To\Wallet;

use App\Contracts\Services\Blockchain\To\Wallet\CreateEmergencyWalletContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Traits\Userable;

/**
 * Make request for creation emergency wallet in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class CreateEmergencyWalletHandler extends AbstractHandler implements CreateEmergencyWalletContract
{
    use Userable;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/emergency/create';

    /**
     * @var string
     */
    protected $publicKey;

    /**
     * {@inheritdoc}
     *
     * @param User   $user
     * @param string $publicKey
     *
     * @return string
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function createEmergencyWallet(
        User $user,
        string $publicKey
    ): string {
        $this->setUser($user)
             ->setPublicKey($publicKey);

        $data = $this->processRequest();
        $walletId = $data->wallet_id;

        $this->saveWalletToUser($walletId);

        return $walletId;
    }

    /**
     * Save wallet and relate it with user
     *
     * @param string $walletId
     *
     * @return $this
     */
    protected function saveWalletToUser(string $walletId)
    {
        $this->user->saveWallet($walletId, 'emergency_wallet');

        return $this;
    }

    /**
     * @param string $publicKey
     *
     * @return $this
     */
    protected function setPublicKey(string $publicKey)
    {
        $this->publicKey = $publicKey;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'         => $this->user->id,
            'user_public_key' => $this->publicKey,
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->publicKey,
        ];
    }
}
