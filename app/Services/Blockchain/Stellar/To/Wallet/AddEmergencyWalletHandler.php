<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 17:14
 */

namespace App\Services\Blockchain\Stellar\To\Wallet;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\To\Wallet\AddEmergencyWalletContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

/**
 * Make request for creation emergency wallet in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class AddEmergencyWalletHandler extends AbstractHandler implements AddEmergencyWalletContract
{
    use Userable,
        CryptoCurrency;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/emergency/add';

    /**
     * @var string
     */
    protected $emergencyWalletId;

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function addEmergencyWallet(
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        string $walletId
    ): bool {
        $this->setUser($user)
             ->setCryptoCurrency($cryptoCurrency)
             ->setEmergencyWallet($walletId);

        $data = $this->processRequest();

        return $data;
    }

    /**
     * @param string $walletId
     *
     * @return $this
     */
    protected function setEmergencyWallet(string $walletId)
    {
        $this->emergencyWalletId = $walletId;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'       => $this->user->id,
            'crypto_type'   => $this->cryptoCurrency->getCode(),
            'crypto_wallet' => $this->emergencyWalletId,
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->cryptoCurrency->getCode(),
            $this->emergencyWalletId,
        ];
    }
}
