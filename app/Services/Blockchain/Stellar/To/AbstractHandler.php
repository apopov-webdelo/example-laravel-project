<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/14/18
 * Time: 11:27
 */

namespace App\Services\Blockchain\Stellar\To;

use App\Exceptions\Blockchain\Stellar\BadRequestStellarException;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Exceptions\Blockchain\Stellar\UnexpectedResponseBodyStellarException;
use App\Exceptions\Blockchain\Stellar\UnexpectedResponseCodeStellarException;
use App\Services\Blockchain\Stellar\Traits\Eventable;
use App\Services\Blockchain\Stellar\Traits\GateSecretKey;
use App\Services\Blockchain\Stellar\Traits\Logging;
use App\Services\Blockchain\Stellar\Traits\StellarToLogs;
use App\Services\Blockchain\Stellar\Traits\Timestamp;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Container\Container;
use Psr\Http\Message\ResponseInterface;

/**
 * Class AbstractStellarHandler
 *
 * @package App\Services\Blockchain\Stellar
 */
abstract class AbstractHandler
{
    use GateSecretKey, Timestamp, Logging, Eventable, StellarToLogs;

    /**
     * Event will be fired after successfully processing (must override in child class)
     *
     * @var string
     */
    protected $event;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = '';

    /**
     * Http status for gate response
     *
     * @var int
     */
    protected $responseStatus = 200;

    /**
     * Path to config array
     *
     * @var string
     */
    protected $configPath = 'app.blockchain.stellar';

    /**
     * Driver for handling http requests
     *
     * @var ClientInterface
     */
    protected $http;

    /**
     * IoC container implementation
     *
     * @var Container
     */
    protected $app;

    /**
     * Response has got from blockchain gate
     *
     * @var ResponseInterface
     */
    protected $response;

    /**
     * AbstractStellarHandler constructor.
     *
     * @param ClientInterface $http
     * @param Container       $app
     */
    public function __construct(ClientInterface $http, Container $app)
    {
        $this->http = $http;
        $this->app  = $app;
    }

    /**
     * Return params that will be sent with http request
     *
     * @return array
     */
    abstract protected function getRequestData(): array;

    /**
     * Process http request to blockchain gate
     *
     * @return mixed
     *
     * @throws StellarException
     */
    protected function processRequest()
    {
        $this->response = $this->sendRequest();

        // check response status
        if (false == $this->checkResponseStatus($this->response)) {
            $code = $this->response->getStatusCode();

            $exception = new UnexpectedResponseCodeStellarException(
                'Unexpected http code! (' . $code . ' instead of ' . $this->responseStatus . ')',
                $this->response->getStatusCode()
            );
            $exception->setResponse($this->response);

            throw $exception;
        };

        // check response body
        if (false == $this->checkResponseBody($this->response)) {
            throw (new UnexpectedResponseBodyStellarException('Empty response body!'))->setResponse($this->response);
        }

        return $this->adaptResponseBody($this->response);
    }

    /**
     * Build and send request to blockchain gate
     *
     * @return ResponseInterface
     * @throws BadRequestStellarException
     */
    protected function sendRequest(): ResponseInterface
    {
        try {
            // execute before-hook method
            $this->beforeRequest(
                $this->method,
                $this->getRequestURL(),
                $this->getPreparedRequestPayload()
            );

            $response = $this->http->request(
                $this->method,
                $this->getRequestURL(),
                [
                    'query' => $this->getPreparedRequestPayload(),
                ]
            );

            // execute after-hook method
            $this->afterRequest(
                $this->method,
                $this->getRequestURL(),
                $this->getPreparedRequestPayload(),
                $response
            );

            return $response;
        } catch (GuzzleException $exception) {
            $this->logError(
                $this->method,
                $this->getRequestURL(),
                $this->getPreparedRequestPayload(),
                $exception
            );

            // throw special exception
            throw new BadRequestStellarException(
                $exception->getMessage(),
                $exception->getCode()
            );
        } finally {
            if (isset($response)) {
                $this->setStellarLogResponse($response);
            }

            if (isset($exception)) {
                $this->setStellarLogException($exception);
            }

            $this->writeStellarLog($this->method, $this->getRequestURL(), $this->getPreparedRequestPayload());
        }
    }

    /**
     * Hook execute before request will be sent
     *
     * You could override that method for integrate custom logic.
     * Base method log request data. Recommend call parent method for logging.
     *
     * @param string $method
     * @param string $url
     * @param array  $body
     *
     * @return $this
     */
    protected function beforeRequest(string $method, string $url, array $body)
    {
        /*
         * You could override that method for integrate custom logic.
         */
        return $this;
    }

    /**
     * Generate valid URL for request
     *
     * @return string
     */
    protected function getRequestURL(): string
    {
        $config = $this->getConfig();

        $protocol = $config['protocol'];
        $host = $config['host'];
        $port = $config['port'];

        return $protocol . '://' . $host . ':' . $port . '/' . $this->uri;
    }

    /**
     * Return config data like array
     *
     * @return array
     */
    protected function getConfig(): array
    {
        return config($this->configPath);
    }

    /**
     * Return prepared payload array with public key
     *
     * @return array
     */
    protected function getPreparedRequestPayload(): array
    {
        $this->generateTimestamp();

        return array_merge(
            array_merge($this->getRequestData(), ['timestamp' => $this->timestamp]),
            [
                'hash' => $this->generateControlSum(),
            ]
        );
    }

    /**
     * Hook execute after request was sent
     *
     * You could override that method for integrate custom logic.
     * Base method log request and response data. Recommend call parent method for logging.
     *
     * @param string            $method
     * @param string            $url
     * @param array             $body
     * @param ResponseInterface $response
     *
     * @return $this
     */
    protected function afterRequest(string $method, string $url, array $body, ResponseInterface $response)
    {
        /*
         * You could override that method for integrate custom logic.
         */

        return $this->logResponse($method, $url, $body, $response);
    }

    /**
     * Check is response http-code equal expected
     *
     * Could be override for custom logic in derived classes.
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    protected function checkResponseStatus(ResponseInterface $response): bool
    {
        /*
         * You could override that method for customize logic
         */
        return $response->getStatusCode() == $this->responseStatus;
    }

    /**
     * Check is response body valid
     *
     * You could override that method for customize logic
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    protected function checkResponseBody(ResponseInterface $response): bool
    {
        /*
         * You could override that method for customize logic
         */
        return $response->getBody() !== null;
    }

    /**
     * Adapt response body for needed format
     *
     * Base method convert data from JSON format.
     * Could be override for custom logic in derived classes.
     *
     * @param ResponseInterface $response
     *
     * @return mixed
     */
    protected function adaptResponseBody(ResponseInterface $response)
    {
        /*
         * You could override that method for integrate custom logic.
         */
        return json_decode($response->getBody());
    }
}
