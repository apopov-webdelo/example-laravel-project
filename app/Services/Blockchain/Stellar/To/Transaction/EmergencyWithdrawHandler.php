<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/21/18
 * Time: 16:20
 */

namespace App\Services\Blockchain\Stellar\To\Transaction;

use App\Contracts\Services\Blockchain\To\Transaction\EmergencyWithdrawContract;
use App\Events\Blockchain\To\Transaction\BlockchainEmergencyWithdraw;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\PrivateKey;
use App\Services\Traits\Userable;

/**
 * Class EmergencyWithdrawHandler
 *
 * Make emergency withdraw transactions from all wallets in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class EmergencyWithdrawHandler extends AbstractHandler implements EmergencyWithdrawContract
{
    use Userable, PrivateKey;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = BlockchainEmergencyWithdraw::class;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'wallet/out/all';

    /**
     * {@inheritdoc}
     *
     * @param User   $user
     * @param string $privateKey
     *
     * @return array
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function emergencyWithdraw(User $user, string $privateKey): array
    {
        $result = $this->setUser($user)
                    ->setPrivateKey($privateKey)
                    ->processRequest();

        $this->fireEvent($this->event, $user);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'     => $this->user->id,
            'private_key' => $this->privateKey,
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            $this->privateKey,
        ];
    }
}