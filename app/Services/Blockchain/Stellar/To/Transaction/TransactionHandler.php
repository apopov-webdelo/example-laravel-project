<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/21/18
 * Time: 16:20
 */

namespace App\Services\Blockchain\Stellar\To\Transaction;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\To\Transaction\TransactionContract;
use App\Events\Blockchain\To\Transaction\BlockchainTransaction;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Amount;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

/**
 * Class TransactionService
 *
 * Make direct transaction between users in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class TransactionHandler extends AbstractHandler implements TransactionContract
{
    use Userable, CryptoCurrency, Amount;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = BlockchainTransaction::class;


    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/send';

    /**
     * @var User
     */
    protected $receiver;

    /**
     * {@inheritdoc}
     *
     * @param User                   $remitter
     * @param User                   $receiver
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $amount
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function transaction(
        User $remitter,
        User $receiver,
        CryptoCurrencyContract $cryptoCurrency,
        string $amount
    ): bool {
        $this->setUser($remitter)
             ->setCryptoCurrency($cryptoCurrency)
             ->setReceiver($receiver)
             ->setAmount($amount);

        $result = $this->processRequest();

        $this->fireEvent($this->event, $remitter);

        return $result;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    protected function setReceiver(User $user)
    {
        $this->receiver = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'     => $this->user->id,
            'crypto_type' => $this->cryptoCurrency->getCode(),
            'amount'      => currencyToCoins($this->amount, $this->cryptoCurrency),
            'receiver_id' => $this->receiver->id,
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            currencyToCoins($this->amount, $this->cryptoCurrency),
            $this->cryptoCurrency->getCode(),
            $this->receiver->id,
        ];
    }
}
