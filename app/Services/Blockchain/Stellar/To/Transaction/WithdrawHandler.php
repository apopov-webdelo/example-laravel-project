<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/21/18
 * Time: 16:20
 */

namespace App\Services\Blockchain\Stellar\To\Transaction;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Services\Blockchain\To\Transaction\WithdrawContract;
use App\Events\Blockchain\To\Transaction\BlockchainWithdraw;
use App\Services\Blockchain\Stellar\To\AbstractHandler;

/**
 * Class WithdrawHandler
 *
 * Make withdraw transaction in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class WithdrawHandler extends AbstractHandler implements WithdrawContract
{
    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = BlockchainWithdraw::class;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * @var PayOutContract
     */
    private $payOut;

    /**
     * {@inheritdoc}
     *
     * @param PayOutContract $payOut
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function withdraw(PayOutContract $payOut): bool
    {
        $this->payOut = $payOut;

        $result = $this->processRequest();

        $this->fireEvent($this->event, $this->payOut);

        return $result;
    }

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'wallet/out';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        $payOut = $this->payOut;
        $data = collect([
            'user_id'       => $payOut->user()->getId(),
            'crypto_type'   => $payOut->cryptoCurrency()->getCode(),
            'amount'        => currencyFromCoins($payOut->amount(), $payOut->cryptoCurrency()),
            'commission'    => currencyFromCoins($payOut->commission(), $payOut->cryptoCurrency()),
            'crypto_wallet' => $payOut->walletId(),
            'payout_id'     => $payOut->id(),
        ]);

        return $data->toArray();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        $payOut = $this->payOut;
        return [
            $payOut->user()->getId(),
            currencyFromCoins($payOut->amount(), $payOut->cryptoCurrency()),
            currencyFromCoins($payOut->commission(), $payOut->cryptoCurrency()),
            $payOut->cryptoCurrency()->getCode(),
            $payOut->walletId(),
            $payOut->id(),
        ];
    }
}
