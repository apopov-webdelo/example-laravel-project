<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/19/18
 * Time: 15:34
 */

namespace App\Services\Blockchain\Stellar\To\User;

use App\Contracts\Services\Blockchain\To\User\CreateUserContract;
use App\Events\Blockchain\To\User\UserEvent;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Timestamp;
use App\Services\Traits\Userable;

/**
 * Class UserCreateService
 *
 * @package App\Services\Blockchain\Stellar
 */
class UserCreateHandler extends AbstractHandler implements CreateUserContract
{
    use Userable, Timestamp;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = UserEvent::class;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/create';

    /**
     * {@inheritdoc}
     *
     * @param User $user
     *
     * @return bool
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function createUser(User $user): bool
    {
        $this->setUser($user);

        $result =  $this->processRequest();

        $this->fireEvent($this->event, $user);

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'   => $this->user->id,
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
        ];
    }
}
