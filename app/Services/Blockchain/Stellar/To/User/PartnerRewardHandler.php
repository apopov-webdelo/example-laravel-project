<?php

namespace App\Services\Blockchain\Stellar\To\User;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Contracts\Services\Blockchain\To\User\PartnerRewardedContract;
use App\Events\Blockchain\To\User\PartnerRewardedEvent;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Amount;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

/**
 * Class UserCreateService
 *
 * @package App\Services\Blockchain\Stellar
 */
class PartnerRewardHandler extends AbstractHandler implements PartnerRewardedContract
{
    use Userable, Amount, CryptoCurrency;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = PartnerRewardedEvent::class;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/partner/enroll';

    /**
     * @var PartnershipCommissionStorageContract
     */
    protected $commissionStorage;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'     => $this->user->id,
            'crypto_type' => $this->cryptoCurrency->getCode(),
            'amount'      => currencyFromCoins($this->amount, $this->cryptoCurrency),
            'entity_id'   => $this->commissionStorage->entity()->entityId(),
            'entity_type' => $this->commissionStorage->entity()->entityType(),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            currencyFromCoins($this->amount, $this->cryptoCurrency),
            $this->cryptoCurrency->getCode(),
            $this->commissionStorage->entity()->entityId(),
            $this->commissionStorage->entity()->entityType(),
        ];
    }

    /**
     * Send partner reward info to blockchain
     *
     * @param PartnershipCommissionStorageContract $commissionStorage
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function reward(PartnershipCommissionStorageContract $commissionStorage): bool
    {
        $this->setUser($commissionStorage->partner()->getUser());
        $this->setCryptoCurrency($commissionStorage->cryptoCurrency());
        $this->setAmount($commissionStorage->amount());
        $this->commissionStorage = $commissionStorage;

        $result = $this->processRequest();

        $this->fireEvent($this->event, $this->user);

        return $result;
    }
}
