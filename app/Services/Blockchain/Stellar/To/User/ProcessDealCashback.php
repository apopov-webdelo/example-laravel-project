<?php

namespace App\Services\Blockchain\Stellar\To\User;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Contracts\Services\Blockchain\To\User\ProcessDealCashbackContract;
use App\Events\Blockchain\To\User\DealCashbackRewardedEvent;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Amount;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;

class ProcessDealCashback extends AbstractHandler implements ProcessDealCashbackContract
{
    use Userable, Amount, CryptoCurrency;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealCashbackRewardedEvent::class;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'user/cashback/add';

    /**
     * @var DealCashbackCommissionStorageContract
     */
    protected $commissionStorage;

    /**
     * @param DealCashbackCommissionStorageContract $commissionStorage
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function reward(DealCashbackCommissionStorageContract $commissionStorage): bool
    {
        $this->setUser($commissionStorage->user());
        $this->setCryptoCurrency($commissionStorage->cryptoCurrency());
        $this->setAmount($commissionStorage->amount());
        $this->commissionStorage = $commissionStorage;

        $result = $this->processRequest();

        $this->fireEvent($this->event, $this->user);

        return $result;
    }

    /**
     * Return params that will be sent with http request
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [
            'user_id'     => $this->user->id,
            'crypto_type' => $this->cryptoCurrency->getCode(),
            'amount'      => currencyFromCoins($this->amount, $this->cryptoCurrency),
            'entity_id'   => $this->commissionStorage->entity()->entityId(),
            'entity_type' => $this->commissionStorage->entity()->entityType(),
            'deals'       => $this->commissionStorage->deals()->pluck('id')->implode('id', ','),
        ];
    }

    /**
     * Return array with element for control-sum generation
     *
     * Warning! You do not include private key! It will be added automatically!
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->user->id,
            currencyFromCoins($this->amount, $this->cryptoCurrency),
            $this->cryptoCurrency->getCode(),
            $this->commissionStorage->entity()->entityId(),
            $this->commissionStorage->entity()->entityType(),
            $this->commissionStorage->deals()->pluck('id')->implode('id', ','),
        ];
    }
}
