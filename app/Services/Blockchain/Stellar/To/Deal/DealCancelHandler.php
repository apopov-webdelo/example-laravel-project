<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/16/18
 * Time: 14:59
 */

namespace App\Services\Blockchain\Stellar\To\Deal;

use App\Contracts\Services\Blockchain\To\Deal\DealCancelContract;
use App\Events\Blockchain\To\Deal\DealCancel;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\Traits\Timestamp;
use App\Services\Traits\Dealable;

/**
 * Class DealCancelService for sending request to finish deal in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class DealCancelHandler extends DealRequestHandler implements DealCancelContract
{
    use Dealable, Timestamp;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealCancel::class;

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'deal/cancel';

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return bool
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \ReflectionException
     * @throws \App\Exceptions\Event\UndefinedEventException
     */
    public function cancel(Deal $deal): bool
    {
        $result = $this->process($deal);

        $this->fireEvent($this->event, $deal);

        return $result;
    }
}
