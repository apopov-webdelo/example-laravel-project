<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/16/18
 * Time: 14:59
 */

namespace App\Services\Blockchain\Stellar\To\Deal;

use App\Contracts\Services\Blockchain\To\CreateKeysContract;
use App\Contracts\Services\Blockchain\To\Deal\DealCreateContract;
use App\Contracts\Services\Blockchain\To\Key\KeysStorageContract;
use App\Events\Blockchain\To\Deal\DealCreate;
use App\Exceptions\Blockchain\Stellar\InvalidKeyStellarException;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\CheckKey;
use App\Services\Traits\Dealable;

/**
 * Class DealCreateService
 *
 * @package App\Services\Blockchain\Stellar
 */
class DealCreateHandler extends AbstractHandler implements DealCreateContract
{
    use CheckKey, Dealable;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealCreate::class;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'deal/create';

    /**
     * Buyer's public key to confirmation smart contract in blockchain
     *
     * @var string
     */
    protected $buyerKey;

    /**
     * Seller's public key to confirmation smart contract in blockchain
     *
     * @var string
     */
    protected $sellerKey;

    /**
     * Server's keys to confirmation smart contract in blockchain
     *
     * @var KeysStorageContract
     */
    protected $serverKeys;

    /**
     * Crypto wallet ID for automatically transaction
     *
     * @var string
     */
    protected $wallet;

    /**
     * {@inheritdoc}
     *
     * @param string $buyerKey
     * @param string $sellerKey
     *
     * @return DealCreateContract
     *
     * @throws InvalidKeyStellarException
     */
    public function keys(string $buyerKey, string $sellerKey): DealCreateContract
    {
        $this->checkKey('buyer', $buyerKey)->checkKey('seller', $sellerKey);
        $this->buyerKey = $buyerKey;
        $this->sellerKey = $sellerKey;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $walletId
     *
     * @return DealCreateContract
     */
    public function wallet(string $walletId): DealCreateContract
    {
        $this->wallet = $walletId;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return string
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function create(Deal $deal): string
    {
        $this->setDeal($deal);

        if ($this->isProtectedDeal()) {
            $this->generateServerKeys()->saveServerKeys();
        }

        $data = $this->processRequest();
        $walletId = $data->wallet_id;

        $this->saveWalletToDeal($walletId);

        $this->fireEvent($this->event, $deal);

        return $walletId;
    }

    /**
     * Check is deal protected
     *
     * @return bool
     */
    protected function isProtectedDeal(): bool
    {
        return ($this->sellerKey && $this->buyerKey);
    }

    /**
     * Use Key Service for keys pair generation
     *
     * @return $this
     */
    protected function generateServerKeys()
    {
        /** @var CreateKeysContract $keyService */
        $keyService = app(CreateKeysContract::class);
        $this->serverKeys = $keyService->createKeys();

        return $this;
    }

    /**
     * Save relation deal with keys
     *
     * @return $this
     */
    protected function saveServerKeys()
    {
        $keys = $this->serverKeys;
        $this->deal->saveBcKey($keys->privateKey(), $keys->privateAlias());
        $this->deal->saveBcKey($keys->publicKey(), $keys->publicAlias());

        return $this;
    }

    /**
     * Save wallet and relate it with deal
     *
     * @param string $walletId
     *
     * @return $this
     */
    protected function saveWalletToDeal(string $walletId)
    {
        $this->deal->saveWallet($walletId, 'deal_wallet');

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        $deal = $this->deal;
        $data = collect([
            'deal_id'     => $deal->id,
            'amount'      => currencyFromCoins($deal->crypto_amount, $deal->cryptoCurrency),
            'commission'  => currencyFromCoins($deal->commission, $deal->cryptoCurrency),
            'crypto_type' => $deal->cryptoCurrency->getCode(),

            'buyer_id'  => $deal->getBuyer()->id,
            'seller_id' => $deal->getSeller()->id,
            'author_id' => $deal->ad->author_id,
        ]);

        empty($this->sellerKey) ? : $data->merge([
            'seller_pub_key' => $this->sellerKey,
            'buyer_pub_key'  => $this->buyerKey,
            'server_pub_key' => $this->serverKeys->publicKey(),
        ]);

        empty($this->wallet) ? : $data->merge([
            'crypto_wallet' => $this->wallet,
        ]);

        return $data->toArray();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->deal->id,
            currencyFromCoins($this->deal->crypto_amount, $this->deal->cryptoCurrency),
            $this->deal->cryptoCurrency->getCode(),
        ];
    }
}
