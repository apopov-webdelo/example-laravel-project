<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/21/18
 * Time: 13:51
 */

namespace App\Services\Blockchain\Stellar\To\Deal;

use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Timestamp;
use App\Services\Traits\Dealable;

/**
 * Abstract Class for send standart deal request to blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
abstract class DealRequestHandler extends AbstractHandler
{
    use Dealable, Timestamp;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return string
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    protected function process(Deal $deal): bool
    {
        $this->setDeal($deal);

        return $this->processRequest();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        $deal = $this->deal;
        $data = collect([
            'deal_id'   => $deal->id,
        ]);

        empty($this->isProtectedDeal()) ?: $data->merge($this->getPubKeys());

        return $data->toArray();
    }

    /**
     * Check is deal protected
     *
     * @return bool
     */
    protected function isProtectedDeal(): bool
    {
        return $this->deal->isProtected();
    }

    /**
     * Retrieve deals public keys
     *
     * @return array
     */
    protected function getPubKeys(): array
    {
        return [
            'second_private_key' => $this->deal->getBcKey('offer'),
            'server_private_key' => $this->getPrivateKey(),
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [
            $this->deal->id,
        ];
    }
}
