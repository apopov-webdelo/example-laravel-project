<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/16/18
 * Time: 14:59
 */

namespace App\Services\Blockchain\Stellar\To\Deal;

use App\Contracts\Services\Blockchain\To\Deal\DealFinishContract;
use App\Events\Blockchain\To\Deal\DealFinish;
use App\Models\Deal\Deal;
use App\Services\Blockchain\Stellar\Traits\Timestamp;
use App\Services\Traits\Dealable;

/**
 * Class DealFinishService for sending request to finish deal in blockchain
 *
 * @package App\Services\Blockchain\Stellar
 */
class DealFinishHandler extends DealRequestHandler implements DealFinishContract
{
    use Dealable, Timestamp;

    /**
     * Event will be fired after successfully processing
     *
     * @var string
     */
    protected $event = DealFinish::class;

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'deal/finish';

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return bool
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     * @throws \App\Exceptions\Event\UndefinedEventException
     * @throws \ReflectionException
     */
    public function finish(Deal $deal): bool
    {
        $result = $this->process($deal);

        $this->fireEvent($this->event, $deal);

        return $result;
    }
}
