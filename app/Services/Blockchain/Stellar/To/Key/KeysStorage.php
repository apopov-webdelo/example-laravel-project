<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/16/18
 * Time: 17:15
 */

namespace App\Services\Blockchain\Stellar\To\Key;

use App\Contracts\Services\Blockchain\To\Key\KeysStorageContract;
use App\Exceptions\Blockchain\Stellar\InvalidKeyStellarException;
use App\Services\Blockchain\Stellar\Traits\CheckKey;

/**
 * Class KeysStorage
 *
 * @package App\Services\Blockchain\Stellar
 */
class KeysStorage implements KeysStorageContract
{
    use CheckKey;

    /**
     * @var string
     */
    protected $privateKey;

    /**
     * @var string
     */
    protected $publicKey;

    /**
     * @var string
     */
    protected $privateAlias = 'private';

    /**
     * @var string
     */
    protected $publicAlias = 'public';

    /**
     * KeysStorage constructor.
     *
     * @param string $private
     * @param string $public
     *
     * @throws InvalidKeyStellarException
     */
    public function __construct(string $private, string $public)
    {
        $this->checkKey($this->privateAlias(), $private)
             ->checkKey($this->publicAlias(), $public);

        $this->privateKey = $private;
        $this->publicKey  = $public;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function privateKey(): string
    {
        return $this->privateKey;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function publicKey(): string
    {
        return $this->publicKey;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function privateAlias(): string
    {
        return $this->privateAlias;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function publicAlias(): string
    {
        return $this->publicAlias;
    }
}
