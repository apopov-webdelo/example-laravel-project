<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/16/18
 * Time: 17:07
 */

namespace App\Services\Blockchain\Stellar\To\Key;

use App\Contracts\Services\Blockchain\To\CreateKeysContract;
use App\Contracts\Services\Blockchain\To\Key\KeysStorageContract;
use App\Services\Blockchain\Stellar\To\AbstractHandler;
use App\Services\Blockchain\Stellar\Traits\Timestamp;

/**
 * Class KeysService
 *
 * @package App\Services\Blockchain\Stellar
 */
class KeysService extends AbstractHandler implements CreateKeysContract
{
    use Timestamp;

    /**
     * Http method for request
     *
     * @var string
     */
    protected $method = 'POST';

    /**
     * Route for http request
     *
     * @var string
     */
    protected $uri = 'wallet/generate';

    /**
     * {@inheritdoc}
     *
     * @return KeysStorageContract
     *
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function createKeys(): KeysStorageContract
    {
        $response = $this->processRequest();

        return app(
            KeysStorageContract::class,
            [
                'private' => $response->private_key,
                'public'  => $response->public_key,
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getRequestData(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function getControlData(): array
    {
        return [];
    }
}
