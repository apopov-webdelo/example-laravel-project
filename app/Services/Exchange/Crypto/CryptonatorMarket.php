<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 06:51
 */

namespace App\Services\Exchange\Crypto;

use App\Exceptions\Exchange\ExchangeException;

/**
 * Class CryptonatorCom
 * @package App\Services\Exchange\Crypto
 */
class CryptonatorMarket extends CryptoMarket
{
    const JSON_URL = 'https://api.cryptonator.com/api/ticker/';

    const CACHE_KEY = 'CryptonatorCom';

    const CONFIG_KEY = 'app.exchange.crypto.markets.cryptonator';

    /**
     * Value in minutes
     */
    const DEFAULT_CACHE_LIFETIME = 30;

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \Exception
     */
    protected function loadRate(): float
    {
        $json = file_get_contents(self::JSON_URL. $this->from.'-'. $this->to);
        $obj = json_decode($json);

        if (false == is_object($obj)) {
            throw new \Exception('Unexpected response was got from gate!');
        }

        if (false == $obj->success) {
            throw new ExchangeException('Exchange gate error: '.$obj->success);
        }

        $rate = (float)$obj->ticker->price;

        if (empty($rate)) {
            throw new ExchangeException('Was return empty rate value: '.$rate);
        }

        return $rate;
    }

    /**
     * Return cache key for current driver
     *
     * @return string
     */
    protected function getCacheKey(): string
    {
        return self::CACHE_KEY;
    }

    /**
     * Return cache lifetime value in minutes
     *
     * @return int
     */
    protected function getCacheLifetime(): int
    {
        return (int)(config(self::CONFIG_KEY)['cache_lifetime'] ?? self::DEFAULT_CACHE_LIFETIME);
    }
}
