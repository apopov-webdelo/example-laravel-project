<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 06:51
 */

namespace App\Services\Exchange\Crypto;

use App\Contracts\Currency\CurrencyContract;
use App\Contracts\Exchange\Crypto\FactoryContract;
use App\Contracts\Exchange\Crypto\MarketContract;
use App\Exceptions\Exchange\ExchangeException;
use App\Exceptions\Exchange\UnknownCurrencyCodeException;
use App\Exceptions\Exchange\UnsupportedCurrencyObjectException;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class CryptonatorCom
 * @package App\Services\Exchange\Crypto
 */
abstract class CryptoMarket implements MarketContract
{
    /**
     * Code for primary currency
     *
     * @var string
     */
    protected $from;

    /**
     * Code for target currency
     *
     * @var string
     */
    protected $to;

    /**
     * Return instanced market engine
     *
     * @param string|null $driver
     * @return MarketContract
     */
    public function market(string $driver = null): MarketContract
    {
        return app(FactoryContract::class)->market($driver);
    }

    /**
     * Return supported all currency list
     *
     * @return Collection
     */
    public function currencies(): Collection
    {
        return Currency::all('code')->pluck('code');
    }

    /**
     * Return all supported crypto currency list
     *
     * @return Collection
     */
    public function cryptoCurrencies(): Collection
    {
        return CryptoCurrency::all('code')->pluck('code');
    }

    /**
     * Return rate for currencies
     *
     * Class check rates for Crypto/Fiat and for Crypto/Crypto exchanges
     *
     * @param string|Currency|CryptoCurrency $from
     * @param string|Currency|CryptoCurrency $to
     * @param bool                           $cache Is need to use cache
     *
     * @return float
     *
     * @throws UnsupportedCurrencyObjectException
     * @throws UnknownCurrencyCodeException
     * @throws ExchangeException
     */
    public function rate($from, $to, bool $cache = true): float
    {
        return $this->setCurrencies($from, $to)
                    ->getRate($cache);
    }

    /**
     * Set currencies codes to local properties
     *
     * @param string|Currency|CryptoCurrency $from
     * @param string|Currency|CryptoCurrency $to
     *
     * @return self
     *
     * @throws UnsupportedCurrencyObjectException
     * @throws UnknownCurrencyCodeException
     * @throws ExchangeException
     */
    protected function setCurrencies($from, $to): self
    {
        $this->from = strtolower($this->getCurrencyCode($from));
        $this->to   = strtolower($this->getCurrencyCode($to));

        return $this;
    }

    /**
     * @param string|Currency|CryptoCurrency $currency
     *
     * @return string
     *
     * @throws UnsupportedCurrencyObjectException
     * @throws UnknownCurrencyCodeException
     */
    protected function getCurrencyCode($currency): string
    {
        if (is_string($currency)) {
            if (strlen($currency) == 3) {
                return $currency;
            }
            throw new UnknownCurrencyCodeException('Currency code must contains only 3 symbols!');
        }

        if ($currency instanceof CurrencyContract) {
            return $currency->getCode();
        }

        throw new UnsupportedCurrencyObjectException('Class '.get_class($currency).' not supported!');
    }

    /**
     * Return rate value for currencies pair
     *
     * @param bool $cache
     *
     * @return float
     *
     * @throws ExchangeException
     */
    protected function getRate(bool $cache = true): float
    {
        $rate = $cache ? $this->getCache() : null;
        if (empty($rate)) {
            $rate = $this->loadRate();
            $this->putCache($rate);
        }
        return $rate;
    }

    /**
     * Return rate from cache if exists
     *
     * @return mixed
     */
    protected function getCache()
    {
        return $this->getCacheEngine()->get($this->getCacheKey());
    }

    /**
     * Return instanced cache engine with set tags
     *
     * @return Cache
     */
    protected function getCacheEngine()
    {
        $key = $this->getCacheKey();
        return Cache::tags([$key, $key.$this->from, $key.$this->to]);
    }

    /**
     * Return cache key for current driver
     *
     * @return string
     */
    abstract protected function getCacheKey(): string;

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \Exception
     */
    abstract protected function loadRate(): float;

    /**
     * Set cache for currencies pair
     *
     * @param float $rate
     *
     * @return $this
     */
    protected function putCache(float $rate)
    {
        $this->getCacheEngine()->put(
            $this->getCacheKey(),
            $rate,
            $this->getCacheLifetime()
        );
        return $this;
    }

    /**
     * Return cache lifetime value in minutes
     *
     * @return int
     */
    abstract protected function getCacheLifetime(): int;

    /**
     * Convert sum according currencies
     *
     * @param float                            $sum
     * @param string|Currency|CryptoCurrency $from
     * @param string|Currency|CryptoCurrency $to
     * @param int                            $accuracy
     * @param bool                           $cache
     *
     * @return float|int
     *
     * @throws UnsupportedCurrencyObjectException
     * @throws UnknownCurrencyCodeException
     * @throws ExchangeException
     */
    public function convert(float $sum, $from, $to, int $accuracy = 0, bool $cache = true)
    {
        $amount = $sum * $this->rate($from, $to, $cache);
        return $accuracy
            ? $amount / pow(10, $accuracy)
            : (int)$amount;
    }

    /**
     * Refresh rates (upload from API, update cache)
     *
     * @param string|Currency|CryptoCurrency|null $currency
     *
     * @return self
     *
     * @throws UnsupportedCurrencyObjectException
     * @throws UnknownCurrencyCodeException
     * @throws ExchangeException
     */
    public function refresh($currency = null): MarketContract
    {
        /** @var Cache $cache */
        $cache = (isset($currency))
            ? Cache::tags([$this->getCacheKey().$this->getCurrencyCode($currency)])
            : Cache::tags([$this->getCacheKey()]);

        $cache->flush();
        return $this;
    }
}
