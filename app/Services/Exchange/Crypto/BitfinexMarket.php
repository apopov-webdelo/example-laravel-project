<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 06:51
 */

namespace App\Services\Exchange\Crypto;

use App\Exceptions\Exchange\ExchangeException;
use codenixsv\Bitfinex\BitfinexManager;

/**
 * Class CryptonatorCom
 * @package App\Services\Exchange\Crypto
 */
class BitfinexMarket extends CryptoMarket
{
    const CACHE_KEY = 'Bitfinex';

    const CONFIG_KEY = 'app.exchange.crypto.markets.bitfinex';

    /**
     * Value in minutes
     */
    const DEFAULT_CACHE_LIFETIME = 30;

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \Exception
     */
    protected function loadRate() : float
    {
        $manager = new BitfinexManager();
        $client = $manager->createClient();

        $response = $client->getTicker($this->from. $this->to);

        $json = json_decode($response);

        if (false == is_object($json)) {
            throw new \Exception(
                'Unexpected response was got from gate! (Response:'.(string)$response.')'
            );
        }

        if (isset($json->message) && empty($json->mid)) {
            throw new ExchangeException('Exchange gate error: '.$json->message);
        }

        return (float)$json->mid;
    }

    /**
     * Return cache key for current driver
     *
     * @return string
     */
    protected function getCacheKey() : string
    {
        return self::CACHE_KEY;
    }

    /**
     * Return cache lifetime value in minutes
     *
     * @return int
     */
    protected function getCacheLifetime() : int
    {
        return (int)(config(self::CONFIG_KEY)['cache_lifetime'] ?? self::DEFAULT_CACHE_LIFETIME);
    }
}
