<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 06:51
 */

namespace App\Services\Exchange\Crypto;

use App\Exceptions\Exchange\ExchangeException;
use Illuminate\Support\Facades\Log;

/**
 * Class CryptonatorCom
 * @package App\Services\Exchange\Crypto
 */
class AverageMarket extends CryptoMarket
{
    const CACHE_KEY = 'cryptoExchangeAverage';

    const CONFIG_MARKETS_KEY = 'app.exchange.crypto.markets';

    const CONFIG_KEY = 'average';

    /**
     * Value in minutes
     */
    const DEFAULT_CACHE_LIFETIME = 30;

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \Exception
     */
    protected function loadRate() : float
    {
        $markets = config(self::CONFIG_MARKETS_KEY);
        unset($markets[self::CONFIG_KEY]);

        $rate = 0;
        $drivers = 0;

        foreach ($markets as $market => $marketConfig) {
            try {
                $driver = $this->market($market);
                $rate += $driver->rate($this->from, $this->to, false);
                $drivers++;
            } catch (\Exception $exception) {
                Log::alert(
                    'Unexpected error in CryptoExchange driver '.$marketConfig['driver'].'!',
                    ['message' => $exception->getMessage()]
                );
            }
        }

        if ($drivers) {
            return $rate/$drivers;
        }

        Log::error('All CryptoExchange drivers have fallen down!');
        throw new ExchangeException('All Crypto Exchange drivers have fallen down!');
    }

    /**
     * Return cache key for current driver
     *
     * @return string
     */
    protected function getCacheKey() : string
    {
        return self::CACHE_KEY;
    }

    /**
     * Return cache lifetime value in minutes
     *
     * @return int
     */
    protected function getCacheLifetime() : int
    {
        return (int)(config(self::CONFIG_KEY)['cache_lifetime'] ?? self::DEFAULT_CACHE_LIFETIME);
    }
}
