<?php

namespace App\Services\Exchange\Currency;

use App\Exceptions\Exchange\ExchangeException;
use Illuminate\Support\Facades\Log;

class AverageFiatExchange extends AbstractFiatExchange
{
    const CONFIG_MARKETS_KEY = 'app.exchange.fiat.markets';
    const SELF_CONFIG = 'average';

    /**
     * @var
     */
    protected $cacheKey = 'AverageFiatExchange';

    /**
     * @var
     */
    protected $configKey = 'app.exchange.fiat.markets.average';

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     */
    protected function loadRate(): float
    {
        $markets = config(self::CONFIG_MARKETS_KEY);
        unset($markets[static::SELF_CONFIG]);

        $rate = 0;
        $drivers = 0;

        foreach ($markets as $market => $marketConfig) {
            try {
                $driver = $this->market($market);
                $newRate = $driver->rate($this->from, $this->to);

                if ($newRate > 0) {
                    $rate += $newRate;
                    $drivers++;
                }
            } catch (\Exception $exception) {
                Log::alert(
                    'Unexpected error in AverageFiatExchange  driver '.$marketConfig['driver'].'!',
                    ['message' => $exception->getMessage()]
                );
            }
        }

        if ($drivers) {
            return $rate/$drivers;
        }

        Log::error('All FiatExchange drivers have fallen down!');

        throw new ExchangeException('All FiatExchange drivers have fallen down!');
    }
}
