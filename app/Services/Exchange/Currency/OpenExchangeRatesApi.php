<?php

namespace App\Services\Exchange\Currency;

use App\Contracts\Exchange\Fiat\FiatBatchUpdateContract;
use App\Exceptions\Exchange\ExchangeException;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

/**
 * Class OpenExchangeRatesApi
 *
 * @package App\Services\Exchange\Currency
 */
class OpenExchangeRatesApi extends AbstractFiatExchange implements FiatBatchUpdateContract
{
    const JSON_URL = 'https://openexchangerates.org/api/latest.json';

    /**
     * @var
     */
    protected $cacheKey = 'openexchangerates';

    /**
     * @var
     */
    protected $configKey = 'app.exchange.fiat.markets.openexchangerates';

    /**
     * If service hase only one base currency and current request is revereted.
     * i.e original request UAH/USD => will get rate=USD/UAH and return (1 / rate)
     *
     * @var bool
     */
    protected $isReversed = false;

    /**
     * @return \GuzzleHttp\Client
     */
    protected function getClient()
    {
        return new \GuzzleHttp\Client([
            'base_uri' => static::JSON_URL,
            'timeout'  => 5,
        ]);
    }

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function loadRate(): float
    {
        $config = $this->getConfig();

        $from = $this->from;
        $to = $this->to;

        // free plan API is limited to this base
        $base = $config['base'];
        if ($base && $base !== $from) {
            $to = $from;
            $from = $base;
            $this->isReversed = true;
        }

        try {
            $response = $this->request($config, $from, $to);

            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    throw new ExchangeException(
                        'Unexpected response got from gate! (Response: ' . (string)$response . ')'
                    );
                }

                $rate = (float)$json->rates->{strtoupper($to)};

                return ($this->isReversed) ? 1 / $rate : $rate;
            }

            return 0;
        } catch (\Exception $e) {
            throw new ExchangeException($e->getMessage());
        }
    }

    /**
     * Batch update rates for from/[all currencies] and [all currencies]/from
     *
     * @param string     $from
     * @param Collection $currencies
     *
     * @throws ExchangeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function batchUpdate(string $from, Collection $currencies): void
    {
        $config = $this->getConfig();

        try {
            $response = $this->request(
                $config,
                $from,
                $currencies->implode(',')
            );

            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    throw new ExchangeException(
                        'Unexpected response got from gate! (Response: ' . (string)$response . ')'
                    );
                }

                $rates = collect($json->rates);

                $rates->each(function ($rate, $currency) use ($from) {
                    // cache straight pair
                    $this->from = $from;
                    $this->to = $currency;
                    $this->putCache($rate);

                    // cache reverse pair
                    $this->from = $currency;
                    $this->to = $from;
                    $this->putCache(1 / $rate);
                });
            }

            throw new ExchangeException('Response code not OK: ' . $response->getBody()->getContents());
        } catch (\Exception $e) {
            throw new ExchangeException($e->getMessage());
        }
    }

    /**
     * @param array  $config
     * @param string $from
     * @param string $to
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request(array $config, string $from, string $to)
    {
        $client = $this->getClient();

        $response = $client->request('GET', '', [
            'query'       => [
                'app_id'      => $config['api_key'],
                'prettyprint' => 0,
                'base'        => $from,
                'symbols'     => $to,
            ],
            'http_errors' => false,
        ]);

        return $response;
    }
}
