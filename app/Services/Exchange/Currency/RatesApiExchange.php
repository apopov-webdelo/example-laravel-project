<?php

namespace App\Services\Exchange\Currency;

use App\Exceptions\Exchange\ExchangeException;
use Illuminate\Http\Response;

class RatesApiExchange extends AbstractFiatExchange
{
    const JSON_URL = 'https://api.exchangeratesapi.io/latest';

    /**
     * @var
     */
    protected $cacheKey = 'exchangeratesapi.io';

    /**
     * @var
     */
    protected $configKey = 'app.exchange.fiat.markets.ratesapi';

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function loadRate(): float
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => static::JSON_URL,
            'timeout'  => 5,
        ]);

        try {
            $response = $client->request('GET', 'latest', [
                'query'       => [
                    'base'    => strtoupper($this->from),
                    'symbols' => strtoupper($this->to),
                ],
                'http_errors' => false,
            ]);

            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    throw new ExchangeException(
                        'Unexpected response got from gate! (Response:' . (string)$response . ')'
                    );
                }

                return (float)$json->rates->{strtoupper($this->to)};
            }

            return 0;
        } catch (\Exception $e) {
            throw new ExchangeException($e->getMessage());
        }
    }
}
