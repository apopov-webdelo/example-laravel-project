<?php

namespace App\Services\Exchange\Currency;

use App\Contracts\Exchange\Fiat\ExchangeContract;
use App\Contracts\Exchange\Fiat\FiatFactoryContract;
use App\Exceptions\Exchange\ExchangeException;
use App\Exceptions\Exchange\UnknownCurrencyCodeException;
use App\Models\Directory\Currency;
use Illuminate\Cache\TaggedCache;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class AbstractFiatExchange
 *
 * @package App\Services\Exchange\Currency
 */
abstract class AbstractFiatExchange implements ExchangeContract
{
    const DEFAULT_CACHE_LIFETIME = 1440; // minutes

    /**
     * Code for primary currency
     *
     * @var string
     */
    protected $from;

    /**
     * Code for target currency
     *
     * @var string
     */
    protected $to;

    /**
     * @var
     */
    protected $cacheKey;

    /**
     * @var
     */
    protected $configKey;

    /**
     * Return cache key for current driver
     *
     * @return string
     */
    protected function getCacheKey(): string
    {
        return $this->cacheKey;
    }

    /**
     * Return cache lifetime value in minutes
     *
     * @return int
     */
    protected function getCacheLifetime(): int
    {
        return (int)(config($this->configKey)['cache_lifetime'] ?? static::DEFAULT_CACHE_LIFETIME);
    }

    /**
     * Pairs that are not supported by driver
     *
     * @return array
     */
    protected function getExcludedPairs(): array
    {
        return (config($this->configKey)['excluded_pairs'] ?? []);
    }

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \Exception
     */
    abstract protected function loadRate(): float;

    /**
     * Return instanced market engine
     *
     * @param string|null $driver
     *
     * @return ExchangeContract
     */
    public function market(string $driver = null): ExchangeContract
    {
        return app(FiatFactoryContract::class)->market($driver);
    }

    /**
     * Return all currency list
     *
     * @return Collection
     */
    public function currencies(): Collection
    {
        return Currency::all('code')->pluck('code');
    }

    /**
     * Return rate for currencies
     *
     * @param string $currencyFrom
     * @param string $currencyTo
     * @param bool   $fromCache
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws UnknownCurrencyCodeException
     */
    public function rate(string $currencyFrom, string $currencyTo, bool $fromCache = true): float
    {
        if ($this->isExcludedPair($currencyFrom, $currencyTo)) {
            return 0;
        }

        // try straight pair
        $this->from = strtoupper($currencyFrom);
        $this->to = strtoupper($currencyTo);

        $rate = $this->getRate($fromCache);
        if ($rate) {
            return $rate;
        }

        // try reverse pair
        $this->from = strtoupper($currencyTo);
        $this->to = strtoupper($currencyFrom);

        $rate = $this->getRate($fromCache);
        if ($rate) {
            return 1 / $rate;
        }

        throw new UnknownCurrencyCodeException('There is no rate for ' . $currencyFrom . '/' . $currencyTo . '!');
    }

    /**
     * Convert sum according currencies
     *
     * @param float  $sum
     * @param string $currencyFrom
     * @param string $currencyTo
     *
     * @return float
     *
     * @throws \Exception
     * @throws UnknownCurrencyCodeException
     */
    public function convert(float $sum, string $currencyFrom, string $currencyTo): float
    {
        $rate = $this->rate($currencyFrom, $currencyTo);

        return $sum * $rate;
    }

    /**
     * Return rate value for currencies pair
     *
     * @param bool $cache
     *
     * @return float
     * @throws ExchangeException
     */
    protected function getRate(bool $cache = true): float
    {
        $rate = $cache ? $this->getCache() : null;

        if (empty($rate)) {
            $rate = $this->loadRate();
            $this->putCache($rate);
        }

        return $rate;
    }

    /**
     * Return rate from cache if exists
     *
     * @return mixed
     */
    protected function getCache()
    {
        return $this->getCacheEngine()->get($this->getCacheKey());
    }

    /**
     * Return instanced cache engine with set tags
     *
     * @return TaggedCache
     */
    protected function getCacheEngine()
    {
        $key = $this->getCacheKey();

        return Cache::tags([$key, $key . $this->from . $this->to]);
    }

    /**
     * Set cache for currencies pair
     *
     * @param float $rate
     *
     * @return $this
     */
    protected function putCache(float $rate)
    {
        dump('caching ' . $this->from . '/' . $this->to);
        $this->getCacheEngine()->put(
            $this->getCacheKey(),
            $rate,
            $this->getCacheLifetime()
        );

        return $this;
    }

    /**
     * @param $from
     * @param $to
     *
     * @return bool
     */
    private function isExcludedPair($from, $to): bool
    {
        $pairStraignt = strtoupper($from . '/' . $to);
        $pairReverse = strtoupper($to . '/' . $from);

        $excluded = collect($this->getExcludedPairs());

        return $excluded->contains($pairStraignt) || $excluded->contains($pairReverse);
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getConfig()
    {
        return config($this->configKey);
    }
}
