<?php

namespace App\Services\Exchange\Currency;

use App\Exceptions\Exchange\ExchangeException;
use Illuminate\Http\Response;

class FreeConverterApiExchange extends AbstractFiatExchange
{
    const JSON_URL = 'https://free.currencyconverterapi.com/api/v6/convert';

    /**
     * @var
     */
    protected $cacheKey = 'currencyconverterapi.com';

    /**
     * @var
     */
    protected $configKey = 'app.exchange.fiat.markets.freecc';

    /**
     * Load rate from API
     *
     * @return float
     *
     * @throws ExchangeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function loadRate(): float
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => static::JSON_URL,
            'timeout'  => 5,
        ]);

        try {
            $pairCode = strtoupper($this->from) . '_' . strtoupper($this->to);

            $response = $client->request('GET', '', [
                'query'       => [
                    'q'    => $pairCode,
                    'compact' => 'y',
                ],
                'http_errors' => false,
            ]);

            $code = $response->getStatusCode();

            if ($code === Response::HTTP_OK) {
                $json = json_decode($response->getBody()->getContents());

                if (is_object($json) === false) {
                    throw new ExchangeException(
                        'Unexpected response got from gate! (Response:' . (string)$response . ')'
                    );
                }

                return (float)$json->{$pairCode}->val;
            }

            return 0;
        } catch (\Exception $e) {
            throw new ExchangeException($e->getMessage());
        }
    }
}
