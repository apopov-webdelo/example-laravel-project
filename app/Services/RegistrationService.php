<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 07.11.2018
 * Time: 11:06
 */

namespace App\Services;

use Illuminate\Foundation\Auth\User;

/**
 * Class RegistrationService
 *
 * @package App\Services
 */
abstract class RegistrationService
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param array $data
     *
     * @return User
     */
    abstract public function store(array $data): User;

    /**
     * @return mixed
     */
    abstract protected function fireEvent();
}
