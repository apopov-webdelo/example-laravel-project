<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 13:11
 */

namespace App\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\FiltersDealsStatisticsContract;
use App\Contracts\Services\Statistics\StatisticsContract;
use App\Http\Requests\Stats\DealStatsRequest;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;

abstract class DealStatistics implements
    StatisticsContract,
    FiltersDealsStatisticsContract
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var DealRepo
     */
    protected $dealRepo;

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    abstract public function getStatistics(): array;

    abstract protected function render(array $statistics): array;

    /**
     * DealStatistics constructor.
     *
     * @param DealRepo              $dealRepo
     */
    public function __construct(DealRepo $dealRepo)
    {
        $this->dealRepo = $dealRepo;
    }

    /**
     * @param DealStatsRequest $request
     *
     * @return $this
     */
    public function filterFromRequest(DealStatsRequest $request)
    {
        if ($request->from) {
            $this->dateFrom(Carbon::parse($request->from));
        }

        if ($request->to) {
            $this->dateTo(Carbon::parse($request->to));
        }

        if ($request->for_sale && !$request->for_buy) {
            $this->sale();
        }

        if ($request->for_buy && !$request->for_sale) {
            $this->buy();
        }

        if ($request->cryptocurrency_id) {
            $this->cryptoCurrency($request->cryptocurrency_id);
        }

        if ($request->currency_id) {
            $this->currency($request->currency_id);
        }

        if ($request->payment_system_id) {
            $this->paymentSystem($request->payment_system_id);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param User $user
     *
     * @return $this
     */
    public function member(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return StatisticsContract
     */
    public function reset(): StatisticsContract
    {
        $this->dealRepo->reset();
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function dateFrom(Carbon $date)
    {
        $this->dealRepo->filterByCreatedAtFrom($date);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function dateTo(Carbon $date)
    {
        $this->dealRepo->filterByCreatedAtTo($date);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function buy()
    {
        $this->dealRepo->filterForBuy($this->user);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function sale()
    {
        $this->dealRepo->filterForSale($this->user);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param CryptoCurrency|int $cryptoCurrency
     *
     * @return $this
     */
    public function cryptoCurrency($cryptoCurrency)
    {
        $this->dealRepo->filterByCryptoCurrency($cryptoCurrency);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param Currency|int $currency
     *
     * @return $this
     */
    public function currency($currency)
    {
        $this->dealRepo->filterByCurrency($currency);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param PaymentSystem|int $paymentSystem
     *
     * @return $this
     */
    public function paymentSystem($paymentSystem)
    {
        $this->dealRepo->filterByPaymentSystem($paymentSystem);
        return $this;
    }

    /**
     * Clone deal repo object and filter by member
     *
     * @return DealRepo
     */
    protected function getRepoClone() : DealRepo
    {
        $clone = clone $this->dealRepo;
        return $clone->filterByDealMember($this->user)
                     ->filterByStatusId(DealStatusConstants::FINISHED);
    }

    /**
     * Get specific field array from repo of deals (by current filter)
     *
     * @param string $field
     *
     * @return array
     */
    protected function getFromAdsByField(string $field): array
    {
        $repo = $this->getRepoClone();

        $deals = $repo->take()->with("ad:id,{$field}")->get();

        $deals = $deals->groupBy(["ad.{$field}"])
                       ->toArray();

        return array_keys($deals);
    }
}
