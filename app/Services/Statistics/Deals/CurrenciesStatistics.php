<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 15:37
 */

namespace App\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\CurrenciesStatisticsContract;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;

class CurrenciesStatistics extends DealStatistics implements CurrenciesStatisticsContract
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getCryptoTurnover(): array
    {
        $stats = $this->calculateCryptoStats();

        return $this->render($stats);
    }

    /**
     * Render statistics data to valid format
     *
     * @param array $statistics
     *
     * @return array
     */
    protected function render(array $statistics): array
    {
        ['stats' => $stats, 'total' => $total] = $statistics;

        $render = [];
        foreach ($stats as $code => $turnover) {
            $percentage = $total ? round($turnover / $total * 100) : 0;
            $render[$code] = [
                'turnover'   => $turnover,
                'percentage' => $percentage,
            ];
        }

        return $render;
    }

    /**
     * Retrieve statistics for crypto turnover
     *
     * @return array
     */
    protected function calculateCryptoStats(): array
    {
        $stats = [];
        $total = 0;

        foreach ($this->getCryptoCurrenciesIdsList() as $cryptoCurrencyId) {
            /** @var CryptoCurrency $cryptoCurrency */
            $cryptoCurrency = CryptoCurrency::findOrFail($cryptoCurrencyId);
            $total += $stats[$cryptoCurrency->code] = $this->calculateCryptoTurnover($cryptoCurrency);
        }

        return [
            'stats' => $stats,
            'total' => $total,
        ];
    }

    /**
     * Retrieve crypto currencies from found deals
     *
     * @return array
     */
    protected function getCryptoCurrenciesIdsList(): array
    {
        return $this->getFromAdsByField('crypto_currency_id');
    }


    /**
     * Calculate turnover for deals by crypto currency
     *
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return float
     */
    protected function calculateCryptoTurnover(CryptoCurrency $cryptoCurrency): float
    {
        $turnover = $this->getRepoClone()
                         ->filterByCryptoCurrency($cryptoCurrency)
                         ->take()->sum('crypto_amount');

        return $turnover;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getFiatTurnover(): array
    {
        $stats = $this->calculateFiatStats();

        return $this->render($stats);
    }

    /**
     * Retrieve statistics for fiat turnover
     *
     * @return array
     */
    protected function calculateFiatStats(): array
    {
        $stats = [];
        $total = 0;

        foreach ($this->getFiatCurrenciesIdsList() as $currencyId) {
            /** @var Currency $currency */
            $currency = Currency::findOrFail($currencyId);
            $total += $stats[$currency->code] = $this->calculateFiatTurnover($currency);
        }

        return [
            'stats' => $stats,
            'total' => $total,
        ];
    }

    /**
     * Retrieve fiat currencies from found deals
     *
     * @return array
     */
    protected function getFiatCurrenciesIdsList(): array
    {
        return $this->getFromAdsByField('currency_id');
    }

    /**
     * Calculate turnover for deals by fiat currency
     *
     * @param Currency $currency
     *
     * @return float
     */
    protected function calculateFiatTurnover(Currency $currency): float
    {
        $turnover = $this->getRepoClone()
                         ->filterByCurrency($currency)
                         ->take()->sum('crypto_amount_in_usd');

        return $turnover;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getStatistics(): array
    {
        return [
            'fiat'   => $this->getFiatTurnover(),
            'crypto' => $this->getCryptoTurnover(),
        ];
    }
}
