<?php

namespace App\Services\Statistics\Deals;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Filters\DealMemberFilterContract;
use App\Contracts\Filters\DealRequestFilterContract;
use App\Contracts\Services\Statistics\Deals\CurrenciesStatisticsContract;
use App\Contracts\Services\Statistics\Deals\DealStatisticsResultContract;
use App\Contracts\Services\Statistics\Deals\OperationsStatisticsContract;
use App\Contracts\Services\Statistics\Deals\PaymentSystemsStatisticsContract;
use App\Http\Requests\Stats\DealStatsRequest;
use App\Models\User\User;

/**
 * Class DealStatisticsResult
 *
 * @package App\Services\Statistics\Deals
 */
class DealStatisticsResult implements DealRequestFilterContract, DealMemberFilterContract, DealStatisticsResultContract
{
    /**
     * @var User
     */
    protected $user;
    /**
     * @var CurrenciesStatistics
     */
    protected $currencies;
    /**
     * @var OperationsStatistics
     */
    protected $operations;
    /**
     * @var PaymentSystemsStatistics
     */
    protected $payments;
    /**
     * @var DealStatsRequest
     */
    protected $request;
    /**
     * @var
     */
    protected $statsOperations;

    /**
     * @var
     */
    protected $statsFiat;
    /**
     * @var
     */
    protected $statsCrypto;
    /**
     * @var
     */
    protected $statsPaymentSystems;

    /**
     * @var boolean
     */
    protected $built;

    /**
     * DealStatisticsResult constructor.
     *
     * @param CurrenciesStatisticsContract     $currencies
     * @param PaymentSystemsStatisticsContract $payments
     * @param OperationsStatisticsContract     $operations
     */
    public function __construct(
        CurrenciesStatisticsContract $currencies,
        PaymentSystemsStatisticsContract $payments,
        OperationsStatisticsContract $operations
    ) {
        $this->currencies = $currencies;
        $this->operations = $operations;
        $this->payments = $payments;
    }

    /**
     * prepare full statistics for current user
     *
     * @return self
     */
    protected function build()
    {
        $this->init();

        ['fiat' => $fiatCurrency, 'crypto' => $cryptoCurrency] = $this->currencies->getStatistics();

        $this->statsOperations = $this->operations->getStatistics();

        $this->statsFiat = $fiatCurrency;
        $this->statsCrypto = $cryptoCurrency;
        $this->statsPaymentSystems = $this->payments->getStatistics();

        $this->buildReady();

        return $this;
    }

    /**
     * Set filter where has request of fiters
     *
     * @param DealStatsRequest $request
     *
     * @return $this
     */
    public function request(DealStatsRequest $request)
    {
        $this->buildReset();

        $this->request = $request;

        return $this;
    }

    /**
     * Set filter where user was deal member
     *
     * @param User $user
     *
     * @return $this
     */
    public function member(User $user)
    {
        $this->buildReset();

        $this->user = $user;

        return $this;
    }

    /**
     * Prepare stats request
     */
    protected function init(): void
    {
        if (!$this->user) {
            $this->user = app(AuthenticatedContract::class);
        }

        $this->currencies->member($this->user);
        $this->payments->member($this->user);
        $this->operations->member($this->user);

        if ($this->request) {
            $this->currencies->filterFromRequest($this->request);
            $this->payments->filterFromRequest($this->request);
            $this->operations->filterFromRequest($this->request);
        }
    }

    /**
     * @return mixed
     */
    public function getStatsOperations()
    {
        $this->checkBuild();

        return $this->statsOperations;
    }

    /**
     * @return mixed
     */
    public function getStatsFiat()
    {
        $this->checkBuild();

        return $this->statsFiat;
    }

    /**
     * @return mixed
     */
    public function getStatsCrypto()
    {
        $this->checkBuild();

        return $this->statsCrypto;
    }

    /**
     * @return mixed
     */
    public function getStatsPaymentSystems()
    {
        $this->checkBuild();

        return $this->statsPaymentSystems;
    }

    /**
     * @return $this
     */
    protected function buildReset()
    {
        $this->built = false;

        return $this;
    }

    /**
     * @return $this
     */
    protected function buildReady()
    {
        $this->built = true;

        return $this;
    }

    /**
     * Check if build is made
     */
    protected function checkBuild(): void
    {
        if (!$this->built) {
            $this->build();
        }
    }
}
