<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-31
 * Time: 15:21
 */

namespace App\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\AverageFinishDealsTimeStatisticsContract;
use App\Contracts\Services\Statistics\Deals\FinishDealTimeStatisticsContract;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use App\Services\Traits\Userable;
use Carbon\Carbon;

/**
 * Class calculate average finish time for User's deals
 *
 * @package App\Services\Statistics\Deals
 */
class AverageFinishDealsTimeStatistics implements AverageFinishDealsTimeStatisticsContract
{
    use Userable;

    /**
     * @var DealRepo
     */
    private $dealRepo;

    /**
     * @var FinishDealTimeStatisticsContract
     */
    private $finishDealTimeStatistics;

    /**
     * AverageFinishDealsTimeStatistics constructor.
     *
     * @param DealRepo                         $dealRepo
     * @param FinishDealTimeStatisticsContract $finishDealTimeStatistics
     */
    public function __construct(DealRepo $dealRepo, FinishDealTimeStatisticsContract $finishDealTimeStatistics)
    {
        $this->dealRepo                 = $dealRepo;
        $this->finishDealTimeStatistics = $finishDealTimeStatistics;
    }

    /**
     * {@inheritdoc}
     *
     * @param User $user
     *
     * @return int
     */
    public function averageTimeInSeconds(User $user): int
    {
        $this->setUser($user);
        $count = $averageTime = 0;

        $this->getUserDeals()->chunk(50, function ($deals) use (&$count, &$averageTime) {
            foreach ($deals as $deal) {
                $averageTime += $this->finishDealTimeStatistics->timeInSeconds($deal);
                $count++;
            }
        });

        return $count
            ? (int)($averageTime/$count)
            : 0;
    }

    /**
     * Return finished deals where user was buyer
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    protected function getUserDeals()
    {
        return $this->dealRepo->filterBySeller($this->user)->filterByStatus(DealStatusConstants::FINISHED)->take();
    }

    /**
     * {@inheritdoc}
     *
     * @param User $user
     *
     * @return int
     */
    public function averageTimeInMinutes(User $user): int
    {
        return (int)floor($this->averageTimeInSeconds($user) / Carbon::SECONDS_PER_MINUTE);
    }

    /**
     * {@inheritdoc}
     *
     * @param User $user
     *
     * @return int
     */
    public function averageTimeInHours(User $user): int
    {
        return (int)floor($this->averageTimeInMinutes($user) / Carbon::MINUTES_PER_HOUR);
    }
}
