<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-28
 * Time: 16:49
 */

namespace App\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\FinishDealTimeStatisticsContract;
use App\Exceptions\Statistics\Deal\NonexistentDealStatusException;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Services\Traits\Dealable;
use Carbon\Carbon;

/**
 * Class FinishDealTimeStatistics calculate finish period for Deal
 *
 * @package App\Services\Statistics\Deals
 */
class FinishDealTimeStatistics implements FinishDealTimeStatisticsContract
{
    use Dealable;

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return int
     */
    public function timeInSeconds(Deal $deal): int
    {
        $this->setDeal($deal);

        if (false == $this->deal->isFinished()) {
            return 0;
        }

        try {
            $paidStatusTime   = $this->getStatusCreatedAt(DealStatusConstants::PAID);
            $finishStatusTime = $this->getStatusCreatedAt(DealStatusConstants::FINISHED);
            return $paidStatusTime->diffInSeconds($finishStatusTime);
        } catch (NonexistentDealStatusException $exception) {
            return 0;
        }
    }

    /**
     * Retrieve created at value for deal's status
     *
     * @param int $statusId
     *
     * @return Carbon
     * @throws NonexistentDealStatusException
     */
    protected function getStatusCreatedAt(int $statusId): Carbon
    {
        $status = $this->deal->statuses()->where('status_id', $statusId)
                                         ->withPivot('created_at')->first();

        if ($status instanceof DealStatus) {
            return $status->pivot->created_at;
        }

        throw new NonexistentDealStatusException(
            'DealStatus with ID='.$statusId.' is not exists for Deal with ID='.$this->deal->id.'!'
        );
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return int
     */
    public function timeInMinutes(Deal $deal): int
    {
        return (int)floor($this->timeInSeconds($deal) / Carbon::SECONDS_PER_MINUTE);
    }

    /**
     * {@inheritdoc}
     *
     * @param Deal $deal
     *
     * @return int
     */
    public function timeInHours(Deal $deal): int
    {
        return (int)floor($this->timeInMinutes($deal) / Carbon::MINUTES_PER_HOUR);
    }
}
