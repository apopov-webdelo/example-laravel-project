<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 11:20
 */

namespace App\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\OperationsStatisticsContract;
use App\Contracts\Services\Statistics\StatisticsContract;

class OperationsStatistics extends DealStatistics implements OperationsStatisticsContract
{
    /**
     * Quantity deals for sale
     *
     * @var int|null
     */
    protected $sale;

    /**
     * Quantity deals for buy
     *
     * @var int|null
     */
    protected $buy;

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getSale(): int
    {
        if (!isset($this->sale)) {
            $this->sale = $this->getRepoClone()->filterForSale($this->user)->take()->count();
        }

        return $this->sale;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getSalePercentage(): int
    {
        return $this->getTotal()
            ? (int)round($this->getSale() / $this->getTotal() * 100)
            : 0;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getBuy(): int
    {
        if (!isset($this->buy)) {
            $this->buy = $this->getRepoClone()->filterForBuy($this->user)->take()->count();
        }

        return $this->buy;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getBuyPercentage(): int
    {
        return $this->getTotal()
            ? (int)round($this->getBuy() / $this->getTotal() * 100)
            : 0;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getTotal(): int
    {
        return $this->getSale() + $this->getBuy();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getStatistics(): array
    {
        $stats = [
            'sale' => $this->getSale(),
            'buy'  => $this->getBuy(),
        ];

        return $this->render($stats);
    }

    /**
     * Render statistics data to valid format
     *
     * @param array $statistics
     *
     * @return array
     */
    protected function render(array $statistics): array
    {
        $total = $this->getTotal();

        $render = [];
        foreach ($statistics as $operation => $turnover) {
            $percentage = $total ? round($turnover / $total * 100) : 0;
            $render[$operation] = [
                'count'   => $turnover,
                'percentage' => $percentage,
            ];
        }

        return $render;
    }

    /**
     * {@inheritdoc}
     *
     * @return StatisticsContract
     */
    public function reset(): StatisticsContract
    {
        $this->buy = $this->sale = null;

        return parent::reset();
    }
}
