<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/6/18
 * Time: 12:16
 */

namespace App\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\PaymentSystemsStatisticsContract;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Deal\DealRepo;
use App\Repositories\Directory\CurrencyRepo;

class PaymentSystemsStatistics extends DealStatistics implements PaymentSystemsStatisticsContract
{
    /**
     * @var Currency
     */
    private $currencyUsd;

    public function __construct(DealRepo $dealRepo, CurrencyRepo $currencyRepo)
    {
        parent::__construct($dealRepo);

        $this->currencyUsd = $currencyRepo->take()
                                          ->where('code', 'USD')
                                          ->get()
                                          ->first();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getStatistics(): array
    {
        $stats = $this->getPaymentSystemsPercentages();

        return $this->render($stats);
    }

    /**
     * Render statistics data to valid format
     *
     * @param array $statistics
     *
     * @return array
     */
    protected function render(array $statistics): array
    {
        $total = array_reduce($statistics, function ($acc, $turnover) {
            return $acc + $turnover;
        });

        $render = [];
        foreach ($statistics as $paySystemId => $turnover) {
            $percentage = $total ? round($turnover / $total * 100) : 0;
            $render[$paySystemId] = [
                'turnover'   => $turnover,
                'percentage' => $percentage,
            ];
        }

        return $render;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getPaymentSystemsPercentages(): array
    {
        $stats = [];
        foreach ($this->getPaymentsSystemsList() as $paymentSystemId) {
            $stats[$paymentSystemId] = $this->getPaymentSystemTurnover($paymentSystemId);
        }

        return $stats;
    }

    /**
     * Retrieve exists payment systems list according filters
     *
     * @return int[]
     */
    public function getPaymentsSystemsList(): array
    {
        return $this->getFromAdsByField('payment_system_id');
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function getPaymentSystemTurnover($paymentSystemId): int
    {
        return $this->getTurnover($paymentSystemId);
    }

    /**
     * @param PaymentSystem|int|null $paymentSystem
     *
     * @return float
     */
    public function getTurnover($paymentSystem = null): float
    {
        $repo = $this->getRepoClone();
        if ($paymentSystem) {
            $repo->filterByPaymentSystem($paymentSystem);
        }

        $turnover = $repo->take()->sum('crypto_amount_in_usd');

        return $turnover;
    }
}
