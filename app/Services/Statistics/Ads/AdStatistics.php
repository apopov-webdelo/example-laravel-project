<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/28/18
 * Time: 15:35
 */

namespace App\Services\Statistics\Ads;

use App\Contracts\Statistics\Ads\AdStatisticsContract;
use App\Models\Ad\Ad;
use App\Models\Deal\DealStatusConstants;
use App\Repositories\Deal\DealRepo;
use Carbon\Carbon;

/**
 * Service for Ad statistics calculation
 *
 * @package App\Services\Statistics
 */
class AdStatistics implements AdStatisticsContract
{
    /**
     * @var DealRepo
     */
    protected $dealRepo;

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * @var Carbon
     */
    protected $from;

    /**
     * @var Carbon
     */
    protected $to;

    /**
     * @param Ad          $ad
     * @param Carbon|null $from
     * @param Carbon|null $to
     */
    public function __construct(Ad $ad, Carbon $from = null, Carbon $to = null)
    {
        $this->dealRepo = app(DealRepo::class);
        $this->setData($ad, $from, $to);
    }

    /**
     * Calculate deals quantity placed by that Ad according period
     *
     * @return int
     */
    public function deals(): int
    {
        return $this->dealRepo->take()->count();
    }

    /**
     * Set base params
     *
     * @param Ad          $ad
     * @param Carbon|null $from
     * @param Carbon|null $to
     *
     * @return $this
     */
    protected function setData(Ad $ad, Carbon $from = null, Carbon $to = null)
    {
        $this->ad = $ad;
        $this->from = $from;
        $this->to = $to;

        return $this->filterRepo();
    }

    /**
     * Filter repo by base params
     *
     * @return $this
     */
    protected function filterRepo()
    {
        $this->dealRepo->filterByAd($this->ad)
                       ->filterByStatusId(DealStatusConstants::FINISHED);

        if ($this->from) {
            $this->dealRepo->filterByCreatedAtFrom($this->from);
        }

        if ($this->to) {
            $this->dealRepo->filterByCreatedAtTo($this->to);
        }

        return $this;
    }

    /**
     * Calculate total crypto turnover by that Ad according period
     *
     * @return int
     */
    public function cryptoTurnover(): int
    {
        return $this->dealRepo->take()->sum('crypto_amount');
    }

    /**
     * Calculate total fiat turnover by that Ad according period
     *
     * @return int
     */
    public function fiatTurnover(): int
    {
        return $this->dealRepo->take()->sum('fiat_amount');
    }

    /**
     * Calculate weighted average price by that Ad according period
     * example: (300к + 100к) / (1 + 0.8) = 400000/1.8 = 222,222р.
     *
     * @return int
     */
    public function weightedAveragePrice(): int
    {
        if ($this->deals() === 0) {
            return 0;
        }

        $result = currencyFromCoins($this->fiatTurnover(), $this->ad->currency)
            / currencyFromCoins($this->cryptoTurnover(), $this->ad->cryptoCurrency);

        return (int)$result;
    }

    /**
     * Calculate average value for field
     *
     * @param string $field
     *
     * @return mixed
     */
    protected function getAverage(string $field)
    {
        return $this->dealRepo->take()->average($field);
    }

    /**
     * @return Ad
     */
    public function getAd()
    {
        return $this->ad;
    }
}
