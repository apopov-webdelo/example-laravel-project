<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/18/18
 * Time: 15:25
 */

namespace App\Services;

use App\Services\Traits\Storable;

abstract class StoreService extends BaseService
{
    use Storable;
}
