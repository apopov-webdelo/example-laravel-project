<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 12.11.2018
 * Time: 13:37
 */

namespace App\Services;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateService
 *
 * @package App\Services\User
 */
abstract class UpdateService
{
    /**
     * @var Model $model
     */
    protected $model;

    /**
     * UpdateService constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function update(array $data) : Model
    {
        $this->modelUpdate($data)
             ->fireEvent();

        return $this->model;
    }

    /**
     * @param array $data
     *
     * @return UpdateService
     */
    protected function modelUpdate(array $data) : self
    {
        $this->model->fill($data)->save();
        return $this;
    }

    /**
     * @return mixed
     */
    abstract protected function fireEvent();
}
