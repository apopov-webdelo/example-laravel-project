<?php

namespace App\Events\Balance;

use App\Contracts\AuthenticatedContract;
use App\Models\User\User;
use App\Repositories\Balance\BalanceRepo;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class TotalFiatAmountChanged
 *
 * @package App\Events\Balance
 */
class TotalFiatAmountChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * TotalAmountChanged constructor.
     *
     * @param User|AuthenticatedContract $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('balance.'. $this->user->id);
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => [
                'total_amount' => app(BalanceRepo::class)->getTotalAmountInFiat($this->user, 2),
            ],
        ];
    }
}
