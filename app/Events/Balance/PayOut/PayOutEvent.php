<?php

namespace App\Events\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutContract;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class PayOutEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var PayOutContract
     */
    public $payOut;

    /**
     * PayOutEvent constructor.
     *
     * @param PayOutContract $payOut
     */
    public function __construct(PayOutContract $payOut)
    {
        $this->payOut = $payOut;
    }
}
