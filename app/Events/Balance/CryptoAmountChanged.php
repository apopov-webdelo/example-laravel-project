<?php

namespace App\Events\Balance;

use App\Contracts\AuthenticatedContract;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Http\Resources\Client\Balance\BalanceCollection;
use App\Repositories\Balance\BalanceRepo;
use Illuminate\Support\Facades\Log;

/**
 * Class CryptoAmountChanged
 *
 * @package App\Events\Balance
 */
class CryptoAmountChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * CryptoAmountChanged constructor.
     *
     * @param AuthenticatedContract|User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('balance.'. $this->user->id);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function broadcastWith()
    {
        try {
            /** @var BalanceRepo $repo */
            $repo = app(BalanceRepo::class)->filterByUser($this->user);
            return app(BalanceCollection::class, ['resource' => $repo->take()->paginate()])->toArray([]);
        } catch (\Exception $e) {
            Log::error(
                'Unexpected error during BalanceCollection generating!',
                ['message' => $e->getMessage()]
            );
            throw $e;
        }
    }
}
