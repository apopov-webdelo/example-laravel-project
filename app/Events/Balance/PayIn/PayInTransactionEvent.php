<?php

namespace App\Events\Balance\PayIn;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class PayInTransactionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var PayInTransactionContract
     */
    public $payInTransaction;

    /**
     * PayInTransactionEvent constructor.
     *
     * @param PayInTransactionContract $payInTransaction
     */
    public function __construct(PayInTransactionContract $payInTransaction)
    {
        $this->payInTransaction = $payInTransaction;
    }
}
