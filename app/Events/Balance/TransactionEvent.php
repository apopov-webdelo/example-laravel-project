<?php

namespace App\Events\Balance;

use App\Models\Balance\Transaction;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class TransactionEvent
 *
 * @package App\Events\Balance
 */
abstract class TransactionEvent
{
    use Dispatchable, SerializesModels;

    /**
     * @var Transaction
     */
    public $transaction;

    /**
     * TransactionExecuted constructor.
     *
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }
}
