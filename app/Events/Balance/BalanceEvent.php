<?php

namespace App\Events\Balance;

use App\Http\Resources\Client\Balance\BalanceResource;
use App\Models\Balance\Balance;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class BalanceReplenished
 *
 * @package App\Events\Balance
 */
abstract class BalanceEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Balance
     */
    public $balance;

    /**
     * BalanceReplenished constructor.
     *
     * @param Balance $balance
     */
    public function __construct(Balance $balance)
    {
        $this->balance = $balance;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('balance.'. $this->balance->user->id),
        ];
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => app(BalanceResource::class, ['resource' => $this->balance])->toArray([])
        ];
    }

}
