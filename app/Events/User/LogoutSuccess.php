<?php

namespace App\Events\User;

use App\Contracts\Utils\Session\SessionStorageContract;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LogoutSuccess
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var SessionStorageContract
     */
    public $sessionStorage;

    /**
     * LogoutSuccess constructor.
     *
     * @param Authenticatable        $user
     * @param SessionStorageContract $sessionStorage
     */
    public function __construct(Authenticatable $user, SessionStorageContract $sessionStorage)
    {
        $this->user = $user;
        $this->sessionStorage = $sessionStorage;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
