<?php

namespace App\Events\User;

use App\Contracts\Utils\Session\SessionStorageContract;
use App\Models\User\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Queue\SerializesModels;

class SecurityLogEvent
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var SessionStorageContract
     */
    public $sessionStorage;

    /**
     * @var array
     */
    public $securityInfo;

    /**
     * SecurityLogEvent constructor.
     *
     * @param Authenticatable        $user
     * @param SessionStorageContract $sessionStorage
     * @param array                  $securityInfo
     */
    public function __construct(Authenticatable $user, SessionStorageContract $sessionStorage, array $securityInfo)
    {
        $this->user = $user;
        $this->sessionStorage = $sessionStorage;
        $this->securityInfo = $securityInfo;
    }
}
