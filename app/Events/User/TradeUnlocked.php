<?php

namespace App\Events\User;

use App\Contracts\LockableContract;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TradeUnlocked implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * LoginError constructor.
     *
     * @param LockableContract $user
     */
    public function __construct(LockableContract $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('users.'. $this->user->id),
        ];
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => [
                'id'         => $this->user->id,
                'login'      => $this->user->login
            ]
        ];
    }
}
