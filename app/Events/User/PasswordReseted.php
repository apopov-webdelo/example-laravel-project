<?php

namespace App\Events\User;

use App\Contracts\AuthenticatedContract;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class PasswordReseted
 * @package App\Events\User
 */
class PasswordReseted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var AuthenticatedContract
     */
    public $user;
    /**
     * @var string
     */
    public $reason;
    /**
     * @var string
     */
    public $password;

    /**
     * PasswordReseted constructor.
     * @param AuthenticatedContract $user
     * @param string $reason
     * @param string $password
     */
    public function __construct(AuthenticatedContract $user, string $reason, string $password)
    {
        $this->user = $user;
        $this->reason = $reason;
        $this->password = $password;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
