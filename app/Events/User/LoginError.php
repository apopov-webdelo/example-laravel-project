<?php

namespace App\Events\User;

use App\Contracts\Utils\Session\SessionStorageContract;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LoginError
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var SessionStorageContract
     */
    public $sessionStorage;

    /**
     * LoginError constructor.
     *
     * @param Authenticatable        $user
     * @param SessionStorageContract $sessionStorage
     */
    public function __construct(Authenticatable $user, SessionStorageContract $sessionStorage)
    {
        $this->user = $user;
        $this->sessionStorage = $sessionStorage;
    }
}
