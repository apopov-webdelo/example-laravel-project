<?php

namespace App\Events\Blockchain;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class StellarLogEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $type;
    public $method;
    public $route;
    public $response_code;
    public $request;
    public $response;

    /**
     * Create a new event instance.
     *
     * @param $type
     * @param $method
     * @param $route
     * @param $response_code
     * @param $request
     * @param $response
     */
    public function __construct($type, $method, $route, $response_code, $request, $response)
    {
        $this->type = $type;
        $this->method = $method;
        $this->route = $route;
        $this->response_code = $response_code;
        $this->request = $request;
        $this->response = $response;
    }
}
