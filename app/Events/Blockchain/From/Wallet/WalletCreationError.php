<?php

namespace App\Events\Blockchain\From\Wallet;

/**
 * Class WalletCreationError
 *
 * @package App\Events\Blockchain\From\Wallet
 */
class WalletCreationError extends WalletEvent
{

}
