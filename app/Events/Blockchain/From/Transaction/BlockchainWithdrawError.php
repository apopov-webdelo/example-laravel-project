<?php

namespace App\Events\Blockchain\From\Transaction;

use App\Contracts\Balance\PayOut\PayOutContract;

class BlockchainWithdrawError
{
    /**
     * @var PayOutContract
     */
    public $payOut;

    /**
     * BlockchainWithdrawError constructor.
     *
     * @param PayOutContract $payOut
     */
    public function __construct(PayOutContract $payOut)
    {
        $this->payOut = $payOut;
    }
}
