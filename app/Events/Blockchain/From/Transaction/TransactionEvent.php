<?php

namespace App\Events\Blockchain\From\Transaction;

use App\Models\User\User;

/**
 * Class TransactionEvent
 *
 * @package App\Events\Blockchain\From\Transaction
 */
abstract class TransactionEvent
{
    /**
     * @var User
     */
    public $user;

    /**
     * TransactionEvent constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = ($user);
    }
}
