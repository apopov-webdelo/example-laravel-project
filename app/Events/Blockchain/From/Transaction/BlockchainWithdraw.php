<?php

namespace App\Events\Blockchain\From\Transaction;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\Traits\Amount;
use App\Services\Blockchain\Stellar\Traits\BalanceAmount;
use App\Services\Blockchain\Stellar\Traits\Commission;
use App\Services\Blockchain\Stellar\Traits\PayOut;
use App\Services\Blockchain\Stellar\Traits\TransactionId;
use App\Services\Traits\CryptoCurrency;
use App\Services\Traits\Userable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BlockchainWithdraw
{
    use Dispatchable, InteractsWithSockets, SerializesModels,
        Userable, Amount, CryptoCurrency, BalanceAmount, TransactionId, Commission, PayOut;

    /**
     * @var int
     */
    public $realAmount;

    /**
     * BlockchainWithdraw constructor.
     *
     * @param User                   $user
     * @param int                    $amount
     * @param int                    $commission
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param int                    $balanceAmount
     * @param int                    $realAmount
     * @param string                 $transactionId
     * @param int                    $payOutId
     */
    public function __construct(
        User $user,
        int $amount,
        int $commission,
        CryptoCurrencyContract $cryptoCurrency,
        int $balanceAmount,
        int $realAmount,
        string $transactionId,
        int $payOutId
    ) {
        $this->setUser($user)
             ->setAmount($amount)
             ->setCommission($commission)
             ->setCryptoCurrency($cryptoCurrency)
             ->setTransactionId($transactionId)
             ->setBalanceAmount($balanceAmount)
             ->setPayOutId($payOutId);
        $this->realAmount = $realAmount;
    }

    /**
     * Retrieve real amount value
     *
     * @return int
     */
    public function getRealAmount(): int
    {
        return $this->realAmount;
    }
}
