<?php

namespace App\Events\Blockchain\From\Deal;

use App\Models\Deal\Deal;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Base event class DealEvent
 *
 * @package App\Events\Blockchain\From\Deal
 */
abstract class DealEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Deal
     */
    protected $deal;

    /**
     * DealCanceled constructor.
     *
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }
}
