<?php

namespace App\Events\Blockchain;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StartCompareBalanceWithStellar
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var CryptoCurrencyContract
     */
    public $crypto;

    /**
     * Create a new event instance.
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $crypto
     */
    public function __construct(User $user, CryptoCurrencyContract $crypto)
    {
        $this->user = $user;
        $this->crypto = $crypto;
    }
}
