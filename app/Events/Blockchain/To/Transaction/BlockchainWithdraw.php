<?php

namespace App\Events\Blockchain\To\Transaction;

use App\Contracts\Balance\PayOut\PayOutContract;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BlockchainWithdraw
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var PayOutContract
     */
    public $payOut;

    /**
     * BlockchainWithdraw constructor.
     *
     * @param PayOutContract $payOut
     */
    public function __construct(PayOutContract $payOut)
    {
        $this->payOut = $payOut;
    }
}
