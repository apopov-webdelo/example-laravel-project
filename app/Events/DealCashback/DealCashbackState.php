<?php

namespace App\Events\DealCashback;

use App\Contracts\DealCashback\DealCashbackContract;
use App\Models\DealCashback\DealCashback;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

abstract class DealCashbackState
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var DealCashbackContract|DealCashback
     */
    public $cashback;

    /**
     * TransactionEvent constructor.
     *
     * @param DealCashbackContract $cashback
     */
    public function __construct(DealCashbackContract $cashback)
    {
        $this->cashback = $cashback;
    }
}
