<?php

namespace App\Events\DealCashback;

use App\Contracts\DealCashback\DealCashbackContract;

class DealCashbackCancelState extends DealCashbackState
{
    /**
     * @var string
     */
    public $error;

    /**
     * @var string
     */
    public $reason;

    public function __construct(DealCashbackContract $cashback, string $error, string $reason)
    {
        parent::__construct($cashback);
        $this->error = $error;
        $this->reason = $reason;
    }
}
