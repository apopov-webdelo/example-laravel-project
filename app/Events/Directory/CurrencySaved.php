<?php

namespace App\Events\Directory;

use App\Models\Directory\Currency;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CurrencySaved
{
    use Dispatchable, SerializesModels;

    /**
     * @var Currency
     */
    public $currency;

    /**
     * Create a new event instance.
     *
     * @param Currency $currency
     */
    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }
}
