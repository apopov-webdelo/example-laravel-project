<?php

namespace App\Events\Directory;

use App\Models\Directory\CryptoCurrency;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CryptoCurrencySaved
{
    use Dispatchable, SerializesModels;

    /**
     * @var CryptoCurrency
     */
    public $crypto;

    /**
     * Create a new event instance.
     *
     * @param CryptoCurrency $crypto
     */
    public function __construct(CryptoCurrency $crypto)
    {
        $this->crypto = $crypto;
    }
}
