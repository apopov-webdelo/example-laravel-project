<?php

namespace App\Events\Directory;

use App\Models\Directory\Bank;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BankSaved
{
    use Dispatchable, SerializesModels;

    /**
     * @var Bank
     */
    public $bank;

    /**
     * Create a new event instance.
     *
     * @param Bank $bank
     */
    public function __construct(Bank $bank)
    {
        $this->bank = $bank;
    }
}
