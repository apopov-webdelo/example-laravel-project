<?php

namespace App\Events\Directory;

use App\Models\Directory\PaymentSystem;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentSystemSaved
{
    use Dispatchable, SerializesModels;

    /**
     * @var PaymentSystem
     */
    public $paymentSystem;

    /**
     * Create a new event instance.
     *
     * @param PaymentSystem $paymentSystem
     */
    public function __construct(PaymentSystem $paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;
    }
}
