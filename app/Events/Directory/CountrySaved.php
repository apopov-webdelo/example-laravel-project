<?php

namespace App\Events\Directory;

use App\Models\Directory\Country;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CountrySaved
{
    use Dispatchable, SerializesModels;

    /**
     * @var Country
     */
    public $country;

    /**
     * Create a new event instance.
     *
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        $this->country = $country;
    }
}
