<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/14/18
 * Time: 14:04
 */

namespace App\Events\Chat;

use App\Models\Chat\Chat;
use Illuminate\Broadcasting\PrivateChannel;

/**
 * Generate array with private channels for all chat recipients
 *
 * @package App\Events\Chat
 */
trait ChatRecipientChannels
{
    /**
     * Return channels for chat recipients
     *
     * @param Chat $chat
     * @return array
     */
    protected function getChannelsForChatMembers(Chat $chat) : array
    {
        $channels = [];
        foreach ($chat->recipients as $recipient) {
            $channels[] = new PrivateChannel('users.'.$recipient->id);
        }
        return $channels;
    }
}
