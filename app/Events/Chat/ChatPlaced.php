<?php

namespace App\Events\Chat;

class ChatPlaced extends ChatEvent
{
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return $this->getChannelsForChatMembers($this->chat);
    }
}
