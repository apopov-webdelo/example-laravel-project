<?php

namespace App\Events\Chat;

use App\Http\Resources\Client\Chat\ChatResource;
use App\Models\Chat\Chat;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class ChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ChatRecipientChannels;

    /**
     * @var Chat
     */
    public $chat;

    /**
     * @param Chat $chat
     */
    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $channels = $this->getChannelsForChatMembers($this->chat);
        array_push(
            $channels,
            new PrivateChannel('chats.'.$this->chat->id)
        );
        return $channels;
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => app(ChatResource::class, ['resource' => $this->chat ])->toArray([])
        ];
    }
}
