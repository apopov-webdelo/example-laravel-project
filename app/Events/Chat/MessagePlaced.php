<?php
namespace App\Events\Chat;

use App\Http\Resources\Client\Message\MessageResource;
use App\Models\Chat\Message;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessagePlaced implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ChatRecipientChannels;

    /**
     * @var Message
     */
    public $message;

    /**
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $channels = $this->getChannelsForChatMembers($this->message->chat);
        array_push(
            $channels,
            new PrivateChannel('chat.'.$this->message->chat_id)
        );
        return $channels;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => app(MessageResource::class, ['resource' => $this->message ])->toArray([])
        ];
    }
}
