<?php

namespace App\Events\Partnership;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class PartnershipEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
}
