<?php

namespace App\Events\Partnership;

use App\Contracts\Partnership\PartnershipEntityContract;

class EntityProcessed extends PartnershipEvent
{
    /**
     * @var PartnershipEntityContract
     */
    public $entity;

    /**
     * DealProcessed constructor.
     *
     * @param PartnershipEntityContract $entity
     */
    public function __construct(PartnershipEntityContract $entity)
    {
        $this->entity = $entity;
    }
}
