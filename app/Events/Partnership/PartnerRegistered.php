<?php

namespace App\Events\Partnership;

use App\Contracts\Partnership\PartnerContract;

class PartnerRegistered extends PartnershipEvent
{
    /**
     * @var PartnerContract
     */
    public $partner;

    /**
     * PartnerAwarded constructor.
     *
     * @param PartnerContract $partner
     */
    public function __construct(PartnerContract $partner)
    {
        $this->partner = $partner;
    }
}
