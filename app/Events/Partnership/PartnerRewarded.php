<?php

namespace App\Events\Partnership;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;

class PartnerRewarded extends PartnershipEvent
{
    /**
     * @var PartnershipCommissionStorageContract $commissionStorage
     */
    public $commissionStorage;

    /**
     * PartnerRewarded constructor.
     *
     * @param PartnershipCommissionStorageContract $commissionStorage
     */
    public function __construct(PartnershipCommissionStorageContract $commissionStorage)
    {
        $this->commissionStorage = $commissionStorage;
    }
}
