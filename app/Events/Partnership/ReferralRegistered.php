<?php

namespace App\Events\Partnership;

use App\Contracts\Partnership\ReferralContract;

class ReferralRegistered extends PartnershipEvent
{
    /**
     * @var ReferralContract
     */
    public $referral;

    /**
     * ReferralRegistered constructor.
     *
     * @param ReferralContract $referral
     */
    public function __construct(ReferralContract $referral)
    {
        $this->referral = $referral;
    }
}
