<?php

namespace App\Events\Ad;

use App\Models\Ad\Ad;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class AdEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Ad
     */
    public $ad;

    /**
     * Create a new event instance.
     *
     * @param Ad $ad
     * @return void
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new Channel('market.ads'),
            new Channel('market.ads.'.$this->ad->id),
            new PrivateChannel('ads'),
            new PrivateChannel('ads.'.$this->ad->id),
            new PrivateChannel('users.'. $this->ad->author->id),
        ];
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => [
                'id'         => $this->ad->id,
                'price'      => currencyFromCoins($this->ad->price, $this->ad->currency),
                'is_sale'    => $this->ad->is_sale,
                'is_deleted' => (bool)$this->ad->deleted_at,
                'is_active'  => $this->ad->is_active,
            ]
        ];
    }
}
