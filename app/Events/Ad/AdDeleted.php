<?php

namespace App\Events\Ad;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;

class AdDeleted extends AdEvent
{
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new Channel('market.ads.'.$this->ad->id),
            new PrivateChannel('ads.'.$this->ad->id),
            new PrivateChannel('users.'. $this->ad->author->id),
        ];
    }
}
