<?php

namespace App\Events\Ad;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;

class AdPlaced extends AdEvent
{
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new Channel('market.ads'),
            new PrivateChannel('ads'),
            new PrivateChannel('users.'. $this->ad->author),
        ];
    }
}
