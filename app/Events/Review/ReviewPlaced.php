<?php

namespace App\Events\Review;

use App\Http\Resources\Client\User\Review\ReviewResource;
use App\Models\User\Review;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReviewPlaced
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Review
     */
    public $review;

    /**
     * ReviewPlaced constructor.
     *
     * @param Review $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('users.'.$this->review->recipient->id),
            new PrivateChannel('users.'.$this->review->author->id),
        ];
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => app(ReviewResource::class, ['resource' => $this->review])->toArray([])
        ];
    }
}
