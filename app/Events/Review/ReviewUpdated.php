<?php

namespace App\Events\Review;

use App\Http\Resources\Client\User\Review\ReviewResource;
use App\Models\User\Review;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReviewUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Review
     */
    protected $review;

    /**
     * @param Review $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('reviews.'.$this->review->id.''); //TODO: confirm finish channels
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => app(ReviewResource::class, ['resource' => $this->review])->toArray([])
        ];
    }
}
