<?php

namespace App\Events\Deal;

use App\Http\Resources\Client\Market\DealResource;
use App\Models\Deal\Deal;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class DealPlaced
 * @package App\Events\Deal
 */
class DealPlaced extends DealEvent
{

}
