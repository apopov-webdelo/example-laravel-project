<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/14/18
 * Time: 13:46
 */

namespace App\Events\Deal;

use App\Models\Deal\Deal;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class DealEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Deal
     */
    public $deal;

    /**
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('deals.'.$this->deal->id),
            new PrivateChannel('users.'.$this->deal->author->id),
            new PrivateChannel('users.'.$this->deal->ad->author->id),
        ];
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'data' => [
                'id'        => $this->deal->id,
                'status'    => $this->deal->status->title,
                'status_id' => $this->deal->status->id,
            ],
        ];
    }
}