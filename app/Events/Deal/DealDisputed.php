<?php

namespace App\Events\Deal;

use App\Models\Deal\Deal;
use App\Models\User\User;

class DealDisputed extends DealEvent
{
    /**
     * @var User $user
     */
    public $user;

    /**
     * DealDisputed constructor.
     *
     * @param Deal $deal
     * @param User $user
     */
    public function __construct(Deal $deal, User $user)
    {
        parent::__construct($deal);
        $this->user = $user;
    }
}
