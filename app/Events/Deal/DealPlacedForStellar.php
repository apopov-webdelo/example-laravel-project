<?php

namespace App\Events\Deal;

use App\Models\Deal\Deal;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class DealPlacedForStellar
 *
 * @package App\Events\Deal
 */
class DealPlacedForStellar
{
    use Dispatchable, SerializesModels;

    /**
     * @var Deal
     */
    public $deal;

    /**
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }
}
