<?php

namespace App\Events\Employee;

use App\Models\Admin\Admin;
use App\Models\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class EmployeeRegistered
 * @package App\Events\User
 */
class EmployeeRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $admin;
    /**
     * @var string
     */
    public $password;

    /**
     * EmployeeRegistered constructor.
     *
     * @param Admin  $admin
     * @param string $password
     */
    public function __construct(Admin $admin, string $password)
    {
        $this->admin = $admin;
        $this->password = $password;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
