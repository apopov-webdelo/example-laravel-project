<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;

/**
 * Global base for all controllers
 *
 * Class BaseController
 *
 * @package App\Http\Controllers\Base
 */
abstract class BaseController extends Controller
{
}
