<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AuthenticatedContract;
use Illuminate\Http\JsonResponse;

/**
 * @resource Profile
 */
class ProfileController extends AdminBaseController
{
    /**
     * Get profile
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function details(AuthenticatedContract $user)
    {
        return new JsonResponse(['data'=>$user]);
    }
}
