<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Directory\CurrencyStoreRequest;
use App\Http\Requests\Admin\Directory\CurrencyUpdateRequest;
use App\Http\Requests\Client\Directory\CurrencyFilterRequest;
use App\Http\Resources\Admin\Currency\CurrencyCollection;
use App\Http\Resources\Admin\Currency\CurrencyResource;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Directory\CurrencyRepo;
use Illuminate\Http\JsonResponse;

/**
 * @resource Currency
 */
class CurrencyController extends AdminBaseController
{
    /**
     * Get currencies list
     *
     * @response {
     *   currency: [ :: Currency ]
     * }
     */
    public function index(CurrencyFilterRequest $request, CurrencyRepo $repo)
    {
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->filter && !$request->exclude) {
            $repo->search($request->filter);
        }
        if ($request->filter && $request->exclude) {
            $repo->search($request->filter, explode(',', $request->exclude));
        }
        if ($request->exclude) {
            $repo->exclude(explode(',', $request->exclude));
        }

        return app(CurrencyCollection::class, ['resource'=>$repo->take()->paginate($request->per_page)]);
    }

    /**
     * Get one
     *
     * @param Currency $currency
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(Currency $currency)
    {
        return app(CurrencyResource::class, ['resource'=>$currency]);
    }

    /**
     * Create one
     *
     * @param CurrencyStoreRequest $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function store(CurrencyStoreRequest $request)
    {
        return app(CurrencyResource::class, ['resource'=>Currency::create($request->all())]);
    }

    /**
     * Update one
     *
     * @param CurrencyUpdateRequest $request
     * @param Currency              $currency
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function update(CurrencyUpdateRequest $request, Currency $currency)
    {
        $currency->fill($request->all())->save();

        return app(CurrencyResource::class, ['resource'=>$currency]);
    }

    /**
     * Delete one
     *
     * @param Currency $currency
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(Currency $currency)
    {
        $currency->delete();

        return response()->json([]);
    }
}
