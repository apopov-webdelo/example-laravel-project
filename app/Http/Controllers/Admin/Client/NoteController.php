<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 01.11.2018
 * Time: 17:00
 */

namespace App\Http\Controllers\Admin\Client;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Resources\Admin\Note\NoteCollection;
use App\Models\User\User;

/**
 * Class NoteController
 * @package App\Http\Controllers\Admin
 *
 * @resource Client
 */
class NoteController extends AdminBaseController
{
    /**
     * Get client notes in list
     *
     * @param User $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function in(User $client)
    {
        return app(NoteCollection::class, ['resource' => $client->notesIncome()->paginate(25)]);
    }

    /**
     * Get client notes out list
     *
     * @param User $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function out(User $client)
    {
        return app(NoteCollection::class, ['resource' => $client->notesOutcome()->paginate(25)]);
    }
}
