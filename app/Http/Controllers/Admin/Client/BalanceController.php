<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 18:17
 */

namespace App\Http\Controllers\Admin\Client;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Admin\Client\UpdateBalanceCommissionPercentRequest;
use App\Http\Resources\Admin\Balance\BalanceCollection;
use App\Http\Resources\Admin\Balance\BalanceResource;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\Balance\Balance;
use App\Models\Directory\CommissionConstants;
use App\Models\User\User;
use App\Repositories\Balance\BalanceRepo;

/**
 * Class BalanceController
 * @package App\Http\Controllers\Admin
 *
 * @resource Client
 */
class BalanceController extends AdminBaseController
{
    /**
     * Get balance list
     *
     * @param User        $client
     * @param BalanceRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(User $client, BalanceRepo $repo)
    {
        return app(BalanceCollection::class, ['resource' => $repo->filterByUser($client)->take()->paginate(25)]);
    }

    /**
     * Get one
     *
     * @param Balance $balance
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(Balance $balance)
    {
        return app(BalanceResource::class, ['resource' => $balance]);
    }

    /**
     * Update users commission percent for exact balance
     *
     * @param UpdateBalanceCommissionPercentRequest $request
     * @param Balance                               $balance
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function updateCommissionPercent(UpdateBalanceCommissionPercentRequest $request, Balance $balance)
    {
        $oldCommission = $balance->accuracyCommission;

        $data = $request->validated();
        if (isset($data['commission'])) {
            $commission = str_replace(',', '.', $data['commission']);
            $data['commission'] = $commission * CommissionConstants::ONE_PERCENT;
        }

        if ($balance->fill($data)->save()) {
            activity_admin(
                "Balance #{$balance->id}|{$balance->cryptoCurrency->code} commission percent has been 
                changed from {$oldCommission}% to {$balance->accuracyCommission}% for user {$balance->user->login}",
                ActivityLogTypeConstants::CLIENTS
            );
        }

        return app(BalanceResource::class, ['resource' => $balance]);
    }
}
