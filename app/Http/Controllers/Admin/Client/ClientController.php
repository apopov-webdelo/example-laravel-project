<?php

namespace App\Http\Controllers\Admin\Client;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\User\LockServiceContract;
use App\Contracts\Services\User\LockTradeServiceContract;
use App\Contracts\Services\User\Statistics\UpdateReviewsCoefficientStorageContract;
use App\Contracts\Services\User\UnlockServiceContract;
use App\Contracts\Services\User\UnlockTradeServiceContract;
use App\Contracts\Services\User\UpdateEmailServiceContract;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Admin\Client\BlacklistFilterRequest;
use App\Http\Requests\Admin\Client\ClientFilterRequest;
use App\Http\Requests\Admin\Client\ClientLockRequest;
use App\Http\Requests\Admin\Client\ClientLockTradeRequest;
use App\Http\Requests\Admin\Client\ClientUnlockRequest;
use App\Http\Requests\Admin\Client\ClientUnlockTradeRequest;
use App\Http\Requests\Admin\Client\FavoritesFilterRequest;
use App\Http\Requests\Admin\User\EmailUpdateRequest;
use App\Http\Resources\Admin\Client\ClientCollection;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\Client\Dashboard\AdCollection;
use App\Http\Resources\Client\Dashboard\DealCollection;
use App\Http\Resources\Client\Market\ReviewCollection;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use App\Repositories\Deal\DealRepo;
use App\Repositories\User\UserRepo;
use Carbon\Carbon;

/**
 * @resource Client
 */
class ClientController extends AdminBaseController
{
    /**
     * Get clients list
     *
     * @param ClientFilterRequest $request
     * @param UserRepo            $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(ClientFilterRequest $request, UserRepo $repo)
    {
        if ($request->login) {
            $repo->search($request->login);
        }
        if ($request->id) {
            $repo->filterById($request->id);
        }
        if ($request->email) {
            $repo->filterByEmail($request->email);
        }
        if ($request->created_at) {
            $repo->filterByCreatedAt(new Carbon($request->created_at));
        }
        if ($request->last_seen) {
            $repo->filterByLastSeen(new Carbon($request->last_seen));
        }
        if ($request->created_at_from) {
            $repo->filterByCreatedAtFrom(new Carbon($request->created_at_from));
        }
        if ($request->created_at_to) {
            $repo->filterByCreatedAtTo(new Carbon($request->created_at_to));
        }
        if ($request->last_seen_from) {
            $repo->filterByLastSeenFrom(new Carbon($request->last_seen_from));
        }
        if ($request->last_seen_to) {
            $repo->filterByLastSeenTo(new Carbon($request->last_seen_to));
        }

        return app(ClientCollection::class, [
            'resource' => $repo->take()
                               ->paginate($request->per_page ?? 10)
        ]);
    }

    /**
     * Clients details
     *
     * @param User $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(User $client)
    {
        return app(ClientResource::class, ['resource' => $client]);
    }

    /**
     * List of income reviews
     *
     * @param User $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function reviews(User $user)
    {
        return app(ReviewCollection::class, [
            'resource' => $user->reviewsIncome()
                               ->paginate(10)
        ]);
    }

    /**
     * List of users ads
     *
     * @param User   $user
     * @param AdRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function ads(User $user, AdRepo $repo)
    {
        return app(AdCollection::class, [
            'resource' => $repo->filterByUser($user)
                               ->take()
                               ->paginate(10)
        ]);
    }

    /**
     * List of deals between authenticated and viewed users
     *
     * @param User     $user
     * @param DealRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function deals(User $user, DealRepo $repo)
    {
        $authUser = app(AuthenticatedContract::class);
        return app(DealCollection::class, [
            'resource' => $repo->filterByAuthor($user)
                               ->filterByAdAuthor($authUser)
                               ->take()
                               ->paginate(10)
        ]);
    }

    /**
     * Unlock client
     *
     * @param ClientUnlockRequest   $request
     * @param User                  $client
     * @param UnlockServiceContract $unlockService
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function unlock(ClientUnlockRequest $request, User $client, UnlockServiceContract $unlockService)
    {
        $unlockService->unlock($client);

        activity_admin(
            "Client #" . $client->id . " unlocked. \r\n Reason: " . $request->reason,
            ActivityLogTypeConstants::CLIENTS
        );

        return app(ClientResource::class, ['resource' => $client]);
    }

    /**
     * Lock client
     *
     * @param ClientLockRequest   $request
     * @param LockServiceContract $lockService
     * @param User                $client
     * @param int                 $days
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function lock(ClientLockRequest $request, LockServiceContract $lockService, User $client, int $days = 0)
    {
        $lockService->lock($client, $days);

        activity_admin(
            "Client #" . $client->id . " locked. \r\n Reason: " . $request->reason,
            ActivityLogTypeConstants::CLIENTS
        );

        return app(ClientResource::class, ['resource' => $client]);
    }

    /**
     * Unlock client trade
     *
     * @param ClientUnlockTradeRequest   $request
     * @param UnlockTradeServiceContract $unlockTradeServiceContract
     * @param User                       $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function unlockTrade(
        ClientUnlockTradeRequest $request,
        UnlockTradeServiceContract $unlockTradeServiceContract,
        User $client
    ) {
        $unlockTradeServiceContract->unlock($client);

        activity_admin(
            "Client #" . $client->id . " trade unlocked. \r\n Reason: " . $request->reason,
            ActivityLogTypeConstants::CLIENTS
        );

        return app(ClientResource::class, ['resource' => $client]);
    }

    /**
     * Lock client trade
     *
     * @param ClientLockTradeRequest   $request
     * @param LockTradeServiceContract $lockTradeService
     * @param User                     $client
     * @param int                      $days
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function lockTrade(
        ClientLockTradeRequest $request,
        LockTradeServiceContract $lockTradeService,
        User $client,
        int $days = 0
    ) {
        $lockTradeService->lock($client, $days);

        activity_admin(
            "Client #" . $client->id . " trade locked. \r\n Reason: " . $request->reason,
            ActivityLogTypeConstants::CLIENTS
        );

        return app(ClientResource::class, ['resource' => $client]);
    }

    /**
     * Get client favorites
     *
     * @param FavoritesFilterRequest $request
     * @param User                   $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function favorites(FavoritesFilterRequest $request, User $client)
    {
        $favorites = app(UserRepo::class)
            ->getFavorites($client)
            ->take()
            ->paginate($request->per_page);
        return app(ClientCollection::class, ['resource' => $favorites]);
    }

    /**
     * Get client blacklist
     *
     * @param BlacklistFilterRequest $request
     * @param User                   $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function blacklist(BlacklistFilterRequest $request, User $client)
    {
        $blacklist = app(UserRepo::class)
            ->getBlacklist($client)
            ->take()
            ->paginate($request->per_page);
        return app(ClientCollection::class, ['resource' => $blacklist]);
    }

    public function updateEmail(
        EmailUpdateRequest $request,
        User $client
    ) {
        $service = app(UpdateEmailServiceContract::class, ['user' => $client]);
        $old     = $client->email;
        $service->update($request->email);
        activity_admin(
            "Client #{$client->id} updated email from {$old} to {$request->email}",
            ActivityLogTypeConstants::CLIENTS
        );

        return app(ClientResource::class, ['resource' => $client]);
    }
}
