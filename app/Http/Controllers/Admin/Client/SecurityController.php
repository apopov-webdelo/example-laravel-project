<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 06.11.2018
 * Time: 11:02
 */

namespace App\Http\Controllers\Admin\Client;

use App\Contracts\PasswordResetContract;
use App\Facades\Robot;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Admin\User\PasswordUpdateRequest;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\User\User;
use Illuminate\Http\Response;

/**
 * Class SecurityController
 * @package App\Http\Controllers\Admin
 *
 * @resource Client
 */
class SecurityController extends AdminBaseController
{
    /**
     * Reset password
     *
     * @param PasswordUpdateRequest $request
     * @param User                  $client
     * @param PasswordResetContract $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function resetPassword(PasswordUpdateRequest $request, User $client, PasswordResetContract $service)
    {
        if (Robot::user()->id === $client->id) {
            abort(Response::HTTP_FORBIDDEN, 'This user does not have access to reset password');
        }

        $service->setUser($client)
                ->reset($request->reason);

        activity_admin(
            'Client #'.$client->id.' password reset. Reason:'.$request->reason,
            ActivityLogTypeConstants::CLIENTS
        );

        return app(ClientResource::class, ['resource' => $client]);
    }
}
