<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.11.2018
 * Time: 11:23
 */

namespace App\Http\Controllers\Admin\Client;

use App\Contracts\SessionableContract;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Admin\SessionFilterRequest;
use App\Http\Resources\Admin\User\Session\SessionCollection;
use App\Models\User\User;
use App\Repositories\User\SessionRepo;
use Carbon\Carbon;

/**
 * Class SessionController
 * @package App\Http\Controllers\Admin
 *
 * @resource Client
 */
class SessionController extends AdminBaseController
{
    /**
     * Get sessions list
     *
     * @param SessionFilterRequest     $request
     * @param User|SessionableContract $client
     * @param SessionRepo              $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(SessionFilterRequest $request, User $client, SessionRepo $repo)
    {
        $repo->filterBySessionableId($client);

        if ($request->ip) {
            $repo->filterByIp($request->ip);
        }
        if ($request->country) {
            $repo->filterByCountry($request->country);
        }
        if ($request->city) {
            $repo->filterByCity($request->city);
        }
        if ($request->type) {
            $repo->filterByType($request->type);
        }
        if ($request->time_from) {
            $repo->filterByTimeFrom(new Carbon($request->time_from));
        }
        if ($request->time_to) {
            $repo->filterByTimeTo(new Carbon($request->time_to));
        }

        return app(SessionCollection::class, [
            'resource' => $repo->take()->orderByDesc('id')->paginate($request->per_page??25)
        ]);
    }
}
