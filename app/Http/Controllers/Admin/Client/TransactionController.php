<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 18:17
 */

namespace App\Http\Controllers\Admin\Client;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Client\Balance\TransactionsRequest;
use App\Http\Resources\Admin\Balance\TransactionCollection;
use App\Models\Balance\Balance;
use App\Repositories\Balance\TransactionRepo;

/**
 * Class TransactionController
 *
 * @package  App\Http\Controllers\Admin
 *
 * @resource Client
 */
class TransactionController extends AdminBaseController
{
    /**
     * @param Balance             $balance
     * @param TransactionsRequest $request
     * @param TransactionRepo     $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(
        Balance $balance,
        TransactionsRequest $request,
        TransactionRepo $repo
    ) {
        $transactions = $repo->filterByBalance($balance)
                             ->take()
                             ->orderByDesc('id')
                             ->paginate($request->per_page??50);

        return app(TransactionCollection::class, ['resource' => $transactions]);
    }
}
