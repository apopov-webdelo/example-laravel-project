<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.11.2018
 * Time: 12:49
 */

namespace App\Http\Controllers\Admin\Client;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Resources\Admin\User\Review\ReviewCollection;
use App\Models\User\User;

/**
 * Class ReviewController
 * @package App\Http\Controllers\Admin
 *
 * @resource Client
 */
class ReviewController extends AdminBaseController
{
    /**
     * Get reviews in list
     *
     * @param User $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function in(User $client)
    {
        return app(ReviewCollection::class, ['resource' => $client->reviewsIncome()->paginate(25)]);
    }

    /**
     * Get reviews out list
     *
     * @param User $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function out(User $client)
    {
        return app(ReviewCollection::class, ['resource' => $client->reviewsOutcome()->paginate(25)]);
    }
}
