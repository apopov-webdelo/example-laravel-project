<?php

namespace App\Http\Controllers\Admin\Client;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Resources\Admin\User\SecurityResource;
use App\Models\User\User;
use App\Services\User\Security\Google2FaService;

/**
 * @resource Google2Fa
 */
class Google2FaController extends AdminBaseController
{
    /**
     * 2fa disconnect
     *
     * @param User $client
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function disconnect(User $client)
    {
        app(Google2FaService::class, ['user' => $client])->disconnect();
        return app(SecurityResource::class, ['resource' => $client->security]);
    }
}
