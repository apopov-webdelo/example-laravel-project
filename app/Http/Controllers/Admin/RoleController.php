<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\Role\RoleChangeException;
use App\Http\Requests\Admin\Role\RoleFilterRequest;
use App\Http\Requests\Admin\Role\RoleStoreRequest;
use App\Http\Requests\Admin\Role\RoleUpdateRequest;
use App\Http\Resources\Admin\Role\RoleCollection;
use App\Http\Resources\Admin\Role\RoleResource;
use App\Models\Role\Role;
use App\Models\Role\RoleConstants;
use App\Repositories\Role\RoleRepo;
use Illuminate\Http\JsonResponse;

/**
 * @resource Roles
 */
class RoleController extends AdminBaseController
{
    /**
     * @var array $additional_data
     */
    private $additional_data = ['guard_name' => 'api'];

    /**
     * Get all
     *
     * Paginated
     *
     * @response {
     *   data: {
     *     role: [ :: Role ]
     *   },
     *   current_page: number,
     *   total: number
     *   last_page: number
     * }
     */
    public function index(RoleFilterRequest $request)
    {
        /** @var RoleRepo $repo */
        $repo = app(RoleRepo::class);
        if ($request->id) {
            $repo->filterById($request->id);
        }
        if ($request->name) {
            $repo->filterByName($request->name);
        }

        return app(RoleCollection::class, ['resource' => $repo->take()->paginate(10)]);
    }

    /**
     * Create one
     *
     * @response {
     *   role :: Role
     * }
     *
     */
    public function store(RoleStoreRequest $request)
    {
        $role = new Role(collect($request->all())->merge($this->additional_data)->toArray());
        $role->syncPermissions(collect($request->permissions)->pluck('id'));
        $role->save();

        return app(RoleResource::class, ['resource' => $role]);
    }

    /**
     * Get one
     *
     * @response {
     *   role :: Role
     * }
     *
     */
    public function details(Role $role)
    {
        return app(RoleResource::class, ['resource' => $role->load('permissions')]);
    }

    /**
     * Update one
     *
     * @response {
     *   role :: Role
     * }
     *
     */
    public function update(Role $role, RoleUpdateRequest $request)
    {
        $role->fill(collect($request->all())->merge($this->additional_data)->toArray());
        $role->syncPermissions(collect($request->permissions)->pluck('id'));
        $role->save();

        return app(RoleResource::class, ['resource' => $role->load('permissions')]);
    }

    /**
     * Delete one
     *
     * @response []
     *
     */
    public function delete(Role $role)
    {
        $this->roleChangePermissionCheck($role);
        return new JsonResponse($role->delete());
    }

    /**
     * @param Role $role
     *
     * @throws RoleChangeException
     */
    private function roleChangePermissionCheck(Role $role)
    {
        if (collect(RoleConstants::ROLES)->contains($role->id)) {
            throw new RoleChangeException('Forbidden changing role: '.$role->name);
        }
    }
}
