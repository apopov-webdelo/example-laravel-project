<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 18:17
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Client\Balance\TransactionsRequest;
use App\Http\Resources\Admin\Balance\TransactionCollection;
use App\Models\Directory\CryptoCurrency;
use App\Repositories\Balance\TransactionRepo;

/**
 * Class TransactionController
 *
 * @package App\Http\Controllers\Admin
 */
class TransactionController extends AdminBaseController
{
    /**
     * @param TransactionsRequest $request
     * @param TransactionRepo     $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(TransactionsRequest $request, TransactionRepo $repo)
    {
        if ($request->period) {
            $repo->{$request->getPeriodMethod()}();
        }
        if ($request->amount_from) {
            $repo->filterByAmountFrom(currencyToCoins($request->amount_from, CryptoCurrency::find(1)));
        }
        if ($request->amount_to) {
            $repo->filterByAmountTo(currencyToCoins($request->amount_to, CryptoCurrency::find(1)));
        }

        $transactions = $repo->take()
                             ->orderByDesc('id')
                             ->paginate($request->per_page ?? 50);

        return app(TransactionCollection::class, ['resource' => $transactions]);
    }
}
