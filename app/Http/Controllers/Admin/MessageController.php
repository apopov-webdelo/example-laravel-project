<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Chat\StoreMessageToDealServiceContract;
use App\Exceptions\Chat\ChatException;
use App\Http\Requests\Admin\Deal\DealFilterRequest;
use App\Http\Requests\Client\Chat\MessageStoreRequest;
use App\Models\Chat\Chat;
use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Repositories\Chat\MessageRepo;
use App\Services\Chat\Message\StoreMessageToUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * @resource Chat
 */
class MessageController extends AdminBaseController
{
    /**
     * @var StoreMessageToDealServiceContract $storeMessageToDealService
     */
    protected $storeMessageToDealService;

    /**
     * MessageController constructor.
     *
     * @param StoreMessageToDealServiceContract $storeMessageToDealService
     */
    public function __construct(StoreMessageToDealServiceContract $storeMessageToDealService)
    {
        $this->storeMessageToDealService = $storeMessageToDealService;
    }

    /**
     * Get messages for chat
     *
     * @param Chat              $chat
     * @param DealFilterRequest $request
     * @param MessageRepo       $messageRepo
     *
     * @return JsonResponse
     */
    public function index(Chat $chat, DealFilterRequest $request, MessageRepo $messageRepo)
    {
        return new JsonResponse(
            $messageRepo
                ->filterByChat($chat)
                ->take()
                ->with('author')
                ->orderByDesc('id')
                ->paginate($request->per_page)
        );
    }

    /**
     * Create message for chat
     *
     * @param MessageStoreRequest $request
     * @param Chat                $chat
     *
     * @return JsonResponse
     */
    public function store(MessageStoreRequest $request, Chat $chat)
    {
        //TODO: complete logic for chat messages storing

        return new JsonResponse(
            ['message' => 'Route temporary unavailable!'],
            Response::HTTP_NOT_ACCEPTABLE
        );
    }

    /**
     * Create message for user
     *
     * @param MessageStoreRequest       $request
     * @param User                      $user
     * @param StoreMessageToUserService $service
     *
     * @return JsonResponse
     */
    public function storeToUser(MessageStoreRequest $request, User $user, StoreMessageToUserService $service)
    {
        return $this->storeMessage($request, $user, $service);
    }

    /**
     * @param $request
     * @param $model
     * @param $service
     * @return JsonResponse
     */
    protected function storeMessage($request, $model, $service)
    {
        try {
            $message = $service->store($model, $request->message);
            return new JsonResponse($message, 201);
        } catch (ChatException $chatException) {
            abort($chatException->getCode(), $chatException->getMessage());
        }
    }

    /**
     * Create message for deal
     *
     * @param MessageStoreRequest       $request
     * @param Deal                      $deal
     *
     * @return JsonResponse
     */
    public function storeToDeal(MessageStoreRequest $request, Deal $deal)
    {
        return $this->storeMessage($request, $deal, $this->storeMessageToDealService);
    }
}
