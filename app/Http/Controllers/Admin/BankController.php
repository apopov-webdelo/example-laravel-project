<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Admin\Directory\BankStoreRequest;
use App\Http\Requests\Admin\Directory\BankUpdateRequest;
use App\Http\Requests\Client\Directory\BankFilterRequest;
use App\Http\Resources\Admin\Bank\BankCollection;
use App\Http\Resources\Admin\Bank\BankResource;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Repositories\Directory\BankRepo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @resource Banks
 */
class BankController extends AdminBaseController
{
    /**
     * Get bank list
     *
     * @param BankFilterRequest $request
     * @param BankRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(BankFilterRequest $request, BankRepo $repo)
    {
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->filter) {
            $repo->search($request->filter);
        }
        if ($request->exclude) {
            $repo->exclude(explode(',', $request->exclude));
        }

        return app(BankCollection::class, ['resource' => $repo->take()
                                                              ->with('author')
                                                              ->orderBy('id', 'desc')
                                                              ->paginate($request->per_page??25)
        ]);
    }

    /**
     * Get one
     *
     * @param Bank $bank
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(Bank $bank)
    {
        return app(BankResource::class, ['resource' => $bank->load('author')]);
    }

    /**
     * Create one
     *
     * @param BankStoreRequest $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function store(BankStoreRequest $request)
    {
        $bank = new Bank($request->all());
        $bank->author()->associate(auth()->user());
        $bank->save();

        return app(BankResource::class, ['resource' => $bank->load('author')]);
    }

    /**
     * Update one
     *
     * @param BankUpdateRequest $request
     * @param Bank             $bank
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function update(BankUpdateRequest $request, Bank $bank)
    {
        $bank->fill($request->all());
        $bank->author()->associate(auth()->user());
        $bank->save();

        return app(BankResource::class, ['resource' => $bank->load('author')]);
    }

    /**
     * Get currencies for bank
     *
     * @param Request $request
     * @param Bank    $bank
     *
     * @return JsonResource
     */
    public function currencies(Request $request, Bank $bank)
    {
        return new JsonResource(
            $bank->currencies()
                 ->withPivot('country_id')
                 ->where('banks_has_currencies.country_id', $request->country_id)->get()
        );
    }

    /**
     * Attach a currency to payment system with country
     *
     * @param Request       $request
     * @param Bank          $bank
     * @param Currency      $currency
     * @param AuthenticatedContract $user
     *
     * @return JsonResponse
     */
    public function attachCurrency(
        Request $request,
        Bank $bank,
        Currency $currency,
        AuthenticatedContract $user
    ) {
        $country = Country::findOrFail($request->country_id);
        $bank->currencies()->attach($currency, ['country_id'=>$country->id, 'author_id'=>$user->id]);

        return app(BankResource::class, ['resource' => $bank->load('author')]);
    }

    /**
     * Detach a currency to payment system with country
     *
     * @param Request               $request
     * @param Bank                  $bank
     * @param Currency              $currency
     * @param AuthenticatedContract $user
     *
     * @return JsonResponse
     */
    public function detachCurrency(
        Request $request,
        Bank $bank,
        Currency $currency,
        AuthenticatedContract $user
    ) {
        $country = Country::findOrFail($request->country_id);
        $bank->currencies()
             ->newPivotStatement()
             ->where('banks_has_currencies.country_id', $country->id)
             ->where('banks_has_currencies.bank_id', $bank->id)
             ->where('banks_has_currencies.currency_id', $currency->id)
             ->delete();

        return app(BankResource::class, ['resource' => $bank->load('author')]);
    }

    /**
     * Delete one
     *
     * @param Bank $bank
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(Bank $bank)
    {
        $bank->delete();

        return response()->json([]);
    }
}
