<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Directory\CountryStoreRequest;
use App\Http\Requests\Admin\Directory\CountryUpdateRequest;
use App\Http\Requests\Client\Directory\CountryFilterRequest;
use App\Http\Resources\Admin\Country\CountryCollection;
use App\Http\Resources\Admin\Country\CountryResource;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Directory\CountryRepo;
use Illuminate\Http\JsonResponse;

/**
 * @resource Directory
 */
class CountryController extends AdminBaseController
{
    /**
     * Get countries list
     *
     * @param CountryFilterRequest $request
     * @param CountryRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(CountryFilterRequest $request, CountryRepo $repo)
    {
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }

        return app(CountryCollection::class, [
            'resource'=>$repo->take()->with('currency')->paginate($request->per_page??10)
        ]);
    }

    /**
     * Get one
     *
     * @param Country $country
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(Country $country)
    {
        return app(CountryResource::class, ['resource'=>$country->load('currency')]);
    }

    /**
     * Update one
     *
     * @param CountryUpdateRequest $request
     * @param Country              $country
     *
     * @return JsonResponse
     */
    public function update(CountryUpdateRequest $request, Country $country)
    {
        $country->fill($request->all())->save();

        return app(CountryResource::class, ['resource'=>$country->load('currency')]);
    }

    /**
     * Store one
     *
     * @param CountryStoreRequest $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function store(CountryStoreRequest $request)
    {
        $country = new Country($request->all());
        $country->currency()->save(Currency::findOrFail($request->currency_id));
        $country->save();

        return app(CountryResource::class, ['resource'=>$country->load('currency')]);
    }

    /**
     * Delete one
     *
     * @param Country $country
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(Country $country)
    {
        $country->delete();

        return response()->json([]);
    }
}
