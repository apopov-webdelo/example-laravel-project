<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.11.2018
 * Time: 12:49
 */

namespace App\Http\Controllers\Admin;

use App\Contracts\Services\Review\DeletingReviewServiceContract;
use App\Contracts\Services\Review\ResetingWithBlockReviewServiceContract;
use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;
use App\Http\Requests\Admin\Review\ReviewFilterRequest;
use App\Http\Requests\Admin\Review\ReviewUpdateRequest;
use App\Http\Resources\Admin\User\Review\ReviewCollection;
use App\Http\Resources\Admin\User\Review\ReviewResource;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\User\Review;
use App\Models\User\ReviewConstants;
use App\Repositories\User\ReviewRepo;

/**
 * Class ReviewController
 *
 * @package  App\Http\Controllers\Admin
 *
 * @resource Reviews
 */
class ReviewController extends AdminBaseController
{
    /**
     * Get all review list
     *
     * @param ReviewFilterRequest $request
     * @param ReviewRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(ReviewFilterRequest $request, ReviewRepo $repo)
    {
        if ($request->trust) {
            $repo->filterByTrust($request->trust);
        }

        return app(
            ReviewCollection::class,
            [
                'resource' => $repo->take()
                                   ->paginate(25)
            ]
        );
    }

    /**
     * Get one
     *
     * @param Review $review
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(Review $review)
    {
        return app(ReviewResource::class, ['resource' => $review]);
    }

    /**
     * Update one
     *
     * @param ReviewUpdateRequest $request
     * @param Review              $review
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function update(ReviewUpdateRequest $request, Review $review)
    {
        $oldReview = clone $review;
        if ($review->fill($request->validated())->save()) {
            $message = $oldReview->message != $review->message
                ? "Message: \"{$oldReview->message}\" -> \"{$review->message}\" \r\n"
                : '';
            $rate    = $oldReview->rate != $review->rate
                ? "Rate: \"{$oldReview->rate}\" -> \"{$review->rate}\" \r\n"
                : '';
            $trust   = $oldReview->trust != $review->trust
                ? "Trust: \"{$oldReview->trust}\" -> \"{$review->trust}\" \r\n"
                : "";

            activity_admin(
                "Review #{$review->id} has been updated: \r\n 
                          {$message} {$rate} {$trust} \r\n
                          Reason: {$request->reason}",
                ActivityLogTypeConstants::CLIENTS
            );
        }

        return app(ReviewResource::class, ['resource' => $review]);
    }

    /**
     * Get negative review list
     *
     * @param ReviewRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function negatives(ReviewRepo $repo)
    {
        return app(
            ReviewCollection::class,
            [
                'resource' => $repo->filterByTrust(ReviewConstants::TRUST_NEGATIVE)
                                   ->take()
                                   ->paginate(25)
            ]
        );
    }

    /**
     * Reset review and block recipient for author
     *
     * @param ActivityLogReasonRequest               $request
     * @param Review                                 $review
     * @param ResetingWithBlockReviewServiceContract $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function resetWithBlock(
        ActivityLogReasonRequest $request,
        Review $review,
        ResetingWithBlockReviewServiceContract $service
    ) {
        if ($service->resetWithBlock($review)) {
            activity_admin(
                "Review #{$review->id} has been reset. \r\n Reason: {$request->reason}",
                ActivityLogTypeConstants::CLIENTS
            );
        }
        return app(ReviewResource::class, ['resource' => $review]);
    }

    /**
     * Delete review
     *
     * @param ActivityLogReasonRequest      $request
     * @param Review                        $review
     * @param DeletingReviewServiceContract $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function delete(ActivityLogReasonRequest $request, Review $review, DeletingReviewServiceContract $service)
    {
        if ($service->delete($review)) {
            activity_admin(
                "Review #{$review->id} has been deleted. \r\n Reason: {$request->reason}",
                ActivityLogTypeConstants::CLIENTS
            );
        }
        return app(ReviewResource::class, ['resource' => $review]);
    }
}
