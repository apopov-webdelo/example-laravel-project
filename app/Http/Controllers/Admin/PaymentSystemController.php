<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Admin\Directory\PaymentSystemStoreRequest;
use App\Http\Requests\Admin\Directory\PaymentSystemUpdateRequest;
use App\Http\Requests\Client\Directory\PaymentSystemFilterRequest;
use App\Http\Resources\Admin\PaymentSystem\PaymentSystemCollection;
use App\Http\Resources\Admin\PaymentSystem\PaymentSystemResource;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Directory\PaymentSystemRepo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @resource Payment systems
 */
class PaymentSystemController extends AdminBaseController
{
    /**
     * Get payment systems list
     *
     * @param PaymentSystemFilterRequest $request
     * @param PaymentSystemRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(PaymentSystemFilterRequest $request, PaymentSystemRepo $repo)
    {
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->filter) {
            $repo->search($request->filter);
        }
        if ($request->exclude) {
            $repo->exclude(explode(',', $request->exclude));
        }

        return app(PaymentSystemCollection::class, [
            'resource' => $repo->take()
                               ->with('currencies')
                               ->paginate($request->per_page)
        ]);
    }

    /**
     * Create one
     *
     * @param PaymentSystemStoreRequest $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function store(PaymentSystemStoreRequest $request)
    {
        return app(PaymentSystemResource::class, ['resource'=>PaymentSystem::create($request->all())]);
    }

    /**
     * Get one
     *
     * @param PaymentSystem $paymentSystem
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(PaymentSystem $paymentSystem)
    {
        return app(PaymentSystemResource::class, ['resource'=>$paymentSystem]);
    }

    /**
     * Get currencies for payment system
     *
     * @param Request       $request
     * @param PaymentSystem $paymentSystem
     *
     * @return JsonResource
     */
    public function currencies(Request $request, PaymentSystem $paymentSystem)
    {
        return new JsonResource(
            $paymentSystem->currencies()
                        ->withPivot('country_id')
                        ->where('payment_systems_has_currencies.country_id', $request->country_id)->get()
        );
    }

    /**
     * Attach a currency to payment system with country
     *
     * @param Request       $request
     * @param PaymentSystem $paymentSystem
     * @param Currency      $currency
     * @param AuthenticatedContract $user
     *
     * @return JsonResponse
     */
    public function attachCurrency(
        Request $request,
        PaymentSystem $paymentSystem,
        Currency $currency,
        AuthenticatedContract $user
    ) {
        $country = Country::findOrFail($request->country_id);
        $paymentSystem->currencies()->attach($currency, ['country_id'=>$country->id, 'author_id'=>$user->id]);
        return app(PaymentSystemResource::class, ['resource'=>$paymentSystem]);
    }

    /**
     * Detach a currency to payment system with country
     *
     * @param Request               $request
     * @param PaymentSystem         $paymentSystem
     * @param Currency              $currency
     *
     * @return JsonResponse
     */
    public function detachCurrency(
        Request $request,
        PaymentSystem $paymentSystem,
        Currency $currency
    ) {
        $country = Country::findOrFail($request->country_id);
        $paymentSystem->currencies()
                      ->newPivotStatement()
                      ->where('payment_systems_has_currencies.country_id', $country->id)
                      ->where('payment_systems_has_currencies.payment_system_id', $paymentSystem->id)
                      ->where('payment_systems_has_currencies.currency_id', $currency->id)
                      ->delete();

        return app(PaymentSystemResource::class, ['resource'=>$paymentSystem]);
    }

    /**
     * Update one
     *
     * @param PaymentSystemUpdateRequest $request
     *
     * @return JsonResponse
     */
    public function update(PaymentSystemUpdateRequest $request, PaymentSystem $paymentSystem)
    {
        $paymentSystem->fill($request->all())->save();

        return app(PaymentSystemResource::class, ['resource'=>$paymentSystem]);
    }

    /**
     * Delete one
     *
     * @param PaymentSystem $paymentSystem
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Exception
     */
    public function delete(PaymentSystem $paymentSystem)
    {
        $paymentSystem->delete();

        return app(PaymentSystemResource::class, ['resource'=>$paymentSystem]);
    }
}
