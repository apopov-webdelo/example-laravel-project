<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\PasswordResetContract;
use App\Contracts\Services\User\LockServiceContract;
use App\Contracts\Services\User\UnlockServiceContract;
use App\Http\Requests\Admin\Employee\EmployeeFilterRequest;
use App\Http\Requests\Admin\Employee\EmployeeLockRequest;
use App\Http\Requests\Admin\Employee\EmployeeStoreRequest;
use App\Http\Requests\Admin\Employee\EmployeeUnlockRequest;
use App\Http\Requests\Admin\Employee\EmployeeUpdateRequest;
use App\Http\Requests\Admin\Employee\PasswordUpdateRequest;
use App\Http\Resources\Admin\Admin\AdminResource;
use App\Http\Resources\Admin\Employee\EmployeeCollection;
use App\Http\Resources\Admin\Employee\EmployeeResource;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\Admin\Admin;
use App\Models\User\User;
use App\Repositories\Admin\AdminRepo;
use App\Services\Employee\DeleteService;
use App\Services\Employee\RegistrationService;
use App\Services\Employee\UpdateService;
use Carbon\Carbon;
use Illuminate\Http\Response;

// TODO: Replace checking permission to Policy

/**
 * Class EmployeeController
 * @package App\Http\Controllers\Admin
 *
 * @resource Employees
 */
class EmployeeController extends AdminBaseController
{
    /**
     * Get list
     *
     * @param EmployeeFilterRequest $request
     * @param AdminRepo             $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(EmployeeFilterRequest $request, AdminRepo $repo)
    {
        $this->hasPermissionTo('Operators list view');

        if ($request->id) {
            $repo->filterById($request->id);
        }
        if ($request->login) {
            $repo->search($request->login);
        }
        if ($request->email) {
            $repo->filterByEmail($request->email);
        }
        if ($request->created_at) {
            $repo->filterByCreatedAt(new Carbon($request->created_at));
        }
        if ($request->last_seen) {
            $repo->filterByLastSeen(new Carbon($request->last_seen));
        }
        if ($request->created_at_from) {
            $repo->filterByCreatedAtFrom(new Carbon($request->created_at_from));
        }
        if ($request->created_at_to) {
            $repo->filterByCreatedAtTo(new Carbon($request->created_at_to));
        }
        if ($request->last_seen_from) {
            $repo->filterByLastSeenFrom(new Carbon($request->last_seen_from));
        }
        if ($request->last_seen_to) {
            $repo->filterByLastSeenTo(new Carbon($request->last_seen_to));
        }

        return app(EmployeeCollection::class, ['resource' => $repo->take()->paginate(10)]);
    }

    /**
     * Create new employee
     *
     * @param EmployeeStoreRequest $request
     * @param RegistrationService $service
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function store(EmployeeStoreRequest $request, RegistrationService $service)
    {
        $this->hasPermissionTo('Operator creation');

        /**@var User $employee*/
        $employee = $service->store($request->all());
        return app(EmployeeResource::class, ['resource' => $employee]);
    }

    /**
     * Get employee details
     *
     * @param Admin $employee
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Exception
     */
    public function details(Admin $employee)
    {
        $this->hasPermissionTo('Operators list view');

        return app(EmployeeResource::class, ['resource' => $employee]);
    }

    /**
     * Update employee
     *
     * @param EmployeeUpdateRequest $request
     * @param Admin                 $employee
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Exception
     */
    public function update(EmployeeUpdateRequest $request, Admin $employee)
    {
        $this->hasPermissionTo('Operator update');

        $service  = app(UpdateService::class, ['model'=>$employee]);
        $employee = $service->update($request->all());

        return app(EmployeeResource::class, ['resource' => $employee]);
    }

    /**
     * Delete employee
     *
     * @param Admin         $employee
     * @param DeleteService $service
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Admin $employee, DeleteService $service)
    {
        $this->hasPermissionTo('Operator delete');
        $service->delete($employee);
        return response()->json([]);
    }

    /**
     * Lock employee
     *
     * @param EmployeeLockRequest $request
     * @param LockServiceContract $service
     * @param Admin               $employee
     * @param int                 $days
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function lock(EmployeeLockRequest $request, LockServiceContract $service, Admin $employee, int $days = 0)
    {
        $this->hasPermissionTo('Operator update');

        $service->lock($employee, $days);

        activity_admin(
            "Employee #{$employee->id} locked. Reason {$request->reason}",
            ActivityLogTypeConstants::EMPLOYEES
        );

        return app(EmployeeResource::class, ['resource' => $employee]);
    }

    /**
     * Unlock employee
     *
     * @param EmployeeUnlockRequest $request
     * @param UnlockServiceContract $service
     * @param Admin                 $employee
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function unlock(EmployeeUnlockRequest $request, UnlockServiceContract $service, Admin $employee)
    {
        $this->hasPermissionTo('Operator update');

        $service->unlock($employee);

        activity_admin(
            "Employee #{$employee->id} unlocked. Reason {$request->reason}",
            ActivityLogTypeConstants::EMPLOYEES
        );

        return app(EmployeeResource::class, ['resource' => $employee]);
    }

    /**
     * Reset password
     *
     * @param PasswordUpdateRequest $request
     * @param Admin                 $employee
     * @param PasswordResetContract $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function resetPassword(PasswordUpdateRequest $request, Admin $employee, PasswordResetContract $service)
    {
        if (root()->id === $employee->id) {
            abort(Response::HTTP_FORBIDDEN, 'This user does not have access to reset password');
        }

        $service->setUser($employee)
                ->reset($request->reason);

        activity_admin(
            "Employee #{$employee->id} password reset. Reason {$request->reason}",
            ActivityLogTypeConstants::EMPLOYEES
        );

        return app(AdminResource::class, ['resource' => $employee]);
    }

    /**
     * @param string $permission
     *
     * @return $this
     */
    private function hasPermissionTo(string $permission)
    {
        /** @var Admin $admin */
//        $admin = auth()->guard('api-admin')->user();
//        if (false === $admin->hasPermissionTo($permission)) {
//            abort(403, 'Access denied');
//        }

        return $this;
    }
}
