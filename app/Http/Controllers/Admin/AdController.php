<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\Ad\AdException;
use App\Http\Requests\Admin\Ad\AdUpdateCommissionPercentRequest;
use App\Http\Requests\Client\Ad\AdFilterRequest;
use App\Http\Requests\Client\Ad\AdUpdateConditionsRequest;
use App\Http\Requests\Client\Ad\AdUpdateRequest;
use App\Http\Resources\Client\Ad\AdCollection;
use App\Http\Resources\Admin\Ad\AdResource;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\Ad\Ad;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use App\Services\Ad\ActivateAdService;
use App\Services\Ad\DeactivateAdService;
use App\Services\Ad\UpdateAdService;

/**
 * @resource Ads
 */
class AdController extends AdminBaseController
{
    /**
     * Get all
     *
     * @param AdFilterRequest $request
     * @param AdRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(AdFilterRequest $request, AdRepo $repo)
    {
        if ($request->client_id) {
            $repo->filterByUser(User::find($request->client_id));
        }
        if ($request->author_id) {
            $repo->filterByUser(User::find($request->author_id));
        }
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }
        if ($request->price_from) {
            $repo->filterPriceFrom($request->price_from, $request->currency_id ?? null);
        }
        if ($request->price_to) {
            $repo->filterPriceTo($request->price_to, $request->currency_id ?? null);
        }
        if ($request->min_from) {
            $repo->filterMinFrom($request->min_from, $request->currency_id ?? null);
        }
        if ($request->min_to) {
            $repo->filterMinTo($request->min_to, $request->currency_id ?? null);
        }
        if ($request->max_from) {
            $repo->filterMaxFrom($request->max_from, $request->currency_id ?? null);
        }
        if ($request->max_to) {
            $repo->filterMaxTo($request->max_to, $request->currency_id ?? null);
        }
        if ($request->review_rate_from) {
            $repo->filterReputationFrom($request->review_rate_from);
        }
        if ($request->review_rate_to) {
            $repo->filterReputationTo($request->review_rate_to);
        }
        if ($request->for_sale) {
            $repo->filterForSale();
        }
        if ($request->for_buy) {
            $repo->filterForBuy();
        }
        if ($request->is_active) {
            $repo->filterForActive();
        }
        if ($request->is_blocked) {
            $repo->filterForBlocked();
        }
        if ($request->tor_denied) {
            $repo->filterForTorDenied();
        }
        if ($request->turnover_from) {
            $repo->filterByTurnoverFrom($request->turnover_from);
        }
        if ($request->turnover_to) {
            $repo->filterByTurnoverTo($request->turnover_to);
        }
        if ($request->time_from) {
            $repo->filterByTimeFrom($request->time_from);
        }
        if ($request->time_to) {
            $repo->filterByTimeTo($request->time_to);
        }
        if ($request->min_deal_finished_count_from) {
            $repo->filterByMinDealFinishedFrom($request->min_deal_finished_count_from);
        }
        if ($request->min_deal_finished_count_to) {
            $repo->filterByMinDealFinishedTo($request->min_deal_finished_count_to);
        }

        return app(AdCollection::class, [
            'resource' => $repo->take()->orderBy('created_at', 'desc')->paginate($request->per_page??20)
        ]);
    }

    /**
     * Return instanced Ad resource object
     *
     * @param Ad $ad
     * @return AdResource
     */
    protected function getAdResource(Ad $ad) : AdResource
    {
        return app(AdResource::class, ['resource' => $ad]);
    }

    /**
     * Update one
     *
     * You could send all or some fields you need to update.
     * For example for price updating you could send only 'price'=NEW_PRICE_VALUE.
     *
     * @response {
     *   ad :: Ad
     * }
     *
     * @param AdUpdateRequest $request
     * @param Ad              $ad
     * @param UpdateAdService $service
     *
     * @return mixed
     * @throws \Exception
     */
    public function update(AdUpdateRequest $request, Ad $ad, UpdateAdService $service)
    {
        $service->setData($ad, $request->validated());
        return $this->executeAction($ad, $service, __FUNCTION__);
    }

    /**
     * Update conditions
     *
     * @response {
     *   ad :: Ad
     * }
     *
     * @param AdUpdateConditionsRequest $request
     * @param Ad              $ad
     * @param UpdateAdService $service
     *
     * @return mixed
     * @throws \Exception
     */
    public function updateConditions(AdUpdateConditionsRequest $request, Ad $ad, UpdateAdService $service)
    {
        $service->setData($ad, $request->validated());
        return $this->executeAction($ad, $service, 'update');
    }

    /**
     * Update commission percent
     *
     * @param AdUpdateCommissionPercentRequest $request
     * @param Ad                               $ad
     * @param UpdateAdService                  $service
     *
     * @return AdResource
     * @throws \Exception
     */
    public function updateCommissionPercent(AdUpdateCommissionPercentRequest $request, Ad $ad, UpdateAdService $service)
    {
        $service->setData($ad, $request->validated());
        return $this->executeAction($ad, $service, 'update');
    }

    /**
     * Execute controller action using service
     *
     * @param Ad     $ad
     * @param        $service
     * @param string $method
     *
     * @return AdResource
     */
    protected function executeAction(Ad $ad, $service, string $method)
    {
        try {
            $ad = $service->$method($ad);
            activity_admin(
                "Ad #{$ad->id} has been changed through method {$method}",
                ActivityLogTypeConstants::ADS
            );
            return $this->getAdResource($ad);
        } catch (AdException $exception) {
            abort($exception->getCode(), $exception->getMessage());
        }
    }

    /**
     * Get one
     *
     * @response {
     *   message: 'You do not have access to this advertisement'
     * }
     *
     * @param Ad $ad
     *
     * @return AdResource
     */
    public function details(Ad $ad)
    {
        return $this->getAdResource($ad);
    }

    /**
     * Activate Ad
     *
     * @param Ad                $ad
     * @param ActivateAdService $service
     *
     * @return mixed
     */
    public function activate(Ad $ad, ActivateAdService $service)
    {
        return $this->executeAction($ad, $service, __FUNCTION__);
    }

    /**
     * Deactivate Ad
     *
     * @param Ad                  $ad
     * @param DeactivateAdService $service
     *
     * @return mixed
     */
    public function deactivate(Ad $ad, DeactivateAdService $service)
    {
        return $this->executeAction($ad, $service, __FUNCTION__);
    }
}
