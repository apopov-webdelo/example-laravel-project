<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Base\BaseController;
use App\Http\Requests\Admin\ActivityLog\ActivityLogFilterRequest;
use App\Http\Resources\Admin\ActivityLog\ActivityLogCollection;
use App\Repositories\ActivityLog\ActivityLogRepo;

/**
 * Class ActivityLogController
 *
 * @package App\Http\Controllers\Admin
 *
 * @resource ActivityLog
 */
class ActivityLogController extends BaseController
{
    /**
     * Get list
     *
     * @param ActivityLogFilterRequest $request
     * @param ActivityLogRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(ActivityLogFilterRequest $request, ActivityLogRepo $repo)
    {
        return app(ActivityLogCollection::class, [
            'resource' => $repo->take()
                               ->orderByDesc('id')
                               ->paginate($request->per_page??25)
        ]);
    }
}
