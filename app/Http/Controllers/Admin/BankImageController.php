<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AuthenticatedContract;
use App\Models\Directory\Bank;
use App\Models\Image\Image;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Validator;
use Illuminate\Support\Facades\Storage;

/**
 * @resource BankImage
 */
class BankImageController extends AdminBaseController
{
    protected $modelClassName = Bank::class;

    /**
     * Icon upload
     *
     * @param Request $request
     * @param Bank    $bank
     *
     * @return $this|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function upload(Request $request, Bank $bank)
    {
        if ($bank->image()->first()) {
            $this->remove($bank);
        }

        $validationRules = [
            'image' => 'image|mimes:jpeg,jpg,png,gif|max:2000'
        ];

        if ($this->validateImages($request->all(), $validationRules)) {
            $image = $request->image;
            $id = $this->addImage($image, $bank->id, $this->modelClassName, $request);
            if (!$id) {
                throw new Exception('Error while trying to save image info in db');
            }

            $move = $image->storeAs(
                Image::IMAGES_LOCATION,
                $id.'.'.$image->getClientOriginalExtension(),
                'public'
            );

            if (!$move) {
                throw new Exception('Error while trying to move image into destination folder');
            }

            return response()->json(true);

        } else {
            return response()->json($this->getValidationErrors($request->all(), $validationRules))->setStatusCode(422);
        }
    }

    /**
     * Icon remove
     *
     * @param Bank $bank
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function remove(Bank $bank)
    {
        /** @var Image $image */
        $image = $bank->image()->firstOrFail();
        if (!Storage::disk('public')->delete($image->getStoragePath())) {
            Log::alert("Can't remove file with id={$image->id}. You should remove it manually.");
        }

        if (!$image->delete()) {
            return response()->json([
                'error' => 'Error during removing image from DB.'
            ], 422);
        }

        return response()->json([]);
    }

    /**
     * @param UploadedFile $file
     * @param int          $parent_id
     * @param              $parentClass
     * @param              $request
     *
     * @return mixed
     */
    private function addImage(UploadedFile $file, int $parent_id, $parentClass, $request)
    {
        $image = new Image();

        $image->author()->associate(app(AuthenticatedContract::class));

        $image->imageable_id   = $parent_id;
        $image->imageable_type = $parentClass;
        $image->title          = '';
        $image->filename       = $file->getClientOriginalName();
        $image->mime           = $file->getMimeType();
        $image->ext            = $file->getClientOriginalExtension();
        $image->size           = $file->getSize();

        $image->save();
        return $image->id;
    }

    /**
     * @param $images
     * @param $validationRules
     * @return bool
     */
    private function validateImages($images, $validationRules)
    {
        $validator = Validator::make($images, $validationRules);
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    /**
     * @param $images
     * @param $validationRules
     * @return mixed
     */
    private function getValidationErrors($images, $validationRules)
    {
        $validator = Validator::make($images, $validationRules);
        return $validator->errors()->getMessages();
    }
}
