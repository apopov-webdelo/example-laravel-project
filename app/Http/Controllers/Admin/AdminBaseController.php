<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Base\BaseController;

/**
 * Global base for all Admin controllers
 *
 * Class AdminBaseController
 *
 * @package App\Http\Controllers\Admin
 */
abstract class AdminBaseController extends BaseController
{
}
