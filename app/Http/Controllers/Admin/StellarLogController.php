<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Repositories\StellarLogRepoContract;
use App\Http\Requests\Admin\StellarLog\StellarLogFilterRequest;
use App\Http\Resources\Admin\StellarLog\StellarLogCollection;

/**
 * @resource Stellar logs
 */
class StellarLogController extends AdminBaseController
{
    /**
     * Get stellar logs list
     *
     * @param StellarLogFilterRequest $request
     * @param StellarLogRepoContract  $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(StellarLogFilterRequest $request, StellarLogRepoContract $repo)
    {
        if ($request->code) {
            $repo->filterByCode($request->code);
        }
        if ($request->search) {
            $repo->searchByRoute($request->search);
        }
        if ($request->type) {
            $repo->filterByType($request->type);
        }
        if ($request->method) {
            $repo->filterByMethod($request->method);
        }
        if ($request->period) {
            $repo->{$request->getPeriodMethod()}();
        }

        return app(StellarLogCollection::class, [
            'resource' => $repo->take()->orderByDesc('id')->paginate($request->per_page ?? 50)
        ]);
    }
}
