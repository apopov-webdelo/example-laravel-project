<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Permission\PermissionFilterRequest;
use App\Http\Resources\Admin\Permission\PermissionCollection;
use App\Repositories\Permission\PermissionRepo;

/**
 * @resource Permissions
 */
class PermissionController extends AdminBaseController
{
    /**
     * Get all
     *
     * Paginated
     *
     * @response {
     *   data: {
     *     permission: [ :: Permission ]
     *   },
     *   current_page: number,
     *   total: number
     *   last_page: number
     * }
     */
    public function index(PermissionFilterRequest $request)
    {
        /** @var PermissionRepo $repo */
        $repo = app(PermissionRepo::class);
        if ($request->id) {
            $repo->filterById($request->id);
        }
        if ($request->name) {
            $repo->filterByName($request->name);
        }

        return app(PermissionCollection::class, ['resource' => $repo->take()->paginate($request->per_page??25)]);
    }
}
