<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Contracts\Balance\Admin\Dashboard\TotalSummaryStorageContract;
use App\Facades\Robot;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Dashboard\BalanceFilterRequest;
use App\Http\Resources\Admin\Dashboard\Balance\TransactionCollection;
use App\Http\Resources\Admin\Dashboard\Balance\TransactionSummaryResource;
use App\Models\Directory\CryptoCurrency;
use App\Repositories\Balance\TransactionRepo;

class BalanceController extends Controller
{
    /**
     * Get list
     *
     * @param BalanceFilterRequest $request
     * @param TransactionRepo      $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(BalanceFilterRequest $request, TransactionRepo $repo)
    {
        $repo->filterByBalance(
            Robot::user()
                 ->getBalance(CryptoCurrency::find($request->crypto_currency_id))
        );

        if ($request->period) {
            $repo->{$request->getPeriodMethod()}();
        }

        return app(
            TransactionCollection::class,
            [
                'resource' => $repo->take()
                                   ->paginate($request->per_page ?? 50)
            ]
        );
    }

    /**
     * Get summary
     *
     * @param BalanceFilterRequest $request
     * @param TransactionRepo      $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function summary(BalanceFilterRequest $request, TransactionRepo $repo)
    {
        $user         = Robot::user();
        $balance      = $user->getBalance(CryptoCurrency::find($request->crypto_currency_id));
        $withdrawRepo = clone $repo;
        $depositRepo  = clone $repo;


        if ($request->period) {
            $withdrawRepo->{$request->getPeriodMethod()}();
            $depositRepo->{$request->getPeriodMethod()}();
        }

        $storage = app(
            TotalSummaryStorageContract::class,
            [
                'user'         => $user,
                'balance'      => $balance,
                'withdrawRepo' => $withdrawRepo,
                'depositRepo'  => $depositRepo
            ]
        );

        return app(TransactionSummaryResource::class, ['resource' => $storage]);
    }
}
