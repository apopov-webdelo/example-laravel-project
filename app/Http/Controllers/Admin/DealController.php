<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\Deal\CancellationDealServiceContract;
use App\Exceptions\Deal\DealException;
use App\Http\Requests\Admin\Deal\DealCancelRequest;
use App\Http\Requests\Admin\Deal\DealFilterRequest;
use App\Http\Requests\Admin\Deal\DealFinishRequest;
use App\Http\Resources\Admin\Deal\DealCollection;
use App\Http\Resources\Admin\Deal\DealResource;
use App\Models\ActivityLog\ActivityLogTypeConstants;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use App\Services\Deal\DisputeDealService;
use App\Services\Deal\FinishDealService;
use App\Services\Deal\PayDealService;
use Illuminate\Http\JsonResponse;

/**
 * @resource Deal
 */
class DealController extends AdminBaseController
{
    /**
     * Get all
     *
     * @param DealFilterRequest $request
     * @param DealRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(DealFilterRequest $request, DealRepo $repo)
    {
        if ($request->author_id) {
            $repo->filterByAuthor(User::findOrFail($request->author_id));
        }
        if ($request->offer_id) {
            $repo->filterByAdAuthor(User::findOrFail($request->offer_id));
        }
        if ($request->recipient_id) {
            $repo->filterByDealMember(User::findOrFail($request->recipient_id));
        }
        if ($request->status_id) {
            $repo->filterByStatus(DealStatus::findOrFail($request->status_id));
        }

        return app(DealCollection::class, [
            'resource' => $repo->take()
                               ->orderBy('created_at', 'desc')
                               ->paginate($request->per_page ?? 20),
        ]);
    }

    /**
     * @param Deal $deal
     *
     * @return DealResource
     */
    protected function getDealResource(Deal $deal): DealResource
    {
        return app(DealResource::class, ['resource' => $deal]);
    }

    /**
     * Get one
     *
     * @response {
     *   deal :: DealResource
     * }
     *
     * @response {
     *   message: 'You do not have access to this advertisement'
     * }
     */
    public function details(Deal $deal, AuthenticatedContract $user)
    {
        return $this->getDealResource($deal);
    }

    /**
     * Execute action using policy and service according method name
     *
     * @param Deal   $deal
     * @param        $service
     * @param string $method
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function executeAction(Deal $deal, $service, string $method)
    {
        try {
            $service->$method($deal);

            return $this->getDealResource($deal);
        } catch (DealException $e) {
            abort($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Dispute one
     *
     * @response {
     *   deal :: DealResource
     * }
     *
     * @response {
     *   message: 'You do not have access to this advertisement'
     * }
     */
    public function dispute(Deal $deal, DisputeDealService $service)
    {
        return $this->executeAction($deal, $service, __FUNCTION__);
    }

    /**
     * Paid one
     *
     * @response {
     *   deal :: DealResource
     * }
     *
     * @response {
     *   message: 'You do not have access to this advertisement'
     * }
     */
    public function paid(Deal $deal, PayDealService $service)
    {
        return $this->executeAction($deal, $service, 'pay');
    }

    /**
     * Cancel one
     *
     * @param DealCancelRequest               $request
     * @param Deal                            $deal
     * @param CancellationDealServiceContract $service
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function cancellation(DealCancelRequest $request, Deal $deal, CancellationDealServiceContract $service)
    {
        $result = $this->executeAction($deal, $service, __FUNCTION__);

        activity_admin(
            'Deal #' . $deal->id . ' cancelled. Reason:' . $request->reason,
            ActivityLogTypeConstants::DEALS
        );

        return $result;
    }

    /**
     * Finish one
     *
     * @param DealFinishRequest $request
     * @param Deal              $deal
     * @param FinishDealService $service
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function finish(DealFinishRequest $request, Deal $deal, FinishDealService $service)
    {
        $result = $this->executeAction($deal, $service, __FUNCTION__);

        activity_admin(
            'Deal #' . $deal->id . ' finished. Reason:' . $request->reason,
            ActivityLogTypeConstants::DEALS
        );

        return $result;
    }

    /**
     * Statuses list
     *
     * @response {
     *   deal :: DealResource
     * }
     *
     * @response {
     *   message: 'You do not have access to this advertisement'
     * }
     */
    public function statuses()
    {
        return new JsonResponse(DealStatus::all());
    }
}
