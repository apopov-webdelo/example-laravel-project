<?php

namespace App\Http\Controllers\Blockchain;

use App\Contracts\Services\Blockchain\From\EmergencyLog\EmergencyLogHandlerContract;
use App\Http\Requests\Blockchain\EmergencyLog\EmergencyLogRequest;

class EmergencyLogController extends BlockchainBaseController
{
    /**
     * @param EmergencyLogRequest         $request
     * @param EmergencyLogHandlerContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function telegram(EmergencyLogRequest $request, EmergencyLogHandlerContract $service)
    {
        try {
            $service->channel('telegram_emergency')->send($request);
            return $this->getOkResponse();
        } catch (\Exception $e) {
            return $this->getErrorResponse($e);
        }
    }
}
