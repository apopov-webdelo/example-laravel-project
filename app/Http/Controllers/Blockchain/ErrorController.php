<?php

namespace App\Http\Controllers\Blockchain;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\Blockchain\From\Deal\DealCreationErrorContract;
use App\Contracts\Services\Blockchain\From\Transaction\WithdrawErrorContract;
use App\Contracts\Services\Blockchain\From\Wallet\WalletCreationErrorContract;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Http\Requests\Blockchain\ErrorRequest;
use App\Models\Balance\PayOut;
use App\Models\Deal\Deal;
use App\Models\User\User;

/**
 * Class ErrorController
 *
 * @package App\Http\Controllers\Blockchain
 */
class ErrorController extends BlockchainBaseController
{
    /**
     * Deal error
     *
     * Notify API that happened an error during Deal creation in blockchain gate
     *
     * @param ErrorRequest              $request
     * @param Deal                      $deal
     * @param DealCreationErrorContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deal(ErrorRequest $request, Deal $deal, DealCreationErrorContract $service)
    {
        // bind user for auto resolving todo any better solution? need AuthenticatedContract later in services
        app()->bind(AuthenticatedContract::class, function ($app) use ($deal) {
            return $deal->author;
        });

        return $this->processRequest($request, $deal, $service);
    }

    /**
     * User error
     *
     * Notify API that happened an error during User creation in blockchain gate
     *
     * @param ErrorRequest                $request
     * @param User                        $user
     * @param WalletCreationErrorContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(ErrorRequest $request, User $user, WalletCreationErrorContract $service)
    {
        return $this->processRequest($request, $user, $service);
    }

    /**
     * Withdraw error
     *
     * Notify API that happened an error during withdraw from blockchain
     *
     * @param ErrorRequest          $request
     * @param PayOut                $payOut
     * @param WithdrawErrorContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function out(ErrorRequest $request, PayOut $payOut, WithdrawErrorContract $service)
    {
        return $this->processRequest($request, $payOut, $service);
    }

    /**
     * Process request using injected service object
     *
     * @param ErrorRequest                                                                $request
     * @param                                                                             $object
     * @param DealCreationErrorContract|WalletCreationErrorContract|WithdrawErrorContract $service
     * @param string                                                                      $method
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function processRequest(ErrorRequest $request, $object, $service, $method = 'error')
    {
        try {
            $service->controlSum($request->hash)->$method(
                $object,
                $request->error,
                $request->reason
            );
            return $this->getOkResponse();
        } catch (StellarException $exception) {
            return $this->getErrorResponse($exception);
        }
    }
}
