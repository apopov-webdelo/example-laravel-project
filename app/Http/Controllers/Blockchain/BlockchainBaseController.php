<?php

namespace App\Http\Controllers\Blockchain;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Http\Response;

/**
 * Class BlockchainBaseController
 *
 * @package App\Http\Controllers\Blockchain
 */
class BlockchainBaseController extends BaseController
{
    /**
     * Default error code for service's exeptions
     *
     * @var int
     */
    protected $defaultErrorCode = 500;

    /**
     * Return error response object
     *
     * @param \Exception $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getErrorResponse(\Exception $exception)
    {
        return response()->json(
            ['message' => 'Errors.', 'errors' => $exception->getMessage()],
            $exception->getCode() ?: $this->defaultErrorCode
        );
    }

    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getOkResponse($message = 'OK')
    {
        return response()->json(['message' => $message], Response::HTTP_OK);
    }
}
