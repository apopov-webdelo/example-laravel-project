<?php

namespace App\Http\Controllers\Blockchain;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\Blockchain\From\Deal\DealCancelContract;
use App\Contracts\Services\Blockchain\From\Deal\DealFinishContract;
use App\Contracts\Services\Blockchain\From\Deal\DealVerifyContract;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Facades\Robot;
use App\Http\Requests\Blockchain\Deal\DealRequest;
use App\Models\Deal\Deal;
use Illuminate\Http\Request;

/**
 * Class DealController
 *
 * @package App\Http\Controllers\Blockchain
 */
class DealController extends BlockchainBaseController
{
    /**
     * Deal finish
     *
     * Notify API that deal was finished on blockchain
     *
     * @param DealRequest        $request
     * @param Deal               $deal
     * @param DealFinishContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function finish(DealRequest $request, Deal $deal, DealFinishContract $service)
    {
        return $this->processRequest($request, $deal, $service, __FUNCTION__);
    }

    /**
     * Deal cancel
     *
     * Notify API that deal was canceled on blockchain
     *
     * @param DealRequest        $request
     * @param Deal               $deal
     * @param DealCancelContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(DealRequest $request, Deal $deal, DealCancelContract $service)
    {
        return $this->processRequest($request, $deal, $service, __FUNCTION__);
    }

    /**
     * Deal create verified
     *
     * Notify API that deal was created on blockchain
     *
     * @param DealRequest        $request
     * @param Deal               $deal
     * @param DealVerifyContract $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verified(DealRequest $request, Deal $deal, DealVerifyContract $service)
    {
        return $this->processRequest($request, $deal, $service, __FUNCTION__);
    }

    /**
     * Process request using injected service object
     *
     * @param Request|DealRequest                                      $request
     * @param Deal                                                     $deal
     * @param DealFinishContract|DealCancelContract|DealVerifyContract $service
     * @param string                                                   $method
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function processRequest(Request $request, Deal $deal, $service, string $method)
    {
        // bind user for auto resolving
        // todo BaseService has redundant AuthContract dependency, needs refactoring and cleanup
        app()->bind(AuthenticatedContract::class, function ($app) use ($deal) {
            return Robot::user();
        });

        try {
            $service->controlSum($request->hash, $request->timestamp)->$method(
                $deal
            );

            return $this->getOkResponse();
        } catch (StellarException $exception) {
            return $this->getErrorResponse($exception);
        } catch (UnexpectedDealStatusException $exception) {
            return $this->getErrorResponse($exception);
        }
    }
}
