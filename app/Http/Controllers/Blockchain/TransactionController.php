<?php

namespace App\Http\Controllers\Blockchain;

use App\Contracts\Services\Blockchain\From\Transaction\DepositContract;
use App\Contracts\Services\Blockchain\From\Transaction\WithdrawContract;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Http\Requests\Blockchain\User\TransactionRequest;
use App\Http\Requests\Blockchain\User\WithdrawRequest;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Services\Blockchain\Stellar\From\Transaction\DepositHandler;
use App\Services\Blockchain\Stellar\From\Transaction\WithdrawHandler;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Blockchain
 */
class TransactionController extends BlockchainBaseController
{
    /**
     * User deposit
     *
     * Notify API that was incoming transaction to user wallet in blockchain
     *
     * @param TransactionRequest             $request
     * @param User                           $user
     * @param DepositContract|DepositHandler $service
     * @param CryptoCurrencyRepo             $cryptoCurrencyRepo
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @response { string }
     * @throws \Exception
     */
    public function deposit(
        TransactionRequest $request,
        User $user,
        DepositContract $service,
        CryptoCurrencyRepo $cryptoCurrencyRepo
    ) {
        try {
            $cryptoCurrency = $cryptoCurrencyRepo->getByCode($request->crypto_type);

            $service->controlSum($request->hash)->deposit(
                $user,
                $cryptoCurrency,
                $request->amount,
                $request->balance_amount,
                $request->transaction_id
            );

            return $this->getOkResponse();
        } catch (StellarException $exception) {
            return $this->getErrorResponse($exception);
        }
    }

    /**
     * User withdraw
     *
     * Notify API that user made withdraw transaction in blockchain gate directly
     *
     * @param WithdrawRequest                  $request
     * @param User                             $user
     * @param WithdrawContract|WithdrawHandler $service
     * @param CryptoCurrencyRepo               $cryptoCurrencyRepo
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @response {string}
     * @throws \Exception
     */
    public function withdraw(
        WithdrawRequest $request,
        User $user,
        WithdrawContract $service,
        CryptoCurrencyRepo $cryptoCurrencyRepo
    ) {
        try {
            $cryptoCurrency = $cryptoCurrencyRepo->getByCode($request->crypto_type);

            $service->controlSum($request->hash)->withdraw(
                $user,
                $cryptoCurrency,
                $request->amount,
                $request->balance_amount,
                $request->transaction_id,
                $request->payout_id
            );

            return $this->getOkResponse();
        } catch (StellarException $exception) {
            return $this->getErrorResponse($exception);
        }
    }
}
