<?php

namespace App\Http\Controllers\Blockchain;

use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackCancelContract;
use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackDoneContract;
use App\Http\Requests\Blockchain\DealCashback\CancelRequest;
use App\Http\Requests\Blockchain\DealCashback\DoneRequest;
use App\Models\DealCashback\DealCashback;

/**
 * Class DealCashbackController
 *
 * @package App\Http\Controllers\Blockchain
 */
class DealCashbackController extends BlockchainBaseController
{
    /**
     * Cashback finished
     *
     * Notify API that cashback was accepted and finished on box
     *
     * @param DoneRequest                     $request
     * @param DealCashback                    $cashback
     *
     * @param ProcessDealCashbackDoneContract $handler
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function done(DoneRequest $request, DealCashback $cashback, ProcessDealCashbackDoneContract $handler)
    {
        try {
            $handler->done($cashback);

            return $this->getOkResponse();
        } catch (\Exception $e) {
            return $this->getErrorResponse($e);
        }
    }

    /**
     * Cashback canceled
     *
     * Notify API that cashback was canceled on box
     *
     * @param CancelRequest                     $request
     * @param DealCashback                      $cashback
     *
     * @param ProcessDealCashbackCancelContract $handler
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(CancelRequest $request, DealCashback $cashback, ProcessDealCashbackCancelContract $handler)
    {
        try {
            $handler->cancel($cashback);

            return $this->getOkResponse();
        } catch (\Exception $e) {
            return $this->getErrorResponse($e);
        }
    }
}
