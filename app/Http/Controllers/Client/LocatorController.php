<?php

namespace App\Http\Controllers\Client;

use App\Facades\GeoLocator;
use App\Http\Resources\Client\User\GeoLocatorResource;

/**
 * Class LocatorController
 *
 * @package App\Http\Controllers\Client
 *
 * @resource GeoLocator
 */
class LocatorController extends ClientBaseController
{
    /**
     * Get geo info by user IP
     *
     * @response {
     *   GeoLocator: [ :: GeoLocatorResource ]
     * }
     */
    public function info()
    {
        return app(GeoLocatorResource::class, ['resource' => GeoLocator::getIp()]);
    }
}
