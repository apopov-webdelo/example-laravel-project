<?php

namespace App\Http\Controllers\Client;

use App\Contracts\Utils\Session\SessionStorageContract;
use App\Events\User\LoginSuccess;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Auth\AuthSocialCollection;
use App\Http\Resources\Client\User\SignInResource;
use App\Models\User\User;
use App\Providers\Social\FacebookServiceProvider;
use App\Providers\Social\GoogleServiceProvider;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Socialite\Two\InvalidStateException;
use League\OAuth1\Client\Credentials\CredentialsException;

/**
 * Class SocialController
 *
 * @resource Auth
 * @package  App\Http\Controllers\Auth
 */
class SocialController extends Controller
{
    protected $providers = [
        'facebook' => FacebookServiceProvider::class,
        'google'   => GoogleServiceProvider::class,
    ];

    /**
     *  Create a new controller instance
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Social list
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index()
    {
        $socialList = array_map(function ($item) {
            return [$item => route('social.auth.login', ['provider' => $item])];
        }, array_keys($this->providers));

        return app(AuthSocialCollection::class, ['resource' => collect($socialList)]);
    }

    /**
     * Social login
     *
     *  Redirect the user to provider authentication page
     *
     * @param  string $driver
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver)
    {
        return (new $this->providers[$driver])->redirect();
    }

    /**
     *  Provider response
     *
     * (not for front)
     *
     * @param  string $driver
     *
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function handleProviderCallback($driver)
    {
        try {
            $user = (new $this->providers[$driver])->handle();

            /* @var User $user */
            if ($user->isBlocked()) {
                return new JsonResponse([
                    'message' => __('validation.auth.banned'),
                ], Response::HTTP_FORBIDDEN);
            }

            $token = $user->generateApiToken();

            event(app(LoginSuccess::class, [
                'user'           => $user,
                'sessionStorage' => app(SessionStorageContract::class),
            ]));

            return view('auth.social.callback', ['token' => $token]);
        } catch (InvalidStateException $e) {
            return $this->redirectToProvider($driver);
        } catch (ClientException $e) {
            return $this->redirectToProvider($driver);
        } catch (CredentialsException $e) {
            return $this->redirectToProvider($driver);
        }
    }
}
