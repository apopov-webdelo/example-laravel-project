<?php

namespace App\Http\Controllers\Client;

use App\Contracts\Services\Idea\IdeaStorageContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\IdeaStoreRequest;
use App\Notifications\IdeaNotification;
use Illuminate\Support\Facades\Notification;

/**
 * Class IdeaController
 *
 * @package App\Http\Controllers\Client
 */
class IdeaController extends Controller
{
    /**
     * Store idea
     *
     * @param IdeaStoreRequest    $request
     * @param IdeaStorageContract $storage
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(IdeaStoreRequest $request, IdeaStorageContract $storage)
    {
        $storage->setName($request->name)
                ->setLogin($request->login)
                ->setEmail($request->email)
                ->setText($request->text);

        Notification::route('mail', config('app.idea.email'))
                    ->notify(new IdeaNotification($storage));

        return response()->json(true);
    }
}
