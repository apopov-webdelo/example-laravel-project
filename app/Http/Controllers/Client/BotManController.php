<?php

namespace App\Http\Controllers\Client;

use App\Conversations\StartConversation;
use BotMan\BotMan\BotMan;

/**
 * Class BotManController
 *
 * @package App\Http\Controllers\Client
 */
class BotManController extends ClientBaseController
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        \Illuminate\Support\Facades\Log::debug('Botman Controller Handle line executed');
        $botman = app('botman');

        $botman->hears('/start', function(BotMan $bot) {
            \Illuminate\Support\Facades\Log::debug('Botman Controller Handle /start line executed');
            $bot->startConversation(new StartConversation);
        });

        $botman->hears('/myid', function(BotMan $bot) {
            \Illuminate\Support\Facades\Log::debug('Botman Controller Handle /myid line executed');
            $bot->reply('Your id is ' . $bot->getMessage()->getSender());
        });

        $botman->hears('/help', function(BotMan $bot) {
            \Illuminate\Support\Facades\Log::debug('Botman Controller Handle /help line executed');
            $commandsString = '';
            collect(config('botman.telegram.commands'))->map(function ($title, $command) use (&$commandsString) {
                $commandsString .= $command . ' - '. $title . "\n";
            });

            $bot->reply("Here are available commands: \n". $commandsString);
        });

        $botman->listen();
    }
}
