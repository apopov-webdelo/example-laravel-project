<?php

namespace App\Http\Controllers\Client;


/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends ClientBaseController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('welcome');
    }
}
