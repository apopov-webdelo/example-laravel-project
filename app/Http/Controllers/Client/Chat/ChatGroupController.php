<?php

namespace App\Http\Controllers\Client\Chat;

use App\Http\Controllers\Client\ClientBaseController;

/**
 * Global base for all ChatGroup controllers
 *
 * Class ClientBaseController
 *
 * @package App\Http\Controllers\Client\Chat
 */
abstract class ChatGroupController extends ClientBaseController
{
}
