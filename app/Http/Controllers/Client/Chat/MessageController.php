<?php

namespace App\Http\Controllers\Client\Chat;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Chat\StoreMessageToDealServiceContract;
use App\Contracts\Chat\StoreMessageToUserServiceContract;
use App\Contracts\Images\ImageUploadServiceContract;
use App\Contracts\Images\ImageUploadStorageServiceContract;
use App\Exceptions\Chat\ChatException;
use App\Http\Requests\Client\Chat\MessageStoreRequest;
use App\Http\Resources\Client\Message\MessageCollection;
use App\Http\Resources\Client\Message\MessageResource;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Repositories\Chat\MessageRepo;
use App\Repositories\Deal\DealRepo;
use App\Services\Chat\Message\StoreMessageToDealService;
use App\Services\Chat\Message\StoreMessageToUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * Class MessageController
 *
 * @package  App\Http\Controllers\Client\Chat
 *
 * @resource Chat
 */
class MessageController extends ChatGroupController
{
    /**
     * @var ImageUploadServiceContract $imageService
     */
    protected $imageService;

    /**
     * MessageController constructor.
     *
     * @param ImageUploadServiceContract $imageService
     */
    public function __construct(ImageUploadServiceContract $imageService)
    {
        $this->imageService = $imageService;
    }


    /**
     * Get messages for chat
     *
     * @param Chat        $chat
     * @param Request     $request
     * @param MessageRepo $messageRepo
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Chat $chat, Request $request, MessageRepo $messageRepo)
    {
        $this->authorize('index', [Message::class, $chat]);

        return app(MessageCollection::class, [
            'resource' => $messageRepo
                ->filterByChat($chat)
                ->take()
                ->orderByDesc('id')
                ->paginate(50),
        ]);
    }

    /**
     * Get messages for deal
     *
     * @param Deal        $deal
     * @param Request     $request
     * @param MessageRepo $messageRepo
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function getDealChat(Deal $deal, Request $request, MessageRepo $messageRepo)
    {
        $chat = $deal->chat();

        if ($chat) {
            return $this->index($chat, $request, $messageRepo);
        }

        return new JsonResponse(
            ['message' => 'Chat not found'],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * Recent messages
     *
     * Get recent messages from all active deals
     *
     * @param MessageRepo           $messageRepo
     * @param DealRepo              $dealRepo
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function getRecentDealMessages(MessageRepo $messageRepo, DealRepo $dealRepo, AuthenticatedContract $user)
    {
        $openChats = $dealRepo->filterByDealMember($user)
                              ->filterByStatusId(DealStatusConstants::ACTIVE_STATUSES)
                              ->take()
                              ->with('chats')
                              ->get()
                              ->pluck('chats')
                              ->filter(function ($chats) {
                                  return count($chats);
                              })
                              ->reduce(function ($carry, $chats) {
                                  return $carry->merge($chats);
                              }, $carry = collect())
                              ->map(function ($chat) {
                                  return $chat->id;
                              });

        return app(MessageCollection::class, [
            'resource' => $messageRepo
                ->filterByChats($openChats)
                ->take()
                ->orderByDesc('id')
                ->paginate(25),
        ]);
    }

    /**
     * Create message for chat
     *
     * @param MessageStoreRequest $request
     * @param Chat                $chat
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(MessageStoreRequest $request, Chat $chat)
    {
        $this->authorize('storeToChat', [Message::class, $chat]);

        //TODO: complete logic for chat messages storing

        return new JsonResponse(
            ['message' => 'Route temporary unavailable!'],
            Response::HTTP_NOT_ACCEPTABLE
        );
    }

    /**
     * Create message for user
     *
     * @param MessageStoreRequest       $request
     * @param User                      $user
     * @param StoreMessageToUserService $service
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function storeToUser(MessageStoreRequest $request, User $user, StoreMessageToUserService $service)
    {
        $this->authorize('storeToUser', [Message::class, $user]);

        return $this->storeMessage($request, $user, $service);
    }

    /**
     * @param MessageStoreRequest $request
     * @param $model
     * @param StoreMessageToDealServiceContract|StoreMessageToUserServiceContract $service
     *
     * @return JsonResponse
     */
    protected function storeMessage($request, $model, $service)
    {
        try {
            $message = $service->store($model, $request->message);
            if ($request->image) {
                $this->imageService->upload(
                    $message,
                    app(ImageUploadStorageServiceContract::class)->setImage($request->image)
                );
            }
            return app(MessageResource::class, ['resource' => $message, 'status_code' => Response::HTTP_CREATED]);
        } catch (ChatException $chatException) {
            abort($chatException->getCode(), $chatException->getMessage());
        } catch (\Exception $exception) {
            Log::debug($exception->getMessage(), ['exception' => $exception]);
            abort($exception->getCode(), $exception->getMessage());
        }
    }

    /**
     * Create message for deal
     *
     * @param MessageStoreRequest       $request
     * @param Deal                      $deal
     * @param StoreMessageToDealService $service
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function storeToDeal(MessageStoreRequest $request, Deal $deal, StoreMessageToDealService $service)
    {
        $this->authorize('storeToDeal', [Message::class, $deal]);

        return $this->storeMessage($request, $deal, $service);
    }
}
