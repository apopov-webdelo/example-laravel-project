<?php

namespace App\Http\Controllers\Client\Chat;

use App\Http\Requests\Client\Chat\ChatFilterRequest;
use App\Http\Resources\Client\Chat\ChatCollection;
use App\Http\Resources\Client\Chat\ChatResource;
use App\Models\Chat\Chat;
use App\Models\User\User;
use App\Repositories\Chat\ChatRepo;
use Illuminate\Http\Response;


/**
 * Class ChatController
 *
 * @package App\Http\Controllers\Client\Chat
 *
 * @resource Chat
 */
class ChatController extends ChatGroupController
{
    /**
     * Get all
     *
     * Retrieve all chats where user is recipient
     *
     * @param ChatFilterRequest $request
     * @param ChatRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(ChatFilterRequest $request, ChatRepo $repo)
    {
        /** @var User $user */
        $user = auth()->user();

        if ($request->type === 'private') {
            $repo->filterPrivate();
        }

        return app(ChatCollection::class, [
            'resource' => $repo->filterByRecipient($user)
                               ->take()
                               ->orderByRaw(' (
                                    SELECT MAX(`id`) 
                                    FROM `chat_messages` 
                                    WHERE `chat_id` = `chat`.`id`
                                    LIMIT 1
                                    
                               ) DESC')
                               ->paginate(25)
        ]);
    }

    /**
     * Get private chat
     *
     * Retrieve chat resource if authenticated user have private chat with needed user
     *
     * @param ChatRepo $repo
     * @param User     $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function user(ChatRepo $repo, User $user)
    {
        /** @var User $authUser */
        $authUser = auth()->user();

        /** @var Chat|null $chat */
        $chat = $repo->getChatBetweenUsers($user, $authUser);

        return $chat
            ? app(ChatResource::class, [
                    'resource' => $chat,
                ])
            : abort(Response::HTTP_NO_CONTENT, "There is no private chat with that user!");
    }
}
