<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Exceptions\User\PhoneDoesntExistException;
use App\Http\Requests\Client\User\PhoneConfirmTokenRequest;
use App\Http\Requests\Client\User\PhoneDeleteRequest;
use App\Http\Resources\Client\User\ContactResource;
use App\Services\User\Security\PhoneDeleteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


/**
 * Class PhoneDeleteController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Phone
 */
class PhoneDeleteController extends UserGroupController
{
    /**
     * Delete phone number
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function delete(PhoneDeleteRequest $request, AuthenticatedContract $user, PhoneDeleteService $service)
    {
        try {
            $service->delete();
        } catch (PhoneDoesntExistException $exception) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    ['phone'=>__('validation.phone_doesnt_exsits')]
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ContactResource::class, ['resource' => $user->refresh()]);
    }

    /**
     * Complete phone number deleting
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function complete(
        PhoneConfirmTokenRequest $request,
        PhoneDeleteService $service,
        AuthenticatedContract $user
    ) {
        $service->complete();

        return app(ContactResource::class, ['resource' => $user->refresh()]);
    }
}
