<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\User\UserChangesServiceContract;
use App\Http\Requests\Client\User\PasswordUpdateRequest;
use App\Http\Requests\Client\User\ProfileUpdateRequest;
use App\Http\Requests\Client\User\UpdateAppealRequest;
use App\Http\Requests\Client\User\SetEmailRequest;
use App\Http\Requests\Client\User\SetLoginRequest;
use App\Http\Resources\Client\User\ProfileResource;
use App\Http\Resources\Client\User\Review\ReviewCollection;
use App\Http\Resources\Client\User\Security\SecurityLogsCollection;
use App\Http\Resources\Client\User\SecurityResource;
use App\Http\Resources\Client\User\Session\SessionCollection;
use App\Http\Resources\Client\User\SettingsResource;
use App\Http\Resources\Client\User\SignInResource;
use App\Http\Resources\Client\User\SpoofingProtectResource;
use App\Http\Resources\Client\User\UserResource;
use App\Http\Rules\CheckGoogle2Fa;
use App\Models\User\ReviewConstants;
use App\Models\User\User;
use App\Repositories\User\ReviewRepo;
use App\Services\User\Security\ChangePasswordService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

/**
 * Class ProfileController
 *
 * @package  App\Http\Controllers\Client\User
 *
 * @resource Profile
 */
class ProfileController extends UserGroupController
{
    /**
     * Get profile
     *
     * is_social: if "true" - you can change login and email once for this acount using special routes
     * and you should not show change email/change password in front (user has no password,
     * cannot confirm with password etc)
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(AuthenticatedContract $user)
    {
        return app(ProfileResource::class, ['resource' => $user]);
    }

    /**
     * Update profile
     *
     * @param ProfileUpdateRequest  $request
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function update(ProfileUpdateRequest $request, AuthenticatedContract $user)
    {
        //TODO: реализовать логику метода по обновлению профиля
        /*
        1. Получаем авторизованного пользователя
        2. Сохраняем обновленные данные из метода $request->validated()
        3. Возвращаем обновленного пользователя во фронт - Resources\User\ProfileResource
        */
        return app(ProfileResource::class, ['resource' => $user, 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Update appeal
     *
     * @param UpdateAppealRequest   $request
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function updateAppeal(UpdateAppealRequest $request, AuthenticatedContract $user)
    {
        $user->appeal = $request->appeal;
        $user->save();

        return app(ProfileResource::class, ['resource' => $user, 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Social reg email
     *
     * Change social registration's email once
     *
     * @param SetEmailRequest       $request
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function setEmail(SetEmailRequest $request, AuthenticatedContract $user)
    {
        /** @var UserChangesServiceContract $service */
        $service = app(UserChangesServiceContract::class);
        $service->emailChange($request->email);

        return app(ProfileResource::class, ['resource' => $user]);
    }

    /**
     * Social reg login
     *
     * Change social registration's login once
     *
     * @param SetLoginRequest       $request
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function setLogin(SetLoginRequest $request, AuthenticatedContract $user)
    {
        /** @var UserChangesServiceContract $service */
        $service = app(UserChangesServiceContract::class);
        $service->loginChange($request->login);

        return app(ProfileResource::class, ['resource' => $user]);
    }

    /**
     * Change password
     *
     * **Notice** This action changes api token
     *
     * @param PasswordUpdateRequest $request
     * @param AuthenticatedContract $user
     *
     * @param ChangePasswordService $service
     *
     * @return \Illuminate\Foundation\Application|JsonResponse|mixed
     * @throws \Exception
     */
    public function changePassword(
        PasswordUpdateRequest $request,
        AuthenticatedContract $user,
        ChangePasswordService $service
    ) {
        if ($user->isGoogle2faActive()) {
            $validator = Validator::make($request->only('google2fa_secret'), [
                'google2fa_secret' => ['required', app(CheckGoogle2Fa::class)],
            ]);
            if ($validator->fails()) {
                $service->logFail2fa();

                return new JsonResponse([
                    'message' => __('validation.user.google2fa_doesnt_match'),
                    'errors'  => $validator->errors(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $service->change($request->new_password);

        $token = $user->generateApiToken();

        return app(SignInResource::class, ['resource' => $user, 'token' => $token]);
    }

    /**
     * List of income reviews
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function reviewsIncome(AuthenticatedContract $user)
    {
        return app(ReviewCollection::class, ['resource' => $user->reviewsIncome()->paginate(25)]);
    }

    /**
     * List of outcome reviews
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function reviewsOutcome(AuthenticatedContract $user)
    {
        return app(ReviewCollection::class, ['resource' => $user->reviewsOutcome()->paginate(25)]);
    }

    /**
     * List of sessions
     *
     * status: good, bad
     *
     * type: sign-in, sign-out
     *
     * In descending order
     *
     * @response {
     *   "data": [
     *   {
     *   "ip": "127.0.0.0",
     *   "country": "United States",
     *   "city": "New Haven",
     *   "os": "Windows",
     *   "browser": "Safari",
     *   "status": "good",
     *   "type": "sign-in",
     *    "time": "2018-12-27 13:59:28 +03:00"
     *   },
     *   {
     *   "ip": "127.0.0.0",
     *   "country": "United States",
     *   "city": "New Haven",
     *   "os": "Android",
     *   "browser": "Chrome",
     *   "status": "good",
     *   "type": "sign-in",
     *   "time": "2018-12-27 12:41:28 +03:00"
     *   }
     *   ]
     * }
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function sessions(AuthenticatedContract $user)
    {
        return app(SessionCollection::class, [
            'resource' => $user->sessions()->orderByDesc('id')->paginate(25),
        ]);
    }

    /**
     * List of security logs
     *
     * status: true, false (good, bad event)
     *
     * type: human readable description
     *
     * status_desc: additional description for 'type' (always null at this moment)
     *
     * @response {
     *   "data": [
     *   {
     *   "ip": "127.0.0.0",
     *   "country": "United States",
     *   "city": "New Haven",
     *   "os": "Windows",
     *   "browser": "Safari",
     *   "status": true,
     *   "status_desc": null,
     *   "type": "начата процедура подтверждения email",
     *    "time": "2018-12-27 13:59:28 +03:00"
     *   },
     *   {
     *   "ip": "127.0.0.0",
     *   "country": "United States",
     *   "city": "New Haven",
     *   "os": "Android",
     *   "browser": "Chrome",
     *   "status": false,
     *   "status_desc": null,
     *   "type": "заблокирована торговля",
     *   "time": "2018-12-27 12:41:28 +03:00"
     *   }
     *   ]
     * }
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function securityLogs(AuthenticatedContract $user)
    {
        return app(SecurityLogsCollection::class, [
            'resource' => $user->securityLogs()->orderByDesc('id')->paginate(25),
        ]);
    }

    /**
     * List of security
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function security(AuthenticatedContract $user)
    {
        return app(SecurityResource::class, ['resource' => $user->security]);
    }

    /**
     * List of settings
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function settings(AuthenticatedContract $user)
    {
        return app(SettingsResource::class, ['resource' => $user->settings]);
    }

    /**
     * List of blocked users
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function blacklist(AuthenticatedContract $user)
    {
        /** @var ReviewRepo $repo */
        $repo = app(ReviewRepo::class);
        $repo->filterByAuthor($user)
             ->filterByTrust(ReviewConstants::TRUST_NEGATIVE);

        return UserResource::collection(
            User::query()->whereIn('id', $repo->take()->pluck('recipient_id')->all())->get()
        );
    }

    /**
     * List of favorite users
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function favoritelist(AuthenticatedContract $user)
    {
        /** @var ReviewRepo $repo */
        $repo = app(ReviewRepo::class);
        $repo->filterByAuthor($user)
             ->filterByRateFrom(4);

        return UserResource::collection(
            User::query()->whereIn('id', $repo->take()->pluck('recipient_id')->all())->get()
        );
    }

    /**
     * Get spoofing protection signature
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function spoofingProtection(AuthenticatedContract $user)
    {
        return app(SpoofingProtectResource::class, ['resource' => $user]);
    }
}
