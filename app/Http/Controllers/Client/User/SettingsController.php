<?php

namespace App\Http\Controllers\Client\User;

use App\Http\Requests\Client\User\UpdateDealCancellationMaxPercentRequest;
use App\Http\Resources\Client\User\ProfileResource;
use App\Services\User\Settings\DealNotificationService;
use App\Services\User\Settings\EmailNotificationService;
use App\Services\User\Settings\MessageNotificationService;
use App\Services\User\Settings\PhoneNotificationService;
use App\Services\User\Settings\PushNotificationService;
use App\Services\User\Settings\TelegramNotificationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


/**
 * Class SettingsController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Profile settings
 */
class SettingsController extends UserGroupController
{
    /**
     * Enable email notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enableEmailNotifications()
    {
        if (false === app(EmailNotificationService::class)->enable()) {
            return new JsonResponse([
                'message' => 'Error during email notification enable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Disable email notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disableEmailNotifications()
    {
        if (false === app(EmailNotificationService::class)->disable()) {
            return new JsonResponse([
                'message' => 'Error during email notification disable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Enable phone notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enablePhoneNotifications()
    {
        if (false === app(PhoneNotificationService::class)->enable()) {
            return new JsonResponse([
                'message' => 'Error during phone notification enable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Disable phone notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disablePhoneNotifications()
    {
        if (false === app(PhoneNotificationService::class)->disable()) {
            return new JsonResponse([
                'message' => 'Error during phone notification disable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Enable telegram notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enableTelegramNotifications()
    {
        if (false === app(TelegramNotificationService::class)->enable()) {
            return new JsonResponse([
                'message' => 'Error during telegram notification enable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Disable telegram notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disableTelegramNotifications()
    {
        if (false === app(TelegramNotificationService::class)->disable()) {
            return new JsonResponse([
                'message' => 'Error during telegram notification disable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Enable deal notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enableDealNotifications()
    {
        if (false === app(DealNotificationService::class)->enable()) {
            return new JsonResponse([
                'message' => 'Error during deal notification enable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Disable deal notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disableDealNotifications()
    {
        if (false === app(DealNotificationService::class)->disable()) {
            return new JsonResponse([
                'message' => 'Error during deal notification disable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Enable message notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enableMessageNotifications()
    {
        if (false === app(MessageNotificationService::class)->enable()) {
            return new JsonResponse([
                'message' => 'Error during message notification enable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Disable message notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disableMessageNotifications()
    {
        if (false === app(MessageNotificationService::class)->disable()) {
            return new JsonResponse([
                'message' => 'Error during message notification disable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Enable push notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enablePushNotifications()
    {
        if (false === app(PushNotificationService::class)->enable()) {
            return new JsonResponse([
                'message' => 'Error during push notification enable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Disable push notification
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disablePushNotifications()
    {
        if (false === app(PushNotificationService::class)->disable()) {
            return new JsonResponse([
                'message' => 'Error during push notification disable'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ProfileResource::class, ['resource' => auth()->user(), 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Update deal cancellation %
     *
     * Update required deal cancellation percent
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function updateDealCancellationMaxPercent(UpdateDealCancellationMaxPercentRequest $request)
    {
        $user     = auth()->user()->load('settings');
        $settings = $user->settings;

        $settings->deal_cancellation_max_percent = $request->deal_cancellation_max_percent;
        $settings->save();

        return app(ProfileResource::class, ['resource' => $user, 'status_code' => Response::HTTP_CREATED]);
    }
}
