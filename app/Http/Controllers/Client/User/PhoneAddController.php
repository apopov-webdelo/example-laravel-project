<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Exceptions\User\PhoneAlreadyExistsException;
use App\Http\Requests\Client\User\PhoneAddRequest;
use App\Http\Requests\Client\User\PhoneConfirmTokenRequest;
use App\Http\Resources\Client\User\ContactResource;
use App\Services\User\Security\PhoneAddService;
use App\Services\User\Security\PhoneService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


/**
 * Class PhoneAddController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Phone
 */
class PhoneAddController extends UserGroupController
{
    /**
     * Add phone number
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function phoneAdd(PhoneAddRequest $request, AuthenticatedContract $user, PhoneAddService $service)
    {
        try {
            $service->add($request->phone);
        } catch (PhoneAlreadyExistsException $exception) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    ['phone'=>__('validation.phone_already_exsits')]
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(ContactResource::class, ['resource' => $user->refresh()]);
    }

    /**
     * Confirm added phone
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function confirm(PhoneConfirmTokenRequest $request, PhoneService $service, AuthenticatedContract $user)
    {
        $service->confirm();

        return app(ContactResource::class, ['resource' => $user->refresh()]);
    }
}
