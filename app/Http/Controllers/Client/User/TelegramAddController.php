<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Client\User\SetTelegramIdRequest;
use App\Http\Requests\Client\User\TelegramConfirmTokenRequest;
use App\Http\Resources\Client\User\ContactResource;
use App\Services\User\Security\TelegramAddService;


/**
 * Class TelegramAddController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Telegram
 */
class TelegramAddController extends UserGroupController
{
    /**
     * Set telegram ID
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function telegramAdd(
        SetTelegramIdRequest $request,
        TelegramAddService $service,
        AuthenticatedContract $user
    ) {
        $service->add($request->telegram_id);

        return app(ContactResource::class, ['resource' => $user]);
    }

    /**
     * Confirm telegram
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function confirmTelegramId(
        TelegramConfirmTokenRequest $request,
        TelegramAddService $service,
        AuthenticatedContract $user
    ) {
        $service->confirm();

        return app(ContactResource::class, ['resource' => $user]);
    }
}
