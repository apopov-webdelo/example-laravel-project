<?php

namespace App\Http\Controllers\Client\User;

use App\Http\Requests\Client\User\AdsUserRequest;
use App\Http\Requests\Client\User\DealsUserRequest;
use App\Http\Requests\Client\User\UserFilterRequest;
use App\Http\Resources\Client\Dashboard\AdCollection;
use App\Http\Resources\Client\Dashboard\DealCollection;
use App\Http\Resources\Client\Market\ReviewCollection;
use App\Http\Resources\Client\NotFoundResource;
use App\Http\Resources\Client\User\TurnoverSummaryResource;
use App\Http\Resources\Client\User\UserCollection;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\ReviewConstants;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use App\Repositories\Deal\DealRepo;
use App\Repositories\User\UserRepo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;


/**
 * Class UserController
 *
 * @package  App\Http\Controllers\Client\User
 *
 * @resource User
 */
class UserController extends UserGroupController
{
    /**
     * Get user list
     *
     * @param UserFilterRequest $request
     * @param UserRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(UserFilterRequest $request, UserRepo $repo)
    {
        return app(UserCollection::class, [
            'resource' => $repo->searchByLogin($request->login)
                               ->take()
                               ->paginate(10),
        ]);
    }

    /**
     * Users details
     *
     * @param User $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(User $user)
    {
        return app(UserResource::class, ['resource' => $user]);
    }

    /**
     * Users details by login
     *
     * @param string   $login
     * @param UserRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function detailsLogin(string $login, UserRepo $repo)
    {
        $user = $repo->filterByLogin($login)
                     ->take()
                     ->first();

        return $user
            ? app(UserResource::class, ['resource' => $user])
            : app(NotFoundResource::class);
    }

    /**
     * List of income reviews
     *
     * @param User $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function reviews(User $user)
    {
        $reviews = $user->reviewsIncome()
                        ->where(function (Builder $builder) {
                            $builder->where('rate', '!=', ReviewConstants::RATE_NEGATIVE_HIDDEN);
                        })
                        ->orderByDesc('id')
                        ->paginate(10);

        return app(ReviewCollection::class, ['resource' => $reviews]);
    }

    /**
     * Ads List of user
     *
     * Retrieve all active user's ads
     *
     * @param AdsUserRequest $request
     * @param User           $user
     * @param AdRepo         $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function ads(AdsUserRequest $request, User $user, AdRepo $repo)
    {
        $repo->filterByUser($user)->filterForActive();

        if ($request->for_sale) {
            $repo->filterForSale();
        } elseif ($request->for_buy) {
            $repo->filterForBuy();
        }

        return app(AdCollection::class, ['resource' => $repo->take()->paginate(10)]);
    }

    /**
     * Deals between authenticated and viewed users
     *
     * Retrieve all deals according applied filters where user and authenticated user is deal members.
     *
     * @param DealsUserRequest $request
     * @param User             $user
     * @param DealRepo         $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function deals(DealsUserRequest $request, User $user, DealRepo $repo)
    {
        /** @var User $authUser */
        $authUser = Auth::user();

        if ($authUser->id !== $user->id) {
            $repo->filterByDealMembersCross($authUser, $user);
        } else {
            $repo->filterByDealMember($authUser);
        }

        if (!$request->for_sale || !$request->for_buy) {
            if ($request->for_sale) {
                $repo->filterForSale($user);
            }
            if ($request->for_buy) {
                $repo->filterForBuy($user);
            }
        }

        if ($request->is_active || $request->is_blocked) {
            if ($request->is_active) {
                $repo->filterByStatusId(DealStatusConstants::ACTIVE_STATUSES);
            } elseif ($request->is_blocked) {
                $repo->filterByStatusId(DealStatusConstants::INACTIVE_STATUSES);
            }
        }

        return app(DealCollection::class, ['resource' => $repo->take()->paginate(10)]);
    }

    /**
     * Turnover summary
     *
     * @param User $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function turnoverSummary(User $user)
    {
        return app(TurnoverSummaryResource::class, ['resource' => $user]);
    }
}
