<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Client\User\EmailAccessTokenRequest;
use App\Http\Requests\Client\User\EmailConfirmTokenRequest;
use App\Http\Requests\Client\User\EmailUpdateRequest;
use App\Http\Resources\Client\User\ContactResource;
use App\Http\Resources\Client\User\ProfileResource;
use App\Http\Rules\CheckGoogle2Fa;
use App\Services\User\Security\EmailConfirmationService;
use App\Services\User\Security\EmailService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

/**
 * Class EmailChangeController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Email change
 */
class EmailChangeController extends UserGroupController
{

    /**
     * Email update
     *
     * @param EmailUpdateRequest    $request
     * @param EmailService          $service
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|JsonResponse|mixed
     */
    public function emailUpdate(
        EmailUpdateRequest $request,
        EmailService $service,
        AuthenticatedContract $user
    ) {
        if ($user->isGoogle2faActive()) {
            $validator = Validator::make($request->only('google2fa_secret'), [
                'google2fa_secret'=> [ 'required', app(CheckGoogle2Fa::class) ]
            ]);
            if ($validator->fails()) {
                return new JsonResponse([
                    'message' => __('validation.user.google2fa_doesnt_match'),
                    'errors'  => $validator->errors()
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        if ($user->email === $request->email) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    ['email'=>__('validation.user.email_diff')]
                ]
            ], 422);
        }

        $service->change($request->email);

        return app(ContactResource::class, ['resource' => $user, 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Confirm email update
     *
     * **Notice** This action changes api token
     *
     * @param EmailAccessTokenRequest $request
     * @param EmailService            $service
     * @param AuthenticatedContract   $user
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function confirmEmailUpdate(
        EmailAccessTokenRequest $request,
        EmailService $service,
        AuthenticatedContract $user
    ) {
        $service->confirmChange();

        $token = $user->generateApiToken();

        return new JsonResponse([
            'data' => [
                'user'      => new ProfileResource($user),
                'api_token' => $token
            ]
        ]);
    }

    /**
     * Start email confirmation
     *
     * @param EmailConfirmationService $service
     * @param AuthenticatedContract    $user
     *
     * @return JsonResource
     * @throws \ReflectionException
     */
    public function startConfirmEmail(EmailConfirmationService $service, AuthenticatedContract $user)
    {
        $service->startProcess();

        return new JsonResource([
            'data' => [
                'user' => new ProfileResource($user),
            ]
        ]);
    }

    /**
     * Confirm email
     *
     * @param EmailConfirmTokenRequest $request
     * @param EmailService             $service
     * @param AuthenticatedContract    $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function confirmEmail(
        EmailConfirmTokenRequest $request,
        EmailService $service,
        AuthenticatedContract $user
    ) {
        $service->confirm();

        return app(ContactResource::class, ['resource' => $user]);
    }
}
