<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Exceptions\User\Google2FaAlreadyConnectedException;
use App\Http\Requests\Client\User\DisableGoogle2FaRequestRequest;
use App\Http\Requests\Client\User\DisconnectGoogle2FaRequestRequest;
use App\Http\Requests\Client\User\EnableGoogle2FaRequestRequest;
use App\Http\Resources\Client\User\SecurityResource;
use App\Services\User\Security\Google2FaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


/**
 * Class Google2FaController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Google2Fa
 */
class Google2FaController extends UserGroupController
{
    /**
     * 2fa connect
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function connect(Google2FaService $service)
    {
        try {
            return new JsonResponse([
                'data' => $service->connect()
            ]);
        } catch (Google2FaAlreadyConnectedException $exception) {
            return new JsonResponse([
                'message' => __('validation.google2fa.connected')
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * 2fa disconnect
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disconnect(
        DisconnectGoogle2FaRequestRequest $request,
        Google2FaService $service,
        AuthenticatedContract $user
    ) {
        $service->disconnect();

        return app(SecurityResource::class, ['resource' => $user->security]);
    }

    /**
     * 2fa enable
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function enable(
        EnableGoogle2FaRequestRequest $request,
        Google2FaService $service,
        AuthenticatedContract $user
    ) {
        $service->enable();

        return app(SecurityResource::class, ['resource' => $user->security]);
    }

    /**
     * 2fa disable
     *
     * @response {
     *   profileResource :: ProfileResource
     * }
     */
    public function disable(
        DisableGoogle2FaRequestRequest $request,
        Google2FaService $service,
        AuthenticatedContract $user
    ) {
        /*
        1. Получение авторизованного пользователя
        Нужен объект для отключения 2fa_confirmed - 2faService
        2. Проверка 2fa_secret
        3. fa_confirmed=null
        4. Возврат профиля во фронт - Resources\User\ProfileResource
        */
        $service->disable();

        return app(SecurityResource::class, ['resource' => $user->security]);
    }
}
