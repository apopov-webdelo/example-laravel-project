<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Models\Image\Image;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Validator;
use Mockery\Exception;

/**
 * Class ProfileImageController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Profile
 */
class ProfileImageController extends UserGroupController
{
    /**
     * @var string
     */
    protected $modelClassName = User::class;

    /**
     * Avatar upload
     *
     * @response {
     *   bool
     * }
     */
    public function upload(Request $request, AuthenticatedContract $user)
    {
        if ($user->image()->first()) {
            $this->remove($user);
        }

        $validationRules = [
            'image' => 'image|mimes:jpeg,jpg,png,gif|max:2000'
        ];

        if ($this->validateImages($request->all(), $validationRules)) {
            $image = $request->image;
            $id = $this->addImage($image, $user->id, $this->modelClassName, $request);

            if (!$id) {
                throw new Exception('Error while trying to save image info in db');
            }

            $move = $image->storeAs(
                Image::IMAGES_LOCATION,
                $id.'.'.$image->getClientOriginalExtension(),
                'public'
            );

            if (!$move) {
                throw new Exception('Error while trying to move image into destination folder');
            }

            return response()->json(true);
        } else {
            return response()->json($this->getValidationErrors($request->all(), $validationRules))
                             ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Avatar remove
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function remove(AuthenticatedContract $user)
    {
        /** @var Image $image */
        $image = $user->image()->firstOrFail();
        if (!Storage::disk('public')->delete($image->getStoragePath())) {
            Log::alert("Can't remove file with id={$image->id}. You should remove it manually.");
        }

        if (!$image->delete()) {
            return response()->json([
                'error' => 'Error during removing image from DB.'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([]);
    }

    /**
     * @param UploadedFile $file
     * @param int          $parent_id
     * @param              $parentClass
     * @param              $request
     *
     * @return mixed
     */
    private function addImage(UploadedFile $file, int $parent_id, $parentClass, $request)
    {
        $image = new Image();

        $image->author()->associate(app(AuthenticatedContract::class));

        $image->imageable_id   = $parent_id;
        $image->imageable_type = $parentClass;
        $image->title          = '';
        $image->filename       = $file->getClientOriginalName();
        $image->mime           = $file->getMimeType();
        $image->ext            = $file->getClientOriginalExtension();
        $image->size           = $file->getSize();

        $image->save();
        return $image->id;
    }

    /**
     * @param $images
     * @param $validationRules
     * @return bool
     */
    private function validateImages($images, $validationRules)
    {
        $validator = Validator::make($images, $validationRules);
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    /**
     * @param $images
     * @param $validationRules
     * @return mixed
     */
    private function getValidationErrors($images, $validationRules)
    {
        $validator = Validator::make($images, $validationRules);
        return $validator->errors()->getMessages();
    }
}
