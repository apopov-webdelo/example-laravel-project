<?php

namespace App\Http\Controllers\Client\User;


use App\Http\Controllers\Client\ClientBaseController;

/**
 * Global base for all UserGroup controllers
 *
 * Class ClientBaseController
 *
 * @package App\Http\Controllers\Client\User
 */
abstract class UserGroupController extends ClientBaseController
{
}
