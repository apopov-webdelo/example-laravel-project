<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Balance\PayOut\PayOutRegisterServiceContract;
use App\Contracts\Services\Blockchain\To\Transaction\EmergencyWithdrawContract;
use App\Contracts\Services\Blockchain\To\Wallet\AddEmergencyWalletContract;
use App\Contracts\Services\Blockchain\To\Wallet\CreateEmergencyWalletContract;
use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Exceptions\Balance\PayOut\PayOutException;
use App\Exceptions\Balance\PayOut\TooSmallAmountForPayOutException;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Http\Requests\Client\Balance\AmountRequest;
use App\Http\Requests\Client\Balance\BalancesListRequest;
use App\Http\Requests\Client\Balance\Emergency\AddWalletRequest;
use App\Http\Requests\Client\Balance\Emergency\CreateWalletRequest;
use App\Http\Requests\Client\Balance\Emergency\WithdrawRequest as EmergencyWithdrawRequest;
use App\Http\Requests\Client\Balance\TransactionsRequest;
use App\Http\Requests\Client\Balance\WithdrawRequest;
use App\Http\Resources\Client\Balance\BalanceCollection;
use App\Http\Resources\Client\Balance\BalanceResource;
use App\Http\Resources\Client\Balance\CommissionConditionsResource;
use App\Http\Resources\Client\Balance\EmergencyWalletResource;
use App\Http\Resources\Client\Balance\PayOutResource;
use App\Http\Resources\Client\Balance\TransactionCollection;
use App\Http\Resources\Client\Balance\WalletResource;
use App\Http\Rules\CheckGoogle2Fa;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Balance\TransactionRepo;
use App\Repositories\Directory\CryptoCurrencyRepo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class BalanceController
 *
 * @package  App\Http\Controllers\Client\User
 *
 * @resource Balance
 */
class BalanceController extends UserGroupController
{
    /**
     * Get All Balances
     *
     * @response {
     *   balance: [ :: Balance ]
     * }
     */
    public function index(BalancesListRequest $request, AuthenticatedContract $user)
    {
        return app(BalanceCollection::class, ['resource' => $user->balance()->paginate(25)]);
    }

    /**
     * Get Balance
     *
     * @param string                $cryptoCode
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function balanceCrypto(string $cryptoCode, AuthenticatedContract $user)
    {
        try {
            $crypto = app(CryptoCurrencyRepo::class)->getByCode($cryptoCode);

            $balance = $user->getBalance($crypto);
        } catch (\Exception $e) {
            return response()->json(['message' => __('balance.balance.find.not_exists')]);
        }

        return app(BalanceResource::class, ['resource' => $balance]);
    }

    /**
     * Get Wallet
     *
     * Get user's crypto wallet
     *
     * @param string                $cryptoCode
     * @param WalletContract        $service
     *
     * @param AuthenticatedContract $user
     *
     * @return string
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function wallet(string $cryptoCode, WalletContract $service, AuthenticatedContract $user)
    {
        $crypto = app(CryptoCurrencyRepo::class)->getByCode($cryptoCode);

        $balance = $user->getBalance($crypto);

        $this->authorize('wallet', $balance);

        try {
            $wallet = $service->getWallet($user, $balance->cryptoCurrency);
        } catch (StellarException $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return app(WalletResource::class, ['resource' => $wallet]);
    }

    /**
     * Create emergency
     *
     * Create emergency crypto wallet pool for user
     *
     * @param CreateWalletRequest           $request
     * @param CreateEmergencyWalletContract $service
     *
     * @param AuthenticatedContract         $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function createEmergencyWallet(
        CreateWalletRequest $request,
        CreateEmergencyWalletContract $service,
        AuthenticatedContract $user
    ) {
        $this->authorize('createEmergencyWallet', Balance::class);

        $wallet = $service->createEmergencyWallet($user, request('public_key'));

        return app(EmergencyWalletResource::class, ['resource' => $wallet]);
    }

    /**
     * Add emergency
     *
     * Add user's emergency crypto wallet to emergency pool
     *
     * @param AddWalletRequest           $request
     *
     * @param AddEmergencyWalletContract $service
     *
     * @param AuthenticatedContract      $user
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function addEmergencyWallet(
        AddWalletRequest $request,
        AddEmergencyWalletContract $service,
        AuthenticatedContract $user
    ) {
        $this->authorize('addEmergencyWallet', Balance::class);

        $crypto = CryptoCurrency::findOrFail(request('crypto_currency_id'));

        $result = $service->addEmergencyWallet($user, $crypto, request('wallet_id'));

        return response()->json($result)->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Emergency withdraw
     *
     * Emergency withdraw all balances to emergency wallets
     *
     * @param EmergencyWithdrawRequest  $request
     * @param EmergencyWithdrawContract $service
     * @param AuthenticatedContract     $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function emergencyWithdraw(
        EmergencyWithdrawRequest $request,
        EmergencyWithdrawContract $service,
        AuthenticatedContract $user
    ) {
        $this->authorize('emergencyWithdraw', Balance::class);

        $result = $service->emergencyWithdraw($user, request('private_key'));

        return response()->json($result)->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Create PayOut
     *
     * Withdraw crypto to specific wallet
     *
     * @param WithdrawRequest               $request
     * @param PayOutRegisterServiceContract $service
     * @param AuthenticatedContract         $user
     *
     * @return JsonResponse
     * @throws \App\Exceptions\Balance\PayOut\InsufficientBalanceAmountForPayOutException
     * @throws TooSmallAmountForPayOutException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function withdraw(
        WithdrawRequest $request,
        PayOutRegisterServiceContract $service,
        AuthenticatedContract $user
    ) {
        $this->authorize('withdraw', Balance::class);

        if ($user->isGoogle2faActive()) {
            $validator = Validator::make($request->only('google2fa_secret'), [
                'google2fa_secret' => ['required', app(CheckGoogle2Fa::class)],
            ]);
            if ($validator->fails()) {
                return new JsonResponse([
                    'message' => __('validation.user.google2fa_doesnt_match'),
                    'errors'  => $validator->errors(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        try {
            $crypto = CryptoCurrency::findOrFail($request->crypto_currency_id);
            $amount = currencyToCoins($request->amount, $crypto);

            $payOut = $service->commissionIncluded((bool)$request->commissionIncluded)->register(
                $user,
                $amount,
                $crypto,
                $request->wallet_id
            );

            return app(PayOutResource::class, ['resource' => $payOut]);
        } catch (PayOutException $exception) {
            return response(
                ['message' => $exception->getMessage()],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Get Amount
     *
     * Get user's wallet amount in crypto currency from blockchain
     *
     * @param AmountRequest         $request
     * @param WalletContract        $service
     * @param AuthenticatedContract $user
     *
     * @return float
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function amount(AmountRequest $request, WalletContract $service, AuthenticatedContract $user)
    {
        $this->authorize('amount', Balance::class);

        $crypto = CryptoCurrency::findOrFail(request('crypto_currency_id'));

        try {
            return $service->getAmount($user, $crypto);
        } catch (StellarException $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Get Transactions
     *
     * Retrieve transactions list for user balance
     *
     * @response {
     *   transactions: [ :: Transaction ]
     * }
     *
     * @param CryptoCurrency      $cryptoCurrency
     * @param TransactionsRequest $request
     * @param TransactionRepo     $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function transactions(CryptoCurrency $cryptoCurrency, TransactionsRequest $request, TransactionRepo $repo)
    {
        /** @var User $user */
        $user = Auth::user();
        $balance = $user->getBalance($cryptoCurrency);

        $transactions = $repo->filterByBalance($balance)->take()->orderByDesc('id')->paginate(20);

        return app(TransactionCollection::class, ['resource' => $transactions]);
    }

    /**
     * Get Commissions
     *
     * Retrieve current commissions for PayIn & PayOut
     *
     * @response {
     *   commissions: [ :: Commissions ]
     * }
     *
     * @param Request $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function commissions(Request $request)
    {
        return app(CommissionConditionsResource::class, ['resource' => config('app.balance.commissions')]);
    }
}
