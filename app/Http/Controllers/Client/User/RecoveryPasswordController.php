<?php

namespace App\Http\Controllers\Client\User;

use App\Http\Requests\User\PasswordResetRequest;
use App\Http\Requests\User\SendPasswordRecoveryLinkRequest;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\PasswordReset;


/**
 * Class RecoveryPasswordController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Password recovery
 */
class RecoveryPasswordController extends UserGroupController
{
    /**
     * Send reset password link to email
     *
     * @param SendPasswordRecoveryLinkRequest $request
     *
     * @return JsonResponse
     */
    public function sendResetLinkEmail(SendPasswordRecoveryLinkRequest $request)
    {
        $response = $this->broker()->sendResetLink($request->only('email'));

        if ($response == Password::RESET_LINK_SENT) {
            return new JsonResponse(true);
        } else {
            return new JsonResponse([
                'errors' => [
                    'email' => [__($response)]
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Reset password
     *
     * @param PasswordResetRequest $request
     *
     * @return JsonResponse
     */
    public function reset(PasswordResetRequest $request)
    {
        $response = $this->broker()->reset(
            $this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        if ($response == Password::PASSWORD_RESET) {
            return new JsonResponse(true);
        } else {
            return new JsonResponse([
                'errors' => [
                    'email' => [__($response)]
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param PasswordResetRequest $request
     *
     * @return array
     */
    protected function credentials(PasswordResetRequest $request)
    {
        return $request->only('email', 'password', 'password_confirmation', 'token');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword|User  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->setPassword($password);
        $user->invalidateAllTokens();
        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker()
    {
        return Password::broker();
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
