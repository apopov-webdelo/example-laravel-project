<?php

namespace App\Http\Controllers\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Client\User\NoteStoreRequest;
use App\Http\Resources\Client\Note\NoteCollection;
use App\Http\Resources\Client\Note\NoteResource;
use App\Models\User\Note;
use App\Models\User\User;
use App\Repositories\User\NoteRepo;
use App\Services\User\Note\NoteService;
use Illuminate\Http\JsonResponse;


/**
 * Class NoteController
 *
 * @package App\Http\Controllers\Client\User
 *
 * @resource Note
 */
class NoteController extends UserGroupController
{
    /**
     * User's notes
     *
     * Get list of notes by authenticated user
     *
     * @response {
     *   note :: Note
     * }
     */
    public function index(AuthenticatedContract $authUser)
    {
        return app(NoteCollection::class, [
            'resource' => app(NoteRepo::class)->filterByAuthor($authUser)
                                              ->take()
                                              ->with('recipient')
                                              ->paginate(25)
        ]);
    }

    /**
     * Delete one
     *
     * @response {
     *   note :: Note
     * }
     */
    public function destroy(Note $note, AuthenticatedContract $authUser)
    {
        $this->authorize('destroy', $note, $authUser);
        return new JsonResponse($note->delete());
    }

    /**
     * Create/update one
     *
     * @response {
     *   note :: Note
     * }
     */
    public function store(NoteStoreRequest $request, User $user)
    {
        return app(NoteResource::class, ['resource' => app(NoteService::class, ['user' => $user])->store($request->note)]);
    }



    /**
     * Delete one by recipient user
     *
     * @response {
     *   note :: Note
     * }
     */
    public function destroyByUser(User $user)
    {
        /** @var NoteRepo $repo */
        /** @var User $repo */
        $repo     = app(NoteRepo::class);
        $authUser = app(AuthenticatedContract::class);

        $note = $repo->filterByAuthor($authUser)->filterByRecipient($user)->take()->firstOrFail();
        $this->authorize('destroy', $note, $authUser);
        return new JsonResponse($note->delete());
    }
}
