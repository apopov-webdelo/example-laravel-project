<?php

namespace App\Http\Controllers\Client;


/**
 * Class ServiceController
 *
 * @package App\Http\Controllers\Client
 */
class ServiceController extends ClientBaseController
{
    /**
     * Ping - Pong testing
     *
     * Service route for testing server availability.
     * Always return string "pong"
     *
     * @response {
     *   [ pong ]
     * }
     */
    public function ping()
    {
        return response()->json('pong');
    }
}
