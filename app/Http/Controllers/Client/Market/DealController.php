<?php

namespace App\Http\Controllers\Client\Market;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\Deal\CancellationDealServiceContract;
use App\Contracts\Services\Deal\FinishingDealServiceContract;
use App\Exceptions\Deal\DealException;
use App\Http\Requests\Client\Deal\DealCancellationRequest;
use App\Http\Requests\Client\Deal\DealFinishingRequest;
use App\Http\Requests\Client\Deal\DealPaidRequest;
use App\Http\Requests\Client\Deal\DealStoreRequest;
use App\Http\Resources\Client\Dashboard\DealCollection;
use App\Http\Resources\Client\Dashboard\DealResource;
use App\Http\Resources\Client\Market\DealResource as MarketDealResource;
use App\Models\Deal\Deal;
use App\Repositories\Deal\DealRepo;
use App\Services\Deal\AutocancelDealService;
use App\Services\Deal\DisputeDealService;
use App\Services\Deal\PayDealService;
use App\Services\Deal\StoreDealService;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DealController
 *
 * @package  App\Http\Controllers\Client\Market
 *
 * @resource Deal
 */
class DealController extends MarketGroupController
{
    /**
     * Get all
     *
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(AuthenticatedContract $user)
    {
        $this->authorize('index', Deal::class);

        $repo = app(DealRepo::class)
            ->filterByAuthor($user)
            ->take()
            ->paginate(20);

        return app(DealCollection::class, ['resource' => $repo]);
    }

    /**
     * Create one
     *
     * @param DealStoreRequest $request
     * @param StoreDealService $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function store(DealStoreRequest $request, StoreDealService $service)
    {
        try {
            $this->authorize('store', Deal::class);
            $deal = $service->store($request->validated());

            return app(DealResource::class, ['resource' => $deal, 'status_code' => Response::HTTP_CREATED]);
        } catch (DealException $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Retrieve resource instanse
     *
     * @param Deal $deal
     *
     * @return DealResource
     */
    protected function getDealResource(Deal $deal): DealResource
    {
        return app(DealResource::class, ['resource' => $deal]);
    }

    /**
     * Get one
     *
     * @param Deal                  $deal
     * @param AuthenticatedContract $user
     *
     * @return DealResource|\Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function details(Deal $deal, AuthenticatedContract $user)
    {
        $this->authorize('details', $deal);

        return $deal->isDealAuthor($user)
            ? $this->getDealResource($deal)
            : app(MarketDealResource::class, ['resource' => $deal]);
    }

    /**
     * Cancel one
     *
     * @param DealCancellationRequest         $request
     * @param Deal                            $deal
     * @param CancellationDealServiceContract $service
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function cancellation(DealCancellationRequest $request, Deal $deal, CancellationDealServiceContract $service)
    {
        return $this->executeAction($deal, $service, __FUNCTION__);
    }

    /**
     * Execute action using policy and service according method name
     *
     * @param Deal   $deal
     * @param        $service
     * @param string $method
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function executeAction(Deal $deal, $service, string $method)
    {
        try {
            $this->authorize($method, $deal);
            $service->$method($deal);

            return $this->getDealResource($deal);
        } catch (DealException $e) {
            return abort(
                $e->getCode(),
                $e->getMessage()
            );
        }
    }

    /**
     * Autocancel one
     *
     * It's work only for deadlined deal.
     * That is request for immediately handling.
     *
     * @param Deal                  $deal
     * @param AutocancelDealService $service
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function autocancel(Deal $deal, AutocancelDealService $service)
    {
        return $this->executeAction($deal, $service, 'cancel');
    }

    /**
     * Dispute one
     *
     * @param Deal                  $deal
     * @param DisputeDealService    $service
     * @param AuthenticatedContract $user
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function dispute(Deal $deal, DisputeDealService $service, AuthenticatedContract $user)
    {
        return $this->executeAction($deal, $service->setUser($user), __FUNCTION__);
    }

    /**
     * Paid one
     *
     * @param DealPaidRequest $request
     * @param Deal            $deal
     * @param PayDealService  $service
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function paid(DealPaidRequest $request, Deal $deal, PayDealService $service)
    {
        return $this->executeAction($deal, $service, 'pay');
    }

    /**
     * Finish one
     *
     * @param DealFinishingRequest         $request
     * @param Deal                         $deal
     * @param FinishingDealServiceContract $service
     *
     * @return DealResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function finishing(DealFinishingRequest $request, Deal $deal, FinishingDealServiceContract $service)
    {
        return $this->executeAction($deal, $service, __FUNCTION__);
    }
}
