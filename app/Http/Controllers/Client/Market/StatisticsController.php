<?php

namespace App\Http\Controllers\Client\Market;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Statistics\Ads\AdStatisticsContract;
use App\Http\Requests\Stats\AdStatsRequest;
use App\Http\Requests\Stats\DealStatsRequest;
use App\Http\Resources\Client\Stats\AdStatsResource;
use App\Http\Resources\Client\Stats\DealStatsResource;
use App\Models\Ad\Ad;
use App\Services\Statistics\Deals\DealStatisticsResult;
use Illuminate\Support\Carbon;

/**
 * Class StatisticsController
 *
 * @package App\Http\Controllers\Client\Market
 *
 * @resource Statistics
 */
class StatisticsController extends MarketGroupController
{

    /**
     * Deal statistics
     *
     * Returns deal statistics by filter
     *
     * @response {
     *  stats :: DealStatsResource
     * }
     *
     * @param DealStatsRequest      $request
     * @param AuthenticatedContract $user
     *
     * @return array
     */
    public function deals(DealStatsRequest $request, AuthenticatedContract $user)
    {
        /* @var DealStatisticsResult $statsResult */
        $statsResult = app(DealStatisticsResult::class);
        $statsResult->request($request);
        $statsResult->member($user);

        return app(DealStatsResource::class, ['resource' => $statsResult]);
    }


    /**
     * Ad statistics
     *
     * Return statistics for that Ad according filters
     *
     * @response {
     *   stats :: AdStatsResource
     * }
     *
     * @param Ad             $ad
     * @param AdStatsRequest $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function ad(Ad $ad, AdStatsRequest $request)
    {
        $this->authorize('statistics', $ad);

        $from = $request->from ? new Carbon($request->from) : null;
        $to   = $request->to   ? new Carbon($request->to)   : null;

        $service = app(
            AdStatisticsContract::class,
            [
                'ad'   => $ad,
                'from' => $from,
                'to'   => $to,
            ]
        );

        return app(AdStatsResource::class, ['resource' => $service]);
    }
}
