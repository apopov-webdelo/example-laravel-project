<?php

namespace App\Http\Controllers\Client\Market;

use App\Http\Requests\Client\Market\AdFilterRequest;
use App\Http\Resources\BestAdResource;
use App\Http\Resources\Client\Market\AdCollection;
use App\Http\Resources\Client\Market\AdResource;
use App\Models\Ad\Ad;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Ad\AdRepo;


/**
 * Class MarketController
 *
 * @package App\Http\Controllers\Client\Market
 *
 * @resource Market
 */
class MarketController extends MarketGroupController
{
    /**
     * Ad list for sellers
     *
     * @param AdFilterRequest $request
     * @param AdRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function buy(AdFilterRequest $request, AdRepo $repo)
    {
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }
        if ($request->price_from) {
            $repo->filterPriceFrom($request->price_from, $request->currency_id ?? null);
        }
        if ($request->price_to) {
            $repo->filterPriceTo($request->price_to, $request->currency_id ?? null);
        }
        if ($request->min_from) {
            $repo->filterMinFrom($request->min_from, $request->currency_id ?? null);
        }
        if ($request->min_to) {
            $repo->filterMinTo($request->min_to, $request->currency_id ?? null);
        }
        if ($request->max_from) {
            $repo->filterMaxFrom($request->max_from, $request->currency_id ?? null);
        }
        if ($request->max_to) {
            $repo->filterMaxTo($request->max_to, $request->currency_id ?? null);
        }
        if ($request->reputation_from) {
            $repo->filterReputationFrom($request->reputation_from);
        }
        if ($request->reputation_to) {
            $repo->filterReputationTo($request->reputation_to);
        }

        $repo->filterForActive()->filterForBuy();

        return app(AdCollection::class, [
            'resource' => $repo->take()->orderBy('price', 'desc')->paginate($request->per_page ?? 25)
        ]);
    }

    /**
     * Ad list for buyers
     *
     * @param AdFilterRequest $request
     * @param AdRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function sale(AdFilterRequest $request, AdRepo $repo)
    {
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }
        if ($request->price_from) {
            $repo->filterPriceFrom($request->price_from, $request->currency_id ?? null);
        }
        if ($request->price_to) {
            $repo->filterPriceTo($request->price_to, $request->currency_id ?? null);
        }
        if ($request->min_from) {
            $repo->filterMinFrom($request->min_from, $request->currency_id ?? null);
        }
        if ($request->min_to) {
            $repo->filterMinTo($request->min_to, $request->currency_id ?? null);
        }
        if ($request->max_from) {
            $repo->filterMaxFrom($request->max_from, $request->currency_id ?? null);
        }
        if ($request->max_to) {
            $repo->filterMaxTo($request->max_to, $request->currency_id ?? null);
        }
        if ($request->reputation_from) {
            $repo->filterReputationFrom($request->reputation_from);
        }
        if ($request->reputation_to) {
            $repo->filterReputationTo($request->reputation_to);
        }

        $repo->filterForActive()->filterForSale();

        return app(AdCollection::class, [
            'resource' => $repo->take()->orderBy('price', 'asc')->paginate($request->per_page ?? 25)
        ]);
    }

    /**
     * Ad details for market
     *
     * @param Ad $ad
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function details(Ad $ad)
    {
        if (!auth()->user() && !$ad->is_active) {
            abort(404);
        }
        if (auth()->user()) {
            if (!$ad->is_active && $ad->author_id !== auth()->user()->id) {
                abort(404);
            }
        }

        return app(AdResource::class, [ 'resource' => $ad ]);
    }

    /**
     * @param AdFilterRequest $request
     * @param AdRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function bestSale(AdFilterRequest $request, AdRepo $repo)
    {
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }

        return app(BestAdResource::class, [
            'resource' => $repo->filterForActive()->filterForSale()->orderBestSale()->take()
        ]);
    }

    /**
     * @param AdFilterRequest $request
     * @param AdRepo          $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function bestBuy(AdFilterRequest $request, AdRepo $repo)
    {
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }

        return app(BestAdResource::class, [
            'resource' => $repo->filterForActive()->filterForBuy()->orderBestBuy()->take()
        ]);
    }
}
