<?php

namespace App\Http\Controllers\Client\Market;

use App\Contracts\AuthenticatedContract;
use App\Exceptions\Ad\AdException;
use App\Http\Requests\Client\Ad\AdFilterRequest;
use App\Http\Requests\Client\Ad\AdPriceUpdateRequest;
use App\Http\Requests\Client\Ad\AdStoreRequest;
use App\Http\Requests\Client\Ad\AdUpdateRequest;
use App\Http\Resources\Client\Ad\AdCollection;
use App\Http\Resources\Client\Ad\AdResource;
use App\Http\Resources\Client\Market\DealCollection;
use App\Models\Ad\Ad;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Ad\AdRepo;
use App\Repositories\Deal\DealRepo;
use App\Services\Ad\ActivateAdService;
use App\Services\Ad\DeactivateAdService;
use App\Services\Ad\RemoveAdService;
use App\Services\Ad\StoreAdService;
use App\Services\Ad\UpdateAdService;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdController
 *
 * @package App\Http\Controllers\Client\Market
 *
 * @resource Ad
 */
class AdController extends MarketGroupController
{
    /**
     * Get all
     *
     * Paginated
     *
     * @param AdFilterRequest       $request
     * @param AdRepo                $repo
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index(AdFilterRequest $request, AdRepo $repo, AuthenticatedContract $user)
    {
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }
        if ($request->price_from) {
            $repo->filterPriceFrom($request->price_from, $request->currency_id ?? null);
        }
        if ($request->price_to) {
            $repo->filterPriceTo($request->price_to, $request->currency_id ?? null);
        }
        if ($request->min_from) {
            $repo->filterMinFrom($request->min_from, $request->currency_id ?? null);
        }
        if ($request->min_to) {
            $repo->filterMinTo($request->min_to, $request->currency_id ?? null);
        }
        if ($request->max_from) {
            $repo->filterMaxFrom($request->max_from, $request->currency_id ?? null);
        }
        if ($request->max_to) {
            $repo->filterMaxTo($request->max_to, $request->currency_id ?? null);
        }
        if ($request->review_rate_from) {
            $repo->filterReputationFrom($request->review_rate_from);
        }
        if ($request->review_rate_to) {
            $repo->filterReputationTo($request->review_rate_to);
        }
        if ($request->for_sale) {
            $repo->filterForSale();
        }
        if ($request->for_buy) {
            $repo->filterForBuy();
        }
        if ($request->is_active) {
            $repo->filterForActive();
        }
        if ($request->is_blocked) {
            $repo->filterForBlocked();
        }
        if ($request->tor_denied) {
            $repo->filterForTorDenied();
        }
        if ($request->turnover_from) {
            $repo->filterByTurnoverFrom($request->turnover_from);
        }
        if ($request->turnover_to) {
            $repo->filterByTurnoverTo($request->turnover_to);
        }
        if ($request->time_from) {
            $repo->filterByTimeFrom($request->time_from);
        }
        if ($request->time_to) {
            $repo->filterByTimeTo($request->time_to);
        }
        if ($request->min_deal_finished_count_from) {
            $repo->filterByMinDealFinishedFrom($request->min_deal_finished_count_from);
        }
        if ($request->min_deal_finished_count_to) {
            $repo->filterByMinDealFinishedTo($request->min_deal_finished_count_to);
        }

        return app(AdCollection::class, [
            'resource' => $repo->filterByUser($user)->take()->paginate(20)
        ]);
    }

    /**
     * Create one
     *
     * @param AdStoreRequest $request
     * @param StoreAdService $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \App\Exceptions\AccessException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \ReflectionException
     */
    public function store(AdStoreRequest $request, StoreAdService $service)
    {
        $this->authorize('store', Ad::class);

        /** @var Ad $ad */
        $ad = $service->store($request->validated());

        return app(AdResource::class, ['resource' => $ad, 'status_code' => Response::HTTP_CREATED]);
    }

    /**
     * Execute controller action using service
     *
     * @param Ad     $ad
     * @param        $service
     * @param string $method
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function executeAction(Ad $ad, $service, string $method)
    {
        try {
            $this->authorize($method, $ad);
            $ad = $service->$method($ad);

            return $this->getAdResource($ad);
        } catch (AdException $exception) {
            abort($exception->getCode(), $exception->getMessage());
        }
    }

    /**
     * Return instanced Ad resource object
     *
     * @param Ad $ad
     * @return AdResource
     */
    protected function getAdResource(Ad $ad) : AdResource
    {
        return app(AdResource::class, ['resource' => $ad]);
    }

    /**
     * Update one
     *
     * You could send all or some fields you need to update.
     * For example for price updating you could send only 'price'=NEW_PRICE_VALUE.
     *
     * @response {
     *   ad :: Ad
     * }
     *
     * @response {
     *   message: 'You do not have access to this advertisement'
     * }
     *
     * @param AdUpdateRequest $request
     * @param Ad              $ad
     * @param UpdateAdService $service
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function update(AdUpdateRequest $request, Ad $ad, UpdateAdService $service)
    {
        $service->setData($ad, $request->validated());
        return $this->executeAction($ad, $service, __FUNCTION__);
    }

    /**
     * Update price
     *
     * That method update only price field and fire that event for socket channels.
     * For example for price updating you could send only 'price'=NEW_PRICE_VALUE.
     *
     * @param AdPriceUpdateRequest $request
     * @param Ad                   $ad
     * @param UpdateAdService      $service
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function updatePrice(AdPriceUpdateRequest $request, Ad $ad, UpdateAdService $service)
    {
        $service->setData($ad, $request->validated());
        return $this->executeAction($ad, $service, 'update');
    }

    /**
     * Delete one
     *
     * @param Ad              $ad
     * @param RemoveAdService $service
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Ad $ad, RemoveAdService $service)
    {
        return $this->executeAction($ad, $service, __FUNCTION__);
    }

    /**
     * Get one
     *
     * @param Ad $ad
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function details(Ad $ad)
    {
        $this->authorize('details', $ad);

        return $this->getAdResource($ad);
    }

    /**
     * Deals for exact ad
     *
     * @param Ad       $ad
     * @param DealRepo $repo
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deals(Ad $ad, DealRepo $repo)
    {
        $this->authorize('deals', $ad);

        return app(DealCollection::class, [ 'resource' => $repo->filterByAd($ad)->take()->paginate(10) ]);
    }

    /**
     * Activate Ad
     *
     * @param Ad                $ad
     * @param ActivateAdService $service
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function activate(Ad $ad, ActivateAdService $service)
    {
        return $this->executeAction($ad, $service, __FUNCTION__);
    }

    /**
     * Deactivate Ad
     *
     * @param Ad                  $ad
     * @param DeactivateAdService $service
     *
     * @return AdResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deactivate(Ad $ad, DeactivateAdService $service)
    {
        return $this->executeAction($ad, $service, __FUNCTION__);
    }
}
