<?php

namespace App\Http\Controllers\Client\Market;

use App\Http\Controllers\Client\ClientBaseController;


/**
 * Global base for all MarketGroup controllers
 *
 * Class ClientBaseController
 *
 * @package App\Http\Controllers\Client\Market
 */
abstract class MarketGroupController extends ClientBaseController
{
}
