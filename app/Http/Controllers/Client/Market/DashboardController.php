<?php

namespace App\Http\Controllers\Client\Market;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Client\Ad\AdFilterRequest;
use App\Http\Requests\Client\Deal\DealDashboardRequest;
use App\Http\Resources\Client\Dashboard\AdCollection;
use App\Http\Resources\Client\Dashboard\DealCollection;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Ad\AdRepo;
use App\Repositories\Deal\DealRepo;


/**
 * Class DashboardController
 *
 * @package App\Http\Controllers\Client\Market
 *
 * @resource Dashboard
 */
class DashboardController extends MarketGroupController
{
    /**
     * Ad list for dashboard
     *
     * Paginated
     *
     * @response {
     *   data: [ ad :: AdResource ]
     *   current_page: number,
     *   total: number
     *   last_page: number
     * }
     */
    public function ads(AdFilterRequest $request, AdRepo $repo, AuthenticatedContract $user)
    {
        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency(CryptoCurrency::find($request->crypto_currency_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->bank_id) {
            $repo->filterByBank(Bank::find($request->bank_id));
        }
        if ($request->price_from) {
            $repo->filterPriceFrom($request->price_from, $request->currency_id ?? null);
        }
        if ($request->price_to) {
            $repo->filterPriceTo($request->price_to, $request->currency_id ?? null);
        }
        if ($request->min_from) {
            $repo->filterMinFrom($request->min_from, $request->currency_id ?? null);
        }
        if ($request->min_to) {
            $repo->filterMinTo($request->min_to, $request->currency_id ?? null);
        }
        if ($request->max_from) {
            $repo->filterMaxFrom($request->max_from, $request->currency_id ?? null);
        }
        if ($request->max_to) {
            $repo->filterMaxTo($request->max_to, $request->currency_id ?? null);
        }
        if ($request->review_rate_from) {
            $repo->filterReviewRateFrom($request->review_rate_from);
        }
        if ($request->review_rate_to) {
            $repo->filterReviewRateTo($request->review_rate_to);
        }
        if ($request->for_sale) {
            $repo->filterForSale();
        }
        if ($request->for_buy) {
            $repo->filterForBuy();
        }
        if ($request->is_active) {
            $repo->filterForActive();
        }
        if ($request->is_blocked) {
            $repo->filterForBlocked();
        }
        if ($request->tor_denied) {
            $repo->filterForTorDenied();
        }
        if ($request->turnover_from) {
            $repo->filterByTurnoverFrom($request->turnover_from);
        }
        if ($request->turnover_to) {
            $repo->filterByTurnoverTo($request->turnover_to);
        }
        if ($request->time_from) {
            $repo->filterByTimeFrom($request->time_from);
        }
        if ($request->time_to) {
            $repo->filterByTimeTo($request->time_to);
        }
        if ($request->min_deal_finished_count_from) {
            $repo->filterByMinDealFinishedFrom($request->min_deal_finished_count_from);
        }
        if ($request->min_deal_finished_count_to) {
            $repo->filterByMinDealFinishedTo($request->min_deal_finished_count_to);
        }

        return app(AdCollection::class, [
            'resource' => $repo->filterByUser($user)->take()->orderByDesc('created_at')->paginate(20)
        ]);
    }

    /**
     * Deal list for dashboard
     *
     * Return deal list for authenticated user according operation type (sale or buy)
     *
     * @response {
     *   data: [ deals :: DealResponse ],
     *   current_page: number,
     *   total: number
     *   last_page: number
     * }
     */
    public function deals(DealDashboardRequest $request, AuthenticatedContract $user, DealRepo $repo)
    {
        if (!$request->for_sale || !$request->for_buy) {
            if ($request->for_sale) {
                $repo->filterForSale($user);
            }
            if ($request->for_buy) {
                $repo->filterForBuy($user);
            }
        }

        $request->is_active
            ? $repo->filterByStatusId(DealStatusConstants::ACTIVE_STATUSES)
            : $repo->filterByStatusId(DealStatusConstants::INACTIVE_STATUSES);

        if ($request->crypto_currency_id) {
            $repo->filterByCryptoCurrency($request->crypto_currency_id);
        }

        if ($request->currency_id) {
            $repo->filterByCurrency($request->currency_id);
        }

        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem($request->payment_system_id);
        }

        if ($request->closed) {
            $repo->filterByStatusId(DealStatusConstants::FINISHED);
        }

        if ($request->canceled) {
            $repo->filterByStatusId(DealStatusConstants::CANCELED);
        }

        return app(
            DealCollection::class,
            ['resource' => $repo->filterByDealMember($user)->take()->orderByDesc('created_at')->paginate(20)]
        );
    }
}
