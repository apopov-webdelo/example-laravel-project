<?php

namespace App\Http\Controllers\Client\Market;

use App\Contracts\AuthenticatedContract;
use App\Exceptions\Review\ReviewException;
use App\Http\Requests\Client\Review\ReviewStoreRequest;
use App\Http\Resources\Client\Market\ReviewResource;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Repositories\Deal\DealRepo;
use App\Repositories\User\UserRepo;
use App\Services\Review\ReviewService;

/**
 * Class ReviewController
 *
 * @package  App\Http\Controllers\Client\Market
 *
 * @resource Review
 */
class ReviewController extends MarketGroupController
{
    /**
     * Create/Update by deal
     *
     * Store new deal or update existing deal for that user (doesn't matter what deal has used for existing review)
     *
     * @param ReviewStoreRequest $request
     * @param Deal               $deal
     * @param ReviewService      $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \ReflectionException
     */
    public function store(ReviewStoreRequest $request, Deal $deal, ReviewService $service)
    {
        $this->authorize('storeReview', $deal);

        try {
            $review = $service->store($deal, $request->validated());

            return $response = app(ReviewResource::class, ['resource' => $review]);
        } catch (ReviewException $exception) {
            return abort($exception->getCode(), $exception->getMessage());
        }
    }

    /**
     * Create/Update by login
     *
     * @param ReviewStoreRequest    $request
     * @param string                $login
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \ReflectionException
     */
    public function storeForLogin(ReviewStoreRequest $request, string $login, AuthenticatedContract $user)
    {
        /* @var AuthenticatedContract $targetUser */
        $targetUser = app(UserRepo::class)->filterByLogin($login)
                                          ->take()
                                          ->first();

        if (!$targetUser) {
            return response()->json(['message' => __('auth.not_found')]);
        }

        $deal = app(DealRepo::class)->filterByStatusId(DealStatusConstants::INACTIVE_STATUSES)
                                    ->filterByDealMembersCross($user, $targetUser)
                                    ->take()
                                    ->latest()
                                    ->first();

        return $deal
            ? $this->store($request, $deal, app(ReviewService::class))
            : response()->json(['message' => __('deal.error.not_found_closed_deal')]);
    }
}
