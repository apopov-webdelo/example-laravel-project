<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Base\BaseController;

/**
 * Global base for all Client controllers
 *
 * Class ClientBaseController
 *
 * @package App\Http\Controllers\Client
 */
abstract class ClientBaseController extends BaseController
{
}
