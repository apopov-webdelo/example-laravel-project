<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\User\RegisterRequest;
use App\Http\Resources\Client\User\SignInResource;
use App\Jobs\Partnership\ReferralRegisterJob;
use App\Models\Partnership\Partner;
use App\Models\User\User;
use App\Services\User\RegistrationService;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Log;

/**
 * @resource Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Sign up
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Exception
     */
    public function register(RegisterRequest $request)
    {
        /* @var User $user */
        $user = app(RegistrationService::class)->store($request->all());

        $this->guard()->login($user);

        $token = $user->generateApiToken();

        if ($user && $request->partner_id) {
            if ($partner = Partner::find($request->partner_id)) {
                ReferralRegisterJob::dispatch($user, $partner);
            } else {
                Log::channel('partnership')
                    ->emergency(
                        "Referral tried to register 
                        with partner code {$request->partner_id}.
                        But this partner is not exists."
                    );
            }
        }

        return app(SignInResource::class, ['resource' => $user, 'token' => $token]);
    }
}
