<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/9/18
 * Time: 17:13
 */

namespace App\Http\Controllers\Client;

use App\Contracts\AuthenticatedContract;
use App\Facades\Robot;
use App\Http\Resources\Client\User\SignInResource;
use App\Models\User\User;
use Illuminate\Http\Response;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers\Client
 *
 * @resource Auth
 */
class LoginController extends \App\Http\Controllers\Auth\LoginController
{
    /**
     * @param string $login
     * @param string $password
     *
     * @return bool
     */
    public function attemptByCredentials(string $login, string $password): bool
    {
        $user = User::where('login', $login)
                    ->orWhere('email', $login)
                    ->first();

        if ($user) {
            if (Robot::user()->id === $user->id) {
                abort(Response::HTTP_FORBIDDEN, 'This user does not have access through sign in');
            }

            if (password_verify($password, $user->password)) {
                auth()->login($user);
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $login
     *
     * @return mixed
     */
    protected function getUserByLogin(string $login)
    {
        $user = User::where('login', $login)
                     ->orWhere('email', $login)
                     ->first();

        return $user;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @param string                $token
     *
     * @return SignInResource
     */
    protected function loginSuccessResponse(AuthenticatedContract $user, string $token)
    {
        return app(SignInResource::class, ['resource' => $user, 'token' => $token]);
    }
}
