<?php

namespace App\Http\Controllers\Client\Directory;

use App\Repositories\Directory\CachedFullDirectoryRepo;

/**
 * Class DirectoryController
 *
 * @package  App\Http\Controllers\Client\Directory
 *
 * @resource Directory
 */
class FullDirectoryController extends DirectoryGroupController
{
    /**
     * Api List all directories: crypto_currencies, countries => currency (main), currencies, banks, payment_systems
     *
     * @param CachedFullDirectoryRepo $repo
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(CachedFullDirectoryRepo $repo)
    {
        return response()->json($repo->all());
    }

    /**
     * View, List all directories: crypto_currencies, countries => currency (main), currencies, banks, payment_systems
     *
     * @hideFromAPIDocumentation
     *
     * @param CachedFullDirectoryRepo $repo
     *
     * @return \Illuminate\Http\Response
     */
    protected function all(CachedFullDirectoryRepo $repo)
    {
        $directory = $repo->all();

        return response()->view('directory.all', compact('directory'))
                         ->header("Content-Type", "application/javascript; charset=utf-8")
                         ->header("Cache-Control", "public, max-age=86400");
    }
}
