<?php

namespace App\Http\Controllers\Client\Directory;

use App\Http\Resources\Client\Directory\CryptoCurrency\CryptoCurrencyCollection;
use App\Repositories\Directory\CryptoCurrencyRepo;


/**
 * Class CryptoCurrencyController
 *
 * @package App\Http\Controllers\Client\Directory
 *
 * @resource Directory
 */
class CryptoCurrencyController extends DirectoryGroupController
{
    /**
     * Get crypto currencies list
     *
     * @response {
     *   cryptoCurrency: [ :: CryptoCurrency ]
     * }
     */
    public function index(CryptoCurrencyRepo $repo)
    {
        return app(CryptoCurrencyCollection::class, ['resource' => $repo->take()->paginate(25)]);
    }
}
