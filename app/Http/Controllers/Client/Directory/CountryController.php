<?php

namespace App\Http\Controllers\Client\Directory;

use App\Http\Requests\Client\Directory\CountryFilterRequest;
use App\Http\Resources\Client\Directory\Country\CountryCollection;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Directory\CountryRepo;


/**
 * Class CountryController
 *
 * @package App\Http\Controllers\Client\Directory
 *
 * @resource Directory
 */
class CountryController extends DirectoryGroupController
{
    /**
     * Get countries list
     *
     * @response {
     *   country: [ :: Country ]
     * }
     */
    public function index(CountryFilterRequest $request, CountryRepo $repo)
    {
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }

        return app(CountryCollection::class, ['resource' => $repo->take()->paginate(25)]);
    }
}
