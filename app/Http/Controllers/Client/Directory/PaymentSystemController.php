<?php

namespace App\Http\Controllers\Client\Directory;

use App\Http\Requests\Client\Directory\PaymentSystemFilterRequest;
use App\Http\Resources\Client\Directory\PaymentSystem\PaymentSystemCollection;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Repositories\Directory\PaymentSystemRepo;


/**
 * Class PaymentSystemController
 *
 * @package App\Http\Controllers\Client\Directory
 *
 * @resource Directory
 */
class PaymentSystemController extends DirectoryGroupController
{
    /**
     * Get payment systems list
     *
     * @response {
     *   paymentSystem: [ :: PaymentSystem ]
     * }
     */
    public function index(PaymentSystemFilterRequest $request, PaymentSystemRepo $repo)
    {
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }

        return app(PaymentSystemCollection::class, ['resource' => $repo->take()->paginate(25)]);
    }
}
