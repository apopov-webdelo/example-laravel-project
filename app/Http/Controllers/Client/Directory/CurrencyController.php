<?php

namespace App\Http\Controllers\Client\Directory;

use App\Http\Requests\Client\Directory\CurrencyFilterRequest;
use App\Http\Resources\Client\Directory\Currency\CurrencyCollection;
use App\Models\Directory\Country;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Directory\CurrencyRepo;


/**
 * Class CurrencyController
 *
 * @package App\Http\Controllers\Client\Directory
 *
 * @resource Directory
 */
class CurrencyController extends DirectoryGroupController
{
    /**
     * Get currencies list
     *
     * @response {
     *   currency: [ :: Fiat ]
     * }
     */
    public function index(CurrencyFilterRequest $request, CurrencyRepo $repo)
    {
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->payment_system_id) {
            $repo->filterByPaymentSystem(PaymentSystem::find($request->payment_system_id));
        }
        if ($request->filter) {
            $repo->search($request->filter);
        }

        return app(CurrencyCollection::class, ['resource' => $repo->take()->paginate(25)]);
    }
}
