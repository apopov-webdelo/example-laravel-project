<?php

namespace App\Http\Controllers\Client\Directory;

use App\Http\Controllers\Client\ClientBaseController;

/**
 * Global base for all DirectoryGroup controllers
 *
 * Class ClientBaseController
 *
 * @package App\Http\Controllers\Client\Directory
 */
abstract class DirectoryGroupController extends ClientBaseController
{
}
