<?php

namespace App\Http\Controllers\Client\Directory;

use App\Http\Requests\Client\Directory\BankFilterRequest;
use App\Http\Resources\Client\Directory\Bank\BankCollection;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Repositories\Directory\BankRepo;


/**
 * Class BankController
 *
 * @package App\Http\Controllers\Client\Directory
 *
 * @resource Directory
 */
class BankController extends DirectoryGroupController
{
    /**
     * Get bank list
     *
     * @response {
     *   bank: [ :: Bank ]
     * }
     */
    public function index(BankFilterRequest $request, BankRepo $repo)
    {
        if ($request->country_id) {
            $repo->filterByCountry(Country::find($request->country_id));
        }
        if ($request->currency_id) {
            $repo->filterByCurrency(Currency::find($request->currency_id));
        }

        return app(BankCollection::class, ['resource' => $repo->take()->paginate(25)]);
    }
}
