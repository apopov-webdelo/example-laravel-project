<?php

namespace App\Http\Controllers\Partnership;

use App\Exceptions\Partnership\NotFoundPartnerPartnershipException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partnership\PartnerResource;
use Illuminate\Http\Response;

/**
 * Class ProfileController
 *
 * @package App\Http\Controllers\Partnership
 * @resource Partnership
 */
class ProfileController extends Controller
{
    /**
     * Get partner profile
     *
     * Get profile for partner using authorized user
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function index()
    {
        try {
            return app(PartnerResource::class, ['resource' => get_partner(auth()->user())]);
        } catch (NotFoundPartnerPartnershipException $exception) {
            abort(Response::HTTP_NO_CONTENT, $exception->getMessage());
        }
    }
}
