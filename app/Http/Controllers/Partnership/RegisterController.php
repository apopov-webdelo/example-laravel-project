<?php

namespace App\Http\Controllers\Partnership;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Partnership\PartnerRegisterContract;
use App\Exceptions\Partnership\PartnerAlreadyRegisteredException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\User\UserResource;
use App\Http\Resources\Partnership\PartnerResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * @resource Partnership
 */
class RegisterController extends Controller
{

    /**
     * Sign up partner
     *
     * Sign up as partner for authenticated user
     *
     * @param AuthenticatedContract $user
     * @param PartnerRegisterContract $service
     *
     * @return \Illuminate\Foundation\Application|mixed
     * @throws \Exception
     */
    public function register(AuthenticatedContract $user, PartnerRegisterContract $service)
    {
        try {
            if ($service->register($user)) {
                Log::channel('partnership')->info("User {$user->login} was registered as partner successfully");
            } else {
                Log::channel('partnership')->debug("User {$user->login} wasn't registered as partner");
            }
        } catch (PartnerAlreadyRegisteredException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return app(PartnerResource::class, ['resource' => get_partner($user)]);
    }
}
