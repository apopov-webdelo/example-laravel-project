<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Http\Requests\Support\ApiAccessRequest;
use App\Notifications\Support\ApiAccessRequestNotification;
use Illuminate\Support\Facades\Notification;

/**
 * Class SupportController
 *
 * @package   App\Http\Controllers\Support
 *
 * @resourece Support
 */
class SupportController extends Controller
{
    /**
     * Api access request
     *
     * Filled form data is sent over email to risex suppport
     *
     * @param ApiAccessRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function apiAccessRequest(ApiAccessRequest $request)
    {
        Notification::route('mail', config('app.support.mail.api_access_request'))
                    ->notify(new ApiAccessRequestNotification($request));

        return response()->json(true);
    }
}
