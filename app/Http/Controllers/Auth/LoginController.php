<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Utils\Session\SessionStorageContract;
use App\Events\User\LoginError;
use App\Events\User\LoginSuccess;
use App\Events\User\LogoutSuccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\User\CredentialsLoginRequest;
use App\Models\User\Google2faConstants;
use Illuminate\Http\Request;
use Google2FA;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * @resource Auth
 */
abstract class LoginController extends Controller
{
    use ThrottlesLogins;

    private $decayMinutes = 2;

    abstract protected function loginSuccessResponse(AuthenticatedContract $user, string $token);

    abstract public function attemptByCredentials(string $login, string $password): bool;

    abstract protected function getUserByLogin(string $login);

    /**
     * @return string
     */
    public function getGuardName(): string
    {
        return 'web';
    }

    /**
     * Describe the field for error showing
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }

    /**
     * @param CredentialsLoginRequest $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    protected function loginSuccess(CredentialsLoginRequest $request)
    {
        /** @var AuthenticatedContract $user */
        $user = auth()->guard($this->getGuardName())->user();

        if ($user->isBlocked()) {
            return new JsonResponse([
                'message' => __('validation.auth.banned'),
            ], Response::HTTP_FORBIDDEN);
        }

        $token = $user->generateApiToken();

        event(app(LoginSuccess::class, [
            'user'           => $user,
            'sessionStorage' => app(SessionStorageContract::class),
        ]));

        $this->clearLoginAttempts($request);

        return $this->loginSuccessResponse($user, $token);
    }

    /**
     * Sign-in
     *
     * @param CredentialsLoginRequest $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function login(CredentialsLoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if (false === $this->attemptByCredentials($request->login, $request->password)) {
            return $this->loginError($request);
        }

        if ($this->isGoogle2faActive()) {
            if (null === $request->google2fa_secret) {
                return $this->askForGoogle2FaCode();
            }

            if (false === $this->verifyGoogle2FaSecret($request->google2fa_secret)) {
                $this->incrementLoginAttempts($request);
                $user = auth()->guard($this->getGuardName())->user();

                event(app(LoginError::class, [
                    'user'           => $user,
                    'sessionStorage' => app(SessionStorageContract::class),
                ]));

                return new JsonResponse([
                    'message' => __('validation.user.google2fa_doesnt_match'),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        return $this->loginSuccess($request);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return new JsonResponse([
            'message' => __('auth.throttle', ['seconds' => $seconds])
        ], Response::HTTP_TOO_MANY_REQUESTS);
    }

    /**
     * Sign-out
     *
     * @param AuthenticatedContract $user
     *
     * @return JsonResponse
     */
    public function logout(AuthenticatedContract $user)
    {
        $user->invalidateApiToken();

        event(app(LogoutSuccess::class, [
            'user'           => $user,
            'sessionStorage' => app(SessionStorageContract::class),
        ]));

        return new JsonResponse([]);
    }

    /**
     * @param CredentialsLoginRequest $request
     *
     * @return JsonResponse
     */
    private function loginError(CredentialsLoginRequest $request)
    {
        $user = $this->getUserByLogin($request->input('login'));

        if ($user) {
            event(app(LoginError::class, [
                'user'           => $user,
                'sessionStorage' => app(SessionStorageContract::class),
            ]));
        }

        $this->incrementLoginAttempts($request);

        return new JsonResponse([
            'message' => __('validation.auth.wrong_login_password'),
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @return mixed
     */
    private function isGoogle2faActive()
    {
        $user = auth()->guard($this->getGuardName())->user();

        return $user->security->google2fa_status == Google2faConstants::STATUS_ACTIVATED;
    }

    /**
     * @return JsonResponse
     */
    private function askForGoogle2FaCode()
    {
        return new JsonResponse([
            'message' => __('validation.auth.google2fa_required'),
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param $google2fa_secret
     *
     * @return mixed
     */
    private function verifyGoogle2FaSecret($google2fa_secret)
    {
        return Google2FA::verifyKey(auth()->guard($this->getGuardName())->user()->google2fa_secret, $google2fa_secret);
    }
}
