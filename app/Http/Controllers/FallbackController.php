<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FallbackController extends Controller
{
    public function notFound()
    {
        return response()->json(
            ['message' => 'Route Not Found!'],
            \Illuminate\Http\Response::HTTP_NOT_FOUND
        );
    }
}
