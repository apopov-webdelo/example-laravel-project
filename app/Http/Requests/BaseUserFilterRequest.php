<?php

namespace App\Http\Requests;

use App\Models\Role\RoleConstants;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BaseUserFilterRequest
 *
 * @property string $login
 * @property int    $id
 * @property string $email
 * @property string $created_at         in date_format:Y-m-d
 * @property string $last_seen          in date_format:Y-m-d
 * @property string $created_at_from    in date_format:Y-m-d
 * @property string $created_at_to      in date_format:Y-m-d
 * @property string $last_seen_from     in date_format:Y-m-d
 * @property string $last_seen_to       in date_format:Y-m-d
 * @property string $role_id
 * @property int    $per_page
 *
 * @package App\Http\Requests
 */
abstract class BaseUserFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                => 'integer',
            'login'             => 'string',
            'email'             => 'email',
            'created_at'        => 'date_format:Y-m-d',
            'last_seen'         => 'date_format:Y-m-d H:i:s',
            'created_at_from'   => 'date_format:Y-m-d',
            'created_at_to'     => 'date_format:Y-m-d',
            'last_seen_from'    => 'date_format:Y-m-d H:i:s',
            'last_seen_to'      => 'date_format:Y-m-d H:i:s',
            'per_page'          => 'integer|max:1000'
        ];
    }
}
