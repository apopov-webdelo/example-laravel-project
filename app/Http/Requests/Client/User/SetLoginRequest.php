<?php

namespace App\Http\Requests\Client\User;

use App\Contracts\AuthenticatedContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SetLoginRequest
 * @property string $login
 * @package App\Http\Requests\Client\User
 */
class SetLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function authorize(AuthenticatedContract $user)
    {
        return $user->changesAvailable->canSetLogin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => [
                'required',
                'string',
                'max:255',
                'unique:users'
            ],
        ];
    }
}
