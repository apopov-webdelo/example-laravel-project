<?php

namespace App\Http\Requests\Client\User;

use App\Http\Rules\CheckPhoneAccessToken;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PhoneAccessTokenRequest
 *
 * @property-read string $token
 * @package App\Http\Requests\Client\User
 */
class PhoneAccessTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => [
                'string',
                'required',
                app(CheckPhoneAccessToken::class)
            ]
        ];
    }
}
