<?php

namespace App\Http\Requests\Client\User;

use App\Http\Rules\CheckPassword;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PasswordUpdateRequest
 * @property string password
 * @property string new_password
 * @property string new_password_confirmation
 * @package App\Http\Requests\Client\User
 */
class PasswordUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => [
                'string',
                'required',
                app(CheckPassword::class)
            ],
            'new_password' => [
                'required',
                'string',
                'confirmed',
                'min:7',
                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$/',
            ],
            'new_password_confirmation' => [
                'required',
                'string',
                'same:new_password'
            ]
        ];
    }
}
