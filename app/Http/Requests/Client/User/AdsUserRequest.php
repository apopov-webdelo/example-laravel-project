<?php

namespace App\Http\Requests\Client\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdsUserRequest for user ads filtering
 *
 * @property-read bool $for_sale
 * @property-read bool $for_buy
 *
 * @package App\Http\Requests\Client\User
 */
class AdsUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'for_sale' => 'boolean',
            'for_buy'  => 'boolean',
        ];
    }
}
