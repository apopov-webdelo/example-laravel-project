<?php

namespace App\Http\Requests\Client\User;

use App\Contracts\AuthenticatedContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SetEmailRequest
 * @property string $email
 * @package App\Http\Requests\Client\User
 */
class SetEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param AuthenticatedContract $user
     *
     * @return bool
     */
    public function authorize(AuthenticatedContract $user)
    {
        return $user->changesAvailable->canSetEmail();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users'
            ],
        ];
    }
}
