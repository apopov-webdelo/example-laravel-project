<?php

namespace App\Http\Requests\Client\User;

use App\Http\Rules\CheckEmailConfirmationToken;
use Illuminate\Foundation\Http\FormRequest;

class EmailConfirmTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => [
                'string',
                'required',
                app(CheckEmailConfirmationToken::class)
            ]
        ];
    }
}
