<?php

namespace App\Http\Requests\Client\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request class for user deals filtering
 *
 * @property int $for_sale Return deals where authorized user sale crypto
 * @property int $for_buy Return deals where authorized user buy crypto
 * @property int $is_active Return active deals only (In Progress, Paid, Disputed)
 * @property int $is_blocked Return Inactive deals only (In Progress, Paid, Disputed)
 *
 * @package App\Http\Requests\Client\Ad
 */
class DealsUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'for_sale'      => 'boolean',
            'for_buy'       => 'boolean',
            'is_active'     => 'boolean',
            'is_blocked'    => 'boolean',
        ];
    }
}
