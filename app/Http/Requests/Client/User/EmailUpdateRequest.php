<?php

namespace App\Http\Requests\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Http\Rules\CheckPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmailUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'string',
                'required',
                'email',
                Rule::unique('users')->ignore(app(AuthenticatedContract::class)->id)
            ],
            'password' => [
                'string',
                'required',
                app(CheckPassword::class)
            ]
        ];
    }
}
