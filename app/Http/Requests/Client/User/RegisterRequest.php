<?php

namespace App\Http\Requests\Client\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegisterRequest
 *
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $password_confirmation
 * @property string $partner_id
 *
 * @package App\Http\Requests\Client\User
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => [
                'required',
                'string',
                'max:255',
                'unique:users'
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users'
            ],
            'password' => [
                'required',
                'string',
                'confirmed',
                'min:7',
                'max:45',
                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$/',
            ],
            'password_confirmation' => [
                'required',
                'string',
                'same:password'
            ],
            'partner_id' => 'nullable|integer|exists:partners,id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login.required' => __('validation.custom_errors.login.required'),
            'login.string' => __('validation.custom_errors.login.string'),
            'login.max' => __('validation.custom_errors.login.max'),
            'login.unique' => __('validation.custom_errors.login.unique'),

            'email.required' => __('validation.custom_errors.email.required'),
            'email.email' => __('validation.custom_errors.email.email'),
            'email.string' => __('validation.custom_errors.email.string'),
            'email.max' => __('validation.custom_errors.email.max'),
            'email.unique' => __('validation.custom_errors.email.unique'),

            'password.required' => __('validation.custom_errors.password.required'),
            'password.string' => __('validation.custom_errors.password.string'),
            'password.confirmed' => __('validation.custom_errors.password.confirmed'),
            'password.min' => __('validation.custom_errors.password.min'),
            'password.max' => __('validation.custom_errors.password.max'),
            'password.regex' => __('validation.custom_errors.password.regex'),

            'password_confirmation.required' => __('validation.custom_errors.password_confirmation.required'),
            'password_confirmation.string' => __('validation.custom_errors.password_confirmation.string'),
            'password_confirmation.same' => __('validation.custom_errors.password_confirmation.same'),
        ];
    }
}
