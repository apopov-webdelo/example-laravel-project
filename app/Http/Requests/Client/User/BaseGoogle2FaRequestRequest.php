<?php

namespace App\Http\Requests\Client\User;

use App\Http\Rules\CheckGoogle2Fa;
use Illuminate\Foundation\Http\FormRequest;

class BaseGoogle2FaRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'google2fa_secret' => [
                'string',
                'required',
                app(CheckGoogle2Fa::class)
            ]
        ];
    }
}
