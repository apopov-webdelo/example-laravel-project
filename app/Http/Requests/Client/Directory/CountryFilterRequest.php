<?php

namespace App\Http\Requests\Client\Directory;

/**
 * Class CountryFilterRequest
 *
 * @property int $currency_id
 * @property int $payment_system_id
 *
 * @package App\Http\Requests\Client\Directory
 */
class CountryFilterRequest extends BaseFilterRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency_id'       => 'integer|exists:currencies,id',
            'payment_system_id' => 'integer|exists:payment_systems,id'
        ];
    }
}
