<?php

namespace App\Http\Requests\Client\Directory;

/**
 * Class CurrencyFilterRequest
 *
 * @property string $filter
 * @property int $country_id
 * @property int $payment_system_id
 * @property string $exclude
 *
 * @package App\Http\Requests\Client\Directory
 */
class CurrencyFilterRequest extends BaseFilterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'        => 'integer|exists:countries,id',
            'payment_system_id' => 'integer|exists:payment_systems,id'
        ];
    }
}
