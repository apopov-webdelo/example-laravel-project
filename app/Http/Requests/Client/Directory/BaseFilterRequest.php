<?php

namespace App\Http\Requests\Client\Directory;

use Illuminate\Foundation\Http\FormRequest;

class BaseFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency_id' => 'integer|exists:currencies,id',
            'country_id'  => 'integer|exists:countries,id',
            'per_page'    => 'integer|max:100'
        ];
    }
}
