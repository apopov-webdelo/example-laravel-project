<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class IdeaStoreRequest
 *
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $text
 *
 * @package App\Http\Requests\Client
 */
class IdeaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string',
            'login' => 'required|string',
            'email' => 'required|string|email',
            'text'  => 'required|string',
        ];
    }
}
