<?php

namespace App\Http\Requests\Client\Deal;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request class for dashboard deals filtering
 *
 * @property int $for_sale Will return deals for sale
 * @property int $for_buy Will return deals for buy
 * @property int $is_active Will return active deals only
 * @property int $currency_id
 * @property int $crypto_currency_id
 * @property int $payment_system_id
 * @property bool $canceled
 * @property bool $closed
 *
 * @package App\Http\Requests\Client\Ad
 */
class DealDashboardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'for_sale'  => 'boolean',
            'for_buy'   => 'boolean',
            'is_active' => 'boolean',
            'currency_id'        => [
                'integer',
                'exists:currencies,id',
            ],
            'crypto_currency_id' => [
                'integer',
                'exists:crypto_currencies,id',
            ],
            'payment_system_id' => [
                'integer',
                'exists:payment_systems,id',
            ],
            'canceled' => 'boolean',
            'closed' => 'boolean',
        ];
    }
}
