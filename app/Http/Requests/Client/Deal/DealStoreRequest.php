<?php

namespace App\Http\Requests\Client\Deal;

use App\Http\Rules\AdMinDealFinishedCountRule;
use App\Http\Rules\CheckAdIsProtected;
use App\Http\Rules\CheckAmountRange;
use App\Http\Rules\CheckBlacklist;
use App\Http\Rules\CheckCryptoAmountMinLimit;
use App\Http\Rules\CheckDealerBalance;
use App\Http\Rules\CheckDealerReputationRate;
use App\Http\Rules\CheckEmailConfirmation;
use App\Http\Rules\CheckPhoneConfirmation;
use App\Http\Rules\CheckPrice;
use App\Http\Rules\CheckTrust;
use App\Http\Rules\CheckTurnOver;
use App\Http\Rules\DealCancellationPercentRule;
use App\Http\Rules\IsAdActive;
use App\Http\Rules\IsAuthorNotOwner;
use App\Http\Rules\IsBalanceMoreThenMinRequired;
use App\Http\Rules\TorDeniedRule;
use App\Models\Ad\Ad;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DealStoreRequest
 *
 * @property string $public_key
 * @property int    $ad_id
 * @property float  $price
 * @property float  $fiat_amount
 * @package App\Http\Requests\Client\Deal
 */
class DealStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'ad_id'       => [
                'integer',
                'required',
                'exists:ads,id',
                app(IsAdActive::class),
                app(IsAuthorNotOwner::class),
                app(IsBalanceMoreThenMinRequired::class),
                app(CheckEmailConfirmation::class),
                app(CheckPhoneConfirmation::class),
                app(CheckTrust::class),
                app(CheckBlacklist::class),
                app(CheckTurnOver::class),
                app(CheckAmountRange::class, ['request' => $this]),
                app(CheckPrice::class, ['request' => $this]),
                app(CheckDealerBalance::class, ['request' => $this]),
                app(CheckDealerReputationRate::class),
                app(DealCancellationPercentRule::class),
                app(TorDeniedRule::class),
                app(AdMinDealFinishedCountRule::class),
            ],
            'price'       => 'numeric|required|min:1',
            'fiat_amount' => [
                'numeric',
                'required',
                app(CheckCryptoAmountMinLimit::class, ['request' => $this]),
            ],
        ];

        if ($this->adIsProtected()) {
            $rules = array_merge([
                'public_key'  => [
                    'required',
                    'string',
                    'max:255',
                    app(CheckAdIsProtected::class, ['request' => $this])
                ],
            ], $rules);
        }

        return $rules;
    }

    /**
     * @return bool
     */
    private function adIsProtected()
    {
        $ad = Ad::find($this->ad_id);

        return $ad ? $ad->isProtected() : false;
    }
}
