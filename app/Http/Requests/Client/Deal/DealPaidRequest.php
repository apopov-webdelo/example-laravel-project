<?php

namespace App\Http\Requests\Client\Deal;

use App\Http\Rules\Deal\DealStatusIsNotVerification;
use App\Models\Deal\Deal;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DealPaidRequest
 *
 * @property Deal $deal
 * @package App\Http\Requests\Client\Deal
 */
class DealPaidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'deal' => app(DealStatusIsNotVerification::class),
        ];
    }

    /**
     * @param null $keys
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['deal'] = $this->route('deal');

        return $data;
    }
}
