<?php

namespace App\Http\Requests\Client\Market;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdFilterRequest
 *
 * @property int price_from
 * @property int price_to
 * @property int min_from
 * @property int min_to
 * @property int max_from
 * @property int max_to
 * @property int reputation_from
 * @property int reputation_to
 * @property int crypto_currency_id
 * @property int currency_id
 * @property int country_id
 * @property int bank_id
 * @property int payment_system_id
 * @property int per_page
 * @property int page
 *
 * @package App\Http\Requests\Client\Market
 */
class AdFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price_from' => 'integer',
            'price_to'   => 'integer',

            'min_from' => 'integer',
            'min_to'   => 'integer',

            'max_from' => 'integer',
            'max_to'   => 'integer',

            'reputation_from' => 'integer',
            'reputation_to'   => 'integer',

            'crypto_currency_id' => 'integer|exists:crypto_currencies,id',
            'currency_id'        => 'integer|exists:currencies,id',
            'country_id'         => 'integer|exists:countries,id',
            'payment_system_id'  => 'integer|exists:payment_systems,id',
            'bank_id'            => 'integer|exists:banks,id',
            'per_page'           => 'integer|max:25',
            'page'               => 'integer|min:1'
        ];
    }
}
