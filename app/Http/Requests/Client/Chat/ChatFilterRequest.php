<?php

namespace App\Http\Requests\Client\Chat;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChatFilterRequest
 *
 * @property string $type
 * @property int $per_page
 *
 * @package App\Http\Requests\Client\Chat
 */
class ChatFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'     => 'string',
            'per_page' => 'integer|max:100'
        ];
    }
}
