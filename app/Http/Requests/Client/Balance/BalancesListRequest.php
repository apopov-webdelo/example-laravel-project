<?php

namespace App\Http\Requests\Client\Balance;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BalancesListRequest
 *
 * @package App\Http\Requests\Client\Balance
 * @property-read string $convert_to_fiat
 */
class BalancesListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'convert_to_fiat' => 'nullable|exists:currencies,code',
        ];
    }
}
