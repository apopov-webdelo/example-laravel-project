<?php

namespace App\Http\Requests\Client\Balance;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TransactionsRequest
 *
 * @property integer $per_page
 * @property string  $amount_from
 * @property string  $amount_to
 * @property string  $period
 *
 * @package App\Http\Requests\Client\Balance
 */
class TransactionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //TODO: реализовать проверку параметров фильтрации транзакций
            'period'      => 'nullable|string|in:any,today,yesterday,this-week,last-week,this-month,last-month',
            'amount_from' => 'nullable|numeric',
            'amount_to'   => 'nullable|numeric',
            'per_page'    => 'nullable|integer'
        ];
    }

    /**
     * @return array|string
     */
    public function getPeriodMethod()
    {
        $methodName = explode('-', $this->period);
        $methodName = (sizeof($methodName) > 1)
            ? ucfirst($methodName[0]) . ucfirst($methodName[1])
            : ucfirst($methodName[0]);

        return 'filterPeriod' . $methodName;
    }
}
