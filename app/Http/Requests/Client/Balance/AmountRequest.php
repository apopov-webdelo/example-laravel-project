<?php

namespace App\Http\Requests\Client\Balance;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AmountRequest
 *
 * @property int    crypto_currency_id
 * @package App\Http\Requests\Client\Balance
 */
class AmountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crypto_currency_id' => [
                'required',
                'integer',
                'exists:crypto_currencies,id',
            ],
        ];
    }
}
