<?php

namespace App\Http\Requests\Client\Balance\Emergency;

use Illuminate\Foundation\Http\FormRequest;

class AddWalletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_id'          => 'required|string',
            'crypto_currency_id' => [
                'required',
                'integer',
                'exists:crypto_currencies,id',
            ],
        ];
    }
}
