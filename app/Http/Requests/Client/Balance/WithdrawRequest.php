<?php

namespace App\Http\Requests\Client\Balance;

use App\Rules\CheckBalanceIsEnough;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class WithdrawRequest
 *
 * @property string $amount
 * @property string $wallet_id
 * @property string $private_key
 * @property int    $crypto_currency_id
 * @property bool   $commissionIncluded
 * @package App\Http\Requests\Client\Balance
 */
class WithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount'             => [
                'required',
                'numeric',
                app(CheckBalanceIsEnough::class, ['request' => $this]),
            ],
            'commissionIncluded' => 'bool|nullable',
            'wallet_id'          => 'string|required',
            'crypto_currency_id' => [
                'required',
                'integer',
                'exists:crypto_currencies,id',
            ],
        ];
    }
}
