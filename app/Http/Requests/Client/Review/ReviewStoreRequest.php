<?php

namespace App\Http\Requests\Client\Review;

use App\Models\User\ReviewConstants;
use Illuminate\Foundation\Http\FormRequest;

class ReviewStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'nullable|string|max:1000',
            'rate'    => 'required|integer|min:0|max:4',
            'trust'   => 'required|in:'.implode(',', ReviewConstants::TRUSTS),
        ];
    }
}
