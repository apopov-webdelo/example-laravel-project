<?php

namespace App\Http\Requests\Client\Ad;

use App\Http\Rules\IsBalanceMoreThanLimit;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdUpdateRequest
 *
 * @property integer $id
 * @property string  $notes
 * @property integer $price
 * @property integer $min
 * @property integer $max
 * @property integer $turnover
 * @property integer $min_deal_finished_count
 * @property integer $deal_cancellation_max_percent
 * @property integer $time
 * @property bool    $liquidity_required
 * @property bool    $email_confirm_required
 * @property bool    $phone_confirm_required
 * @property bool    $trust_required
 * @property bool    $tor_denied
 * @property bool    $is_active
 * @property integer $review_rate
 * @property string  $conditions
 *
 * @package App\Http\Requests\Client\Ad
 */
class AdUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                            => 'integer|required|exists:ads,id',
            'notes'                         => 'string|max:1000',
            'price'                         => 'numeric|required|min:1|max:' . config('app.ad.max_price_int'),
            'min'                           => 'numeric|required|min:1|max:' . $this->max,
            'max'                           => [
                'numeric',
                'required',
                'min:' . ($this->min + 1),
                app(IsBalanceMoreThanLimit::class, ['request' => $this]),
            ],
            'turnover'                      => 'integer|min:0',
            'min_deal_finished_count'       => 'integer|min:0',
            'deal_cancellation_max_percent' => 'integer|between:0,100',
            'review_rate'                   => 'integer|between:0,100',
            'time'                          => 'integer|in:' . config('app.ad.time_periods'),
            'liquidity_required'            => 'boolean',
            'email_confirm_required'        => 'boolean',
            'phone_confirm_required'        => 'boolean',
            'trust_required'                => 'boolean',
            'tor_denied'                    => 'boolean',
            'is_active'                     => 'boolean',
            'conditions'                    => 'string|max:1000|nullable',
            'banks.*'                       => 'integer|exists:banks,id',
        ];
    }
}
