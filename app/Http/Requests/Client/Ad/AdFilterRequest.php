<?php

namespace App\Http\Requests\Client\Ad;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdFilterRequest
 *
 * @property int  $payment_system_id
 * @property int  $bank_id
 * @property int  $country_id
 * @property int  $for_sale
 * @property int  $for_buy
 * @property int  $currency_id
 * @property int  $client_id
 * @property int  $author_id
 * @property int  $crypto_currency_id
 * @property int  $price_from
 * @property int  $price_to
 * @property int  $min_from
 * @property int  $min_to
 * @property int  $max_from
 * @property int  $max_to
 * @property int  $turnover_from
 * @property int  $turnover_to
 * @property int  $time_from
 * @property int  $time_to
 * @property bool $liquidity_required
 * @property bool $email_confirm_required
 * @property bool $phone_confirm_required
 * @property bool $trust_required
 * @property bool $tor_denied
 * @property int  $review_rate_from
 * @property int  $review_rate_to
 * @property bool $is_active
 * @property bool $is_blocked
 * @property int  $min_deal_finished_count_from
 * @property int  $min_deal_finished_count_to
 *
 * @package App\Http\Requests\Client\Ad
 */
class AdFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_system_id'            => 'integer|exists:payment_systems,id',
            'country_id'                   => 'integer|exists:countries,id',
            'for_sale'                     => 'boolean',
            'for_buy'                      => 'boolean',
            'currency_id'                  => 'integer|exists:currencies,id',
            'client_id'                    => 'integer|exists:users,id',
            'crypto_currency_id'           => 'integer|exists:crypto_currencies,id',
            'bank_id'                      => 'integer|exists:banks,id',
            'price_from'                   => 'numeric|min:1',
            'price_to'                     => 'numeric|min:1',
            'min_from'                     => 'numeric',
            'min_to'                       => 'numeric',
            'max_from'                     => 'numeric',
            'max_to'                       => 'numeric',
            'turnover_from'                => 'numeric',
            'turnover_to'                  => 'numeric',
            'time_from'                    => 'integer|min:' . config('app.ad.time_min') . '|max:' . config('app.ad.time_max'),
            'time_to'                      => 'integer|min:' . config('app.ad.time_min') . '|max:' . config('app.ad.time_max'),
            'liquidity_required'           => 'boolean',
            'email_confirm_required'       => 'boolean',
            'phone_confirm_required'       => 'boolean',
            'trust_required'               => 'boolean',
            'tor_denied'                   => 'boolean',
            'review_rate_from'             => 'numeric',
            'review_rate_to'               => 'numeric',
            'is_active'                    => 'boolean',
            'is_blocked'                   => 'boolean',
            'min_deal_finished_count_from' => 'integer',
            'min_deal_finished_count_to'   => 'integer',
        ];
    }
}
