<?php

namespace App\Http\Requests\Client\Ad;

use App\Http\Rules\IsPriceFitsWithAdMaxLimit;
use App\Models\Ad\Ad;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdPriceUpdateRequest
 *
 * @property float $price
 * @property Ad    $ad
 * @package App\Http\Requests\Client\Ad
 */
class AdPriceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => [
                'numeric',
                'required',
                'min:1',
                'max:' . config('app.ad.max_price_int'),
                app(IsPriceFitsWithAdMaxLimit::class, ['request' => $this]),
            ],
        ];
    }
}
