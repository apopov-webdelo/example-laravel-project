<?php

namespace App\Http\Requests\Client\Ad;

use App\Http\Rules\AdDuplicateDeniedRule;
use App\Http\Rules\IsBalanceMoreThanLimit;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdStoreRequest
 *
 * @package App\Http\Requests\Client\Ad
 * @property int $min
 * @property int $max
 */
class AdStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'public_key'                    => 'string|max:255',
            'payment_system_id'             => [
                'integer',
                'required',
                'exists:payment_systems,id',
                app(AdDuplicateDeniedRule::class, ['request' => $this]),
            ],
            'is_sale'                       => [
                'boolean',
                app(AdDuplicateDeniedRule::class, ['request' => $this]),
            ],
            'currency_id'                   => [
                'integer',
                'required',
                'exists:currencies,id',
                app(AdDuplicateDeniedRule::class, ['request' => $this]),
            ],
            'crypto_currency_id'            => [
                'integer',
                'required',
                'exists:crypto_currencies,id',
                app(AdDuplicateDeniedRule::class, ['request' => $this]),
            ],
            'notes'                         => 'string|max:1000',
            'country_id'                    => 'integer|required|exists:countries,id',
            'price'                         => 'numeric|required|min:1|max:' . config('app.ad.max_price_int'),
            'min'                           => 'numeric|required|min:1|max:' . $this->max,
            'max'                           => [
                'numeric',
                'required',
                'min:' . ($this->min + 1),
                app(IsBalanceMoreThanLimit::class, ['request' => $this]),
                'max:' . config('app.ad.max_price_int'),
            ],
            'turnover'                      => 'numeric|min:0',
            'min_deal_finished_count'       => 'integer|min:0',
            'deal_cancellation_max_percent' => 'integer|between:0,100',
            'review_rate'                   => 'integer|between:0,100',
            'time'                          => 'integer|required|in:' . config('app.ad.time_periods'),
            'liquidity_required'            => 'boolean',
            'email_confirm_required'        => 'boolean',
            'phone_confirm_required'        => 'boolean',
            'trust_required'                => 'boolean',
            'tor_denied'                    => 'boolean',
            'is_active'                     => 'boolean',
            'conditions'                    => 'max:1000',
            'banks.*'                       => 'integer|exists:banks,id',
        ];
    }
}
