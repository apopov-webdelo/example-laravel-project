<?php

namespace App\Http\Requests\Client\Ad;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdUpdateConditionsRequest
 *
 * @package App\Http\Requests\Client\Ad
 */
class AdUpdateConditionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'conditions' => 'nullable|string|max:255'
        ];
    }
}
