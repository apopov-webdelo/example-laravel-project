<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class PasswordResetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => [
                'required',
                'string',
                'confirmed',
                'min:7',
                'max:45',
                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$/',
            ],
            'password_confirmation' => [
                'required',
                'string',
                'same:password'
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'token.required' => __('validation.custom_errors.token.required'),

            'email.required' => __('validation.custom_errors.email.required'),
            'email.email' => __('validation.custom_errors.email.email'),

            'password.required' => __('validation.custom_errors.password.required'),
            'password.string' => __('validation.custom_errors.password.string'),
            'password.confirmed' => __('validation.custom_errors.password.confirmed'),
            'password.min' => __('validation.custom_errors.password.min'),
            'password.max' => __('validation.custom_errors.password.max'),
            'password.regex' => __('validation.custom_errors.password.regex'),

            'password_confirmation.required' => __('validation.custom_errors.password_confirmation.required'),
            'password_confirmation.string' => __('validation.custom_errors.password_confirmation.string'),
            'password_confirmation.same' => __('validation.custom_errors.password_confirmation.same'),
        ];
    }
}
