<?php

namespace App\Http\Requests\Support;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ApiAccessRequest
 *
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $text
 *
 * @package App\Http\Requests\Support
 */
class ApiAccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string',
            'login' => 'required|string',
            'email' => 'required|string|email',
            'text'  => 'required|string',
        ];
    }
}
