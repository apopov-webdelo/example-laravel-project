<?php

namespace App\Http\Requests\Blockchain\EmergencyLog;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EmergencyLogRequest
 *
 * @property int    $timestamp
 * @property string $hash
 * @package App\Http\Requests\Blockchain\EmergencyLog
 */
class EmergencyLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timestamp' => 'int|required',
            'hash'      => 'string|required',
        ];
    }
}
