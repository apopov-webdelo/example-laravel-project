<?php

namespace App\Http\Requests\Blockchain\DealCashback;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CancelRequest
 *
 * @property string $error
 * @property string $reason
 * @property string $hash
 * @package App\Http\Requests\Blockchain
 */
class CancelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'error'  => 'required|string',
            'reason' => 'required|string',
            'hash'   => 'required|string',
        ];
    }
}
