<?php

namespace App\Http\Requests\Blockchain\DealCashback;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DoneRequest
 *
 * @property int    $transaction_id
 * @property int    $timestamp
 * @property string $hash
 * @package App\Http\Requests\Blockchain
 */
class DoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id' => 'required|string',
            'timestamp'      => 'required|int',
            'hash'           => 'required|string',
        ];
    }
}
