<?php

namespace App\Http\Requests\Blockchain;

/**
 * Class ErrorRequest
 *
 * @package App\Http\Requests\Blockchain
 * @property-read string $error
 * @property-read string $reason
 * @property-read string $hash
 */
class ErrorRequest extends BlockchainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'error'  => 'string|required',
            'reason' => 'string|required',
            'hash'   => 'string|required',
        ];
    }
}
