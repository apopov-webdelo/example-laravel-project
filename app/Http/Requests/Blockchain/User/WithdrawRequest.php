<?php

namespace App\Http\Requests\Blockchain\User;

use App\Http\Requests\Blockchain\BlockchainRequest;

/**
 * Class TransactionRequest
 *
 * @package App\Http\Requests\Blockchain\User
 * @property-read float  $amount
 * @property-read float  $balance_amount
 * @property-read string $crypto_type
 * @property-read string $hash
 * @property-read string $transaction_id
 * @property-read string $payout_id
 */
class WithdrawRequest extends BlockchainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crypto_type'    => 'string|exists:crypto_currencies,code',
            'amount'         => 'numeric|required',
            'balance_amount' => 'numeric|required',
            'transaction_id' => 'string|required|unique:payout_transactions,transaction_id',
            'payout_id'      => 'int|required|exists:payout_transactions,id',
            'hash'           => 'string|required',
        ];
    }
}
