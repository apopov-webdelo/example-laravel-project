<?php

namespace App\Http\Requests\Blockchain\Deal;

use App\Http\Requests\Blockchain\BlockchainRequest;

/**
 * Class DealRequest
 *
 * @package App\Http\Requests\Blockchain\Deal
 * @property-read int    $timestamp
 * @property-read string $hash
 */
class DealRequest extends BlockchainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timestamp' => 'int|required',
            'hash'      => 'string|required',
        ];
    }
}
