<?php

namespace App\Http\Requests\Blockchain;

use Illuminate\Foundation\Http\FormRequest;

abstract class BlockchainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
         * Use control sum data validation for blockchain gate requests
         */
        return true;
    }
}
