<?php

namespace App\Http\Requests\Stats;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

/**
 * Class DealStatsRequest
 *
 * @package App\Http\Requests\Stats
 *
 * @property Carbon  $from
 * @property Carbon  $to
 * @property boolean $for_sale
 * @property boolean $for_buy
 * @property integer $cryptocurrency_id
 * @property integer $currency_id
 * @property integer $payment_system_id
 */
class DealStatsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from'              => 'date',
            'to'                => 'date|after_or_equal:from',
            'for_sale'          => 'boolean',
            'for_buy'           => 'boolean',
            'cryptocurrency_id' => 'integer|exists:crypto_currencies,id',
            'currency_id'       => 'integer|exists:currencies,id',
            'payment_system_id' => 'integer|exists:payment_systems,id',
        ];
    }
}
