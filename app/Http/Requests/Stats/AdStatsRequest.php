<?php

namespace App\Http\Requests\Stats;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdStatsRequest
 *
 * @package App\Http\Requests\Stats
 *
 * @property string $from
 * @property string $to
 */
class AdStatsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'nullable|date', // TODO: продумать конечное правило валидации
            'to'   => 'nullable|date',
        ];
    }
}
