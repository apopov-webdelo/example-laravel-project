<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 22.10.2018
 * Time: 14:54
 */

namespace App\Http\Requests\Admin\Client;

use App\Http\Requests\BaseUserFilterRequest;
use App\Models\Role\RoleConstants;

/**
 * Class FavoritesFilterRequest
 *
 * @property integer $per_page
 *
 * @package App\Http\Requests\Admin\Client
 */
class FavoritesFilterRequest extends BaseUserFilterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'per_page' => 'integer|max:100'
        ];
    }
}
