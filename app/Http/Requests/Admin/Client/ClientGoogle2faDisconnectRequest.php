<?php

namespace App\Http\Requests\Admin\Client;

use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;

/**
 * Class ClientGoogle2faDisconnectRequest
 *
 * @package App\Http\Requests\Admin\Client
 */
class ClientGoogle2faDisconnectRequest extends ActivityLogReasonRequest
{
}
