<?php

namespace App\Http\Requests\Admin\Client;

use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;

/**
 * Class ClientLockRequest
 *
 * @package App\Http\Requests\Admin\Client
 */
class ClientLockTradeRequest extends ActivityLogReasonRequest
{
}
