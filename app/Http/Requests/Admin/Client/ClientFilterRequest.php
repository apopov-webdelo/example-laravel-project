<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 22.10.2018
 * Time: 14:54
 */

namespace App\Http\Requests\Admin\Client;

use App\Http\Requests\BaseUserFilterRequest;
use App\Models\Role\RoleConstants;

/**
 * Class ClientFilterRequest
 *
 * @package App\Http\Requests\Client\Client
 */
class ClientFilterRequest extends BaseUserFilterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
