<?php

namespace App\Http\Requests\Admin\Client;

use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;

/**
 * Class ClientUnlockRequest
 *
 * @package App\Http\Requests\Admin\Client
 */
class ClientUnlockTradeRequest extends ActivityLogReasonRequest
{
}
