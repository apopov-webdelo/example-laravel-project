<?php

namespace App\Http\Requests\Admin\Deal;

use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;

/**
 * Request class for dashboard deals filtering
 *
 * @property string $reason
 *
 * @package App\Http\Requests\Client\Ad
 */
class DealCancelRequest extends ActivityLogReasonRequest
{
}
