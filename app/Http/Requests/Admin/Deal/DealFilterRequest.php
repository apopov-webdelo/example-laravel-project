<?php

namespace App\Http\Requests\Admin\Deal;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request class for dashboard deals filtering
 *
 * @property int $author_id
 * @property int $offer_id
 * @property int $recipient_id
 * @property int $status_id
 * @property int $per_page Quantity of items on page
 *
 * @package App\Http\Requests\Client\Ad
 */
class DealFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id'    => 'integer|exists:deals_statuses,id',
            'author_id'    => 'integer|exists:users,id',
            'recipient_id' => 'integer|exists:users,id',
            'offer_id'     => 'integer|exists:users,id',
            'per_page'     => 'integer|max:100',
        ];
    }
}
