<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 05.11.2018
 * Time: 12:27
 */

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SessionFilterRequest
 *
 * @property string $ip
 * @property string $country
 * @property string $city
 * @property string $type
 * @property string $time_from
 * @property string $time_to
 * @property integer $per_page
 *
 * @package App\Http\Requests\Admin
 */
class SessionFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ip' => 'string|ip',
            'country' => 'string',
            'city' => 'string',
            'type' => 'string',
            'time_from' => 'date_format:Y-m-d H:i:s',
            'time_to' => 'date_format:Y-m-d H:i:s',
            'per_page' => 'integer|max:100'
        ];
    }
}
