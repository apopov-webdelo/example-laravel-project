<?php

namespace App\Http\Requests\Admin\Review;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReviewUpdateRequest
 *
 * @property string $message
 * @property int $rate
 * @property string $trust
 * @property string $reason
 *
 * @package App\Http\Requests\Admin\Review
 */
class ReviewUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'nullable|string|max:2000',
            'rate'    => 'required|integer|in:0,1,2,3,4',
            'trust'   => 'required|string|in:negative,positive,neutral',
            'reason'  => 'required|string',
        ];
    }
}
