<?php

namespace App\Http\Requests\Admin\Review;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReviewFilterRequest
 *
 * @property string $trust
 *
 * @package App\Http\Requests\Admin\Review
 */
class ReviewFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trust' => 'string|nullable|in:negative,neutral,positive'
        ];
    }
}
