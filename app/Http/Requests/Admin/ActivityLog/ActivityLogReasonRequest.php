<?php

namespace App\Http\Requests\Admin\ActivityLog;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ActivityLogReasonRequest
 *
 * @property string $reason
 *
 * @package App\Http\Requests\Admin\Deal
 */
class ActivityLogReasonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason' => 'string|required',
        ];
    }
}
