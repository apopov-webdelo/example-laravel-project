<?php

namespace App\Http\Requests\Admin\Role;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RoleUpdateRequest
 *
 * @property string $name
 * @property array $permissions
 *
 * @package App\Http\Requests\Admin\Role
 */
class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'permissions.*.id' => 'integer|exists:permissions,id',
        ];
    }
}
