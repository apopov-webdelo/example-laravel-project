<?php

namespace App\Http\Requests\Admin\Role;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RoleFilterRequest
 *
 * @property integer $id
 * @property integer $name
 *
 * @package App\Http\Requests\Admin\Role
 */
class RoleFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'   => 'integer',
            'name' => 'string'
        ];
    }
}
