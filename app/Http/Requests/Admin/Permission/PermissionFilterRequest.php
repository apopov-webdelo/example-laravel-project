<?php

namespace App\Http\Requests\Admin\Permission;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RoleFilterRequest
 *
 * @property integer $id
 * @property integer $name
 * @property integer $per_page
 *
 * @package App\Http\Requests\Admin\Role
 */
class PermissionFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'       => 'integer',
            'name'     => 'string',
            'per_page' => 'integer|max:200'
        ];
    }
}
