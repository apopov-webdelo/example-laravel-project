<?php

namespace App\Http\Requests\Admin\StellarLog;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StellarLogFilterRequest
 *
 * @property integer $per_page
 * @property string  $type
 * @property string  $method
 * @property string  $code
 * @property string  $search
 * @property string  $period
 *
 * @package App\Http\Requests\StellarLog
 */
class StellarLogFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'     => 'nullable|string|in:any,to,from',
            'method'   => 'nullable|string|in:POST,GET,PUT,DELETE',
            'code'     => 'nullable|string',
            'search'   => 'nullable|string',
            'period'   => 'nullable|string|in:any,today,yesterday,this-week,last-week,this-month,last-month',
            'per_page' => 'integer|max:1000'
        ];
    }

    /**
     * @return array|string
     */
    public function getPeriodMethod()
    {
        $methodName = explode('-', $this->period);
        $methodName = (sizeof($methodName) > 1)
            ? ucfirst($methodName[0]) . ucfirst($methodName[1])
            : ucfirst($methodName[0]);

        return 'filterPeriod' . $methodName;
    }
}
