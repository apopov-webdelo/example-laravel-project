<?php

namespace App\Http\Requests\Admin\Directory;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CountryStoreRequest
 *
 * @property int $currency_id
 *
 * @package App\Http\Requests\Admin\Directory
 */
class CountryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'string|required',
            'currency_id' => 'integer|exists:currencies,id',
            'iso'         => 'integer',
        ];
    }
}
