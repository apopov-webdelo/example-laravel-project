<?php

namespace App\Http\Requests\Admin\Employee;

use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;

/**
 * Class EmployeeLockRequest
 *
 * @property string $reason
 *
 * @package App\Http\Requests\Admin\Employee
 */
class EmployeeLockRequest extends ActivityLogReasonRequest
{
}
