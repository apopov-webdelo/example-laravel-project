<?php

namespace App\Http\Requests\Admin\Employee;

use App\Http\Requests\BaseUserFilterRequest;

/**
 * Class EmployeeFilterRequest
 * @package App\Http\Requests\Employee
 */
class EmployeeFilterRequest extends BaseUserFilterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
