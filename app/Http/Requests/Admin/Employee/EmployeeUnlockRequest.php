<?php

namespace App\Http\Requests\Admin\Employee;

use App\Http\Requests\Admin\ActivityLog\ActivityLogReasonRequest;

/**
 * Class EmployeeUnlockRequest
 *
 * @property string $reason
 *
 * @package App\Http\Requests\Admin\Employee
 */
class EmployeeUnlockRequest extends ActivityLogReasonRequest
{
}
