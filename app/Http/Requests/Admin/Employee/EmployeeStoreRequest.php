<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EmployeeStoreRequest
 *
 * @property string $login
 * @property string $email
 * @property string $phone
 * @property array $permissions
 *
 * @package App\Http\Requests\Admin\Employee
 */
class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|string',
            'login'             => 'required|unique:admins,login|string',
            'email'             => 'required|unique:admins,email|string|email',
            'phone'             => 'string',
            'permissions.*.id'  => 'integer|exists:permissions,id'
        ];
    }
}
