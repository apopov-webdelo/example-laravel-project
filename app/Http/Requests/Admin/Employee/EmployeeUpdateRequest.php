<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EmployeeUpdateRequest
 *
 * @property string $login
 * @property string $email
 * @property string $phone
 * @property array $permissions
 *
 * @package App\Http\Requests\Admin\Employee
 */
class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login'             => 'unique:admins,login,'.$this->input('id').'|string',
            'email'             => 'unique:admins,email,'.$this->input('id').'|string|email',
            'phone'             => 'string',
            'permissions.*.id'  => 'integer|exists:permissions,id'
        ];
    }
}
