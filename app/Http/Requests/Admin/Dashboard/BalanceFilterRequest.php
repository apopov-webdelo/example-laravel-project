<?php

namespace App\Http\Requests\Admin\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BalanceFilterRequest
 *
 * @property int $crypto_currency_id
 * @property string $period
 * @property int    $per_page
 *
 * @package App\Http\Requests\Admin\Dashboard
 */
class BalanceFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crypto_currency_id' => 'required|integer|exists:crypto_currencies,id',
            'period'             => 'nullable|string|in:any,today,yesterday,this-week,last-week,this-month,last-month',
            'per_page'           => 'integer|max:1000'
        ];
    }

    /**
     * @return array|string
     */
    public function getPeriodMethod()
    {
        $methodName = explode('-', $this->period);
        $methodName = (sizeof($methodName) > 1)
            ? ucfirst($methodName[0]) . ucfirst($methodName[1])
            : ucfirst($methodName[0]);

        return 'filterPeriod' . $methodName;
    }
}
