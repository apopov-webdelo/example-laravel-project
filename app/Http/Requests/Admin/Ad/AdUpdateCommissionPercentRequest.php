<?php

namespace App\Http\Requests\Admin\Ad;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdUpdateCommissionPercentRequest
 *
 * @package App\Http\Requests\Admin\Ad
 */
class AdUpdateCommissionPercentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commission_percent' => 'required'
        ];
    }
}
