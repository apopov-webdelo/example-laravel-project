<?php

namespace App\Http\Middleware;

use App\Events\Blockchain\StellarLogEvent;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class StellarFromLog
 *
 * @package App\Http\Middleware
 */
class StellarFromLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * @param Request  $request
     * @param Response $response
     */
    public function terminate($request, $response)
    {
        event(app(StellarLogEvent::class, [
            'type'          => 'from',
            'method'        => $request->getMethod(),
            'route'         => $request->path(),
            'request'       => serialize($request->all()),
            'response'      => serialize(json_decode($response->getContent(), true)),
            'response_code' => $response->getStatusCode(),
        ]));
    }
}
