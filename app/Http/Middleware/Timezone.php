<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Timezone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $timezone = $request->header('timezone') ?? $request->header('Timezone');

        if (isset($timezone)) {
            if (isValidTimezone($timezone)) {
                date_default_timezone_set($timezone);
                Config::set('app.timezone', $timezone);
                try {
                    DB::update('SET time_zone = ?', [$timezone]);
                } catch (\Exception $e) {
                    Log::critical('There are no timezone "'.$timezone.'" in mysql table! Need to update!');
                }
            } else {
                Log::alert('Try to set incorrect timezone "' . $timezone . '"!');
                abort('400', 'Incorrect timezone "' . $timezone . '"!');
            }
        }

        return $next($request);
    }
}
