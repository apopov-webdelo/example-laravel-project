<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

/**
 * Class MarketGuestMiddleware
 *
 * Authorizes users on public routes to show user-to-user relations (blocked, favorite etc)
 *
 * @package App\Http\Middleware
 */
class MarketGuestMiddleware
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     *
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->authenticate($request);

        return $next($request);
    }

    /**
     * @param $request
     */
    protected function authenticate($request)
    {
        $guard = 'api';
        if ($this->auth->guard($guard)->check()) {
            $this->auth->shouldUse($guard);
        }
    }
}
