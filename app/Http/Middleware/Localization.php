<?php

namespace App\Http\Middleware;

use Closure;


class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('locale') ?? $request->header('Locale');
        if ($locale) {
            app()->setLocale(strtolower($locale));
        }

        return $next($request);
    }
}
