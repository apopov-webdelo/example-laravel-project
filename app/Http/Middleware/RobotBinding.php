<?php

namespace App\Http\Middleware;

use Closure;

class RobotBinding
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \App\Utils\User\RobotBinding::bind();
        return $next($request);
    }
}
