<?php

namespace App\Http\Middleware;

use App\Contracts\Services\Auth\TokenServiceContract;
use App\Services\Auth\TokenService;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class ProlongTokenMiddleware
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()) {
            /* @var TokenService $tokenService */
            $tokenService = app(TokenServiceContract::class)->setUser($this->auth->user());
            $tokenService->updateExpireOnVisit();
        }

        return $next($request);
    }
}
