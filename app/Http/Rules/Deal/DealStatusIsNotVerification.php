<?php

namespace App\Http\Rules\Deal;

use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Contracts\Validation\Rule;

class DealStatusIsNotVerification implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string     $attribute
     * @param  mixed|Deal $deal
     *
     * @return bool
     */
    public function passes($attribute, $deal)
    {
        return $deal->status->id !== DealStatusConstants::VERIFICATION;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('deal.error.cant_cancel');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
