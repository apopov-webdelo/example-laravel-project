<?php

namespace App\Http\Rules;

use App\Http\Requests\Client\Deal\DealStoreRequest;
use App\Models\Ad\Ad;
use Illuminate\Contracts\Validation\Rule;

class CheckPrice implements Rule
{
    private $request;

    /**
     * CheckPrice constructor.
     *
     * @param DealStoreRequest $request
     */
    public function __construct(DealStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::find((int)$value);
        $price = currencyToCoins($this->request->input('price'), $ad->currency);

        return $ad->is_sale
            ? $price >= $ad->price
            : $price <= $ad->price;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.invalid_price');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
