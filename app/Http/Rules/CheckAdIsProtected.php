<?php

namespace App\Http\Rules;

use App\Http\Requests\Client\Deal\DealStoreRequest;
use App\Models\Ad\Ad;
use App\Rules\Ad\AdActiveRule;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class CheckAdIsProtected
 *
 * @package App\Http\Rules
 */
class CheckAdIsProtected implements Rule
{
    /**
     * @var DealStoreRequest
     */
    private $request;

    /**
     * CheckAmountPeriod constructor.
     *
     * @param DealStoreRequest $request
     */
    public function __construct(DealStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::findOrFail($this->request->ad_id);

        if ($ad->isProtected() && !$value) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.ad_is_protected');
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return get_class($this);
    }
}
