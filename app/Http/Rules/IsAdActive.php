<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use App\Rules\Ad\AdActiveRule;
use Illuminate\Contracts\Validation\Rule;

class IsAdActive implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {}

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return app(AdActiveRule::class, ['ad' => Ad::find((int)$value) ])->check();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.ad_is_not_active');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
