<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use App\Rules\Ad\AdCryptoCurrencyMinBalanceRule;
use Illuminate\Contracts\Validation\Rule;

class IsBalanceMoreThenMinRequired implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return app(AdCryptoCurrencyMinBalanceRule::class, [ 'ad' => Ad::find($value) ])->check();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.balance_is_to_small');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
