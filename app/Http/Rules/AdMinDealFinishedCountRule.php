<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use Illuminate\Contracts\Validation\Rule;
use \App\Rules\Ad\AdMinDealFinishedCountRule as AdMinDealFinishedCountRuleStandard;

class AdMinDealFinishedCountRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return app(AdMinDealFinishedCountRuleStandard::class, ['ad' => Ad::find((int)$value) ])->check();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.min_deal_finished_count');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
