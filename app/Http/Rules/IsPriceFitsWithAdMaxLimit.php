<?php

namespace App\Http\Rules;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Client\Ad\AdPriceUpdateRequest;
use App\Models\Ad\Ad;
use App\Rules\Ad\IsBalanceMoreThanLimit as IsBalanceMoreThanLimitRule;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

/**
 * Check if new Ad price fits well with Ad max limit
 *
 * Class IsPriceFitsWithAdMaxLimit
 *
 * @package App\Http\Rules
 */
class IsPriceFitsWithAdMaxLimit implements Rule
{
    /**
     * @var AuthenticatedContract
     */
    protected $user;

    /**
     * @var Request|AdPriceUpdateRequest
     */
    protected $request;

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * @var IsBalanceMoreThanLimitRule
     */
    protected $rule;

    /**
     * @param AuthenticatedContract $user
     * @param Request               $request
     */
    public function __construct(AuthenticatedContract $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;
        $this->ad = $this->request->ad;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  float  $price
     *
     * @return bool
     */
    public function passes($attribute, $price)
    {
        $isSale = $this->ad->is_sale;

        $cryptoCurrency = $this->ad->cryptoCurrency;
        $balance = $this->user->getBalance($cryptoCurrency);

        if ($isSale) {
            $this->rule = app(
                IsBalanceMoreThanLimitRule::class,
                [
                    'limit'      => $this->ad->accuracyMax,
                    'price'      => $price,
                    'balance'    => $balance->accuracyAmount,
                    'commission' => $this->ad->accuracyCommissionPercent,
                ]
            );

            return $this->rule->check();
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->rule->message();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return get_class($this);
    }
}
