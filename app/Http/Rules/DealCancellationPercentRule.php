<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use App\Rules\Ad\AdDealCancellationPercentRule;
use Illuminate\Contracts\Validation\Rule;

class DealCancellationPercentRule implements Rule
{
    /**
     * DealCancellationPercentRule constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::find($value);

        return $ad->is_sale
            ? app(AdDealCancellationPercentRule::class, [ 'ad' => $ad ])->check()
            : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.invalid_buyer_deal_cancellation_max_percent');
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return get_class($this);
    }
}