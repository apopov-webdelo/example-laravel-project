<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use App\Rules\Ad\AdBlacklistFreeRule;
use Illuminate\Contracts\Validation\Rule;

class CheckBlacklist implements Rule
{
    /**
     * @var AdBlacklistFreeRule
     */
    protected $rule;

    /**
     * CheckBlacklist constructor.
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::findOrFail($value);

        $this->rule = app(AdBlacklistFreeRule::class, ['ad' => $ad]);

        return $this->rule->check();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->rule->message();
    }

    public function __toString()
    {
        return get_class($this);
    }
}
