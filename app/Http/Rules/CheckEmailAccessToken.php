<?php

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckEmailAccessToken implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return auth()->user()->security->email_access_token === $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.user.email_access_token');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
