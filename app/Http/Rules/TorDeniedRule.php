<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use App\Rules\Ad\AdActiveRule;
use App\Rules\Ad\AdTorDeniedRule;
use Illuminate\Contracts\Validation\Rule;

class TorDeniedRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return app(AdTorDeniedRule::class, ['ad' => Ad::find((int)$value) ])->check();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.tor_denied');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
