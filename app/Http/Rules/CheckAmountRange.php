<?php

namespace App\Http\Rules;

use App\Http\Requests\Client\Deal\DealStoreRequest;
use App\Models\Ad\Ad;
use Illuminate\Contracts\Validation\Rule;

class CheckAmountRange implements Rule
{
    private $request;

    /**
     * CheckAmountPeriod constructor.
     *
     * @param DealStoreRequest $request
     */
    public function __construct(DealStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::find($value);
        $amount = currencyToCoins($this->request->fiat_amount, $ad->currency);
        return $ad->min <= $amount && $ad->max >= $amount;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.invalid_amount_period');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
