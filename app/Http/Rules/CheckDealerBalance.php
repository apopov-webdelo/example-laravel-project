<?php

namespace App\Http\Rules;

use App\Http\Requests\Client\Deal\DealStoreRequest;
use App\Models\Ad\Ad;
use Illuminate\Contracts\Validation\Rule;

class CheckDealerBalance implements Rule
{
    private $request;

    /**
     * CheckDealerBalance constructor.
     *
     * @param DealStoreRequest $request
     */
    public function __construct(DealStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad     = Ad::find($value);
        $dealer = $ad->is_sale
            ? $ad->author
            : auth()->user();

        $fiatAmount   = currencyToCoins($this->request->fiat_amount, $ad->currency);
        $cryptoAmount = $fiatAmount / $ad->price;

        return $dealer->getBalance($ad->cryptoCurrency)->accuracyAmount > $cryptoAmount;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.invalid_dealer_balance');
    }

    public function __toString()
    {
        return get_class($this);
    }
}