<?php

namespace App\Http\Rules;

use App\Models\Ad\Ad;
use App\Rules\Ad\AdEmailConfirmationRule;
use Illuminate\Contracts\Validation\Rule;

class CheckEmailConfirmation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return app(AdEmailConfirmationRule::class, [ 'ad' => Ad::find($value) ])->check() ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.email_required');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
