<?php

namespace App\Http\Rules;

use App\Contracts\AuthenticatedContract;
use App\Models\Ad\Ad;
use App\Rules\Ad\AdBlacklistFreeRule;
use Illuminate\Contracts\Validation\Rule;

class CheckTrust implements Rule
{
    /**
     * @var AuthenticatedContract
     */
    protected $user;

    /**
     * Create a new rule instance.
     *
     * @param AuthenticatedContract $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::find($value);

        return $ad->trust_required
            ? app(AdBlacklistFreeRule::class, ['ad' => $ad])->check()
            : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.deal.trust_required');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
