<?php

namespace App\Http\Rules;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Http\Requests\Client\Deal\DealStoreRequest;
use App\Models\Ad\Ad;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class CheckCryptoAmountMinLimit check is crypto amount according with min limit
 *
 * @package App\Http\Rules
 */
class CheckCryptoAmountMinLimit implements Rule
{
    /**
     * @var DealStoreRequest
     */
    private $request;

    /**
     * @var CryptoCurrencyContract
     */
    private $crypto;

    /**
     * CheckCryptoAmountMinLimit constructor.
     *
     * @param DealStoreRequest $request
     */
    public function __construct(DealStoreRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ad = Ad::findOrFail($this->request->ad_id);
        $amount = currencyToCoins($value, $ad->currency);
        $cryptoAmount = currencyToCoins($amount / $ad->price, $ad->cryptoCurrency);
        $this->crypto = $ad->cryptoCurrency;

        return $this->isCryptoAmountMoreThanMinLimit($cryptoAmount, $ad);
    }

    /**
     * @param $cryptoAmount
     * @param $ad
     *
     * @return bool
     */
    protected function isCryptoAmountMoreThanMinLimit($cryptoAmount, $ad): bool
    {
        return $cryptoAmount >= currencyToCoins($this->getLimit(), $ad->cryptoCurrency);
    }

    /**
     * Retrieve minimal crypto amount from config
     *
     * @return float
     */
    protected function getLimit()
    {
        return config('app.deal.min_crypto_amount');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $limit = $this->getLimit();

        return trans('validation.deal.crypto_amount_less_min_limit', [
            'limit' => floatToString($limit, $this->crypto->getAccuracy()),
            'code'  => mb_strtoupper($this->crypto->getCode()),
        ]);
    }

    public function __toString()
    {
        return get_class($this);
    }
}
