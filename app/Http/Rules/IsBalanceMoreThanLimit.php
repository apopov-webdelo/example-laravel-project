<?php

namespace App\Http\Rules;

use App\Contracts\AuthenticatedContract;
use App\Http\Requests\Client\Ad\AdStoreRequest;
use App\Http\Requests\Client\Ad\AdUpdateRequest;
use App\Models\Ad\Ad;
use App\Models\Directory\CryptoCurrency;
use App\Rules\Ad\IsBalanceMoreThanLimit as IsBalanceMoreThanLimitRule;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

/**
 * Class IsBalanceMoreThanLimit
 *
 * @package App\Http\Rules
 */
class IsBalanceMoreThanLimit implements Rule
{
    /**
     * @var AuthenticatedContract
     */
    protected $user;

    /**
     * @var IsBalanceMoreThanLimitRule
     */
    protected $rule;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var Request|AdUpdateRequest|AdStoreRequest
     */
    protected $request;

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * @var bool
     */
    protected $isNew;

    /**
     * @param AuthenticatedContract $user
     * @param Request               $request
     */
    public function __construct(AuthenticatedContract $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;
        $this->isNew = (bool)$this->request->crypto_currency_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $limit
     *
     * @return bool
     * @throws \Exception
     */
    public function passes($attribute, $limit)
    {
        if (!$this->request->price) {
            $this->message = __('validation.ad.price_unknown');

            return false;
        }

        $cryptoCurrency = $this->isNew
            ? CryptoCurrency::findOrFail($this->request->crypto_currency_id)
            : $this->getAd()->cryptoCurrency;

        $isSale = $this->request->is_sale ?? $this->getAd()->is_sale;

        $balance = $this->user->getBalance($cryptoCurrency);

        $commission = $this->isNew ? $balance->accuracyCommission : $this->getAd()->accuracyCommissionPercent;

        if ($isSale) {
            $this->rule = app(
                IsBalanceMoreThanLimitRule::class,
                [
                    'limit'      => $limit,
                    'price'      => $this->request->price,
                    'balance'    => $balance->accuracyAmount,
                    'commission' => $commission,
                ]
            );

            return $this->rule->check();
        }

        return true;
    }

    /**
     * Return Ad model using request data
     *
     * @return Ad
     */
    protected function getAd(): Ad
    {
        return $this->ad
            ?? $this->ad = $this->request->ad
                ?? Ad::findOrFail($this->request->id);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->rule ? $this->rule->message() : $this->message;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return get_class($this);
    }
}
