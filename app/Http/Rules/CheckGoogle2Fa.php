<?php

namespace App\Http\Rules;

use App\Contracts\AuthenticatedContract;
use Illuminate\Contracts\Validation\Rule;
use Google2FA;

class CheckGoogle2Fa implements Rule
{
    /**
     * @var AuthenticatedContract
     */
    protected $user;

    /**
     * @param AuthenticatedContract $user
     */
    public function __construct(AuthenticatedContract $user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Google2FA::verifyKey($this->user->google2fa_secret, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.user.google2fa_doesnt_match');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
