<?php

namespace App\Http\Rules;

use App\Contracts\AuthenticatedContract;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Ad\AdRepo;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class AdDuplicateDeniedRule implements Rule
{
    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var AdRepo $repo
     */
    private $repo;

    /**
     * @var AuthenticatedContract $user
     */
    private $user;

    private $exclude;

    /**
     * AdDuplicateDeniedRule constructor.
     *
     * @param Request               $request
     * @param AdRepo                $repo
     * @param AuthenticatedContract $user
     */
    public function __construct(Request $request, AdRepo $repo, AuthenticatedContract $user)
    {
        $this->request = $request;
        $this->repo    = $repo;
        $this->user    = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->repo
            ->filterByUser($this->user)
            ->filterByType($this->request->is_sale)
            ->filterByCryptoCurrency(CryptoCurrency::findOrFail($this->request->crypto_currency_id))
            ->filterByCountry(Country::findOrFail($this->request->country_id))
            ->filterByCurrency(Currency::findOrFail($this->request->currency_id))
            ->filterByPaymentSystem(PaymentSystem::findOrFail($this->request->payment_system_id));

        if (!empty($this->exclude['id'])) {
            $this->repo->excludeById($this->exclude['id']);
        }

        return $this->repo->take()->get()->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.ad.unique_ad');
    }

    public function __toString()
    {
        return get_class($this);
    }

    public function exclude($value, $field = 'id')
    {
        $this->exclude = [ $field => $value ];

        return $this;
    }
}
