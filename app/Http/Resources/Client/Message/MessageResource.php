<?php

namespace App\Http\Resources\Client\Message;

use App\Http\Resources\Admin\Admin\AdminResource;
use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Chat\ChatResource;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Chat\Message;
use App\Models\User\User;

/**
 * Class MessageResource
 *
 * @property Message $resource
 *
 * @package App\Http\Resources\Message
 */
class MessageResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $dealId = $this->getDealId();

        return array_merge(parent::toArray($request), [
            'deal_id'    => $dealId,
            'chat'       => app(ChatResource::class, ['resource' => $this->resource->chat]),
            'author'     => ($this->resource->author instanceof User)
                ? app(UserResource::class, ['resource' => $this->resource->author])
                : app(AdminResource::class, ['resource' => $this->resource->author]),
            'is_admin'   => ! ($this->resource->author instanceof User),
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at ? $this->resource->updated_at : null,
            'image'      => $this->resource->image ? $this->resource->image->getUrl() : null,
        ]);
    }

    private function getDealId()
    {
        return count($this->resource->chat->deal) ? $this->resource->chat->deal->first()->id : null;
    }
}
