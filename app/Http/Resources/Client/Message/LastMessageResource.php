<?php

namespace App\Http\Resources\Client\Message;

use App\Http\Resources\Admin\Admin\AdminResource;
use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Chat\Message;
use App\Models\User\User;

/**
 * Class LastMessageResource
 *
 * @property Message $resource
 *
 * @package App\Http\Resources\Message
 */
class LastMessageResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $dealId = $this->getDealId();

        $chat = $this->resource->chat;
        unset($this->resource->chat);

        return array_merge(parent::toArray($request), [
            'deal_id'    => $dealId,
            'chat_id'    => $chat->id,
            'author'     => ($this->resource->author instanceof User)
                ? app(UserResource::class, ['resource' => $this->resource->author])
                : app(AdminResource::class, ['resource' => $this->resource->author]),
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at ? $this->resource->updated_at : null,
        ]);
    }

    private function getDealId()
    {
        return count($this->resource->chat->deal) ? $this->resource->chat->deal->first()->id : null;
    }
}
