<?php

namespace App\Http\Resources\Client\Balance;

use App\Http\Resources\BaseCollection;

class TransactionCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return TransactionResource::collection($this);
    }
}
