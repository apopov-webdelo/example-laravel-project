<?php

namespace App\Http\Resources\Client\Balance;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Http\Resources\Client\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * PayOutResource for PayOutContract entities
 *
 * @package App\Http\Resources\Client\Balance
 * @property-read PayOutContract $resource
 */
class PayOutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $payOut = $this->resource;
        return [
            'id'          => $payOut->id(),
            'status'      => $payOut->status(),
            'amount'      => $payOut->amount(),
            'commission'  => $payOut->commission(),
            'wallet'      => $payOut->walletId(),
            'crypto_code' => $payOut->cryptoCurrency()->getCode(),
            'transaction' => $payOut->transactionId(),
            'user'        => app(UserResource::class, ['resource' => $payOut->user()]),
        ];
    }
}
