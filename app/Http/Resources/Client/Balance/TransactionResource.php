<?php

namespace App\Http\Resources\Client\Balance;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Balance\Balance;
use App\Models\Balance\Transaction;
use App\Models\User\User;
use App\Repositories\Balance\TransactionBalanceStateRepo;
use Illuminate\Support\Facades\Auth;

/**
 * @package App\Http\Resources\Balance
 * @property Transaction $resource
 */
class TransactionResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $transaction = $this->resource;

        /** @var TransactionBalanceStateRepo $stateRepo */
        $stateRepo = app(TransactionBalanceStateRepo::class);
        $state = $stateRepo->getByTransaction($transaction, $this->getAuthUserBalance());

        $balanceAfter = $state->amount + ($this->isAuthUserReceiver()
            ? $transaction->amount
            : $transaction->amount * (-1));

        return [
            'id'             => $transaction->id,
            'amount'         => $this->getFormattedAmount(),
            'type'           => $this->isAuthUserReceiver() ? 'deposit' : 'withdraw',
            'date'           => $transaction->created_at,
            'description'    => $transaction->description,
            'user'           => app(UserResource::class, ['resource' => $this->getAnotherUser()]),
            'reason'         => app(TransactionReasonResource::class, ['resource' => $transaction->reason]),
            'balance_before' => currencyFromCoins($state->amount, $state->balance->cryptoCurrency, true),
            'balance_after'  => currencyFromCoins($balanceAfter, $state->balance->cryptoCurrency, true),
        ];
    }

    /**
     * Return amount in string format
     *
     * @return string
     */
    protected function getFormattedAmount()
    {
        $amount = currencyFromCoins($this->resource->amount, $this->getAuthUserBalance()->cryptoCurrency, true);
        return ($this->isAuthUserReceiver() ? '+' : '-') . $amount;
    }

    /**
     * Check is authenticated user is receiver in that transaction
     *
     * @return bool
     */
    protected function isAuthUserReceiver(): bool
    {
        return $this->resource->receiverBalance->user_id == Auth::user()->id;
    }

    /**
     * Return balance for authorized user
     *
     * @return Balance
     */
    protected function getAuthUserBalance(): Balance
    {
        return $this->isAuthUserReceiver()
            ? $this->resource->receiverBalance
            : $this->resource->remitterBalance;
    }

    /**
     * Retrieve another user (non authorized) in that transaction
     *
     * @return User
     */
    protected function getAnotherUser(): User
    {
        return $this->isAuthUserReceiver()
            ? $this->resource->remitterBalance->user
            : $this->resource->receiverBalance->user;
    }
}
