<?php

namespace App\Http\Resources\Client\Balance;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Commissions
 *
 * @package App\Http\Resources\Client\Balance
 * @property-read array $resource
 */
class CommissionConditionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource;
    }
}
