<?php

namespace App\Http\Resources\Client\Balance;

use App\Http\Resources\BaseResource;

/**
 * Class EmergencyWalletResource
 *
 * @package App\Http\Resources\Balance
 * @property string $resource wallet ID
 */
class EmergencyWalletResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'wallet_id' => $this->resource,
        ];
    }
}
