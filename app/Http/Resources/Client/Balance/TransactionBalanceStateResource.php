<?php

namespace App\Http\Resources\Client\Balance;

use App\Http\Resources\BaseResource;
use App\Models\Balance\TransactionBalanceState;

/**
 * @package App\Http\Resources\Balance
 * @property TransactionBalanceState $resource
 */
class TransactionBalanceStateResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $state = $this->resource;
        return [
            'amount' => currencyFromCoins($state->amount, $state->balance->cryptoCurrency, true),
        ];
    }
}
