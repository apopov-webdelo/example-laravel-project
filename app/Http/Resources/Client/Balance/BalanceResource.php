<?php

namespace App\Http\Resources\Client\Balance;

use App\Facades\CryptoExchange;
use App\Http\Requests\Client\Balance\BalancesListRequest;
use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\CryptoCurrency\CryptoCurrencyResource;
use App\Models\Balance\Balance;
use App\Models\Directory\FiatConstants;

/**
 * Class BalanceResource
 * @package App\Http\Resources\Balance
 * @property Balance $resource
 */
class BalanceResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  BalancesListRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        $balance = $this->resource;

        $fiatCode = $request->convert_to_fiat ?? FiatConstants::USD;
        $amountInUSD = CryptoExchange::convert(
            $balance->accuracyAmount,
            $balance->cryptoCurrency->getCode(),
            $fiatCode
        );

        return [
            'user'            => $balance->user_id,
            'crypto_currency' => app(CryptoCurrencyResource::class, ['resource' => $balance->cryptoCurrency]),
            'amount'          => $this->adapt($balance->amount),
            'escrow'          => $this->adapt($balance->escrow),
            'due'             => $this->adapt($balance->due),
            'turnover'        => $this->adapt($balance->turnover),
            'commission'      => $balance->accuracyCommission,
            'fiat'            => [
                'code'     => $fiatCode,
                'amount'   => $amountInUSD,
            ],
        ];
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->cryptoCurrency);
    }
}
