<?php

namespace App\Http\Resources\Client\Deal;

use App\Http\Resources\BaseResource;
use App\Models\Deal\Deal;

/**
 * DealResource for author
 *
 * @package App\Http\Resources\Market
 * @property Deal $resource
 */
class DealResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $currency = $this->resource->ad->currency;

        return array_merge(parent::toArray($request), [
            'price'         => currencyFromCoins($this->resource->price, $currency),
            'fiat_amount'   => currencyFromCoins($this->resource->fiat_amount, $currency),
            'created_at'    => $this->resource->created_at,
            'crypto_amount' => $this->adapt($this->resource->crypto_amount),
            'commission'    => $this->adapt($this->resource->commission),
        ]);
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->ad->cryptoCurrency);
    }
}
