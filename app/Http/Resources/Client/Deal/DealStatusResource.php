<?php

namespace App\Http\Resources\Client\Deal;

use App\Http\Resources\BaseResource;
use App\Models\Deal\DealStatus;

/**
 * @package App\Http\Resources\Deal
 * @property DealStatus $resource
 */
class DealStatusResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->resource->id,
            'title'      => $this->resource->title,
            'created_at' => $this->resource->pivot->created_at,
        ];
    }
}
