<?php

namespace App\Http\Resources\Client\Note;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\User\UserResource;
use App\Models\User\Note;

/**
 * Class NoteResource
 *
 * @property Note $resource
 *
 * @package App\Http\Resources\Note
 */
class NoteResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'created_at' => $this->resource->created_at,
            'recipient'  => app(UserResource::class, ['resource'=>$this->resource->recipient])
        ]);
    }
}
