<?php

namespace App\Http\Resources\Client\Stats;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Stats\Deal\DealCountStatsResource;
use App\Http\Resources\Client\Stats\Deal\DealCryptoStatsResource;
use App\Http\Resources\Client\Stats\Deal\DealFiatStatsResource;
use App\Http\Resources\Client\Stats\Deal\DealPaymentSystemsStatsResource;
use App\Services\Statistics\Deals\DealStatisticsResult;

/**
 * Class DealStatsResource
 *
 * @package App\Http\Resources\Stats
 * @property DealStatisticsResult $resource
 */
class DealStatsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'deals' => app(
                DealCountStatsResource::class,
                ['resource' => $this->resource->getStatsOperations()]
            ),

            'fiat' => app(
                DealFiatStatsResource::class,
                ['resource' => $this->resource->getStatsFiat()]
            ),

            'crypto' => app(
                DealCryptoStatsResource::class,
                ['resource' => $this->resource->getStatsCrypto()]
            ),

            'payment_systems' => app(
                DealPaymentSystemsStatsResource::class,
                ['resource' => $this->resource->getStatsPaymentSystems()]
            ),
        ];
    }
}
