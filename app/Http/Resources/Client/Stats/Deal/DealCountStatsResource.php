<?php

namespace App\Http\Resources\Client\Stats\Deal;

use App\Http\Resources\BaseResource;

/**
 * Class DealStatsResource
 *
 * @package App\Http\Resources\Stats
 * @property array $resource
 */
class DealCountStatsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource;
    }
}
