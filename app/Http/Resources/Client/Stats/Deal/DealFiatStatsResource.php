<?php

namespace App\Http\Resources\Client\Stats\Deal;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\Currency\CurrencyResource;
use App\Models\Directory\Currency;
use App\Repositories\Directory\CurrencyRepo;

/**
 * Class DealFiatStatsResource
 *
 * @package App\Http\Resources\Stats
 * @property array $resource
 */
class DealFiatStatsResource extends BaseResource
{
    /**
     * @var CurrencyRepo
     */
    private $repo;
    /**
     * @var Currency
     */
    private $currencyUsd;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $this->repo = app(CurrencyRepo::class);
        $this->currencyUsd = $this->repo->search('USD')->take()->get()->first();

        return $this->convert($this->resource);
    }

    /**
     * @param $data
     *
     * @return array
     */
    protected function convert($data)
    {
        $result = [];
        foreach ($data as $currencyCode => $values) {
            $currency = $this->repo->search($currencyCode)->take()->get()->first();
            if ($currency) {
                $values['turnover_usd'] = currencyFromCoins($values['turnover'], $this->currencyUsd);
                unset($values['turnover']);

                $result[] = [
                    'currency' => app(CurrencyResource::class, ['resource' => $currency]),
                    'values'   => $values,
                ];
            }
        }

        return $result;
    }
}
