<?php

namespace App\Http\Resources\Client\Stats\Deal;

use App\Http\Resources\Admin\PaymentSystem\PaymentSystemResource;
use App\Http\Resources\BaseResource;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Directory\CurrencyRepo;

/**
 * Class DealPaymentSystemsStatsResource
 *
 * @package App\Http\Resources\Stats
 * @property array $resource
 */
class DealPaymentSystemsStatsResource extends BaseResource
{
    /**
     * @var CurrencyRepo
     */
    private $repo;
    /**
     * @var Currency
     */
    private $currencyUsd;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $this->repo = app(CurrencyRepo::class);
        $this->currencyUsd = $this->repo->search('USD')->take()->get()->first();

        return $this->convert($this->resource);
    }

    /**
     * @param $data
     *
     * @return array
     */
    protected function convert($data)
    {
        $result = [];
        foreach ($data as $paymentSystemId => $values) {
            $paySystem = PaymentSystem::find($paymentSystemId);
            if ($paySystem) {
                $values['turnover_usd'] = currencyFromCoins($values['turnover'], $this->currencyUsd);
                unset($values['turnover']);

                $result[] = [
                    'payment_system' => app(PaymentSystemResource::class, ['resource' => $paySystem]),
                    'values'   => $values,
                ];
            }
        }

        return $result;
    }
}
