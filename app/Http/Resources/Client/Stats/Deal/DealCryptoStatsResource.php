<?php

namespace App\Http\Resources\Client\Stats\Deal;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\Currency\CurrencyResource;
use App\Repositories\Directory\CryptoCurrencyRepo;

/**
 * Class DealCryptoStatsResource
 *
 * @package App\Http\Resources\Stats
 * @property array $resource
 */
class DealCryptoStatsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return $this->convert($this->resource);
    }

    /**
     * @param $data
     *
     * @return array
     */
    protected function convert($data)
    {
        $result = [];
        foreach ($data as $code => $values) {
            $crypto = app(CryptoCurrencyRepo::class)->getByCode($code);
            if ($crypto) {
                $values['turnover'] = currencyFromCoins($values['turnover'], $crypto);

                $result[] = [
                    'cryptoCurrency' => app(CurrencyResource::class, ['resource' => $crypto]),
                    'values'   => $values,
                ];
            }
        }

        return $result;
    }
}
