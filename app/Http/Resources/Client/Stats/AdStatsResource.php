<?php

namespace App\Http\Resources\Client\Stats;

use App\Contracts\Statistics\Ads\AdStatisticsContract;
use App\Http\Resources\BaseResource;
use App\Services\Statistics\Ads\AdStatistics;

/**
 * Class AdStatsResource
 *
 * @package App\Http\Resources\Stats
 * @property AdStatisticsContract|AdStatistics $resource
 */
class AdStatsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'deals'                  => $this->resource->deals(),
            'crypto_turnover'        => $this->adapt($this->resource->cryptoTurnover()),
            'fiat_turnover'          => currencyFromCoins(
                $this->resource->fiatTurnover(),
                $this->resource->getAd()->currency
            ),
            'weighted_average_price' => $this->resource->weightedAveragePrice(),
        ];
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->getAd()->cryptoCurrency);
    }
}
