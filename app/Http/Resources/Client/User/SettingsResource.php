<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;

class SettingsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email_notification_required'    => $this->email_notification_required,
            'phone_notification_required'    => $this->phone_notification_required,
            'telegram_notification_required' => $this->telegram_notification_required,
            'deal_notification_required'     => $this->deal_notification_required,
            'message_notification_required'  => $this->message_notification_required,
            'push_notification_required'     => $this->push_notification_required,
        ];
    }
}
