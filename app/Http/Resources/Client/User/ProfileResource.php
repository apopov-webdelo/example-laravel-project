<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;
use App\Models\User\User;
use App\Repositories\Balance\BalanceRepo;
use App\Utils\User\UserChangesState;

/**
 * Class ProfileResource
 *
 * @package App\Http\Resources\User
 * @property User $resource
 */
class ProfileResource extends BaseResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->resource;

        /** @var BalanceRepo $balanceRepo */
        $balanceRepo = app(BalanceRepo::class);

        /** @var UserChangesState $changesState */
        $changesState = app(UserChangesState::class, ['state' => $user->changesAvailable]);

        return [
            'id'                => $user->id,
            'login'             => $user->login,
            'avatar'            => $user->image ? $user->image->getUrl() : null,
            'email'             => $user->email,
            'created_at'        => $this->resource->created_at,
            'updated_at'        => $this->resource->updated_at ? $this->resource->updated_at : null,
            'last_visit'        => $user->last_seen,
            'trust_coef'        => $user->reputation->rate,
            'country'           => $user->settings->country,
            'language'          => $user->settings->language,
            'reputation'        => $user->reputation,
            'reviews'           => $user->reviewsIncome,
            'appeal'            => $user->appeal,
            'totalBalanceInUSD' => $balanceRepo->getTotalAmountInFiat($user, 2),
            'is_social'         => $changesState->isOAuthRegistration(),
            'can_set_login'     => $changesState->canSetLogin(),
            'can_set_email'     => $changesState->canSetEmail(),
        ];
    }
}
