<?php

namespace App\Http\Resources\Client\User;

use App\Models\User\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 *
 * @property-read User $resource
 * @package App\Http\Resources\User
 */
class SpoofingProtectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $user */
        $user      = $this->resource;
        $domain = str_replace('https://', '', config('app.states.front.main'));
        $domain = str_replace('/', '', $domain);

        $signature = [
            $domain,
            $user->id,
            $user->login,
            $user->image ? $user->image->getUrl() : '',
            replace(config('app.states.front.profile'), ['user_id' => $user->id]),
            config('app.bro_chat.secret_key')
        ];

        return [
            'signature'  => md5(implode('', $signature))
        ];
    }
}
