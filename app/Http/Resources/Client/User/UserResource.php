<?php

namespace App\Http\Resources\Client\User;

use App\Contracts\AuthenticatedContract;
use App\Models\User\User;
use App\Repositories\User\NoteRepo;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 *
 * @property-read User $resource
 * @package App\Http\Resources\User
 */
class UserResource extends JsonResource
{
    /**
     * @var User
     */
    protected $authUser;

    /**
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->authUser = app(AuthenticatedContract::class);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        if ($this->authUser) {
            $note = app(NoteRepo::class)->filterByAuthor($this->authUser)
                                        ->filterByRecipient($this->resource)
                                        ->take()->first();

            $noteText = $note ? $note->note : null;
            $noteId = $note ? $note->id : null;
        }

        $user = $this->resource;

        return [
            'id'              => $user->id,
            'login'           => $user->login,
            'avatar'          => $user->image ? $user->image->getUrl() : null,
            'last_seen'       => $user->last_seen,
            'trust_coef'      => $user->reputation->rate,
            'negative_count'  => $user->reputation->negative_count,
            'positive_count'  => $user->reputation->positive_count,
            'appeal'          => $user->appeal,
            'statistics'      => app(StatisticsResource::class, ['resource' => $user->statistic]),
            'email_confirmed' => $user->security->email_confirmed,
            'phone_confirmed' => $user->security->phone_confirmed,
            'created_at'      => $this->resource->created_at,
            $this->mergeWhen($this->authUser, [
                'is_blocked'    => !$this->authUser ? : $this->authUser->isInBlacklist($this->resource),
                'is_blocked_by' => !$this->authUser ? : $this->resource->isInBlacklist($this->authUser),
                'is_favorite'   => !$this->authUser ? : $this->authUser->isInFavoriteList($this->resource),
                'note'          => !$this->authUser ? : $noteText,
                'note_id'       => !$this->authUser ? : $noteId,
            ]),
        ];
    }
}
