<?php

namespace App\Http\Resources\Client\User\Security;

use App\Models\User\SecurityLog;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SecurityLogsResource
 *
 * @property SecurityLog $resource
 * @package App\Http\Resources\Client\User\Security
 */
class SecurityLogsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->resource->id,
            'status'      => $this->resource->status,
            'status_desc' => $this->resource->status_desc,
            'type'        => $this->convertTypeToLang($this->resource->type),
            'time'        => $this->resource->created_at,
        ];
    }

    /**
     * @param string $type
     *
     * @return array|string|null
     */
    private function convertTypeToLang(string $type)
    {
        return __('security_logs.' . $type);
    }
}
