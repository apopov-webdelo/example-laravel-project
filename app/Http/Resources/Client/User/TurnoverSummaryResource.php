<?php

namespace App\Http\Resources\Client\User;

use App\Facades\CryptoExchange;
use App\Http\Resources\BaseResource;
use App\Models\Balance\Balance;
use App\Models\Directory\FiatConstants;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BalanceSummaryResource
 *
 * @property User $resource
 *
 * @package App\Http\Resources\User
 */
class TurnoverSummaryResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Balance $item */
        /** @var Collection $balance */
        //TODO: Check Etherium exchange rate
        $balance = collect([]);
        foreach ($this->resource->balance as $item) {
            $balance->push([
                'code'     => $item->cryptoCurrency->code,
                'turnover' => currencyFromCoins($item->turnover, $item->cryptoCurrency),
                'usd'      => CryptoExchange::convert(
                    currencyFromCoins($item->turnover, $item->cryptoCurrency),
                    $item->cryptoCurrency->getCode(),
                    FiatConstants::USD
                )
            ]);
        }

        return [
            'data'    => $balance->toArray(),
            'turnoverInUSD' => $balance->sum('usd'),
        ];
    }
}
