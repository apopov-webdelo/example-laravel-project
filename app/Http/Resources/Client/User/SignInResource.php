<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;
use App\Models\User\User;
use Illuminate\Http\Response;

/**
 * Class SignInResource
 *
 * @property User resource
 * @package App\Http\Resources\Client\User
 */
class SignInResource extends BaseResource
{
    /**
     * @var string
     */
    protected $token;

    public function __construct($resource, int $status_code = Response::HTTP_OK, string $token = null)
    {
        parent::__construct($resource, $status_code);
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->resource;

        return [
            'id'               => $user->id,
            'token'            => $this->token,
            'photo'            => '',
            'country'          => $user->settings->country_id ? $user->settings->country->title : null,
            'language'         => $user->settings->language_id ? $user->settings->language->title : null,
        ];
    }
}
