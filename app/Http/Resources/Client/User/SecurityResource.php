<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;

class SecurityResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email_confirmed'  => $this->email_confirmed,
            'phone_confirmed'  => $this->phone_confirmed,
            'google2fa_status' => $this->google2fa_status
        ];
    }
}
