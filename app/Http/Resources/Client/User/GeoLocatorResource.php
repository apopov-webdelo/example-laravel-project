<?php

namespace App\Http\Resources\Client\User;

use App\Facades\GeoLocator;
use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\Country\CountryResource;
use App\Http\Resources\Client\Directory\Currency\CurrencyResource;
use App\Models\Directory\Country;
use App\Repositories\Directory\CountryRepo;
use App\Repositories\Directory\CurrencyRepo;

/**
 * Class GeoLocatorResource
 * @package App\Http\Resources\User
 */
class GeoLocatorResource extends BaseResource
{
    /**
     * @var CountryRepo
     */
    protected $countryRepo;

    /**
     * @var CurrencyRepo
     */
    protected $currencyRepo;

    /**
     * @param mixed $resource
     * @param CountryRepo $countryRepo
     * @param CurrencyRepo $currencyRepo
     */
    public function __construct(
        $resource,
        CountryRepo $countryRepo,
        CurrencyRepo $currencyRepo
    ) {
        parent::__construct($resource);
        $this->countryRepo  = $countryRepo;
        $this->currencyRepo = $currencyRepo;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'country'  => $this->getCountryResource(),
            'currency' => $this->getCurrencyResource(),
            'timezone' => GeoLocator::getTimezone($this->resource),
        ];
    }

    /**
     * Return CountryResource or null if could not find by alpha code
     *
     * @return mixed|string
     */
    protected function getCountryResource()
    {
        $country = $this->getCountryByAlpha2();
        return $country
            ? app(CountryResource::class, ['resource' => $country])
            : null;
    }

    /**
     * @return Country|null
     */
    protected function getCountryByAlpha2()
    {
//        TODO: Disable default country before deployment
//        $alpha2 = GeoLocator::getCountryAlpha2($this->resource);
//        return $this->countryRepo->filterByAlpha2($alpha2)->take()->get()->first();

        return Country::find(1);
    }

    /**
     * @return \Illuminate\Foundation\Application|mixed|null
     */
    protected function getCurrencyResource()
    {
        $country = $this->getCountryByAlpha2();
        return $country
            ? app(CurrencyResource::class, ['resource' => $country->currency])
            : null;
    }
}
