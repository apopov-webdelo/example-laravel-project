<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;

class ContactResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'phone'           => $this->phone,
            'phone_confirmed' => $this->security->phone_confirmed,

            'email'           => $this->email,
            'email_confirmed' => $this->security->email_confirmed,

            'telegram_id' => $this->telegram_id,
            'telegram_id_confirmed' => $this->security->telegram_id_confirmed,
        ];
    }
}
