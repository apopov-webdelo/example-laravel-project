<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;

class ReputationResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'positive_count' => $this->positive_count,
            'negative_count' => $this->negative_count,
        ];
    }
}
