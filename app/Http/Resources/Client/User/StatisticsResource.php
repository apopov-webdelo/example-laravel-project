<?php

namespace App\Http\Resources\Client\User;

use App\Http\Resources\BaseResource;
use App\Models\User\Statistic;

/**
 * Class StatisticsResource
 *
 * @property Statistic $resource
 *
 * @package App\Http\Resources\User
 */
class StatisticsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at?$this->resource->updated_at:null,
        ]);
    }
}
