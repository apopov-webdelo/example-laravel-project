<?php

namespace App\Http\Resources\Client\User\Review;

use App\Http\Resources\BaseResource;

class ReviewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at?$this->resource->updated_at:null,
        ]);
    }
}
