<?php

namespace App\Http\Resources\Client\Auth;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AuthSocialCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => AuthSocialResource::collection($this),
        ];
    }
}
