<?php

namespace App\Http\Resources\Client\Auth;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class AuthSocialResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource;
    }
}
