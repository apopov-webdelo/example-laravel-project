<?php

namespace App\Http\Resources\Client\Ad;

use App\Http\Resources\BaseResource;
use App\Models\Ad\Ad;
use App\Models\Ad\AdAverageRate;
use App\Models\Ad\AdCryptoTurnover;
use App\Models\Ad\AdDealsQuantity;
use App\Models\Ad\AdFiatTurnover;
use App\Repositories\Directory\BankRepo;

/**
 * Class AdResource
 *
 * @package App\Http\Resources\Ad
 * @property Ad $resource
 */
class AdResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /** @var AdDealsQuantity $dealsQuantity */
        /** @var AdCryptoTurnover $cryptoTurnover */
        /** @var AdFiatTurnover $fiatTurnover */
        /** @var AdAverageRate $averageRate */
        /** @var BankRepo $bankRepo */

        $this->resource->load([
            'cryptoCurrency',
            'paymentSystem',
            'country',
            'currency',
        ]);

        $dealsQuantity = app(AdDealsQuantity::class, ['ad' => $this->resource]);
        $cryptoTurnover = app(AdCryptoTurnover::class, ['ad' => $this->resource]);
        $fiatTurnover = app(AdFiatTurnover::class, ['ad' => $this->resource]);
        $averageRate = app(AdAverageRate::class, ['ad' => $this->resource]);

        return array_merge($this->resource->toArray(), [
            'price'              => $this->resource->accuracyPrice,
            'min'                => $this->resource->accuracyMin,
            'max'                => $this->resource->accuracyMax,
            'original_max'       => $this->resource->accuracyOriginalMax,
            'payment_system'     => $this->resource->paymentSystem,
            'crypto_currency'    => $this->resource->cryptoCurrency,
            'deals_count'        => $dealsQuantity->get(),
            'crypto_turnover'    => $this->adapt($cryptoTurnover->get()),
            'fiat_turnover'      => $fiatTurnover->get(),
            'turnover'           => $this->resource->turnover ? $this->adapt($this->resource->turnover) : 0,
            'average_rate'       => $averageRate->get(),
            'created_at'         => $this->resource->created_at,
            $this->mergeWhen($this->paymentSystem->isBank(), [
                'banks' => $this->resource->banks,
            ]),
            'commission_percent' => $this->resource->accuracyCommissionPercent,
        ]);
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->cryptoCurrency);
    }
}
