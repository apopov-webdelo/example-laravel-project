<?php

namespace App\Http\Resources\Client\Dashboard;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Market\DealAvailableResource;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Ad\Ad;

/**
 * Class AdResource
 *
 * @package App\Http\Resources\Dashboard
 * @property Ad $resource
 */
class AdResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $ad = $this->resource;

        return [
            'id'                 => $ad->id,
            'author'             => app(UserResource::class, ['resource' => $ad->author]),
            'payment_system'     => [
                'id'    => $ad->paymentSystem->id,
                'title' => $ad->paymentSystem->title,
            ],
            $this->mergeWhen($ad->paymentSystem->isBank(), [
                'banks' => $ad->banks,
            ]),
            'country'            => $ad->country->title,
            'is_sale'            => $ad->is_sale,
            'currency'           => [
                'id'   => $ad->currency->id,
                'code' => $ad->currency->code,
            ],
            'crypto_currency'    => [
                'id'    => $ad->cryptoCurrency->id,
                'code'  => $ad->cryptoCurrency->code,
                'title' => $ad->cryptoCurrency->title,
            ],
            'price'              => $ad->accuracyPrice,
            'min'                => $ad->accuracyMin,
            'max'                => $ad->accuracyMax,
            'is_active'          => $ad->is_active,
            'created_at'         => $ad->created_at,
            $this->mergeWhen(auth()->check(), [
                'is_deal_available' => app(DealAvailableResource::class, ['resource' => $this]),
            ]),
            'commission_percent' => $ad->accuracyCommissionPercent,
            'conditions'         => $ad->conditions,
        ];
    }
}
