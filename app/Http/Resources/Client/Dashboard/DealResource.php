<?php

namespace App\Http\Resources\Client\Dashboard;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Chat\ChatResource;
use App\Http\Resources\Client\Deal\DealStatusResource;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Repositories\Chat\ChatRepo;
use Illuminate\Support\Facades\Auth;

/**
 * Class DealResource
 *
 * @property Deal $resource
 *
 * @package App\Http\Resources\Dashboard
 */
class DealResource extends BaseResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function toArray($request)
    {
        /** @var ChatRepo $chatRepo */
        $deal = $this->resource;
        $chatRepo = app(ChatRepo::class);
        $chat = $chatRepo->getDealChat($deal);
        $user = Auth::user();

        $isSale = $user instanceof User
            ? $deal->isSeller($user, false)
            : null;

        return [
            'id'             => $deal->id,
            'author'         => app(UserResource::class, ['resource' => $deal->author]),
            'offer'          => app(UserResource::class, ['resource' => $deal->ad->author]),
            'ad'             => app(AdResource::class, ['resource' => $deal->ad]),
            'status'         => app(DealStatusResource::class, ['resource' => $deal->getCurrentStatus()]),
            'status_history' => DealStatusResource::collection($deal->statuses),

            $this->mergeWhen($isSale, [
                'is_sale' => $isSale,
            ]),

            'payment_system'  => [
                'id'    => $deal->ad->paymentSystem->id,
                'title' => $deal->ad->paymentSystem->title,
            ],
            'currency'        => [
                'id'    => $deal->ad->currency->id,
                'title' => $deal->ad->currency->title,
            ],
            'crypto_currency' => [
                'id'    => $deal->ad->cryptoCurrency->id,
                'title' => $deal->ad->cryptoCurrency->title,
            ],
            'crypto_amount'   => $this->adapt($deal->crypto_amount),
            'price'           => currencyFromCoins($deal->price, $deal->ad->currency),
            'fiat_amount'     => currencyFromCoins($deal->fiat_amount, $deal->ad->currency),
            'conditions'      => $deal->conditions,
            'commission'      => $this->adapt($deal->commission),

            $this->mergeWhen(is_object($chat), [
                'chat' => app(ChatResource::class, ['resource' => $chat]),
            ]),

            'time'       => $deal->time,
            'created_at' => $deal->created_at,
        ];
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->ad->cryptoCurrency);
    }
}
