<?php

namespace App\Http\Resources\Client\Market;

use App\Http\Resources\Client\Deal\DealStatusResource;
use App\Models\Deal\Deal;

/**
 * DealResource for market
 *
 * @package App\Http\Resources\Market
 * @property Deal $resource
 */
class DealResource extends \App\Http\Resources\Client\Dashboard\DealResource
{
    public function toArray($request)
    {
        $currency = $this->resource->ad->currency;

        return array_merge(parent::toArray($request), [
            'price'         => currencyFromCoins($this->resource->price, $currency),
            'fiat_amount'   => currencyFromCoins($this->resource->fiat_amount, $currency),
            'status'        => app(DealStatusResource::class, ['resource' => $this->resource->getCurrentStatus()]),
            'created_at'    => $this->resource->created_at,
            'crypto_amount' => $this->adapt($this->resource->crypto_amount),
            'commission'    => $this->adapt($this->resource->commission),
        ]);
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->ad->cryptoCurrency);
    }
}
