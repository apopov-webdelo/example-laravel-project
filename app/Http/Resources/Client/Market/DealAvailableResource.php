<?php

namespace App\Http\Resources\Client\Market;

use App\Contracts\RuleContract;
use App\Http\Resources\BaseResource;
use App\Rules\Ad\AdActiveRule;
use App\Rules\Ad\AdBlacklistFreeRule;
use App\Rules\Ad\AdCryptoCurrencyMinBalanceRule;
use App\Rules\Ad\AdDealCancellationPercentRule;
use App\Rules\Ad\AdEmailConfirmationRule;
use App\Rules\Ad\AdFavoritesRule;
use App\Rules\Ad\AdMinDealFinishedCountRule;
use App\Rules\Ad\AdNotAuthorRule;
use App\Rules\Ad\AdPhoneConfirmationRule;
use App\Rules\Ad\AdReputationRateRule;
use App\Rules\Ad\AdTorDeniedRule;
use App\Rules\Ad\AdTurnOverRule;

/**
 * Class DealAvailableResource
 * @package App\Http\Resources\Market
 */
class DealAvailableResource extends BaseResource
{
    /**
     * @var array $messages
     */
    private $messages = [];

    /**
     * @var array $dealAvailabilityRules
     */
    private $dealAvailabilityRules = [
        AdActiveRule::class,
        AdBlacklistFreeRule::class,
        AdCryptoCurrencyMinBalanceRule::class,
        AdDealCancellationPercentRule::class,
        AdEmailConfirmationRule::class,
        AdFavoritesRule::class,
        AdNotAuthorRule::class,
        AdPhoneConfirmationRule::class,
        AdReputationRateRule::class,
        AdTurnOverRule::class,
        AdMinDealFinishedCountRule::class,
        AdTorDeniedRule::class
    ];

    private function isDealAvailable()
    {
        $result = true;
        foreach ($this->dealAvailabilityRules as $ruleClass) {
            /** @var RuleContract $rule */
            $rule = app($ruleClass, [ 'ad' => $this->resource->resource ]);
            if (false === $rule->check()) {
                $result = false;
                array_push($this->messages, $rule->message());
            };
        }

        return $result;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'result'   => $this->isDealAvailable(),
            'messages' => $this->messages,
        ];
    }
}
