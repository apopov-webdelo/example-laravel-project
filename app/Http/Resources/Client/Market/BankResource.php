<?php

namespace App\Http\Resources\Client\Market;

use App\Models\Directory\Bank;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BaseResource;

/**
 * Class BankResource
 *
 * @property Bank $resource
 *
 * @package App\Http\Resources\Client\Market
 */
class BankResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->resource->title,
            'id'    => $this->resource->id,
            'icon'  => $this->resource->image ? $this->resource->image->getUrl() : null
        ];
    }
}
