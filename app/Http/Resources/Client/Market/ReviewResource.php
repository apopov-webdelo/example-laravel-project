<?php

namespace App\Http\Resources\Client\Market;

use App\Http\Resources\BaseResource;

/**
 * Class ReviewResource
 *
 * @property int $id
 * @property string|null $message
 * @property int $rate
 * @property string $trust
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $author_id
 * @property int $recipient_id
 * @property-read \App\Models\User\User $author
 * @property-read \App\Models\User\User $recipient
 * @package App\Http\Resources\Client\Market
 */
class ReviewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'message'    => $this->message,
            'trust'      => $this->trust,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'from_user'  => new ReviewAuthorResource($this->author),
        ];
    }
}
