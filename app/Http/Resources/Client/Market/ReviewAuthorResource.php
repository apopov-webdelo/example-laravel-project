<?php

namespace App\Http\Resources\Client\Market;

use App\Http\Resources\BaseResource;

/**
 * Class ReviewAuthorResource
 *
 * @package App\Http\Resources\User
 */
class ReviewAuthorResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'login' => $this->login
        ];
    }
}
