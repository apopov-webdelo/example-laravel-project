<?php

namespace App\Http\Resources\Client\Chat;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Message\LastMessageResource;
use App\Http\Resources\Client\Message\MessageResource;
use App\Http\Resources\Client\User\UserCollection;
use App\Http\Resources\Client\User\UserResource;
use App\Models\Chat\Chat;
use App\Repositories\Deal\DealRepo;

/**
 * Class ChatResource
 *
 * @property Chat $resource
 *
 * @package App\Http\Resources\Chat
 */
class ChatResource extends BaseResource
{
    /**
     * @var DealRepo $repo
     */
    private $repo;

    /**
     * ChatResource constructor.
     *
     * @param      $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->repo = app(DealRepo::class);
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $additional = [
            'created_at' => $this->resource->created_at,
            'type'       => 'private',
        ];

        $deals = $this->repo->filterByChat($this->resource)->take();

        if (!$deals->get()->isEmpty()) {
            $additional['deal_id'] = $deals->first()->id;
            $additional['type']    = 'deal';
        }

        $additional['recipients'] = UserResource::collection($this->resource->recipients()->get());

        $lastMessage = $this->resource->lastMessage();

        $additional['last_message'] = $lastMessage ?
            app(LastMessageResource::class, ['resource' => $lastMessage])
            : null;

        return array_merge(parent::toArray($request), $additional);
    }
}
