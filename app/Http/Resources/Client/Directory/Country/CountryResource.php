<?php

namespace App\Http\Resources\Client\Directory\Country;

use App\Http\Resources\BaseResource;
use App\Models\Directory\Country;

class CountryResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /* @var Country $this */
        return [
            'label'       => $this->title,
            'value'       => $this->id,
            'short_title' => $this->alpha2,
        ];
    }
}
