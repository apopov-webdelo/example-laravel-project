<?php

namespace App\Http\Resources\Client\Directory\Country;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\Bank\BankResource;
use App\Http\Resources\Client\Directory\Currency\CurrencyResource;
use App\Models\Directory\Country;
use App\Utils\Filters\PaymentSystemByCountryFilter;

class CountryFullResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /* @var Country $this */
        $paymentSystems = app(PaymentSystemByCountryFilter::class, [
            'paymentSystems' => $this->payment_systems,
            'country'        => $this->resource,
        ]);

        return [
            'country'         => app(CountryResource::class, ['resource' => $this]),
            'currency'        => app(CurrencyResource::class, ['resource' => $this->currency]),
            'currencies'      => CurrencyResource::collection($this->currencies),
            'payment_systems' => $paymentSystems(),
            'banks'           => BankResource::collection($this->banks),
        ];
    }
}
