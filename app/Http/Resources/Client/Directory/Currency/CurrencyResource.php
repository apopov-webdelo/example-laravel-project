<?php

namespace App\Http\Resources\Client\Directory\Currency;

use App\Http\Resources\BaseResource;
use App\Models\Directory\Currency;

class CurrencyResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /* @var Currency $this */
        return [
            'label' => $this->title,
            'value' => $this->id,
            'short_title' => $this->code,
            'accuracy'    => $this->getAccuracy(),
        ];
    }
}
