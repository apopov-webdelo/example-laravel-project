<?php

namespace App\Http\Resources\Client\Directory\PaymentSystem;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\Currency\CurrencyResource;
use App\Models\Directory\PaymentSystem;

class PaymentSystemResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /* @var PaymentSystem $this */
        return [
            'label'      => $this->title,
            'value'      => $this->id,
            'currencies' => CurrencyResource::collection($this->resource->currencies()->get()->unique()),
            // todo until payment systems separation is discussed
            'is_bank'    => $this->isBank(),
        ];
    }
}
