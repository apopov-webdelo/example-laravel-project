<?php

namespace App\Http\Resources\Client\Directory\CryptoCurrency;

use App\Http\Resources\BaseResource;
use App\Models\Directory\CryptoCurrency;

class CryptoCurrencyResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var CryptoCurrency $cryptoCurrency */
        $cryptoCurrency = $this->resource;
        return [
            'value'       => $cryptoCurrency->id,
            'label'       => $cryptoCurrency->getTitle(),
            'short_title' => $cryptoCurrency->getCode(),
            'accuracy'    => $cryptoCurrency->getAccuracy(),
        ];
    }
}
