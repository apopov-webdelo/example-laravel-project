<?php

namespace App\Http\Resources\Client\Directory\Bank;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Client\Directory\Currency\CurrencyResource;
use App\Models\Directory\Bank;

/**
 * Class BankResource
 *
 * @property Bank $resource
 *
 * @package App\Http\Resources\Client\Directory\Bank
 */
class BankResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'label'      => $this->resource->title,
            'value'      => $this->resource->id,
            'icon'       => $this->resource->image ? $this->resource->image->getUrl() : null,
            'currencies' => CurrencyResource::collection($this->resource->currencies),
        ];
    }
}
