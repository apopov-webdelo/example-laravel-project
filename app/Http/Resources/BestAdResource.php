<?php

namespace App\Http\Resources;

use App\Http\Resources\Client\Market\AdResource;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BestAdResource
 *
 * @property Builder $resource
 *
 * @package App\Http\Resources
 */
class BestAdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'best'       => $this->getBestAdResource(),
            'followings' => $this->getFollowingsCollection()
        ];
    }

    /**
     * @return \Illuminate\Foundation\Application|mixed
     */
    private function getBestAdResource()
    {
        $resource = clone $this->resource;
        return app(AdResource::class, ['resource' => $resource->first()]);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    private function getFollowingsCollection()
    {
        $collection = $this->resource->get();
        $collection->shift();
        return AdResource::collection($collection);
    }
}
