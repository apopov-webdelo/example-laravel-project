<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

/**
 * Class BaseResource
 *
 * @package App\Http\Resources
 */
abstract class BaseResource extends JsonResource
{
    /**
     * @var int Symfony response constant
     */
    protected $status_code;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param  int   $status_code
     */
    public function __construct($resource, $status_code = Response::HTTP_OK)
    {
        parent::__construct($resource);

        $this->status_code = $status_code;
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response $response
     *
     * @return void
     */
    public function withResponse($request, $response)
    {
        if ($this->status_code) {
            $response->setStatusCode($this->status_code);
        }
    }
}