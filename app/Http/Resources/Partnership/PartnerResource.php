<?php

namespace App\Http\Resources\Partnership;

use App\Http\Resources\Client\User\UserResource;
use App\Models\Partnership\Partner;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProfileResource
 *
 * @property Partner $resource
 *
 * @package App\Http\Resources\Partnership
 */
class PartnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'partner_id' => $this->resource->getCode(),
            'user'       => app(UserResource::class, ['resource' => $this->resource->user]),
            'program'    => app(PartnershipProgramResource::class, ['resource' => $this->resource->partnershipProgram])
        ];
    }
}
