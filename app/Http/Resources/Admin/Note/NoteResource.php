<?php

namespace App\Http\Resources\Admin\Note;

use App\Http\Resources\Admin\Client\ClientResource;
use App\Models\User\Note;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NoteResource
 *
 * @property Note $resource
 *
 * @package App\Http\Resources\Admin\Note
 */
class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'created_at' => $this->resource->created_at->format('Y-m-d H:i:s P'),
            'recipient'  => app(ClientResource::class, ['resource'=>$this->resource->recipient]),
            'author'  => app(ClientResource::class, ['resource'=>$this->resource->author])
        ]);
    }
}
