<?php

namespace App\Http\Resources\Admin\Note;

use App\Http\Resources\BaseCollection;

class NoteCollection extends BaseCollection
{
    public function getCollection()
    {
        return NoteResource::collection($this);
    }
}
