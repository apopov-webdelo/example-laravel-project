<?php

namespace App\Http\Resources\Admin\ActivityLog;

use App\Http\Resources\Admin\Admin\AdminResource;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Admin\Admin;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ActivityLogResource
 *
 * @property ActivityLog $resource
 *
 * @package App\Http\Resources\Admin\ActivityLog
 */
class ActivityLogResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->resource->id,
            'message'    => $this->resource->message,
            'level'      => $this->resource->level,
            'type'       => $this->resource->type,
            'created_at' => $this->resource->created_at,
            'author'     => ($this->resource->authorable instanceof Admin)
                ? app(AdminResource::class, ['resource' => $this->resource->authorable])
                : app(ClientResource::class, ['resource' => $this->resource->authorable]),
        ];
    }
}
