<?php

namespace App\Http\Resources\Admin\ActivityLog;

use App\Http\Resources\BaseCollection;

/**
 * Class ActivityLogCollection
 *
 * @package App\Http\Resources\Admin\Ad
 */
class ActivityLogCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return ActivityLogResource::collection($this);
    }
}
