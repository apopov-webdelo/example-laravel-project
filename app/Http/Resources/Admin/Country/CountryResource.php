<?php

namespace App\Http\Resources\Admin\Country;

use App\Models\Directory\Bank;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BankResource
 *
 * @property Bank $resource
 *
 * @package App\Http\Resources\Admin\Bank
 */
class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
