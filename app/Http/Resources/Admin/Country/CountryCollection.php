<?php

namespace App\Http\Resources\Admin\Country;

use App\Http\Resources\BaseCollection;

class CountryCollection extends BaseCollection
{
    public function getCollection()
    {
        return CountryResource::collection($this);
    }
}
