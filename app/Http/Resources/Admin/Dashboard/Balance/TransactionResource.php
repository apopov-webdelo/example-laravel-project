<?php

namespace App\Http\Resources\Admin\Dashboard\Balance;

use App\Facades\Robot;
use App\Http\Resources\Admin\Balance\TransactionReasonResource;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\Admin\CryptoCurrency\CryptoCurrencyResource;
use App\Models\Balance\Balance;
use App\Models\Balance\Transaction;
use App\Models\User\User;
use App\Repositories\Balance\TransactionBalanceStateRepo;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TransactionResource
 *
 * @property Transaction $resource
 *
 * @package App\Http\Resources\Admin\Dashboard\Balance
 */
class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $transaction = $this->resource;

        /** @var TransactionBalanceStateRepo $stateRepo */
        $stateRepo = app(TransactionBalanceStateRepo::class);
        $state     = $stateRepo->getByTransaction($transaction, $this->getRobotUserBalance());

        $balanceAfter = $state->amount + ($this->isRobotUserReceiver()
                ? $transaction->amount
                : $transaction->amount * (-1));

        return [
            'id'              => $transaction->id,
            'amount'          => $this->getFormattedAmount(),
            'description'     => $transaction->description,
            'crypto_currency' => app(CryptoCurrencyResource::class, ['resource' => $transaction->getCryptoCurrency()]),
            'user'            => app(ClientResource::class, ['resource' => $this->getRobotUser()]),
            'reason'          => app(TransactionReasonResource::class, ['resource' => $transaction->reason]),
            'balance_before'  => currencyFromCoins($state->amount, $state->balance->cryptoCurrency, true),
            'balance_after'   => currencyFromCoins($balanceAfter, $state->balance->cryptoCurrency, true),
            'date'            => $transaction->created_at,
        ];
    }

    /**
     * Return amount in string format
     *
     * @return string
     */
    protected function getFormattedAmount()
    {
        $amount = currencyFromCoins($this->resource->amount, $this->getRobotUserBalance()->cryptoCurrency, true);
        return ($this->isRobotUserReceiver() ? '+' : '-') . $amount;
    }

    /**
     * @return bool
     */
    protected function isRobotUserReceiver(): bool
    {
        return $this->resource->receiverBalance->user_id == Robot::user()->id;
    }

    /**
     * Return balance for authorized user
     *
     * @return Balance
     */
    protected function getRobotUserBalance(): Balance
    {
        return $this->isRobotUserReceiver()
            ? $this->resource->receiverBalance
            : $this->resource->remitterBalance;
    }

    /**
     * Retrieve another user (non robot) in that transaction
     *
     * @return User
     */
    protected function getRobotUser(): User
    {
        return $this->isRobotUserReceiver()
            ? $this->resource->remitterBalance->user
            : $this->resource->receiverBalance->user;
    }
}
