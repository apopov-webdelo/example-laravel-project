<?php

namespace App\Http\Resources\Admin\Dashboard\Balance;

use App\Contracts\Balance\Admin\Dashboard\TotalSummaryStorageContract;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\Admin\CryptoCurrency\CryptoCurrencyResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TransactionSummaryResource
 *
 * @property TotalSummaryStorageContract $resource
 *
 * @package App\Http\Resources\Admin\Dashboard\Balance
 */
class TransactionSummaryResource extends JsonResource
{
    /**
     * @var float
     */
    private $withdraw_sum;

    /**
     * @var float
     */
    private $deposit_sum;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'withdraw_amount' => $this->getFormattedAmount($this->countWithdraws()),
            'deposit_amount'  => $this->getFormattedAmount($this->countDeposits()),
            'total_amount'    => currencyFromCoins($this->countTotal(), $this->resource->cryptoCurrency(), true),
            'user'            => app(ClientResource::class, ['resource' => $this->resource->user()]),
            'cryptoCurrency'  => app(CryptoCurrencyResource::class, ['resource' => $this->resource->cryptoCurrency()]),
        ];
    }

    /**
     *  Return amount in string format
     *
     * @param $amount
     *
     * @return string
     */
    protected function getFormattedAmount($amount)
    {
        $amount = currencyFromCoins($amount, $this->resource->cryptoCurrency(), true);
        return ($amount>0 ? '+' : '') . $amount;
    }

    protected function countWithdraws()
    {
        $sum = $this->resource->withdraws()
                              ->take()
                              ->pluck('amount')
                              ->sum() * -1;

        return $this->withdraw_sum ?? $this->withdraw_sum = $sum;
    }

    protected function countDeposits()
    {
        $sum = $this->resource->deposits()
                              ->take()
                              ->pluck('amount')
                              ->sum();

        return $this->deposit_sum ?? $this->deposit_sum = $sum;
    }

    protected function countTotal()
    {
        return $this->countDeposits() - $this->countWithdraws();
    }

}
