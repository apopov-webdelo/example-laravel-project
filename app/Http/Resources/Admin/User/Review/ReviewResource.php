<?php

namespace App\Http\Resources\Admin\User\Review;

use App\Http\Resources\Admin\Client\ClientResource;
use App\Models\User\Review;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ReviewResource
 *
 * @property Review $resource
 *
 * @package App\Http\Resources\Admin\User\Review
 */
class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'created_at' => $this->resource->created_at?$this->resource->created_at->format('Y-m-d H:i:s P'):null,
            'updated_at' => $this->resource->updated_at?$this->resource->updated_at->format('Y-m-d H:i:s P'):null,
            'recipient'  => app(ClientResource::class, ['resource'=>$this->resource->recipient]),
            'author'     => app(ClientResource::class, ['resource'=>$this->resource->author])
        ]);
    }
}
