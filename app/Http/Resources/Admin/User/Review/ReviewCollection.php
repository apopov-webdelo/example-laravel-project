<?php

namespace App\Http\Resources\Admin\User\Review;

use App\Http\Resources\BaseCollection;

/**
 * Class ReviewCollection
 *
 * @package App\Http\Resources\Admin\User\Review
 */
class ReviewCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return ReviewResource::collection($this);
    }
}
