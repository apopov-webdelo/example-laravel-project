<?php

namespace App\Http\Resources\Admin\User\Session;

use App\Http\Resources\BaseCollection;

class SessionCollection extends BaseCollection
{
    public function getCollection()
    {
        return SessionResource::collection($this);
    }
}
