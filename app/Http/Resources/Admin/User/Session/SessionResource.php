<?php

namespace App\Http\Resources\Admin\User\Session;

use App\Models\Session\Session;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SessionResource
 *
 * @property Session $resource
 * @package App\Http\Resources\Admin\User\Session
 */
class SessionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->resource->id,
            'ip'      => $this->resource->ip,
            'country' => $this->resource->country,
            'city'    => $this->resource->city,
            'os'      => $this->resource->os,
            'browser' => $this->resource->browser,
            'status'  => $this->resource->status,
            'type'    => $this->resource->type,
            'time'    => $this->resource->created_at,
        ];
    }
}
