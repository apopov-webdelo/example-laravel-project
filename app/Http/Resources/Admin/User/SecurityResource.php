<?php

namespace App\Http\Resources\Admin\User;

class SecurityResource extends \App\Http\Resources\Client\User\SecurityResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
