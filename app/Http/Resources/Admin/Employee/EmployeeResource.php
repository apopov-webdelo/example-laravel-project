<?php

namespace App\Http\Resources\Admin\Employee;

use App\Http\Resources\Admin\Permission\PermissionResource;
use App\Http\Resources\Admin\Role\RoleResource;
use App\Models\Admin\Admin;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EmployeeResource
 *
 * @property Admin $resource
 *
 * @package App\Http\Resources\Admin\Employee
 */
class EmployeeResource extends JsonResource
{
    /**
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->resource->id,
            'login'         => $this->resource->login,
            'name'          => $this->resource->name,
            'avatar'        => $this->resource->image ? $this->resource->image->getFullPathAttribute() : null,
            'email'         => $this->resource->email,
            'last_seen'     => $this->resource->last_seen ? $this->resource->last_seen->format('Y-m-d H:i:s') : '',
            'created_at'    => $this->resource->created_at ? $this->resource->created_at->format('Y-m-d H:i:s') : '',
            'updated_at'    => $this->resource->updated_at ? $this->resource->updated_at->format('Y-m-d H:i:s') : '',
            'phone'         => $this->resource->phone,
            'role'          => app(RoleResource::class, ['resource' => $this->resource->role]),
            'permissions'   => PermissionResource::collection($this->resource->permissions),
            'is_locked'     => $this->resource->isBlocked(),
        ];
    }
}
