<?php

namespace App\Http\Resources\Admin\Employee;

use App\Http\Resources\BaseCollection;

/**
 * Class EmployeeCollection
 * @package App\Http\Resources\Admin\Employee
 */
class EmployeeCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return EmployeeResource::collection($this);
    }
}
