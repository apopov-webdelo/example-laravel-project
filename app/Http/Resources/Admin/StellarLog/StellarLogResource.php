<?php

namespace App\Http\Resources\Admin\StellarLog;

use App\Models\Blockchain\StellarLog;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StellarLogsResource
 *
 * @property StellarLog $resource
 *
 * @package App\Http\Resources\Admin\StellarLogs
 */
class StellarLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
