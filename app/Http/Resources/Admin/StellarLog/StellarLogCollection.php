<?php

namespace App\Http\Resources\Admin\StellarLog;

use App\Http\Resources\BaseCollection;

/**
 * Class StellarLogsCollection
 *
 * @package App\Http\Resources\Admin\StellarLogs
 */
class StellarLogCollection extends BaseCollection
{
    public function getCollection()
    {
        return StellarLogResource::collection($this);
    }
}
