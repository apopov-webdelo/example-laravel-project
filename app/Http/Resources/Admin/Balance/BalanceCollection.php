<?php

namespace App\Http\Resources\Admin\Balance;

use App\Http\Resources\BaseCollection;

/**
 * Class BalanceCollection
 *
 * @package App\Http\Resources\Admin\Balance
 */
class BalanceCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return BalanceResource::collection($this);
    }
}
