<?php

namespace App\Http\Resources\Admin\Balance;

use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\BaseResource;
use App\Models\Balance\Transaction;

/**
 * @package App\Http\Resources\Balance
 * @property Transaction $resource
 */
class TransactionResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $transaction = $this->resource;
        return [
            'id'              => $transaction->id,
            'amount'          => $this->getFormattedAmount(),
            'crypto_currency' => $transaction->receiverBalance->cryptoCurrency,
            'date'            => $transaction->created_at,
            'description'     => $transaction->description,
            'receiver'        => app(ClientResource::class, ['resource' => $transaction->receiverBalance->user]),
            'remitter'        => app(ClientResource::class, ['resource' => $transaction->remitterBalance->user]),
            'reason'          => app(TransactionReasonResource::class, ['resource' => $transaction->reason]),
        ];
    }

    /**
     * Return amount in string format
     *
     * @return string
     */
    protected function getFormattedAmount()
    {
        return currencyFromCoins(
            $this->resource->amount,
            $this->resource->receiverBalance->cryptoCurrency,
            true
        );
    }
}
