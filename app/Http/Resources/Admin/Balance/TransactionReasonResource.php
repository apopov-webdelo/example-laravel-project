<?php

namespace App\Http\Resources\Admin\Balance;

use App\Models\Balance\TransactionReason;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TransactionReasonResource
 *
 * @package App\Http\Resources\Client\Balance
 * @property TransactionReason $resource
 */
class TransactionReasonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->toArray();
    }
}