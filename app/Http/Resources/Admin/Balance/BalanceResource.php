<?php

namespace App\Http\Resources\Admin\Balance;

use App\Facades\CryptoExchange;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\Admin\CryptoCurrency\CryptoCurrencyResource;
use App\Http\Resources\BaseResource;
use App\Models\Balance\Balance;
use App\Models\Directory\FiatConstants;

/**
 * Class BalanceResource
 *
 * @property Balance $resource
 *
 * @package App\Http\Resources\Admin\Balance
 */
class BalanceResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $balance = $this->resource;

        $amountInUSD = $balance->cryptoCurrency
            ? CryptoExchange::convert(
                $balance->accuracyAmount,
                $balance->cryptoCurrency->getCode(),
                FiatConstants::USD
            )
            : null;

        return [
            'id'              => $balance->id,
            'user'            => app(ClientResource::class, ['resource' => $balance->user]),
            'crypto_currency' => app(CryptoCurrencyResource::class, ['resource' => $balance->cryptoCurrency]),
            'amount'          => $this->adapt($balance->amount),
            'escrow'          => $this->adapt($balance->escrow),
            'due'             => $this->adapt($balance->due),
            'turnover'        => $this->adapt($balance->turnover),
            'commission'      => $balance->accuracyCommission,
            'amountInUSD'     => $amountInUSD,
        ];
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt($cryptoAmount)
    {
        return $cryptoAmount ? currencyFromCoins($cryptoAmount, $this->resource->cryptoCurrency) : null;
    }
}
