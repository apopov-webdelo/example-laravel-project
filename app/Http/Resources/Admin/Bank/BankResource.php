<?php

namespace App\Http\Resources\Admin\Bank;

use App\Models\Directory\Bank;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BankResource
 *
 * @property Bank $resource
 *
 * @package App\Http\Resources\Admin\Bank
 */
class BankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'icon'   => $this->resource->image ? $this->resource->image->getUrl() : null,
            'author' => $this->resource->author
        ]);
    }
}
