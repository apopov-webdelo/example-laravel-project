<?php

namespace App\Http\Resources\Admin\Bank;

use App\Http\Resources\BaseCollection;

class BankCollection extends BaseCollection
{
    public function getCollection()
    {
        return BankResource::collection($this);
    }
}
