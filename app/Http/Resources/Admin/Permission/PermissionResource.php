<?php

namespace App\Http\Resources\Admin\Permission;

use App\Models\Permission\Permission;
use App\Http\Resources\BaseResource;
use App\Models\Role\Role;
use App\Models\User\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PermissionResource
 *
 * @property Permission $resource
 *
 * @package App\Http\Resources\User
 */
class PermissionResource extends BaseResource
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->resource->id,
            'name'  => $this->resource->name
        ];
    }
}
