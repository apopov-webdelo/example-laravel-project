<?php

namespace App\Http\Resources\Admin\Permission;

use App\Http\Resources\BaseCollection;

/**
 * Class PermissionCollection
 *
 * @package App\Http\Resources\User
 */
class PermissionCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return PermissionResource::collection($this);
    }
}
