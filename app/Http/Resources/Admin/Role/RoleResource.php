<?php

namespace App\Http\Resources\Admin\Role;

use App\Http\Resources\BaseResource;
use App\Models\Role\Role;
use App\Models\User\User;

/**
 * Class RoleResource
 *
 * @property Role $resource
 *
 * @package App\Http\Resources\User
 */
class RoleResource extends BaseResource
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->resource->id,
            'name'        => $this->resource->name,
            'created_at'  => $this->resource->created_at?$this->resource->created_at->format('Y-m-d H:i:s P'):null,
            'updated_at'  => $this->resource->updated_at?$this->resource->updated_at->format('Y-m-d H:i:s P'):null,
            'permissions' => $this->resource->permissions
        ];
    }
}
