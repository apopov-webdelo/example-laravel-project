<?php

namespace App\Http\Resources\Admin\Role;

use App\Http\Resources\BaseCollection;

/**
 * Class RoleCollection
 *
 * @package App\Http\Resources\User
 */
class RoleCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return RoleResource::collection($this);
    }
}
