<?php

namespace App\Http\Resources\Admin\Currency;

use App\Http\Resources\BaseCollection;

class CurrencyCollection extends BaseCollection
{
    public function getCollection()
    {
        return CurrencyResource::collection($this);
    }
}
