<?php

namespace App\Http\Resources\Admin\Admin;

use App\Contracts\AuthenticatedContract;
use App\Http\Resources\BaseResource;
use App\Models\User\User;

/**
 * Class ClientResource
 *
 * @property User $resource
 *
 * @package App\Http\Resources\User
 */
class AdminResource extends BaseResource
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $admin = $this->resource;

        return [
            'id'              => $admin->id,
            'login'           => $admin->login,
            'avatar'          => 'assets/images/client/default-avatar.png',
            'last_seen'       => $admin->last_seen ? $admin->last_seen : '',
            'created_at'      => $admin->created_at ? $admin->created_at : '',
            'is_locked'       => $admin->isBlocked()
        ];
    }
}
