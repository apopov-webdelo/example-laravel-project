<?php

namespace App\Http\Resources\Admin\Admin;

use App\Http\Resources\BaseCollection;

/**
 * Class ClientCollection
 * @package App\Http\Resources\Admin\Client
 */
class AdminCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return AdminResource::collection($this);
    }
}
