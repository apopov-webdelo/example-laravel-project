<?php

namespace App\Http\Resources\Admin\Deal;

use App\Http\Resources\BaseCollection;

class DealCollection extends BaseCollection
{
    public function getCollection()
    {
        return DealResource::collection($this);
    }
}
