<?php

namespace App\Http\Resources\Admin\Deal;

use App\Http\Resources\Admin\Ad\AdResource;
use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\Admin\Chat\ChatResource;
use App\Http\Resources\Client\Deal\DealStatusResource;
use App\Repositories\Chat\ChatRepo;
use Illuminate\Http\Resources\Json\JsonResource;

class DealResource extends JsonResource
{
    public function toArray($request)
    {
        /** @var ChatRepo $chatRepo */
        $chatRepo = app(ChatRepo::class);
        $chat = $chatRepo->getDealChat($this->resource);
        $currency = $this->resource->ad->currency;

        return [
            'id'             => $this->resource->id,
            'author'         => app(ClientResource::class, ['resource'=>$this->resource->author]),
            'offer'          => app(ClientResource::class, ['resource'=>$this->resource->ad->author]),
            'ad'             => app(AdResource::class, ['resource'=>$this->resource->ad]),
            'status'         => app(DealStatusResource::class, ['resource'=>$this->resource->getCurrentStatus()]),
            'status_history' => $this->resource->statuses,
            'payment_system' => [
                'id'    => $this->resource->ad->paymentSystem->id,
                'title' => $this->resource->ad->paymentSystem->title
            ],
            'currency' => [
                'id'    => $this->resource->ad->currency->id,
                'title' => $this->resource->ad->currency->title
            ],
            'crypto_currency' => [
                'id'    => $this->resource->ad->cryptoCurrency->id,
                'title' => $this->resource->ad->cryptoCurrency->title
            ],
            'fiat_amount'    => currencyFromCoins($this->resource->fiat_amount, $currency),
            'crypto_amount'  => $this->adapt($this->resource->crypto_amount),
            'price'          => currencyFromCoins($this->resource->price, $currency),
            'conditions'     => $this->resource->conditions,
            'commission'     => $this->adapt($this->resource->commission),
            $this->mergeWhen(is_object($chat), [
                'chat' => app(ChatResource::class, ['resource' => $chat]),
            ]),
            'time'           => $this->resource->time,
            'created_at'     => $this->resource->created_at->format('Y-m-d H:i:s P'),
        ];
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->ad->cryptoCurrency);
    }
}
