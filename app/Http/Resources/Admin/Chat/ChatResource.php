<?php

namespace App\Http\Resources\Admin\Chat;

use App\Http\Resources\Admin\Client\ClientResource;
use App\Http\Resources\BaseResource;
use App\Models\Chat\Chat;
use App\Repositories\Deal\DealRepo;

/**
 * Class ChatResource
 *
 * @property Chat $resource
 *
 * @package App\Http\Resources\Chat
 */
class ChatResource extends BaseResource
{
    /**
     * @var DealRepo $repo
     */
    private $repo;

    /**
     * ChatResource constructor.
     *
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->repo = app(DealRepo::class);
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $additional = [
            'created_at' => $this->resource->created_at,
            'type'       => 'private',
        ];

        $deals = $this->repo->filterByChat($this->resource)->take();

        if (!$deals->get()->isEmpty()) {
            $additional['deal_id'] = $deals->first()->id;
            $additional['type']    = 'deal';
        }

        $additional['recipients'] = ClientResource::collection($this->resource->recipients()->get());

        return array_merge(parent::toArray($request), $additional);
    }
}
