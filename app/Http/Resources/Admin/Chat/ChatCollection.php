<?php

namespace App\Http\Resources\Admin\Chat;

use App\Models\Chat\Chat;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class ChatCollection
 *
 * @property Chat $resource
 *
 * @package App\Http\Resources\Chat
 */
class ChatCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ChatResource::collection($this),
            'pagination' => [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ],
        ];
    }
}
