<?php

namespace App\Http\Resources\Admin\Ad;

use App\Http\Resources\Admin\Client\ClientResource;
use App\Models\Ad\Ad;
use App\Models\Ad\AdAverageRate;
use App\Models\Ad\AdCryptoTurnover;
use App\Models\Ad\AdDealsQuantity;
use App\Models\Ad\AdFiatTurnover;
use App\Repositories\Directory\BankRepo;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AdResource
 *
 * @property Ad $resource
 *
 * @package App\Http\Resources\Admin\Ad
 */
class AdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /** @var AdDealsQuantity $dealsQuantity */
        /** @var AdCryptoTurnover $cryptoTurnover */
        /** @var AdFiatTurnover $fiatTurnover */
        /** @var AdAverageRate $averageRate */
        /** @var BankRepo $bankRepo */

        $this->resource->load([
            'cryptoCurrency',
            'paymentSystem',
            'country',
            'currency',
        ]);

        $dealsQuantity  = app(AdDealsQuantity::class, ['ad' => $this->resource]);
        $cryptoTurnover = app(AdCryptoTurnover::class, ['ad' => $this->resource]);
        $fiatTurnover   = app(AdFiatTurnover::class, ['ad' => $this->resource]);
        $averageRate    = app(AdAverageRate::class, ['ad' => $this->resource]);

        return array_merge($this->resource->toArray(), [
            'payment_system'     => $this->resource->paymentSystem,
            'crypto_currency'    => $this->resource->cryptoCurrency,
            'deals_count'        => $dealsQuantity->get(),
            'crypto_turnover'    => $this->adapt($cryptoTurnover->get()),
            'fiat_turnover'      => $fiatTurnover->get(),
            'author'             => app(ClientResource::class, ['resource' => $this->resource->author]),
            'turnover'           => $this->adapt($this->resource->turnover),
            'average_rate'       => $averageRate->get(),
            'created_at'         => $this->resource->created_at->format('Y-m-d H:i:s P'),
            $this->mergeWhen($this->paymentSystem->isBank(), [
                'banks' => $this->resource->banks,
            ]),
            'commission_percent' => $this->resource->accuracyCommissionPercent,
        ]);
    }

    /**
     * Convert crypto amount to needed type
     *
     * @param int $cryptoAmount
     *
     * @return float|string
     */
    protected function adapt(int $cryptoAmount)
    {
        return currencyFromCoins($cryptoAmount, $this->resource->cryptoCurrency);
    }
}
