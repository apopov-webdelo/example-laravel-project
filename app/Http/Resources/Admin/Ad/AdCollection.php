<?php

namespace App\Http\Resources\Admin\Ad;

use App\Http\Resources\BaseCollection;

class AdCollection extends BaseCollection
{
    public function getCollection()
    {
        return AdResource::collection($this);
    }
}
