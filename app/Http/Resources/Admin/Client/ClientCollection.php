<?php

namespace App\Http\Resources\Admin\Client;

use App\Http\Resources\BaseCollection;

/**
 * Class ClientCollection
 * @package App\Http\Resources\Admin\Client
 */
class ClientCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return ClientResource::collection($this);
    }
}
