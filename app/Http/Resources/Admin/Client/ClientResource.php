<?php

namespace App\Http\Resources\Admin\Client;

use App\Contracts\AuthenticatedContract;
use App\Http\Resources\BaseResource;
use App\Models\User\User;

/**
 * Class ClientResource
 *
 * @property User $resource
 *
 * @package App\Http\Resources\User
 */
class ClientResource extends BaseResource
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->user = app(AuthenticatedContract::class);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $client = $this->resource;

        return [
            'id'         => $client->id,
            'login'      => $client->login,
            'avatar'     => $client->image ? $client->image->getUrl() : null,
            'last_seen'  => $client->last_seen?$client->last_seen : '',
            'trust_coef' => $client->reputation->rate,
            'negative_count' => $client->reputation->negative_count,
            'positive_count' => $client->reputation->positive_count,
            'appeal'         => $client->appeal,
            'deals_count'    => $client->statistic->deals_count,
            'email'          => $client->email,
            'email_confirmed'=> $client->security->email_confirmed,
            'deal_cancellation'  => $client->statistic->deals_cancellation_percent,
            'phone'              => $client->phone,
            'phone_confirmed'    => $client->security->phone_confirmed,
            'created_at'         => $client->created_at ? $client->created_at : '',
            'is_locked'          => $client->isBlocked(),
            'is_trade_locked'    => $client->isTradeBlocked(),
            'is_google2fa_active'=> in_array($client->security->google2fa_status, ['activated', 'paused']),
        ];
    }
}
