<?php

namespace App\Http\Resources\Admin\PaymentSystem;

use App\Http\Resources\BaseCollection;

class PaymentSystemCollection extends BaseCollection
{
    public function getCollection()
    {
        return PaymentSystemResource::collection($this);
    }
}
