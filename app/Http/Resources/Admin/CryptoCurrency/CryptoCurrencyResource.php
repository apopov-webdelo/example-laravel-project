<?php

namespace App\Http\Resources\Admin\CryptoCurrency;

use App\Models\Directory\Bank;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BankResource
 *
 * @property Bank $resource
 *
 * @package App\Http\Resources\Admin\Bank
 */
class CryptoCurrencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
