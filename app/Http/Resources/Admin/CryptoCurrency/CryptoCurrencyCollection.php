<?php

namespace App\Http\Resources\Admin\CryptoCurrency;

use App\Http\Resources\BaseCollection;

/**
 * Class CryptoCurrencyCollection
 *
 * @package App\Http\Resources\Admin\Currency
 */
class CryptoCurrencyCollection extends BaseCollection
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCollection()
    {
        return CryptoCurrencyResource::collection($this);
    }
}
