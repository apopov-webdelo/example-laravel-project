<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/31/18
 * Time: 15:32
 */

namespace App\Traits;

use App\Models\User\User;

trait Authorable
{
    /**
     * Check user is ad author
     *
     * @param User $user
     * @return bool
     */
    public function isAuthor(User $user) : bool
    {
        return $this->author->id == $user->id;
    }
}