<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 01.11.2018
 * Time: 15:48
 */

namespace App\Traits;

/**
 * Trait MutatorSwitcher
 * @package App\Traits
 */
trait MutatorSwitcher
{
    /**
     * @var bool
     */
    public $useMutator = true;

    /**
     * @return self
     */
    public function disableMutator() : self
    {
        $this->useMutator = false;
        return $this;
    }

    /**
     * @return self
     */
    public function enableMutator() : self
    {
        $this->useMutator = true;
        return $this;
    }
}
