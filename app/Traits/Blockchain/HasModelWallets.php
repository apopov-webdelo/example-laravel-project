<?php

namespace App\Traits\Blockchain;

use App\Contracts\Blockchain\ModelWalletsContract;
use App\Contracts\Blockchain\ModelWalletsRepoContract;
use App\Models\Blockchain\ModelWallet;
use App\Repositories\Blockchain\ModelWalletsRepo;

/**
 * Trait HasModelWallets
 *
 * @package App\Traits\Blockchain
 * @mixin ModelWalletsContract
 */
trait HasModelWallets
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function wallets()
    {
        return $this->morphMany(ModelWallet::class, 'model', 'model');
    }

    /**
     * @inheritdoc
     *
     * @param string $alias
     *
     * @return bool
     */
    public function isWalletExists(string $alias): bool
    {
        $repo = $this->getWalletsRepo();

        $reslut = $repo->isExists($this, $alias);

        return $reslut;
    }

    /**
     * @inheritdoc
     *
     * @param string $alias
     *
     * @return string|false
     */
    public function getWallet(string $alias)
    {
        $repo = $this->getWalletsRepo();

        $reslut = $repo->wallet($this, $alias);

        return $reslut;
    }

    /**
     * @inheritdoc
     *
     * @param string $key
     * @param string $alias
     *
     * @return bool
     */
    public function saveWallet(string $key, string $alias): bool
    {
        $repo = $this->getWalletsRepo();

        $reslut = $repo->alias($alias)->save($this, $key);

        return $reslut;
    }

    /**
     * @inheritdoc
     *
     * @param string $alias
     *
     * @return bool
     */
    public function removeWallet(string $alias): bool
    {
        $repo = $this->getWalletsRepo();

        $reslut = $repo->alias($alias)->remove($this, $alias);

        return $reslut;
    }

    /**
     * @return ModelWalletsRepo
     */
    protected function getWalletsRepo(): ModelWalletsRepo
    {
        return app(ModelWalletsRepoContract::class);
    }
}
