<?php

namespace App\Traits\Bcrypt;

use App\Contracts\Bcrypt\ModelBcKeysContract;
use App\Contracts\Bcrypt\ModelBcKeysRepoContract;
use App\Models\Bcrypt\ModelBcKey;
use App\Repositories\Bcrypt\ModelBcKeysRepo;

/**
 * Trait HasModelKeys
 *
 * @package App\Traits\Bcrypt
 * @mixin ModelBcKeysContract
 */
trait HasModelBcKeys
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function keys()
    {
        return $this->morphMany(ModelBcKey::class, 'model', 'model');
    }

    /**
     * @inheritdoc
     *
     * @param string $alias
     *
     * @return bool
     */
    public function isBcKeyExists(string $alias): bool
    {
        $repo = $this->getKeysRepo();

        $reslut = $repo->isExists($this, $alias);

        return $reslut;
    }

    /**
     * @inheritdoc
     *
     * @param string $alias
     *
     * @return string|false
     */
    public function getBcKey(string $alias)
    {
        $repo = $this->getKeysRepo();

        $reslut = $repo->key($this, $alias);

        return $reslut;
    }

    /**
     * @inheritdoc
     *
     * @param string $key
     * @param string $alias
     *
     * @return bool
     */
    public function saveBcKey(string $key, string $alias): bool
    {
        $repo = $this->getKeysRepo();

        $reslut = $repo->alias($alias)->save($this, $key);

        return $reslut;
    }

    /**
     * @inheritdoc
     *
     * @param string $alias
     *
     * @return bool
     */
    public function removeBcKey(string $alias): bool
    {
        $repo = $this->getKeysRepo();

        $reslut = $repo->alias($alias)->remove($this, $alias);

        return $reslut;
    }

    /**
     * @return ModelBcKeysRepo
     */
    protected function getKeysRepo(): ModelBcKeysRepo
    {
        return app(ModelBcKeysRepoContract::class);
    }
}
