<?php

namespace App\Traits\Service;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Utils\Session\SessionStorageContract;
use App\Events\User\SecurityLogEvent;

/**
 * Trait HasSecurityLogs
 *
 * @package App\Traits\Service
 */
trait HasSecurityLogs
{
    /**
     * @var string
     */
    private $securityLogEvent = SecurityLogEvent::class;

    /**
     * @param AuthenticatedContract $user
     * @param string                $type
     * @param string|null           $desc
     */
    protected function fireGoodSecurityLog(
        AuthenticatedContract $user,
        string $type,
        ?string $desc = null
    ) {
        $this->fireSecurityLog($user, $type, true, $desc);
    }

    /**
     * @param AuthenticatedContract $user
     * @param string                $type
     * @param string|null           $desc
     */
    protected function fireBadSecurityLog(
        AuthenticatedContract $user,
        string $type,
        ?string $desc = null
    ) {
        $this->fireSecurityLog($user, $type, false, $desc);
    }

    /**
     * @param AuthenticatedContract $user
     * @param string                $type
     * @param bool                  $status
     * @param string|null           $desc
     */
    private function fireSecurityLog(
        AuthenticatedContract $user,
        string $type,
        bool $status = true,
        ?string $desc = null
    ) {
        event(app($this->securityLogEvent, [
            'user'           => $user,
            'sessionStorage' => app(SessionStorageContract::class),
            'securityInfo'   => [
                'type'       => $type,
                'status'     => $status,
                'statusDesc' => $desc,
            ],
        ]));
    }
}
