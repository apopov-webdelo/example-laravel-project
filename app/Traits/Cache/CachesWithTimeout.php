<?php

namespace App\Traits\Cache;

use Illuminate\Cache\Repository as CacheRepository;
use Illuminate\Support\Collection;

trait CachesWithTimeout
{
    /**
     * Minutes to cache each notification
     *
     * @var int
     */
    protected $timeOut = 15;

    private $tag = ':timeout:';

    /**
     * @var CacheRepository
     */
    private $cache;

    /**
     * @param Collection $items
     *
     * @return Collection
     */
    protected function onlyNotCached(Collection $items)
    {
        $cache = $this->getCacheRepo();

        return $items->filter(function ($item) use ($cache) {
            return $cache->get($this->cacheKey($item)) !== true;
        });
    }

    /**
     * @return CacheRepository
     */
    private function getCacheRepo()
    {
        if (!$this->cache) {
            $this->cache = app(CacheRepository::class);
        }

        return $this->cache;
    }

    /**
     * @param $item
     *
     * @return $this
     */
    protected function cacheTimeout($item)
    {
        $cache = $this->getCacheRepo();

        $cache->put($this->cacheKey($item), true, $this->timeOut);

        return $this;
    }

    /**
     * @param $item
     *
     * @return string
     */
    private function cacheKey($item)
    {
        return get_class($item) . $this->tag . $item->id;
    }
}
