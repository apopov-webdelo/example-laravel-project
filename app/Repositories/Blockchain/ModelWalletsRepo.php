<?php

namespace App\Repositories\Blockchain;

use App\Contracts\Blockchain\ModelWalletsContract;
use App\Contracts\Blockchain\ModelWalletsRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Blockchain\ModelWallet;
use App\Repositories\Repo;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModelWalletsRepo
 *
 * @package App\Repositories\Blockchain
 */
class ModelWalletsRepo extends Repo implements ModelWalletsRepoContract
{
    /**
     * @var Builder
     */
    protected $model;
    /**
     * @var string
     */
    protected $alias;

    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = ModelWallet::query();

        return $this;
    }

    /**
     * Check if key exists
     *
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return bool
     */
    public function isExists(ModelWalletsContract $model, string $alias): bool
    {
        return $this->wallet($model, $alias) ? true : false;
    }

    /**
     * Get key by alias
     *
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return string|bool
     */
    public function wallet(ModelWalletsContract $model, string $alias)
    {
        $modelWallet = $this->getModelWallet($model, $alias);

        return $modelWallet ? $modelWallet->wallet_id : false;
    }

    /**
     * Save or update existing key
     *
     * @param ModelWalletsContract $model
     * @param string               $key
     *
     * @return bool
     */
    public function save(ModelWalletsContract $model, string $key): bool
    {
        try {
            $modelWallet = $this->getModelWallet($model, $this->alias);

            if ($modelWallet) {
                $modelWallet->update(['wallet_id' => $key]);
            } else {
                $model->wallets()
                      ->save(new ModelWallet([
                          'wallet_id' => $key,
                          'alias'     => $this->alias,
                      ]));
            }

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    /**
     * Delete key by alias
     *
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return bool
     */
    public function remove(ModelWalletsContract $model, string $alias): bool
    {
        try {
            $modelWallet = $this->getModelWallet($model, $this->alias);

            if ($modelWallet) {
                $modelWallet->delete();
            }

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    /**
     * @param ModelWalletsContract $model
     *
     * @return ModelWalletsRepoContract
     */
    public function model(ModelWalletsContract $model): ModelWalletsRepoContract
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param string $alias
     *
     * @return ModelWalletsRepoContract
     */
    public function alias(string $alias): ModelWalletsRepoContract
    {
        $this->alias = $alias;

        return $this;
    }


    /**
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return ModelWallet
     */
    protected function getModelWallet(ModelWalletsContract $model, string $alias)
    {
        return $model->wallets()
                     ->where('alias', $alias)
                     ->get()
                     ->first();
    }
}
