<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Chat;


use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;

/**
 * Class MessageRepo
 *
 * @package App\Repositories\Chat
 */
class MessageRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * ChatRepo constructor.
     */
    public function __construct()
    {
        $this->model = Message::query();
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByAuthor(User $user)
    {
        $this->model->where('author_id', $user->id)->where('author_type', get_class($user));

        return $this;
    }

    /**
     * @param Chat $chat
     *
     * @return $this
     */
    public function filterByChat(Chat $chat)
    {
        $this->model->where('chat_id', $chat->id);

        return $this;
    }

    /**
     * @param Collection $chat
     *
     * @return $this
     */
    public function filterByChats(Collection $chat)
    {
        $this->model->whereIn('chat_id', $chat);

        return $this;
    }

    /**
     * @param string $expression
     *
     * @return $this
     */
    public function filterByMessage(string $expression)
    {
        $this->model->where('message', 'LIKE', '%'.$expression.'%');

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtFrom(Carbon $createdAt)
    {
        $this->model->where('created_at', '>=', $createdAt);

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtTo(Carbon $createdAt)
    {
        $this->model->where('created_at', '<=', $createdAt);

        return $this;
    }

    /**
     * @param Carbon $date
     *
     * @return $this
     */
    public function filterByUpdatedAtFrom(Carbon $date)
    {
        $this->model->where('updated_at', '>=', $date);

        return $this;
    }

    /**
     * @param Carbon $date
     *
     * @return $this
     */
    public function filterByUpdatedAtTo(Carbon $date)
    {
        $this->model->where('updated_at', '<=', $date);

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }
}