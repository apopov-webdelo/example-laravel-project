<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Chat;

use App\Models\Admin\Admin;
use App\Models\Chat\Chat;
use App\Models\Deal\Deal;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class ChatRepo
 *
 * @package App\Repositories\Chat
 */
class ChatRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * ChatRepo constructor.
     */
    public function __construct()
    {
        $this->model = Chat::query();
    }

    public function filterPrivate()
    {
        $this->model->whereDoesntHave('deals');

        return $this;
    }

    /**
     * @param Authenticatable $user
     *
     * @return $this
     */
    public function filterByAuthor(Authenticatable $user)
    {
        $this->model->where('author_id', $user->id)->where('author_type', get_class($user));

        return $this;
    }

    /**
     * @param Deal $deal
     *
     * @return $this
     */
    public function filterByDeal(Deal $deal)
    {
        $this->model->whereHas('deals', function (Builder $builder) use ($deal) {
            $builder->where('id', $deal->id);
        });

        return $this;
    }

    /**
     * @param Authenticatable $recipient
     *
     * @return $this
     */
    public function filterByRecipient(Authenticatable $recipient)
    {
        $this->model->whereHas('recipients', function (Builder $builder) use ($recipient) {
            $builder->where('users.id', $recipient->id);
        })->orWhereHas('adminRecipients', function (Builder $builder) use ($recipient) {
            $builder->where('admins.id', $recipient->id);
        });

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtFrom(Carbon $createdAt)
    {
        $this->model->where('created_at', '>=', $createdAt);

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtTo(Carbon $createdAt)
    {
        $this->model->where('created_at', '<=', $createdAt);

        return $this;
    }

    /**
     * @param Authenticatable[] $user
     *
     * @return Chat|null|Model|object
     */
    public function getUsersChat(Authenticatable ...$user)
    {
        $users = collect(func_get_args())->pluck('id')->all();
        $this->model
            ->whereDoesntHave('deals')
            ->whereHas('recipients', function (Builder $builder) use ($users) {
                $builder->where('users.id', $users)->groupBy('chat_id');
            });

        return $this->model->first();
    }

    /**
     *
     * @param User|Authenticatable $first
     * @param User|Authenticatable $second
     *
     * @return Chat|null|object|static
     */
    public function getChatBetweenUsers(Authenticatable $first, Authenticatable $second)
    {
        $this->model
            ->where(function (Builder $builder) use ($first, $second) {
                $builder->where(function (Builder $builder) use ($first) {
                    $builder->where('author_id', $first->id)
                            ->where('author_type', get_class($first));
                })->whereHas('recipients', function (Builder $builder) use ($second) {
                    $builder->where('recipientable_id', $second->id)
                            ->where('recipientable_type', get_class($second));
                })->whereDoesntHave('deals');
            })->orWhere(function (Builder $builder) use ($first, $second) {
                $builder->where(function (Builder $builder) use ($second) {
                    $builder->where('author_id', $second->id)
                            ->where('author_type', get_class($second));
                })->whereHas('recipients', function (Builder $builder) use ($first) {
                    $builder->where('recipientable_id', $first->id)
                            ->where('recipientable_type', get_class($first));
                })->whereDoesntHave('deals');
            });

        return $this->model->first();
    }

    /**
     * @param Admin[] $admin
     *
     * @return Chat|null|Model|object
     */
    public function getAdminsChat(Admin ...$admin)
    {
        $admins = collect(func_get_args())->pluck('id')->all();
        $this->model
            ->whereDoesntHave('deals')
            ->whereHas('adminRecipients', function (Builder $builder) use ($admins) {
                $builder->whereIn('admins.id', $admins)->groupBy('chat_id');
            });

        return $this->model->first();
    }

    /**
     * @param Deal $deal
     *
     * @return Chat|null|Model|object
     */
    public function getDealChat(Deal $deal)
    {
        $this->model->whereHas('deals', function (Builder $builder) use ($deal) {
            $builder->whereIn('deals.id', [$deal->id]);
        });

        return $this->model->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }
}
