<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Permission;

use App\Contracts\Repositories\RepoContract;
use App\Models\Permission\Permission;
use App\Repositories\Repo;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionRepo
 *
 * @package App\Repositories\Permission
 */
class PermissionRepo extends Repo
{
    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init() : RepoContract
    {
        $this->model = Permission::with('roles');
        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function filterById(int $id)
    {
        $this->model->where('id', $id);

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function filterByName(string $name)
    {
        $this->model->where('name', $name);

        return $this;
    }
}
