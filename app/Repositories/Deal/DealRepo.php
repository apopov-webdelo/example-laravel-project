<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Deal;

use App\Contracts\Repositories\RepoContract;
use App\Models\Ad\Ad;
use App\Models\Chat\Chat;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use App\Repositories\Repo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class DealRepo
 *
 * @package App\Repositories\Deal
 * @method DealRepo reset(): RepoContract
 * @method Deal findById($id)
 */
class DealRepo extends Repo
{
    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = Deal::with(['author', 'ad', 'reviews']);
        return $this;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByAuthor(User $user)
    {
        $this->model->where('author_id', $user->id);

        return $this;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByAdAuthor(User $user)
    {
        $this->model->whereHas('ad', function (Builder $builder) use ($user) {
            $builder->where('author_id', $user->id);
        });

        return $this;
    }

    /**
     * Deals where this user was buyer
     *
     * @param User $user
     *
     * @return $this
     */
    public function filterByBuyer(User $user)
    {
        $this->model->where(function (Builder $builder) use ($user) {
            $builder->where('author_id', $user->id)
                    ->whereHas('ad', function (Builder $builder) use ($user) {
                        $builder->where('is_sale', true)
                                ->where('author_id', '!=', $user->id);
                    });
        })
        ->orWhere(function (Builder $builder) use ($user) {
            $builder->whereHas('ad', function (Builder $builder) use ($user) {
                $builder->where('is_sale', false)
                        ->where('author_id', $user->id);
            });
        });

        return $this;
    }

    /**
     * Deals where this user was seller
     *
     * @param User $user
     *
     * @return $this
     */
    public function filterBySeller(User $user)
    {
        $this->model->where(function (Builder $builder) use ($user) {
            $builder->where('author_id', $user->id)
                    ->whereHas('ad', function (Builder $builder) use ($user) {
                        $builder->where('is_sale', false)
                                ->where('author_id', '!=', $user->id);
                    });
        })
        ->orWhere(function (Builder $builder) use ($user) {
            $builder->whereHas('ad', function (Builder $builder) use ($user) {
                $builder->where('is_sale', true)
                        ->where('author_id', $user->id);
            });
        });

        return $this;
    }

    /**
     * Filter deals by exact user
     *
     * @param User $user
     *
     * @return $this
     */
    public function filterByDealMember(User $user)
    {
        $this->model->where(function (Builder $builder) use ($user) {
            $builder->where('author_id', $user->id)
                    ->orWhereHas('ad', function (Builder $builder) use ($user) {
                        $builder->where('author_id', $user->id);
                    });
        });

        return $this;
    }

    /**
     * Filter deals are crossing between two users
     *
     * @param User $userOne
     * @param User $userTwo
     *
     * @return $this
     */
    public function filterByDealMembersCross(User $userOne, User $userTwo)
    {
        $this->model->where(function (Builder $builder) use ($userOne, $userTwo) {
            $builder->where('author_id', $userOne->id)
                    ->whereHas('ad', function (Builder $builder) use ($userTwo) {
                        $builder->where('author_id', $userTwo->id);
                    });
        })
        ->orWhere(function (Builder $builder) use ($userOne, $userTwo) {
            $builder->where('author_id', $userTwo->id)
                    ->whereHas('ad', function (Builder $builder) use ($userOne) {
                        $builder->where('author_id', $userOne->id);
                    });
        });

        return $this;
    }

    /**
     * @param Ad $ad
     *
     * @return $this
     */
    public function filterByAd(Ad $ad)
    {
        $this->model->where('ad_id', $ad->id);

        return $this;
    }

    /**
     * Filter deals by CURRENT status (last set status)
     *
     * @param DealStatus|int $status
     * @return DealRepo
     */
    public function filterByStatus($status)
    {
        $status = $status instanceof DealStatus ? $status : DealStatus::findOrFail($status);
        return $this->filterByStatusId($status->id);
    }

    /**
     * Filter deals by CURRENT status (last set status)
     *
     * @param int|int[] $statusId
     *
     * @return $this
     */
    public function filterByStatusId($statusId)
    {
        $statuses = is_int($statusId) ? [$statusId] : $statusId;
        $this->model->where(function (Builder $builder) use ($statuses) {
            $builder->whereHas('statuses', function (Builder $builder) use ($statuses) {
                $builder->whereIn('status_id', $statuses)
                        ->whereRaw('
                            `deals_has_statuses`.`id` IN (
                                SELECT max(`deals_has_statuses`.`id`) 
                                FROM `deals_has_statuses` 
                                WHERE `deals`.`id` = `deals_has_statuses`.`deal_id`
                            )');
            });
        });

        return $this;
    }

    /**
     * @param DealStatus $status
     *
     * @return $this
     */
    public function filterByUsedStatus(DealStatus $status)
    {
        return $this->filterByUsedStatusId($status->id);
    }

    /**
     * @param int|int[] $statusId
     *
     * @return $this
     */
    public function filterByUsedStatusId($statusId)
    {
        $statuses = is_int($statusId) ? [$statusId] : $statusId;
        $this->model->whereHas('statuses', function (Builder $builder) use ($statuses) {
            $builder->whereIn('status_id', $statuses);
        });

        return $this;
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function filterPriceFrom(float $price)
    {
        $this->model->where('price', '>=', $price);

        return $this;
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function filterPriceTo(float $price)
    {
        $this->model->where('price', '<=', $price);

        return $this;
    }

    /**
     * @param float $fiatAmount
     *
     * @return $this
     */
    public function filterFiatAmountFrom(float $fiatAmount)
    {
        $this->model->where('fiat_amount', '>=', $fiatAmount);

        return $this;
    }

    /**
     * @param float $fiatAmount
     *
     * @return $this
     */
    public function filterFiatAmountTo(float $fiatAmount)
    {
        $this->model->where('fiat_amount', '<=', $fiatAmount);

        return $this;
    }

    /**
     * @param float $cryptoAmount
     *
     * @return $this
     */
    public function filterCryptoFiatAmountFrom(float $cryptoAmount)
    {
        $this->model->where('crypto_amount', '>=', $cryptoAmount);

        return $this;
    }

    /**
     * @param float $cryptoAmount
     *
     * @return $this
     */
    public function filterCryptoFiatAmountTo(float $cryptoAmount)
    {
        $this->model->where('crypto_amount', '<=', $cryptoAmount);

        return $this;
    }

    /**
     * Filter deals by commission (all from that amount)
     *
     * @param float $comission
     *
     * @return $this
     */
    public function filterComissionFrom(float $comission)
    {
        $this->model->where('crypto_amount', '>=', $comission);

        return $this;
    }

    /**
     * Filter deals by commission (all to that amount)
     *
     * @param float $comission
     *
     * @return $this
     */
    public function filterComissionTo(float $comission)
    {
        $this->model->where('crypto_amount', '<=', $comission);

        return $this;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function filterTimeFrom(int $time)
    {
        $this->model->where('time', '>=', $time);

        return $this;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function filterTimeTo(int $time)
    {
        $this->model->where('time', '<=', $time);

        return $this;
    }

    /**
     * Filter deals by created date (all from that date)
     *
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtFrom(Carbon $createdAt)
    {
        $this->model->where('created_at', '>=', $createdAt);

        return $this;
    }

    /**
     * Filter deals by created date (all to that date)
     *
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtTo(Carbon $createdAt)
    {
        $this->model->where('created_at', '<=', $createdAt);

        return $this;
    }

    /**
     * Filter deals must be handled for autocancel by deadline reason
     *
     * @return $this
     */
    public function filterExpired()
    {
        $this->filterByStatusId(DealStatusConstants::VERIFIED)
             ->model->whereRaw('(`created_at` + INTERVAL `time` MINUTE) < NOW()');

        return $this;
    }


    /**
     * Filter deals where user is buyer
     *
     * @param User $user
     * @return $this
     */
    public function filterForSale(User $user)
    {
        $this->model->where(function (Builder $builder) use ($user) {
            $builder->whereHas('ad', function (Builder $builder) use ($user) {
                $builder->where('is_sale', 1)
                    ->where('author_id', $user->id);
            })->orWhere(function (Builder $builder) use ($user) {
                $builder->whereHas('ad', function (Builder $builder) use ($user) {
                    $builder->where('is_sale', 0);
                })->where('author_id', $user->id);
            });
        });

        return $this;
    }

    /**
     * Filter deals where user is buyer
     *
     * @param User $user
     * @return $this
     */
    public function filterForBuy(User $user)
    {
        $this->model->where(function (Builder $builder) use ($user) {
            $builder->whereHas('ad', function (Builder $builder) use ($user) {
                $builder->where('is_sale', 0)
                    ->where('author_id', $user->id);
            })->orWhere(function (Builder $builder) use ($user) {
                $builder->whereHas('ad', function (Builder $builder) use ($user) {
                    $builder->where('is_sale', 1);
                })->where('author_id', $user->id);
            });
        });

        return $this;
    }

    /**
     * Filter deals by ads currency
     *
     * @param int|Currency $currency
     *
     * @return $this
     */
    public function filterByCurrency($currency)
    {
        $currencyId = $currency instanceof Currency
            ? $currency->id
            : $currency;

        $this->model->whereHas('ad', function (Builder $builder) use ($currencyId) {
            $builder->where('currency_id', $currencyId);
        });

        return $this;
    }

    /**
     * Filter deals by ads crypto currency
     *
     * @param int|CryptoCurrency $cryptoCurrency
     *
     * @return $this
     */
    public function filterByCryptoCurrency($cryptoCurrency)
    {
        $cryptoCurrencyId = $cryptoCurrency instanceof CryptoCurrency
            ? $cryptoCurrency->id
            : $cryptoCurrency;

        $this->model->whereHas('ad', function (Builder $builder) use ($cryptoCurrencyId) {
            $builder->where('crypto_currency_id', $cryptoCurrencyId);
        });

        return $this;
    }

    /**
     * Filter deals by ads payment system
     *
     * @param int|PaymentSystem $paymentSystem
     *
     * @return $this
     */
    public function filterByPaymentSystem($paymentSystem)
    {
        $paymentSystemId = $paymentSystem instanceof PaymentSystem
            ? $paymentSystem->id
            : $paymentSystem;

        $this->model->whereHas('ad', function (Builder $builder) use ($paymentSystemId) {
            $builder->where('payment_system_id', $paymentSystemId);
        });

        return $this;
    }

    /**
     * @param Chat $chat
     *
     * @return $this
     */
    public function filterByChat(Chat $chat)
    {
        $this->model->whereHas('chats', function (Builder $builder) use ($chat) {
            $builder->where('id', $chat->id);
        });
        return $this;
    }
}
