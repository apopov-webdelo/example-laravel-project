<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Admin;

use App\Contracts\Repositories\RepoContract;
use App\Models\Admin\Admin;
use App\Models\Role\RoleConstants;
use App\Models\User\User;
use App\Repositories\Repo;
use Carbon\Carbon;

/**
 * Class AdminRepo
 *
 * @package App\Repositories\User
 */
class AdminRepo extends Repo
{
    /**
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = Admin::query();
        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function filterById(int $id): self
    {
        $this->model->where('id', $id);
        return $this;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function searchByLogin(string $login): self
    {
        $this->model->where('login', 'LIKE', $login . '%');
        return $this;
    }

    /**
     * @param string $expression
     *
     * @return $this
     */
    public function search(string $expression): self
    {
        $this->model->where('login', 'LIKE', $expression . '%')
                    ->where('email', 'LIKE', $expression . '%')
                    ->where('id', 'LIKE', $expression . '%');
        return $this;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function filterByLogin(string $login): self
    {
        $this->model->where('login', $login);
        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function filterByEmail(string $email): self
    {
        $this->model->where('email', $email);
        return $this;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function filterByPhone(string $phone): self
    {
        $this->model->where('phone', $phone);
        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function excludeById(int $id): self
    {
        $this->model->where('id', '!=', $id);
        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return AdminRepo
     */
    public function filterByCreatedAt(Carbon $createdAt): self
    {
        $this->model->where('created_at', '>=', $createdAt)
                    ->where(
                        'created_at',
                        '<',
                        $createdAt->copy()
                                  ->addDays(1)
                    );

        return $this;
    }

    /**
     * @param Carbon $lastSeen
     *
     * @return AdminRepo
     */
    public function filterByLastSeen(Carbon $lastSeen): self
    {
        $this->model->where('last_seen', '>=', $lastSeen)
                    ->where(
                        'last_seen',
                        '<',
                        $lastSeen->copy()
                                 ->addDays(1)
                    );

        return $this;
    }

    /**
     * @param Carbon $createdAtFrom
     *
     * @return AdminRepo
     */
    public function filterByCreatedAtFrom(Carbon $createdAtFrom): self
    {
        $this->model->where('created_at', '>=', $createdAtFrom);
        return $this;
    }


    /**
     * @param Carbon $createdAtTo
     *
     * @return AdminRepo
     */
    public function filterByCreatedAtTo(Carbon $createdAtTo): self
    {
        $this->model->where('created_at', '<', $createdAtTo->addDay(1));
        return $this;
    }


    /**
     * @param Carbon $lastSeenFrom
     *
     * @return AdminRepo
     */
    public function filterByLastSeenFrom(Carbon $lastSeenFrom): self
    {
        $this->model->where('last_seen', '>=', $lastSeenFrom);
        return $this;
    }


    /**
     * @param Carbon $lastSeenTo
     *
     * @return AdminRepo
     */
    public function filterByLastSeenTo(Carbon $lastSeenTo): self
    {
        $this->model->where('last_seen', '<', $lastSeenTo->addDay(1));
        return $this;
    }

    /**
     * @param array $ids
     *
     * @return AdminRepo
     */
    public function filterByIds(array $ids): self
    {
        $this->model->whereIn('id', $ids);
        return $this;
    }

    /**
     * @return AdminRepo
     */
    public function filterByClientRole(): self
    {
        return $this->filterByRole(RoleConstants::CLIENT);
    }

    /**
     * @param $roleId
     *
     * @return AdminRepo
     */
    private function filterByRole($roleId): self
    {
        $this->model->where('role_id', $roleId);
        return $this;
    }
}
