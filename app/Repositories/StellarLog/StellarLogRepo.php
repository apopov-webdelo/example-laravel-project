<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\StellarLog;

use App\Contracts\Repositories\PeriodRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Contracts\Repositories\StellarLogRepoContract;
use App\Models\Ad\Ad;
use App\Models\Blockchain\StellarLog;
use App\Repositories\Repo;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Class StellarLogRepo
 *
 * @package App\Repositories\Ad
 * @property QueryBuilder|Ad $model
 */
class StellarLogRepo extends Repo implements StellarLogRepoContract, PeriodRepoContract
{
    /**
     * @return RepoContract
     */
    public function init(): RepoContract
    {
        $this->model = StellarLog::query();

        return $this;
    }

    /**
     * @param string $type
     *
     * @return StellarLogRepoContract
     */
    public function filterByType(string $type): StellarLogRepoContract
    {
        $this->model->where('type', $type);

        return $this;
    }

    /**
     * @param string $expression
     *
     * @return StellarLogRepoContract
     */
    public function searchByRoute(string $expression): StellarLogRepoContract
    {
        $this->model->where('route', 'LIKE', '%'.$expression.'%');

        return $this;
    }

    /**
     * @param int $code
     *
     * @return StellarLogRepoContract
     */
    public function filterByCode(int $code): StellarLogRepoContract
    {
        $this->model->where('response_code', $code);

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodToday(): PeriodRepoContract
    {
        $this->model
            ->where('created_at', '>=', now()->startOfDay())
            ->where('created_at', '<=', now()->endOfDay());

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodYesterday(): PeriodRepoContract
    {
        $this->model
            ->where('created_at', '>=', now()->subDay()->startOfDay())
            ->where('created_at', '<=', now()->subDay()->endOfDay());

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodThisWeek(): PeriodRepoContract
    {
        $this->model
            ->where('created_at', '>=', now()->startOfWeek())
            ->where('created_at', '<=', now()->endOfWeek());

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodLastWeek(): PeriodRepoContract
    {
        $this->model
            ->where('created_at', '>=', now()->subWeek()->startOfWeek())
            ->where('created_at', '<=', now()->subWeek()->endOfWeek());

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodThisMonth(): PeriodRepoContract
    {
        $this->model
            ->where('created_at', '>=', now()->startOfMonth())
            ->where('created_at', '<=', now()->endOfMonth());

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodLastMonth(): PeriodRepoContract
    {
        $this->model
            ->where('created_at', '>=', now()->subMonth()->startOfMonth())
            ->where('created_at', '<=', now()->subMonth()->endOfMonth());

        return $this;
    }

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodAny(): PeriodRepoContract
    {
        return $this;
    }

    /**
     * @param string $method
     *
     * @return StellarLogRepoContract
     */
    public function filterByMethod(string $method): StellarLogRepoContract
    {
        $this->model->where('method', $method);

        return $this;
    }
}
