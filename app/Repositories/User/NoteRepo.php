<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\User;


use App\Models\User\Note;
use App\Models\User\User;

class NoteRepo
{
    private $model;

    public function __construct()
    {
        $this->model = Note::query();
    }

    public function filterByRecipient(User $user)
    {
        $this->model->where('recipient_id', $user->id);

        return $this;
    }

    public function filterByAuthor(User $user)
    {
        $this->model->where('author_id', $user->id);

        return $this;
    }

    public function take()
    {
        return $this->model;
    }
}