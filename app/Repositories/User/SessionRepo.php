<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\User;

use App\Contracts\SessionableContract;
use App\Models\User\Review;
use App\Models\Session\Session;
use App\Models\User\User;
use Carbon\Carbon;

/**
 * Class SessionRepo
 *
 * @package App\Repositories\User
 */
class SessionRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * SessionRepo constructor.
     */
    public function __construct()
    {
        $this->model = Session::query();
    }

    /**
     * @param SessionableContract|\Illuminate\Foundation\Auth\User $user
     *
     * @return $this
     */
    public function filterBySessionableId(SessionableContract $user)
    {
        $this->model
             ->where('sessionable_id', $user->id)
             ->where('sessionable_type', get_class($user));

        return $this;
    }

    /**
     * @param string $ip
     *
     * @return $this
     */
    public function filterByIp(string $ip)
    {
        $this->model->where('ip', $ip);

        return $this;
    }

    /**
     * @param string $country
     *
     * @return $this
     */
    public function filterByCountry(string $country)
    {
        $this->model->where('country', $country);

        return $this;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function filterByCity(string $city)
    {
        $this->model->where('city', $city);

        return $this;
    }

    /**
     * @param string $os
     *
     * @return $this
     */
    public function filterByOs(string $os)
    {
        $this->model->where('os', $os);

        return $this;
    }

    /**
     * @param string $browser
     *
     * @return $this
     */
    public function filterByBrowser(string $browser)
    {
        $this->model->where('browser', $browser);

        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function filterByType(string $type)
    {
        $this->model->where('type', $type);

        return $this;
    }

    /**
     * @param Carbon $time
     * @return $this
     */
    public function filterByTimeFrom(Carbon $time)
    {
        return $this->filterByCreatedAtFrom($time);
    }

    /**
     * @param Carbon $time
     * @return $this
     */
    public function filterByTimeTo(Carbon $time)
    {
        return $this->filterByCreatedAtTo($time);
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtFrom(Carbon $createdAt)
    {
        $this->model->where('created_at', '>=', $createdAt);

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtTo(Carbon $createdAt)
    {
        $this->model->where('created_at', '<=', $createdAt);

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }
}
