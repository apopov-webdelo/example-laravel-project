<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\User;

use App\Models\User\Review;
use App\Models\User\ReviewConstants;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ReviewRepo
 * @package App\Repositories\User
 */
class ReviewRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * ReviewRepo constructor.
     */
    public function __construct()
    {
        $this->model = Review::query();
        return $this;
    }

    /**
     * Reset all filters
     *
     * @return $this
     */
    public function reset()
    {
        return $this->__construct();
    }

    /**
     * @param User $user
     * @return $this
     */
    public function filterByRecipient(User $user)
    {
        $this->model->where('recipient_id', $user->id);

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function filterByAuthor(User $user)
    {
        $this->model->where('author_id', $user->id);

        return $this;
    }

    /**
     * @param int $rate
     * @return $this
     */
    public function filterByRateFrom(int $rate)
    {
        $this->model->where('rate', '>=', $rate);

        return $this;
    }

    /**
     * @param int $rate
     * @return $this
     */
    public function filterByRateTo(int $rate)
    {
        $this->model->where('rate', '<=', $rate);

        return $this;
    }

    /**
     * @param int $rate
     * @return $this
     */
    public function filterByRate(int $rate)
    {
        $this->model->where('rate', $rate);

        return $this;
    }

    /**
     * @param string $trust
     * @return $this
     */
    public function filterByTrust(string $trust)
    {
        $this->model->where('trust', $trust);

        return $this;
    }

    /**
     * Filter public positive reviews for transmitting user
     *
     * @param User $recipient
     * @return $this
     */
    public function filterPublicPositive(User $recipient)
    {
        return $this->filterByRecipient($recipient)
                    ->filterByTrustPositive();
    }

    /**
     * Filter positive reviews
     *
     * @return $this
     */
    public function filterByTrustPositive()
    {
        $this->filterByTrust(ReviewConstants::TRUST_POSITIVE);
        return $this;
    }

    /**
     * Filter public negative reviews for transmitting user
     *
     * @param User $recipient
     * @return $this
     */
    public function filterPublicNegative(User $recipient)
    {
        return $this->filterByRecipient($recipient)
                    ->filterByTrustNegative()
                    ->filterOutHiddenNegative();
    }

    /**
     * Filter negative reviews
     *
     * @return $this
     */
    public function filterByTrustNegative()
    {
        $this->filterByTrust(ReviewConstants::TRUST_NEGATIVE);
        return $this;
    }

    /**
     * Filter out negative reviews hidden
     *
     * @return $this
     */
    public function filterOutHiddenNegative()
    {
        $this->model->where(function (Builder $builder) {
            $builder->where('rate', '!=', ReviewConstants::RATE_NEGATIVE_HIDDEN);
        });

        return $this;
    }

    /**
     * Filter public neutral reviews for transmitting user
     *
     * @param User $recipient
     * @return $this
     */
    public function filterPublicNeutral(User $recipient)
    {
        return $this->filterByRecipient($recipient)
                    ->filterByTrustNeutral();
    }

    /**
     * Filter negative reviews
     *
     * @return $this
     */
    public function filterByTrustNeutral()
    {
        $this->filterByTrust(ReviewConstants::TRUST_NEUTRAL);
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }
}
