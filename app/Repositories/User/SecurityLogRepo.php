<?php

namespace App\Repositories\User;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\User\SecurityLog;
use App\Repositories\Repo;
use Carbon\Carbon;

/**
 * Class SecurityLogRepo
 *
 * @package App\Repositories\User
 */
class SecurityLogRepo extends Repo
{
    /**
     * Set default builder to model property
     *
     * @return mixed
     */
    protected function init(): RepoContract
    {
        $this->model = SecurityLog::query();

        return $this;
    }

    /**
     * @param AuthenticatedContract $user
     *
     * @return $this
     */
    public function filterBySessionableId(AuthenticatedContract $user)
    {
        $this->model
             ->where('sessionable_id', $user->getId())
             ->where('sessionable_type', get_class($user));

        return $this;
    }

    /**
     * @param string $ip
     *
     * @return $this
     */
    public function filterByIp(string $ip)
    {
        $this->model->where('ip', $ip);

        return $this;
    }

    /**
     * @param string $country
     *
     * @return $this
     */
    public function filterByCountry(string $country)
    {
        $this->model->where('country', $country);

        return $this;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function filterByCity(string $city)
    {
        $this->model->where('city', $city);

        return $this;
    }

    /**
     * @param string $os
     *
     * @return $this
     */
    public function filterByOs(string $os)
    {
        $this->model->where('os', $os);

        return $this;
    }

    /**
     * @param string $browser
     *
     * @return $this
     */
    public function filterByBrowser(string $browser)
    {
        $this->model->where('browser', $browser);

        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function filterByType(string $type)
    {
        $this->model->where('type', $type);

        return $this;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function filterByStatus(string $status)
    {
        $this->model->where('status', $status);

        return $this;
    }

    /**
     * @param string $statusDesc
     *
     * @return $this
     */
    public function filterByStatusDesc(string $statusDesc)
    {
        $this->model->where('status_desc', $statusDesc);

        return $this;
    }

    /**
     * @param Carbon $time
     * @return $this
     */
    public function filterByTimeFrom(Carbon $time)
    {
        return $this->filterByCreatedAtFrom($time);
    }

    /**
     * @param Carbon $time
     * @return $this
     */
    public function filterByTimeTo(Carbon $time)
    {
        return $this->filterByCreatedAtTo($time);
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtFrom(Carbon $createdAt)
    {
        $this->model->where('created_at', '>=', $createdAt);

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtTo(Carbon $createdAt)
    {
        $this->model->where('created_at', '<=', $createdAt);

        return $this;
    }
}
