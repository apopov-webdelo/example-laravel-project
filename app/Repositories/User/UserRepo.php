<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\User;


use App\Models\Role\RoleConstants;
use App\Models\User\User;
use Carbon\Carbon;

/**
 * Class UserRepo
 *
 * @package App\Repositories\User
 */
class UserRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * UserRepo constructor.
     */
    public function __construct()
    {
        $this->model = User::query();
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function filterById(int $id): self
    {
        $this->model->where('id', $id);
        return $this;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function searchByLogin(string $login): self
    {
        $this->model->where('login', 'LIKE', $login . '%');
        return $this;
    }

    /**
     * @param string $expression
     *
     * @return $this
     */
    public function search(string $expression): self
    {
        $this->model->where('login', 'LIKE', $expression . '%')
                    ->orWhere('email', 'LIKE', $expression . '%')
                    ->orWhere('id', 'LIKE', $expression . '%');
        return $this;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function filterByLogin(string $login): self
    {
        $this->model->where('login', $login);
        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function filterByEmail(string $email): self
    {
        $this->model->where('email', $email);
        return $this;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function filterByPhone(string $phone): self
    {
        $this->model->where('phone', $phone);
        return $this;
    }

    /**
     * @return $this
     */
    public function filterByEmailConfirmed(): self
    {
        $this->model->whereHas('security', function ($builder) {
            $builder->where('email_confirmed', true);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function filterByPhoneConfirmed(): self
    {
        $this->model->whereHas('security', function ($builder) {
            $builder->where('phone_confirmed', true);
        });

        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function excludeById(int $id): self
    {
        $this->model->where('id', '!=', $id);
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return UserRepo
     */
    public function filterByCreatedAt(Carbon $createdAt): self
    {
        $this->model->where('created_at', '>=', $createdAt)
                    ->where('created_at', '<', $createdAt->copy()->addDays(1));

        return $this;
    }

    /**
     * @param Carbon $lastSeen
     *
     * @return UserRepo
     */
    public function filterByLastSeen(Carbon $lastSeen): self
    {
        $this->model->where('last_seen', '>=', $lastSeen)
                    ->where('last_seen', '<', $lastSeen->copy()
                                                       ->addDays(1));

        return $this;
    }

    /**
     * @param Carbon $createdAtFrom
     *
     * @return UserRepo
     */
    public function filterByCreatedAtFrom(Carbon $createdAtFrom): self
    {
        $this->model->where('created_at', '>=', $createdAtFrom);
        return $this;
    }


    /**
     * @param Carbon $createdAtTo
     *
     * @return UserRepo
     */
    public function filterByCreatedAtTo(Carbon $createdAtTo): self
    {
        $this->model->where('created_at', '<', $createdAtTo->addDay(1));
        return $this;
    }


    /**
     * @param Carbon $lastSeenFrom
     *
     * @return UserRepo
     */
    public function filterByLastSeenFrom(Carbon $lastSeenFrom): self
    {
        $this->model->where('last_seen', '>=', $lastSeenFrom);
        return $this;
    }


    /**
     * @param Carbon $lastSeenTo
     *
     * @return UserRepo
     */
    public function filterByLastSeenTo(Carbon $lastSeenTo): self
    {
        $this->model->where('last_seen', '<', $lastSeenTo->addDay(1));
        return $this;
    }

    /**
     * @param User $user
     *
     * @return UserRepo
     */
    public function getFavorites(User $user): self
    {
        $this->filterByIds(
            app(ReviewRepo::class)
                ->filterByAuthor($user)
                ->filterByTrustPositive()
                ->take()
                ->pluck('recipient_id')
                ->toArray()
        );

        return $this;
    }

    /**
     * @param array $ids
     *
     * @return UserRepo
     */
    public function filterByIds(array $ids): self
    {
        $this->model->whereIn('id', $ids);
        return $this;
    }

    /**
     * @param User $user
     *
     * @return UserRepo
     */
    public function getBlacklist(User $user): self
    {
        $this->filterByIds(
            app(ReviewRepo::class)
                ->filterByAuthor($user)
                ->filterByTrustNegative()
                ->take()
                ->pluck('recipient_id')
                ->toArray()
        );

        return $this;
    }

    /**
     * @return UserRepo
     */
    public function filterByClientRole(): self
    {
        return $this->filterByRole(RoleConstants::CLIENT);
    }

    /**
     * @param $roleId
     *
     * @return UserRepo
     */
    private function filterByRole($roleId): self
    {
        $this->model->where('role_id', $roleId);
        return $this;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function filterByProvider(string $id): self
    {
        $this->model->where('provider', $id);
        return $this;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function filterByProviderId(string $id): self
    {
        $this->model->where('provider_id', $id);
        return $this;
    }
}
