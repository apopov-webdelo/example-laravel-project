<?php

namespace App\Repositories\Directory;

use App\Contracts\Repositories\FlushRepoCacheContract;
use App\Contracts\Repositories\FullDirectoryRepoContract;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Cache\Repository as Cache;

/**
 * Class CachedFullDirectoryRepo
 *
 * Caching decorator for FullDirectoryRepo
 *
 * @package App\Repositories\Directory
 */
class CachedFullDirectoryRepo implements FullDirectoryRepoContract, FlushRepoCacheContract
{
    const MINS_CACHED = 1440; // 1440 = 1 day
    const TAG = 'directory-all';

    /**
     * @var FullDirectoryRepoContract
     */
    protected $repo;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * CachedFullDirectoryRepo constructor.
     *
     * @param FullDirectoryRepoContract $repo
     * @param Cache                     $cache
     */
    public function __construct(FullDirectoryRepoContract $repo, Cache $cache)
    {
        $this->repo = $repo;
        $this->cache = $cache;
    }

    /**
     * get all country collection
     *
     * @return Collection
     */
    public function countries(): Collection
    {
        return $this->cache(__METHOD__, function () {
            return $this->repo->countries();
        });
    }

    /**
     * get all crypto collection
     *
     * @return Collection
     */
    public function cryptoCurrencies(): Collection
    {
        return $this->cache(__METHOD__, function () {
            return $this->repo->cryptoCurrencies();
        });
    }

    /**
     * get all directory data
     *
     * @return array
     */
    public function all(): array
    {
        return $this->cache(__METHOD__, function () {
            return $this->repo->all();
        });
    }

    /**
     * todo extract to abstract BaseCachingRepo
     * flush repo cache
     */
    public function flush()
    {
        return $this->cache->tags(static::TAG)->flush();
    }

    /**
     * todo extract to abstract BaseCachingRepo
     * @param string   $name
     * @param callable $func
     *
     * @return mixed
     */
    protected function cache(string $name, callable $func)
    {
        return $this->cache->tags(static::TAG)
                           ->remember($name, static::MINS_CACHED, $func);
    }
}
