<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Directory;

use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;

/**
 * Class CountryRepo
 *
 * @package App\Repositories\Directory
 */
class CountryRepo extends BaseRepo
{
    /**
     * BankRepo constructor.
     */
    public function __construct()
    {
        $this->model = Country::query();
    }

    /**
     * @param string $iso
     * @return $this
     */
    public function filterByISO(string $iso)
    {
        $this->model->where('iso', $iso)->limit(1);

        return $this;
    }

    /**
     * @param string $alpha2
     * @return $this
     */
    public function filterByAlpha2(string $alpha2)
    {
        $this->model->where('alpha2', $alpha2)->limit(1);

        return $this;
    }

    /**
     * @param string $alpha3
     * @return $this
     */
    public function filterByAlpha3(string $alpha3)
    {
        $this->model->where('iso', $alpha3)->limit(1);

        return $this;
    }

    /**
     * @param Currency $currency
     *
     * @return $this
     */
    public function filterByCurrency(Currency $currency)
    {
        $this->model->whereIn('id', $currency->paymentSystems->pluck('pivot.country_id')->unique()->all());

        return $this;
    }

    /**
     * @param PaymentSystem $paymentSystem
     *
     * @return $this
     */
    public function filterByPaymentSystem(PaymentSystem $paymentSystem)
    {
        $this->model->whereIn('id', $paymentSystem->currencies->pluck('pivot.country_id')->unique()->all());

        return $this;
    }
}
