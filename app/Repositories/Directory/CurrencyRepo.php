<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Directory;

use App\Models\Directory\Country;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CurrencyRepo
 *
 * @package App\Repositories\Directory
 */
class CurrencyRepo extends BaseRepo
{
    /**
     * @param Country $country
     *
     * @return $this
     */
    public function filterByCountry(Country $country)
    {
        $this->model->whereHas('paymentSystems', function ($builder) use ($country) {
            $builder->where('country_id', $country->id);
        });

        return $this;
    }

    /**
     * @param PaymentSystem $paymentSystem
     *
     * @return $this
     */
    public function filterByPaymentSystem(PaymentSystem $paymentSystem)
    {
        $this->model->whereHas('paymentSystems', function ($builder) use ($paymentSystem) {
            $builder->where('id', $paymentSystem->id);
        });

        return $this;
    }

    /**
     * @param array $exclude
     *
     * @return $this
     */
    public function exclude(array $exclude)
    {
        $this->model->whereNotIn('id', $exclude);
        return $this;
    }

    /**
     * @param Country $country
     *
     * @return $this
     */
    public function filterByCountryInBanks(Country $country)
    {
        $this->model->whereHas('banks', function ($builder) use ($country) {
            $builder->where('country_id', $country->id);
        });

        return $this;
    }

    /**
     * @param Currency $currency
     *
     * @return $this
     */
    public function filterByCurrency(Currency $currency)
    {
        $this->model->whereHas('banks', function ($builder) use ($currency) {
            $builder->where('currency_id', $currency->id);
        });

        return $this;
    }

    /**
     * @param string $expression
     * @param array  $exclude
     *
     * @return $this
     */
    public function search(string $expression, array $exclude = [])
    {
        if (sizeof($exclude) > 0) {
            $this->model->where(function (Builder $builder) use ($expression, $exclude) {
                $builder->where('title', 'LIKE', '%'.$expression.'%')
                        ->orWhere('code', 'LIKE', '%'.$expression.'%');
            })->whereNotIn('id', $exclude);
        } else {
            $this->model
                ->where('title', 'LIKE', '%'.$expression.'%')
                ->orWhere('code', 'LIKE', '%'.$expression.'%');
        }

        return $this;
    }

    /**
     * BankRepo constructor.
     */
    public function __construct()
    {
        $this->model = Currency::query();
    }
}