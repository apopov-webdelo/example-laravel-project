<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Directory;

use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;

/**
 * Class BankRepo
 *
 * @package App\Repositories\Directory
 */
class BankRepo extends BaseRepo
{
    /**
     * BankRepo constructor.
     */
    public function __construct()
    {
        $this->model = Bank::query();
    }

    /**
     * @param Country $country
     *
     * @return $this
     */
    public function filterByCountry(Country $country)
    {
        $this->model->whereHas('currencies', function ($builder) use ($country) {
            $builder->where('country_id', $country->id);
        });

        return $this;
    }

    /**
     * @param string $expression
     *
     * @return $this
     */
    public function search(string $expression)
    {
        $this->model->where('title', 'LIKE', '%'.$expression.'%');

        return $this;
    }

    /**
     * @param array $exclude
     *
     * @return $this
     */
    public function exclude(array $exclude)
    {
        $this->model->whereNotIn('id', $exclude);

        return $this;
    }

    /**
     * @param Currency $currency
     *
     * @return $this
     */
    public function filterByCurrency(Currency $currency)
    {
        $this->model->whereHas('currencies', function ($builder) use ($currency) {
            $builder->where('currency_id', $currency->id);
        });

        return $this;
    }
}
