<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Directory;

use App\Models\Directory\CryptoCurrency;

/**
 * Class CryptoCurrencyRepo
 *
 * @package App\Repositories\Directory
 */
class CryptoCurrencyRepo extends BaseRepo
{
    /**
     * BankRepo constructor.
     */
    public function __construct()
    {
        $this->model = CryptoCurrency::query();
    }

    /**
     * Return model by code
     *
     * @param string $code
     *
     * @return CryptoCurrency
     */
    public function getByCode(string $code): CryptoCurrency
    {
        /** @var CryptoCurrency $crypto */
        $crypto = $this->model->where('code', strtolower($code))->firstOrFail();
        return $crypto;
    }
}
