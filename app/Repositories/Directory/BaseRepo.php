<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Directory;


use App\Models\Ad\Ad;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\Currency;

/**
 * Class BaseRepo
 *
 * @package App\Repositories\Directory
 */
class BaseRepo
{
    protected $model;

    /**
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function take()
    {
        return $this->model;
    }
}