<?php

namespace App\Repositories\Directory;

use App\Contracts\Repositories\FullDirectoryRepoContract;
use App\Http\Resources\Client\Directory\Country\CountryFullResource;
use App\Http\Resources\Client\Directory\CryptoCurrency\CryptoCurrencyResource;
use Illuminate\Support\Collection;

class FullDirectoryRepo extends BaseRepo implements FullDirectoryRepoContract
{
    /**
     * @var CountryRepo
     */
    protected $countryRepo;
    /**
     * @var CryptoCurrencyRepo
     */
    protected $cryptoCurrencyRepo;

    /**
     * FullDirectoryRepo constructor.
     *
     * @param CountryRepo        $countryRepo
     * @param CryptoCurrencyRepo $cryptoCurrencyRepo
     */
    public function __construct(CountryRepo $countryRepo, CryptoCurrencyRepo $cryptoCurrencyRepo)
    {
        $this->countryRepo = $countryRepo;
        $this->cryptoCurrencyRepo = $cryptoCurrencyRepo;
    }

    /**
     * get all country collection
     *
     * @return Collection
     */
    public function countries(): Collection
    {
        return $this->countryRepo->take()->get();
    }

    /**
     * get all crypto collection
     *
     * @return Collection
     */
    public function cryptoCurrencies(): Collection
    {
        return $this->cryptoCurrencyRepo->take()->get();
    }

    /**
     * get all directory data
     *
     * @return array
     */
    public function all(): array
    {
        // return [
        //     'crypto_currencies' => $this->cryptoCurrencies(),
        //     'countries'         => $this->countries(),
        // ];

        return [
            'crypto_currencies' => CryptoCurrencyResource::collection($this->cryptoCurrencies()),
            'countries'         => CountryFullResource::collection($this->countries()),
        ];
    }
}
