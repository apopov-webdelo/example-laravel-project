<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Partnership;

use App\Contracts\Partnership\ReferralRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Partnership\Partner;
use App\Models\Partnership\Referral;
use App\Models\User\User;
use App\Repositories\Repo;

/**
 * Class ReferralRepo
 *
 * @package App\Repositories\Partnership
 */
class ReferralRepo extends Repo implements ReferralRepoContract
{
    /**
     * @return RepoContract
     */
    protected function init() : RepoContract
    {
        $this->model = Referral::query();
        return $this;
    }

    /**
     * @param User $user
     *
     * @return ReferralRepoContract
     */
    public function filterByUser(User $user): ReferralRepoContract
    {
        $this->model->where('user_id', $user->id);
        return $this;
    }

    /**
     * @param Partner $partner
     *
     * @return ReferralRepoContract
     */
    public function filterByPartner(Partner $partner): ReferralRepoContract
    {
        $this->model->where('partner_id', $partner->id);
        return $this;
    }
}
