<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-27
 * Time: 13:23
 */

namespace App\Repositories\Partnership;

use App\Contracts\Partnership\PartnershipProgramContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Partnership\PartnershipProgram;
use App\Repositories\Repo;

/**
 * Class ProgramRepo
 *
 * @package App\Repositories\Partnership
 * @property PartnershipProgram $model
 */
class ProgramRepo extends Repo implements ProgramRepoContract
{
    const DEFAULT_DRIVER_CONFIG_PATH = 'app.partnership.default_driver';

    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = PartnershipProgram::query();
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $driverAlias
     *
     * @return PartnershipProgramContract
     */
    public function getByDriver(string $driverAlias): PartnershipProgramContract
    {
        return PartnershipProgram::where('driver', $driverAlias)->firstOrFail();
    }

    /**
     * {@inheritdoc}
     *
     * @return PartnershipProgramContract
     */
    public function getDefaultProgram(): PartnershipProgramContract
    {
        return $this->getByDriver(config(self::DEFAULT_DRIVER_CONFIG_PATH));
    }
}
