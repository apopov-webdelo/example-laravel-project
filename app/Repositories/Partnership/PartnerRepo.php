<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Partnership;

use App\Contracts\Partnership\PartnerRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Partnership\Partner;
use App\Models\Partnership\PartnershipProgram;
use App\Models\User\User;
use App\Repositories\Repo;

/**
 * Class PartnerRepo
 *
 * @package App\Repositories\Partnership
 */
class PartnerRepo extends Repo implements PartnerRepoContract
{
    /**
     * @return RepoContract
     */
    protected function init() : RepoContract
    {
        $this->model = Partner::query();
        return $this;
    }

    /**
     * @param User $user
     *
     * @return PartnerRepoContract
     */
    public function filterByUser(User $user): PartnerRepoContract
    {
        $this->model->where('user_id', $user->id);
        return $this;
    }

    /**
     * @param PartnershipProgram $partnershipProgram
     *
     * @return PartnerRepoContract
     */
    public function filterByPartnershipProgram(PartnershipProgram $partnershipProgram): PartnerRepoContract
    {
        $this->model->where('partnership_program_id', $partnershipProgram->id);
        return $this;
    }
}
