<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Ad;

use App\Models\Ad\Ad;
use App\Models\Directory\Bank;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\FiatConstants;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use App\Repositories\Directory\CurrencyRepo;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AdRepo
 *
 * @package App\Repositories\Ad
 * @property QueryBuilder|Ad $model
 */
class AdRepo
{
    /**
     * @var CurrencyRepo
     */
    protected $currencyRepo;
    /**
     * @var \Illuminate\Database\Eloquent\Builder|static $model
     */
    private $model;

    /**
     * AdRepo constructor.
     *
     * @param CurrencyRepo $currencyRepo
     */
    public function __construct(CurrencyRepo $currencyRepo)
    {
        $this->model = Ad::with(['author', 'paymentSystem', 'currency', 'country', 'cryptoCurrency']);
        $this->currencyRepo = $currencyRepo;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByUser(User $user)
    {
        $this->model->where('author_id', $user->id);

        return $this;
    }

    /**
     * Alias for filterByUser
     *
     * @param User $user
     *
     * @return $this
     */
    public function filterByAuthor(User $user)
    {
        return $this->filterByUser($user);
    }

    /**
     * @param PaymentSystem $paymentSystem
     *
     * @return $this
     */
    public function filterByPaymentSystem(PaymentSystem $paymentSystem)
    {
        $this->model->where('payment_system_id', $paymentSystem->id);

        return $this;
    }

    /**
     * @param Country $country
     *
     * @return $this
     */
    public function filterByCountry(Country $country)
    {
        $this->model->where('country_id', $country->id);

        return $this;
    }

    /**
     * @param Currency $currency
     *
     * @return $this
     */
    public function filterByCurrency(Currency $currency)
    {
        $this->model->where('currency_id', $currency->id);

        return $this;
    }

    /**
     * @param Bank $bank
     *
     * @return $this
     */
    public function filterByBank(Bank $bank)
    {
        $this->model->whereHas('banks', function (Builder $builder) use ($bank) {
            $builder->where('bank_id', $bank->id);
        });

        return $this;
    }

    /**
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return $this
     */
    public function filterByCryptoCurrency(CryptoCurrency $cryptoCurrency)
    {
        $this->model->where('crypto_currency_id', $cryptoCurrency->id);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForSale()
    {
        $this->filterByType(true);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForBuy()
    {
        $this->filterByType(false);

        return $this;
    }

    /**
     * @param bool $type
     *
     * @return $this
     */
    public function filterByType(bool $type)
    {
        $this->model->where('is_sale', $type);

        return $this;
    }

    /**
     * @param int      $price
     *
     * @param int|null $currencyId
     *
     * @return $this
     */
    public function filterPriceFrom(int $price, ?int $currencyId)
    {
        $currency = $this->getCurrency($currencyId);

        $this->model->where('price', '>=', currencyToCoins($price, $currency));

        return $this;
    }

    /**
     * @param int      $price
     *
     * @param int|null $currencyId
     *
     * @return $this
     */
    public function filterPriceTo(int $price, ?int $currencyId)
    {
        $currency = $this->getCurrency($currencyId);

        $this->model->where('price', '<=', currencyToCoins($price, $currency));

        return $this;
    }

    /**
     * @param float    $min
     *
     * @param int|null $currencyId
     *
     * @return $this
     */
    public function filterMinFrom(float $min, ?int $currencyId)
    {
        $currency = $this->getCurrency($currencyId);

        $this->model->where('min', '>=', currencyToCoins($min, $currency));

        return $this;
    }

    /**
     * @param float    $min
     *
     * @param int|null $currencyId
     *
     * @return $this
     */
    public function filterMinTo(float $min, ?int $currencyId)
    {
        $currency = $this->getCurrency($currencyId);

        $this->model->where('min', '<=', currencyToCoins($min, $currency));

        return $this;
    }

    /**
     * @param float    $max
     *
     * @param int|null $currencyId
     *
     * @return $this
     */
    public function filterMaxFrom(float $max, ?int $currencyId)
    {
        $currency = $this->getCurrency($currencyId);

        $this->model->where('max', '>=', currencyToCoins($max, $currency));

        return $this;
    }

    /**
     * @param float    $max
     *
     * @param int|null $currencyId
     *
     * @return $this
     */
    public function filterMaxTo(float $max, ?int $currencyId)
    {
        $currency = $this->getCurrency($currencyId);

        $this->model->where('max', '<=', currencyToCoins($max, $currency));

        return $this;
    }

    /**
     * @param float $turnover
     *
     * @return $this
     */
    public function filterUserQuilityFrom(float $turnover)
    {
        $this->model->where('turnover', '>=', $turnover);

        return $this;
    }

    /**
     * @param float $turnover
     *
     * @return $this
     */
    public function filterUserQuilityTo(float $turnover)
    {
        $this->model->where('turnover', '<=', $turnover);

        return $this;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function filterTimeFrom(int $time)
    {
        $this->model->where('time', '>=', $time);

        return $this;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function filterTimeTo(int $time)
    {
        $this->model->where('time', '<=', $time);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForLiquidityRequired()
    {
        $this->model->where('liquidity_required', true);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForEmailConfirmRequired()
    {
        $this->model->where('email_confirm_required', true);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForPhoneConfirmRequired()
    {
        $this->model->where('phone_confirm_required', true);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForTrustRequired()
    {
        $this->model->where('trust_required', true);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForTorDenied()
    {
        $this->model->where('tor_denied', true);

        return $this;
    }

    /**
     * @param int $dealCount
     *
     * @return $this
     */
    public function filterByMinDealFinishedFrom(int $dealCount)
    {
        $this->model->where('min_deal_finished_count', '>=', $dealCount);

        return $this;
    }

    /**
     * @param int $dealCount
     *
     * @return $this
     */
    public function filterByMinDealFinishedTo(int $dealCount)
    {
        $this->model->where('min_deal_finished_count', '<=', $dealCount);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterForActive()
    {
        $this->model->where('is_active', true);

        return $this;
    }

    /**
     * @return AdRepo
     */
    public function filterForInactive()
    {
        return $this->filterForBlocked();
    }

    /**
     * @return $this
     */
    public function filterForBlocked()
    {
        $this->model->where('is_active', false);

        return $this;
    }

    /**
     * @param float $rate
     *
     * @return $this
     */
    public function filterReviewRateFrom(float $rate)
    {
        $this->model->where('review_rate', '>=', $rate);

        return $this;
    }

    /**
     * @param float $rate
     *
     * @return $this
     */
    public function filterReviewRateTo(float $rate)
    {
        $this->model->where('review_rate', '<=', $rate);

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtFrom(Carbon $createdAt)
    {
        $this->model->where('created_at', '>=', $createdAt);

        return $this;
    }

    /**
     * @param Carbon $createdAt
     *
     * @return $this
     */
    public function filterByCreatedAtTo(Carbon $createdAt)
    {
        $this->model->where('created_at', '<=', $createdAt);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterByInactiveAuthor()
    {
        $this->model->whereHas('author', function (Builder $builder) {
            $builder->where('last_seen', '<', now()->subMinutes((int)config('app.user_idle_time')));
        });

        return $this;
    }

    /**
     * @param int $reputation
     *
     * @return $this
     */
    public function filterReputationFrom(int $reputation)
    {
        $this->model->whereHas('author', function (Builder $builder) use ($reputation) {
            $builder->whereHas('reputation', function (Builder $builder) use ($reputation) {
                $builder->where('rate', '>=', $reputation);
            });
        });

        return $this;
    }

    /**
     * @param int $reputation
     *
     * @return $this
     */
    public function filterReputationTo(int $reputation)
    {
        $this->model->whereHas('author', function (Builder $builder) use ($reputation) {
            $builder->whereHas('reputation', function (Builder $builder) use ($reputation) {
                $builder->where('rate', '<=', $reputation);
            });
        });

        return $this;
    }

    /**
     * @param int $turnover
     *
     * @return $this
     */
    public function filterByTurnoverFrom(int $turnover)
    {
        $this->model->where('turnover', '>=', $turnover);

        return $this;
    }

    /**
     * @param int $turnover
     *
     * @return $this
     */
    public function filterByTurnoverTo(int $turnover)
    {
        $this->model->where('turnover', '<=', $turnover);

        return $this;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function filterByTimeFrom(int $time)
    {
        $this->model->where('time', '>=', $time);

        return $this;
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function filterByTimeTo(int $time)
    {
        $this->model->where('time', '<=', $time);

        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function excludeById(int $id)
    {
        $this->model->where('id', '!=', $id);

        return $this;
    }

    public function orderBestBuy()
    {
        $this->model->orderByRaw('
            (
                SELECT reviews_coefficient 
                FROM users_statistics
                WHERE user_id = ads.author_id
            ) asc,
            (
                SELECT average_finish_deal_time 
                FROM users_statistics 
                WHERE user_id = ads.author_id
            ) asc,
            (
                (SELECT MIN(price) FROM ads) / price
            ) asc
        ');

        return $this;
    }

    public function orderBestSale()
    {
        $this->model->orderByRaw('
            (
                SELECT reviews_coefficient 
                FROM users_statistics
                WHERE user_id = ads.author_id
            ) asc,
            (
                SELECT average_finish_deal_time 
                FROM users_statistics 
                WHERE user_id = ads.author_id
            ) asc,
            (
                (SELECT MAX(price) FROM ads) / price
            ) asc
        ');

        return $this;
    }

    /**
     * @return Ad|QueryBuilder
     */
    public function take()
    {
        return $this->model;
    }

    /**
     * Return request currency or abstract
     *
     * @param int|null $currencyId
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    protected function getCurrency(?int $currencyId)
    {
        if ($currencyId) {
            $currency = $this->currencyRepo->take()
                                           ->where('id', $currencyId)
                                           ->get()->first();
        } else {
            $currency = app(Currency::class);
            $currency->accuracy = FiatConstants::DEFAULT_ACCURACY;
        }

        return $currency;
    }
}
