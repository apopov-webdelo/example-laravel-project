<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\ActivityLog;

use App\Contracts\Repositories\RepoContract;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Ad\Ad;
use App\Models\User\User;
use App\Repositories\Repo;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Class AdRepo
 *
 * @package App\Repositories\Ad
 * @property QueryBuilder|Ad $model
 */
class ActivityLogRepo extends Repo
{
    public function init(): RepoContract
    {
        $this->model = ActivityLog::with(['authorable']);

        return $this;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByUser(User $user)
    {
        $this->model
             ->where('authorable_id', $user->id)
             ->where('authorable_type', get_class($user));

        return $this;
    }
}
