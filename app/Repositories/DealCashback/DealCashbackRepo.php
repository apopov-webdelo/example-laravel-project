<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 21:33
 */

namespace App\Repositories\DealCashback;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\DealCashback\DealCashbackContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Deal\Deal;
use App\Models\DealCashback\DealCashback;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Repositories\Repo;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Class DealCashbackRepo
 *
 * @package App\Repositories\DealCashback
 */
class DealCashbackRepo extends Repo implements DealCashbackRepoContract
{
    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = DealCashback::query();
        return $this;
    }

    /**
     * @inheritdoc
     *
     * @param User $user
     *
     * @return DealCashbackRepoContract
     */
    public function filterByRecipient(User $user): DealCashbackRepoContract
    {
        $this->model->where('user_id', $user->id);

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @param Deal $deal
     *
     * @return DealCashbackRepoContract
     */
    public function filterByDeal(Deal $deal): DealCashbackRepoContract
    {
        $this->model->whereHas('dealsRelation', function (QueryBuilder $builder) use ($deal) {
            $builder->where('deal_id', $deal->id);
        });

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @param string $status
     *
     * @return DealCashbackRepoContract
     */
    public function filterByStatus(string $status): DealCashbackRepoContract
    {
        $this->model->where('status', $status);

        return $this;
    }

    /**
     * Check is entity exists by transaction code
     *
     * @param string $transactionId
     *
     * @return bool
     */
    public function isExists(string $transactionId): bool
    {
        return $this->model->where('transaction_id', $transactionId)->exists();
    }

    /**
     * @inheritdoc
     *
     * @param string $transactionId
     *
     * @return DealCashbackRepoContract
     */
    public function filterByTransactionId(string $transactionId): DealCashbackRepoContract
    {
        $this->model->where('transaction_id', $transactionId);

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return DealCashbackRepoContract
     */
    public function filterByCryptoCurrency(CryptoCurrencyContract $cryptoCurrency): DealCashbackRepoContract
    {
        /** @var CryptoCurrencyRepo $repo */
        $repo = app(CryptoCurrencyRepo::class);
        $cryptoCurrency = ($cryptoCurrency instanceof CryptoCurrency)
            ? $cryptoCurrency
            : $repo->getByCode($cryptoCurrency->getCode());
        $this->model->where('crypto_currency_id', $cryptoCurrency->id);

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return DealCashbackContract
     */
    public function getInProgressOrCreate(User $user, CryptoCurrencyContract $cryptoCurrency): DealCashbackContract
    {
        /** @var CryptoCurrencyRepo $repo */
        $repo = app(CryptoCurrencyRepo::class);
        $cryptoCurrency = ($cryptoCurrency instanceof CryptoCurrency)
            ? $cryptoCurrency
            : $repo->getByCode($cryptoCurrency);
        $cashback = DealCashback::where('user_id', $user->id)
            ->where('crypto_currency_id', $cryptoCurrency->id)
            ->where('status', DealCashback::STATUS_IN_PROGRESS)
            ->first();

        if (!$cashback) {
            $cashback = new DealCashback();
            $cashback->fill([
                'status'     => DealCashback::STATUS_IN_PROGRESS,
                'commission' => config('app.cashback.deals.commission'),
            ])
                ->user()
                ->associate($user);
            $cashback->cryptoCurrencyRelation()
                ->associate($cryptoCurrency);
            $cashback->save();
        }
        return $cashback;
    }
}
