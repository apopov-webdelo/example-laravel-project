<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Balance;

use App\Models\Balance\Balance;
use App\Models\Deal\Deal;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use Carbon\Carbon;

class BalanceLockedRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * ChatRepo constructor.
     */
    public function __construct()
    {
        $this->model = Balance::query();
    }

    /**
     * @param Balance $balance
     *
     * @return $this
     */
    public function filterByBalance(Balance $balance)
    {
        $this->model->where('balance_id', $balance->id);

        return $this;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function filterByAmountFrom(float $amount)
    {
        $this->model->where('amount', '>=', $amount);
        return $this;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function filterByAmountTo(float $amount)
    {
        $this->model->where('amount', '<=', $amount);
        return $this;
    }

    /**
     * @param Carbon $expirationAt
     *
     * @return $this
     */
    public function filterByExpirationAtFrom(Carbon $expirationAt)
    {
        $this->model->where('expiration_at', '>=', $expirationAt);
        return $this;
    }
    /**
     * @param Carbon $expirationAt
     *
     * @return $this
     */
    public function filterByExpirationAtTo(Carbon $expirationAt)
    {
        $this->model->where('expiration_at', '<=', $expirationAt);
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }
}