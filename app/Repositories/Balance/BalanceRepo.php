<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/29/18
 * Time: 17:27
 */

namespace App\Repositories\Balance;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Facades\CryptoExchange;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\FiatConstants;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;

class BalanceRepo
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $model;

    /**
     * ChatRepo constructor.
     */
    public function __construct()
    {
        $this->model = Balance::query();
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function filterByUser(User $user)
    {
        $this->model->where('user_id', $user->id);

        return $this;
    }

    /**
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return $this
     */
    public function filterByCryptoCurrency(CryptoCurrencyContract $cryptoCurrency)
    {
        $cryptoCurrency = $cryptoCurrency instanceof CryptoCurrency
            ? $cryptoCurrency
            : app(CryptoCurrencyRepo::class)->getByCode($cryptoCurrency->getCode());

        $this->filterByCryptoCurrencyId($cryptoCurrency->id);

        return $this;
    }

    /**
     * @param int $cryptoCurrencyId
     *
     * @return $this
     */
    public function filterByCryptoCurrencyId(int $cryptoCurrencyId)
    {
        $this->model->where('crypto_currency_id', $cryptoCurrencyId);

        return $this;
    }

    /**
     * @param float $balance
     *
     * @return $this
     */
    public function filterByBalanceFrom(float $balance)
    {
        $this->model->where('balance', '>=', $balance);
        return $this;
    }

    /**
     * @param float $balance
     *
     * @return $this
     */
    public function filterByBalanceTo(float $balance)
    {
        $this->model->where('balance', '<=', $balance);
        return $this;
    }

    /**
     * @param float $turnover
     *
     * @return $this
     */
    public function filterByTurnoverFrom(float $turnover)
    {
        $this->model->where('turnover', '>=', $turnover);
        return $this;
    }

    /**
     * @param float $turnover
     *
     * @return $this
     */
    public function filterByTurnoverTo(float $turnover)
    {
        $this->model->where('turnover', '<=', $turnover);
        return $this;
    }

    /**
     * @param float $escrow
     *
     * @return $this
     */
    public function filterByEscrowFrom(float $escrow)
    {
        $this->model->where('escrow', '>=', $escrow);
        return $this;
    }

    /**
     * @param float $escrow
     *
     * @return $this
     */
    public function filterByEscrowTo(float $escrow)
    {
        $this->model->where('escrow', '<=', $escrow);
        return $this;
    }

    /**
     * @param float $commission
     *
     * @return $this
     */
    public function filterByCommissionFrom(float $commission)
    {
        $this->model->where('commission', '>=', $commission);
        return $this;
    }

    /**
     * @param float $commission
     *
     * @return $this
     */
    public function filterByCommissionTo(float $commission)
    {
        $this->model->where('commission', '<=', $commission);
        return $this;
    }

    /**
     * Convert total user's crypto balances to fiat
     *
     * @param User   $user
     * @param int    $accuracy
     * @param string $currencyCode
     *
     * @return float
     */
    public function getTotalAmountInFiat(
        User $user,
        int $accuracy = 0,
        string $currencyCode = FiatConstants::USD
    ) {
        $amount = 0;
        /** @var Balance $balance */
        foreach ($this->filterByUser($user)->take()->get() as $balance) {
            $amount += CryptoExchange::convert(
                $balance->amount,
                $balance->cryptoCurrency,
                $currencyCode,
                $balance->cryptoCurrency->accuracy
            );
        }
        return round($amount, $accuracy);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take()
    {
        return $this->model;
    }

    /**
     * Reset filters applied to model
     *
     * @return BalanceRepo
     */
    public function reset(): self
    {
        $this->__construct();
        return $this;
    }
}
