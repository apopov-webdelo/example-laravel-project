<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-21
 * Time: 13:07
 */

namespace App\Repositories\Balance;

use App\Contracts\Repositories\RepoContract;
use App\Models\Balance\Balance;
use App\Models\Balance\Transaction;
use App\Repositories\Repo;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TransactionRepo
 *
 * @package App\Repositories\Balance
 * @property Builder $model
 */
class TransactionRepo extends Repo
{
    /**
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = Transaction::query();
        return $this;
    }

    /**
     * Filter transactions by amount from
     *
     * @param int $amount
     *
     * @return $this
     */
    public function filterByAmountFrom(int $amount)
    {
        $this->model->where('amount', '>=', $amount);

        return $this;
    }

    /**
     * Filter transactions by amount to
     *
     * @param int $amount
     *
     * @return $this
     */
    public function filterByAmountTo(int $amount)
    {
        $this->model->where('amount', '<=', $amount);

        return $this;
    }

    /**
     * Filter transactions by balance
     *
     * @param Balance $balance
     *
     * @return $this
     */
    public function filterByBalance(Balance $balance)
    {
        $this->model->where(function (Builder $builder) use ($balance) {
            $builder->where('remitter_balance_id', $balance->id)->orWhere('receiver_balance_id', $balance->id);
        });

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodToday(): TransactionRepo
    {
        $this->model
            ->where('created_at', '>=', now()->startOfDay())
            ->where('created_at', '<=', now()->endOfDay());

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodYesterday(): TransactionRepo
    {
        $this->model
            ->where('created_at', '>=', now()->subDay()->startOfDay())
            ->where('created_at', '<=', now()->subDay()->endOfDay());

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodThisWeek(): TransactionRepo
    {
        $this->model
            ->where('created_at', '>=', now()->startOfWeek())
            ->where('created_at', '<=', now()->endOfWeek());

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodLastWeek(): TransactionRepo
    {
        $this->model
            ->where('created_at', '>=', now()->subWeek()->startOfWeek())
            ->where('created_at', '<=', now()->subWeek()->endOfWeek());

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodThisMonth(): TransactionRepo
    {
        $this->model
            ->where('created_at', '>=', now()->startOfMonth())
            ->where('created_at', '<=', now()->endOfMonth());

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodLastMonth(): TransactionRepo
    {
        $this->model
            ->where('created_at', '>=', now()->subMonth()->startOfMonth())
            ->where('created_at', '<=', now()->subMonth()->endOfMonth());

        return $this;
    }

    /**
     * @return TransactionRepo
     */
    public function filterPeriodAny(): TransactionRepo
    {
        return $this;
    }

    /**
     * Filter transactions by receiver balance
     *
     * @param Balance $balance
     *
     * @return $this
     */
    public function filterReceiverBalance(Balance $balance)
    {
        $this->model->where('receiver_balance_id', $balance->id);

        return $this;
    }

    /**
     * Filter transactions by remitter balance
     *
     * @param Balance $balance
     *
     * @return $this
     */
    public function filterRemitterBalance(Balance $balance)
    {
        $this->model->where('remitter_balance_id', $balance->id);

        return $this;
    }
}
