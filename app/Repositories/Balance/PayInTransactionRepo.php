<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 12:50
 */

namespace App\Repositories\Balance;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\PayInTransactionRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Balance\PayInTransaction;
use App\Models\User\User;
use App\Repositories\Repo;

/**
 * Class PayInTransactionRepo
 *
 * @package App\Repositories\Balance
 */
class PayInTransactionRepo extends Repo implements PayInTransactionRepoContract
{
    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = PayInTransaction::query();
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $transactionId
     *
     * @return PayInTransactionContract|null
     */
    public function getByTransactionId(string $transactionId)
    {
        return $this->model->where('transaction_id', $transactionId)->first();
    }

    /**
     * {@inheritdoc}
     *
     * @param string $transactionId
     *
     * @return bool
     */
    public function isExists(string $transactionId): bool
    {
        return $this->model->where('transaction_id', $transactionId)->exists();
    }

    /**
     * {@inheritdoc}
     *
     * @param string                 $transactionId
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param int                    $amount
     *
     * @return PayInTransaction
     * @throws \Exception
     */
    public function store(
        string $transactionId,
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        int $amount
    ): PayInTransactionContract {
        $model = new PayInTransaction();
        $model->fill([
            'transaction_id' => $transactionId,
            'user_id'        => $user->getId(),
            'crypto_code'    => $cryptoCurrency->getCode(),
            'amount'         => $amount,
            'status'         => PayInTransaction::STATUS_PENDING,
        ]);

        if ($model->save()) {
            return $model;
        }

        throw new \Exception('Unexpected error during saving PayInTransaction model!');
    }

    /**
     * @param User $user
     *
     * @return PayInTransactionRepoContract
     */
    public function filterByUser(User $user): PayInTransactionRepoContract
    {
        $this->model->where('user_id', $user->id);

        return $this;
    }

    /**
     * @param string $code
     *
     * @return PayInTransactionRepoContract
     */
    public function filterByCryptoCode(string $code): PayInTransactionRepoContract
    {
        $this->model->where('crypto_code', $code);

        return $this;
    }

    /**
     * @param string $status
     *
     * @return PayInTransactionRepoContract
     */
    public function filterByStatus(string $status): PayInTransactionRepoContract
    {
        $this->model->where('status', $status);

        return $this;
    }
}
