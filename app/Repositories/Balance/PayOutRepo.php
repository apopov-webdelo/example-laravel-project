<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 12:50
 */

namespace App\Repositories\Balance;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\PayOutRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Balance\PayOut;
use App\Models\User\User;
use App\Repositories\Repo;

/**
 * Class PayInTransactionRepo
 *
 * @package App\Repositories\Balance
 */
class PayOutRepo extends Repo implements PayOutRepoContract
{
    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = PayOut::query();
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param int $id
     *
     * @return bool
     */
    public function isExists(int $id): bool
    {
        return $this->model->where('id', $id)->exists();
    }

    /**
     * {@inheritdoc}
     *
     * @param int $id
     *
     * @return PayOutRepoContract
     */
    public function filterByTransactionId(string $transactionId)
    {
        $this->model->where('transaction_id', $transactionId);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param User|int $user
     *
     * @return PayOutRepoContract
     */
    public function filterByUser($user)
    {
        $user_id = $user instanceof User ? $user->getId() : $user;
        $this->model->where('user_id', $user_id);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $status
     *
     * @return PayOutRepoContract
     */
    public function filterByStatus(string $status)
    {
        $this->model->where('status', $status);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @param User                   $user
     * @param int                    $amount
     * @param int                    $commission
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId
     *
     * @return PayOutContract
     * @throws \Exception
     */
    public function store(
        User $user,
        int $amount,
        int $commission,
        CryptoCurrencyContract $cryptoCurrency,
        string $walletId
    ): PayOutContract {
        $model = new PayOut();
        $model->fill([
            'user_id'     => $user->getId(),
            'crypto_code' => $cryptoCurrency->getCode(),
            'amount'      => $amount,
            'commission'  => $commission,
            'status'      => PayOut::STATUS_PENDING,
            'wallet_id'   => $walletId,
        ]);

        if ($model->save()) {
            return $model;
        }

        throw new \Exception('Unexpected error during saving PayInTransaction model!');
    }
}
