<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-21
 * Time: 13:07
 */

namespace App\Repositories\Balance;

use App\Contracts\Repositories\RepoContract;
use App\Models\Balance\Balance;
use App\Models\Balance\Transaction;
use App\Models\Balance\TransactionBalanceState;
use App\Repositories\Repo;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TransactionBalanceStateRepo
 *
 * @package App\Repositories\Balance
 * @property TransactionBalanceState|Builder $model
 */
class TransactionBalanceStateRepo extends Repo
{
    /**
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = TransactionBalanceState::query();
        return $this;
    }

    /**
     * Retrieve state for transactions and balance
     *
     * @param Transaction $transaction
     * @param Balance     $balance
     *
     * @return TransactionBalanceState|null
     */
    public function getByTransaction(Transaction $transaction, Balance $balance)
    {
        return $this->model->where('transaction_id', $transaction->id)
                           ->where('balance_id', $balance->id)
                           ->first();
    }
}
