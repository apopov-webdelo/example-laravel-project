<?php

namespace App\Repositories\Bcrypt;

use App\Contracts\Bcrypt\ModelBcKeysContract;
use App\Contracts\Bcrypt\ModelBcKeysRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Bcrypt\ModelBcKey;
use App\Repositories\Repo;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModelBcKeysRepo
 *
 * @package App\Repositories\Bcrypt
 */
class ModelBcKeysRepo extends Repo implements ModelBcKeysRepoContract
{
    /**
     * @var Builder
     */
    protected $model;
    /**
     * @var string
     */
    protected $alias;

    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    protected function init(): RepoContract
    {
        $this->model = ModelBcKey::query();

        return $this;
    }

    /**
     * Check if key exists
     *
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return bool
     */
    public function isExists(ModelBcKeysContract $model, string $alias): bool
    {
        return $this->key($model, $alias) ? true : false;
    }

    /**
     * Get key by alias
     *
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return string|bool
     */
    public function key(ModelBcKeysContract $model, string $alias)
    {
        $modelKey = $this->getModelKey($model, $alias);

        return $modelKey ? $modelKey->key : false;
    }

    /**
     * Save or update existing key
     *
     * @param ModelBcKeysContract $model
     * @param string              $key
     *
     * @return bool
     */
    public function save(ModelBcKeysContract $model, string $key): bool
    {
        try {
            $modelKey = $this->getModelKey($model, $this->alias);

            if ($modelKey) {
                $modelKey->update(['key' => $key]);
            } else {
                $model->keys()
                      ->save(new ModelBcKey([
                          'key'   => $key,
                          'alias' => $this->alias,
                      ]));
            }

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    /**
     * Delete key by alias
     *
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return bool
     */
    public function remove(ModelBcKeysContract $model, string $alias): bool
    {
        try {
            $modelKey = $this->getModelKey($model, $this->alias);

            if ($modelKey) {
                $modelKey->delete();
            }

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    /**
     * @param ModelBcKeysContract $model
     *
     * @return ModelBcKeysRepoContract
     */
    public function model(ModelBcKeysContract $model): ModelBcKeysRepoContract
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param string $alias
     *
     * @return ModelBcKeysRepoContract
     */
    public function alias(string $alias): ModelBcKeysRepoContract
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return ModelBcKey
     */
    protected function getModelKey(ModelBcKeysContract $model, string $alias)
    {
        return $model->keys()
                     ->where('alias', $alias)
                     ->get()
                     ->first();
    }
}
