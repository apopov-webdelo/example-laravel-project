<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/10/18
 * Time: 15:12
 */

namespace App\Repositories;

use App\Contracts\Repositories\RepoContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Base class for repo objects
 *
 * @package App\Repositories
 */
abstract class Repo implements RepoContract
{
    /**
     * @var Model|Builder
     */
    protected $model;

    /**
     * Repo constructor
     *
     * Init empty builder
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Set default builder to model property
     *
     * @return mixed
     */
    abstract protected function init(): RepoContract;

    /**
     * {@inheritdoc}
     *
     * @return RepoContract
     */
    public function reset(): RepoContract
    {
        return $this->init();
    }

    /**
     * {@inheritdoc}
     *
     * @return Builder|Model
     */
    public function take(): Builder
    {
        return $this->model;
    }

    /**
     * Valid clone repo object
     */
    public function __clone()
    {
        $this->model = clone $this->model;
    }

    /**
     * Retrieve instant model by Id
     *
     * @param $id
     *
     * @return Model
     */
    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }
}
