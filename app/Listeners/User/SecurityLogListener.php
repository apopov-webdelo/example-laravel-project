<?php

namespace App\Listeners\User;

use App\Events\User\SecurityLogEvent;
use App\Listeners\BaseListener;
use App\Models\User\SecurityLog;
use App\Services\User\Security\SecurityLogService;
use Illuminate\Contracts\Queue\ShouldQueue;

class SecurityLogListener extends BaseListener implements ShouldQueue
{
    /**
     * @param SecurityLogEvent $event
     *
     * @return SecurityLog
     */
    public function handle(SecurityLogEvent $event)
    {
        return app(SecurityLogService::class, [
            'user' => $event->user,
            'sessionStorage' => $event->sessionStorage
        ])->addRecord(...array_values($event->securityInfo));
    }
}
