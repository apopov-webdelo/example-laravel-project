<?php

namespace App\Listeners\User;

use App\Events\User\LogoutSuccess;
use App\Listeners\BaseListener;
use App\Services\User\Session\SessionLogService;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogoutSuccessSessionListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  LogoutSuccess $event
     *
     * @return void
     */
    public function handle(LogoutSuccess $event)
    {
        app(
            SessionLogService::class,
            ['user' => $event->user, 'sessionStorage' => $event->sessionStorage]
        )->outGood();
    }
}
