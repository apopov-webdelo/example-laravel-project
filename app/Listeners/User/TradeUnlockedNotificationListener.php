<?php

namespace App\Listeners\User;

use App\Events\User\TradeUnlocked;
use App\Listeners\BaseListener;
use App\Notifications\User\TradeUnlockedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class TradeUnlockedNotificationListener extends BaseListener implements ShouldQueue
{
    /**
     * @param TradeUnlocked $event
     */
    public function handle(TradeUnlocked $event)
    {
        Notification::send($event->user, new TradeUnlockedNotification());
    }
}
