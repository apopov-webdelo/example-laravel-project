<?php

namespace App\Listeners\User;

use App\Events\User\Unlocked;
use App\Listeners\BaseListener;
use App\Notifications\User\UnlockedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class UnlockedNotificationListener extends BaseListener implements ShouldQueue
{
    /**
     * @param Unlocked $event
     */
    public function handle(Unlocked $event)
    {
        Notification::send($event->user, new UnlockedNotification());
    }
}
