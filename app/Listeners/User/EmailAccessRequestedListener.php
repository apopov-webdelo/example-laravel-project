<?php

namespace App\Listeners\User;

use App\Events\User\EmailAccessRequested;
use App\Listeners\BaseListener;
use App\Notifications\User\EmailAccessNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailAccessRequestedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  EmailAccessRequested  $event
     * @return void
     */
    public function handle(EmailAccessRequested $event)
    {
        Notification::send($event->user, new EmailAccessNotification());
    }
}
