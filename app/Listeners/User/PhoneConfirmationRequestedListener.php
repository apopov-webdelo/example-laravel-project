<?php

namespace App\Listeners\User;

use App\Events\User\PhoneConfirmationRequested;
use App\Listeners\BaseListener;
use App\Notifications\User\PhoneConfirmationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class PhoneConfirmationRequestedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  PhoneConfirmationRequested  $event
     * @return void
     */
    public function handle(PhoneConfirmationRequested $event)
    {
        Notification::send($event->user, new PhoneConfirmationNotification());
    }
}
