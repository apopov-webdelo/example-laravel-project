<?php

namespace App\Listeners\User;

use App\Events\User\LoginSuccess;
use App\Listeners\BaseListener;
use App\Services\User\Session\SessionLogService;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoginSuccessSessionListener extends BaseListener implements ShouldQueue
{
    /**
     * @param LoginSuccess $event
     */
    public function handle(LoginSuccess $event)
    {
        app(SessionLogService::class, ['user' =>$event->user, 'sessionStorage' => $event->sessionStorage])->inGood();
    }
}
