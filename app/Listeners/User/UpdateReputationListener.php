<?php

namespace App\Listeners\User;

use App\Events\Review\ReviewPlaced;
use App\Listeners\BaseListener;
use App\Services\Review\UpdateReputationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class UpdateReputationListener extends BaseListener implements ShouldQueue
{
    /**
     * @var UpdateReputationService
     */
    protected $service;

    /**
     * @param UpdateReputationService $service
     */
    public function __construct(UpdateReputationService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  ReviewPlaced  $event
     * @return void
     */
    public function handle($event)
    {
        try {
            $this->service->updateByReview($event->review);
            Log::info('Reputation was updated for user ID='.$event->review->recipient->id.'.');
        } catch (\Exception $e) {
            Log::critical('Unexpected error during user reputation updating! (Message: '.$e->getMessage().')');
        }
    }
}
