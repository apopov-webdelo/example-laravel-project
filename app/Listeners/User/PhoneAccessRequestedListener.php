<?php

namespace App\Listeners\User;

use App\Events\User\PhoneAccessRequested;
use App\Listeners\BaseListener;
use App\Notifications\User\PhoneAccessNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class PhoneAccessRequestedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  PhoneAccessRequested  $event
     * @return void
     */
    public function handle(PhoneAccessRequested $event)
    {
        Notification::send($event->user, new PhoneAccessNotification());
    }
}
