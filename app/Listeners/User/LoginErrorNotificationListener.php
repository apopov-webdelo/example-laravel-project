<?php

namespace App\Listeners\User;

use App\Events\User\LoginError;
use App\Listeners\BaseListener;
use App\Notifications\User\LoginErrorNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class LoginErrorNotificationListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  LoginError $event
     *
     * @return void
     */
    public function handle(LoginError $event)
    {
        Notification::send($event->user, new LoginErrorNotification($event));
    }
}
