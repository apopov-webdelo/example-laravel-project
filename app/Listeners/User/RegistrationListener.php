<?php

namespace App\Listeners\User;

use App\Listeners\BaseListener;
use App\Notifications\User\RegistrationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class RegistrationListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send($event->user, new RegistrationNotification());
    }
}
