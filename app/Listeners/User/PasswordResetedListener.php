<?php

namespace App\Listeners\User;

use App\Events\User\PasswordReseted;
use App\Listeners\BaseListener;
use App\Notifications\User\PasswordResetedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class PasswordResetedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  PasswordReseted  $event
     * @return void
     */
    public function handle(PasswordReseted $event)
    {
        Notification::send(
            $event->user,
            app(PasswordResetedNotification::class, ['reason' => $event->reason, 'password' => $event->password])
        );
    }
}
