<?php

namespace App\Listeners\User;

use App\Events\User\LoginError;
use App\Listeners\BaseListener;
use App\Services\User\Session\SessionLogService;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoginErrorSessionListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  LoginError  $event
     * @return void
     */
    public function handle(LoginError $event)
    {
        app(SessionLogService::class, ['user' =>$event->user, 'sessionStorage' => $event->sessionStorage])->inProblem();
    }
}
