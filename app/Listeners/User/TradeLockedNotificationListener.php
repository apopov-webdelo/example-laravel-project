<?php

namespace App\Listeners\User;

use App\Events\User\TradeLocked;
use App\Listeners\BaseListener;
use App\Notifications\User\TradeLockedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class TradeLockedNotificationListener extends BaseListener implements ShouldQueue
{
    /**
     * @param TradeLocked $event
     */
    public function handle(TradeLocked $event)
    {
        Notification::send($event->user, new TradeLockedNotification());
    }
}
