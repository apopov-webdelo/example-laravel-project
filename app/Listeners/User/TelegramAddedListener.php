<?php

namespace App\Listeners\User;

use App\Events\User\TelegramAdded;
use App\Listeners\BaseListener;
use App\Notifications\User\TelegramConfirmationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Notification;

class TelegramAddedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send($event->user, new TelegramConfirmationNotification());
    }
}
