<?php

namespace App\Listeners\User;

use App\Events\User\Locked;
use App\Listeners\BaseListener;
use App\Notifications\User\LockedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class LockedNotificationListener extends BaseListener implements ShouldQueue
{
    /**
     * @param Locked $event
     */
    public function handle(Locked $event)
    {
        Notification::send($event->user, new LockedNotification());
    }
}
