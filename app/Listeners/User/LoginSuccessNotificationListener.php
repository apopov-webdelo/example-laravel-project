<?php

namespace App\Listeners\User;

use App\Events\User\LoginSuccess;
use App\Listeners\BaseListener;
use App\Notifications\User\LoginSuccessNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class LoginSuccessNotificationListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  LoginSuccess  $event
     * @return void
     */
    public function handle(LoginSuccess $event)
    {
        Notification::send($event->user, new LoginSuccessNotification($event));
    }
}
