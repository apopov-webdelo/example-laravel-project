<?php

namespace App\Listeners\User;

use App\Events\User\EmailConfirmationRequested;
use App\Listeners\BaseListener;
use App\Notifications\User\EmailConfirmationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailConfirmationRequestedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  EmailConfirmationRequested  $event
     * @return void
     */
    public function handle(EmailConfirmationRequested $event)
    {
        Notification::send($event->user, new EmailConfirmationNotification());
    }
}
