<?php

namespace App\Listeners\User;

use App\Listeners\BaseListener;
use App\Models\Chat\Chat;
use App\Notifications\User\ChatPlacedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Notification;

class ChatPlacedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send($this->getRecipientsExcludeAuthor($event->chat), new ChatPlacedNotification($event->chat));
    }

    /**
     * @param Chat $chat
     *
     * @return Collection
     */
    private function getRecipientsExcludeAuthor(Chat $chat) : Collection
    {
        return $chat->recipients->where('id', '!=', $chat->author->id);
    }
}
