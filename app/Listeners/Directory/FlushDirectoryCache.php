<?php

namespace App\Listeners\Directory;

use App\Repositories\Directory\CachedFullDirectoryRepo;
use Illuminate\Contracts\Queue\ShouldQueue;

class FlushDirectoryCache implements ShouldQueue
{
    /**
     * @var CachedFullDirectoryRepo
     */
    protected $repo;

    /**
     * FlushDirectoryCache constructor.
     *
     * @param CachedFullDirectoryRepo $repo
     */
    public function __construct(CachedFullDirectoryRepo $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Handle the event.
     *
     * @param object $event
     *
     * @return void
     */
    public function handle($event)
    {
        $this->repo->flush();
    }
}
