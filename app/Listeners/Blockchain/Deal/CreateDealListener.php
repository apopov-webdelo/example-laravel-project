<?php

namespace App\Listeners\Blockchain\Deal;

use App\Contracts\Services\Blockchain\To\Deal\DealCreateContract;
use App\Exceptions\Blockchain\Stellar\MissingKeyStellarException;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use Illuminate\Support\Facades\Log;

/**
 * Sync blockchain deal create actions
 *
 * Class CreateDeal
 *
 * @package App\Listeners\Blockchain\Deal
 */
class CreateDealListener // extends BaseListener implements ShouldQueue
{
    /**
     * @var DealCreateContract
     */
    protected $service;

    /**
     * @var Deal
     */
    private $deal;
    /**
     * @var Ad
     */
    private $ad;

    /**
     * Create the event listener.
     *
     * @param DealCreateContract $service
     */
    public function __construct(DealCreateContract $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return bool
     * @throws MissingKeyStellarException
     */
    public function handle($event)
    {
        $this->deal = $event->deal;
        $this->ad = $this->deal->ad;

        if ($this->ad->isProtected()) {
            $sellerKey = $this->getSellerKey();
            $buyerKey = $this->getBuyerKey();

            if ($sellerKey && $buyerKey) {
                $this->service->keys($buyerKey, $sellerKey);
            } else {
                throw new MissingKeyStellarException;
            }
        }

        return $this->service->create($this->deal);
    }

    /**
     * @param $event
     * @param $exception
     */
    public function failed($event, $exception)
    {
        Log::warning('Listener fallen with event ' . get_class($event) . ' in queue.', ['exception' => $exception]);
    }

    /**
     * @return false|string
     */
    private function getSellerKey()
    {
        if ($this->ad->isSale()) {
            return $this->getOwnerKey($this->ad);
        }

        return $this->getOfferKey($this->deal);
    }

    /**
     * @return false|string
     */
    private function getBuyerKey()
    {
        if ($this->ad->isSale()) {
            return $this->getOfferKey($this->deal);
        }

        return $this->getOwnerKey($this->ad);
    }

    /**
     * @param Ad|Deal $model
     *
     * @return false|string
     */
    private function getOwnerKey($model)
    {
        return $model->getBcKey('owner');
    }

    /**
     * @param Ad|Deal $model
     *
     * @return false|string
     */
    private function getOfferKey($model)
    {
        return $model->getBcKey('offer');
    }
}
