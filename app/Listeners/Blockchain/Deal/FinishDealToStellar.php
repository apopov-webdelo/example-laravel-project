<?php

namespace App\Listeners\Blockchain\Deal;

use App\Contracts\Services\Blockchain\To\Deal\DealFinishContract;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class FinishDealToStellar
 *
 * Responsible for sending deal/finish to Stellar
 *
 * @package App\Listeners\Blockchain\Deal
 */
class FinishDealToStellar extends BaseListener implements ShouldQueue
{
    /**
     * @var DealFinishContract
     */
    protected $service;

    /**
     * Create the event listener.
     *
     * @param DealFinishContract $service
     */
    public function __construct(DealFinishContract $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return bool
     */
    public function handle($event)
    {
        return $this->service->finish($event->deal);
    }
}
