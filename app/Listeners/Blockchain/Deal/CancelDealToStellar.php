<?php

namespace App\Listeners\Blockchain\Deal;

use App\Contracts\Services\Blockchain\To\Deal\DealCancelContract;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class CancelDealToStellar
 *
 * Send request to Stellar to cancel deal
 *
 * @package App\Listeners\Blockchain\Deal
 */
class CancelDealToStellar extends BaseListener implements ShouldQueue
{
    /**
     * @var DealCancelContract
     */
    protected $service;

    /**
     * Create the event listener.
     *
     * @param DealCancelContract $service
     */
    public function __construct(DealCancelContract $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return bool
     */
    public function handle($event)
    {
        return $this->service->cancel($event->deal);
    }
}
