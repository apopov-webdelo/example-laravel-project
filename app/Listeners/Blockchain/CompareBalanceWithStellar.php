<?php

namespace App\Listeners\Blockchain;

use App\Events\Blockchain\StartCompareBalanceWithStellar;
use App\Jobs\Blockchain\CompareBalanceJob;

/**
 * Class CompareBalanceOnPayIn
 *
 * @package App\Listeners\Blockchain\Deal
 */
class CompareBalanceWithStellar
{
    /**
     * Handle the event.
     *
     * @param  StartCompareBalanceWithStellar $event
     *
     * @return bool
     */
    public function handle(StartCompareBalanceWithStellar $event)
    {
        CompareBalanceJob::dispatch($event->user, $event->crypto);

        return true;
    }
}
