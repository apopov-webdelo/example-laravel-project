<?php

namespace App\Listeners\Blockchain\Wallet;

use App\Listeners\BaseListener;
use App\Models\User\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\JiraException;

class WalletCreationErrorListener extends BaseListener implements ShouldQueue
{
    /**
     * @var IssueField
     */
    protected $jiraIssue;
    /**
     * @var IssueService
     */
    protected $jiraService;

    /**
     * Create the event listener.
     *
     * @param IssueField   $jiraIssue
     * @param IssueService $jiraService
     */
    public function __construct(IssueField $jiraIssue, IssueService $jiraService)
    {
        $this->jiraIssue = $jiraIssue;
        $this->jiraService = $jiraService;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return bool
     * @throws \JsonMapper_Exception
     */
    public function handle($event)
    {
        /* @var User $user */
        $user = $event->user;
        try {
            $this->jiraIssue->setProjectKey(config('app.jira.board'))
                            ->setSummary("User ID {$user->id}: wallet not created! User's trading is blocked.")
                            ->setPriorityName("Highest")
                            ->setIssueType("Bug");

            $this->jiraService->create($this->jiraIssue);
        } catch (JiraException $exception) {
            return false;
        }

        return true;
    }
}
