<?php

namespace App\Listeners\Blockchain;

use App\Events\Blockchain\StellarLogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Response;
use Illuminate\Queue\InteractsWithQueue;

class LogErrorStellarRequest implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param StellarLogEvent $event
     *
     * @return void
     */
    public function handle(StellarLogEvent $event)
    {
        if ($this->isErrorEvent($event)) {
            $this->notify($event);
        }
    }

    /**
     * @param StellarLogEvent $event
     *
     * @return bool
     */
    private function isErrorEvent(StellarLogEvent $event)
    {
        return (int)$event->response_code >= Response::HTTP_MULTIPLE_CHOICES || (int)$event->response_code === 0;
    }

    /**
     * @param StellarLogEvent $event
     */
    private function notify(StellarLogEvent $event)
    {
        $message = 'Bad request [' . strtoupper($event->type) . '] Stellar';
        $data = [
            'method'        => $event->method,
            'route'         => $event->route,
            'response_code' => $event->response_code,
            'request'       => $event->request,
            'response'      => $event->response,
        ];

        toSlack($message, $data);
        toTelegram($message, $data);
    }
}
