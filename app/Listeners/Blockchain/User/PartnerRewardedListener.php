<?php

namespace App\Listeners\Blockchain\User;

use App\Contracts\Services\Blockchain\To\User\PartnerRewardedContract;
use App\Events\Partnership\PartnerRewarded;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Send partner reward info to Stellar
 *
 * Class PartnerRewardedListener
 *
 * @package App\Listeners\Partnership
 */
class PartnerRewardedListener extends BaseListener implements ShouldQueue
{
    /**
     * @var PartnerRewardedContract
     */
    protected $service;

    /**
     * Create the event listener.
     *
     * @param PartnerRewardedContract $service
     */
    public function __construct(PartnerRewardedContract $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  PartnerRewarded  $event
     * @return void
     */
    public function handle(PartnerRewarded $event)
    {
        $this->service->reward($event->commissionStorage);
    }
}
