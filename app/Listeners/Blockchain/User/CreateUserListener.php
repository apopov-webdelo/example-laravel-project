<?php

namespace App\Listeners\Blockchain\User;

use App\Contracts\Services\Blockchain\To\User\CreateUserContract;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserListener extends BaseListener implements ShouldQueue
{
    /**
     * @var CreateUserContract
     */
    protected $service;

    /**
     * Create the event listener.
     *
     * @param CreateUserContract $service
     */
    public function __construct(CreateUserContract $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return bool
     */
    public function handle($event)
    {
        return $this->service->createUser($event->user);
    }
}
