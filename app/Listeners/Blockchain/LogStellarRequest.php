<?php

namespace App\Listeners\Blockchain;

use App\Events\Blockchain\StellarLogEvent;
use App\Models\Blockchain\StellarLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogStellarRequest implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param  StellarLogEvent $event
     *
     * @return void
     */
    public function handle(StellarLogEvent $event)
    {
        StellarLog::insert([
            'type'          => $event->type,
            'method'        => $event->method,
            'route'         => $event->route,
            'request'       => $event->request,
            'response'      => $event->response,
            'response_code' => $event->response_code,
            'created_at'    => now(),
        ]);
    }
}
