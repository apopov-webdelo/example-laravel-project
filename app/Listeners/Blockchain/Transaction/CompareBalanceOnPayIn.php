<?php

namespace App\Listeners\Blockchain\Transaction;

use App\Events\Balance\PayIn\PayInTransactionEvent;
use App\Jobs\Blockchain\CompareBalanceJob;

/**
 * Class CompareBalanceOnPayIn
 *
 * @package App\Listeners\Blockchain\Deal
 */
class CompareBalanceOnPayIn
{
    /**
     * Handle the event.
     *
     * @param  PayInTransactionEvent $event
     *
     * @return bool
     */
    public function handle(PayInTransactionEvent $event)
    {
        CompareBalanceJob::dispatch($event->payInTransaction->recipient(), $event->payInTransaction->cryptoCurrency());

        return true;
    }
}
