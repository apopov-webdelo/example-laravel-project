<?php

namespace App\Listeners\Blockchain\Transaction;

use App\Events\Balance\PayOut\PayOutProcessed;
use App\Jobs\Blockchain\CompareBalanceJob;

/**
 * Class CompareBalanceOnPayOut
 *
 * @package App\Listeners\Blockchain\Deal
 */
class CompareBalanceOnPayOut
{
    /**
     * Handle the event.
     *
     * @param  PayOutProcessed $event
     *
     * @return bool
     */
    public function handle(PayOutProcessed $event)
    {
        CompareBalanceJob::dispatch($event->payOut->user(), $event->payOut->cryptoCurrency());

        return true;
    }
}
