<?php

namespace App\Listeners\DealCashback;

use App\Contracts\DealCashback\DealCashbackProcessServiceContract;
use App\Events\Deal\DealFinished;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessDealCashback extends BaseListener implements ShouldQueue
{
    /**
     * @var DealCashbackProcessServiceContract
     */
    private $cashbackProcessService;

    /**
     * DealFinishedCashbackStateListener constructor.
     *
     * @param DealCashbackProcessServiceContract $cashbackProcessService
     */
    public function __construct(DealCashbackProcessServiceContract $cashbackProcessService)
    {
        $this->cashbackProcessService = $cashbackProcessService;
    }

    /**
     * @param DealFinished $event
     */
    public function handle(DealFinished $event)
    {
        $this->cashbackProcessService->process($event->deal);
    }
}
