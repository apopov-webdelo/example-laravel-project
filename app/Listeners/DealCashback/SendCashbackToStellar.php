<?php

namespace App\Listeners\DealCashback;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Contracts\Services\Blockchain\To\User\ProcessDealCashbackContract;
use App\Events\DealCashback\DealCashbackState;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCashbackToStellar extends BaseListener implements ShouldQueue
{
    /**
     * @var ProcessDealCashbackContract
     */
    protected $service;

    /**
     * SendCashbackToStellar constructor.
     *
     * @param ProcessDealCashbackContract $service
     */
    public function __construct(ProcessDealCashbackContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param DealCashbackState $event
     */
    public function handle(DealCashbackState $event)
    {
        /* @var DealCashbackCommissionStorageContract $commissionStorage */
        $commissionStorage = app(DealCashbackCommissionStorageContract::class, ['cashback' => $event->cashback]);
        $this->service->reward($commissionStorage);
    }
}
