<?php

namespace App\Listeners\DealCashback;

use App\Events\DealCashback\DealCashbackCancelState;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendCashbackCancelAlert extends BaseListener implements ShouldQueue
{
    /**
     * @param DealCashbackCancelState $event
     */
    public function handle(DealCashbackCancelState $event)
    {
        $message = "Cashback #{$event->cashback->id} canceled by Stellar.";
        $data = [
            'error'  => $event->error,
            'reason' => $event->reason,
        ];

        Log::channel('cashback')->alert($message, $data);

        toJira($message, $data);

        toSlack($message, $data);
    }
}
