<?php

namespace App\Listeners\DealCashback;

use App\Contracts\Balance\CashbackServiceContract;
use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Events\DealCashback\DealCashbackDoneState;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

class CashbackTransfer extends BaseListener implements ShouldQueue
{
    /**
     * @var CashbackServiceContract
     */
    protected $service;

    public function __construct(CashbackServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param DealCashbackDoneState $event
     */
    public function handle(DealCashbackDoneState $event)
    {
        /* @var DealCashbackCommissionStorageContract $commissionStorage */
        $commissionStorage = app(DealCashbackCommissionStorageContract::class, ['cashback' => $event->cashback]);

        try {
            $this->service->description($this->getDescription($commissionStorage))
                          ->cashback($commissionStorage);
        } catch (\Exception $e) {
            toSlack('Cashback transfer fail', [
                'exception' => $e->getMessage(),
                'user'      => $event->cashback->recipient()->id,
                'crypto'    => $event->cashback->cryptoCurrency()->getCode(),
            ]);
        }
    }

    /**
     * @param DealCashbackCommissionStorageContract $cashbackStorage
     *
     * @return array|string|null
     */
    private function getDescription(DealCashbackCommissionStorageContract $cashbackStorage)
    {
        return __('balance.transaction.cashback', [
            'cashback_id' => $cashbackStorage->entity()->entityId(),
            'deal_list'   => $cashbackStorage->deals()->pluck('id')->implode(', '),
        ]);
    }
}
