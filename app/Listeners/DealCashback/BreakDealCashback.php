<?php

namespace App\Listeners\DealCashback;

use App\Contracts\DealCashback\DealCashbackBreakServiceContract;
use App\Events\Deal\DealCanceled;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

class BreakDealCashback extends BaseListener implements ShouldQueue
{
    /**
     * @var DealCashbackBreakServiceContract
     */
    private $cashbackBreakService;

    /**
     * DealCanceledCashbackStateListener constructor.
     *
     * @param DealCashbackBreakServiceContract $cashbackBreakService
     */
    public function __construct(DealCashbackBreakServiceContract $cashbackBreakService)
    {
        $this->cashbackBreakService = $cashbackBreakService;
    }

    /**
     * @param DealCanceled $event
     */
    public function handle(DealCanceled $event)
    {
        $this->cashbackBreakService->break($event->deal);
    }
}
