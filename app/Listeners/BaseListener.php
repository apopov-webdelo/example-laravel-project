<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

abstract class BaseListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function failed($event, $exception)
    {
        Log::warning('Listener fallen with event '.get_class($event).' in queue.', ['exception'=>$exception]);
    }
}
