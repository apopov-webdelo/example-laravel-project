<?php

namespace App\Listeners\User;

use App\Events\Review\ReviewPlaced;
use App\Listeners\BaseListener;
use App\Notifications\User\ReviewPlacedToRecipientNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class ReviewPlacedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  ReviewPlaced  $event
     * @return void
     */
    public function handle(ReviewPlaced $event)
    {
        Notification::send($event->review->recipient, new ReviewPlacedToRecipientNotification($event->review));
    }
}
