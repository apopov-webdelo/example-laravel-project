<?php

namespace App\Listeners\Deal;

use App\Jobs\Deal\UpdateDealCryptoAmountInUsd;

class DealUpdateUsdAmount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return bool
     */
    public function handle($event)
    {
        UpdateDealCryptoAmountInUsd::dispatch($event->deal);

        return true;
    }
}
