<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealPlaced;
use App\Listeners\BaseListener;
use App\Notifications\Deal\DealPlacedToAdAuthorNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class DealPlacedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  DealPlaced  $event
     * @return void
     */
    public function handle(DealPlaced $event)
    {
        Notification::send($event->deal->ad->author, new DealPlacedToAdAuthorNotification($event->deal));

        $amount = currencyFromCoins($event->deal->fiat_amount, $event->deal->currency);
        $price = currencyFromCoins($event->deal->price, $event->deal->currency);
        $cryptoAmount = currencyFromCoins($event->deal->crypto_amount, $event->deal->cryptoCurrency);

        toTelegramDeal("Создана сделка #{$event->deal->id} 
        Продавец: {$event->deal->getSeller()->login} 
        Покупатель: {$event->deal->getBuyer()->login} 
        Сумма: {$amount} {$event->deal->currency->getCode()} 
        Криптовалюта: {$cryptoAmount} {$event->deal->cryptoCurrency->getCode()} 
        Цена: {$price} {$event->deal->currency->getCode()}");
    }
}
