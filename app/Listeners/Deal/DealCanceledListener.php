<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealCanceled;
use App\Listeners\BaseListener;
use App\Notifications\Deal\DealCanceledToSellerNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class DealCanceledListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  DealCanceled  $event
     * @return void
     */
    public function handle(DealCanceled $event)
    {
        $recipient = $event->deal->ad->is_sale
            ? $event->deal->ad->author
            : $event->deal->author;

        Notification::send($recipient, new DealCanceledToSellerNotification($event->deal));
    }
}
