<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealFinished;
use App\Listeners\BaseListener;
use App\Notifications\Deal\DealFinishedToBuyerNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class DealFinishedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  DealFinished  $event
     * @return void
     */
    public function handle(DealFinished $event)
    {
        $recipient = $event->deal->ad->is_sale
            ? $event->deal->author
            : $event->deal->ad->author;

        Notification::send($recipient, new DealFinishedToBuyerNotification($event->deal));
    }
}
