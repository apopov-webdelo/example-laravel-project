<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealDisputed;
use App\Listeners\BaseListener;
use App\Notifications\Deal\DealDisputedToAdminNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class DealDisputedAdminEmailNotification extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param DealDisputed $event
     *
     * @return void
     */
    public function handle(DealDisputed $event)
    {
        Notification::route('mail', config('app.deal.dispute_admin_email'))
            ->notify(new DealDisputedToAdminNotification($event->deal, $event->user));
    }
}
