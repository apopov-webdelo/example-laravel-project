<?php

namespace App\Listeners\Deal\Statistics;

use App\Contracts\Services\Statistics\Deals\FinishDealTimeStatisticsContract;
use App\Events\Deal\DealFinished;
use App\Models\User\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

/**
 * Observer for updating user average finish deal time
 *
 * @package App\Listeners\Deal\Statistics
 */
class UpdateAverageFinishDealTimeListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var FinishDealTimeStatisticsContract
     */
    private $finishDealTimeStatisticsService;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var DealFinished
     */
    private $event;

    /**
     * UpdateAverageFinishDealTimeListener constructor.
     *
     * @param FinishDealTimeStatisticsContract $finishDealTimeStatisticsService
     * @param Log                              $log
     */
    public function __construct(FinishDealTimeStatisticsContract $finishDealTimeStatisticsService, Log $log)
    {
        $this->finishDealTimeStatisticsService = $finishDealTimeStatisticsService;
        $this->log = $log;
    }

    /**
     * @param DealFinished $event
     */
    public function handle(DealFinished $event)
    {
        $this->event = $event;
        $this->updateSellerStatistics()->log();
    }

    /**
     * Calculate statistics and update for seller
     *
     * @return $this
     */
    private function updateSellerStatistics()
    {
        $finishTime = $this->finishDealTimeStatisticsService->timeInSeconds($this->event->deal);
        $this->getSeller()->statistic->incrementAverageFinishDealTime($finishTime);
        return $this;
    }

    /**
     * Retrieve deal's seller
     *
     * @return User
     */
    private function getSeller(): User
    {
        return $this->event->deal->getSeller();
    }

    /**
     * Log successfully action
     *
     * @return $this
     */
    private function log()
    {
        $this->log::info('Was updated average finish deal time statistics for user ID='.$this->getSeller()->getId());
        return $this;
    }
}
