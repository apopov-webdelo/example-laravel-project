<?php

namespace App\Listeners\Deal\Statistics;

use App\Events\Deal\DealEvent;
use App\Events\Deal\DealFinished;
use App\Listeners\BaseListener;
use App\Models\Deal\Deal;
use App\Services\Balance\TurnoverService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Sync job - update user's balance turnover
 *
 * Class UpdateBalanceTurnoverListener
 *
 * @package App\Listeners\Deal\Statistics
 */
class UpdateBalanceTurnoverListener extends BaseListener implements ShouldQueue
{
    /**
     * @var DealFinished
     */
    protected $event;

    /**
     * @var TurnoverService
     */
    protected $turnoverService;

    /**
     * UpdateStatistics constructor.
     *
     * @param TurnoverService $turnoverService
     */
    public function __construct(TurnoverService $turnoverService)
    {
        $this->turnoverService = $turnoverService;
    }

    /**
     * @param DealEvent $event
     *
     * @throws \Exception
     */
    public function handle(DealEvent $event)
    {
        $this->event = $event;
        $this->turnoverService->increaseByDeal($this->getDeal());

        Log::info(
            'User statistic [' . __CLASS__
            . '] was successfully updated by finished deal (ID = '
            . $this->getDeal()->id,
            ['deal_id' => $this->getDeal()->id]
        );
    }

    /**
     * @return Deal
     */
    protected function getDeal(): Deal
    {
        return $this->event->deal;
    }
}
