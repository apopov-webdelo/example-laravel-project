<?php

namespace App\Listeners\Deal\Statistics;

use App\Contracts\Services\User\Statistics\UserStatisticsContract;
use App\Events\Deal\DealEvent;
use App\Events\Deal\DealFinished;
use App\Listeners\BaseListener;
use App\Models\Deal\Deal;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Sync job - update user's statistics counters
 *
 * Class UpdateUserDealCountersListener
 *
 * @package App\Listeners\Deal\Statistics
 */
class UpdateUserDealCountersListener extends BaseListener implements ShouldQueue
{
    /**
     * @var DealFinished
     */
    protected $event;

    /**
     * @var UserStatisticsContract
     */
    protected $statisticsService;

    /**
     * UpdateStatistics constructor.
     *
     * @param UserStatisticsContract $statisticsService
     */
    public function __construct(UserStatisticsContract $statisticsService)
    {
        $this->statisticsService = $statisticsService;
    }

    /**
     * @param DealEvent $event
     *
     * @throws \Exception
     */
    public function handle(DealEvent $event)
    {
        $this->event = $event;

        $this->statisticsService->setUser($this->getDeal()->getSeller())
                                ->update();

        $this->statisticsService->setUser($this->getDeal()->getBuyer())
                                ->update();

        Log::info(
            'User statistic [' . __CLASS__
            . '] was successfully updated by deal (ID = '
            . $this->getDeal()->id,
            ['deal_id' => $this->getDeal()->id]
        );
    }

    /**
     * @return Deal
     */
    protected function getDeal(): Deal
    {
        return $this->event->deal;
    }
}
