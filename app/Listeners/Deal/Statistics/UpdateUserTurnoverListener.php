<?php

namespace App\Listeners\Deal\Statistics;

use App\Events\Deal\DealEvent;
use App\Events\Deal\DealFinished;
use App\Facades\Exchange;
use App\Listeners\BaseListener;
use App\Models\Deal\Deal;
use App\Models\Directory\FiatConstants;
use App\Models\User\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Async job - increment statistics turnover by finished deal
 *
 * Class UpdateUserTurnoverListener
 *
 * @package App\Listeners\Deal\Statistics
 */
class UpdateUserTurnoverListener extends BaseListener implements ShouldQueue
{
    /**
     * @var DealFinished
     */
    protected $event;

    /**
     * @param DealEvent $event
     *
     * @throws \Exception
     */
    public function handle(DealEvent $event)
    {
        $this->event = $event;

        $this->changeUserStatistic($this->getDeal()->getSeller())
             ->changeUserStatistic($this->getDeal()->getBuyer());

        Log::info(
            'User statistic [' . __CLASS__
            . '] was successfully updated by finished deal (ID = '
            . $this->getDeal()->id,
            ['deal_id' => $this->getDeal()->id]
        );
    }

    /**
     * @return Deal
     */
    protected function getDeal(): Deal
    {
        return $this->event->deal;
    }

    /**
     * @param User $user
     *
     * @return UpdateUserTurnoverListener
     */
    protected function changeUserStatistic(User $user): self
    {
        $statistic = $user->statistic;
        $statistic->incrementTotalTurnover($this->getDealAmountInUSD());

        return $this;
    }

    /**
     * Convert deal fiat amount according current exchange rate
     *
     * @return float
     */
    protected function getDealAmountInUSD(): float
    {
        $currencyCode = $this->getDeal()->ad->currency->code;

        return $currencyCode == FiatConstants::USD
            ? $this->getDeal()->fiat_amount
            : Exchange::convert(
                currencyFromCoins($this->getDeal()->fiat_amount, $this->getDeal()->ad->currency),
                $currencyCode,
                FiatConstants::USD
            );
    }
}
