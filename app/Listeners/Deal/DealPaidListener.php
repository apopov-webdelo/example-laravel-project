<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealPaid;
use App\Listeners\BaseListener;
use App\Notifications\Deal\DealPaidToSellerNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class DealPaidListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  DealPaid  $event
     * @return void
     */
    public function handle(DealPaid $event)
    {
        $recipient = $event->deal->ad->is_sale
            ? $event->deal->ad->author
            : $event->deal->author;

        Notification::send($recipient, new DealPaidToSellerNotification($event->deal));
    }
}
