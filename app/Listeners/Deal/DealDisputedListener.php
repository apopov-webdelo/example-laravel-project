<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealDisputed;
use App\Listeners\BaseListener;
use App\Notifications\Deal\DealDisputedToParticipantNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class DealDisputedListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param DealDisputed $event
     * @return void
     */
    public function handle(DealDisputed $event)
    {
        $recipient = $event->deal->author->id === $event->user->id
            ? $event->deal->ad->author
            : $event->deal->author;

        Notification::send($recipient, new DealDisputedToParticipantNotification($event->deal, $event->user));
    }
}
