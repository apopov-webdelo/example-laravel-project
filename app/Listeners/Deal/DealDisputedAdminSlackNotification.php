<?php

namespace App\Listeners\Deal;

use App\Events\Deal\DealDisputed;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;

class DealDisputedAdminSlackNotification extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param DealDisputed $event
     *
     * @return void
     */
    public function handle(DealDisputed $event)
    {
        toSlack('Has been opened a dispute by user '. $event->user->login, [
            'deal' => $event->deal,
            'user' => $event->user,
        ]);
    }
}
