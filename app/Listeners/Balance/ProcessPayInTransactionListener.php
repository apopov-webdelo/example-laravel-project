<?php

namespace App\Listeners\Balance;

use App\Contracts\Balance\PayIn\PayInProcessServiceContract;
use App\Events\Balance\PayIn\PayInTransactionStored;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessPayInTransactionListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var PayInProcessServiceContract
     */
    private $payInProcessService;

    /**
     * ProcessPayInTransactionListener constructor.
     *
     * @param PayInProcessServiceContract $payInProcessService
     */
    public function __construct(PayInProcessServiceContract $payInProcessService)
    {
        $this->payInProcessService = $payInProcessService;
    }

    /**
     * @param PayInTransactionStored $event
     */
    public function handle(PayInTransactionStored $event)
    {
        $this->payInProcessService->process($event->payInTransaction);
    }
}
