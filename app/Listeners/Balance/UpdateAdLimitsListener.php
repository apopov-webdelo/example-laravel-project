<?php

namespace App\Listeners\Balance;

use App\Events\Balance\BalanceEvent;
use App\Listeners\BaseListener;
use App\Models\Ad\Ad;
use App\Models\Balance\Balance;
use App\Models\Directory\CommissionConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use App\Rules\Ad\IsBalanceMoreThanLimit;
use App\Services\Ad\DeactivateAdService;
use App\Services\Ad\UpdateAdLimitsService;
use Illuminate\Container\Container;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class UpdateAdLimitsListener extends BaseListener implements ShouldQueue
{
    /**
     * @var Balance
     */
    protected $balance;
    /**
     * @var AdRepo
     */
    protected $adRepo;
    /**
     * @var User
     */
    protected $author;
    /**
     * @var Container
     */
    protected $app;

    /**
     * UpdateAdLimitsListener constructor.
     *
     * @param AdRepo    $adRepo
     * @param Container $app
     */
    public function __construct(AdRepo $adRepo, Container $app)
    {
        $this->adRepo = $adRepo;
        $this->app = $app;
    }

    /**
     * Handle the event.
     *
     * @param  BalanceEvent $event
     *
     * @return void
     * @throws \Exception
     */
    public function handle(BalanceEvent $event)
    {
        $this->balance = $event->balance;
        $this->author = $this->getAuthor();

        if (!$this->author) {
            throw new \Exception('Balance owner not found');
        }

        $this->processAds();
    }

    /**
     * @return null|User
     */
    private function getAuthor()
    {
        return $this->balance->user()->get()->first();
    }

    /**
     * @return null|CryptoCurrency
     */
    private function getCrypto()
    {
        return $this->balance->cryptoCurrency;
    }

    /**
     * @param Ad    $ad
     * @param float $limit
     *
     * @return bool
     */
    private function isBalanceNotEnough(Ad $ad, float $limit)
    {
        return !$this->isBalanceEnough($ad, $limit);
    }

    /**
     * @param Ad    $ad
     * @param float $limit
     *
     * @return bool
     */
    private function isBalanceEnough(Ad $ad, float $limit)
    {
        $rule = app(
            IsBalanceMoreThanLimit::class,
            [
                'limit'      => $limit,
                'price'      => $ad->accuracyPrice,
                'balance'    => $this->author->getBalance($ad->cryptoCurrency)->accuracyAmount,
                'commission' => $ad->accuracyCommissionPercent,
            ]
        );

        return $rule->check();
    }

    /**
     * @param Ad $ad
     *
     * @throws \Exception
     */
    private function lowerLimit(Ad $ad)
    {
        $oldLimit = $ad->accuracyMax;
        $minLimit = $ad->accuracyMin;
        $newLimit = $this->getMaxLimit($ad);

        $resultLimit = max($newLimit, $minLimit);

        if ($resultLimit < $oldLimit) {
            app(UpdateAdLimitsService::class, ['user' => $ad->author])->update($ad, ['max' => $resultLimit]);
        }
    }

    /**
     * @param Ad $ad
     *
     * @throws \Exception
     */
    private function upLimit(Ad $ad)
    {
        $oldLimit = $ad->accuracyMax;
        $maxLimit = $ad->accuracyOriginalMax;
        $newLimit = $this->getMaxLimit($ad);

        $resultLimit = min($newLimit, $maxLimit);

        if ($resultLimit > $oldLimit) {
            app(UpdateAdLimitsService::class, ['user' => $ad->author])->update($ad, ['max' => $resultLimit]);
        }
    }

    /**
     * Max current user's limit in fiat for ad
     *
     * @param Ad $ad
     *
     * @return float|int
     */
    private function getMaxLimit(Ad $ad)
    {
        $commission = $ad->accuracyCommissionPercent ?? $this->balance->accuracyCommission;
        $limit = ($this->balance->accuracyAmount
                * (1 - $commission / CommissionConstants::ONE_PERCENT))
            * $ad->accuracyPrice;

        return $limit;
    }

    /**
     * @param Ad $ad
     */
    private function deactivateAd(Ad $ad)
    {
        if ($ad->is_active) {
            app(DeactivateAdService::class, ['user' => $ad->author])->deactivate($ad);
        }
    }

    /**
     * Do work with ad limits
     */
    private function processAds()
    {
        $userAds = $this->adRepo->filterByAuthor($this->author)
                                ->filterByCryptoCurrency($this->getCrypto())
                                ->filterForSale();

        $userAds->take()->chunk(10, function (Collection $ads) {
            $ads->each(function ($ad) {
                /* @var Ad $ad */

                // balance lower than current min
                if ($this->isBalanceNotEnough($ad, $ad->accuracyMin)) {
                    $this->deactivateAd($ad);
                }

                // balance is lower than current max
                if ($this->isBalanceNotEnough($ad, $ad->accuracyMax)) {
                    $this->lowerLimit($ad);

                    return;
                }

                // balance is higher than current max and has not liquidity_required
                if ($this->isBalanceEnough($ad, $ad->accuracyMax) && $ad->liquidity_required === false) {
                    $this->upLimit($ad);

                    return;
                }
            });
        });
    }
}
