<?php

namespace App\Listeners\Balance\PayOut;

use App\Events\Balance\PayOut\PayOutFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BroadcastFailedPayOutToSlackListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param PayOutFailed $event
     */
    public function handle(PayOutFailed $event)
    {
        //TODO: оформить оповещение в слек-канал
    }
}
