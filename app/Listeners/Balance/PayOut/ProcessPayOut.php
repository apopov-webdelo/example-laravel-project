<?php

namespace App\Listeners\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutProcessServiceContract;
use App\Events\Blockchain\From\Transaction\BlockchainWithdraw;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

/**
 * Observer process payout request on backend after blockchain gate request
 *
 * @package App\Listeners\Balance\PayOut
 */
class ProcessPayOut extends BaseListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var PayOutProcessServiceContract
     */
    private $service;

    /**
     * ProcessPayOut constructor.
     *
     * @param PayOutProcessServiceContract $service
     */
    public function __construct(PayOutProcessServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * @param BlockchainWithdraw $event
     *
     * @throws \Exception
     */
    public function handle(BlockchainWithdraw $event)
    {
        try {
            $this->service->process($event->getPayOut(), $event->getTransactionId());
        } catch (\Exception $exception) {
            $this->failed($event, $exception);
            throw $exception;
        }
    }
}
