<?php

namespace App\Listeners\Balance\PayOut;

use App\Contracts\Services\Blockchain\To\Transaction\WithdrawContract;
use App\Events\Balance\PayOut\PayOutRegistered;
use App\Listeners\BaseListener;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class RegisterPayOutInBlockckain extends BaseListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var WithdrawContract
     */
    private $service;

    /**
     * @var Log
     */
    private $logger;

    /**
     * RegisterPayOutInBlockckain constructor.
     *
     * @param WithdrawContract $service
     * @param Log              $logger
     */
    public function __construct(WithdrawContract $service, Log $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * Send PayOut request to blockchain using service
     *
     * @param PayOutRegistered $event
     *
     * @throws \Exception
     */
    public function handle(PayOutRegistered $event)
    {
        try {
            $this->service->withdraw($event->payOut);
        } catch (\Exception $exception) {
            $this->failed($event, $exception);

            //TODO: use Jira and Slack services to notify about error!

            throw $exception;
        }
    }
}
