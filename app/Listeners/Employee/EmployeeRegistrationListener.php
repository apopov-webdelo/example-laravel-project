<?php

namespace App\Listeners\Employee;

use App\Events\Employee\EmployeeRegistered;
use App\Listeners\BaseListener;
use App\Notifications\Employee\EmployeeRegistrationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

/**
 * Class EmployeeRegistrationListener
 * @package App\Listeners\User
 */
class EmployeeRegistrationListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  EmployeeRegistered  $event
     * @return void
     */
    public function handle(EmployeeRegistered $event)
    {
        Notification::send(
            $event->admin,
            app(EmployeeRegistrationNotification::class, ['password' => $event->password])
        );
    }
}
