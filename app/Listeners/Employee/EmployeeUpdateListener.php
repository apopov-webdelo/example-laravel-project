<?php

namespace App\Listeners\Employee;

use App\Listeners\BaseListener;
use App\Notifications\Employee\EmployeeUpdateNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

/**
 * Class EmployeeUpdateListener
 * @package App\Listeners\User
 */
class EmployeeUpdateListener extends BaseListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send($event->admin, app(EmployeeUpdateNotification::class));
    }
}
