<?php

namespace App\Listeners\Partnership;

use App\Events\Partnership\PartnerRegistered;
use App\Notifications\Partnership\PartnerRegistrationNotification;
use Illuminate\Support\Facades\Notification;

/**
 * Class PartnerRegisteredNotification
 *
 * @package App\Listeners\Partnership
 */
class PartnerRegisteredNotification
{
    /**
     * Handle the event.
     *
     * @param  PartnerRegistered  $event
     * @return void
     */
    public function handle(PartnerRegistered $event)
    {
        Notification::send(
            $event->partner->getUser(),
            app(PartnerRegistrationNotification::class, ['partner' => $event->partner])
        );
    }
}
