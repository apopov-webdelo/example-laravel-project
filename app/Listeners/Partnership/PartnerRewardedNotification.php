<?php

namespace App\Listeners\Partnership;

use App\Events\Partnership\PartnerRewarded;
use App\Notifications\Partnership\PartnerRewardNotification;
use Illuminate\Support\Facades\Notification;

/**
 * Class PartnerRewardedNotification
 *
 * @package App\Listeners\Partnership
 */
class PartnerRewardedNotification
{
    /**
     * Handle the event.
     *
     * @param  PartnerRewarded  $event
     * @return void
     */
    public function handle(PartnerRewarded $event)
    {
        Notification::send(
            $event->commissionStorage->partner()->getUser(),
            app(PartnerRewardNotification::class, ['commissionStorage' => $event->commissionStorage])
        );
    }
}
