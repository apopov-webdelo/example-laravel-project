<?php

namespace App\Listeners\Partnership;

use App\Events\Partnership\ReferralRegistered;
use App\Notifications\Partnership\ReferralRegistrationNotification;
use Illuminate\Support\Facades\Notification;

/**
 * Class ReferralRegisteredNotification
 *
 * @package App\Listeners\Partnership
 */
class ReferralRegisteredNotification
{
    /**
     * Handle the event.
     *
     * @param  ReferralRegistered $event
     * @return void
     */
    public function handle(ReferralRegistered $event)
    {
        Notification::send(
            $event->referral->getPartner()->getUser(),
            app(ReferralRegistrationNotification::class, ['referral' => $event->referral])
        );
    }
}
