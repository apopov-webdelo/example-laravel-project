<?php

namespace App\Listeners\Partnership;

use App\Contracts\Partnership\PartnershipProcessingContract;
use App\Events\Deal\DealFinished;
use App\Services\Partnership\Entities\DealEntity;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Listener deal must be processed
 *
 * Observer listen finished deals event and process it use PartnershipProcessingContract service
 *
 * @package App\Listeners\Partnership
 */
class PartnerProgramProcessing implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param  DealFinished  $event
     * @return void
     */
    public function handle(DealFinished $event)
    {
        /** @var PartnershipProcessingContract $service */
        $service = app(PartnershipProcessingContract::class);
        $service->process(
            app(DealEntity::class, ['deal' => $event->deal])
        );
    }
}
