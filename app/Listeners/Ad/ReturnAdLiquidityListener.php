<?php

namespace App\Listeners\Ad;

use App\Events\Deal\DealCanceled;
use App\Listeners\BaseListener;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Services\Ad\ActivateAdService;
use App\Services\Ad\UpdateAdLimitsService;
use Illuminate\Container\Container;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Return liquidity to Ad if deal was canceled
 *
 * Class ReturnAdLiquidityListener
 *
 * @package App\Listeners\Ad
 */
class ReturnAdLiquidityListener extends BaseListener implements ShouldQueue
{
    /**
     * @var Container
     */
    protected $app;

    /**
     * UpdateAdLimitsListener constructor.
     *
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * Handle the event.
     *
     * @param  DealCanceled $event
     *
     * @return void
     * @throws \Exception
     */
    public function handle(DealCanceled $event)
    {
        /**
         * @var Deal
         */
        $deal = $event->deal;
        /**
         * @var Ad
         */
        $ad = $deal->ad;
        if ($ad->liquidity_required && $deal->isCanceled()) {
            $ad->max += $deal->fiat_amount;
            $ad->max = min($ad->max, $ad->original_max);

            app(UpdateAdLimitsService::class, ['user' => $ad->author])->update($ad, ['max' => $ad->accuracyMax]);

            if ($ad->isLiquid() && $ad->is_active === false) {
                app(ActivateAdService::class, ['user' => $ad->author])->activate($ad);
            }
        }
    }
}
