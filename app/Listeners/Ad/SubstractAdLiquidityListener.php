<?php

namespace App\Listeners\Ad;

use App\Events\Deal\DealPlaced;
use App\Listeners\BaseListener;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Services\Ad\DeactivateAdService;
use App\Services\Ad\UpdateAdLimitsService;
use Illuminate\Container\Container;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubstractAdLiquidityListener extends BaseListener implements ShouldQueue
{
    /**
     * @var Container
     */
    protected $app;

    /**
     * UpdateAdLimitsListener constructor.
     *
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * Handle the event.
     *
     * @param  DealPlaced $event
     *
     * @return void
     * @throws \Exception
     */
    public function handle(DealPlaced $event)
    {
        /**
         * @var Deal
         */
        $deal = $event->deal;
        /**
         * @var Ad
         */
        $ad = $deal->ad;

        if ($ad->liquidity_required) {
            $ad->max -= $deal->fiat_amount;
            app(UpdateAdLimitsService::class, ['user' => $ad->author])->update($ad, ['max' => $ad->accuracyMax]);

            if ($ad->isNotLiquid() && $ad->is_active) {
                app(DeactivateAdService::class, ['user' => $ad->author])->deactivate($ad);
            }
        }
    }
}
