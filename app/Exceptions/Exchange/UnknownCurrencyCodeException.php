<?php

namespace App\Exceptions\Exchange;

class UnknownCurrencyCodeException extends ExchangeException
{
    //
}
