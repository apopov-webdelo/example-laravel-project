<?php

namespace App\Exceptions\Deal;

use Exception;

class DealException extends Exception
{
    /**
     * @var int Default error code
     */
    protected $code = 422;
}
