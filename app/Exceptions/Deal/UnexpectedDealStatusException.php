<?php

namespace App\Exceptions\Deal;

class UnexpectedDealStatusException extends DealException
{
    /**
     * @var int Default error code
     */
    protected $code = 422;
}
