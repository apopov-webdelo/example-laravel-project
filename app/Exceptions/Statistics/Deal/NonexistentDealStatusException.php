<?php

namespace App\Exceptions\Statistics\Deal;

use Exception;

class NonexistentDealStatusException extends Exception
{
    //
}
