<?php

namespace App\Exceptions\Blockchain\Stellar;

class BadRequestStellarException extends StellarException
{
    //
}
