<?php

namespace App\Exceptions\Blockchain\Stellar;

class UnexpectedResponseCodeStellarException extends ResponseStellarException
{
    //
}
