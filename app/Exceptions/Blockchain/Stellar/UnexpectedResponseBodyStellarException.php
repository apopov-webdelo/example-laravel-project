<?php

namespace App\Exceptions\Blockchain\Stellar;

class UnexpectedResponseBodyStellarException extends ResponseStellarException
{
    //
}
