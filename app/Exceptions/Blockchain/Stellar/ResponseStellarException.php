<?php

namespace App\Exceptions\Blockchain\Stellar;

use Psr\Http\Message\ResponseInterface;

class ResponseStellarException extends StellarException
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Set Response object
     *
     * @param ResponseInterface $response
     */
    public function setResponse(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * Return response object
     *
     * @return ResponseInterface|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}
