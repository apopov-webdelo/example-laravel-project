<?php

namespace App\Exceptions\Blockchain\Stellar;

/**
 * Class InvalidKeyStellarException
 *
 * That exception will thrown when user public key can't be validated
 *
 * @package App\Exceptions\Blockchain\Stellar
 */
class InvalidKeyStellarException extends StellarException
{
    //
}
