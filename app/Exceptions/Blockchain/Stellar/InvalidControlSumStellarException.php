<?php

namespace App\Exceptions\Blockchain\Stellar;

use Illuminate\Http\Response;

/**
 * Class InvalidControlSumStellarException
 *
 * That exception will thrown when control sum hash is invalid accourding request data
 *
 * @package App\Exceptions\Blockchain\Stellar
 */
class InvalidControlSumStellarException extends StellarException
{
    protected $code = Response::HTTP_UNPROCESSABLE_ENTITY;
}
