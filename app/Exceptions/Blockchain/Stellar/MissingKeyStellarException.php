<?php

namespace App\Exceptions\Blockchain\Stellar;

/**
 * Class MissingKeyStellarException
 *
 * That exception will thrown when deal is protected and missing atleast one key
 *
 * @package App\Exceptions\Blockchain\Stellar
 */
class MissingKeyStellarException extends StellarException
{
    //
}
