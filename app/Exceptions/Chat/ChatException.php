<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/8/18
 * Time: 14:45
 */

namespace App\Exceptions\Chat;


class ChatException extends \Exception
{
    /**
     * @var int Default error code
     */
    protected $code = 422;
}