<?php

namespace App\Exceptions\Balance\PayOut;

class InsufficientBalanceAmountForPayOutException extends PayOutException
{
    //
}
