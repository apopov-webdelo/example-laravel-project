<?php

namespace App\Exceptions\Balance\PayOut;

class TooSmallAmountForPayOutException extends PayOutException
{
    //
}
