<?php

namespace App\Exceptions\Balance\PayOut;

class NotUniqueTransactionIdPayOutException extends PayOutException
{
    //
}
