<?php

namespace App\Exceptions\Balance\PayOut;

/**
 * Class UnexpectedStatusPayOutException
 *
 * @package App\Exceptions\Balance\PayOut
 */
class UnexpectedStatusPayOutException extends PayOutException
{

}
