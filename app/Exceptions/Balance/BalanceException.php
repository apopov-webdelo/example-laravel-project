<?php

namespace App\Exceptions\Balance;

use Exception;

class BalanceException extends Exception
{
    /**
     * @var int Default error code
     */
    protected $code = 422;
}
