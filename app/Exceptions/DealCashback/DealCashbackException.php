<?php

namespace App\Exceptions\DealCashback;

use Exception;
use Illuminate\Http\Response;

class DealCashbackException extends Exception
{
    protected $code = Response::HTTP_UNPROCESSABLE_ENTITY;
}
