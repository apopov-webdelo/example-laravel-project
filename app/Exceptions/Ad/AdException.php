<?php

namespace App\Exceptions\Ad;

use Exception;
use Throwable;

/**
 * Class AdException
 * @package App\Exceptions\Ad
 */
class AdException extends Exception
{
    /**
     * @var int Default error code
     */
    protected $code = 422;
}
