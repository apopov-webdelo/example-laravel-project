<?php

namespace App\Exceptions\Ad;

/**
 * Class UnexpectedAdStatusException
 * @package App\Exceptions\Ad
 */
class UnexpectedAdStatusException extends AdException
{
    /**
     * @var int Default error code
     */
    protected $code = 422;
}
