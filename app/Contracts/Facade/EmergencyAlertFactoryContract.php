<?php

namespace App\Contracts\Facade;

interface EmergencyAlertFactoryContract
{
    /**
     * Set current channel
     *
     * @param mixed $names
     *
     * @return EmergencyAlertFactoryContract
     */
    public function channel($names): self;

    /**
     * Send message to selected channels
     *
     * @param string $message
     * @param array  $data
     *
     * @return EmergencyAlertFactoryContract
     */
    public function send(string $message, array $data = []): self;
}
