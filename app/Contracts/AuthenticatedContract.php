<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/5/18
 * Time: 12:50
 */

namespace App\Contracts;

use App\Models\User\User;

/**
 * Interface AuthenticatedContract
 * @package App\Contracts
 * @mixin User
 */
interface AuthenticatedContract
{
    /**
     * Create new token for user
     *
     * @return string
     * @throws \Exception
     */
    public function generateApiToken(): string;

    /**
     * invalidate Api Token
     */
    public function invalidateApiToken();

    /**
     * invalidate Api Token
     */
    public function invalidateAllTokens();

    /**
     * Retrieve id for object
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Retrieve login
     *
     * @return string
     */
    public function getLogin(): string;

    /**
     * Retrieve email
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Set password
     *
     * @param string $password
     *
     * @return AuthenticatedContract
     */
    public function setPassword(string $password): AuthenticatedContract;
}