<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/25/19
 * Time: 13:18
 */

namespace App\Contracts\Repositories;

/**
 * Interface PeriodRepoContract
 *
 * @package App\Contracts\Repositories
 */
interface PeriodRepoContract
{
    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodAny(): PeriodRepoContract;

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodToday(): PeriodRepoContract;

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodYesterday(): PeriodRepoContract;

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodThisWeek(): PeriodRepoContract;

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodLastWeek(): PeriodRepoContract;

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodThisMonth(): PeriodRepoContract;

    /**
     * @return PeriodRepoContract
     */
    public function filterPeriodLastMonth(): PeriodRepoContract;
}