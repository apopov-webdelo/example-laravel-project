<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/10/18
 * Time: 15:15
 */

namespace App\Contracts\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface RepoContract
{
    /**
     * Get configured model builder
     *
     * @return Builder
     */
    public function take(): Builder;

    /**
     * Reset all filters
     *
     * @return RepoContract
     */
    public function reset(): RepoContract;

    /**
     * Retrieve instant model by Id
     *
     * @param $id
     *
     * @return Model
     */
    public function findById($id);
}
