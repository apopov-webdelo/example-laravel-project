<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/13/19
 * Time: 17:21
 */

namespace App\Contracts\Repositories;

/**
 * Interface StellarLogRepoContract
 *
 * @package App\Contracts\Repositories
 */
interface StellarLogRepoContract extends RepoContract
{
    /**
     * Filter list by type
     *
     * @param string $type
     *
     * @return StellarLogRepoContract
     */
    public function filterByType(string $type): StellarLogRepoContract;

    /**
     * Filter list by method
     *
     * @param string $method
     *
     * @return StellarLogRepoContract
     */
    public function filterByMethod(string $method): StellarLogRepoContract;

    /**
     * Search by expression in the route
     *
     * @param string $expression
     *
     * @return StellarLogRepoContract
     */
    public function searchByRoute(string $expression): StellarLogRepoContract;

    /**
     * Filter list by code
     *
     * @param int $code
     *
     * @return StellarLogRepoContract
     */
    public function filterByCode(int $code): StellarLogRepoContract;
}
