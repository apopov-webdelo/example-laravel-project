<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-21
 * Time: 15:05
 */

namespace App\Contracts\Repositories;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

/**
 * Interface PayOutRepoContract
 *
 * @package App\Contracts\Repositories
 */
interface PayOutRepoContract extends RepoContract
{
    /**
     * Check is entity exists by transaction code
     *
     * @param int $id
     *
     * @return bool
     */
    public function isExists(int $id): bool;

    /**
     * Filter by transaction code
     *
     * @param string $transactionId
     *
     * @return PayOutRepoContract
     */
    public function filterByTransactionId(string $transactionId);

    /**
     * Filter by User
     *
     * @param User|int $user User object or user ID
     *
     * @return PayOutRepoContract
     */
    public function filterByUser($user);

    /**
     * Filter by status
     *
     * @param string $status
     *
     * @return PayOutRepoContract
     */
    public function filterByStatus(string $status);

    /**
     * Save model into DB
     *
     * @param User                   $user
     * @param int                    $amount
     * @param int                    $commission
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId
     *
     * @return PayOutContract
     */
    public function store(
        User $user,
        int $amount,
        int $commission,
        CryptoCurrencyContract $cryptoCurrency,
        string $walletId
    ): PayOutContract;
}
