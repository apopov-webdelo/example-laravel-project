<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 12:47
 */

namespace App\Contracts\Repositories;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

/**
 * Interface PayInTransactionRepoContract for PayInTransaction model
 *
 * @package App\Contracts\Repositories
 */
interface PayInTransactionRepoContract extends RepoContract
{
    /**
     * @param User $user
     *
     * @return PayInTransactionRepoContract
     */
    public function filterByUser(User $user): PayInTransactionRepoContract;

    /**
     * @param string $code
     *
     * @return PayInTransactionRepoContract
     */
    public function filterByCryptoCode(string $code): PayInTransactionRepoContract;

    /**
     * @param string $status
     *
     * @return PayInTransactionRepoContract
     */
    public function filterByStatus(string $status): PayInTransactionRepoContract;

    /**
     * Check is entity exists by transaction code
     *
     * @param string $transactionId
     *
     * @return bool
     */
    public function isExists(string $transactionId): bool;

    /**
     * Retrieve PayInTransaction by transaction code
     *
     * @param string $transactionId
     *
     * @return PayInTransactionContract|null
     */
    public function getByTransactionId(string $transactionId);

    /**
     * Save model into DB
     *
     * @param string                 $transactionId
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param int                    $amount
     *
     * @return PayInTransactionContract
     */
    public function store(
        string $transactionId,
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        int $amount
    ): PayInTransactionContract;
}
