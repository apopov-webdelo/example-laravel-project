<?php

namespace App\Contracts\Repositories;

use Illuminate\Support\Collection;

interface FullDirectoryRepoContract
{
    /**
     * get all country collection
     *
     * @return Collection
     */
    public function countries(): Collection;

    /**
     * get all crypto collection
     *
     * @return Collection
     */
    public function cryptoCurrencies(): Collection;

    /**
     * get all directory data
     *
     * @return array
     */
    public function all(): array;
}
