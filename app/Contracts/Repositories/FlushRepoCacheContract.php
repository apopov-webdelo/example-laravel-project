<?php

namespace App\Contracts\Repositories;

interface FlushRepoCacheContract
{
    /**
     * flush repo cache
     */
    public function flush();
}
