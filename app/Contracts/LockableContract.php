<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/13/18
 * Time: 16:45
 */

namespace App\Contracts;

use Carbon\Carbon;

/**
 * Interface LockableContract
 *
 * @package App\Contracts
 */
interface LockableContract
{
    /**
     * @return bool
     */
    public function isBlocked() : bool;

    /**
     * @return bool
     */
    public function isUnlocked() : bool;

    /**
     * @param Carbon      $lockedAt
     * @param Carbon|null $lockedTo
     *
     * @return bool
     */
    public function block(Carbon $lockedAt, Carbon $lockedTo = null) : bool;

    /**
     * @return bool
     */
    public function unlock() : bool;
}
