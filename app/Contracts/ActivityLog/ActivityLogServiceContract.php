<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/22/18
 * Time: 13:51
 */

namespace App\Contracts\ActivityLog;

/**
 * Interface ActivityLogServiceContract
 *
 * @package App\Contracts\ActivityLog
 */
interface ActivityLogServiceContract
{
    public function emergency(string $message, string $type);
    public function alert(string $message, string $type);
    public function critical(string $message, string $type);
    public function error(string $message, string $type);
    public function warning(string $message, string $type);
    public function notice(string $message, string $type);
    public function info(string $message, string $type);
    public function debug(string $message, string $type);
}
