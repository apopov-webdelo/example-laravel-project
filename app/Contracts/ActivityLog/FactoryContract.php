<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/31/18
 * Time: 10:08
 */

namespace App\Contracts\ActivityLog;

interface FactoryContract
{
    /**
     * @param string|null $guard
     *
     * @return ActivityLogServiceContract
     */
    public function guard(string $guard = null) : ActivityLogServiceContract;
}
