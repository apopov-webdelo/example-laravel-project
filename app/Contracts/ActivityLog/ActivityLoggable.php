<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/22/18
 * Time: 15:05
 */

namespace App\Contracts\ActivityLog;

interface ActivityLoggable
{
    public function activityLogs();
}
