<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 12:11
 */

namespace App\Contracts;


interface RuleContract
{
    public function check(): bool;
    public function message(): string;
}