<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/13/18
 * Time: 16:45
 */

namespace App\Contracts;

use Carbon\Carbon;

/**
 * Interface TradeLockableContract
 *
 * @package App\Contracts
 */
interface TradeLockableContract
{
    /**
     * @return bool
     */
    public function isTradeBlocked() : bool;

    /**
     * @return bool
     */
    public function isTradeAvailable() : bool;

    /**
     * @param Carbon|null $lockedTo
     *
     * @return bool
     */
    public function blockTrade(Carbon $lockedTo = null) : bool;

    /**
     * @return bool
     */
    public function unlockTrade() : bool;
}
