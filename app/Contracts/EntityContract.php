<?php

namespace App\Contracts;

interface EntityContract
{
    /**
     * Retrieve entity type
     *
     * That value must be similar for all entities in that classname (ex. Deal, Exchange)
     *
     * @return string
     */
    public function entityType(): string;

    /**
     * Retrieve entity ID code
     *
     * @return string
     */
    public function entityId(): string;

    /**
     * Retrieve entity title
     *
     * That value is unique for concrete entity (ex. Deal-Id-117)
     *
     * @return string
     */
    public function entityTitle(): string;

    /**
     * Retrieve entity description
     *
     * That value is unique for concrete entity (ex. Deal-117-10BTC-to-30000USD)
     *
     * @return string
     */
    public function entityDescription(): string;
}
