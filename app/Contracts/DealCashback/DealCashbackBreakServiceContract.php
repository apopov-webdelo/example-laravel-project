<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 16:49
 */

namespace App\Contracts\DealCashback;

use App\Models\Deal\Deal;

/**
 * Interface DealCashbackBreakServiceContract
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackBreakServiceContract
{
    /**
     * Set status break to cashback model
     *
     * @param Deal $deal
     *
     * @return bool
     */
    public function break(Deal $deal): bool;
}
