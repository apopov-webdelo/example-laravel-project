<?php

namespace App\Contracts\DealCashback;

use App\Exceptions\DealCashback\DealCashbackException;

/**
 * Interface DealCashbackDoneServiceContract
 *
 * Service for finishing cashback operation
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackDoneServiceContract
{
    /**
     * Finish cashback transaction
     *
     * @param DealCashbackContract $cashback
     *
     * @param string               $transactionId
     *
     * @throws DealCashbackException
     *
     * @return bool
     */
    public function done(DealCashbackContract $cashback, string $transactionId): bool;
}
