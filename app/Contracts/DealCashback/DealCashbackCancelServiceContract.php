<?php

namespace App\Contracts\DealCashback;

/**
 * Interface DealCashbackCancelServiceContract
 *
 * Cancel cashback service
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackCancelServiceContract
{
    /**
     * Cancel cashback by request from box
     *
     * @param DealCashbackContract $cashback
     *
     * @return bool
     */
    public function cancel(DealCashbackContract $cashback): bool;
}
