<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 16:46
 */

namespace App\Contracts\DealCashback;

use App\Models\Deal\Deal;

/**
 * Interface DealCashbackProcessServiceContract
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackProcessServiceContract
{
    /**
     * Create cashback model or increase exists with deal
     *
     * @param Deal $deal
     *
     * @return bool
     */
    public function process(Deal $deal): bool;
}
