<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 14:45
 */

namespace App\Contracts\DealCashback;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface ProcessDealCashbackContract
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackContract
{
    /**
     * Retrieve cashback recipient
     *
     * @return User
     */
    public function recipient(): User;

    /**
     * Retrieve deals collection
     *
     * @return Collection
     */
    public function deals(): Collection;

    /**
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract;

    /**
     * Retrieve cashback commission
     *
     * @return int
     */
    public function commission(): int;

    /**
     * Encapsulation for condition to pay cashback
     *
     * @return bool
     */
    public function isReadyToPay(): bool;

    /**
     * Set status
     *
     * @param string $status
     *
     * @return mixed
     */
    public function setStatus(string $status);

    /**
     * Set transactionId
     *
     * @param string $transactionId
     *
     * @return mixed
     */
    public function setTransactionId(string $transactionId);

    /**
     * Is current status `pending`
     *
     * @return bool
     */
    public function isPending(): bool;

    /**
     * Is current status `canceled`
     *
     * @return bool
     */
    public function isCanceled(): bool;

    /**
     * Current status
     *
     * @return string
     */
    public function status(): string;
}
