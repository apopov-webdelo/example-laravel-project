<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 14:45
 */

namespace App\Contracts\DealCashback;

use App\Contracts\CommissionStorageContract;
use App\Models\User\User;
use Illuminate\Support\Collection;

/**
 * Interface DealCashbackCommissionStorageContract
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackCommissionStorageContract extends CommissionStorageContract
{
    /**
     * Percent
     *
     * @return mixed
     */
    public function percent();

    /**
     * Cashback receiving user
     *
     * @return User
     */
    public function user(): User;

    /**
     * Retrieve entity that is reason for partner reward
     *
     * @return CashbackEntityContract
     */
    public function entity(): CashbackEntityContract;

    /**
     * All deals of cashback
     *
     * @return Collection
     */
    public function deals(): Collection;
}
