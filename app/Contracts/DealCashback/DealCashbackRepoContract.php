<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/21/19
 * Time: 14:58
 */

namespace App\Contracts\DealCashback;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\Deal\Deal;
use App\Models\User\User;

/**
 * Interface DealCashbackRepoContract
 *
 * @package App\Contracts\DealCashback
 */
interface DealCashbackRepoContract
{
    /**
     * Filter list by cashback recipient
     *
     * @param User $user
     *
     * @return mixed
     */
    public function filterByRecipient(User $user): DealCashbackRepoContract;

    /**
     * Filter list by exact deal
     *
     * @param Deal $deal
     *
     * @return mixed
     */
    public function filterByDeal(Deal $deal): DealCashbackRepoContract;

    /**
     * Filter list by exact crypto currency
     *
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return DealCashbackRepoContract
     */
    public function filterByCryptoCurrency(CryptoCurrencyContract $cryptoCurrency): DealCashbackRepoContract;

    /**
     * Filter list by status
     *
     * @param string $status
     *
     * @return mixed
     */
    public function filterByStatus(string $status): DealCashbackRepoContract;

    /**
     * Check is entity exists by transaction code
     *
     * @param string $transactionId
     *
     * @return bool
     */
    public function isExists(string $transactionId): bool;

    /**
     * Filter list by Stellar's transaction id
     *
     * @param string $transactionId
     *
     * @return mixed
     */
    public function filterByTransactionId(string $transactionId): DealCashbackRepoContract;

    /**
     * Retrieve cashback model found by recipient OR create new one and return
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return DealCashbackContract
     */
    public function getInProgressOrCreate(User $user, CryptoCurrencyContract $cryptoCurrency): DealCashbackContract;
}
