<?php
namespace App\Contracts\Filters;

use App\Http\Requests\Stats\DealStatsRequest;

interface DealRequestFilterContract
{
    /**
     * Set filter where has request of fiters
     *
     * @param DealStatsRequest $request
     *
     * @return $this
     */
    public function request(DealStatsRequest $request);
}
