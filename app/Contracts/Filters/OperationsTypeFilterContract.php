<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 11:54
 */

namespace App\Contracts\Filters;

interface OperationsTypeFilterContract
{
    /**
     * Set filters by operation type (where is BUY type)
     *
     * @return $this
     */
    public function buy();

    /**
     * Set filters by operation type (where is SELL type)
     *
     * @return $this
     */
    public function sale();
}
