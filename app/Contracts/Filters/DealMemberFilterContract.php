<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 13:07
 */

namespace App\Contracts\Filters;

use App\Models\User\User;

interface DealMemberFilterContract
{
    /**
     * Set filter where user was deal member
     *
     * @param User $user
     *
     * @return $this
     */
    public function member(User $user);
}
