<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 11:53
 */

namespace App\Contracts\Filters;

use Carbon\Carbon;

interface PeriodFiltersContract
{
    /**
     * Set filters by start date
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function dateFrom(Carbon $date);

    /**
     * Set filters by end date
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function dateTo(Carbon $date);
}
