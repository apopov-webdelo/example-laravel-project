<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 11:56
 */

namespace App\Contracts\Filters;

use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;

interface PaymentsFiltersContract
{
    /**
     * Set filters by crypto currency
     *
     * @param CryptoCurrency|int $cryptoCurrency
     *
     * @return $this
     */
    public function cryptoCurrency($cryptoCurrency);

    /**
     * Set filters by fiat currency
     *
     * @param Currency|int $currency
     *
     * @return $this
     */
    public function currency($currency);

    /**
     * Set filters by fiat currency
     *
     * @param PaymentSystem|int $paymentSystem
     *
     * @return $this
     */
    public function paymentSystem($paymentSystem);
}
