<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 07:37
 */

namespace App\Contracts\Currency;

/**
 * Interface FiatCurrencyContract
 * @package App\Contracts\Currency
 */
interface FiatCurrencyContract extends CurrencyContract
{

}