<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 07:37
 */

namespace App\Contracts\Currency;

/**
 * Interface CryptoCurrencyContract
 * @package App\Contracts\Currency
 */
interface CryptoCurrencyContract extends CurrencyContract
{
    /**
     * Return accuracy value
     *
     * @return int
     */
    public function getAccuracy(): int;
}
