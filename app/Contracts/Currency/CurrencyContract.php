<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/29/18
 * Time: 07:37
 */

namespace App\Contracts\Currency;

/**
 * Interface CurrencyContract
 * @package App\Contracts\Currency
 */
interface CurrencyContract
{
    /**
     * Return ISO code for currency (3 symbols)
     *
     * @return string
     */
    public function getCode() : string;

    /**
     * Return currency name
     *
     * @return string
     */
    public function getTitle() : string;

    /**
     * Return accuracy value
     *
     * @return int
     */
    public function getAccuracy() : int;
}
