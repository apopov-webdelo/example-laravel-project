<?php

namespace App\Contracts\Blockchain;

/**
 * Interface ModelWalletsRepoContract
 *
 * @package App\Contracts\Blockchain
 */
interface ModelWalletsRepoContract
{
    /**
     * Check if key exists
     *
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return bool
     */
    public function isExists(ModelWalletsContract $model, string $alias): bool;

    /**
     * Get key by alias
     *
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return string|bool
     */
    public function wallet(ModelWalletsContract $model, string $alias);

    /**
     * Delete key by alias
     *
     * @param ModelWalletsContract $model
     * @param string               $alias
     *
     * @return bool
     */
    public function remove(ModelWalletsContract $model, string $alias): bool;

    /**
     * Save or update existing wallet
     *
     * @param ModelWalletsContract $model
     * @param string               $wallet
     *
     * @return bool
     */
    public function save(ModelWalletsContract $model, string $wallet): bool;

    /**
     * @param ModelWalletsContract $model
     *
     * @return ModelWalletsRepoContract
     */
    public function model(ModelWalletsContract $model): ModelWalletsRepoContract;

    /**
     * @param string $alias
     *
     * @return ModelWalletsRepoContract
     */
    public function alias(string $alias): ModelWalletsRepoContract;
}
