<?php

namespace App\Contracts\Blockchain;

/**
 * Interface ModelWalletsContract
 *
 * @package App\Contracts\Blockchain
 */
interface ModelWalletsContract
{
    /**
     * Check if key exists
     *
     * @param string $alias
     *
     * @return bool
     */
    public function isWalletExists(string $alias): bool;

    /**
     * Get key string or false
     *
     * @param string $alias
     *
     * @return string|false
     */
    public function getWallet(string $alias);

    /**
     * Save key for current model or update existing one
     *
     * @param string $key
     * @param string $alias
     *
     * @return bool
     */
    public function saveWallet(string $key, string $alias): bool;

    /**
     * Delete key for urrent model or update existing one
     *
     * @param string $alias
     *
     * @return bool
     */
    public function removeWallet(string $alias): bool;
}
