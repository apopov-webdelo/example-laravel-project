<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 11:47
 */

namespace App\Contracts\Chat;

use App\Models\Chat\Message;
use App\Models\User\User;

/**
 * Interface StoreMessageToDealServiceContract
 *
 * @package App\Contracts\Chat
 */
interface StoreMessageToUserServiceContract
{
    /**
     * Save message to private chat with user and create new chat if not exists
     *
     * @param User   $user
     * @param string $message
     *
     * @return Message
     */
    public function store(User $user, string $message): Message;
}