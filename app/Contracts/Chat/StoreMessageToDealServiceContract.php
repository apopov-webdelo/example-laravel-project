<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 11:47
 */

namespace App\Contracts\Chat;

use App\Models\Chat\Message;
use App\Models\Deal\Deal;

/**
 * Interface StoreMessageToDealServiceContract
 *
 * @package App\Contracts\Chat
 */
interface StoreMessageToDealServiceContract
{
    /**
     * Save message to deal chat and create new chat if not exists
     *
     * @param Deal   $deal
     * @param string $message
     *
     * @return Message
     */
    public function store(Deal $deal, string $message): Message;
}