<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/23/18
 * Time: 15:16
 */

namespace App\Contracts;


use App\Models\User\User;

interface RobotFactoryContract
{
    /**
     * Return instance of RobotUser
     *
     * @return User
     */
    public function user(): User;


}