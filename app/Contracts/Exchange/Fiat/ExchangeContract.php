<?php

namespace App\Contracts\Exchange\Fiat;

use App\Exceptions\Exchange\UnknownCurrencyCodeException;
use Illuminate\Support\Collection;

/**
 * Interface ExchangeContract
 * @package App\Services\Exchange
 */
interface ExchangeContract extends FiatFactoryContract
{
    /**
     * Return all currency list
     *
     * @return Collection
     */
    public function currencies(): Collection;

    /**
     * Return rate for currencies
     *
     * @param string $currencyFrom
     * @param string $currencyTo
     * @param bool   $fromCache
     *
     * @return float
     *
     */
    public function rate(string $currencyFrom, string $currencyTo, bool $fromCache = true): float;

    /**
     * Convert sum according currencies
     *
     * @param float $sum
     * @param string $currencyFrom
     * @param string $currencyTo
     * @return float
     *
     * @throws UnknownCurrencyCodeException
     */
    public function convert(float $sum, string $currencyFrom, string $currencyTo): float;
}
