<?php

namespace App\Contracts\Exchange\Fiat;

use Illuminate\Support\Collection;

interface FiatBatchUpdateContract
{
    /**
     * Batch update rates for from/[all currencies]
     *
     * @param string     $from
     * @param Collection $currencies
     */
    public function batchUpdate(string $from, Collection $currencies): void;
}
