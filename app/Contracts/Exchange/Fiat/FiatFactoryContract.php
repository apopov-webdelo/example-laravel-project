<?php

namespace App\Contracts\Exchange\Fiat;

use App\Exceptions\Exchange\ExchangeException;

interface FiatFactoryContract
{
    /**
     * Return instanced market engine
     *
     * @param string|null $engine
     *
     * @return ExchangeContract
     *
     * @throws ExchangeException
     */
    public function market(string $engine = null): ExchangeContract;
}
