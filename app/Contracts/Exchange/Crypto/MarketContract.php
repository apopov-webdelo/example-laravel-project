<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/28/18
 * Time: 16:10
 */

namespace App\Contracts\Exchange\Crypto;

use App\Exceptions\Exchange\UnknownCurrencyCodeException;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use Illuminate\Support\Collection;

/**
 * Interface MarketContract
 * @package App\Contracts
 */
interface MarketContract
{
    /**
     * Return instanced market driver
     *
     * @param string|null $driver
     * @return MarketContract
     */
    public function market(string $driver = null): MarketContract;

    /**
     * Return supported all currency list
     *
     * @return Collection
     */
    public function currencies(): Collection;

    /**
     * Return all supported crypto currency list
     *
     * @return Collection
     */
    public function cryptoCurrencies(): Collection;

    /**
     * Return rate for currencies
     *
     * @param string|Currency|CryptoCurrency $from
     * @param string|Currency|CryptoCurrency $to
     * @param bool $cache
     *
     * @return float
     *
     * @throws UnknownCurrencyCodeException
     */
    public function rate($from, $to, bool $cache = true): float;

    /**
     * Convert sum according currencies
     *
     * @param float $sum
     * @param string|Currency|CryptoCurrency $from
     * @param string|Currency|CryptoCurrency $to
     * @param int $accuracy
     * @param bool $cache
     *
     * @return float|int
     *
     * @throws UnknownCurrencyCodeException
     */
    public function convert(float $sum, $from, $to, int $accuracy = 0, bool $cache = true);

    /**
     * Refresh rates (upload from API, update cache)
     *
     * @param string|Currency|CryptoCurrency|null $currency
     *
     * @return MarketContract
     */
    public function refresh($currency = null): MarketContract;
}
