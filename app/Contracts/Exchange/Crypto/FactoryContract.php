<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/31/18
 * Time: 10:08
 */

namespace App\Contracts\Exchange\Crypto;

use App\Exceptions\Exchange\ExchangeException;

interface FactoryContract
{
    /**
     * Return instanced market engine
     *
     * @param string|null $engine
     *
     * @return MarketContract
     *
     * @throws ExchangeException
     */
    public function market(string $engine = null): MarketContract;
}
