<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/18/19
 * Time: 11:49
 */

namespace App\Contracts\Services\User\Statistics;

use App\Models\User\Statistic;

/**
 * Interface UserUpdateReviewStorage
 *
 * @package App\Contracts\Services\User
 */
interface UpdateReviewsCoefficientStorageContract
{
    /**
     * @return Statistic
     */
    public function model(): Statistic;

    /**
     * @return int
     */
    public function positiveCount(): int;

    /**
     * @return int
     */
    public function negativeCount(): int;

    /**
     * @return int
     */
    public function neutralCount(): int;

    /**
     * @return int
     */
    public function dealsCount(): int;
}
