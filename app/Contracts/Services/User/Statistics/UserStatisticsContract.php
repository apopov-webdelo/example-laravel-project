<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/10/18
 * Time: 14:28
 */

namespace App\Contracts\Services\User\Statistics;

use App\Models\User\User;

interface UserStatisticsContract
{
    /**
     * Set user for statistics updating
     *
     * Stats will check for deals in which user is member
     *
     * @param User $user
     *
     * @return UserStatisticsContract
     */
    public function setUser(User $user): UserStatisticsContract;

    /**
     * Update all statistics fields
     *
     * @return bool
     */
    public function update(): bool;

    /**
     * Recalculate and update statistics by finished deals
     *
     * @return UserStatisticsContract
     */
    public function updateFinishedDealsCount(): UserStatisticsContract;

    /**
     * Recalculate and update statistics by canceled deals
     *
     * @return UserStatisticsContract
     */
    public function updateCanceledDealsCount(): UserStatisticsContract;

    /**
     * Recalculate and update statistics by disputed deals
     *
     * @return UserStatisticsContract
     */
    public function updateDisputedDealsCount(): UserStatisticsContract;

    /**
     * Recalculate and update statistics by paid deals
     *
     * @return UserStatisticsContract
     */
    public function updatePaidDealsCount(): UserStatisticsContract;

    /**
     * Recalculate and update statistics for total deals
     *
     * @return UserStatisticsContract
     */
    public function updateTotalDealsCount(): UserStatisticsContract;

    /**
     * Recalculate and update statistics cancellation percents
     *
     * @return UserStatisticsContract
     */
    public function updateCancellationPercent(): UserStatisticsContract;

    /**
     * Save statistics model and update flag
     *
     * @return bool
     */
    public function save(): bool;
}
