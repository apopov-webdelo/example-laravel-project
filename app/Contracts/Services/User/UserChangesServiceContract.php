<?php

namespace App\Contracts\Services\User;

interface UserChangesServiceContract
{
    /**
     * @param string $login
     *
     * @return bool
     */
    public function loginChange(string $login): bool;

    /**
     * @param string $email
     *
     * @return bool
     */
    public function emailChange(string $email): bool;
}
