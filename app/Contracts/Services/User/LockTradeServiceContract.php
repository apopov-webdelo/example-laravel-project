<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:35
 */

namespace App\Contracts\Services\User;

use App\Contracts\TradeLockableContract;

/**
 * Interface LockTradeServiceContract
 * @package App\Contracts\Services\User
 */
interface LockTradeServiceContract
{
    /**
     * Lock user trade
     *
     * @param TradeLockableContract $user
     * @param int                   $days
     *
     * @return LockTradeServiceContract
     */
    public function lock(TradeLockableContract $user, int $days) : self;
}
