<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:38
 */

namespace App\Contracts\Services\User;

use App\Contracts\LockableContract;

/**
 * Interface UnlockServiceContract
 * @package App\Contracts\Services\User
 */
interface UnlockServiceContract
{
    /**
     * Unlock user
     *
     * @param LockableContract $user
     *
     * @return UnlockServiceContract
     */
    public function unlock(LockableContract $user) : self;
}
