<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:35
 */

namespace App\Contracts\Services\User;

use App\Contracts\LockableContract;

/**
 * Interface LockServiceContract
 * @package App\Contracts\Services\User
 */
interface LockServiceContract
{
    /**
     * Lock user
     *
     * @param LockableContract $user
     * @param int              $days
     *
     * @return LockServiceContract
     */
    public function lock(LockableContract $user, int $days) : self;
}
