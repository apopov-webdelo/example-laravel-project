<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:38
 */

namespace App\Contracts\Services\User;

/**
 * Interface SyncPermissionsServiceContract
 * @package App\Contracts\Services\User
 */
interface SyncPermissionsServiceContract
{
    public function syncPermissions(array $permission);
}
