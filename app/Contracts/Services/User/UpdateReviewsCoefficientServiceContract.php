<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 1/12/19
 * Time: 08:59
 */

namespace App\Contracts\Services\User;

/**
 * Interface UpdateReviewsCoefficientService
 *
 * @package App\Contracts\Services\User
 */
interface UpdateReviewsCoefficientServiceContract
{
    /**
     * Update reviews coefficient by formula
     *
     * @return bool
     */
    public function update(): bool;
}
