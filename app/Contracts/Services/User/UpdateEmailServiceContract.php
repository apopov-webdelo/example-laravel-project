<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/20/19
 * Time: 20:02
 */

namespace App\Contracts\Services\User;

/**
 * Interface UpdateEmailServiceContract
 *
 * @package App\Contracts\Services\User
 */
interface UpdateEmailServiceContract
{
    /**
     * Update email for user
     *
     * @param string $email
     *
     * @return bool
     */
    public function update(string $email): bool;
}