<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 02.11.2018
 * Time: 14:38
 */

namespace App\Contracts\Services\User;

use App\Contracts\TradeLockableContract;

/**
 * Interface UnlockTradeServiceContract
 * @package App\Contracts\Services\User
 */
interface UnlockTradeServiceContract
{
    /**
     * Unlock user trade
     *
     * @param TradeLockableContract $user
     *
     * @return UnlockTradeServiceContract
     */
    public function unlock(TradeLockableContract $user) : self;
}
