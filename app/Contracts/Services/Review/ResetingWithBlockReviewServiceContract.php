<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/22/19
 * Time: 17:08
 */
namespace App\Contracts\Services\Review;

use App\Models\User\Review;

/**
 * Interface ResetingReviewServiceContract
 *
 * @package App\Contracts\Services\Review
 */
interface ResetingWithBlockReviewServiceContract
{
    /**
     * Reset review fields and save trust negative to block recipient for author
     *
     * @param Review $review
     *
     * @return bool
     */
    public function resetWithBlock(Review $review): bool;
}