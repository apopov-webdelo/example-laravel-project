<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/22/19
 * Time: 17:08
 */
namespace App\Contracts\Services\Review;

use App\Models\User\Review;

/**
 * Interface DeletingReviewServiceContract
 *
 * @package App\Contracts\Services\Review
 */
interface DeletingReviewServiceContract
{
    /**
     * Delete review from DB
     *
     * @param Review $review
     *
     * @return bool
     */
    public function delete(Review $review): bool;
}
