<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/26/18
 * Time: 12:50
 */

namespace App\Contracts\Services\Idea;

/**
 * Interface IdeaContract
 *
 * Should contain necessary data for IdeaNotification
 *
 * @package App\Contracts
 */
interface IdeaStorageContract
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getLogin(): string;

    /**
     * @return string
     */
    public function getText(): string;
}