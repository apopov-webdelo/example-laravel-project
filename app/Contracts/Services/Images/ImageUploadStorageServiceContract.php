<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 15:17
 */

namespace App\Contracts\Images;

use App\Contracts\ImageableContract;
use Illuminate\Http\UploadedFile;

/**
 * Object should operate with an image
 *
 * Interface ImageStorageServiceContract
 *
 * @package App\Contracts\Images
 */
interface ImageUploadStorageServiceContract
{
    /**
     * Set image from UploadedFile
     *
     * @param UploadedFile $file
     *
     * @return bool
     */
    public function setImage(UploadedFile $file): ImageUploadStorageServiceContract;

    /**
     * Get image from UploadedFile
     *
     * @return UploadedFile
     */
    public function getImage(): UploadedFile;

    /**
     * @param ImageContract     $image
     * @param ImageableContract $object
     *
     * @return bool
     */
    public function uploadFor(ImageContract $image, ImageableContract $object): bool;
}
