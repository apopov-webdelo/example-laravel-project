<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 09:03
 */

namespace App\Contracts\Images;

use App\Contracts\ImageableContract;
use Illuminate\Http\UploadedFile;

/**
 * Interface ImageUploadServiceContract
 *
 * @package App\Contracts\Images
 */
interface ImageUploadServiceContract
{
    /**
     * Attach image to object
     *
     * @param ImageableContract                 $object
     * @param ImageUploadStorageServiceContract $storage
     *
     * @return ImageContract
     */
    public function upload(ImageableContract $object, ImageUploadStorageServiceContract $storage): ImageContract;
}
