<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 09:00
 */

namespace App\Contracts;

use App\Contracts\Images\ImageContract;

/**
 * Interface ImageOneAbleContract
 *
 * @package App\Contracts
 */
interface ImageableContract
{
    public function getImage(): ImageContract;
}