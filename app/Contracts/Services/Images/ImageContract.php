<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 09:03
 */

namespace App\Contracts\Images;

use App\Contracts\ImageableContract;

/**
 * Interface ImageContract
 *
 * @package App\Contracts\Images
 */
interface ImageContract
{
    /**
     * @return string
     */
    public function getBasePath(): string;

    /**
     * @return string
     */
    public function getFullPath(): string;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return string
     */
    public function getStoragePath(): string;

    /**
     * @return ImageableContract
     */
    public function getOwner(): ImageableContract;

    /**
     * @return string
     */
    public function getFilename(): string;

    /**
     * @return string
     */
    public function getFilenameById(): string;
}
