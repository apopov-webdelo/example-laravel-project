<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 15:17
 */

namespace App\Contracts\Images;

use App\Contracts\ImageableContract;
use Illuminate\Http\UploadedFile;

/**
 * Interface ImageDeleteStorageServiceContract
 *
 * @package App\Contracts\Images
 */
interface ImageDeleteStorageServiceContract
{
    /**
     * @param ImageContract $image
     *
     * @return bool
     */
    public function delete(ImageContract $image): bool;
}
