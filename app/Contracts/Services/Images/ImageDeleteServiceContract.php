<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/18/18
 * Time: 15:17
 */

namespace App\Contracts\Images;

/**
 * Interface ImageDeleteServiceContract
 *
 * @package App\Contracts\Images
 */
interface ImageDeleteServiceContract
{
    /**
     * Set image from UploadedFile
     *
     * @param ImageContract $image
     *
     * @return bool
     */
    public function delete(ImageContract $image): bool;
}
