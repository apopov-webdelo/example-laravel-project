<?php

namespace App\Contracts\Services\Messaging\Telegram;

interface TelegramSendMessageContract
{
    /**
     * Send message thru telegram api
     *
     * @param array $record
     *
     * @return bool
     */
    public function send(array $record): bool;
}
