<?php

namespace App\Contracts\Services\Auth;

use App\Contracts\AuthenticatedContract;

interface TokenServiceContract
{
    /**
     * Manually set user
     *
     * @param AuthenticatedContract $user
     *
     * @return self
     */
    public function setUser(AuthenticatedContract $user);

    /**
     * Creates new token and revokes all older tokens
     *
     * @return string
     * @throws \Exception
     */
    public function createNew(): string;

    /**
     * Revoke all tokens from same device
     */
    public function revokeForDevice(): void;

    /**
     * Revoke all tokens from all devices
     */
    public function revokeAll(): void;

    /**
     * Set token expire by project rules on visits
     *
     * @throws \Exception
     */
    public function updateExpireOnVisit(): void;
}