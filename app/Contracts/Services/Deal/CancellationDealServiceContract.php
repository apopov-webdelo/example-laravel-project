<?php

namespace App\Contracts\Services\Deal;

use App\Models\Deal\Deal;

interface CancellationDealServiceContract
{
    /**
     * Register cancellation of deal in backend service
     *
     * @param Deal $deal
     *
     * @return Deal
     */
    public function cancellation(Deal $deal): Deal;
}
