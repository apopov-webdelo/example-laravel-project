<?php

namespace App\Contracts\Services\Deal;

use App\Models\Deal\Deal;

interface VerifyDealServiceContract
{
    /**
     * Register verified deal in backend service
     *
     * @param Deal $deal
     *
     * @return Deal
     */
    public function verified(Deal $deal): Deal;
}
