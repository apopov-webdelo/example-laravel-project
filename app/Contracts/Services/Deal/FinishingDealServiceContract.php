<?php

namespace App\Contracts\Services\Deal;

use App\Models\Deal\Deal;

interface FinishingDealServiceContract
{
    /**
     * Register finishing deal in backend service
     *
     * @param Deal $deal
     *
     * @return Deal
     */
    public function finishing(Deal $deal): Deal;
}
