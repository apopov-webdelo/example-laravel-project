<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/28/18
 * Time: 15:41
 */

namespace App\Contracts\Statistics\Ads;

use App\Models\Ad\Ad;
use Carbon\Carbon;

interface AdStatisticsContract
{
    /**
     * @param Ad          $ad
     * @param Carbon|null $from
     * @param Carbon|null $to
     */
    public function __construct(Ad $ad, Carbon $from = null, Carbon $to = null);

    /**
     * Calculate deals quantity placed by that Ad according period
     *
     * @return int
     */
    public function deals(): int;

    /**
     * Calculate total crypto turnover by that Ad according period
     *
     * @return int
     */
    public function cryptoTurnover(): int;

    /**
     * Calculate total fiat turnover by that Ad according period
     * @return int
     */
    public function fiatTurnover(): int;

    /**
     * Calculate weighted average price by that Ad according period
     *
     * @return int
     */
    public function weightedAveragePrice(): int;
}
