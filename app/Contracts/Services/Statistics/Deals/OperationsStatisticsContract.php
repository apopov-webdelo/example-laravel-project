<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 11:22
 */

namespace App\Contracts\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\StatisticsContract;

interface OperationsStatisticsContract extends
    StatisticsContract,
    FiltersDealsStatisticsContract
{
    /**
     * Retrieve count of selling operations
     *
     * @return int
     */
    public function getSale() : int;

    /**
     * Retrieve count of selling percentage
     *
     * @return int
     */
    public function getSalePercentage() : int;

    /**
     * Retrieve count of buying operations
     *
     * @return int
     */
    public function getBuy() : int;

    /**
     * Retrieve count of buying percentage
     *
     * @return int
     */
    public function getBuyPercentage() : int;

    /**
     * Retrieve total deals by filters
     *
     * @return int
     */
    public function getTotal() : int;
}
