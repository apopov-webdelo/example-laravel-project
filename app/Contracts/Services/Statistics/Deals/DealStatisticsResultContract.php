<?php

namespace App\Contracts\Services\Statistics\Deals;

/**
 * Interface DealStatisticsResultContract
 *
 * @package App\Contracts\Services\Statistics\Deals
 */
interface DealStatisticsResultContract
{
    /**
     * Get operations stats
     *
     * @return mixed
     */
    public function getStatsOperations();

    /**
     * Get fiat stats
     *
     * @return mixed
     */
    public function getStatsFiat();

    /**
     * Get crypto stats
     *
     * @return mixed
     */
    public function getStatsCrypto();

    /**
     * Get payment systems stats
     *
     * @return mixed
     */
    public function getStatsPaymentSystems();
}