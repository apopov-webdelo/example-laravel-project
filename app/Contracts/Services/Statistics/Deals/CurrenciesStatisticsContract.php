<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 15:39
 */

namespace App\Contracts\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\StatisticsContract;

interface CurrenciesStatisticsContract extends
    StatisticsContract,
    FiltersDealsStatisticsContract
{
    /**
     * Retrieve statistics for crypto currencies
     *
     * @return array
     */
    public function getCryptoTurnover() : array;

    /**
     * Retrieve statistics for fiat currencies
     *
     * @return array
     */
    public function getFiatTurnover() : array;
}
