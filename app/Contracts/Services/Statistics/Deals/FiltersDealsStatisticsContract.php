<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 13:14
 */

namespace App\Contracts\Services\Statistics\Deals;

use App\Contracts\Filters\DealMemberFilterContract;
use App\Contracts\Filters\OperationsTypeFilterContract;
use App\Contracts\Filters\PaymentsFiltersContract;
use App\Contracts\Filters\PeriodFiltersContract;

interface FiltersDealsStatisticsContract extends
    PeriodFiltersContract,
    OperationsTypeFilterContract,
    PaymentsFiltersContract,
    DealMemberFilterContract
{

}
