<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/6/18
 * Time: 11:41
 */

namespace App\Contracts\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\StatisticsContract;
use App\Models\Directory\PaymentSystem;

interface PaymentSystemsStatisticsContract extends
    StatisticsContract,
    FiltersDealsStatisticsContract
{
    /**
     * Retrieve users turnover for payment system
     *
     * @param PaymentSystem|int $paymentSystem
     *
     * @return int
     */
    public function getPaymentSystemTurnover($paymentSystem): int;

    /**
     * Retrieve percentages in total turnover for all payments systems
     *
     * @return array
     */
    public function getPaymentSystemsPercentages(): array;
}
