<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-28
 * Time: 16:46
 */

namespace App\Contracts\Services\Statistics\Deals;

use App\Models\User\User;

/**
 * Interface AverageFinishDealsTimeStatisticsContract
 *
 * @package App\Contracts\Services\Statistics\Deals
 */
interface AverageFinishDealsTimeStatisticsContract
{
    /**
     * Check average finish deal time for all finished deals for user in seconds
     *
     * @param User $user
     *
     * @return int
     */
    public function averageTimeInSeconds(User $user): int;

    /**
     * Check average finish deal time for all finished deals for user in minutes
     *
     * @param User $user
     *
     * @return int
     */
    public function averageTimeInMinutes(User $user): int;

    /**
     * Check average finish deal time for all finished deals for user in hours
     *
     * @param User $user
     *
     * @return int
     */
    public function averageTimeInHours(User $user): int;
}
