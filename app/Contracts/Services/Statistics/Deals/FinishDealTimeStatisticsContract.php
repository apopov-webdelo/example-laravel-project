<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-28
 * Time: 16:41
 */

namespace App\Contracts\Services\Statistics\Deals;

use App\Models\Deal\Deal;

/**
 * Interface AverageFinishDealStatisticsContract
 *
 * @package App\Contracts\Services\Statistics\Deals
 */
interface FinishDealTimeStatisticsContract
{
    /**
     * Check finish period in seconds for deal
     *
     * @param Deal $deal
     *
     * @return int
     */
    public function timeInSeconds(Deal $deal): int;

    /**
     * Check finish period in minutes for deal
     *
     * @param Deal $deal
     *
     * @return int
     */
    public function timeInMinutes(Deal $deal): int;

    /**
     * Check finish period in hours for deal
     *
     * @param Deal $deal
     *
     * @return int
     */
    public function timeInHours(Deal $deal): int;
}
