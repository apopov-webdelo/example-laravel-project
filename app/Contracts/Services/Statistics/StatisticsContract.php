<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 10/30/18
 * Time: 11:50
 */

namespace App\Contracts\Services\Statistics;

interface StatisticsContract
{
    /**
     * Reset all filters
     *
     * @return StatisticsContract
     */
    public function reset() : StatisticsContract;

    /**
     * Retrieve all statistics in array
     *
     * @return array
     */
    public function getStatistics() : array;
}
