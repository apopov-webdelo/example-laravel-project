<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:27
 */

namespace App\Contracts\Services\Blockchain\From\Wallet;

use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Models\User\User;

interface WalletCreationErrorContract extends ControlSumContract
{
    /**
     * Register error about wallet creation in blockchain
     *
     * @param User   $user
     * @param string $error
     * @param string $reason
     *
     * @return bool
     */
    public function error(User $user, string $error, string $reason): bool;
}
