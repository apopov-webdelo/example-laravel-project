<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 14:20
 */

namespace App\Contracts\Services\Blockchain\From;

interface ControlSumContract
{
    /**
     * Set control sum for that request
     *
     * @param string      $hash
     * @param string|null $controlKey Usual using timestamp like additional key for control sum
     *
     * @return $this
     */
    public function controlSum(string $hash, string $controlKey = null);
}
