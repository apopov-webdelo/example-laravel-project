<?php

namespace App\Contracts\Services\Blockchain\From\User;

use App\Contracts\DealCashback\DealCashbackContract;
use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Exceptions\DealCashback\DealCashbackException;

/**
 * Interface ProcessDealCashbackCancelContract
 *
 * Process cancel cashback request from controller
 *
 * @package App\Contracts\Services\Blockchain\From\User
 */
interface ProcessDealCashbackCancelContract extends ControlSumContract
{
    /**
     * Action on cashback transfer cancel from blockchain
     *
     * @param DealCashbackContract $cashback
     * @throws DealCashbackException
     * @return ProcessDealCashbackCancelContract
     */
    public function cancel(DealCashbackContract $cashback): self;
}
