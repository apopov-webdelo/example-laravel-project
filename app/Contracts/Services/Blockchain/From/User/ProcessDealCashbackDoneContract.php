<?php

namespace App\Contracts\Services\Blockchain\From\User;

use App\Contracts\DealCashback\DealCashbackContract;
use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Exceptions\Blockchain\Stellar\StellarException;
use App\Exceptions\DealCashback\DealCashbackException;

/**
 * Interface ProcessDealCashbackDoneContract
 *
 * Process cashback done request from box
 *
 * @package App\Contracts\Services\Blockchain\From\User
 */
interface ProcessDealCashbackDoneContract extends ControlSumContract
{
    /**
     * Action on cashback transfer cancel from blockchain
     *
     * @param DealCashbackContract $cashback
     *
     * @throws DealCashbackException
     * @throws StellarException
     * @return ProcessDealCashbackDoneContract
     */
    public function done(DealCashbackContract $cashback): self;
}
