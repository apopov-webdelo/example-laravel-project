<?php

namespace App\Contracts\Services\Blockchain\From\EmergencyLog;

use App\Http\Requests\Blockchain\EmergencyLog\EmergencyLogRequest;

interface EmergencyLogHandlerContract
{
    /**
     * Set channel
     *
     * @param string $name
     *
     * @return mixed
     */
    public function channel(string $name);

    /**
     * Send log message fro request
     *
     * @param EmergencyLogRequest $request
     *
     * @return mixed
     */
    public function send(EmergencyLogRequest $request);
}
