<?php

namespace App\Contracts\Services\Blockchain\From\Deal;

use App\Models\Deal\Deal;

interface DealVerifyContract
{
    /**
     * Register verified deal in blockchain gate
     *
     * @param Deal $deal
     *
     * @return bool
     */
    public function verified(Deal $deal): bool;
}
