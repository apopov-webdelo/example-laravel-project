<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:26
 */

namespace App\Contracts\Services\Blockchain\From\Deal;

use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Models\Deal\Deal;

interface DealCreationErrorContract extends ControlSumContract
{
    /**
     * Register cancellation deal in blockchain gate
     *
     * @param Deal   $deal
     * @param string $error
     * @param string $reason
     *
     * @return bool
     */
    public function error(Deal $deal, string $error, string $reason): bool;
}
