<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:25
 */

namespace App\Contracts\Services\Blockchain\From\Deal;

use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Models\Deal\Deal;

interface DealFinishContract extends ControlSumContract
{
    /**
     * Register finish deal in blockchain gate
     *
     * @param Deal $deal
     *
     * @return bool
     */
    public function finish(Deal $deal): bool;
}
