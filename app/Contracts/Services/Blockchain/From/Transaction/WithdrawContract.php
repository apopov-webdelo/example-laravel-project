<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:25
 */

namespace App\Contracts\Services\Blockchain\From\Transaction;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Models\User\User;

interface WithdrawContract extends ControlSumContract
{
    /**
     * Register withdraw transaction in blockchain gate
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $amount
     * @param string                 $balanceAmount
     * @param string                 $transactionId
     * @param int                    $payOutId
     *
     * @return bool
     */
    public function withdraw(
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        string $amount,
        string $balanceAmount,
        string $transactionId,
        int $payOutId
    ): bool;
}
