<?php

namespace App\Contracts\Services\Blockchain\From\Transaction;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Services\Blockchain\From\ControlSumContract;

interface WithdrawErrorContract extends ControlSumContract
{
    /**
     * Register error about withdraw from blockchain
     *
     * @param PayOutContract $payOut
     * @param string         $error
     * @param string         $reason
     *
     * @return bool
     */
    public function error(PayOutContract $payOut, string $error, string $reason): bool;
}
