<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:24
 */

namespace App\Contracts\Services\Blockchain\From\Transaction;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\From\ControlSumContract;
use App\Models\User\User;

interface DepositContract extends ControlSumContract
{
    /**
     * Register deposit transaction in blockchain gate
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $amount
     * @param string                 $balanceAmount
     * @param string                 $transactionId
     *
     * @return bool
     */
    public function deposit(
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        string $amount,
        string $balanceAmount,
        string $transactionId
    ): bool;
}
