<?php

namespace App\Contracts\Services\Blockchain\To\User;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;

interface ProcessDealCashbackContract
{
    public function reward(DealCashbackCommissionStorageContract $commissionStorage): bool;
}
