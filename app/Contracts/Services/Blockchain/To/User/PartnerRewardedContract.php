<?php

namespace App\Contracts\Services\Blockchain\To\User;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;

interface PartnerRewardedContract
{
    /**
     * Send partner reward info to blockchain
     *
     * @param PartnershipCommissionStorageContract $commissionStorage
     *
     * @return bool
     */
    public function reward(PartnershipCommissionStorageContract $commissionStorage): bool;
}
