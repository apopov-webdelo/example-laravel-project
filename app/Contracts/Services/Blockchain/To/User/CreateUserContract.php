<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:04
 */

namespace App\Contracts\Services\Blockchain\To\User;

use App\Models\User\User;

interface CreateUserContract
{
    /**
     * Create user in blockchain gate
     *
     * @param User $user
     *
     * @return bool
     */
    public function createUser(User $user): bool;
}
