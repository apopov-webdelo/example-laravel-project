<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:10
 */

namespace App\Contracts\Services\Blockchain\To\Transaction;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

interface TransactionContract
{
    /**
     * Make transaction to another user in blockchain
     *
     * @param User                   $remitter
     * @param User                   $receiver
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $amount
     *
     * @return bool
     */
    public function transaction(
        User                   $remitter,
        User                   $receiver,
        CryptoCurrencyContract $cryptoCurrency,
        string                 $amount
    ): bool;
}
