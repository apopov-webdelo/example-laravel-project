<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:02
 */

namespace App\Contracts\Services\Blockchain\To\Transaction;

use App\Models\User\User;

/**
 * Interface EmergencyWithdrawContract
 *
 * @package App\Contracts\Services\Blockchain
 */
interface EmergencyWithdrawContract
{
    /**
     * Make emergency withdraw from all wallets in blockchain
     *
     * @param User   $user
     * @param string $privateKey
     *
     * @return array
     */
    public function emergencyWithdraw(User $user, string $privateKey): array;
}