<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:01
 */

namespace App\Contracts\Services\Blockchain\To\Transaction;

use App\Contracts\Balance\PayOut\PayOutContract;

/**
 * Interface WithdrawContract
 *
 * @package App\Contracts\Services\Blockchain
 */
interface WithdrawContract
{
    /**
     * Make withdraw transaction in blockchain
     *
     * @param PayOutContract $payOut
     *
     * @return bool
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function withdraw(PayOutContract $payOut): bool;
}
