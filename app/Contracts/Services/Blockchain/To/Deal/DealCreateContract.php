<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 13:32
 */

namespace App\Contracts\Services\Blockchain\To\Deal;

use App\Models\Deal\Deal;

interface DealCreateContract
{
    /**
     * Create deal in blockchain gate
     *
     * @param Deal $deal
     *
     * @return string
     */
    public function create(Deal $deal): string;

    /**
     * Set public keys for blockchain protected deal
     *
     * @param string $buyerKey
     * @param string $sellerKey
     *
     * @return DealCreateContract
     */
    public function keys(string $buyerKey, string $sellerKey): DealCreateContract;

    /**
     * Set crypto wallet ID for automatically transaction after deal finish
     *
     * @param string $walletId
     *
     * @return DealCreateContract
     */
    public function wallet(string $walletId): DealCreateContract;
}
