<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:08
 */

namespace App\Contracts\Services\Blockchain\To\Deal;

use App\Models\Deal\Deal;

interface DealCancelContract
{
    /**
     * Send request for cancel deal in blockchain
     *
     * @param Deal $deal
     *
     * @return bool
     */
    public function cancel(Deal $deal): bool;
}
