<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:04
 */

namespace App\Contracts\Services\Blockchain\To\Deal;

use App\Models\Deal\Deal;

interface DealFinishContract
{
    /**
     * Send request for finish deal in blockchain
     *
     * @param Deal $deal
     *
     * @return bool
     */
    public function finish(Deal $deal): bool;
}
