<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 13:45
 */

namespace App\Contracts\Services\Blockchain\To\Key;

interface KeysStorageContract
{
    /**
     * Retrieve public key from pair
     *
     * @return string
     */
    public function publicKey(): string;

    /**
     * Retrieve private key from pair
     *
     * @return string
     */
    public function privateKey(): string;

    /**
     * Retrieve private key alias
     *
     * @return string
     */
    public function privateAlias(): string;

    /**
     * Retrieve private key alias
     *
     * @return string
     */
    public function publicAlias(): string;
}