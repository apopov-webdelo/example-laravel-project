<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:09
 */

namespace App\Contracts\Services\Blockchain\To;

use App\Contracts\Services\Blockchain\To\Key\KeysStorageContract;

interface CreateKeysContract
{
    /**
     * Generate private/public keys pair in blockchain
     *
     * @return KeysStorageContract
     */
    public function createKeys(): KeysStorageContract;
}
