<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:09
 */

namespace App\Contracts\Services\Blockchain\To\Wallet;

use App\Models\User\User;

interface CreateEmergencyWalletContract
{
    /**
     * Create wallet for emergency transactions
     *
     * @param User   $user
     * @param string $publicKey
     *
     * @return string
     */
    public function createEmergencyWallet(
        User   $user,
        string $publicKey
    ): string;
}
