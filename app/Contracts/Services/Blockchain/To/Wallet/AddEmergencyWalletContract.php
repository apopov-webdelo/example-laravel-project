<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 12:09
 */

namespace App\Contracts\Services\Blockchain\To\Wallet;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

interface AddEmergencyWalletContract
{
    /**
     * Save wallet ID for emergency withdraw
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId
     *
     * @return bool
     */
    public function addEmergencyWallet(
        User                   $user,
        CryptoCurrencyContract $cryptoCurrency,
        string                 $walletId
    ): bool;
}
