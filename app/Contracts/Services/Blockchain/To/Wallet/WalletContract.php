<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/13/18
 * Time: 11:54
 */

namespace App\Contracts\Services\Blockchain\To\Wallet;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

interface WalletContract
{
    /**
     * Return wallet ID in blockchain
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return string
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function getWallet(User $user, CryptoCurrencyContract $cryptoCurrency): string;

    /**
     * Return amount in blockchain
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return float
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function getAmount(User $user, CryptoCurrencyContract $cryptoCurrency): float;
}
