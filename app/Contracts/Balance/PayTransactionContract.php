<?php

namespace App\Contracts\Balance;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

interface PayTransactionContract
{
    /**
     * Retrieve recipient for that transaction
     *
     * @return User
     */
    public function recipient(): User;

    /**
     * Retrieve crypto currency type
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract;

    /**
     * Retrieve amount for payin transaction
     *
     * @return int
     */
    public function amount(): int;
}
