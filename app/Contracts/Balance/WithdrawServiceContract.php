<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/29/18
 * Time: 10:43
 */

namespace App\Contracts\Balance;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\Balance\Transaction;
use App\Models\User\User;

/**
 * Interface DepositServiceContract
 *
 * @package App\Contracts\Balance
 */
interface WithdrawServiceContract
{
    /**
     * Decrease user's balance
     *
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return Transaction
     * @throws \Exception
     */
    public function withdraw(
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency
    ): Transaction;

    /**
     * Set custom description to withdraw transaction
     *
     * @param string $description
     *
     * @return WithdrawServiceContract
     */
    public function description(string $description): self;
}
