<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 12:22
 */

namespace App\Models\Balance\PayIn;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException;
use App\Models\User\User;

/**
 * Interface PayInServiceContract
 *
 * @package App\Models\Balance\PayIn
 */
interface PayInRegisterServiceContract
{
    /**
     * Store request with pending status
     *
     * @param string                 $transactionId
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @return PayInTransactionContract
     * @throws ExistingsTransactionIdPayInException
     */
    public function register(
        string $transactionId,
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency
    ): PayInTransactionContract;
}
