<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-14
 * Time: 18:03
 */

namespace App\Contracts\Balance\PayIn;

use App\Contracts\Balance\PayTransactionContract;
use Carbon\Carbon;

/**
 * Interface PayinTransactionContract
 *
 * @package App\Contracts\Balance
 */
interface PayInTransactionContract extends PayTransactionContract
{
    /**
     * Retrieve date for payin request creation
     *
     * @return Carbon
     */
    public function createdAt(): Carbon;

    /**
     * Retrieve date for payin request status updating
     *
     * @return Carbon
     */
    public function updatedAt(): Carbon;

    /**
     * Retrieve blockchain transaction ID number
     *
     * @return string
     */
    public function transactionId(): string;

    /**
     * Retrieve current processing payin status in backend (pending, processed, failed)
     *
     * @return string
     */
    public function status(): string;

    /**
     * Update entity status
     *
     * @param string $status
     *
     * @return bool
     */
    public function setStatus(string $status): bool;

    /**
     * Set status Processed
     *
     * @return bool
     */
    public function setProcessedStatus(): bool;

    /**
     * Set status Failed
     *
     * @return bool
     */
    public function setFailedStatus(): bool;

    /* Utils Methods */

    /**
     * Check is current status PENDING
     *
     * @return bool
     */
    public function isPending(): bool;

    /**
     * Check is current status PROCESSED
     *
     * @return bool
     */
    public function isProcessed(): bool;

    /**
     * Check is current status FAILE
     *
     * @return bool
     */
    public function isFailed(): bool;
}
