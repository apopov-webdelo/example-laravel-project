<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-15
 * Time: 17:19
 */

namespace App\Contracts\Balance\PayIn;

/**
 * Interface PayInProcessServiceContract
 *
 * @package App\Contracts\Balance\PayIn
 */
interface PayInProcessServiceContract
{
    /**
     * Process PayIn transaction request and mark as processed or failed
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @return PayInTransactionContract
     */
    public function process(PayInTransactionContract $payInTransaction): PayInTransactionContract;
}
