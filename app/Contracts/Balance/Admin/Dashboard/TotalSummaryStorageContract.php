<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/25/19
 * Time: 18:39
 */

namespace App\Contracts\Balance\Admin\Dashboard;

use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Repositories\Balance\TransactionRepo;

/**
 * Interface TotalSummaryStorageContract
 *
 * @package App\Contracts\Balance\Admin\Dashboard
 */
interface TotalSummaryStorageContract
{
    /**
     * Retrieve transactions repo filtered to withdraws
     *
     * @return TransactionRepo
     */
    public function deposits(): TransactionRepo;

    /**
     * Retrieve transactions repo filtered to withdraws
     *
     * @return TransactionRepo
     */
    public function withdraws(): TransactionRepo;

    /**
     * Retrieve user that summary for
     *
     * @return User
     */
    public function user(): User;

    /**
     * Retrieve crypto currency that summary for
     *
     * @return CryptoCurrency
     */
    public function cryptoCurrency(): CryptoCurrency;
}
