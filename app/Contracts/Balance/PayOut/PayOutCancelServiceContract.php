<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-02-13
 * Time: 14:42
 */

namespace App\Contracts\Balance\PayOut;

use App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException;

/**
 * Interface PayOutCancelServiceContract for cancellation payout request
 *
 * @package App\Contracts\Balance\PayOut
 */
interface PayOutCancelServiceContract
{
    /**
     * Cancel PayOut
     *
     * @param PayOutContract $payOut
     *
     * @return PayOutContract
     * @throws UnexpectedStatusPayOutException
     */
    public function cancel(PayOutContract $payOut): PayOutContract;

    /**
     * Set cancellation reason
     *
     * @param string $reason
     *
     * @return PayOutCancelServiceContract
     */
    public function reason(string $reason): self;
}
