<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-21
 * Time: 13:48
 */

namespace App\Contracts\Balance\PayOut;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;
use Carbon\Carbon;

/**
 * Class PayOutContract
 *
 * @package App\Contracts\Balance\PayOut
 */
interface PayOutContract
{
    /**
     * Retrieve PayOut ID number
     *
     * @return int
     */
    public function id(): int;

    /**
     * Retrieve recipient for that transaction
     *
     * @return User
     */
    public function user(): User;

    /**
     * Retrieve crypto currency type
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract;

    /**
     * Retrieve amount for payout transaction
     *
     * @return int
     */
    public function amount(): int;

    /**
     * Retrieve commission in coins for payout transaction
     *
     * @return int
     */
    public function commission(): int;

    /**
     * Retrieve total due for that payout
     *
     * Calculate sum of amount and comission
     *
     * @return int
     */
    public function due(): int;

    /**
     * Retrieve date for payout request creation
     *
     * @return Carbon
     */
    public function createdAt(): Carbon;

    /**
     * Retrieve date for payout request status updating
     *
     * @return Carbon
     */
    public function updatedAt(): Carbon;

    /**
     * Retrieve blockchain transaction ID number
     *
     * @return string
     */
    public function transactionId(): string;

    /**
     * Retrieve wallet ID for blockchain withdraw transaction
     *
     * @return string
     */
    public function walletId(): string;

    /**
     * Retrieve current processing payout status in backend (pending, processed, canceled)
     *
     * @return string
     */
    public function status(): string;

    /**
     * Update entity status
     *
     * @param string $status One from variants (pending, processed, canceled)
     *
     * @return bool
     */
    public function setStatus(string $status): bool;

    /**
     * Set status Processed
     *
     * @return bool
     */
    public function setProcessedStatus(): bool;

    /**
     * Set status Failed
     *
     * @return bool
     */
    public function setCanceledStatus(): bool;

    /**
     * Store transaction ID from blockchain
     *
     * @param string $transactionId
     *
     * @return bool
     */
    public function setTransactionId(string $transactionId): bool;

    /* Utils Methods */

    /**
     * Check is current status PENDING
     *
     * @return bool
     */
    public function isPending(): bool;

    /**
     * Check is current status PROCESSED
     *
     * @return bool
     */
    public function isProcessed(): bool;

    /**
     * Check is current status CANCELED
     *
     * @return bool
     */
    public function isCanceled(): bool;
}
