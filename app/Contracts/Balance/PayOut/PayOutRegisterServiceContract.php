<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-21
 * Time: 15:35
 */

namespace App\Contracts\Balance\PayOut;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Exceptions\Balance\PayOut\InsufficientBalanceAmountForPayOutException;
use App\Exceptions\Balance\PayOut\TooSmallAmountForPayOutException;
use App\Models\User\User;

/**
 * Contract PayOutRegisterServiceContract for services register new PayOut requests
 *
 * @package App\Contracts\Balance\PayOut
 */
interface PayOutRegisterServiceContract
{
    /**
     * Set is amount include commission for payout or not
     *
     * @param bool $isIncluded
     *
     * @return PayOutRegisterServiceContract
     */
    public function commissionIncluded(bool $isIncluded): PayOutRegisterServiceContract;

    /**
     * Register new request for PayOut
     *
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param string                 $walletId  Wallet in blockchain
     *
     * @return PayOutContract
     *
     * @throws InsufficientBalanceAmountForPayOutException
     * @throws TooSmallAmountForPayOutException
     */
    public function register(
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency,
        string $walletId
    ): PayOutContract;
}
