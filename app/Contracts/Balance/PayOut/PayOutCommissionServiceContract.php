<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-23
 * Time: 16:26
 */

namespace App\Contracts\Balance\PayOut;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\User\User;

/**
 * Contract PayOutCommissionServiceContract for calculating withdraw commission for user
 *
 * @package App\Contracts\Balance\PayOut
 */
interface PayOutCommissionServiceContract
{
    /**
     * Retrieve calculated commission for concrete amount
     *
     * @param int                    $amount
     * @param User                   $user
     * @param CryptoCurrencyContract $cryptoCurrency
     * @param bool                   $commissionIncluded Detect is commission include in amount or not
     *
     * @return int
     */
    public function calculateCommission(
        int $amount,
        User $user,
        CryptoCurrencyContract $cryptoCurrency,
        bool $commissionIncluded = true
    ): int;
}
