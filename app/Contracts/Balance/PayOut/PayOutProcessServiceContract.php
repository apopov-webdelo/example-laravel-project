<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2019-01-21
 * Time: 16:25
 */

namespace App\Contracts\Balance\PayOut;

/**
 * Contract PayOutProcessServiceContract
 *
 * @package App\Contracts\Balance\PayOut
 */
interface PayOutProcessServiceContract
{
    /**
     * Process PayOut request
     *
     * Make withdraw from balance, decrement escrow, fire event, change request status
     *
     * @param PayOutContract $payOut
     * @param string         $transactionId
     *
     * @return PayOutContract
     */
    public function process(PayOutContract $payOut, string $transactionId): PayOutContract;
}
