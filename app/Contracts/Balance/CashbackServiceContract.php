<?php

namespace App\Contracts\Balance;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Models\Balance\Transaction;

/**
 * Interface CashbackServiceContract
 *
 * For backend transaction of cashback
 *
 * @package App\Contracts\Balance
 */
interface CashbackServiceContract
{
    /**
     * Increase user's balance
     *
     * @param DealCashbackCommissionStorageContract $cashbackStorage
     *
     * @return Transaction
     */
    public function cashback(DealCashbackCommissionStorageContract $cashbackStorage): Transaction;

    /**
     * Set custom description to deposit transaction
     *
     * @param string $description
     *
     * @return CashbackServiceContract
     */
    public function description(string $description): self;
}
