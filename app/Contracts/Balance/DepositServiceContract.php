<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 11/29/18
 * Time: 10:43
 */

namespace App\Contracts\Balance;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Models\Balance\Transaction;

/**
 * Interface DepositServiceContract
 *
 * @package App\Contracts\Balance
 */
interface DepositServiceContract
{
    /**
     * Increase user's balance
     *
     * @param PayInTransactionContract $payInTransaction
     *
     * @return Transaction
     */
    public function deposit(PayInTransactionContract $payInTransaction): Transaction;

    /**
     * Set custom description to deposit transaction
     *
     * @param string $description
     *
     * @return DepositServiceContract
     */
    public function description(string $description): self;
}
