<?php

namespace App\Contracts\Bcrypt;

/**
 * Interface ModelKeyContract
 *
 * @package App\Contracts\Bcrypt
 */
interface ModelBcKeysContract
{
    /**
     * Check if key exists
     *
     * @param string $alias
     *
     * @return bool
     */
    public function isBcKeyExists(string $alias): bool;

    /**
     * Get key string or false
     *
     * @param string $alias
     *
     * @return string|false
     */
    public function getBcKey(string $alias);

    /**
     * Save key for current model or update existing one
     *
     * @param string $key
     * @param string $alias
     *
     * @return bool
     */
    public function saveBcKey(string $key, string $alias): bool;

    /**
     * Delete key for urrent model or update existing one
     *
     * @param string $alias
     *
     * @return bool
     */
    public function removeBcKey(string $alias): bool;
}
