<?php

namespace App\Contracts\Bcrypt;

/**
 * Interface ModelBcKeysRepoContract
 *
 * @package App\Contracts\Bcrypt
 */
interface ModelBcKeysRepoContract
{
    /**
     * Check if key exists
     *
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return bool
     */
    public function isExists(ModelBcKeysContract $model, string $alias): bool;

    /**
     * Get key by alias
     *
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return string|bool
     */
    public function key(ModelBcKeysContract $model, string $alias);

    /**
     * Delete key by alias
     *
     * @param ModelBcKeysContract $model
     * @param string              $alias
     *
     * @return bool
     */
    public function remove(ModelBcKeysContract $model, string $alias): bool;

    /**
     * Save or update existing key
     *
     * @param ModelBcKeysContract $model
     * @param string              $key
     *
     * @return bool
     */
    public function save(ModelBcKeysContract $model, string $key): bool;

    /**
     * @param ModelBcKeysContract $model
     *
     * @return ModelBcKeysRepoContract
     */
    public function model(ModelBcKeysContract $model): ModelBcKeysRepoContract;

    /**
     * @param string $alias
     *
     * @return ModelBcKeysRepoContract
     */
    public function alias(string $alias): ModelBcKeysRepoContract;
}
