<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/2/18
 * Time: 16:06
 */

namespace App\Contracts;

/**
 * Interface GeoIPContracts
 * @package App\Contracts
 */
interface GeoLocatorContract
{
    /**
     * Return user IP
     *
     * @return string
     */
    public function getIp(): string;

    /**
     * Return country code in ISO format
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountryISO(string $ip = null): string;

    /**
     * Return country code in Alpha2 format
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountryAlpha2(string $ip = null): string;

    /**
     * Return country code in Alpha3 format
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountryAlpha3(string $ip = null): string;

    /**
     * Return country full name
     *
     * @param string|null $ip
     * @return string
     */
    public function getCountry(string $ip = null): string;

    /**
     * Return city name
     *
     * @param string|null $ip
     * @return string
     */
    public function getCity(string $ip = null): string;

    /**
     * Return state name
     *
     * @param string|null $ip
     * @return string
     */
    public function getState(string $ip = null): string;

    /**
     * Return state code
     *
     * @param string|null $ip
     * @return string
     */
    public function getStateCode(string $ip = null): string;

    /**
     * Return ZIP code (postal code)
     *
     * @param string|null $ip
     * @return string
     */
    public function getPostalCode(string $ip = null): string;

    /**
     * Return timezone
     *
     * @param string|null $ip
     * @return string
     */
    public function getTimezone(string $ip = null): string;

    /**
     * Return continent code
     *
     * @param string|null $ip
     * @return string
     */
    public function getContinent(string $ip = null): string;

    /**
     * Return currency code
     *
     * @param string|null $ip
     * @return string
     */
    public function getCurrency(string $ip = null): string;

    /**
     * Check is that value is default (IP was not found in database)
     *
     * @param string|null $ip
     * @return bool
     */
    public function isDefault(string $ip = null): bool;

}