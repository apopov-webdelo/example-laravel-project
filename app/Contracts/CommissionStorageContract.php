<?php

namespace App\Contracts;

use App\Contracts\Currency\CryptoCurrencyContract;

interface CommissionStorageContract
{
    /**
     * Retrieve crypto currency type for the bonus
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract;

    /**
     * Retrieve amount in satoshies
     *
     * @return int
     */
    public function amount(): int;
}
