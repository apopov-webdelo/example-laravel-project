<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/27/18
 * Time: 08:43
 */

namespace App\Contracts;

/**
 * Interface PasswordResetContract
 *
 * @package App\Contracts
 */
interface PasswordResetContract
{
    /**
     * @param AuthenticatedContract $user
     *
     * @return PasswordResetContract
     */
    public function setUser(AuthenticatedContract $user);

    /**
     * @param string $reason
     *
     * @return PasswordResetContract
     */
    public function reset(string $reason): PasswordResetContract;
}
