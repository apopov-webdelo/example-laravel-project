<?php

namespace App\Contracts\Utils\Session;

/**
 * Interface SessionStorageContract
 *
 * @package App\Contracts\Utils\Session
 */
interface SessionStorageContract
{
    /**
     * Client ip
     *
     * @return string
     */
    public function ip(): string;

    /**
     * Client OS
     *
     * @return string
     */
    public function os(): string;

    /**
     * Client browser
     *
     * @return string
     */
    public function browser(): string;
}
