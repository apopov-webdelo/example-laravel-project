<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 10:30
 */

namespace App\Contracts\Partnership;

/**
 * Interface PartnershipProgramProcessingContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipProcessingContract
{
    /**
     * Transfer coins to partners if referrals will be detected in that entity
     *
     * Detect if exists entity's referrals and transfer money to partners. Log any result of action.
     *
     * @param PartnershipEntityContract $entity
     *
     * @return bool
     */
    public function process(PartnershipEntityContract $entity): bool;
}
