<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 10:29
 */

namespace App\Contracts\Partnership;

use App\Exceptions\Partnership\PartnerAlreadyRegisteredException;
use App\Models\User\User;

/**
 * Interface PartnerRegisterContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnerRegisterContract
{
    /**
     * Register new Partner
     *
     * @param User $user
     * @param PartnershipProgramContract $programContract
     *
     * @return PartnerContract
     *
     * @throws \Exception
     * @throws PartnerAlreadyRegisteredException
     */
    public function register(User $user, PartnershipProgramContract $programContract = null): PartnerContract;
}
