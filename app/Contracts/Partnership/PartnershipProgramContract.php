<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 12:27
 */

namespace App\Contracts\Partnership;

use App\Models\Partnership\PartnershipProgramStatus;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface PartnershipProgramContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipProgramContract
{
    /**
     * @return PartnershipProgramStatus
     */
    public function getStatus(): PartnershipProgramStatus;

    /**
     * @return Collection<PartnerContract>
     */
    public function getPartners(): Collection;

    /**
     * Retrieve commission driver
     *
     * @return PartnershipCommissionStrategyContract
     */
    public function getCommissionDriver(): PartnershipCommissionStrategyContract;

    /**
     * Retrieve driver alias
     *
     * @return string
     */
    public function getDriverAlias(): string;
}
