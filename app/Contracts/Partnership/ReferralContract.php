<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 12:24
 */

namespace App\Contracts\Partnership;

use App\Models\User\User;
use Carbon\Carbon;

/**
 * Interface ReferralContract
 *
 * @package App\Contracts\Partnership
 */
interface ReferralContract
{
    /**
     * Retrieve User object related with that referral
     *
     * @return User
     */
    public function getUser(): User;

    /**
     * Retrieve referral's partner
     *
     * @return PartnerContract
     */
    public function getPartner(): PartnerContract;

    /**
     * Referral registration date
     *
     * @return Carbon
     */
    public function getRegistrationDate(): Carbon;
}
