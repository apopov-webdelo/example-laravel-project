<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 10:28
 */

namespace App\Contracts\Partnership;

use App\Contracts\Repositories\RepoContract;
use App\Models\Partnership\PartnershipProgram;
use App\Models\User\User;

/**
 * Interface PartnerRepoContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnerRepoContract extends RepoContract
{
    /**
     * @param User $user
     *
     * @return PartnerRepoContract
     */
    public function filterByUser(User $user): PartnerRepoContract;

    /**
     * @param PartnershipProgram $partnershipProgram
     *
     * @return PartnerRepoContract
     */
    public function filterByPartnershipProgram(PartnershipProgram $partnershipProgram): PartnerRepoContract;
}
