<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 10:29
 */

namespace App\Contracts\Partnership;

/**
 * Interface PartnershipCommissionStrategyContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipCommissionStrategyContract
{
    /**
     * Get commission storage by partnership entity
     *
     * @param PartnershipEntityContract $entity
     * @param ReferralContract          $referral
     *
     * @return PartnershipCommissionStorageContract
     */
    public function getCommission(
        PartnershipEntityContract $entity,
        ReferralContract $referral
    ): PartnershipCommissionStorageContract;
}
