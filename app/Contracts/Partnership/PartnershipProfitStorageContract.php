<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-12
 * Time: 11:52
 */

namespace App\Contracts\Partnership;

use App\Contracts\Currency\CryptoCurrencyContract;

/**
 * Interface PartnershipProfitStorage
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipProfitStorageContract
{
    /**
     * Retrieve profit amount in coins
     *
     * @return int
     */
    public function amount(): int;

    /**
     * Retrieve crypto type for that profit
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract;
}
