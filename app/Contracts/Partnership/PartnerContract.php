<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 12:24
 */

namespace App\Contracts\Partnership;

use App\Models\Partnership\PartnershipProgram;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface PartnerContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnerContract
{
    /**
     * @return User
     */
    public function getUser(): User;

    /**
     * @return PartnershipProgramContract
     */
    public function getPartnershipProgram(): PartnershipProgramContract;

    /**
     * @return Collection<ReferralContract>
     */
    public function getReferrals(): Collection;

    /**
     * Retrieve partner referral code
     *
     * @return string
     */
    public function getCode(): string;
}
