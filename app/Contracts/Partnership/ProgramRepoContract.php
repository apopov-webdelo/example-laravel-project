<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-27
 * Time: 13:25
 */

namespace App\Repositories\Partnership;

use App\Contracts\Partnership\PartnershipProgramContract;
use App\Contracts\Repositories\RepoContract;

interface ProgramRepoContract extends RepoContract
{
    /**
     * Return Program model by driver alias
     *
     * @param string $driverAlias
     *
     * @return PartnershipProgramContract
     */
    public function getByDriver(string $driverAlias): PartnershipProgramContract;

    /**
     * Return default Program model by driver alias
     *
     * @return PartnershipProgramContract
     */
    public function getDefaultProgram(): PartnershipProgramContract;
}
