<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 10:32
 */

namespace App\Contracts\Partnership;

use App\Contracts\Repositories\RepoContract;
use App\Models\Partnership\Partner;
use App\Models\User\User;

/**
 * Interface ReferralRepoContract
 *
 * @package App\Contracts\Referral
 */
interface ReferralRepoContract extends RepoContract
{
    /**
     * @param User $user
     *
     * @return ReferralRepoContract
     */
    public function filterByUser(User $user): ReferralRepoContract;

    /**
     * @param Partner $partner
     *
     * @return ReferralRepoContract
     */
    public function filterByPartner(Partner $partner): ReferralRepoContract;
}
