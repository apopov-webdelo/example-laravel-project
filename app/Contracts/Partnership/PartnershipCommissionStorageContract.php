<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 11:52
 */

namespace App\Contracts\Partnership;

use App\Contracts\CommissionStorageContract;

/**
 * Interface PartnershipCommissionStorageContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipCommissionStorageContract extends CommissionStorageContract
{
    /**
     * Retrieve partner that must get reward
     *
     * @return PartnerContract
     */
    public function partner(): PartnerContract;

    /**
     * Retrieve referral that related with that reward
     *
     * @return ReferralContract
     */
    public function referral(): ReferralContract;

    /**
     * Retrieve entity that is reason for partner reward
     *
     * @return PartnershipEntityContract
     */
    public function entity(): PartnershipEntityContract;
}
