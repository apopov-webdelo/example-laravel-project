<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-12
 * Time: 11:50
 */

namespace App\Contracts\Partnership;

use App\Contracts\EntityContract;
use App\Models\Partnership\Referral;

/**
 * Interface PartnershipRewardContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipEntityContract extends EntityContract
{
    /**
     * Retrieve service profit for partnership entity
     *
     * @return PartnershipProfitStorageContract
     */
    public function entityProfit(): PartnershipProfitStorageContract;

    /**
     * Retrieve all referrals related with that entity
     *
     * Warning! That must be array of Refferals objects! Otherwise will be thrown exception!
     *
     * @return Referral[]
     */
    public function entityReferrals(): array;
}
