<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 12/11/18
 * Time: 10:29
 */

namespace App\Contracts\Partnership;

use App\Models\Partnership\Partner;
use App\Models\User\User;

/**
 * Interface ReferralRegisterContract
 *
 * @package App\Contracts\Partnership
 */
interface ReferralRegisterContract
{
    /**
     * Register new Referral ralated with User model
     *
     * @param User    $user
     * @param PartnerContract $partner
     *
     * @return ReferralContract
     */
    public function register(User $user, PartnerContract $partner): ReferralContract;
}
