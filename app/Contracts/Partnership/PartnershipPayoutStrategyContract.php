<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-12
 * Time: 08:11
 */

namespace App\Contracts\Partnership;

/**
 * Interface PartnershipPayoutStrategyContract
 *
 * @package App\Contracts\Partnership
 */
interface PartnershipPayoutStrategyContract
{
    /**
     * Make payout for referral
     *
     * @param PartnershipCommissionStorageContract $commissionStorage
     *
     * @return bool
     */
    public function pay(PartnershipCommissionStorageContract $commissionStorage): bool;
}
