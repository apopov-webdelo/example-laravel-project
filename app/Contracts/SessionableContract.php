<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/16/18
 * Time: 09:39
 */

namespace App\Contracts;

interface SessionableContract
{
    public function sessions();
}