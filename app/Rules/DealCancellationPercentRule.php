<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */
namespace App\Rules;

use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\User\User;

class DealCancellationPercentRule implements RuleContract
{
    /**
     * @var AuthenticatedContract|User $firstUser
     */
    private $firstUser;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $secondUser;

    /**
     * DealCancellationPercentRule constructor.
     *
     * @param AuthenticatedContract $firstUser
     * @param AuthenticatedContract $secondUser
     */
    public function __construct(AuthenticatedContract $firstUser, AuthenticatedContract $secondUser)
    {
        $this->firstUser  = $firstUser;
        $this->secondUser = $secondUser;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        if (is_null($this->firstUser->statistic->deals_cancellation_percent)
            && is_null($this->secondUser->settings->deal_cancellation_max_percent)
       ) {
            return true;
        }
        return $this->firstUser
                    ->statistic
                    ->deals_cancellation_percent <= $this->secondUser->settings->deal_cancellation_max_percent;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.invalid_buyer_deal_cancellation_max_percent');
    }
}