<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules;

use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Balance\Balance;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;

class CryptoCurrencyMinBalanceRule implements RuleContract
{
    /**
     * @var CryptoCurrency $cryptoCurrency
     */
    private $cryptoCurrency;

    /**
     * @var Balance $balance
     */
    private $balance;

    /**
     * CryptoCurrencyMinBalanceRule constructor.
     *
     * @param CryptoCurrency        $cryptoCurrency
     * @param AuthenticatedContract|User $user
     *
     * @throws \Exception
     */
    public function __construct(CryptoCurrency $cryptoCurrency, AuthenticatedContract $user)
    {
        $this->cryptoCurrency = $cryptoCurrency;
        $this->balance        = $user->getBalance($cryptoCurrency);

        $this->checkCompatibility();
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        $balance    = $this->balance->amount;
        $minBalance = $this->cryptoCurrency->min_balance;

        return $balance >= $minBalance;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.balance_is_to_small');
    }

    /**
     * @return $this
     * @throws \Exception
     */
    private function checkCompatibility()
    {
        if ($this->balance->cryptoCurrency->id !== $this->cryptoCurrency->id) {
            throw new \Exception('Crypto currency model doesn\'t match to transmitted balance');
        }
        return $this;
    }
}