<?php

namespace App\Rules;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Balance\PayOut\PayOutCommissionServiceContract;
use App\Http\Requests\Client\Balance\WithdrawRequest;
use App\Models\Directory\CryptoCurrency;
use Illuminate\Contracts\Validation\Rule;

class CheckBalanceIsEnough implements Rule
{
    /**
     * @var WithdrawRequest
     */
    protected $request;

    /**
     * @var AuthenticatedContract
     */
    protected $user;

    /**
     * @var PayOutCommissionServiceContract
     */
    protected $payOutCommissionService;

    /**
     * CheckBalanceIsEnough constructor.
     *
     * @param WithdrawRequest                 $request
     * @param AuthenticatedContract           $user
     * @param PayOutCommissionServiceContract $payOutCommissionService
     */
    public function __construct(
        WithdrawRequest $request,
        AuthenticatedContract $user,
        PayOutCommissionServiceContract $payOutCommissionService
    ) {
        $this->request = $request;
        $this->user = $user;
        $this->payOutCommissionService = $payOutCommissionService;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $crypto = CryptoCurrency::findOrFail($this->request->crypto_currency_id);
        $value  = currencyToCoins($value, $crypto);

        $balance = $this->user->getBalance($crypto)->amount;
        $cIncluded = (bool)$this->request->commissionIncluded;

        $neededAmount = $cIncluded
            ? $value
            : $value + $this->payOutCommissionService->calculateCommission(
                $value,
                $this->user,
                $crypto,
                false
            );

        return $balance >= $neededAmount;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('balance.balance.check.not_enough');
    }

    public function __toString()
    {
        return get_class($this);
    }
}
