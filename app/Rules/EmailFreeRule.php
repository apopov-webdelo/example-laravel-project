<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */
namespace App\Rules;

use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\User\User;
use App\Repositories\User\UserRepo;

class EmailFreeRule implements RuleContract
{
    private $repo;

    private $user;

    /**
     * EmailFreeRule constructor.
     *
     * @param AuthenticatedContract|User $user
     * @param UserRepo              $repo
     */
    public function __construct(AuthenticatedContract $user, UserRepo $repo)
    {
        $this->repo = $repo;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->repo
                    ->filterByLogin($this->user->login)
                    ->excludeById($this->user->id)
                    ->take()
                    ->get()
                    ->isEmpty();
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.email_free');
    }
}