<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */
namespace App\Rules;

use App\Contracts\RuleContract;
use App\Utils\TorChecker\ExitNodeList;
use App\Utils\TorChecker\TorIpChecker;

class TorDeniedRule implements RuleContract
{
    /**
     * TorDeniedRule constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        //TODO: Check if this plugin really works
        $checker = new TorIpChecker(new ExitNodeList());
        if (empty($_SERVER['REMOTE_ADDR'])) {
            return true;
        }
        return ! $checker->isInList($_SERVER['REMOTE_ADDR']);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.tor_denied');
    }
}