<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/19/18
 * Time: 13:24
 */

namespace App\Rules\Deal;

use App\Contracts\RuleContract;
use App\Models\Deal\Deal;

/**
 * Check is deal deadline happend
 *
 * @package App\Rules\Deal
 */
class DealDeadlineRule implements RuleContract
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return bool
     */
    public function check() : bool
    {
        return $this->deal->deadline->lt(now());
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.is_not_deadline_time');
    }
}