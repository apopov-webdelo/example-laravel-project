<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;

use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;

class AdBlacklistFreeRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;

    /**
     * @var AuthenticatedContract
     */
    private $user;

    /**
     * AdBlacklistFreeRule constructor.
     *
     * @param Ad                    $ad
     * @param AuthenticatedContract $user
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->ad->author->isNotInBlacklist($this->user)
            && $this->user->isNotInBlacklist($this->ad->author);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->user->isInBlacklist($this->ad->author)
            ? __('validation.deal.blacklist_detected_owner')
            : __('validation.deal.blacklist_detected');
    }
}