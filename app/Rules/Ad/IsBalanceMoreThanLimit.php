<?php

namespace App\Rules\Ad;

use App\Contracts\RuleContract;
use App\Models\Directory\CommissionConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\User\User;

/**
 * Class IsBalanceMoreThanLimit
 *
 * @package App\Rules\Ad
 */
class IsBalanceMoreThanLimit implements RuleContract
{
    /**
     * @var int
     */
    protected $limit;

    /**
     * @var CryptoCurrency
     */
    protected $cryptoCurrency;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var User
     */
    protected $user;
    /**
     * @var float
     */
    protected $price;
    /**
     * @var float
     */
    protected $balance;
    /**
     * @var int
     */
    protected $commission;

    /**
     * @param float $limit
     * @param float $price
     * @param float $balance
     * @param float $commission
     */
    public function __construct(float $limit, float $price, float $balance, float $commission)
    {
        $this->limit = $limit;
        $this->price = $price;
        $this->balance = $balance;
        $this->commission = $commission;
    }

    /**
     * Check if crypto currency balance more that max limit
     *
     * @return bool
     */
    public function check(): bool
    {
        $userLimit = $this->balance * $this->price;
        $expectedLimit = $this->limit * (1 + $this->commission / CommissionConstants::ONE_PERCENT);

        return $userLimit >= $expectedLimit;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.ad.limit_less_balance');
    }
}
