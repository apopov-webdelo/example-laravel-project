<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;


use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

class AdDealCancellationPercentRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * AdDealCancellationPercentRule constructor.
     *
     * @param Ad                    $ad
     * @param AuthenticatedContract $user
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->ad->deal_cancellation_max_percent
            ? $this->user->statistic->deals_cancellation_percent <= $this->ad->deal_cancellation_max_percent
            : true;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.invalid_buyer_deal_cancellation_max_percent');
    }
}