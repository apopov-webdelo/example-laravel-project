<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;


use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

class AdNotAuthorRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * NotAdAuthor constructor.
     *
     * @param Ad                    $ad
     * @param AuthenticatedContract|User $user
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad   = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->user->id !== $this->ad->author_id;
    }

    public function message(): string
    {
        return __('validation.deal.author_is_ad_author');
    }
}