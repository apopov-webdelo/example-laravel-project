<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;


use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

class AdMinDealFinishedCountRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;

    /**
     * @var AuthenticatedContract|User $user
     */
    private $user;

    /**
     * AdActiveRule constructor.
     *
     * @param Ad $ad
     * @param User|AuthenticatedContract $user
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad   = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->ad->min_deal_finished_count
            ? $this->ad->min_deal_finished_count < $this->user->statistic->deals_finished_count
            : true;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.min_deal_finished_count');
    }
}