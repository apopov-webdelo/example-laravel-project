<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;


use App\Contracts\RuleContract;
use App\Models\Ad\Ad;
use App\Rules\TorDeniedRule;

class AdTorDeniedRule implements RuleContract
{
    /**
     * @var Ad
     */
    private $object;

    /**
     * AdActiveRule constructor.
     *
     * @param Ad $ad
     */
    public function __construct(Ad $ad)
    {
        $this->object = $ad;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->object->tor_denied
            ? app(TorDeniedRule::class)->check()
            : true;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.tor_denied');
    }
}