<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;

use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

class AdTurnOverRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;

    /**
     * @var User $user
     */
    private $user;

    /**
     * AdTurnOverRule constructor.
     *
     * @param Ad                    $ad
     * @param AuthenticatedContract $user
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad   = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->ad->turnover <= $this->user->statistic->total_turnover;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.turnover_required');
    }
}