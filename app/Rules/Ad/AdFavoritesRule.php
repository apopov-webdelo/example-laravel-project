<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;


use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;

class AdFavoritesRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;


    private $user;

    /**
     * BlacklistFreeRule constructor.
     *
     * @param Ad $ad
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad   = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->ad->trust_required
            ? $this->ad->author->isInFavoriteList($this->user)
            : true;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.trust_required');
    }
}