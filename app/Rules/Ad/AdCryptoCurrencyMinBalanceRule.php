<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;

use App\Contracts\AuthenticatedContract;
use App\Contracts\RuleContract;
use App\Models\Ad\Ad;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use App\Rules\CryptoCurrencyMinBalanceRule;

class AdCryptoCurrencyMinBalanceRule implements RuleContract
{
    /**
     * @var Ad $ad
     */
    private $ad;

    /**
     * @var User $user
     */
    private $user;

    /**
     * CryptoCurrencyMinBalanceRule constructor.
     *
     * @param Ad   $ad
     * @param User|AuthenticatedContract $user
     */
    public function __construct(Ad $ad, AuthenticatedContract $user)
    {
        $this->ad   = $ad;
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        $user = $this->ad->is_sale
            ? $this->ad->author
            : $this->user;

        $cryptoCurency = CryptoCurrency::where('id', $this->ad->crypto_currency_id)->first();

        return app(CryptoCurrencyMinBalanceRule::class, ['cryptoCurrency' => $cryptoCurency, 'user' => $user])->check();
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.balance_is_to_small');
    }
}
