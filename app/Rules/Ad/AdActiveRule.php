<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/2/18
 * Time: 11:34
 */

namespace App\Rules\Ad;

use App\Contracts\RuleContract;
use App\Models\Ad\Ad;

class AdActiveRule implements RuleContract
{
    /**
     * @var Ad
     */
    private $object;

    /**
     * AdActiveRule constructor.
     *
     * @param Ad $ad
     */
    public function __construct(Ad $ad)
    {
        $this->object = $ad;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->object->is_active;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('validation.deal.ad_is_not_active');
    }
}
