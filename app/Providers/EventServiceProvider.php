<?php

namespace App\Providers;

use App\Events\Balance\BalanceReplenished;
use App\Events\Balance\BalanceWrittenOff;
use App\Events\Balance\PayIn\PayInTransactionProcessed;
use App\Events\Balance\PayIn\PayInTransactionStored;
use App\Events\Balance\PayOut\PayOutProcessed;
use App\Events\Balance\PayOut\PayOutRegistered;
use App\Events\Blockchain\From\Transaction\BlockchainWithdraw;
use App\Events\Blockchain\From\Wallet\WalletCreationError;
use App\Events\Blockchain\StartCompareBalanceWithStellar;
use App\Events\Blockchain\StellarLogEvent;
use App\Events\Chat\ChatPlaced;
use App\Events\Chat\MessagePlaced;
use App\Events\Deal\DealCanceled;
use App\Events\Deal\DealCancellation;
use App\Events\Deal\DealDisputed;
use App\Events\Deal\DealFinished;
use App\Events\Deal\DealFinishing;
use App\Events\Deal\DealPaid;
use App\Events\Deal\DealPlaced;
use App\Events\Deal\DealPlacedForStellar;
use App\Events\DealCashback\DealCashbackCancelState;
use App\Events\DealCashback\DealCashbackDoneState;
use App\Events\DealCashback\DealCashbackPendingState;
use App\Events\Directory\BankSaved;
use App\Events\Directory\CountrySaved;
use App\Events\Directory\CryptoCurrencySaved;
use App\Events\Directory\CurrencySaved;
use App\Events\Directory\PaymentSystemSaved;
use App\Events\Employee\EmployeeRegistered;
use App\Events\Employee\EmployeeUpdated;
use App\Events\Partnership\PartnerRegistered;
use App\Events\Partnership\PartnerRewarded;
use App\Events\Partnership\ReferralRegistered;
use App\Events\Review\ReviewDeleted;
use App\Events\Review\ReviewPlaced;
use App\Events\Review\ReviewUpdated;
use App\Events\User\EmailAccessRequested;
use App\Events\User\EmailConfirmationRequested;
use App\Events\User\Locked;
use App\Events\User\LoginError;
use App\Events\User\LoginSuccess;
use App\Events\User\LogoutSuccess;
use App\Events\User\PasswordReseted;
use App\Events\User\PhoneAccessRequested;
use App\Events\User\PhoneAddedEvent;
use App\Events\User\PhoneConfirmationRequested;
use App\Events\User\Registered;
use App\Events\User\SecurityLogEvent;
use App\Events\User\TelegramAdded;
use App\Events\User\TradeLocked;
use App\Events\User\TradeUnlocked;
use App\Events\User\Unlocked;
use App\Listeners\Ad\ReturnAdLiquidityListener;
use App\Listeners\Ad\SubstractAdLiquidityListener;
use App\Listeners\Balance\PayOut\ProcessPayOut;
use App\Listeners\Balance\PayOut\RegisterPayOutInBlockckain;
use App\Listeners\Balance\ProcessPayInTransactionListener;
use App\Listeners\Balance\UpdateAdLimitsListener;
use App\Listeners\Blockchain\CompareBalanceWithStellar;
use App\Listeners\Blockchain\Deal\CancelDealToStellar;
use App\Listeners\Blockchain\Deal\CreateDealListener;
use App\Listeners\Blockchain\Deal\FinishDealToStellar;
use App\Listeners\Blockchain\LogErrorStellarRequest;
use App\Listeners\Blockchain\LogStellarRequest;
use App\Listeners\Blockchain\Transaction\CompareBalanceOnPayIn;
use App\Listeners\Blockchain\Transaction\CompareBalanceOnPayOut;
use App\Listeners\Blockchain\User\CreateUserListener;
use App\Listeners\Blockchain\User\PartnerRewardedListener;
use App\Listeners\Blockchain\Wallet\WalletCreationErrorListener;
use App\Listeners\Deal\DealCanceledListener;
use App\Listeners\Deal\DealDisputedAdminEmailNotification;
use App\Listeners\Deal\DealDisputedAdminSlackNotification;
use App\Listeners\Deal\DealDisputedListener;
use App\Listeners\Deal\DealFinishedListener;
use App\Listeners\Deal\DealPaidListener;
use App\Listeners\Deal\DealPlacedListener;
use App\Listeners\Deal\DealUpdateUsdAmount;
use App\Listeners\Deal\Statistics\UpdateAverageFinishDealTimeListener;
use App\Listeners\Deal\Statistics\UpdateBalanceTurnoverListener;
use App\Listeners\Deal\Statistics\UpdateUserDealCountersListener;
use App\Listeners\Deal\Statistics\UpdateUserTurnoverListener;
use App\Listeners\DealCashback\BreakDealCashback;
use App\Listeners\DealCashback\CashbackTransfer;
use App\Listeners\DealCashback\ProcessDealCashback;
use App\Listeners\DealCashback\SendCashbackCancelAlert;
use App\Listeners\DealCashback\SendCashbackToStellar;
use App\Listeners\Directory\FlushDirectoryCache;
use App\Listeners\Employee\EmployeeRegistrationListener;
use App\Listeners\Employee\EmployeeUpdateListener;
use App\Listeners\Partnership\PartnerProgramProcessing;
use App\Listeners\Partnership\PartnerRegisteredNotification;
use App\Listeners\Partnership\PartnerRewardedNotification;
use App\Listeners\Partnership\ReferralRegisteredNotification;
use App\Listeners\User\ChatMessagePlacedListener;
use App\Listeners\User\ChatPlacedListener;
use App\Listeners\User\EmailAccessRequestedListener;
use App\Listeners\User\EmailConfirmationRequestedListener;
use App\Listeners\User\LockedNotificationListener;
use App\Listeners\User\LoginErrorNotificationListener;
use App\Listeners\User\LoginErrorSessionListener;
use App\Listeners\User\LoginSuccessNotificationListener;
use App\Listeners\User\LoginSuccessSessionListener;
use App\Listeners\User\LogoutSuccessSessionListener;
use App\Listeners\User\PasswordResetedListener;
use App\Listeners\User\PhoneAccessRequestedListener;
use App\Listeners\User\PhoneConfirmationRequestedListener;
use App\Listeners\User\RegistrationListener;
use App\Listeners\User\SecurityLogListener;
use App\Listeners\User\TelegramAddedListener;
use App\Listeners\User\TradeLockedNotificationListener;
use App\Listeners\User\TradeUnlockedNotificationListener;
use App\Listeners\User\UnlockedNotificationListener;
use App\Listeners\User\UpdateReputationListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 *
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        BalanceReplenished::class             => [
            UpdateAdLimitsListener::class,
        ],
        BalanceWrittenOff::class              => [
            UpdateAdLimitsListener::class,
        ],
        Registered::class                     => [
            RegistrationListener::class,
            CreateUserListener::class,
        ],
        DealPlaced::class                     => [
            DealPlacedListener::class,
            UpdateUserDealCountersListener::class,
            DealUpdateUsdAmount::class,
            SubstractAdLiquidityListener::class,
        ],
        DealPlacedForStellar::class           => [
            CreateDealListener::class,
        ],
        DealCanceled::class                   => [
            DealCanceledListener::class,
            UpdateUserDealCountersListener::class,
            ReturnAdLiquidityListener::class,
            BreakDealCashback::class,
        ],
        DealCancellation::class               => [
            CancelDealToStellar::class,
        ],
        DealDisputed::class                   => [
            DealDisputedListener::class,
            UpdateUserDealCountersListener::class,
            DealDisputedAdminSlackNotification::class,
            DealDisputedAdminEmailNotification::class,
        ],
        DealPaid::class                       => [
            DealPaidListener::class,
            UpdateUserDealCountersListener::class,
        ],
        DealFinishing::class                  => [
            FinishDealToStellar::class,
        ],
        DealFinished::class                   => [
            DealFinishedListener::class,
            UpdateBalanceTurnoverListener::class,
            UpdateUserTurnoverListener::class,
            UpdateUserDealCountersListener::class,
            PartnerProgramProcessing::class,
            UpdateAverageFinishDealTimeListener::class,
            ProcessDealCashback::class,
        ],
        ChatPlaced::class                     => [
            ChatPlacedListener::class,
        ],
        MessagePlaced::class                  => [
            ChatMessagePlacedListener::class,
        ],
        EmailAccessRequested::class           => [
            EmailAccessRequestedListener::class,
        ],
        EmailConfirmationRequested::class     => [
            EmailConfirmationRequestedListener::class,
        ],
        PhoneAccessRequested::class           => [
            PhoneAccessRequestedListener::class,
        ],
        PhoneConfirmationRequested::class     => [
            PhoneConfirmationRequestedListener::class,
        ],
        PhoneAddedEvent::class                => [
            PhoneConfirmationRequestedListener::class,
        ],
        TelegramAdded::class                  => [
            TelegramAddedListener::class,
        ],
        LoginSuccess::class                   => [
            LoginSuccessSessionListener::class,
            LoginSuccessNotificationListener::class,
        ],
        LoginError::class                     => [
            LoginErrorSessionListener::class,
            LoginErrorNotificationListener::class,
        ],
        LogoutSuccess::class                  => [
            LogoutSuccessSessionListener::class,
        ],
        ReviewPlaced::class                   => [
            UpdateReputationListener::class,
        ],
        ReviewUpdated::class                  => [
            UpdateReputationListener::class,
        ],
        ReviewDeleted::class                  => [
            UpdateReputationListener::class,
        ],
        PasswordReseted::class                => [
            PasswordResetedListener::class,
        ],
        EmployeeRegistered::class             => [
            EmployeeRegistrationListener::class,
        ],
        EmployeeUpdated::class                => [
            EmployeeUpdateListener::class,
        ],
        Locked::class                         => [
            LockedNotificationListener::class,
        ],
        Unlocked::class                       => [
            UnlockedNotificationListener::class,
        ],
        TradeLocked::class                    => [
            TradeLockedNotificationListener::class,
        ],
        TradeUnlocked::class                  => [
            TradeUnlockedNotificationListener::class,
        ],
        PartnerRegistered::class              => [
            PartnerRegisteredNotification::class,
        ],
        PartnerRewarded::class                => [
            PartnerRewardedNotification::class,
            PartnerRewardedListener::class,
        ],
        ReferralRegistered::class             => [
            ReferralRegisteredNotification::class,
        ],
        WalletCreationError::class            => [
            WalletCreationErrorListener::class,
        ],
        PayInTransactionProcessed::class      => [
            CompareBalanceOnPayIn::class,
        ],
        PayOutProcessed::class                => [
            CompareBalanceOnPayOut::class,
        ],
        SecurityLogEvent::class               => [
            SecurityLogListener::class,
        ],
        PayInTransactionStored::class         => [
            ProcessPayInTransactionListener::class,
        ],
        PayOutRegistered::class               => [
            RegisterPayOutInBlockckain::class,
        ],
        BlockchainWithdraw::class             => [
            ProcessPayOut::class,
        ],
        DealCashbackPendingState::class       => [
            SendCashbackToStellar::class,
        ],
        DealCashbackCancelState::class        => [
            SendCashbackCancelAlert::class,
        ],
        DealCashbackDoneState::class          => [
            CashbackTransfer::class,
        ],
        StellarLogEvent::class                => [
            LogStellarRequest::class,
            LogErrorStellarRequest::class,
        ],
        StartCompareBalanceWithStellar::class => [
            CompareBalanceWithStellar::class,
        ],
        BankSaved::class                      => [
            FlushDirectoryCache::class,
        ],
        CountrySaved::class                   => [
            FlushDirectoryCache::class,
        ],
        CryptoCurrencySaved::class            => [
            FlushDirectoryCache::class,
        ],
        CurrencySaved::class                  => [
            FlushDirectoryCache::class,
        ],
        PaymentSystemSaved::class             => [
            FlushDirectoryCache::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
