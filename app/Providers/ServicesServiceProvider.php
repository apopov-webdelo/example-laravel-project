<?php

namespace App\Providers;

use App\Contracts\Balance\Admin\Dashboard\TotalSummaryStorageContract;
use App\Contracts\Chat\StoreMessageToDealServiceContract;
use App\Contracts\DealCashback\DealCashbackBreakServiceContract;
use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Contracts\DealCashback\DealCashbackProcessServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Contracts\Images\ImageUploadServiceContract;
use App\Contracts\Images\ImageUploadStorageServiceContract;
use App\Contracts\PasswordResetContract;
use App\Contracts\Repositories\StellarLogRepoContract;
use App\Contracts\Services\Auth\TelescopePathServiceContract;
use App\Contracts\Services\Auth\TokenServiceContract;
use App\Contracts\Services\Deal\CancellationDealServiceContract;
use App\Contracts\Services\Deal\FinishingDealServiceContract;
use App\Contracts\Services\Deal\VerifyDealServiceContract;
use App\Contracts\Services\Directory\DirectoryServiceContract;
use App\Contracts\Services\Idea\IdeaStorageContract;
use App\Contracts\Services\Review\DeletingReviewServiceContract;
use App\Contracts\Services\Review\ResetingWithBlockReviewServiceContract;
use App\Contracts\Services\Statistics\Deals\AverageFinishDealsTimeStatisticsContract;
use App\Contracts\Services\Statistics\Deals\CurrenciesStatisticsContract;
use App\Contracts\Services\Statistics\Deals\FinishDealTimeStatisticsContract;
use App\Contracts\Services\Statistics\Deals\OperationsStatisticsContract;
use App\Contracts\Services\Statistics\Deals\PaymentSystemsStatisticsContract;
use App\Contracts\Services\User\LockServiceContract;
use App\Contracts\Services\User\LockTradeServiceContract;
use App\Contracts\Services\User\Statistics\UpdateReviewsCoefficientStorageContract;
use App\Contracts\Services\User\Statistics\UserStatisticsContract;
use App\Contracts\Services\User\SyncPermissionsServiceContract;
use App\Contracts\Services\User\UnlockServiceContract;
use App\Contracts\Services\User\UnlockTradeServiceContract;
use App\Contracts\Services\User\UpdateEmailServiceContract;
use App\Contracts\Services\User\UpdateReviewsCoefficientServiceContract;
use App\Contracts\Services\User\UserChangesServiceContract;
use App\Contracts\Statistics\Ads\AdStatisticsContract;
use App\Contracts\Utils\Session\SessionStorageContract;
use App\Http\Controllers\Admin\MessageController as AdminMessageController;
use App\Http\Controllers\Client\Chat\MessageController as ClientMessageController;
use App\Repositories\DealCashback\DealCashbackRepo;
use App\Repositories\StellarLog\StellarLogRepo;
use App\Services\Auth\TelescopePathService;
use App\Services\Auth\TokenService;
use App\Services\Balance\Admin\Dashboard\TotalSummaryStorage;
use App\Services\Chat\Admin\StoreMessageToDealService as AdminStoreMessageToDealService;
use App\Services\Chat\Message\StoreMessageToDealService as ClientStoreMessageToDealService;
use App\Services\Deal\CancellationDealService;
use App\Services\Deal\FinishingDealService;
use App\Services\Deal\VerifyDealService;
use App\Services\DealCashback\DealCashbackBreakService;
use App\Services\DealCashback\DealCashbackCommission;
use App\Services\DealCashback\DealCashbackProcessService;
use App\Services\Directory\DirectoryService;
use App\Services\IdeaStorageService;
use App\Services\Images\ImageUploadService;
use App\Services\Images\ImageUploadStorageService;
use App\Services\PasswordResetService;
use App\Services\Review\DeletingReviewService;
use App\Services\Review\ResetingWithBlockReviewService;
use App\Services\Statistics\Ads\AdStatistics;
use App\Services\Statistics\Deals\AverageFinishDealsTimeStatistics;
use App\Services\Statistics\Deals\CurrenciesStatistics;
use App\Services\Statistics\Deals\FinishDealTimeStatistics;
use App\Services\Statistics\Deals\OperationsStatistics;
use App\Services\Statistics\Deals\PaymentSystemsStatistics;
use App\Services\User\LockService;
use App\Services\User\LockTradeService;
use App\Services\User\Security\UpdateEmailService;
use App\Services\User\Statistics\UpdateReviewsCoefficientService;
use App\Services\User\Statistics\UpdateReviewsCoefficientStorage;
use App\Services\User\Statistics\UserStatisticsService;
use App\Services\User\SyncPermissionsService;
use App\Services\User\UnlockService;
use App\Services\User\UnlockTradeService;
use App\Services\User\UserChangesService;
use App\Utils\Session\SessionStorage;
use Illuminate\Support\ServiceProvider;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserStatisticsContract::class, UserStatisticsService::class);
        $this->app->bind(AdStatisticsContract::class, AdStatistics::class);
        $this->app->bind(LockServiceContract::class, LockService::class);
        $this->app->bind(UnlockServiceContract::class, UnlockService::class);
        $this->app->bind(LockTradeServiceContract::class, LockTradeService::class);
        $this->app->bind(UnlockTradeServiceContract::class, UnlockTradeService::class);
        $this->app->bind(SyncPermissionsServiceContract::class, SyncPermissionsService::class);
        $this->app->bind(TokenServiceContract::class, TokenService::class);

        /** Statistics Services */
        $this->app->bind(PaymentSystemsStatisticsContract::class, PaymentSystemsStatistics::class);
        $this->app->bind(CurrenciesStatisticsContract::class, CurrenciesStatistics::class);
        $this->app->bind(OperationsStatisticsContract::class, OperationsStatistics::class);
        $this->app->bind(FinishDealTimeStatisticsContract::class, FinishDealTimeStatistics::class);
        $this->app->bind(
            AverageFinishDealsTimeStatisticsContract::class,
            AverageFinishDealsTimeStatistics::class
        );

        $this->app->bind(IdeaStorageContract::class, IdeaStorageService::class);

        $this->app
            ->when(AdminMessageController::class)
            ->needs(StoreMessageToDealServiceContract::class)
            ->give(AdminStoreMessageToDealService::class);

        $this->app
            ->when(ClientMessageController::class)
            ->needs(StoreMessageToDealServiceContract::class)
            ->give(ClientStoreMessageToDealService::class);

        $this->app->bind(PasswordResetContract::class, PasswordResetService::class);

        $this->app->bind(ImageUploadServiceContract::class, ImageUploadService::class);
        $this->app->bind(ImageUploadStorageServiceContract::class, ImageUploadStorageService::class);
        $this->app->bind(SessionStorageContract::class, SessionStorage::class);
        $this->app->bind(TelescopePathServiceContract::class, TelescopePathService::class);

        $this->app->bind(UserChangesServiceContract::class, UserChangesService::class);
        $this->app->bind(UpdateReviewsCoefficientServiceContract::class, UpdateReviewsCoefficientService::class);
        $this->app->bind(UpdateReviewsCoefficientStorageContract::class, UpdateReviewsCoefficientStorage::class);

        $this->app->bind(DeletingReviewServiceContract::class, DeletingReviewService::class);
        $this->app->bind(ResetingWithBlockReviewServiceContract::class, ResetingWithBlockReviewService::class);

        $this->app->bind(DealCashbackCommissionStorageContract::class, DealCashbackCommission::class);
        $this->app->bind(DealCashbackRepoContract::class, DealCashbackRepo::class);
        $this->app->bind(DealCashbackProcessServiceContract::class, DealCashbackProcessService::class);
        $this->app->bind(DealCashbackBreakServiceContract::class, DealCashbackBreakService::class);

        $this->app->bind(DirectoryServiceContract::class, DirectoryService::class);

        $this->app->bind(StellarLogRepoContract::class, StellarLogRepo::class);
        $this->app->bind(UpdateEmailServiceContract::class, UpdateEmailService::class);

        $this->app->bind(VerifyDealServiceContract::class, VerifyDealService::class);

        $this->app->bind(CancellationDealServiceContract::class, CancellationDealService::class);

        $this->app->bind(FinishingDealServiceContract::class, FinishingDealService::class);

        $this->app->bind(TotalSummaryStorageContract::class, TotalSummaryStorage::class);
    }
}
