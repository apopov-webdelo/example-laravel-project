<?php

namespace App\Providers;

use App\Contracts\ActivityLog\ActivityLogServiceContract;
use App\Contracts\ActivityLog\FactoryContract;
use App\Factories\ActivityLogFactory;
use App\Services\ActivityLog\ActivityLogService;
use Illuminate\Support\ServiceProvider;

class ActivityLogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ActivityLogServiceContract::class, ActivityLogService::class);
        $this->app->bind(FactoryContract::class, ActivityLogFactory::class);
    }
}
