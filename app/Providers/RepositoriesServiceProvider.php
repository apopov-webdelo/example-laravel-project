<?php

namespace App\Providers;

use App\Contracts\Repositories\FullDirectoryRepoContract;
use App\Contracts\Repositories\PayInTransactionRepoContract;
use App\Contracts\Repositories\PayOutRepoContract;
use App\Repositories\Balance\PayInTransactionRepo;
use App\Repositories\Balance\PayOutRepo;
use App\Repositories\Directory\CachedFullDirectoryRepo;
use App\Repositories\Directory\FullDirectoryRepo;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PayInTransactionRepoContract::class, PayInTransactionRepo::class);
        $this->app->bind(PayOutRepoContract::class, PayOutRepo::class);

        $this->app->bind(FullDirectoryRepoContract::class, FullDirectoryRepo::class);

        $this->app->singleton(CachedFullDirectoryRepo::class, function () {
            $cachingRepo = new CachedFullDirectoryRepo(
                app(FullDirectoryRepoContract::class),
                $this->app['cache.store']
            );

            return $cachingRepo;
        });
    }
}
