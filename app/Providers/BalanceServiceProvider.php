<?php

namespace App\Providers;

use App\Contracts\Balance\CashbackServiceContract;
use App\Contracts\Balance\DepositServiceContract;
use App\Contracts\Balance\PayIn\PayInProcessServiceContract;
use App\Contracts\Balance\PayOut\PayOutCommissionServiceContract;
use App\Contracts\Balance\PayOut\PayOutProcessServiceContract;
use App\Contracts\Balance\PayOut\PayOutRegisterServiceContract;
use App\Contracts\Balance\WithdrawServiceContract;
use App\Models\Balance\PayIn\PayInRegisterServiceContract;
use App\Services\Balance\CashbackService;
use App\Services\Balance\DepositService;
use App\Services\Balance\PayIn\PayInProcessService;
use App\Services\Balance\PayIn\PayInRegisterService;
use App\Services\Balance\PayOut\PayOutFixedCommissionService;
use App\Services\Balance\PayOut\PayOutProcessService;
use App\Services\Balance\PayOut\PayOutRegisterService;
use App\Services\Balance\WithdrawService;
use Illuminate\Support\ServiceProvider;

class BalanceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WithdrawServiceContract::class, WithdrawService::class);
        $this->app->bind(DepositServiceContract::class, DepositService::class);
        $this->app->bind(CashbackServiceContract::class, CashbackService::class);

        /* PayIn Module */
        $this->app->bind(PayInRegisterServiceContract::class, PayInRegisterService::class);
        $this->app->bind(PayInProcessServiceContract::class, PayInProcessService::class);

        /* PayOut Module */
        $this->app->bind(PayOutCommissionServiceContract::class, PayOutFixedCommissionService::class);
        $this->app->bind(PayOutRegisterServiceContract::class, PayOutRegisterService::class);
        $this->app->bind(PayOutProcessServiceContract::class, PayOutProcessService::class);
    }
}
