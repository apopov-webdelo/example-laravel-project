<?php

namespace App\Providers\Social;

use App\Models\User\User;
use App\Repositories\User\UserRepo;
use App\Services\User\SocialRegistrationService;
use Laravel\Socialite\Facades\Socialite;

abstract class AbstractSocialServiceProvider
{
    protected $provider;
    protected $providerShort;
    protected $providerName;

    /**
     *  Create a new SocialServiceProvider instance
     *
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->providerName = str_replace(
            'serviceprovider',
            '',
            strtolower((new \ReflectionClass($this))->getShortName())
        );
        $this->provider = Socialite::driver($this->providerName);
    }

    /**
     *  Handle response and register/return user
     *
     * @return User
     * @throws \Exception
     */
    public function handle()
    {
        if (!$this->providerShort) {
            throw new \InvalidArgumentException('Login service provider must not have empty $providerShort');
        }

        $userSocial = $this->provider->stateless()->user();

        /* @var UserRepo $userRepo */
        $userRepo = app(UserRepo::class);

        $user = $userRepo->filterByProviderId($userSocial->getId())
                         ->filterByProvider($this->providerName)
                         ->take()
                         ->first();

        if ($user) {
            return $user;
        }

        return $this->register([
            'login'       => $this->generateLogin($this->providerShort, $userSocial->getId()),
            'provider_id' => $userSocial->getId(),
            'provider'    => $this->providerName,
        ]);
    }

    /**
     *  Register the user
     *
     * @param  array $userData
     *
     * @return User $user
     * @throws \Exception
     */
    protected function register(array $userData)
    {
        return app(SocialRegistrationService::class)->store($userData);
    }

    /**
     *  Redirect the user to provider authentication page
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return $this->provider->redirect();
    }

    /**
     * @param string $providerShort
     * @param        $id
     *
     * @return string
     */
    protected function generateLogin(string $providerShort, $id)
    {
        return $providerShort . '-' . $id . str_random(3);
    }
}
