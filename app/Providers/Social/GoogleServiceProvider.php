<?php

namespace App\Providers\Social;

class GoogleServiceProvider extends AbstractSocialServiceProvider
{
    protected $providerShort = 'ggl';
}
