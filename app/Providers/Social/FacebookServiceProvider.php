<?php

namespace App\Providers\Social;

class FacebookServiceProvider extends AbstractSocialServiceProvider
{
    protected $providerShort = 'fb';
}
