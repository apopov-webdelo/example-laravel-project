<?php

namespace App\Providers;

use App\Models\Ad\Ad;
use App\Models\Balance\Balance;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Deal\Deal;
use App\Models\User\Note;
use App\Models\User\Review;
use App\Policies\AdPolicy;
use App\Policies\BalancePolicy;
use App\Policies\ChatPolicy;
use App\Policies\DealPolicy;
use App\Policies\MessagePolicy;
use App\Policies\NotePolicy;
use App\Policies\ReviewPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',

        Ad::class      => AdPolicy::class,
        Note::class    => NotePolicy::class,
        Deal::class    => DealPolicy::class,
        Chat::class    => ChatPolicy::class,
        Message::class => MessagePolicy::class,
        Balance::class => BalancePolicy::class,
        Review::class  => ReviewPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::personalAccessClientId(config('auth.personal_client_id'));
    }
}
