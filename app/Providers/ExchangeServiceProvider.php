<?php

namespace App\Providers;

use App\Contracts\Exchange\Crypto\FactoryContract;
use App\Contracts\Exchange\Fiat\FiatFactoryContract;
use App\Factories\CryptoExchangeFactory;
use App\Factories\FiatExchangeFactory;
use Illuminate\Support\ServiceProvider;

class ExchangeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FiatFactoryContract::class, FiatExchangeFactory::class);
        $this->app->bind(FactoryContract::class, CryptoExchangeFactory::class);
    }
}
