<?php

namespace App\Providers;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Facade\EmergencyAlertFactoryContract;
use App\Contracts\GeoLocatorContract;
use App\Contracts\RobotFactoryContract;
use App\Contracts\Services\Messaging\Telegram\TelegramSendMessageContract;
use App\Factories\EmergencyAlertFactory;
use App\Factories\RobotFactory;
use App\Models\User\User;
use App\Services\GeoIP\TorannGeoIP;
use App\Services\Messaging\Telegram\TelegramSendMessageService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::serializeUsing(function (Carbon $timestamp) {
            return $timestamp->format(config('app.date_format'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RobotFactoryContract::class, RobotFactory::class);

        $this->app->bind(AuthenticatedContract::class, function ($app) {
            if ($admin = auth()->guard('api-admin')->user()) {
                return $admin;
            }
            return (config('app.noop_user'))
                ? User::find(1)
                : auth()->user();
        });

        $this->app->bind(GeoLocatorContract::class, TorannGeoIP::class);

        $this->app->bind(ClientInterface::class, Client::class);

        $this->app->bind(EmergencyAlertFactoryContract::class, EmergencyAlertFactory::class);

        $this->app->bind(TelegramSendMessageContract::class, TelegramSendMessageService::class);
    }
}
