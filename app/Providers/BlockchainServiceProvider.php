<?php

namespace App\Providers;

use App\Contracts\Bcrypt\ModelBcKeysRepoContract;
use App\Contracts\Blockchain\ModelWalletsRepoContract;
use App\Contracts\DealCashback\DealCashbackCancelServiceContract;
use App\Contracts\DealCashback\DealCashbackDoneServiceContract;
use App\Contracts\Services\Blockchain\From\Deal\DealCancelContract as FromDealCancelContract;
use App\Contracts\Services\Blockchain\From\Deal\DealCreationErrorContract;
use App\Contracts\Services\Blockchain\From\Deal\DealFinishContract as FromDealFinishContract;
use App\Contracts\Services\Blockchain\From\Deal\DealVerifyContract;
use App\Contracts\Services\Blockchain\From\EmergencyLog\EmergencyLogHandlerContract;
use App\Contracts\Services\Blockchain\From\Transaction\DepositContract as FromDepositContract;
use App\Contracts\Services\Blockchain\From\Transaction\WithdrawContract as FromWithdrawContract;
use App\Contracts\Services\Blockchain\From\Transaction\WithdrawErrorContract;
use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackCancelContract;
use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackDoneContract;
use App\Contracts\Services\Blockchain\From\Wallet\WalletCreationErrorContract;
use App\Contracts\Services\Blockchain\To\CreateKeysContract;
use App\Contracts\Services\Blockchain\To\Deal\DealCancelContract;
use App\Contracts\Services\Blockchain\To\Deal\DealCreateContract;
use App\Contracts\Services\Blockchain\To\Deal\DealFinishContract;
use App\Contracts\Services\Blockchain\To\Key\KeysStorageContract;
use App\Contracts\Services\Blockchain\To\Transaction\EmergencyWithdrawContract;
use App\Contracts\Services\Blockchain\To\Transaction\TransactionContract;
use App\Contracts\Services\Blockchain\To\Transaction\WithdrawContract;
use App\Contracts\Services\Blockchain\To\User\CreateUserContract;
use App\Contracts\Services\Blockchain\To\User\PartnerRewardedContract;
use App\Contracts\Services\Blockchain\To\User\ProcessDealCashbackContract;
use App\Contracts\Services\Blockchain\To\Wallet\AddEmergencyWalletContract;
use App\Contracts\Services\Blockchain\To\Wallet\CreateEmergencyWalletContract;
use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Repositories\Bcrypt\ModelBcKeysRepo;
use App\Repositories\Blockchain\ModelWalletsRepo;
use App\Services\Blockchain\Stellar\From\Deal\DealService as FromDealService;
use App\Services\Blockchain\Stellar\From\EmergencyLog\EmergencyLogHandler;
use App\Services\Blockchain\Stellar\From\Transaction\DepositHandler as FromDepositHandler;
use App\Services\Blockchain\Stellar\From\Transaction\WithdrawErrorHandler;
use App\Services\Blockchain\Stellar\From\Transaction\WithdrawHandler as FromWithdrawHandler;
use App\Services\Blockchain\Stellar\From\User\ProcessDealCashbackCancel;
use App\Services\Blockchain\Stellar\From\User\ProcessDealCashbackDone;
use App\Services\Blockchain\Stellar\From\Wallet\WalletService as FromWalletService;
use App\Services\Blockchain\Stellar\To\Deal\DealCancelHandler;
use App\Services\Blockchain\Stellar\To\Deal\DealCreateHandler;
use App\Services\Blockchain\Stellar\To\Deal\DealFinishHandler;
use App\Services\Blockchain\Stellar\To\Key\KeysService;
use App\Services\Blockchain\Stellar\To\Key\KeysStorage;
use App\Services\Blockchain\Stellar\To\Transaction\EmergencyWithdrawHandler;
use App\Services\Blockchain\Stellar\To\Transaction\TransactionHandler;
use App\Services\Blockchain\Stellar\To\Transaction\WithdrawHandler;
use App\Services\Blockchain\Stellar\To\User\PartnerRewardHandler;
use App\Services\Blockchain\Stellar\To\User\ProcessDealCashback;
use App\Services\Blockchain\Stellar\To\User\UserCreateHandler;
use App\Services\Blockchain\Stellar\To\Wallet\WalletService;
use App\Services\DealCashback\DealCashbackCancelService;
use App\Services\DealCashback\DealCashbackDoneService;
use Illuminate\Support\ServiceProvider;

class BlockchainServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //FROM BLOCKCHAIN REQUESTS
        $this->app->bind(FromDepositContract::class, FromDepositHandler::class);
        $this->app->bind(FromWithdrawContract::class, FromWithdrawHandler::class);

        $this->app->bind(FromDealCancelContract::class, FromDealService::class);
        $this->app->bind(FromDealFinishContract::class, FromDealService::class);
        $this->app->bind(DealVerifyContract::class, FromDealService::class);

        $this->app->bind(DealCreationErrorContract::class, FromDealService::class);
        $this->app->bind(WalletCreationErrorContract::class, FromWalletService::class);
        $this->app->bind(WithdrawErrorContract::class, WithdrawErrorHandler::class);

        $this->app->bind(ProcessDealCashbackCancelContract::class, ProcessDealCashbackCancel::class);
        $this->app->bind(DealCashbackCancelServiceContract::class, DealCashbackCancelService::class);
        $this->app->bind(DealCashbackDoneServiceContract::class, DealCashbackDoneService::class);
        $this->app->bind(ProcessDealCashbackDoneContract::class, ProcessDealCashbackDone::class);

        $this->app->bind(EmergencyLogHandlerContract::class, EmergencyLogHandler::class);

        //TO BLOCKCHAIN REQUESTS
        $this->app->bind(ModelBcKeysRepoContract::class, ModelBcKeysRepo::class);
        $this->app->bind(ModelWalletsRepoContract::class, ModelWalletsRepo::class);
        $this->app->bind(CreateUserContract::class, UserCreateHandler::class);
        $this->app->bind(PartnerRewardedContract::class, PartnerRewardHandler::class);
        $this->app->bind(DealCancelContract::class, DealCancelHandler::class);
        $this->app->bind(DealFinishContract::class, DealFinishHandler::class);
        $this->app->bind(DealCreateContract::class, DealCreateHandler::class);
        $this->app->bind(WalletContract::class, WalletService::class);
        $this->app->bind(CreateEmergencyWalletContract::class, WalletService::class);
        $this->app->bind(AddEmergencyWalletContract::class, WalletService::class);
        $this->app->bind(EmergencyWithdrawContract::class, EmergencyWithdrawHandler::class);
        $this->app->bind(WithdrawContract::class, WithdrawHandler::class);
        $this->app->bind(CreateKeysContract::class, KeysService::class);
        $this->app->bind(KeysStorageContract::class, KeysStorage::class);
        $this->app->bind(TransactionContract::class, TransactionHandler::class);
        $this->app->bind(ProcessDealCashbackContract::class, ProcessDealCashback::class);
    }
}
