<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/13/18
 * Time: 12:49
 */

namespace App\Providers\BotMan;

use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\SymfonyCache;
use BotMan\BotMan\Container\LaravelContainer;
use BotMan\BotMan\Storages\Drivers\FileStorage;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class BotManServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('botman', function ($app) {
            $storage = new FileStorage(storage_path('botman'));
            $adapter = new FilesystemAdapter();

            $botman = BotManFactory::create(
                config('botman', []),
                new SymfonyCache($adapter),
                $app->make('request'),
                $storage
           );

            $botman->setContainer(new LaravelContainer($this->app));

            return $botman;
        });
    }
}