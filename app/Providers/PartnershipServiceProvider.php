<?php

namespace App\Providers;

use App\Contracts\Partnership\PartnerCodeGeneratorContract;
use App\Contracts\Partnership\PartnerRegisterContract;
use App\Contracts\Partnership\PartnerRepoContract;
use App\Contracts\Partnership\Partnership2ndLineStrategyContract;
use App\Contracts\Partnership\PartnershipCommissionStrategyContract;
use App\Contracts\Partnership\PartnershipPayoutStrategyContract;
use App\Contracts\Partnership\PartnershipProcessingContract;
use App\Contracts\Partnership\ReferralRegisterContract;
use App\Contracts\Partnership\ReferralRepoContract;
use App\Repositories\Partnership\PartnerRepo;
use App\Repositories\Partnership\ProgramRepo;
use App\Repositories\Partnership\ProgramRepoContract;
use App\Repositories\Partnership\ReferralRepo;
use App\Services\Partnership\Commission\Strategies\Partnership1stLineCommissionStrategy;
use App\Services\Partnership\Commission\Strategies\Partnership2ndLineCommissionStrategy;
use App\Services\Partnership\PartnerCodeGenerator;
use App\Services\Partnership\PartnerRegisterService;
use App\Services\Partnership\PartnershipProcessingService;
use App\Services\Partnership\Payout\BalanceTransactionPayoutService;
use App\Services\Partnership\ReferralRegisterService;
use Illuminate\Support\ServiceProvider;

class PartnershipServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PartnerCodeGeneratorContract::class, PartnerCodeGenerator::class);
        $this->app->bind(PartnerRepoContract::class, PartnerRepo::class);
        $this->app->bind(ReferralRepoContract::class, ReferralRepo::class);
        $this->app->bind(ProgramRepoContract::class, ProgramRepo::class);

        $this->app->bind(PartnerRegisterContract::class, PartnerRegisterService::class);
        $this->app->bind(ReferralRegisterContract::class, ReferralRegisterService::class);

        $this->app->bind(PartnershipCommissionStrategyContract::class, Partnership1stLineCommissionStrategy::class);
        $this->app->bind(Partnership2ndLineStrategyContract::class, Partnership2ndLineCommissionStrategy::class);
        $this->app->bind(PartnershipProcessingContract::class, PartnershipProcessingService::class);
        $this->app->bind(PartnershipPayoutStrategyContract::class, BalanceTransactionPayoutService::class);
    }
}
