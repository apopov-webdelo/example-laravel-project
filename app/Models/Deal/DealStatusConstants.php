<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/6/18
 * Time: 15:35
 */

namespace App\Models\Deal;

/**
 * Interface with key deal constants for Deal Statuses
 *
 * @package App\Models\Deal
 */
interface DealStatusConstants
{
    const VERIFICATION = 7;
    const VERIFIED     = 1;
    const PAID         = 2;
    const FINISHING    = 9;
    const FINISHED     = 3;
    const CANCELLATION = 8;
    const AUTOCANCELED = 4;
    const CANCELED     = 5;
    const IN_DISPUTE   = 6;

    /**
     * All deal statuses list
     */
    const STATUSES = [
        self::VERIFICATION,
        self::VERIFIED,
        self::PAID,
        self::FINISHING,
        self::FINISHED,
        self::CANCELLATION,
        self::AUTOCANCELED,
        self::CANCELED,
        self::IN_DISPUTE,
    ];

    /**
     * Statuses list for active deal
     */
    const ACTIVE_STATUSES = [
        self::VERIFICATION,
        self::VERIFIED,
        self::PAID,
        self::IN_DISPUTE,
    ];

    /**
     * Statuses list for inactive deal
     */
    const INACTIVE_STATUSES = [
        self::FINISHED,
        self::FINISHING,
        self::CANCELLATION,
        self::CANCELED,
        self::AUTOCANCELED,
    ];
}
