<?php

namespace App\Models\Deal;

use App\Models\BaseModel;

/**
 * Class DealStatus
 *
 * @package App\Models\Deal
 * @property int $id
 * @property string|null $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deal\DealStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deal\DealStatus whereTitle($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deal\DealStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deal\DealStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Deal\DealStatus query()
 */
class DealStatus extends BaseModel
{
    protected $table = 'deals_statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
        ];
    }
}
