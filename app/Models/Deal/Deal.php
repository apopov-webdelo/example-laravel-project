<?php

namespace App\Models\Deal;

use App\Contracts\Bcrypt\ModelBcKeysContract;
use App\Contracts\Blockchain\ModelWalletsContract;
use App\Models\Ad\Ad;
use App\Models\BaseModel;
use App\Models\Chat\Chat;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\User\Review;
use App\Models\User\User;
use App\Traits\Authorable;
use App\Traits\Bcrypt\HasModelBcKeys;
use App\Traits\Blockchain\HasModelWallets;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Deal
 *
 * @package App\Models\Deal
 * @property User                                                                        $author
 * @property Chat                                                                        $chat
 * @property Ad                                                                          $ad
 * @property DealStatus                                                                  $status
 * @property DealStatus                                                                  $prev_status
 * @property float                                                                       $price
 * @property float                                                                       $fiat_amount
 * @property float                                                                       $crypto_amount
 * @property float                                                                       $crypto_amount_in_usd
 * @property float                                                                       $commission
 * @property int                                                                         $time
 * @property Carbon                                                                      $created_at
 * @property Carbon                                                                      $updated_at
 * @property int                                                                         $id
 * @property int                                                                         $ad_id
 * @property int                                                                         $author_id
 * @property string|null                                                                 $conditions
 * @property Carbon                                                                      $deadline
 * @property-read Currency                                                               $currency
 * @property-read CryptoCurrency                                                         $cryptoCurrency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chat\Chat[]       $chats
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Review[]     $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|DealStatus[] $statuses
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereAdId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereConditions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCryptoAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereFiatAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereUpdatedAt($value)
 * @mixin \Illuminate\Database\Eloquent\
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Bcrypt\ModelBcKey[] $keys
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Blockchain\ModelWallet[] $wallets
 * @method static \Illuminate\Database\Eloquent\Builder|Deal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCryptoAmountInUsd($value)
 */
class Deal extends BaseModel implements ModelBcKeysContract, ModelWalletsContract
{
    use Authorable, HasModelBcKeys, HasModelWallets;

    protected $table = 'deals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_id',
        'author_id',
        'price',
        'fiat_amount',
        'crypto_amount',
        'commission',
        'time',
        'conditions',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'status',
        'prev_status',
        'chat',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reviews()
    {
        return $this->belongsToMany(Review::class, 'deals_has_reviews', 'deal_id', 'review_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chats()
    {
        return $this->belongsToMany(Chat::class, 'chat_has_deals', 'deal_id', 'chat_id');
    }

    /**
     * @return Chat
     */
    public function chat()
    {
        return $this->chats()->first() ? $this->chats()->first()->load('recipients') : $this->chats()->first();
    }

    /**
     * @return Chat
     */
    public function getChatAttribute()
    {
        return $this->chat();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ad()
    {
        //TODO: Build fast decision for deals wit removed ads
        return $this->hasOne(Ad::class, 'id', 'ad_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function statuses()
    {
        return $this->belongsToMany(
            DealStatus::class,
            'deals_has_statuses',
            'deal_id',
            'status_id'
        )->withPivot(['created_at', 'updated_at']);
    }

    /**
     * Return latest (current) deal status
     *
     * @return DealStatus
     */
    public function getCurrentStatus(): DealStatus
    {
        return $this->getStatusAttribute();
    }

    /**
     * @return Model|mixed|null|object|static
     */
    public function getStatusAttribute()
    {
        return $this->statuses()->orderBy('deals_has_statuses.id', 'desk')->first();
    }

    /**
     * @return Model|mixed|null|object|static
     */
    public function getPrevStatusAttribute()
    {
        return $this->statuses()->orderBy('deals_has_statuses.id', 'desk')->take(2)->latest();
    }

    /**
     * Check is user deal member
     *
     * @param User $user
     *
     * @return bool
     */
    public function isUserDealMember(User $user): bool
    {
        return $this->getDealMembers()->pluck('id')->contains($user->id);
    }

    /**
     * Return collection with deal members
     *
     * @return Collection
     */
    public function getDealMembers(): Collection
    {
        return new Collection([
            $this->author,
            $this->ad->author,
        ]);
    }

    /**
     * Check is user not deal member (reverse of function isUserDealMember())
     *
     * @param User $user
     *
     * @return bool
     */
    public function isUserNotDealMember(User $user): bool
    {
        return !$this->isUserDealMember($user);
    }

    /**
     * Return another one deal member
     *
     * @param User $user
     *
     * @return User
     */
    public function getAnotherMember(User $user): User
    {
        return $this->getSeller()->id == $user->id
            ? $this->getBuyer()
            : $this->getSeller();
    }

    /**
     * Check is user is seller in that deal
     *
     * @param User $user
     * @param bool $strict Is user not member in that deal will throw exception
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function isSeller(User $user, $strict = true): bool
    {
        if ($strict && $this->isUserNotDealMember($user)) {
            throw new \Exception(
                'User is not deal member! (User ID: ' . $user->id . ', 
                Members: ' . $this->getDealMembers()->pluck('id')->implode(',') . ')'
            );
        }

        return $this->getSeller()->id == $user->id;
    }

    /**
     * @return User
     */
    public function getSeller(): User
    {
        return $this->ad->is_sale ? $this->ad->author : $this->author;
    }

    /**
     * Check is user is buyer in that deal
     *
     * @param User $user
     * @param bool $strict Is user not member in that deal will throw exception
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function isBuyer(User $user, $strict = true): bool
    {
        if ($strict && $this->isUserNotDealMember($user)) {
            throw new \Exception('User is not deal member!');
        }

        return $this->getBuyer()->id == $user->id;
    }

    /**
     * @return User
     */
    public function getBuyer(): User
    {
        return $this->ad->is_sale ? $this->author : $this->ad->author;
    }

    /**
     * @return User
     */
    public function getOffer(): User
    {
        return $this->ad->author;
    }

    /**
     * @return User
     */
    public function getClient(): User
    {
        return $this->author;
    }

    /**
     * @return bool
     */
    public function isClientSeller(): bool
    {
        return $this->getClient()->id == $this->getSeller()->id;
    }

    /**
     * @return bool
     */
    public function isClientNotSeller(): bool
    {
        return false == $this->isClientSeller();
    }

    /**
     * @return bool
     */
    public function isClientBuyer(): bool
    {
        return $this->isClientNotSeller();
    }

    /**
     * @return bool
     */
    public function isClientNotBuyer(): bool
    {
        return $this->isClientNotSeller();
    }

    /**
     * @return bool
     */
    public function isOfferSeller(): bool
    {
        return $this->getOffer()->id == $this->getSeller()->id;
    }

    /**
     * @return bool
     */
    public function isOfferNotSeller(): bool
    {
        return false == $this->isOfferSeller();
    }

    /**
     * @return bool
     */
    public function isOfferBuyer(): bool
    {
        return $this->isOfferNotSeller();
    }

    /**
     * @return bool
     */
    public function isOfferNotBuyer(): bool
    {
        return $this->isOfferSeller();
    }

    /**
     * Check is that user is deal author
     *
     * @param User $user
     *
     * @return bool
     */
    public function isDealAuthor(User $user): bool
    {
        return $this->author->id == $user->id;
    }

    /**
     * @return CryptoCurrency
     */
    protected function getCryptoCurrencyAttribute(): CryptoCurrency
    {
        return $this->getCryptoCurrency();
    }

    /**
     * Return CryptoCurrency for that deal
     *
     * @return CryptoCurrency
     */
    public function getCryptoCurrency(): CryptoCurrency
    {
        return $this->ad->cryptoCurrency;
    }

    /**
     * @return Currency
     */
    protected function getCurrencyAttribute(): Currency
    {
        return $this->getCurrency();
    }

    /**
     * Return fiat currency for that deal
     *
     * @return Currency
     */
    protected function getCurrency(): Currency
    {
        return $this->ad->currency;
    }

    /**
     * Return escrow amount for seller
     *
     * @return float
     */
    public function getEscrowAmount(): float
    {
        return $sum = $this->isClientSeller()
            ? $this->crypto_amount
            : $this->crypto_amount + $this->commission;
    }

    /**
     * Return deadline time
     *
     * @return Carbon
     */
    protected function getDeadlineAttribute()
    {
        return $this->created_at->addMinutes($this->time);
    }

    /**
     * Check is deal linked with protected Ad
     *
     * @return bool
     */
    public function isProtected()
    {
        return $this->ad->isProtected();
    }

    /**
     * Check is Deal was canceled
     *
     * @return bool
     */
    public function isCanceled()
    {
        return $this->getCurrentStatus()->id === DealStatusConstants::CANCELED;
    }

    /**
     * Check is Deal was finished
     *
     * @return bool
     */
    public function isFinished()
    {
        return $this->getCurrentStatus()->id === DealStatusConstants::FINISHED;
    }

    /**
     * Check is Deal was paid
     *
     * @return bool
     */
    public function isPaid()
    {
        return $this->getCurrentStatus()->id === DealStatusConstants::PAID;
    }

    /**
     * Check is Deal was disputed
     *
     * @return bool
     */
    public function isDisputed()
    {
        return $this->getCurrentStatus()->id === DealStatusConstants::IN_DISPUTE;
    }
}
