<?php

namespace App\Models\Chat;

use App\Models\Admin\Admin;
use App\Models\BaseModel;
use App\Models\Deal\Deal;
use App\Models\User\User;
use Illuminate\Support\Collection;

/**
 * App\Models\Chat\Chat
 *
 * @property int                                                                   $id
 * @property string                                                                $title
 * @property \Carbon\Carbon                                                        $created_at
 * @property \Carbon\Carbon                                                        $updated_at
 * @property int                                                                   $author_id
 * @property Collection                                                            $deal
 * @property-read \App\Models\User\User                                            $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deal\Deal[] $deals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[] $recipients
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $author_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin\Admin[] $adminRecipients
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chat\Message[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Chat whereAuthorType($value)
 */
class Chat extends BaseModel
{
    protected $table = 'chat';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'author_id',
        'author_type',
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function author()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function recipients()
    {
        return $this->morphedByMany(User::class, 'recipientable', 'chat_recipients');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function adminRecipients()
    {
        return $this->morphedByMany(Admin::class, 'recipientable', 'chat_recipients');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deals()
    {
        return $this->belongsToMany(Deal::class, 'chat_has_deals', 'chat_id', 'deal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deal()
    {
        return $this->deals()->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }


    /**
     * @return Message
     */
    public function lastMessage()
    {
        return $this->messages()->latest()->get()->first();
    }

    /**
     * Check if user is member in that chat room
     *
     * @param User $user
     *
     * @return bool
     */
    public function isUserChatMember(User $user): bool
    {
        return $this->recipients->contains($user);
    }
}
