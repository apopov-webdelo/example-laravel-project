<?php

namespace App\Models\Chat;

use App\Contracts\ImageableContract;
use App\Contracts\Images\ImageContract;
use App\Models\BaseModel;
use App\Models\Image\Image;
use App\Traits\Authorable;

/**
 * App\Models\Chat\Message
 *
 * @property int $id
 * @property string|null $message
 * @property ImageContract $image
 * @property int|null $chat_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $author_id
 * @property-read \App\Models\User\User|null $author
 * @property-read \App\Models\Chat\Chat $chat
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $author_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat\Message whereAuthorType($value)
 */
class Message extends BaseModel implements ImageableContract
{
    use Authorable;

    protected $table = 'chat_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'author_id',
        'author_type',
        'chat_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function author()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function chat()
    {
        return $this->hasOne(Chat::class, 'id', 'chat_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * @return Image
     */
    public function getImage(): ImageContract
    {
        return $this->image;
    }
}
