<?php

namespace App\Models\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\ReferralContract;
use App\Models\BaseModel;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Referral
 *
 * @property int $id
 * @property User $user
 * @property Partner $partner
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @package App\Models\Partnership
 * @property int $user_id
 * @property int $partner_id
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\Referral onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Referral whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\Referral withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\Referral withoutTrashed()
 * @mixin \Eloquent
 */
class Referral extends BaseModel implements ReferralContract
{
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    /**
     * {@inheritdoc}
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     *
     * @return PartnerContract
     */
    public function getPartner(): PartnerContract
    {
        return $this->partner;
    }

    /**
     * {@inheritdoc}
     *
     * @return Carbon
     */
    public function getRegistrationDate(): Carbon
    {
        return $this->created_at;
    }
}
