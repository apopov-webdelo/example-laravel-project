<?php

namespace App\Models\Partnership;

use App\Models\BaseModel;

/**
 * Class PartnershipProgramStatus
 *
 * @property int    $id
 * @property string $title
 * @package App\Models\PartnershipProgram
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgramStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgramStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgramStatus query()
 * @mixin \Eloquent
 */
class PartnershipProgramStatus extends BaseModel
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
        ];
    }
}
