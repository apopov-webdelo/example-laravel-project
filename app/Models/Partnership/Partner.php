<?php

namespace App\Models\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\PartnershipProgramContract;
use App\Models\BaseModel;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Partner
 *
 * @property int $id
 * @property int $user_id
 * @property User $user
 * @property PartnershipProgram $partnershipProgram
 * @property Collection $referrals
 * @package App\Models\Partnership
 * @property int $partnership_program_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\Partner onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner wherePartnershipProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\Partner whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\Partner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\Partner withoutTrashed()
 * @mixin \Eloquent
 */
class Partner extends BaseModel implements PartnerContract
{
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partnershipProgram()
    {
        return $this->belongsTo(PartnershipProgram::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referrals()
    {
        return $this->hasMany(Referral::class);
    }

    /**
     * Get user model for partner
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Get partnership program model
     *
     * @return PartnershipProgram
     */
    public function getPartnershipProgram(): PartnershipProgramContract
    {
        return $this->partnershipProgram;
    }

    /**
     * Get referrals collection
     *
     * @return Collection
     */
    public function getReferrals(): Collection
    {
        return $this->referrals;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->id;
    }
}
