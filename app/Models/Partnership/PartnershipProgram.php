<?php

namespace App\Models\Partnership;

use App\Contracts\Partnership\PartnershipCommissionStrategyContract;
use App\Contracts\Partnership\PartnershipProgramContract;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartnershipProgram
 *
 * @property int $id
 * @property PartnershipProgramStatus $status
 * @property Collection $partners
 * @property string $driver
 * @package App\Models\Directory
 * @property string $title
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\PartnershipProgram onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partnership\PartnershipProgram whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\PartnershipProgram withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partnership\PartnershipProgram withoutTrashed()
 * @mixin \Eloquent
 */
class PartnershipProgram extends BaseModel implements PartnershipProgramContract
{
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(PartnershipProgramStatus::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partners()
    {
        return $this->hasMany(Partner::class);
    }

    /**
     * @return PartnershipProgramStatus
     */
    public function getStatus(): PartnershipProgramStatus
    {
        return $this->status;
    }

    /**
     * @return Collection
     */
    public function getPartners(): Collection
    {
        return $this->partners;
    }

    /**
     * @return PartnershipCommissionStrategyContract
     */
    public function getCommissionDriver(): PartnershipCommissionStrategyContract
    {
        return app(config('app.partnership.'. $this->driver. '.program_driver'));
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getDriverAlias(): string
    {
        return $this->driver;
    }
}
