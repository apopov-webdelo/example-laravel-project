<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/22/18
 * Time: 14:02
 */

namespace App\Models\ActivityLog;

/**
 * Interface ActivityLogConstants
 *
 * @package App\Models\ActivityLog
 */
interface ActivityLogTypeConstants
{
    const ADS        = 'ads';
    const DEALS      = 'deals';
    const PAYMENTS   = 'payments';
    const BLOCKCHAIN = 'blockchain';
    const CLIENTS    = 'clients';
    const EMPLOYEES  = 'employees';
}
