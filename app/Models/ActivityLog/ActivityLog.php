<?php

namespace App\Models\ActivityLog;

use App\Models\Admin\Admin;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityLog
 *
 * @property int $id
 * @property string $message
 * @property string $level
 * @property string $type
 * @property Admin|User $authorable
 * @property int $authorable_id
 * @property string $authorable_type
 * @property Carbon $created_at
 * @package App\Models\ActivityLog
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereAuthorableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereAuthorableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityLog\ActivityLog whereType($value)
 * @mixin \Eloquent
 */
class ActivityLog extends Model
{
    public $timestamps = false;

    protected $table = 'activity_logs';

    protected $fillable = [
        'message',
        'level',
        'type',
        'authorable_id',
        'authorable_type'
    ];

    protected $dates = [ 'created_at' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function authorable()
    {
        return $this->morphTo();
    }
}
