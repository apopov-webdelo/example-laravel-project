<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/22/18
 * Time: 14:02
 */

namespace App\Models\ActivityLog;

/**
 * Interface ActivityLogLevelConstants
 *
 * @package App\Models\ActivityLog
 */
interface ActivityLogLevelConstants
{
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const DEBUG     = 'debug';
    const EMERGENCY = 'emergency';
    const ERROR     = 'error';
    const INFO      = 'info';
    const NOTICE    = 'notice';
    const WARNING   = 'warning';
}
