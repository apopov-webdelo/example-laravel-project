<?php

namespace App\Models\Blockchain;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ModelWallet
 *
 * @property int    $id
 * @property string $model
 * @property int    $model_id
 * @property string $alias
 * @property string $wallet_id
 * @mixin \Eloquent
 * @package App
 */
class ModelWallet extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model',
        'model_id',
        'alias',
        'wallet_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Get all of the owning keys models.
     */
    public function model()
    {
        return $this->morphTo('model', 'model');
    }
}
