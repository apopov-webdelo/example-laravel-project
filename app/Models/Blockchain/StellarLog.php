<?php

namespace App\Models\Blockchain;

use App\Models\BaseModel;
use Illuminate\Support\Carbon;

/**
 * Class StellarFromLog
 *
 * @property int    $id
 * @property string $type
 * @property string $method
 * @property string $route
 * @property array  $request
 * @property array  $response
 * @property int    $response_code
 * @property Carbon $created_at
 * @mixin \Eloquent
 * @package App
 */
class StellarLog extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'route',
        'request',
        'response',
        'method',
        'created_at'
    ];

    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getRequestAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getResponseAttribute($value)
    {
        return unserialize($value);
    }
}
