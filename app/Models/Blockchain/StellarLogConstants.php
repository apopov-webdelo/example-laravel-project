<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/12/19
 * Time: 12:30
 */

namespace App\Models\Blockchain;

/**
 * Interface StellarLogConstants
 *
 * @package App\Models\Blockchain
 */
interface StellarLogConstants
{
    const TYPE_FROM = 'from';
    const TYPE_TO = 'to';
}
