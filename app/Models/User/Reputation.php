<?php

namespace App\Models\User;

use App\Models\BaseModel;

/**
 * Class Reputation
 *
 * @package App\Models\User
 * @property int $id
 * @property int $user_id
 * @property int $rate
 * @property int $positive_count
 * @property int $neutral_count
 * @property int $negative_count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation whereNegativeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation wherePositiveCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Reputation whereRate($value)
 */
class Reputation extends BaseModel
{
    protected $table = 'reputation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'rate',
        'positive_count',
        'neutral_count',
        'negative_count'
    ];

    protected $hidden = [
        'id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @var array
     */
    protected $casts = [
        'neutral_count'  => 'number',
        'positive_count' => 'number',
        'rate'           => 'number',
        'negative_count' => 'number'
    ];
}
