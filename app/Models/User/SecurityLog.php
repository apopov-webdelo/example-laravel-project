<?php

namespace App\Models\User;

use App\Models\BaseModel;

/**
 * Class SecurityLog
 *
 * @package App\Models\User
 * @property int                                                $id
 * @property int                                                $sessionable_id
 * @property string                                             $sessionable_type
 * @property string                                             $ip
 * @property string|null                                        $country
 * @property string|null                                        $city
 * @property string|null                                        $os
 * @property string|null                                        $browser
 * @property string                                             $type
 * @property bool                                               $status
 * @property string                                             $status_desc
 * @property \Carbon\Carbon                                     $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereOs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereStatusDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereSessionableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session whereSessionableType($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $sessionable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Session\Session query()
 */
class SecurityLog extends BaseModel
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sessionable_id',
        'sessionable_type',
        'ip',
        'country',
        'city',
        'os',
        'browser',
        'status',
        'status_desc',
        'type',
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at'];

    /**
     * @var array
     */
    protected $casts = [
        'status' => 'bool',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function sessionable()
    {
        return $this->morphTo();
    }
}
