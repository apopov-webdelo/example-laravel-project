<?php

namespace App\Models\User;

use App\Models\BaseModel;
use Illuminate\Support\Carbon;

/**
 * Class UserChanges
 * @property null|Carbon $email_changed_at
 * @property null|Carbon $login_changed_at
 * @property int $user_id
 * @package App\Models\User
 */
class UserChanges extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_changes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
