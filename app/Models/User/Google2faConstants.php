<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/7/18
 * Time: 09:29
 */

namespace App\Models\User;

/**
 * Interface Google2faConstants
 *
 * @package App\Models\User
 */
interface Google2faConstants
{
    const STATUS_DEACTIVATED = 'deactivated';
    const STATUS_ACTIVATED   = 'activated';
    const STATUS_PAUSED      = 'paused';

    /**
     * Trust vatiants
     */
    const STATUSES = [
        self::STATUS_DEACTIVATED,
        self::STATUS_ACTIVATED,
        self::STATUS_PAUSED,
    ];
}