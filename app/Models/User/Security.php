<?php

namespace App\Models\User;

use App\Models\BaseModel;

/**
 * Class Security
 *
 * @package App\Models\User
 * @property int $id
 * @property int $user_id
 * @property bool $email_confirmed
 * @property string|null $email_access_token
 * @property string|null $email_inactive
 * @property string|null $email_confirm_token
 * @property bool $phone_confirmed
 * @property string|null $phone_access_token
 * @property string|null $phone_inactive
 * @property string|null $phone_confirm_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $google2fa_status
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailConfirmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereGoogle2faConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneConfirmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereUserId($value)
 * @mixin \Eloquent
 * @property bool $telegram_id_confirmed
 * @property string|null $telegram_id_confirm_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereGoogle2faStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereTelegramIdConfirmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereTelegramIdConfirmed($value)
 */
class Security extends BaseModel
{
    protected $table = 'security';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'email_confirmed',
        'email_access_token',
        'email_inactive',
        'email_confirm_token',
        'google2fa_status',
        'phone_confirmed',
        'phone_access_token',
        'phone_inactive',
        'phone_confirm_token',
        'telegram_id_confirmed',
        'telegram_id_confirm_token',
    ];

    protected $hidden = [
        'email_access_token',
        'email_inactive',
        'email_confirm_token',
        'phone_access_token',
        'phone_inactive',
        'phone_confirm_token',
        'telegram_inactive',
        'telegram_confirm_token',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'user_locked_at', 'user_locked_to'
    ];

    protected $casts = [
        'email_confirmed'  => 'bool',
        'phone_confirmed'  => 'bool',
        'telegram_id_confirmed'  => 'bool',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if google 2fa active
     *
     * @return bool
     */
    public function isGoogle2faActive() : bool
    {
        return $this->google2fa_status == Google2faConstants::STATUS_ACTIVATED;
    }

    /**
     * Check if google 2fa is paused
     *
     * @return bool
     */
    public function isGoogle2faPaused() : bool
    {
        return $this->google2fa_status == Google2faConstants::STATUS_PAUSED;
    }

    /**
     * Check if google 2fa is paused
     *
     * @return bool
     */
    public function isGoogle2faDeactivated() : bool
    {
        return $this->google2fa_status == Google2faConstants::STATUS_DEACTIVATED;
    }
}
