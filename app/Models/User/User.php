<?php

namespace App\Models\User;

use App\Contracts\ActivityLog\ActivityLoggable;
use App\Contracts\AuthenticatedContract;
use App\Contracts\Blockchain\ModelWalletsContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\ImageableContract;
use App\Contracts\Images\ImageContract;
use App\Contracts\LockableContract;
use App\Contracts\Services\Auth\TokenServiceContract;
use App\Contracts\SessionableContract;
use App\Contracts\TradeLockableContract;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Balance\Balance;
use App\Models\Chat\Chat;
use App\Models\Image\Image;
use App\Models\Permission\Permission;
use App\Models\Session\Session;
use App\Notifications\User\ResetPasswordNotification;
use App\Repositories\Balance\BalanceRepo;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Services\Auth\TokenService;
use App\Traits\Blockchain\HasModelWallets;
use App\Traits\MutatorSwitcher;
use App\Utils\User\Blacklist;
use App\Utils\User\FavoriteList;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package App\Models\User
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $telegram_id
 * @property string $one_signal_player_id
 * @property string $google2fa_secret
 * @property string $appeal
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Carbon $last_seen
 * @property Carbon $user_locked_at
 * @property Carbon $user_locked_to
 * @property Carbon $trade_locked_at
 * @property Carbon $trade_locked_to
 * @property Reputation $reputation
 * @property Statistic $statistic
 * @property Security $security
 * @property Settings $settings
 * @property Session $last_session
 * @property Session $lastSession
 * @property Image $image
 * @property Permission permissions
 * @property Collection $balance
 * @property Collection $reviewsIncome
 * @property Collection $reviewsOutcome
 * @property Collection $notesIncome
 * @property Collection $notesOutcome
 * @property Collection $sessions
 * @property UserChanges $changesAvailable
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereAppeal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereGoogle2faSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereLastSeen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereOneSignalPlayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereTradeLockedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereTradeLockedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUserLockedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUserLockedTo($value)
 * @mixin \Eloquent
 * @method static User find($id)
 * @property string|null $remember_token deprecated, model uses Passport tokens
 * @property string|null $api_token deprecated, model uses Passport tokens
 * @property int $role_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivityLog\ActivityLog[] $activityLogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chat\Chat[] $chats
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Blockchain\ModelWallet[] $wallets
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereTelegramId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\User withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\SecurityLog[] $securityLogs
 */
class User extends Authenticatable implements
    AuthenticatedContract,
    LockableContract,
    SessionableContract,
    ActivityLoggable,
    TradeLockableContract,
    ModelWalletsContract,
    ImageableContract
{
    use Notifiable,
        MutatorSwitcher,
        SoftDeletes,
        HasApiTokens,
        HasModelWallets;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'email',
        'phone',
        'password',
        'api_token',
        'google2fa_secret',
        'appeal',
        'deals_cancellation_percent',
        'last_seen',
        'telegram_id',
        'provider',
        'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'api_token',
        'password',
        'remember_token',
        'google2fa_secret'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'user_locked_at',
        'user_locked_to',
        'trade_locked_at',
        'trade_locked_to',
        'last_seen',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balance()
    {
        return $this->hasMany(Balance::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * Return balance model in that crypto currency
     *
     * @param CryptoCurrencyContract|string $cryptoCurrency
     * @param null|bool|string $lock LockForUpdate - true, ShareLock - false, Concrete locking - string
     *
     * @return Balance
     */
    public function getBalance($cryptoCurrency, $lock = null) : Balance
    {
        // Convert crypto currency code to object
        if (is_string($cryptoCurrency)) {
            /** @var CryptoCurrencyRepo $repo */
            $repo = app(CryptoCurrencyRepo::class);
            $cryptoCurrency = $repo->getByCode($cryptoCurrency);
        }

        /** @var BalanceRepo $repo */
        $repo = app(BalanceRepo::class);

        /** @var Balance $balance */
        $balance =  $repo->filterByUser($this)
                         ->filterByCryptoCurrency($cryptoCurrency)
                         ->take();
        if ($lock) {
            $balance->lock($lock);
        }

        /* TODO: Check this balance.default_balance_values expression.
                 Probably it is wrong. Look at RegistrationService on line 68
        */
        return $balance->first() ?? Balance::create(
            collect(config('balance.default_balance_values'))
                ->merge([
                    'user_id' => $this->id,
                    'crypto_currency_id' => $cryptoCurrency->id,
                ])->toArray()
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(Statistic::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function security()
    {
        return $this->hasOne(Security::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviewsIncome()
    {
        return $this->hasMany(Review::class, 'recipient_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviewsOutcome()
    {
        return $this->hasMany(Review::class, 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notesIncome()
    {
        return $this->hasMany(Note::class, 'recipient_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notesOutcome()
    {
        return $this->hasMany(Note::class, 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reputation()
    {
        return $this->hasOne(Reputation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function sessions()
    {
        return $this->morphMany(Session::class, 'sessionable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function securityLogs()
    {
        return $this->morphMany(SecurityLog::class, 'sessionable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function settings()
    {
        return $this->hasOne(Settings::class);
    }

    /**
     * @param Carbon|null $lockedTo
     *
     * @return bool
     */
    public function blockTrade(Carbon $lockedTo = null): bool
    {
        $this->trade_locked_at = Carbon::now();
        $this->trade_locked_to = $lockedTo;
        $this->save();

        return true;
    }

    /**
     * @return bool
     */
    public function unlockTrade(): bool
    {
        $this->trade_locked_at = null;
        $this->trade_locked_to = null;
        $this->save();

        return true;
    }

    /**
     * Check is trading available
     *
     * @return bool
     */
    public function isTradeAvailable() : bool
    {
        return !$this->isTradeBlocked();
    }

    /**
     * Check is trading was blocked for that user
     *
     * @return bool
     */
    public function isTradeBlocked() : bool
    {
        $tradeBlock = false;

        if ($this->trade_locked_at && $this->trade_locked_to) {
            $tradeBlock = now()->between($this->trade_locked_at, $this->trade_locked_to);
        } elseif ($this->trade_locked_at) {
            $tradeBlock = now()->greaterThanOrEqualTo($this->trade_locked_at);
        } elseif ($this->trade_locked_to) {
            $tradeBlock = now()->lessThanOrEqualTo($this->trade_locked_to);
        }

        return $tradeBlock || $this->isBlocked();
    }

    /**
     * Check is trading available
     *
     * @return bool
     */
    public function isUnlocked() : bool
    {
        return ! $this->isBlocked();
    }

    /**
     * Check is trading was blocked for that user
     *
     * @return bool
     */
    public function isBlocked() : bool
    {
        if ($this->user_locked_at && $this->user_locked_to) {
            return now()->between($this->user_locked_at, $this->user_locked_to);
        }

        if ($this->user_locked_at) {
            return now()->greaterThanOrEqualTo($this->user_locked_at);
        }

        if ($this->user_locked_to) {
            return now()->lessThanOrEqualTo($this->user_locked_to);
        }

        return false;
    }

    /**
     * @param Carbon      $lockedAt
     * @param Carbon|null $lockedTo
     *
     * @return bool
     */
    public function block(Carbon $lockedAt, Carbon $lockedTo = null): bool
    {
        $this->user_locked_at = $lockedAt;
        $this->user_locked_to = $lockedTo;
        $this->invalidateAllTokens();
        $this->save();

        return true;
    }

    /**
     * @return bool
     */
    public function unlock(): bool
    {
        $this->user_locked_at = null;
        $this->user_locked_to = null;
        $this->save();

        return true;
    }

    /**
     * Check is somebody has blocked opponent
     *
     * @param AuthenticatedContract $user
     * @return bool
     */
    public function isInFavoriteList(AuthenticatedContract $user) : bool
    {
        return app(FavoriteList::class)->isFavorite($this, $user);
    }

    /**
     * Check is users clean for business
     *
     * @param AuthenticatedContract $user
     * @return bool
     */
    public function isNotInFavoriteList(AuthenticatedContract $user) : bool
    {
        return app(FavoriteList::class)->isNotFavorite($this, $user);
    }

    /**
     * Check is somebody has blocked opponent
     *
     * @param AuthenticatedContract $user
     * @return bool
     */
    public function isInBlacklist(AuthenticatedContract $user) : bool
    {
        return app(Blacklist::class)->isBlocked($this, $user);
    }

    /**
     * Check is users clean for business
     *
     * @param AuthenticatedContract $user
     * @return bool
     */
    public function isNotInBlacklist(AuthenticatedContract $user) : bool
    {
        return app(Blacklist::class)->isClean($this, $user);
    }

    /**
     * Route notifications for the SmsAero channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSmsAero($notification)
    {
        return $this->phone;
    }

    /**
     * Route notifications for the Telegram channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForTelegram($notification)
    {
        return $this->telegram_id;
    }

    /**
     * Route notifications for the Web-Push channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForOneSignal($notification)
    {
        return $this->one_signal_player_id;
    }

    /**
     * Encrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return $this
     */
    public function setGoogle2faSecretAttribute(string $value)
    {
        $this->attributes['google2fa_secret'] = $this->useMutator ? encrypt($value) : $value;
        return $this;
    }

    /**
     * Decrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string|bool
     */
    public function getGoogle2faSecretAttribute($value)
    {
        return $value ? decrypt($value) : false;
    }

    /**
     * @return bool
     */
    public function isGoogle2faActive()
    {
        return $this->security->isGoogle2faActive();
    }

    /**
     * @return bool
     */
    public function isGoogle2faPaused()
    {
        return $this->security->isGoogle2faPaused();
    }

    /**
     * @return bool
     */
    public function isGoogle2faDeactivated()
    {
        return $this->security->isGoogle2faDeactivated();
    }

    /**
     * @return bool
     */
    public function isGoogle2faConnected()
    {
        return $this->google2fa_secret;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activityLogs()
    {
        return $this->morphMany(ActivityLog::class, 'authorable');
    }

    /**
     * Create new Passport token for user
     *
     * @return string
     * @throws \Exception
     */
    public function generateApiToken(): string
    {
        /* @var TokenService $tokenService */
        $tokenService = app(TokenServiceContract::class)->setUser($this);

        return $tokenService->createNew();
    }

    /**
     * invalidate Api Token
     */
    public function invalidateApiToken()
    {
        /* @var TokenService $tokenService */
        $tokenService = app(TokenServiceContract::class)->setUser($this);

        $tokenService->revokeForDevice();
    }

    /**
     * Revoke all usrs tokens on all devices
     */
    public function invalidateAllTokens()
    {
        /* @var TokenService $tokenService */
        $tokenService = app(TokenServiceContract::class)->setUser($this);

        $tokenService->revokeAll();
    }

    /**
     * Get all of the chats for the post.
     */
    public function chats()
    {
        return $this->morphToMany(Chat::class, 'recipientable', 'chat_recipients');
    }

    /**
     * @return ImageContract
     */
    public function getImage(): ImageContract
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $password
     *
     * @return AuthenticatedContract
     */
    public function setPassword(string $password): AuthenticatedContract
    {
        $this->password = bcrypt($password);
        $this->save();

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function changesAvailable()
    {
        return $this->hasOne(UserChanges::class);
    }
}
