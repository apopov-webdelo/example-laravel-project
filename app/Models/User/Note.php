<?php

namespace App\Models\User;

use App\Models\BaseModel;
use App\Traits\Authorable;

/**
 * Class User
 *
 * @package App\Models\User
 * @property int $id
 * @property string $note
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $author_id
 * @property int $recipient_id
 * @property-read \App\Models\User\User $author
 * @property-read \App\Models\User\User $recipient
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note whereRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Note query()
 */
class Note extends BaseModel
{
    use Authorable;

    protected $table = 'users_notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'author_id',
        'recipient_id'
    ];

    protected $hidden = [
        'author_id',
        'recipient_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id', 'id');
    }

    /**
     * @param string $note
     *
     * @return $this
     */
    public function setValue(string $note)
    {
        $this->note = $note;
        $this->save();
        return $this;
    }
}
