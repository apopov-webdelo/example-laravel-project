<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/7/18
 * Time: 09:29
 */

namespace App\Models\User;

/**
 * Interface ReviewConstants
 *
 * @package App\Models\User
 */
interface ReviewConstants
{
    const TRUST_POSITIVE       = 'positive';
    const TRUST_NEUTRAL        = 'neutral';
    const TRUST_NEGATIVE       = 'negative';
    const RATE_NEGATIVE_HIDDEN = 1;

    /**
     * Trust variants
     */
    const TRUSTS = [
        self::TRUST_POSITIVE,
        self::TRUST_NEUTRAL,
        self::TRUST_NEGATIVE,
    ];

    /**
     * Rates-trust relations
     */
    const RATES = [
        self::TRUST_POSITIVE => [3, 4],
        self::TRUST_NEUTRAL  => [2],
        self::TRUST_NEGATIVE => [0, self::RATE_NEGATIVE_HIDDEN],
    ];

    /**
     * Rates for public reviews
     */
    const PUBLIC_RATES = [
        'positive' => 4,
        'negative' => 0,
    ];
}
