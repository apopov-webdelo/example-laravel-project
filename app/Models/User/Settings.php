<?php

namespace App\Models\User;

use App\Models\BaseModel;
use App\Models\Directory\Country;
use App\Models\Directory\Language;

/**
 * Class Settings
 *
 * @package App\Models\User
 * @property int $id
 * @property int|null $user_id
 * @property bool $email_notification_required
 * @property bool $phone_notification_required
 * @property bool $telegram_notification_required
 * @property bool $deal_notification_required
 * @property bool $message_notification_required
 * @property bool $push_notification_required
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $country_id
 * @property int|null $language_id
 * @property int|null $deal_cancellation_max_percent
 * @property-read \App\Models\Directory\Country $country
 * @property-read \App\Models\Directory\Language $language
 * @property-read \App\Models\User\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereDealCancellationMaxPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereDealNotificationRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereEmailNotificationRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereMessageNotificationRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings wherePhoneNotificationRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings wherePushNotificationRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereTelegramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereTelegramNotificationRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings query()
 */
class Settings extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'deal_cancellation_max_percent',
        'email_notification_required',
        'phone_notification_required',
        'telegram_notification_required',
        'message_notification_required',
        'push_notification_required',
        'deal_notification_required',
        'country_id',
        'language_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'email_notification_required'    => 'bool',
        'phone_notification_required'    => 'bool',
        'telegram_notification_required' => 'bool',
        'message_notification_required'  => 'bool',
        'push_notification_required'     => 'bool',
        'deal_notification_required'     => 'bool',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }
}
