<?php

namespace App\Models\User;

use App\Models\BaseModel;
use App\Traits\Authorable;

/**
 * App\Models\User\Review
 *
 * @property int $id
 * @property string|null $message
 * @property int $rate
 * @property string $trust
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $author_id
 * @property int $recipient_id
 * @property int $deal_id
 * @property-read \App\Models\User\User $author
 * @property-read \App\Models\User\User $recipient
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereTrust($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereRecipientId($value)
 * @property-read \App\Models\User\Review $deal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review whereDealId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Review query()
 */
class Review extends BaseModel
{
    use Authorable;

    /**
     * @var string
     */
    protected $table = 'reviews';

    const TRUST_POSITIVE = 'positive';
    const TRUST_NEUTRAL  = 'neutral';
    const TRUST_NEGATIVE = 'negative';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'rate',
        'trust',
        'author_id',
        'recipient_id',
        'deal_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deal()
    {
        return $this->belongsTo(Review::class, 'deal_id', 'id');
    }
}
