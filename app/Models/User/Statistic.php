<?php

namespace App\Models\User;

use App\Models\BaseModel;

/**
 * Class User
 *
 * @package App\Models\User
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $user_id
 * @property int $total_turnover
 * @property float $deals_cancellation_percent
 * @property int $deals_finished_count
 * @property int $deals_canceled_count
 * @property int $deals_disputed_count
 * @property int $deals_paid_count
 * @property int $reviews_coefficient
 * @property int $deals_count
 * @property int $average_finish_deal_time
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereDealsCanceledCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereDealsCancellationPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereDealsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereDealsDisputedCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereDealsFinishedCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereDealsPaidCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereTotalTurnover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Statistic whereAverageFinishDealTime($value)
 */
class Statistic extends BaseModel
{
    protected $table = 'users_statistics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'total_turnover',
        'deals_cancellation_percent',
        'deals_count',
        'deals_finished_count',
        'deals_canceled_count',
        'deals_disputed_count',
        'deals_paid_count',
        'average_finish_deal_time',
        'user_id',
    ];

    protected $casts = [
        'deals_cancellation_percent' => 'float',
        'average_finish_deal_time'   => 'integer',
        'deals_count'                => 'integer',
        'deals_finished_count'       => 'integer',
        'deals_canceled_count'       => 'integer',
        'deals_disputed_count'       => 'integer',
        'deals_paid_count'           => 'integer',
        'reviews_coefficient'        => 'integer',
    ];

    protected $hidden = [
        'user_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return $this
     */
    public function incrementDeals(): Statistic
    {
        $this->deals_count++;
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function incrementFinishedDeals(): Statistic
    {
        $this->deals_finished_count++;
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function incrementCanceledDeals(): Statistic
    {
        $this->deals_canceled_count++;
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function incrementDisputedDeals(): Statistic
    {
        $this->deals_disputed_count++;
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function incrementPaidDeals(): Statistic
    {
        $this->deals_paid_count++;
        $this->save();
        return $this;
    }

    /**
     * Increment total turnover
     *
     * @param float $sum
     * @return Statistic
     */
    public function incrementTotalTurnover(float $sum): Statistic
    {
        $this->total_turnover += $sum;
        $this->save();
        return $this;
    }

    /**
     * Increment average finish deal time for user statistics
     *
     * @param int $seconds
     *
     * @return Statistic
     */
    public function incrementAverageFinishDealTime(int $seconds): Statistic
    {
        $this->average_finish_deal_time += $seconds;
        $this->save();
        return $this;
    }
}
