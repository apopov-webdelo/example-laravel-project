<?php

namespace App\Models\User;

/**
 * type name = part of lang file path for SecurityLogResource
 *
 * Interface SecurityLogConstants
 *
 * @package App\Models\User
 */
interface SecurityLogConstants
{
    const PASSWORD_CHANGED = 'password_changed';
    const PASSWORD_CHANGE_FAIL_2FA = 'password_change_fail_2fa';

    const EMAIL_CONFIRMATION_START = 'email_confirmation_start';

    const USER_LOCKED = 'user_locked';
    const USER_LOCK_TRADE = 'user_block_trade';
    const USER_UNLOCKED = 'user_unlocked';
    const USER_UNLOCK_TRADE = 'user_unlock_trade';

    const PASSWORD_IS_RESET = 'password_is_reset';

    const TWOFA_CONNECT = '2fa_connect';
    const TWOFA_DISCONNECT = '2fa_disconnect';
    const TWOFA_ENABLE = '2fa_enable';
    const TWOFA_DISABLE = '2fa_disable';
    const TWOFA_DEACTIVATE = '2fa_deactivate';

    const PHONE_ADD = 'phone_add';
    const PHONE_ADD_CONFIRM = 'phone_add_confirm';
    const PHONE_DELETE = 'phone_delete';
    const PHONE_DELETE_COMPLETE = 'phone_delete_complete';

    const TELEGRAMM_ADD = 'telegramm_add';
    const TELEGRAMM_ADD_CONFIRM = 'telegramm_add_confirm';

    const EMAIL_CHANGE = 'email_change';
    const EMAIL_CONFIRM_CHANGE = 'email_confirm_change';
    const EMAIL_CONFIRM = 'email_confirm';

    const PHONE_CHANGE = 'phone_change';
    const PHONE_CONFIRM_CHANGE = 'phone_confirm_change';
    const PHONE_CONFIRM = 'phone_confirm';
}
