<?php

namespace App\Models\Admin;

use App\Models\User\Google2faConstants;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Security
 *
 * @package App\Models\User
 * @property int $id
 * @property int $admin_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $google2fa_status
 * @property-read \App\Models\Admin\Admin $admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailConfirmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereEmailInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereGoogle2faConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneConfirmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security wherePhoneInactive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Security whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Security newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Security newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Security query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Security whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Security whereGoogle2faStatus($value)
 */
class Security extends Model
{
    protected $table = 'admins_security';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_id',
        'google2fa_status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * Check if google 2fa active
     *
     * @return bool
     */
    public function isGoogle2faActive() : bool
    {
        return $this->google2fa_status == Google2faConstants::STATUS_ACTIVATED;
    }

    /**
     * Check if google 2fa is paused
     *
     * @return bool
     */
    public function isGoogle2faPaused() : bool
    {
        return $this->google2fa_status == Google2faConstants::STATUS_PAUSED;
    }

    /**
     * Check if google 2fa is paused
     *
     * @return bool
     */
    public function isGoogle2faDeactivated() : bool
    {
        return $this->google2fa_status == Google2faConstants::STATUS_DEACTIVATED;
    }
}
