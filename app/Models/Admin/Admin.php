<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/7/18
 * Time: 16:33
 */

namespace App\Models\Admin;

use App\Contracts\ActivityLog\ActivityLoggable;
use App\Contracts\AuthenticatedContract;
use App\Contracts\LockableContract;
use App\Contracts\SessionableContract;
use App\Models\ActivityLog\ActivityLog;
use App\Models\Chat\Chat;
use App\Models\Role\Role;
use App\Models\Session\Session;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class Admin
 *
 * @property int $id
 * @property string $login
 * @property string $name
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $telegram_id
 * @property string $remember_token
 * @property string $api_token
 * @property string $one_signal_player_id
 * @property string $google2fa_secret
 * @property string $appeal
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Carbon $last_seen
 * @property Carbon $user_locked_at
 * @property Carbon $user_locked_to
 * @property Carbon $trade_locked_at
 * @property Carbon $trade_locked_to
 * @property Security $security
 * @property Collection $sessions
 * @package App\Models\Admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivityLog\ActivityLog[] $activityLogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chat\Chat[] $chats
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \App\Models\Role\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Admin\Admin onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereGoogle2faSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereLastSeen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereUserLockedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin whereUserLockedTo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Admin\Admin withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Admin\Admin withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Admin role($roles)
 */
class Admin extends Authenticatable implements
    AuthenticatedContract,
    LockableContract,
    SessionableContract,
    ActivityLoggable
{
    use Notifiable,
        HasRoles,
        SoftDeletes;

    protected $guard_name = 'api-admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'login',
        'email',
        'phone',
        'password',
        'api_token',
        'google2fa_secret',
        'last_seen'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'api_token',
        'password',
        'remember_token',
        'google2fa_secret'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'user_locked_at',
        'user_locked_to',
        'last_seen',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * Check is trading available
     *
     * @return bool
     */
    public function isUnlocked() : bool
    {
        return ! $this->isBlocked();
    }

    /**
     * Check is trading was blocked for that user
     *
     * @return bool
     */
    public function isBlocked() : bool
    {
        if ($this->user_locked_at && $this->user_locked_to) {
            return now()->between($this->user_locked_at, $this->user_locked_to);
        }

        if ($this->user_locked_at) {
            return now()->greaterThanOrEqualTo($this->user_locked_at);
        }

        if ($this->user_locked_to) {
            return now()->lessThanOrEqualTo($this->user_locked_to);
        }

        return false;
    }

    /**
     * @param Carbon      $lockedAt
     * @param Carbon|null $lockedTo
     *
     * @return bool
     */
    public function block(Carbon $lockedAt, Carbon $lockedTo = null): bool
    {
        $this->user_locked_at = $lockedAt;
        $this->user_locked_to = $lockedTo;
        $this->generateApiToken();
        $this->save();

        return true;
    }

    /**
     * @return bool
     */
    public function unlock(): bool
    {
        $this->user_locked_at = null;
        $this->user_locked_to = null;
        $this->save();

        return true;
    }

    /**
     * Generate api token for admin (old style)
     *
     * @return $this
     */
    public function generateApiToken(): string
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

    /**
     * Invalidate old token by generating new one (old style)
     */
    public function invalidateApiToken()
    {
        $this->generateApiToken();
    }

    /**
     * Invalidate all tokens
     */
    public function invalidateAllTokens()
    {
        $this->invalidateApiToken();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function security()
    {
        return $this->hasOne(Security::class, 'admin_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function sessions()
    {
        return $this->morphMany(Session::class, 'sessionable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activityLogs()
    {
        return $this->morphMany(ActivityLog::class, 'authorable');
    }

    /**
     * Get all of the chats for the post.
     */
    public function chats()
    {
        return $this->morphToMany(Chat::class, 'recipientable', 'chat_recipients');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $password
     *
     * @return AuthenticatedContract
     */
    public function setPassword(string $password): AuthenticatedContract
    {
        $this->password = bcrypt($password);
        $this->save();

        return $this;
    }
}
