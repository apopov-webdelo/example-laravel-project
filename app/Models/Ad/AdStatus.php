<?php

namespace App\Models\Ad;

use App\Models\BaseModel;

/**
 * Class AdStatus
 *
 * @package App\Models\Ad
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\AdStatus query()
 */
class AdStatus extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'ads_statuses';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updates_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
    ];
}
