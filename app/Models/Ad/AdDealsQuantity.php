<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/26/18
 * Time: 11:22
 */

namespace App\Models\Ad;

use App\Repositories\Deal\DealRepo;

/**
 * Class AdDealsQuantity
 *
 * @package App\Models\Ad
 */
class AdDealsQuantity extends AdStatistic
{
    /**
     * @return int
     */
    public function get()
    {
        if (! $this->value) {
            $this->value = app(DealRepo::class)->filterByAd($this->ad)->take()->count();
        }

        return $this->value;
    }
}