<?php

namespace App\Models\Ad;

use App\Contracts\Bcrypt\ModelBcKeysContract;
use App\Contracts\Blockchain\ModelWalletsContract;
use App\Models\BaseModel;
use App\Models\Directory\Bank;
use App\Models\Directory\CommissionConstants;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use App\Traits\Authorable;
use App\Traits\Bcrypt\HasModelBcKeys;
use App\Traits\Blockchain\HasModelWallets;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ad
 *
 * @property Ad                                                                                 $ad
 * @property Currency                                                                           $currency
 * @property User                                                                               $author
 * @property CryptoCurrency                                                                     $cryptoCurrency
 * @property Country                                                                            $country
 * @package App\Models\Ad
 * @property int                                                                                $id
 * @property int                                                                                $author_id
 * @property int                                                                                $payment_system_id
 * @property string|null                                                                        $notes
 * @property int                                                                                $country_id
 * @property bool                                                                               $is_sale
 * @property int                                                                                $currency_id
 * @property int                                                                                $crypto_currency_id
 * @property float                                                                              $price
 * @property int                                                                                $min
 * @property int                                                                                $max
 * @property int                                                                                $original_max
 * @property float                                                                              $turnover
 * @property int                                                                                $time
 * @property bool                                                                               $liquidity_required
 * @property bool                                                                               $email_confirm_required
 * @property bool                                                                               $phone_confirm_required
 * @property bool                                                                               $trust_required
 * @property bool                                                                               $is_active
 * @property string|null                                                                        $conditions
 * @property \Carbon\Carbon                                                                     $created_at
 * @property \Carbon\Carbon                                                                     $updated_at
 * @property int|null                                                                           $review_rate
 * @property bool                                                                               $tor_denied
 * @property int $min_deal_finished_count
 * @property int $deal_cancellation_max_percent
 * @property int                                                                                $commission_percent
 * @property float  $accuracyCommissionPercent
 * @property float                                                                              $accuracyPrice
 * @property float                                                                              $accuracyMax
 * @property float                                                                              $accuracyMin
 * @property float                                                                              $accuracyOriginalMax
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\Bank[]         $banks
 * @property-read \App\Models\Directory\PaymentSystem                                           $paymentSystem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereConditions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereCryptoCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereEmailConfirmRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereIsSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereLiquidityRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereMinDealFinishedCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereOriginalMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad wherePhoneConfirmRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereReviewRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereTorDenied($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereTrustRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereTurnover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ad\AdStatus[]            $statuses
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ad\Ad onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ad\Ad withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ad\Ad withoutTrashed()
 * @property \Carbon\Carbon|null                                                                $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereDeletedAt($value)
 * @method static \App\Models\Ad\Ad find($id)
 * @property-read float                                                                         $accuracy_max
 * @property-read float                                                                         $accuracy_min
 * @property-read float                                                                         $accuracy_original_max
 * @property-read float                                                                         $accuracy_price
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Bcrypt\ModelBcKey[]      $keys
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Blockchain\ModelWallet[] $wallets
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ad\Ad whereDealCancellationMaxPercent($value)
 */
class Ad extends BaseModel implements ModelBcKeysContract, ModelWalletsContract
{
    use SoftDeletes, Authorable, HasModelBcKeys, HasModelWallets;

    /**
     * @var array
     */
    protected $fillable = [
        'author_id',
        'payment_system_id',
        'notes',
        'country_id',
        'is_sale',
        'currency_id',
        'crypto_currency_id',
        'balance',
        'price',
        'min',
        'max',
        'original_max',
        'turnover',
        'time',
        'liquidity_required',
        'email_confirm_required',
        'phone_confirm_required',
        'trust_required',
        'tor_denied',
        'is_active',
        'conditions',
        'created_at',
        'review_rate',
        'min_deal_finished_count',
        'deal_cancellation_max_percent',
        'commission_percent',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_sale'                => 'bool',
        'is_active'              => 'bool',
        'liquidity_required'     => 'bool',
        'email_confirm_required' => 'bool',
        'phone_confirm_required' => 'bool',
        'trust_required'         => 'bool',
        'tor_denied'             => 'bool',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function statuses()
    {
        return $this->belongsToMany(AdStatus::class, 'ads_has_statuses', 'ad_id', 'status_id')
                    ->withPivot(['created_at', 'updated_at']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentSystem()
    {
        return $this->belongsTo(PaymentSystem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cryptoCurrency()
    {
        return $this->belongsTo(CryptoCurrency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function banks()
    {
        return $this->belongsToMany(Bank::class, 'ads_has_banks', 'ad_id', 'bank_id');
    }

    /**
     * @return mixed|null
     */
    public function getSeller()
    {
        return $this->is_sale ? $this->author : null;
    }

    /**
     * @return mixed|null
     */
    public function getBuyer()
    {
        return $this->is_sale ? null : $this->author;
    }

    /**
     * @return bool
     */
    public function isSale(): bool
    {
        return (bool)$this->is_sale;
    }

    /**
     * @return bool
     */
    public function isLiquid(): bool
    {
        return $this->min <= $this->max;
    }

    /**
     * @return bool
     */
    public function isNotLiquid(): bool
    {
        return !$this->isLiquid();
    }

    /**
     * Check is ad in that currency by currency code
     *
     * @param string $currencyCode
     *
     * @return bool
     */
    public function isCurrencyEqual(string $currencyCode): bool
    {
        return strtoupper($this->currency->code) == strtoupper($currencyCode);
    }

    /**
     * Return current ad's status
     *
     * @return AdStatus
     */
    public function getCurrentStatus(): AdStatus
    {
        return $this->statuses()->orderBy('ads_has_statuses.id', 'desc')
                    ->limit(1)
                    ->get()->first();
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isProtected()
    {
        return $this->isBcKeyExists('owner');
    }

    /**
     * Return fiat amount according accuracy for it's currency
     *
     * @return float
     */
    public function getAccuracyPriceAttribute()
    {
        return currencyFromCoins($this->price, $this->currency);
    }

    /**
     * Return fiat max according accuracy for it's currency
     *
     * @return float
     */
    public function getAccuracyMaxAttribute()
    {
        return currencyFromCoins($this->max, $this->currency);
    }

    /**
     * Return fiat min according accuracy for it's currency
     *
     * @return float
     */
    public function getAccuracyMinAttribute()
    {
        return currencyFromCoins($this->min, $this->currency);
    }

    /**
     * Return fiat original_max according accuracy for it's currency
     *
     * @return float
     */
    public function getAccuracyOriginalMaxAttribute()
    {
        return currencyFromCoins($this->original_max, $this->currency);
    }

    /**
     * Human readable value (i.e. 0.8%)
     *
     * @return float
     */
    public function getAccuracyCommissionPercentAttribute()
    {
        return $this->commission_percent
            ? (float)$this->commission_percent / CommissionConstants::ONE_PERCENT
            : 0;
    }
}
