<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/26/18
 * Time: 11:22
 */

namespace App\Models\Ad;

use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Repositories\Deal\DealRepo;

/**
 * Class AdCryptoTurnover
 *
 * @package App\Models\Ad
 */
class AdCryptoTurnover extends AdStatistic
{
    /**
     * @return int
     */
    public function get()
    {
        if (! $this->value) {
            $this->value = app(DealRepo::class)
                ->filterByAd($this->ad)
                ->filterByUsedStatus(DealStatus::find(DealStatusConstants::FINISHED))
                ->take()
                ->sum('crypto_amount');
        }

        return $this->value;
    }
}