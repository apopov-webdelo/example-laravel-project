<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/26/18
 * Time: 15:43
 */

namespace App\Models\Ad;

/**
 * Ad statuses info
 *
 * @package App\Models\Ad
 */
interface AdStatusConstants
{
    const ACTIVE  = 1;
    const BLOCKED = 2;
}