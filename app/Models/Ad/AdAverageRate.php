<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/26/18
 * Time: 11:22
 */

namespace App\Models\Ad;

use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Repositories\Deal\DealRepo;

/**
 * Class AdAverageRate
 *
 * @package App\Models\Ad
 */
class AdAverageRate extends AdStatistic
{
    /**
     * @return int
     */
    public function get()
    {
        if (!$this->value) {
            /** @var AdCryptoTurnover $adCrypto */
            /** @var AdFiatTurnover $adFiat */
            $adCrypto       = app(AdCryptoTurnover::class, [ 'ad' => $this->ad ]);
            $adFiat         = app(AdFiatTurnover::class, [ 'ad' => $this->ad ]);
            $cryptoTurnover = $adCrypto->get();
            $fiatTurnover   = $adFiat->get();

            if ($fiatTurnover > 0) {
                $this->value   = $fiatTurnover / $cryptoTurnover;
            }
        }

        return $this->value;
    }
}