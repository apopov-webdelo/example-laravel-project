<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 6/26/18
 * Time: 11:18
 */

namespace App\Models\Ad;

/**
 * Class AdStatistic
 *
 * @package App\Models\Ad
 */
abstract class AdStatistic
{
    /**
     * @var Ad $ad
     */
    protected $ad;

    /**
     * @var int $value
     */
    protected $value;

    /**
     * AdStatistic constructor.
     *
     * @param Ad $ad
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }
}