<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/1/18
 * Time: 15:10
 */

namespace App\Models\Role;

use Carbon\Carbon;

/**
 * Class Role
 *
 * @property int $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property array $permissions
 * @package App\Models\Role
 * @property string $guard_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends \Spatie\Permission\Models\Role
{
    /**
     * @var array $fillable
     */
    protected $fillable = ['name', 'guard_name'];

    /**
     * @var array $hidden
     */
    protected $hidden = ['guard_name'];

    /**
     * @var array $dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
