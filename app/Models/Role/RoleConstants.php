<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/7/18
 * Time: 09:29
 */

namespace App\Models\Role;

/**
 * Interface RoleConstants
 *
 * @package App\Models\Role
 */
interface RoleConstants
{
    const CLIENT = 1;
    const ADMIN  = 2;
    const OPERATOR = 3;

    /**
     * Trust variants
     */
    const ROLES = [
        self::CLIENT,
        self::ADMIN,
        self::OPERATOR
    ];
}
