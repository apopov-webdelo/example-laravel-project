<?php

namespace App\Models\Bcrypt;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ModelKey
 *
 * @property int    $id
 * @property string $model
 * @property int    $model_id
 * @property string $alias
 * @property string $key
 * @mixin \Eloquent
 * @package App
 */
class ModelBcKey extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model',
        'model_id',
        'alias',
        'key',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Get all of the owning keys models.
     */
    public function model()
    {
        return $this->morphTo('model', 'model');
    }
}
