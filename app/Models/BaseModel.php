<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Global base for all Eloquent models
 *
 * Class BaseModel
 * @mixin \Eloquent
 * @package App\Models
 */
abstract class BaseModel extends Model
{

}
