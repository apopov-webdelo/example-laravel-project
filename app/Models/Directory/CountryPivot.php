<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/6/18
 * Time: 18:36
 */

namespace App\Models\Directory;


/**
 * App\Models\Directory\CountryPivot
 *
 * @property-read \App\Models\Directory\Country $country
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CountryPivot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CountryPivot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CountryPivot query()
 * @mixin \Eloquent
 */
class CountryPivot extends \Illuminate\Database\Eloquent\Relations\Pivot
{
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }
}