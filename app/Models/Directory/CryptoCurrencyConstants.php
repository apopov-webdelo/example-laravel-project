<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/17/18
 * Time: 16:52
 */

namespace App\Models\Directory;

interface CryptoCurrencyConstants
{
    // Codes
    const BTC_CODE = 'btc';
    const ETH_CODE = 'eth';

    // Amounts
    const ONE_BTC  = 100000000;
    const HALF_BTC = self::ONE_BTC/2;
    const TWO_BTC  = self::ONE_BTC*2;
    const FIVE_BTC = self::ONE_BTC*5;
    const TEN_BTC  = self::ONE_BTC*10;

    const ONE_ETH  = 100000000;
    const HALF_ETH = self::ONE_ETH/2;
    const TWO_ETH  = self::ONE_ETH*2;
    const FIVE_ETH = self::ONE_ETH*5;
    const TEN_ETH  = self::ONE_ETH*10;
}