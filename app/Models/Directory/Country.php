<?php

namespace App\Models\Directory;

use App\Events\Directory\CountrySaved;
use App\Models\BaseModel;
use App\Repositories\Directory\BankRepo;
use App\Repositories\Directory\CurrencyRepo;
use App\Repositories\Directory\PaymentSystemRepo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 *
 * @package App\Models\Directory
 * @property int $id
 * @property string|null $title
 * @property int|null $iso
 * @property string|null $alpha2
 * @property string|null $alpha3
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereAlpha2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereAlpha3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereTitle($value)
 * @mixin \Eloquent
 * @property int $currency_id
 * @property-read \App\Models\Directory\Currency $currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereCurrencyId($value)
 * @property int|null $author_id
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|static[] $banks
 * @property-read \Illuminate\Database\Eloquent\Collection|static[] $currencies
 * @property-read \Illuminate\Database\Eloquent\Collection|static[] $payment_systems
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Country whereUpdatedAt($value)
 */
class Country extends BaseModel
{
    use SoftDeletes;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => CountrySaved::class,
    ];

    protected $fillable = [
        'title',
        'currency_id',
        'iso',
        'alpha2',
        'alpha3',
        'author_id'
    ];

    protected $appends = [
        'currencies',
        'banks',
        'paymentSystems',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCurrencies()
    {
        /** @var CurrencyRepo $repo */
        $repo = app(CurrencyRepo::class);
        return $repo->filterByCountry($this)->take()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCurrenciesAttribute()
    {
        return $this->getCurrencies();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPaymentSystems()
    {
        /** @var PaymentSystemRepo $repo */
        $repo = app(PaymentSystemRepo::class);
        return $repo->filterByCountry($this)->take()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPaymentSystemsAttribute()
    {
        return $this->getPaymentSystems();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getBanks()
    {
        /** @var PaymentSystemRepo $repo */
        $repo = app(BankRepo::class);
        return $repo->filterByCountry($this)->take()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getBanksAttribute()
    {
        return $this->getBanks();
    }
}
