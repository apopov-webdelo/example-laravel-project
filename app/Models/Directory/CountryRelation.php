<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 7/6/18
 * Time: 18:34
 */

namespace App\Models\Directory;


class CountryRelation extends \Illuminate\Database\Eloquent\Relations\BelongsToMany
{
    protected $pivotColumns = ['country_id'];

    public function newPivot(array $attributes = [], $exists = false)
    {
        $pivot = new CountryPivot($attributes);

        return $pivot->setPivotKeys($this->foreignPivotKey, $this->relatedPivotKey);
    }
}