<?php

namespace App\Models\Directory;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Events\Directory\CryptoCurrencySaved;
use App\Models\BaseModel;

/**
 * Class CryptoCurrency
 *
 * @package App\Models\Directory
 * @property int $id
 * @property string|null $title
 * @property string|null $code
 * @property int $accuracy
 * @property float|null $min_balance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereMinBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency whereAccuracy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\CryptoCurrency query()
 */
class CryptoCurrency extends BaseModel implements CryptoCurrencyContract
{
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => CryptoCurrencySaved::class,
    ];

    protected $guarded = [
        'accuracy',
    ];

    protected $casts = [
        'accuracy' => 'int',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'code';
    }

    /* Start Implementation CryptoCurrencyContract */

    /**
     * Return ISO code for currency (3 symbols)
     *
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * Return currency name
     *
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * Return accuracy value
     *
     * @return int
     */
    public function getAccuracy() : int
    {
        return $this->accuracy;
    }

    /* End Implementation CryptoCurrencyContract */
}
