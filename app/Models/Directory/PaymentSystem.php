<?php

namespace App\Models\Directory;

use App\Events\Directory\PaymentSystemSaved;
use App\Models\Ad\Ad;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentSystem
 *
 * @package App\Models\Directory
 * @property int                                                                            $id
 * @property string|null                                                                    $title
 * @property \Carbon\Carbon                                                                 $created_at
 * @property \Carbon\Carbon|null                                                            $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ad\Ad[]              $ads
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\Currency[] $currencies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\Country[] $country
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null                                                                    $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\PaymentSystem whereDeletedAt($value)
 */
class PaymentSystem extends BaseModel
{
    use SoftDeletes;

    // todo until payment systems separation is discussed
    const BANK_SYSTEM_ID = 3;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => PaymentSystemSaved::class,
    ];

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'id',
        'title',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies()
    {
        return $this->belongsToMany(
            Currency::class,
            'payment_systems_has_currencies',
            'payment_system_id',
            'currency_id'
        )->withPivot('country_id')->distinct();
    }

    /**
     * @param $countryId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currenciesByCountry($countryId)
    {
        return $this->belongsToMany(
            Currency::class,
            'payment_systems_has_currencies',
            'payment_system_id',
            'currency_id'
        )->where('country_id', $countryId)->distinct();
    }

    /**
     * @return CountryRelation
     */
    public function country()
    {
        return (new CountryRelation(
            (new Country())->newQuery(),
            $this,
            'payment_systems_has_currencies',
            'payment_system_id',
            'country_id',
            'id',
            'id'
        ));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany(Ad::class, 'payment_system_id', 'id');
    }

    /**
     * @return bool
     */
    public function isBank()
    {
        return $this->id === self::BANK_SYSTEM_ID;
    }
}
