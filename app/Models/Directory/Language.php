<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class Country
 *
 * @package App\Models\Directory
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Language query()
 */
class Language extends BaseModel
{
    //
}
