<?php

namespace App\Models\Directory;

use App\Contracts\Currency\FiatCurrencyContract;
use App\Events\Directory\CurrencySaved;
use App\Models\Ad\Ad;
use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Currency
 *
 * @property string $title
 * @property string $code
 * @property int $accuracy
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @package App\Models\Directory
 * @property int $id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ad\Ad[] $ads
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\Bank[] $banks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\PaymentSystem[] $paymentSystems
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\Country[] $countries
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereAccuracy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Currency whereDeletedAt($value)
 */
class Currency extends BaseModel implements FiatCurrencyContract
{
    use SoftDeletes;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => CurrencySaved::class,
    ];

    protected $fillable = [
        'title',
        'code',
        'accuracy'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function banks()
    {
        return $this->belongsToMany(
            Bank::class,
            'banks_has_currencies',
            'currency_id',
            'bank_id'
        )->withPivot('country_id')->with('country')->distinct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function paymentSystems()
    {
        return $this->belongsToMany(
            PaymentSystem::class,
            'payment_systems_has_currencies',
            'currency_id',
            'payment_system_id'
        )->withPivot('country_id')->with('country')->distinct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany(Ad::class, 'currency_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function countries()
    {
        return $this->hasMany(Country::class, 'currency_id', 'id');
    }

    /* Start Implementation FiatCurrencyContract */

    /**
     * Return ISO code for currency (3 symbols)
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Return currency name
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Return accuracy value
     *
     * @return int
     */
    public function getAccuracy(): int
    {
        return $this->accuracy;
    }

    /* End Implementation FiatCurrencyContract */
}
