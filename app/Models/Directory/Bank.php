<?php

namespace App\Models\Directory;

use App\Events\Directory\BankSaved;
use App\Models\Ad\Ad;
use App\Models\Admin\Admin;
use App\Models\BaseModel;
use App\Models\Image\Image;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Bank
 *
 * @package App\Models\Directory
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $author_id
 * @property Image $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ad\Ad[] $ads
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Directory\Currency[] $currencies
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $deleted_at
 * @property-read \App\Models\Admin\Admin|null $author
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Directory\Bank onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Directory\Bank whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Directory\Bank withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Directory\Bank withoutTrashed()
 */
class Bank extends BaseModel
{
    use SoftDeletes;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => BankSaved::class,
    ];

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'id',
        'title',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies()
    {
        return $this->belongsToMany(
            Currency::class,
            'banks_has_currencies',
            'bank_id',
            'currency_id'
        )->withPivot('country_id')->distinct();
    }

    /**
     * @return CountryRelation
     */
    public function country()
    {
        return new CountryRelation(
            (new Country())->newQuery(),
            $this,
            'banks_has_currencies',
            'bank_id',
            'country_id',
            'id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ads()
    {
        return $this->belongsToMany(
            Ad::class,
            'ads_has_banks',
            'bank_id',
            'ad_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
