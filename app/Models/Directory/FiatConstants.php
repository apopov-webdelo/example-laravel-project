<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/13/18
 * Time: 16:27
 */

namespace App\Models\Directory;

interface FiatConstants
{
    const USD = 'USD';
    const RUB = 'RUB';
    const EUR = 'EUR';

    const DEFAULT_ACCURACY = 2;
}
