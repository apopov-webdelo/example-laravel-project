<?php

namespace App\Models\Image;

use App\Contracts\ImageableContract;
use App\Contracts\Images\ImageContract;
use App\Models\Admin\Admin;
use App\Models\BaseModel;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class Image
 *
 * @property int $id
 * @property string $title
 * @property string $filename
 * @property string $mime
 * @property string $ext
 * @property string $size
 * @property User|Admin $author
 * @property ImageableContract $imageable
 * @property int $imageable_id
 * @property string $imageable_type
 * @property int $author_id
 * @property string $author_type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @package App\Images\Models
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read string $base_path
 * @property-read \Illuminate\Contracts\Routing\UrlGenerator|string $full_path
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereAuthorType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereImageableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereImageableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereMime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image\Image whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Image extends BaseModel implements ImageContract
{
    const IMAGES_LOCATION = 'images';

    /**
     * @var string
     */
    protected $table = 'images';

    protected $fillable = [
        'title',
        'filename',
        'mime',
        'ext',
        'size',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'imageable_id',
        'imageable_type',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $appends = ['basePath', 'fullPath'];


    /**
     * @return string
     */
    public function getBasePathAttribute()
    {
        return $this->getBasePath();
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getFullPathAttribute()
    {
        return $this->getFullPath();
    }

    /**
     * @return string
     */
    public function getStoragePath(): string
    {
        return self::IMAGES_LOCATION.'/'.$this->getFilenameById();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function author()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function getFilenameById(): string
    {
        return $this->id.'.'.$this->ext;
    }

    /**
     * @return string
     */
    public function getBasePath(): string
    {
        return secure_url(self::IMAGES_LOCATION);
    }

    /**
     * @return string
     */
    public function getFullPath(): string
    {
        return secure_url(self::IMAGES_LOCATION . '/' . $this->getFilenameById());
    }

    /**
     * @return ImageableContract
     */
    public function getOwner(): ImageableContract
    {
        return $this->imageable;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return secure_url(Storage::url(self::IMAGES_LOCATION.'/'.$this->getFilenameById()));
    }
}
