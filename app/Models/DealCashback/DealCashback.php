<?php

namespace App\Models\DealCashback;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\DealCashback\DealCashbackContract;
use App\Models\Deal\Deal;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DealCashback
 *
 * @property int            $id
 * @property int            $commission
 * @property string         $transaction_id
 * @property string         $status
 * @property int            $user_id
 * @property int            $crypto_currency_id
 * @property Collection     $dealsRelation
 * @property User           $user
 * @property CryptoCurrency $cryptoCurrencyRelation
 *
 * @package App\Models\DealCashback
 */
class DealCashback extends Model implements DealCashbackContract
{
    use SoftDeletes;

    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_BREAK       = 'break';
    const STATUS_PENDING     = 'pending';
    const STATUS_DONE        = 'done';
    const STATUS_CANCELED    = 'canceled';

    /**
     * Statuses list
     */
    const STATUSES = [
        self::STATUS_IN_PROGRESS,
        self::STATUS_BREAK,
        self::STATUS_PENDING,
        self::STATUS_DONE,
        self::STATUS_CANCELED,
    ];

    /**
     * @var string
     */
    protected $table = 'deals_cashback';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status',
        'commission',
        'crypto_currency_id',
        'transaction_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cryptoCurrencyRelation()
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dealsRelation()
    {
        return $this->belongsToMany(Deal::class, 'deals_cashback_has_deals', 'deals_cashback_id');
    }

    /**
     * @return User
     */
    public function recipient(): User
    {
        return $this->user;
    }

    /**
     * @return Collection
     */
    public function deals(): Collection
    {
        return $this->dealsRelation;
    }

    /**
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract
    {
        return $this->cryptoCurrencyRelation;
    }

    /**
     * Commission % is fixed when first deal in sequence was finished
     *
     * @return int
     */
    public function commission(): int
    {
        return $this->commission;
    }

    /**
     * @return bool
     */
    public function isReadyToPay(): bool
    {
        return $this->deals()->count() >= config('app.cashback.deals.quantity');
    }

    /**
     * @param string $status
     *
     * @return bool
     * @throws \Exception
     */
    public function setStatus(string $status): bool
    {
        $this->checkStatus($status)->status = $status;

        return $this->save();
    }

    /**
     * @param string $status
     *
     * @return $this
     * @throws \Exception
     */
    private function checkStatus(string $status)
    {
        if (in_array($status, self::STATUSES)) {
            return $this;
        }

        throw new \Exception('Try to set invalid status for DealCashback!');
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     *
     * @return mixed
     */
    public function setTransactionId(string $transactionId)
    {
        $this->transaction_id = $transactionId;

        return $this->save();
    }

    /**
     * Is current status `pending`
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status() === static::STATUS_PENDING;
    }

    /**
     * Current status
     *
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * Is current status `canceled`
     *
     * @return bool
     */
    public function isCanceled(): bool
    {
        return $this->status() === static::STATUS_CANCELED;
    }
}
