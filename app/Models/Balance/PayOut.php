<?php

namespace App\Models\Balance;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\BaseModel;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Builder;

/**
 * Class PayOut
 *
 * @package App\Models\Balance
 * @property int $id
 * @property string $transaction_id Transaction ID in blockchain
 * @property string $wallet_id Blockchain wallet for withdraw transaction
 * @property string $status
 * @property int|null $user_id
 * @property string $crypto_code
 * @property int $amount
 * @property int $commission
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User|null $userModel
 * @method static Builder|PayOut newModelQuery()
 * @method static Builder|PayOut newQuery()
 * @method static Builder|PayOut query()
 * @method static Builder|PayOut whereAmount($value)
 * @method static Builder|PayOut whereCommission($value)
 * @method static Builder|PayOut whereCreatedAt($value)
 * @method static Builder|PayOut whereCryptoCode($value)
 * @method static Builder|PayOut whereId($value)
 * @method static Builder|PayOut whereStatus($value)
 * @method static Builder|PayOut whereTransactionId($value)
 * @method static Builder|PayOut whereUpdatedAt($value)
 * @method static Builder|PayOut whereUserId($value)
 * @mixin \Eloquent
 */
class PayOut extends BaseModel implements PayOutContract
{
    const STATUS_PENDING   = 'pending';
    const STATUS_PROCESSED = 'processed';
    const STATUS_CANCELED  = 'canceled';

    /**
     * Statuses list
     */
    const STATUSES = [
        self::STATUS_PENDING,
        self::STATUS_PROCESSED,
        self::STATUS_CANCELED,
    ];

    protected $fillable = [
        'transaction_id',
        'status',
        'user_id',
        'crypto_code',
        'amount',
        'commission',
        'wallet_id',
    ];

    protected $table = 'payout_transactions';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userModel()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * {@inheritdoc}
     *
     * @return User
     */
    public function user(): User
    {
        return $this->userModel;
    }

    /**
     * {@inheritdoc}
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract
    {
        /** @var CryptoCurrencyRepo $repo */
        $repo = app(CryptoCurrencyRepo::class);
        return $repo->getByCode($this->crypto_code);
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function due(): int
    {
        return $this->amount() + $this->commission();
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function amount(): int
    {
        return (int)$this->amount;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function commission(): int
    {
        return (int)$this->commission;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function transactionId(): string
    {
        return (string)$this->transaction_id;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function walletId(): string
    {
        return (string)$this->wallet_id;
    }


    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     *
     * @return Carbon
     */
    public function createdAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * {@inheritdoc}
     *
     * @return Carbon
     */
    public function updatedAt(): Carbon
    {
        return $this->updated_at;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status() == self::STATUS_PENDING;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->status() == self::STATUS_PROCESSED;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isCanceled(): bool
    {
        return $this->status() == self::STATUS_CANCELED;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $transactionId
     *
     * @return bool
     */
    public function setTransactionId(string $transactionId): bool
    {
        $this->transaction_id = $transactionId;
        return $this->save();
    }

    /**
     * {@inheritdoc}
     *
     * @param string $status
     *
     * @return bool
     * @throws \Exception
     */
    public function setStatus(string $status): bool
    {
        $this->checkStatus($status)->status = $status;
        return $this->save();
    }

    /**
     * @param string $status
     *
     * @return $this
     * @throws \Exception
     */
    private function checkStatus(string $status)
    {
        if (in_array($status, self::STATUSES)) {
            return $this;
        }

        throw new \Exception('Try to set invalid status for PayOut!');
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     * @throws \Exception
     */
    public function setProcessedStatus(): bool
    {
        return $this->setStatus(self::STATUS_PROCESSED);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     * @throws \Exception
     */
    public function setCanceledStatus(): bool
    {
        return $this->setStatus(self::STATUS_CANCELED);
    }
}
