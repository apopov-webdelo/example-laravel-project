<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 7/18/18
 * Time: 16:43
 */

namespace App\Models\Balance;

/**
 * Interface TransactionConstants
 * @package App\Models\Balance
 */
interface TransactionConstants
{
    const REASON_DEAL       = 1;
    const REASON_TRANSFER   = 2;
    const REASON_BLOCKCHAIN = 3;
    const REASON_EXCHANGE   = 4;
    const REASON_CODE       = 5;
    const REASON_COMMISSION = 6;

    const REASONS = [
        self::REASON_DEAL,
        self::REASON_TRANSFER,
        self::REASON_BLOCKCHAIN,
        self::REASON_EXCHANGE,
        self::REASON_CODE,
        self::REASON_COMMISSION,
    ];
}