<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/29/18
 * Time: 15:24
 */

namespace App\Models\Balance;

use App\Models\BaseModel;

/**
 * Class TransactionReason
 *
 * @package App\Models\Balance
 * @property string $reason
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionReason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionReason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionReason query()
 * @mixin \Eloquent
 * @property int $id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionReason whereReason($value)
 */
class TransactionReason extends BaseModel
{
    const REASON_DEAL       = 'deal';
    const REASON_TRANSFER   = 'transfer';
    const REASON_BLOCKCHAIN = 'blockchain';
    const REASON_EXCHANGE   = 'exchange';
    const REASON_CODE       = 'code';
    const REASON_COMMISSION = 'commission';

    protected $table = 'transactions_reasons';
}
