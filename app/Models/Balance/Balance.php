<?php

namespace App\Models\Balance;

use App\Models\BaseModel;
use App\Models\Directory\CommissionConstants;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;

/**
 * Class Balance
 *
 * @package App\Models\Balance
 * @property int                                       $id
 * @property int                                       $user_id
 * @property int                                       $crypto_currency_id
 * @property int                                       $amount
 * @property int                                       $due
 * @property float                                     $accuracyAmount
 * @property float                                     $turnover
 * @property float                                     $escrow
 * @property int|null                                  $commission
 * @property float                                     $accuracyCommission
 * @property \Carbon\Carbon                            $created_at
 * @property \Carbon\Carbon                            $updated_at
 * @property int                                       $deals_count
 * @property-read \App\Models\Directory\CryptoCurrency $cryptoCurrency
 * @property-read \App\Models\User\User                $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereCryptoCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereDealsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereEscrow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereTurnover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance whereUserId($value)
 * @mixin \Eloquent
 * @property-read float                                $accuracy_amount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Balance query()
 */
class Balance extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'crypto_currency_id',
        'amount',
        'turnover',
        'escrow',
        'commission',
        'deals_count',
    ];

    /**
     * @var string
     */
    protected $table = 'balance';

    /**
     * @var array
     */
    protected $casts = [
        'min_balance' => 'numeric',
    ];

    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cryptoCurrency()
    {
        return $this->hasOne(CryptoCurrency::class, 'id', 'crypto_currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Return amount according accuracy for that crypto currency
     *
     * @return float
     */
    public function getAccuracyAmountAttribute()
    {
        return $this->amount ? currencyFromCoins($this->amount, $this->cryptoCurrency) : 0;
    }

    /**
     * Human readable value (i.e. 0.8%)
     *
     * @return float
     */
    public function getAccuracyCommissionAttribute()
    {
        return $this->commission
            ? (float)$this->commission / CommissionConstants::ONE_PERCENT
            : 0;
    }

    /**
     * Total balance including escrow
     *
     * @return float|int
     */
    public function getDueAttribute()
    {
        return $this->amount + $this->escrow;
    }
}
