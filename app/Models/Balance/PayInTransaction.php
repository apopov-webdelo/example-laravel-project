<?php

namespace App\Models\Balance;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Models\BaseModel;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Balance\PayInTransaction
 *
 * @property int $id
 * @property string $transaction_id Transaction ID in blockchain
 * @property string $status
 * @property int|null $user_id
 * @property string $crypto_code
 * @property int $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User|null $user
 * @method static Builder|PayInTransaction newModelQuery()
 * @method static Builder|PayInTransaction newQuery()
 * @method static Builder|PayInTransaction query()
 * @method static Builder|PayInTransaction whereAmount($value)
 * @method static Builder|PayInTransaction whereCreatedAt($value)
 * @method static Builder|PayInTransaction whereCryptoCode($value)
 * @method static Builder|PayInTransaction whereId($value)
 * @method static Builder|PayInTransaction whereStatus($value)
 * @method static Builder|PayInTransaction whereTransactionId($value)
 * @method static Builder|PayInTransaction whereUpdatedAt($value)
 * @method static Builder|PayInTransaction whereUserId($value)
 * @mixin \Eloquent
 */
class PayInTransaction extends BaseModel implements PayInTransactionContract
{
    const STATUS_PENDING   = 'pending';
    const STATUS_PROCESSED = 'processed';
    const STATUS_FAILED    = 'failed';

    /**
     * Statuses list
     */
    const STATUSES = [
        self::STATUS_PENDING,
        self::STATUS_PROCESSED,
        self::STATUS_FAILED,
    ];

    protected $table = 'payin_transactions';

    protected $fillable = [
        'transaction_id',
        'status',
        'user_id',
        'crypto_code',
        'amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * {@inheritdoc}
     *
     * @return User
     */
    public function recipient(): User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     *
     * @return CryptoCurrencyContract
     */
    public function cryptoCurrency(): CryptoCurrencyContract
    {
        /** @var CryptoCurrencyRepo $repo */
        $repo = app(CryptoCurrencyRepo::class);
        return $repo->getByCode($this->crypto_code);
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function transactionId(): string
    {
        return $this->transaction_id;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     *
     * @return Carbon
     */
    public function createdAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * {@inheritdoc}
     *
     * @return Carbon
     */
    public function updatedAt(): Carbon
    {
        return $this->updated_at;
    }

    /**
     * Check is current status PENDING
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status() == self::STATUS_PENDING;
    }

    /**
     * Check is current status PROCESSED
     *
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->status() == self::STATUS_PROCESSED;
    }

    /**
     * Check is current status FAILED
     *
     * @return bool
     */
    public function isFailed(): bool
    {
        return $this->status() == self::STATUS_FAILED;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $status
     *
     * @return bool
     * @throws \Exception
     */
    public function setStatus(string $status): bool
    {
        $this->checkStatus($status)->status = $status;
        return $this->save();
    }

    /**
     * @param string $status
     *
     * @return $this
     * @throws \Exception
     */
    private function checkStatus(string $status)
    {
        if (in_array($status, self::STATUSES)) {
            return $this;
        }

        throw new \Exception('Try to set invalid status for PayInTransaction!');
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     * @throws \Exception
     */
    public function setProcessedStatus(): bool
    {
        return $this->setStatus(self::STATUS_PROCESSED);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     * @throws \Exception
     */
    public function setFailedStatus(): bool
    {
        return $this->setStatus(self::STATUS_FAILED);
    }
}
