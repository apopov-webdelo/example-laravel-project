<?php

namespace App\Models\Balance;

use App\Models\BaseModel;

/**
 * Class BalanceLocked
 *
 * @package App\Models\Balance
 * @property int $id
 * @property int|null $balance_id
 * @property float|null $amount
 * @property string|null $expiration_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Balance\Balance $balance
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked whereBalanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked whereExpirationAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\BalanceLocked query()
 */
class BalanceLocked extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'balance_locked';

    /**
     * @var array
     */
    protected $hidden = ['created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function balance()
    {
        return $this->hasOne(Balance::class, 'balance_id', 'id');
    }
}
