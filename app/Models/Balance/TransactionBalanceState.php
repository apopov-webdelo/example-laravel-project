<?php

namespace App\Models\Balance;

use App\Models\BaseModel;

/**
 * App\Models\Balance\TransactionBalanceState
 *
 * @property int $id
 * @property int $transaction_id
 * @property int $balance_id
 * @property int $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Balance\Balance|null $balance
 * @property-read \App\Models\Balance\Transaction|null $transaction
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState whereBalanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\TransactionBalanceState query()
 */
class TransactionBalanceState extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'transactions_balances_states';

    /**
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'balance_id',
        'amount',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function balance()
    {
        return $this->belongsTo(Balance::class, 'balance_id', 'id');
    }
}
