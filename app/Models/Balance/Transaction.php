<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 6/29/18
 * Time: 15:24
 */

namespace App\Models\Balance;


use App\Models\BaseModel;
use App\Models\Directory\CryptoCurrency;
use App\Models\User\User;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Carbon\Carbon;

/**
 * Class Transaction
 *
 * @package App\Models\Balance
 * @property int $id
 * @property int $author_id
 * @property int $amount
 * @property string $type
 * @property float $balance_amount
 * @property int $remitter_balance_id
 * @property int $receiver_balance_id
 * @property TransactionReason $reason
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read User $author
 * @static Transaction
 * @mixin \Eloquent
 * @property-read Balance $receiverBalance
 * @property-read Balance $remitterBalance
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereBalanceAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereReceiverBalanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereRemitterBalanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Balance\TransactionBalanceState[] $transactionBalancesStates
 * @property int $reason_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction whereReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Balance\Transaction query()
 */
class Transaction extends BaseModel
{
    const TYPE_DEPOSIT  = 'deposit';
    const TYPE_WITHDRAW = 'withdraw';

    const REASON_DEAL       = 'deal';
    const REASON_TRANSFER   = 'transfer';
    const REASON_BLOCKCHAIN = 'blockchain';
    const REASON_EXCHANGE   = 'exchange';
    const REASON_CODE       = 'code';
    const REASON_COMMISSION = 'commission';

    /**
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'amount' => 'int',
        'balance_amount' => 'int',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remitterBalance()
    {
        return $this->belongsTo(Balance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiverBalance()
    {
        return $this->belongsTo(Balance::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reason()
    {
        return $this->belongsTo(TransactionReason::class);
    }

    /**
     * Check is that transaction type is deposit
     *
     * @return bool
     */
    public function isDeposit() : bool
    {
        return $this->type == self::TYPE_DEPOSIT;
    }

    /**
     * Check is that transaction type is deposit
     *
     * @return bool
     */
    public function isWithdraw() : bool
    {
        return $this->type == self::TYPE_WITHDRAW;
    }

    /**
     * Check is transmitted user is author
     *
     * @param User $user
     * @return bool
     */
    public function isAuthor(User $user) : bool
    {
        return $this->author->id == $user->id;
    }

    /**
     * Return CryptoCurrency model for that transaction
     *
     * @return CryptoCurrency
     */
    public function getCryptoCurrency() : CryptoCurrency
    {
        return $this->remitterBalance->cryptoCurrency;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactionBalancesStates()
    {
        return $this->hasMany(TransactionBalanceState::class, 'balance_id', 'id');
    }
}