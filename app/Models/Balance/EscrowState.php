<?php

namespace App\Models\Balance;

use App\Models\BaseModel;

/**
 * App\Models\Balance\EscrowState
 *
 * @property int $id
 * @property int $balance_id
 * @property int $escrow_before
 * @property int $escrow
 * @property int $escrow_after
 * @property int $balance_before
 * @property int $balance_after
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Balance\Balance|null $balance
 * @mixin \Eloquent
 */
class EscrowState extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'balance_id',
        'escrow_before',
        'escrow',
        'escrow_after',
        'balance_before',
        'balance_after',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function balance()
    {
        return $this->belongsTo(Balance::class, 'balance_id', 'id');
    }
}
