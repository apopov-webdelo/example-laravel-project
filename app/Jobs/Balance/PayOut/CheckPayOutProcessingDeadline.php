<?php

namespace App\Jobs\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Contracts\Repositories\PayOutRepoContract;
use App\Models\Balance\PayOut;
use App\Traits\Cache\CachesWithTimeout;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class CheckPayOutProcessingDeadline for checking deadline for PayOut processing
 *
 * @package App\Jobs\Balance\PayOut
 */
class CheckPayOutProcessingDeadline implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, CachesWithTimeout;

    const DEFAULT_DEADLINE = 5;

    /**
     * @param PayOutRepoContract $payOutRepo
     */
    public function handle(PayOutRepoContract $payOutRepo)
    {
        $processingDeadlineTime = config('app.payout.processing_deadline_time') ?? self::DEFAULT_DEADLINE;

        $payOutList = $payOutRepo
            ->filterByStatus(PayOut::STATUS_PENDING)
            ->take()->where('created_at', '<', now()->subMinutes($processingDeadlineTime))
            ->get();

        $payOutList = $this->onlyNotCached($payOutList);

        $payOutList->each(function ($payOut) {
            $this->cacheTimeout($payOut);
            $this->notify($payOut);
        });

        // Log::info(
        //     'Checked deadline for PayOut processing.' .
        //     'Was processed ' . $payOutList->count() . ' payout requests.'
        // );
    }

    /**
     * Send notification to Slack
     *
     * @param PayOutContract $payOut
     *
     * @return $this
     */
    protected function notify(PayOutContract $payOut)
    {
        $data = [
            'Payout ID'  => $payOut->id(),
            'User ID'    => $payOut->user()->getId(),
            'User Login' => $payOut->user()->getLogin(),
            'Amount'     => $payOut->amount(),
            'Commission' => $payOut->commission(),
            'Crypto'     => $payOut->cryptoCurrency()->getCode(),
            'Created At' => $payOut->createdAt(),
        ];

        toSlack('Long processing time for PayOut #' . $payOut->id() . '!', $data);

        return $this;
    }
}
