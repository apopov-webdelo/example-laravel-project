<?php

namespace App\Jobs\Utils;

use App\Contracts\Services\Messaging\Telegram\TelegramSendMessageContract;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class TelegramSendMessageJob
 *
 * @package App\Jobs\Utils
 */
class TelegramSendMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * How many times job will be repushed with delay
     */
    const MAX_TRIES = 5;

    /**
     * Seconds to delay each retry in sequence
     *
     * @var int
     */
    const DELAY_RETRY = 10;

    /**
     * @var
     */
    public $failedCounter;

    /**
     * @var array
     */
    public $record;

    /**
     * @var string
     */
    public $config;

    /**
     * Create a new job instance.
     *
     * @param array $config
     * @param array $record
     * @param int   $failedCount
     */
    public function __construct(array $config, array $record, int $failedCount = 0)
    {
        $this->config = $config;
        $this->record = $record;
        $this->failedCounter = $failedCount;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        /** @var TelegramSendMessageContract $service */
        $service = app(TelegramSendMessageContract::class, ['config' => $this->config]);
        $result = $service->send($this->record);

        if (!$result) {
            $this->jobFailed();
        }
    }

    /**
     * Restart job on fail
     */
    private function restartJob()
    {
        TelegramSendMessageJob::dispatch($this->config, $this->record, $this->failedCounter)
                              ->delay(now()->addSeconds(static::DELAY_RETRY));
    }

    /**
     * @throws \Exception
     */
    private function jobFailed()
    {
        $this->failedCounter++;

        if ($this->failedCounter >= static::MAX_TRIES) {
            $message = "TelegramSendMessageJob failed {$this->failedCounter} times. Check app logs.";

            toSlack($message, $this->record);

            throw new \Exception($message . json_encode($this->record));
        }

        $this->restartJob();
    }
}
