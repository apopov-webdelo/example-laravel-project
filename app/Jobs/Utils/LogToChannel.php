<?php

namespace App\Jobs\Utils;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class LogToChannel
 *
 * @package App\Jobs\Utils
 */
class LogToChannel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $data;
    /**
     * @var string
     */
    protected $channel;

    /**
     * @var string
     */
    protected $level = 'critical';

    /**
     * Create a new job instance.
     *
     * @param string $channel
     * @param string $message
     * @param array  $data
     */
    public function __construct(string $channel, string $message, array $data = [])
    {
        $this->message = $message;
        $this->data = $data;
        $this->channel = $channel;
    }

    /**
     * @param $level
     *
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::channel($this->channel)->{$this->level}($this->message, $this->data);
    }
}
