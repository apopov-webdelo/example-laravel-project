<?php

namespace App\Jobs\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Laravel\Passport\Token;

/**
 * Class RevokeExpiredTokens
 *
 * @package App\Jobs\Auth
 */
class RevokeExpiredTokens implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Token::query()->where('revoked', false)
             ->where('expires_at', '<=', now())
             ->each(function ($token) {
                 $token->revoke();
             });
    }
}
