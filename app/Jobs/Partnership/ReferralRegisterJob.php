<?php

namespace App\Jobs\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\ReferralRegisterContract;
use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ReferralRegisterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User $user
     */
    private $user;

    /**
     * @var PartnerContract $partner
     */
    private $partner;

    /**
     * ReferralRegisterJob constructor.
     *
     * @param User            $user
     * @param PartnerContract $partner
     */
    public function __construct(User $user, PartnerContract $partner)
    {
        $this->user    = $user;
        $this->partner = $partner;
    }

    /**
     * @param ReferralRegisterContract $service
     */
    public function handle(ReferralRegisterContract $service)
    {
        /** @var User $user */
        $user    = $this->user;
        /** @var User $partner */
        $partner = $this->partner->getUser();
        /** @var Log $channel */
        $channel = Log::channel('partnership');

        if ($service->register($user, $this->partner)) {
            $channel->info("Referral by user {$user->login} successfully registered for partner {$partner->id}");
        } else {
            $channel->debug("Referral by user {$user->login} wasn't registered for partner {$partner->id}");
        }
    }
}
