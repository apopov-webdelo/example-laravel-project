<?php

namespace App\Jobs\Blockchain;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\PayInTransactionRepoContract;
use App\Contracts\Repositories\PayOutRepoContract;
use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Models\Balance\PayInTransaction;
use App\Models\Balance\PayOut;
use App\Models\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CompareBalanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var WalletContract
     */
    protected $walletService;

    /**
     * @var User
     */
    public $user;

    /**
     * @var CryptoCurrencyContract
     */
    public $crypto;

    /**
     * Create a new job instance.
     *
     * @param User                   $user
     * @param CryptoCurrencyContract $crypto
     */
    public function __construct(User $user, CryptoCurrencyContract $crypto)
    {
        $this->user = $user;
        $this->crypto = $crypto;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function handle()
    {
        if ($this->noPendingTransactions()) {
            $this->walletService = app(WalletContract::class);

            $boxBalance = currencyToCoins(
                $this->walletService->getAmount($this->user, $this->crypto),
                $this->crypto
            );

            $siteBalance = $this->user->getBalance($this->crypto)->amount;

            if ($this->balanceIncorrect($boxBalance, $siteBalance)) {
                $message = "Bad balance | userId: {$this->user->id}";
                $data = [
                    'userId'      => $this->user->id,
                    'boxBalance'  => $boxBalance,
                    'siteBalance' => $siteBalance,
                    'crypto'      => $this->crypto->getCode(),
                ];

                toJira($message, $data);
                toSlack($message, $data);
            }
        }
    }

    /**
     * @param int $boxBalance
     * @param int $siteBalance
     *
     * @return bool
     */
    private function balanceIncorrect(int $boxBalance, int $siteBalance)
    {
        return $boxBalance !== $siteBalance;
    }

    /**
     * @return bool
     */
    protected function noPendingTransactions()
    {
        /* @var PayInTransactionRepoContract $payInRepo */
        $payInRepo = app(PayInTransactionRepoContract::class);

        /* @var PayOutRepoContract $payInRepo */
        $payOutRepo = app(PayOutRepoContract::class);

        $pendingInCount = $payInRepo->filterByUser($this->user)
                                    ->filterByStatus(PayInTransaction::STATUS_PENDING)
                                    ->take()
                                    ->count();

        $pendingOutCount = $payOutRepo->filterByUser($this->user)
                                      ->filterByStatus(PayOut::STATUS_PENDING)
                                      ->take()
                                      ->count();

        return $pendingInCount === 0 && $pendingOutCount === 0;
    }
}
