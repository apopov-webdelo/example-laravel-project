<?php

namespace App\Jobs\Deal;

use App\Exceptions\Deal\DealException;
use App\Repositories\Deal\DealRepo;
use App\Services\Deal\AutocancelDealService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

/**
 * Find and cancel expired deals
 *
 * @package App\Jobs\Deal
 */
class AutocancelDeal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @param DealRepo $dealRepo
     * @param AutocancelDealService $service
     */
    public function handle(DealRepo $dealRepo, AutocancelDealService $service)
    {
        $filter = $dealRepo->filterExpired();
        $filter->take()->chunk(20, function ($deals) use ($service) {
            foreach ($deals as $deal) {
                try {
                    $service->cancellation($deal);
                } catch (DealException $exception) {
                    Log::error($exception->getMessage());
                }
            }
        });
    }
}
