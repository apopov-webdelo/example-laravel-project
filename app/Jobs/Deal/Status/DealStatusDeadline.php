<?php

namespace App\Jobs\Deal\Status;

use App\Models\Deal\Deal;
use App\Models\Deal\DealStatus;
use App\Repositories\Deal\DealRepo;
use App\Traits\Cache\CachesWithTimeout;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class DealStatusDeadline implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, CachesWithTimeout;

    const DEFAULT_DEADLINE = 3;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $configKey;

    /**
     * @param DealRepo $dealRepo
     */
    public function handle(DealRepo $dealRepo)
    {
        $this->checkStatus();

        $this->checkConfigKey();

        $processingDeadlineTime = config($this->configKey) ?? self::DEFAULT_DEADLINE;

        $items = $dealRepo
            ->filterByStatus($this->status)
            ->take()->where('created_at', '<', now()->subMinutes($processingDeadlineTime))
            ->get();

        $items = $this->onlyNotCached($items);

        $items->each(function ($payOut) {
            $this->cacheTimeout($payOut);
            $this->notify($payOut);
        });

        // Log::info(
        //     'Checked deadline for Deal [' . DealStatus::find($this->status)->title . '].' .
        //     'Was processed ' . $items->count() . ' deals.'
        // );
    }

    /**
     * Send notification to Slack
     *
     * @param Deal $deal
     *
     * @return $this
     */
    protected function notify(Deal $deal)
    {
        $data = [
            'ID'      => $deal->id,
            'Crypto'  => $deal->ad->cryptoCurrency->getCode(),
            'Created' => $deal->created_at,
        ];

        toSlack('Long [' . DealStatus::find($this->status)->title . '] status for Deal #' . $deal->id . '!', $data);

        return $this;
    }

    /**
     * Validate has status set
     */
    private function checkStatus()
    {
        if (!$this->status) {
            throw new \InvalidArgumentException('Deal status must be defined.');
        }
    }

    /**
     * Validate has config key set
     */
    private function checkConfigKey()
    {
        if (!$this->configKey) {
            throw new \InvalidArgumentException('Deal configKey must be defined.');
        }
    }
}
