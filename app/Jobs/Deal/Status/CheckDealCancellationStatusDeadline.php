<?php

namespace App\Jobs\Deal\Status;

use App\Models\Deal\DealStatusConstants;

/**
 * Class CheckDealCancellationStatusDeadline
 *
 * Checks if deal is in CANCELLATION status for very long time
 *
 * @package App\Jobs\Deal\Status
 */
class CheckDealCancellationStatusDeadline extends DealStatusDeadline
{
    /**
     * @var int
     */
    protected $status = DealStatusConstants::CANCELLATION;

    /**
     * @var string
     */
    protected $configKey = 'app.deal.cancellation_deadline_time';
}
