<?php

namespace App\Jobs\Deal\Status;

use App\Models\Deal\DealStatusConstants;

/**
 * Class CheckDealFinishingStatusDeadline
 *
 * Checks if deal is in FINISHING status for very long time
 *
 * @package App\Jobs\Deal\Status
 */
class CheckDealFinishingStatusDeadline extends DealStatusDeadline
{
    /**
     * @var int
     */
    protected $status = DealStatusConstants::FINISHING;

    /**
     * @var string
     */
    protected $configKey = 'app.deal.finishing_deadline_time';
}
