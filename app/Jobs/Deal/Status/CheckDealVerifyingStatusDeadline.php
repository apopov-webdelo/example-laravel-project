<?php

namespace App\Jobs\Deal\Status;

use App\Models\Deal\DealStatusConstants;

/**
 * Class CheckDealVerifyingStatusDeadline
 *
 * Checks if deal is in VERIFICATION status for very long time
 *
 * @package App\Jobs\Deal\Status
 */
class CheckDealVerifyingStatusDeadline extends DealStatusDeadline
{
    /**
     * @var int
     */
    protected $status = DealStatusConstants::VERIFICATION;

    /**
     * @var string
     */
    protected $configKey = 'app.deal.verifying_deadline_time';
}
