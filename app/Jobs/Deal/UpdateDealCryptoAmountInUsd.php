<?php

namespace App\Jobs\Deal;

use App\Facades\Exchange;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Directory\Currency;
use App\Repositories\Directory\CurrencyRepo;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateDealCryptoAmountInUsd implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Deal
     */
    protected $deal;

    /**
     * @var Currency
     */
    private $currencyUsd;

    /**
     * Create a new job instance.
     *
     * @param Deal         $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;

        $this->currencyUsd = app(CurrencyRepo::class)->take()
                                          ->where('code', 'USD')
                                          ->get()
                                          ->first();
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        /* @var Ad $ad */
        $ad = $this->deal->ad;

        try {
            $currencyCode = $ad->currency->getCode();
            $fiatAmount = $this->deal->fiat_amount;

            if ($currencyCode === $this->currencyUsd->getCode()) {
                $this->deal->crypto_amount_in_usd = $fiatAmount;
                $this->deal->save();

                return;
            }

            $amountUsd = Exchange::convert(
                currencyFromCoins($fiatAmount, $ad->currency),
                $currencyCode,
                $this->currencyUsd->getCode()
            );

            $amountUsd = currencyToCoins($amountUsd, $this->currencyUsd);

            if ($amountUsd) {
                $this->deal->crypto_amount_in_usd = $amountUsd;
                $this->deal->save();
            }
        } catch (\Exception $e) {
            throw new \Exception(
                'Error updating "crypto_amount_in_usd" in job: ' . $e->getMessage()
                . ' Deal id = ' . $this->deal->id . ' | Ad = ' . ($ad ?: 'null')
            );
        }
    }
}
