<?php

namespace App\Jobs\Ad;

use App\Exceptions\Ad\AdException;
use App\Repositories\Ad\AdRepo;
use App\Services\Ad\DeactivateAdService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

/**
 * Find and deactivate ads by inactive authors
 *
 * @package App\Jobs\Ad
 */
class DeactivateAd implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Deactivate Ads by inactive authors
     *
     * @param AdRepo $adRepo
     * @param DeactivateAdService $service
     */
    public function handle(AdRepo $adRepo, DeactivateAdService $service)
    {
        $filter = $adRepo->filterForActive()->filterByInactiveAuthor();
        $filter->take()->chunk(20, function ($ads) use ($service) {
            foreach ($ads as $ad) {
                try {
                    $service->deactivate($ad);
                } catch (AdException $exception) {
                    Log::error($exception->getMessage());
                }
            }
        });
    }
}
