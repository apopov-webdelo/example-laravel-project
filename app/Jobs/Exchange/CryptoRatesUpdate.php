<?php

namespace App\Jobs\Exchange;

use App\Facades\CryptoExchange;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\FiatConstants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CryptoRatesUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Currencies needed for recache
     *
     * @var array
     */
    protected $fiatCurrencies = [
        FiatConstants::USD,
        FiatConstants::EUR,
    ];

    /**
     * Recache all cryptocurrency rates
     *
     * @throws \App\Exceptions\Exchange\UnknownCurrencyCodeException
     */
    public function handle()
    {
        foreach (CryptoCurrency::all() as $cryptoCurrency) {
            $this->updateRatesForCryptoCurrency($cryptoCurrency);
        }
    }

    /**
     * Update rate cache using
     *
     * @param CryptoCurrency $cryptoCurrency
     *
     * @return $this
     *
     * @throws \App\Exceptions\Exchange\UnknownCurrencyCodeException
     */
    protected function updateRatesForCryptoCurrency(CryptoCurrency $cryptoCurrency)
    {
        foreach ($this->fiatCurrencies as $currency) {
            CryptoExchange::market('average')->rate(
                $cryptoCurrency,
                $currency,
                false
            );
        }

        return $this;
    }
}
