<?php

namespace App\Jobs\Exchange;

use App\Contracts\Exchange\Fiat\ExchangeContract;
use App\Models\Directory\FiatConstants;
use App\Services\Exchange\Currency\AverageFiatExchange;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class FiatRatesJob
 *
 * @package App\Jobs\Exchange
 */
abstract class FiatRatesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $USD = FiatConstants::USD;

    /**
     * @var ExchangeContract
     */
    protected $service;

    /**
     * @var array
     */
    protected $markets;

    /**
     * @param array $markets
     *
     * @return mixed
     */
    protected function removeAverageMarket(array $markets)
    {
        unset($markets[AverageFiatExchange::SELF_CONFIG]);

        return $markets;
    }

    /**
     * @param array $markets
     *
     * @return array
     */
    protected function onlyBatchUpdateMarkets(array $markets)
    {
        return array_filter($markets, function ($market) {
            return array_key_exists('batch_updates', $market) && $market['batch_updates'] === true;
        });
    }

    /**
     * @param array $markets
     *
     * @return array
     */
    protected function filterBatchUpdateMarkets(array $markets)
    {
        return array_filter($markets, function ($market) {
            return !array_key_exists('batch_updates', $market) || $market['batch_updates'] !== true;
        });
    }
}
