<?php

namespace App\Jobs\Exchange;

use App\Contracts\Exchange\Fiat\ExchangeContract;
use App\Contracts\Exchange\Fiat\FiatBatchUpdateContract;
use App\Factories\FiatExchangeFactory;
use App\Services\Exchange\Currency\AverageFiatExchange;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * Class FiatRatesBatchUpdate
 *
 * For services that support batch rate requests
 *
 * @package App\Jobs\Exchange
 */
class FiatRatesBatchUpdate extends FiatRatesJob
{
    /**
     * Handle job
     */
    public function handle()
    {
        $this->service = app(FiatExchangeFactory::class);

        $this->markets = config(AverageFiatExchange::CONFIG_MARKETS_KEY);

        $this->markets = $this->removeAverageMarket($this->markets);

        $this->markets = $this->onlyBatchUpdateMarkets($this->markets);

        $currencies = $this->service->currencies()->filter(function ($item) {
            return $item !== $this->USD;
        });

        $this->update($currencies);
    }

    /**
     * Refresh all rates on single driver
     *
     * @param Collection $currencies
     */
    private function update(Collection $currencies)
    {
        foreach ($this->markets as $market => $marketConfig) {
            try {
                /* @var ExchangeContract|FiatBatchUpdateContract $driver */
                $driver = $this->service->market($market);

                if ($this->hasBatchUpdateContact($driver, $marketConfig)) {
                    $driver->batchUpdate($this->USD, $currencies);
                }
            } catch (\Exception $e) {
                Log::alert(
                    'Error in FiatRatesBatchUpdate job [' . $marketConfig['driver'] . '] !',
                    ['message' => $e->getMessage()]
                );
            }
        }
    }

    /**
     * @param ExchangeContract $driver
     * @param array            $marketConfig
     *
     * @return bool
     */
    private function hasBatchUpdateContact(ExchangeContract $driver, array $marketConfig)
    {
        if ($driver instanceof FiatBatchUpdateContract) {
            return true;
        }

        Log::alert('Error [' . $marketConfig['driver'] . '] does not support FiatBatchUpdateContract!');

        return false;
    }
}
