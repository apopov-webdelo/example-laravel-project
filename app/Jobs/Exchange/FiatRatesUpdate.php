<?php

namespace App\Jobs\Exchange;

use App\Factories\FiatExchangeFactory;
use App\Services\Exchange\Currency\AverageFiatExchange;
use Illuminate\Support\Facades\Log;

/**
 * Class FiatRatesUpdate
 *
 * @package App\Jobs\Exchange
 */
class FiatRatesUpdate extends FiatRatesJob
{
    /**
     * Handle job
     */
    public function handle()
    {
        $this->service = app(FiatExchangeFactory::class);

        $this->markets = config(AverageFiatExchange::CONFIG_MARKETS_KEY);

        $this->markets = $this->removeAverageMarket($this->markets);

        $this->markets = $this->filterBatchUpdateMarkets($this->markets);

        $this->service->currencies()->filter(function ($item) {
            return $item !== $this->USD;
        })->each(function ($item) {
            $this->update($item);
        });
    }

    /**
     * Refresh single rate on all drivers
     *
     * @param $item
     *
     * @throws \App\Exceptions\Exchange\ExchangeException
     */
    private function update($item)
    {
        foreach ($this->markets as $market => $marketConfig) {
            try {
                $driver = $this->service->market($market);
                $driver->rate($item, $this->USD, false);
            } catch (\Exception $exception) {
                Log::alert(
                    'Error in FiatRatesUpdate job [' . $marketConfig['driver'] . '] !',
                    ['message' => $exception->getMessage()]
                );
            }
        }

        $this->service->market('average')
                      ->rate($item, $this->USD, false);
    }
}
