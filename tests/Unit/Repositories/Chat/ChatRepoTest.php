<?php

namespace Tests\Unit\Repositories\Chat;

use App\Makers\Chat\ChatMaker;
use App\Models\Chat\Chat;
use App\Repositories\Chat\ChatRepo;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class ChatRepoTest extends TestCase
{
    use AdHelpers, DealHelpers;

    /** @test */
    public function testNoPrivateChatWhenHasDealChat()
    {
        $user1 = $this->registerUser(['login' => 'user1']);
        $user2 = $this->registerUser(['login' => 'user2']);

        $this->makeAd($user1);
        $ad = $this->lastAd($user1);
        $this->makeDeal($user2, $ad);

        $chat = ChatMaker::init(['author_id' => $user2])
                         ->author($user2)
                         ->withRecepient($user2)
                         ->withRecepient($user1)
                         ->withMessages($user1, $user2)
                         ->create();

        $chat->deals()->attach($this->lastDeal($user2));

        /* @var ChatRepo $repo */
        $repo = app(ChatRepo::class);

        /** @var Chat|null $chat */
        $chat = $repo->getUsersChat($user1, $user2);

        $this->assertEquals(null, $chat);
    }

    /** @test */
    public function testHasPrivateChatWhenNoDeals()
    {
        $user1 = $this->registerUser(['login' => 'user1']);
        $user2 = $this->registerUser(['login' => 'user2']);

        ChatMaker::init(['author_id' => $user2])
                         ->author($user2)
                         ->withRecepient($user2)
                         ->withRecepient($user1)
                         ->withMessages($user1, $user2)
                         ->create();

        /* @var ChatRepo $repo */
        $repo = app(ChatRepo::class);

        /** @var Chat|null $chat */
        $chat = $repo->getUsersChat($user1, $user2);

        $this->assertInstanceOf(Chat::class, $chat);
    }
}
