<?php

namespace Tests\Unit\Repositories\Diretory;

use App\Contracts\Repositories\FlushRepoCacheContract;
use App\Contracts\Repositories\FullDirectoryRepoContract;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Repositories\Directory\CachedFullDirectoryRepo;
use App\Repositories\Directory\FullDirectoryRepo;
use Illuminate\Contracts\Cache\Repository as Cache;
use Tests\TestCase;

class CachedFullDirectoryRepoTest extends TestCase
{
    const TAG = 'directory-all';

    /** @test */
    public function testInstantiate()
    {
        /* @var CachedFullDirectoryRepo $service */
        $service = app(CachedFullDirectoryRepo::class);

        $this->assertInstanceOf(FullDirectoryRepoContract::class, $service);
        $this->assertInstanceOf(FlushRepoCacheContract::class, $service);
        $this->assertInstanceOf(CachedFullDirectoryRepo::class, $service);
    }
    
    /** @test */
    public function testGetCountries()
    {
        /* @var CachedFullDirectoryRepo $service */
        $service = app(CachedFullDirectoryRepo::class);

        // act
        $items = $service->countries();

        // assert
        $this->assertCount(Country::count(), $items);
    }

    /** @test */
    public function testGetCryptos()
    {
        /* @var CachedFullDirectoryRepo $service */
        $service = app(CachedFullDirectoryRepo::class);

        // act
        $items = $service->cryptoCurrencies();

        // assert
        $this->assertCount(CryptoCurrency::count(), $items);
    }

    /** @test */
    public function testGetAll()
    {
        $repo = \Mockery::mock(FullDirectoryRepo::class);
        $repo->shouldReceive('all')->andReturn([]);

        $cache = \Mockery::mock(Cache::class);
        $cache->shouldReceive('tags')->with(static::TAG)->andReturn($cache);
        $cache->shouldReceive('remember')->andReturn([]);

        $repository = new CachedFullDirectoryRepo($repo, $cache);

        // act
        $items = $repository->all();

        // assert
        $this->assertCount(0, $items);
    }

    /** @test */
    public function testFlush()
    {
        /* @var CachedFullDirectoryRepo $service */
        $service = app(CachedFullDirectoryRepo::class);

        // act
        $service->cryptoCurrencies();
        $this->assertTrue($service->flush());
    }
}
