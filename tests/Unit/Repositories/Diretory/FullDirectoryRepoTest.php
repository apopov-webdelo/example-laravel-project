<?php

namespace Tests\Unit\Repositories\Diretory;

use App\Contracts\Repositories\FullDirectoryRepoContract;
use App\Http\Resources\Client\Directory\Country\CountryFullResource;
use App\Http\Resources\Client\Directory\CryptoCurrency\CryptoCurrencyResource;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Repositories\Directory\FullDirectoryRepo;
use Tests\TestCase;

class FullDirectoryRepoTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var FullDirectoryRepoContract $service */
        $service = app(FullDirectoryRepoContract::class);

        $this->assertInstanceOf(FullDirectoryRepoContract::class, $service);
        $this->assertInstanceOf(FullDirectoryRepo::class, $service);
    }

    /** @test */
    public function testHasCountries()
    {
        /* @var FullDirectoryRepoContract $service */
        $service = app(FullDirectoryRepoContract::class);

        $countries = $service->countries();

        $this->assertEquals(Country::all(), $countries);
    }

    /** @test */
    public function testHasCryptoCurrencies()
    {
        /* @var FullDirectoryRepoContract $service */
        $service = app(FullDirectoryRepoContract::class);

        $countries = $service->cryptoCurrencies();

        $this->assertEquals(CryptoCurrency::all(), $countries);
    }

    /** @test */
    public function testHasAllResource()
    {
        /* @var FullDirectoryRepoContract $service */
        $service = app(FullDirectoryRepoContract::class);

        $all = $service->all();

        $this->assertEquals([
            'crypto_currencies' => CryptoCurrencyResource::collection(CryptoCurrency::all()),
            'countries'         => CountryFullResource::collection(Country::all()),
        ], $all);
    }
}
