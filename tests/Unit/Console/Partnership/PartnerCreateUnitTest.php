<?php

namespace Tests\Unit\Console\Partnership;

use App\Makers\User\UserMaker;
use App\Models\User\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PartnerCreateUnitTest extends TestCase
{
    //use DatabaseTransactions;

    /**
     * @var User
     */
    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = UserMaker::init()->active()->create();
    }

    /**
     * Test check that command expect User id or login
     *
     * @return void
     */
    public function testValidExpectationUserIds()
    {
        $this->artisan('partner:create')
             ->expectsOutput('Please specify --login or --userId')
             ->assertExitCode(0);
    }

    /**
     * Test is artisan command create partner with valid data
     *
     * @return void
     */
    public function testPartnerCreation()
    {
        $this->assertFalse(is_user_partner($this->user));

        $this->artisan('partner:create', ['--userId' => $this->user->id])
             ->expectsOutput('Successfully created new partner.')
             ->assertExitCode(0);

        $this->assertTrue(is_user_partner($this->user));
    }
}
