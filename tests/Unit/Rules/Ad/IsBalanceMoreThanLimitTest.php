<?php

namespace Tests\Unit\Rules\Ad;

use App\Models\Directory\CommissionConstants;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Rules\Ad\IsBalanceMoreThanLimit;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class IsBalanceMoreThanLimitTest extends TestCase
{
    use AdHelpers, BalanceHelpers, DealHelpers;

    /** @test */
    public function testAdNotCreatedOverLimit()
    {
        $crypto = $this->getRandomCrypto();
        $user = $this->registerUser(['login' => 'userAd']);
        $this->addBalance($user, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $rule = app(
            IsBalanceMoreThanLimit::class,
            [
                'limit'      => 1000,
                'price'      => 1000,
                'balance'    => $user->getBalance($crypto)->accuracyAmount,
                'commission' => $user->getBalance($crypto)->accuracyAmount,
            ]
        );

        $result = $rule->check();

        $this->assertFalse($result); // price + 1%
    }

    /** @test */
    public function testAdCreateWithOkLimit()
    {
        $crypto = $this->getRandomCrypto();
        $user = $this->registerUser(['login' => 'userAd']);
        $this->addBalance(
            $user,
            $crypto,
            CryptoCurrencyConstants::ONE_BTC
            + (CryptoCurrencyConstants::ONE_BTC * 1 / CommissionConstants::ONE_PERCENT) // +1%
        );

        $rule = app(
            IsBalanceMoreThanLimit::class,
            [
                'limit'      => 1000,
                'price'      => 1000,
                'balance'    => $user->getBalance($crypto)->accuracyAmount,
                'commission' => $user->getBalance($crypto)->accuracyCommission,
            ]
        );

        $result = $rule->check();

        $this->assertTrue($result);
    }

    /** @test */
    public function testAdOverLimitZeroBalance()
    {
        $crypto = $this->getRandomCrypto();
        $user = $this->registerUser(['login' => 'userAd']);

        $this->makeAd($user, [
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'commission_percent' => 100 // 1%
        ]);

        $ad = $this->lastAd($user);

        $rule = app(
            IsBalanceMoreThanLimit::class,
            [
                'limit'      => 1000,
                'price'      => $ad->price,
                'balance'    => $user->getBalance($crypto)->accuracyAmount,
                'commission' => $ad->accuracyCommissionPercent,
            ]
        );

        $result = $rule->check();

        $this->assertFalse($result);
    }

    /** @test */
    public function testAdWithOkLimit()
    {
        $crypto = $this->getRandomCrypto();
        $user = $this->registerUser(['login' => 'userAd']);
        $this->addBalance(
            $user,
            $crypto,
            CryptoCurrencyConstants::ONE_BTC
            + (CryptoCurrencyConstants::ONE_BTC * 1 / CommissionConstants::ONE_PERCENT) // +1%
        );

        $this->makeAd($user, [
            'crypto_currency_id' => $crypto,
            'price'              => CryptoCurrencyConstants::ONE_BTC,
            'commission_percent' => 100 // 1%
        ]);

        $ad = $this->lastAd($user);

        $rule = app(
            IsBalanceMoreThanLimit::class,
            [
                'limit'      => CryptoCurrencyConstants::ONE_BTC,
                'price'      => $ad->price,
                'balance'    => $user->getBalance($crypto)->accuracyAmount,
                'commission' => $ad->accuracyCommissionPercent,
            ]
        );

        $result = $rule->check();

        $this->assertTrue($result);
    }

    /** @test */
    public function testAdOverLimitNotZeroBalance()
    {
        $crypto = $this->getRandomCrypto();
        $user = $this->registerUser(['login' => 'userAd']);
        $this->addBalance(
            $user,
            $crypto,
            CryptoCurrencyConstants::ONE_BTC
            + (CryptoCurrencyConstants::ONE_BTC * 1 / CommissionConstants::ONE_PERCENT) // +1%
        );

        $this->makeAd($user, [
            'crypto_currency_id' => $crypto,
            'price'              => CryptoCurrencyConstants::ONE_BTC,
            'commission_percent' => 500 // 5%
        ]);

        $ad = $this->lastAd($user);

        $rule = app(
            IsBalanceMoreThanLimit::class,
            [
                'limit'      => CryptoCurrencyConstants::ONE_BTC,
                'price'      => $ad->price,
                'balance'    => $user->getBalance($crypto)->accuracyAmount,
                'commission' => $ad->accuracyCommissionPercent,
            ]
        );

        $result = $rule->check();

        $this->assertFalse($result);
    }
}
