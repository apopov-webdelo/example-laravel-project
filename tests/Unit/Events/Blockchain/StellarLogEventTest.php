<?php

namespace Tests\Unit\Events\Blockchain;

use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Listeners\Blockchain\LogErrorStellarRequest;
use App\Listeners\Blockchain\LogStellarRequest;
use App\Models\User\User;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class StellarLogEventTest extends TestCase
{
    protected $reqData = [
        'crypto_type'    => 'btc',
        'amount '        => 0.0001,
        'balance_amount' => 0.0001,
        'hash'           => 'bad-hash',
        'transaction_id' => 'tx456666',
    ];

    protected $route;

    /** @test */
    public function testQueuesJobOnFromRequest()
    {
        Queue::fake();

        // act
        $user = factory(User::class)->create();
        $this->route = route('blockchain.transaction.deposit', ['user' => $user]);

        // act on public route
        $this->json('POST', $this->route, $this->reqData);

        // assert
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == LogStellarRequest::class;
        });
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == LogErrorStellarRequest::class;
        });
    }

    /** @test */
    public function testOnToRequestFailed()
    {
        Queue::fake();

        $user = factory(User::class)->make();

        /* @var WalletContract $service */
        $service = app(WalletContract::class);

        try {
            $service->getWallet($user, $this->getCryptoByCode('btc'));
        } catch (\Exception $e) {
        }

        // assert
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == LogStellarRequest::class;
        });
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == LogErrorStellarRequest::class;
        });
    }

    /**
     * @test
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function testOnToRequestSuccess()
    {
        Queue::fake();

        $user = factory(User::class)->make();

        // mock guzzle
        $this->app->bind(ClientInterface::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode(['wallet_id' => '123'])),
            ]);

            $handler = HandlerStack::create($mock);
            return new Client(['handler' => $handler]);
        });

        /* @var WalletContract $service */
        $service = app(WalletContract::class);

        $service->getWallet($user, $this->getCryptoByCode('btc'));

        // assert
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == LogStellarRequest::class;
        });
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == LogErrorStellarRequest::class;
        });
    }
}
