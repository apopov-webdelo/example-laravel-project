<?php

namespace Tests\Unit\Jobs\Blockchain;

use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Facades\EmergencyAlert;
use App\Jobs\Blockchain\CompareBalanceJob;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use Illuminate\Support\Facades\Queue;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestCase;

class CompareBalanceJobTest extends TestCase
{
    use BalanceHelpers;

    /**
     * @var User
     */
    private $user;

    /**
     * @var CryptoCurrencyContract
     */
    private $crypto;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->registerUser();
        $this->crypto = $this->getRandomCrypto();
    }

    /**
     * @param       $class
     * @param array $methods
     */
    protected function mockClass($class, $methods = [])
    {
        $this->app->bind($class, function () use ($class, $methods) {
            $mock = \Mockery::mock($class)->makePartial();

            return $mock->allows($methods);
        });
    }

    /**
     * @param       $class
     * @param array $methods
     */
    protected function mockNotCalled($class, $methods = [])
    {
        $this->app->bind($class, function () use ($class, $methods) {
            $mock = \Mockery::mock($class)->makePartial();

            return $mock->shouldNotHaveReceived($methods);
        });
    }

    /**
     * @param array $methods
     */
    protected function mockWalletService($methods = [])
    {
        $this->mockClass(WalletContract::class, $methods);
    }

    /** @test */
    public function testDispatch()
    {
        Queue::fake();
        CompareBalanceJob::dispatch($this->user, $this->crypto);

        Queue::assertPushed(CompareBalanceJob::class, function ($job) {
            return $job->user->id === $this->user->id
                && $job->crypto->getCode() === $this->crypto->getCode();
        });
    }

    /** @test */
    public function testWhenBalanceCorrect()
    {
        $this->addBalance($this->user, $this->crypto, CryptoCurrencyConstants::ONE_BTC);

        $this->mockWalletService([
            'getAmount' => 1,
        ]);

        // job dispath
        dispatch(app(CompareBalanceJob::class, ['user' => $this->user, 'crypto' => $this->crypto]));
    }

    /** @test */
    public function testWhenBadBalance()
    {
        $this->addBalance($this->user, $this->crypto, 0);

        $this->mockWalletService([
            'getAmount' => 1,
        ]);

        EmergencyAlert::shouldReceive('channel->send')->twice(); // slack + jira

        // job dispath
        dispatch(app(CompareBalanceJob::class, ['user' => $this->user, 'crypto' => $this->crypto]));
    }
}
