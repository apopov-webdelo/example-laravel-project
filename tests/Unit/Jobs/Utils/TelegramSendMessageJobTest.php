<?php

namespace Tests\Unit\Jobs\Utils;

use App\Contracts\Services\Messaging\Telegram\TelegramSendMessageContract;
use App\Jobs\Utils\TelegramSendMessageJob;
use App\Services\Messaging\Telegram\TelegramSendMessageService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Queue;
use Monolog\Logger;
use Tests\TestCase;

class TelegramSendMessageJobTest extends TestCase
{
    protected function getTestData()
    {
        return [
            config('logging.channels.telegram_deals'),
            [
                'message'    => 'message body',
                'context'    => ['some' => 'data', 'more' => 'data etc'],
                'channel'    => 'Deals',
                'level_name' => 'INFO',
                'level'      => Logger::INFO,
            ],
        ];
    }

    /** @test */
    public function testInstantiate()
    {
        /** @var TelegramSendMessageJob $job */
        list($config, $record) = $this->getTestData();
        $job = app(TelegramSendMessageJob::class, ['config' => $config, 'record' => $record]);

        $this->assertInstanceOf(TelegramSendMessageJob::class, $job);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testFailedJobIsQueuedAgain()
    {
        Queue::fake();

        // mock guzzle
        $this->app->bind(ClientInterface::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode([])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /** @var TelegramSendMessageJob $job */
        list($config, $record) = $this->getTestData();
        $job = app(TelegramSendMessageJob::class, ['config' => $config, 'record' => $record]);

        $job->handle();

        Queue::assertPushed(
            TelegramSendMessageJob::class,
            function (TelegramSendMessageJob $job) use ($record, $config) {
                return $job->record === $record
                    && $job->config === $config
                    && $job->failedCounter === 1;
            }
        );
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testJobsNotRepushedAfterMaxFailedCounter()
    {
        Queue::fake();

        // mock guzzle
        $this->app->bind(ClientInterface::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode([])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /** @var TelegramSendMessageJob $job */
        list($config, $record) = $this->getTestData();
        $job = app(
            TelegramSendMessageJob::class,
            ['config' => $config, 'record' => $record, 'failedCount' => TelegramSendMessageJob::MAX_TRIES - 1]
        );

        // assert
        $this->expectException(\Exception::class);

        // act
        $job->handle();

        // assert
        Queue::assertNotPushed(TelegramSendMessageJob::class);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testSendMessageServiceCalled()
    {
        /** @var TelegramSendMessageJob $job */
        list($config, $record) = $this->getTestData();
        $job = app(
            TelegramSendMessageJob::class,
            ['config' => $config, 'record' => $record, 'failedCount' => 0]
        );

        // mock
        $this->app->bind(TelegramSendMessageContract::class, function () use ($record) {
            $mock = \Mockery::mock(TelegramSendMessageService::class)
                            ->makePartial();

            // assert
            $mock->shouldReceive('send')
                 ->with($record)
                 ->once()
                 ->andReturnTrue();

            return $mock;
        });

        // act
        $job->handle();
    }
}
