<?php

namespace Tests\Unit\Jobs\Deal;

use App\Events\Deal\DealPlaced;
use App\Listeners\Deal\DealUpdateUsdAmount;
use App\Models\Deal\Deal;
use App\Models\Directory\Currency;
use App\Models\Directory\FiatConstants;
use Tests\TestCase;

class UpdateDealCryptoAmountInUsdUnitTest extends TestCase
{
    /** @test */
    public function testItFillsAmountUsd()
    {
        /** @var \App\Models\Ad\Ad $ad */
        $ad = \App\Makers\AdMaker::init([
            'currency_id' => Currency::where('code', FiatConstants::USD)->get()->first()
        ])->create();

        $deal = factory(Deal::class)->create(['ad_id' => $ad]);

        $listener = new DealUpdateUsdAmount;
        $listener->handle(new DealPlaced($deal));
        $deal = Deal::find($deal->id)->fresh();

        $this->assertNotEquals(0, $deal->crypto_amount_in_usd);
    }

}
