<?php

namespace Tests\Unit\Jobs\Deal\Status;

use App\Jobs\Deal\Status\CheckDealFinishingStatusDeadline;
use App\Models\Deal\DealStatusConstants;

class CheckDealFinishingStatusDeadlineTest extends DealStatusDeadlineTest
{
    /**
     * Must define testing status in each sub-test
     *
     * @var int
     */
    protected $status = DealStatusConstants::FINISHING;

    /**
     * Get concrete tested job class
     *
     * @return mixed
     */
    protected function getJob()
    {
        return app(CheckDealFinishingStatusDeadline::class);
    }

    /** @test */
    public function testNoAlertInCorrectPeriod()
    {
        $this->noAlertInCorrectPeriod();
    }

    /** @test */
    public function testAlertOnTimeOut()
    {
        $this->alertOnTimeOut();
    }

    /** @test */
    public function testCachedAlertsTimer()
    {
        $this->cachedAlertsTimer();
    }
}
