<?php

namespace Tests\Unit\Jobs\Deal\Status;

use App\Jobs\Deal\Status\DealStatusDeadline;
use App\Jobs\Utils\LogToChannel;
use App\Models\Deal\Deal;
use App\Repositories\Deal\DealRepo;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

abstract class DealStatusDeadlineTest extends TestCase
{
    /**
     * Must define testing status in each sub-test
     *
     * @var int
     */
    protected $status;

    /**
     * Get concrete tested job class
     *
     * @return mixed
     */
    abstract protected function getJob();

    protected function setUp()
    {
        parent::setUp();
        Queue::fake();
    }

    public function noAlertInCorrectPeriod()
    {
        /* @var Deal $deal */
        $deal = factory(Deal::class)->create();
        $deal->statuses()->attach($this->status);

        /* @var DealStatusDeadline $job */
        $job = $this->getJob();
        $job->handle(app(DealRepo::class));

        Queue::assertNotPushed(LogToChannel::class);
    }

    /** @ */
    public function alertOnTimeOut()
    {
        // exceeded time limit
        $period = DealStatusDeadline::DEFAULT_DEADLINE * 3;
        /* @var Deal $deal */
        $deal = factory(Deal::class)->create(['created_at' => now()->subMinutes($period)]);
        $deal->statuses()->attach($this->status);

        /* @var DealStatusDeadline $job */
        $job = $this->getJob();
        $job->handle(app(DealRepo::class));

        Queue::assertPushed(LogToChannel::class, 1);
    }

    /** @ */
    public function cachedAlertsTimer()
    {
        // exceeded time limit
        $period = DealStatusDeadline::DEFAULT_DEADLINE * 3;
        /* @var Deal $deal */
        $deal = factory(Deal::class)->create(['created_at' => now()->subMinutes($period)]);
        $deal->statuses()->attach($this->status);

        /* @var DealStatusDeadline $job */
        $job = $this->getJob();
        $job->handle(app(DealRepo::class)); // must cache here
        $job->handle(app(DealRepo::class)); // must no call notify second time
        $job->handle(app(DealRepo::class)); // must no call notify third time

        Queue::assertPushed(LogToChannel::class, 1);
    }
}
