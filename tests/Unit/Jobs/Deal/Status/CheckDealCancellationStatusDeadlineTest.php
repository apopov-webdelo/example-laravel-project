<?php

namespace Tests\Unit\Jobs\Deal\Status;

use App\Jobs\Deal\Status\CheckDealCancellationStatusDeadline;
use App\Models\Deal\DealStatusConstants;

class CheckDealCancellationStatusDeadlineTest extends DealStatusDeadlineTest
{
    /**
     * Must define testing status in each sub-test
     *
     * @var int
     */
    protected $status = DealStatusConstants::CANCELLATION;

    /**
     * Get concrete tested job class
     *
     * @return mixed
     */
    protected function getJob()
    {
        return app(CheckDealCancellationStatusDeadline::class);
    }

    /** @test */
    public function testNoAlertInCorrectPeriod()
    {
        $this->noAlertInCorrectPeriod();
    }

    /** @test */
    public function testAlertOnTimeOut()
    {
        $this->alertOnTimeOut();
    }

    /** @test */
    public function testCachedAlertsTimer()
    {
        $this->cachedAlertsTimer();
    }
}
