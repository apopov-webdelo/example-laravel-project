<?php

namespace Tests\Unit\Jobs\Balance\PayOut;

use App\Contracts\Repositories\PayOutRepoContract;
use App\Jobs\Balance\PayOut\CheckPayOutProcessingDeadline;
use App\Jobs\Utils\LogToChannel;
use App\Models\Balance\PayOut;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class CheckPayOutProcessingDeadlineTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Queue::fake();
    }

    /** @test */
    public function testNoAlertInCorrectPeriod()
    {
        factory(PayOut::class)->create([
            'created_at' => now(),
            'status'     => 'pending',
        ]);

        /* @var CheckPayOutProcessingDeadline $job */
        $job = app(CheckPayOutProcessingDeadline::class);
        $job->handle(app(PayOutRepoContract::class));

        Queue::assertNotPushed(LogToChannel::class);
    }

    /** @test */
    public function testAlertOnTimeOut()
    {
        // exceeded time limit
        $period = CheckPayOutProcessingDeadline::DEFAULT_DEADLINE * 3;
        factory(PayOut::class)->create([
            'created_at' => now()->subMinutes($period),
            'status'     => 'pending',
        ]);

        /* @var CheckPayOutProcessingDeadline $job */
        $job = app(CheckPayOutProcessingDeadline::class);
        $job->handle(app(PayOutRepoContract::class));

        Queue::assertPushed(LogToChannel::class, 1);
    }

    /** @test */
    public function testCachedAlertsTimer()
    {
        // exceeded time limit
        $period = CheckPayOutProcessingDeadline::DEFAULT_DEADLINE * 3;
        factory(PayOut::class)->create([
            'created_at' => now()->subMinutes($period),
            'status'     => 'pending',
        ]);

        /* @var CheckPayOutProcessingDeadline $job */
        $job = app(CheckPayOutProcessingDeadline::class);
        $job->handle(app(PayOutRepoContract::class)); // must cache here
        $job->handle(app(PayOutRepoContract::class)); // must no call notify second time
        $job->handle(app(PayOutRepoContract::class)); // must no call notify third time

        Queue::assertPushed(LogToChannel::class, 1);
    }
}
