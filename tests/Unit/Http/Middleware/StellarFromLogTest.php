<?php

namespace Tests\Unit\Http\Middleware;

use App\Events\Blockchain\StellarLogEvent;
use App\Models\Blockchain\StellarLog;
use App\Models\User\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class StellarFromLogTest extends TestCase
{
    protected $reqData = [
        'crypto_type'    => 'btc',
        'amount '        => 0.0001,
        'balance_amount' => 0.0001,
        'hash'           => 'bad-hash',
        'transaction_id' => 'tx456666',
    ];

    protected $route;

    /** @test */
    public function testWithoutMwEventNotFired()
    {
        Event::fake();

        $user = factory(User::class)->make();
        $this->actingAs($user);
        $count = StellarLog::count();

        // act on public route
        $response = $this->json('GET', route('market.buy'));

        // assert
        Event::assertNotDispatched(StellarLogEvent::class);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals($count, StellarLog::count());
    }

    /** @test */
    public function testMmEventCorrectData()
    {
        Event::fake();

        $user = factory(User::class)->create();

        $this->route = route('blockchain.transaction.deposit', ['user' => $user]);

        // act
        $response = $this->json('POST', $this->route, $this->reqData);

        Event::assertDispatched(StellarLogEvent::class, function (StellarLogEvent $event) use ($response) {
            return $event->type === 'from'
                && $event->method === 'POST'
                && strpos($this->route, $event->route)
                && $event->response_code === $response->getStatusCode()
                && $event->request === serialize($this->reqData)
                && $event->response === serialize($response->decodeResponseJson());
        });
    }
}
