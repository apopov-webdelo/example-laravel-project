<?php

namespace Tests\Unit\Http\Controllers;

use App\Makers\User\ReviewMaker;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use AdHelpers, DealHelpers;

    /** @test */
    public function testNoRate1Reviews()
    {
        $user1 = $this->registerUser();
        $user2 = $this->registerUser();

        $this->makeAd($user1);
        $ad = $this->lastAd($user1);
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);

        ReviewMaker::init([
            'deal_id' => $deal,
            'rate'    => 1,
            'trust'   => 'negative',
        ])->recipient($user1)
                   ->author($user2)
                   ->deal($deal)
                   ->create();

        // act
        $user3 = $this->registerUser();
        $this->actingAs($user3);
        $response = $this->json('GET', route('user.reviews', ['user' => $user1]));
        $result = $response->decodeResponseJson();

        // assert
        $this->assertCount(0, $result['data']);
    }
}
