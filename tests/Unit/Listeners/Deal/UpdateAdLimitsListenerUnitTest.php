<?php

namespace Tests\Unit\Listeners\Deal;

use App\Events\Balance\BalanceReplenished;
use App\Events\Balance\BalanceWrittenOff;
use App\Listeners\Balance\UpdateAdLimitsListener;
use App\Models\Directory\CryptoCurrencyConstants;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestCase;

/**
 * Class UpdateAdLimitsListenerUnitTest
 *
 * @covers  \App\Listeners\Balance\UpdateAdLimitsListener
 * @package Tests\Unit\Listeners\Deal
 */
class UpdateAdLimitsListenerUnitTest extends TestCase
{
    use AdHelpers, BalanceHelpers;

    /**
     * @test
     * @throws \Exception
     */
    public function testAdIsTurnedOffWhenBalanceZero()
    {
        $crypto = $this->getRandomCrypto();
        $userAd = $this->registerUser(['testerAd']); // 0 balance by default
        // $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 3000,
            'min'                => 1000,
            'max'                => 3000,
            'original_max'       => 3000,
            'liquidity_required' => false,
        ]);

        $ad = $this->lastAd($userAd);

        $this->assertTrue($ad->is_active);

        $balance = $userAd->getBalance($crypto);

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff($balance));

        $this->assertFalse($ad->fresh()->is_active);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdNotTurnedOffWhenBalanceNotZero()
    {
        $crypto = $this->getRandomCrypto();
        $userAd = $this->registerUser(['testerAd']); // 0 balance by default
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 3000,
            'min'                => 1000,
            'max'                => 3000,
            'original_max'       => 3000,
            'liquidity_required' => false,
            'commission_percent' => 100,
        ]);

        $ad = $this->lastAd($userAd);

        $this->assertTrue($ad->is_active);

        $this->subBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC / 2);

        $balance = $userAd->getBalance($crypto);

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff($balance));

        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertTrue($ad->max < $ad->original_max);
        $this->assertEquals(1485, $ad->max); // default commission is 1%
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testBuyAdNotTurnedOffOrModified()
    {
        $crypto = $this->getRandomCrypto();
        $userAd = $this->registerUser(['testerAd']); // 0 balance by default
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $this->makeAd($userAd, [
            'is_sale'            => false,
            'crypto_currency_id' => $crypto,
            'price'              => 3000,
            'min'                => 1000,
            'max'                => 3000,
            'original_max'       => 3000,
            'liquidity_required' => false,
            'commission_percent' => 1,
        ]);

        $ad = $this->lastAd($userAd);

        $this->assertTrue($ad->is_active);

        $this->subBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $balance = $userAd->getBalance($crypto);

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff($balance));

        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertTrue($ad->max === $ad->original_max);
        $this->assertEquals(3000, $ad->max);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdTurnedOffWhenBalanceZero()
    {
        $crypto = $this->getRandomCrypto();
        $userAd = $this->registerUser(['testerAd']); // 0 balance by default
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 3000,
            'min'                => 1000,
            'max'                => 3000,
            'original_max'       => 3000,
            'liquidity_required' => false,
            'commission_percent' => 1,
        ]);

        $ad = $this->lastAd($userAd);

        $this->assertTrue($ad->is_active);

        $this->subBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC);

        $balance = $userAd->getBalance($crypto);

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff($balance));

        $ad = $ad->fresh();
        $this->assertFalse($ad->is_active);
        $this->assertTrue($ad->max === $ad->min);
        $this->assertEquals(1000, $ad->max);
    }
}
