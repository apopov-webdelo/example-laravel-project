<?php

namespace Tests\Unit\Listeners\Deal;

use App\Events\Deal\DealPlaced;
use App\Listeners\Ad\SubstractAdLiquidityListener;
use App\Models\Directory\CryptoCurrencyConstants;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

/**
 * Class SubstractAdLiquidityListenerUnitTest
 *
 * @covers  \App\Listeners\Balance\UpdateAdLimitsListener
 * @package Tests\Unit\Listeners\Deal
 */
class SubstractAdLiquidityListenerUnitTest extends TestCase
{
    use DealHelpers, AdHelpers, BalanceHelpers;

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitIsLoweredAfterSellDealCreated()
    {
        // seller
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // buyer
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 5000]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertEquals(5000, $ad->max);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitIsLoweredToMinAndAdTurnedOffSell()
    {
        // seller
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // buyer
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 10000]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertFalse($ad->is_active);
        $this->assertEquals(0, $ad->max);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitIsLoweredAfterBuyDealCreated()
    {
        // buyer
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => false,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // seller
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 5000]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertEquals(5000, $ad->max);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitIsLoweredToMinAndAdTurnedOffBuy()
    {
        // buyer
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => false,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // seller
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 10000]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertFalse($ad->is_active);
        $this->assertEquals(0, $ad->max);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitNotAffectedWithoutLiquidity()
    {
        // seller
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => false,
        ]);

        $ad = $this->lastAd($userAd);

        // buyer
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 9800]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));


        // asserts
        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertEquals(10000, $ad->max);
    }
}
