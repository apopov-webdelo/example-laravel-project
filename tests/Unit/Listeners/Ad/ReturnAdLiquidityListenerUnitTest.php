<?php

namespace Tests\Unit\Listeners\Deal;

use App\Events\Deal\DealCanceled;
use App\Events\Deal\DealPlaced;
use App\Listeners\Ad\ReturnAdLiquidityListener;
use App\Listeners\Ad\SubstractAdLiquidityListener;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Services\Deal\CancelDealService;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

/**
 * Class ReturnAdLiquidityListenerUnitTest
 *
 * @covers  \App\Listeners\Balance\UpdateAdLimitsListener
 * @package Tests\Unit\Listeners\Deal
 */
class ReturnAdLiquidityListenerUnitTest extends TestCase
{
    use DealHelpers, AdHelpers, BalanceHelpers;

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitIsReturnedAndTurnedOnForSeller()
    {
        // seller
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);
        $this->addEscrow($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 100);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // buyer
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 9800]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        $this->actingAs($userDeal);

        /* @var CancelDealService $service */
        $deal->statuses()->attach(DealStatusConstants::CANCELLATION);
        $service = app(CancelDealService::class);
        $service->cancel($deal);
        $deal = $deal->fresh();

        /* @var ReturnAdLiquidityListener $listener */
        $listener = app(ReturnAdLiquidityListener::class);
        $listener->handle(new DealCanceled($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertEquals(10000, $ad->max);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testAdMaxLimitIsReturnedAndTurnedOnForBuyer()
    {
        // buyer
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => false,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // seller
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 9800]);
        $deal = $this->lastDeal($userDeal);
        $this->addEscrow($userDeal, $crypto, CryptoCurrencyConstants::ONE_BTC * 100);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        $this->actingAs($userDeal);

        /* @var CancelDealService $service */
        $deal->statuses()->attach(DealStatusConstants::CANCELLATION);
        $service = app(CancelDealService::class);
        $service->cancel($deal);
        $deal = $deal->fresh();

        /* @var ReturnAdLiquidityListener $listener */
        $listener = app(ReturnAdLiquidityListener::class);
        $listener->handle(new DealCanceled($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertEquals(10000, $ad->max);
    }
}
