<?php

namespace Tests\Unit\Listeners\DealCashback;

use App\Events\DealCashback\DealCashbackPendingState;
use App\Listeners\DealCashback\SendCashbackToStellar;
use App\Models\DealCashback\DealCashback;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Support\Facades\Queue;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class SendCashbackToStellarTest extends TestCase
{
    use DealHelpers, AdHelpers;

    /** @test */
    public function testItFires()
    {
        // setup
        Queue::fake();

        $cashback = factory(DealCashback::class)->create([
            'status' => DealCashback::STATUS_PENDING,
        ]);

        // act
        event(app(DealCashbackPendingState::class, ['cashback' => $cashback]));

        // assert
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == SendCashbackToStellar::class;
        });
    }
}
