<?php

namespace Tests\Unit\Listeners\DealCashback;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackDoneContract;
use App\Facades\CryptoExchange;
use App\Facades\Robot;
use App\Http\Requests\Blockchain\DealCashback\DoneRequest;
use App\Models\Balance\Transaction;
use App\Models\DealCashback\DealCashback;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\From\User\ProcessDealCashbackDone;
use App\Services\DealCashback\DealCashbackCommission;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class CashbackTransferTest extends TestCase
{
    use AdHelpers, DealHelpers, BalanceHelpers;

    public function setUp()
    {
        parent::setUp();
        $this->actingAs(Robot::user());

        CryptoExchange::shouldReceive('convert')->andReturn(999);

        $this->withoutExceptionHandling();
    }

    /** @test
     * @throws \Exception
     */
    public function testCashbackIsTransfered()
    {
        // setup
        /* @var DealCashbackCommission $commissionStorage */
        /* @var DealCashback $cashback */
        list($cashback, $commissionStorage) = $this->prepareData();

        $amount = $commissionStorage->amount();
        $user = $commissionStorage->user();
        $ad = $this->lastAd($user);

        $request = new DoneRequest([
            'transaction_id' => 9999,
            'timestamp'      => 12345,
            'hash'           => md5($cashback->id . '-' . 9999 . '-' . 12345 . '-' . config('app.blockchain.stellar.private_key')),
        ]);

        // act
        /* @var ProcessDealCashbackDone $handler */
        $handler = app(ProcessDealCashbackDoneContract::class, ['request' => $request]);
        $handler->done($cashback);

        // assert
        $this->assertEquals($amount, $user->getBalance($ad->cryptoCurrency)->amount);
    }

    /** @test
     * @throws \Exception
     */
    public function testTransactionHasDescription()
    {
        // setup
        /* @var DealCashbackCommission $commissionStorage */
        /* @var DealCashback $cashback */
        list($cashback, $commissionStorage) = $this->prepareData();

        $request = new DoneRequest([
            'transaction_id' => 9999,
            'timestamp'      => 12345,
            'hash'           => md5($cashback->id . '-' . 9999 . '-' . 12345 . '-' . config('app.blockchain.stellar.private_key')),
        ]);

        // act
        /* @var ProcessDealCashbackDone $handler */
        $handler = app(ProcessDealCashbackDoneContract::class, ['request' => $request]);
        $handler->done($cashback);

        $transaction = Transaction::query()->get()->last();

        // assert
        $this->assertStringContainsString($commissionStorage->entity()->entityId(), $transaction->description);
        $this->assertStringContainsString(
            $commissionStorage->deals()->pluck('id')->implode(', '),
            $transaction->description
        );
    }

    /**
     * @return array
     */
    protected function prepareData(): array
    {
        $dealCount = env('DEAL_CASHBACK_DEAL_QUANTITY');

        $user = factory(User::class)->create();

        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create([
            'status'  => DealCashback::STATUS_PENDING,
            'user_id' => $user,
        ]);

        $this->makeAd($user);
        $ad = $this->lastAd($user);
        $this->addBalance(Robot::user(), $ad->cryptoCurrency, CryptoCurrencyConstants::ONE_BTC * 100);
        $this->addBalance($user, $ad->cryptoCurrency, 0);

        $userDeal = factory(User::class)->create();
        $this->times($dealCount)->makeDeal($userDeal, $this->lastAd($user));

        $this->getDeals($userDeal)->each(function ($deal) use ($cashback) {
            $cashback->dealsRelation()->attach($deal);
        });

        /* @var DealCashbackCommissionStorageContract $commissionStorage */
        $commissionStorage = app(DealCashbackCommissionStorageContract::class, ['cashback' => $cashback]);

        return [$cashback, $commissionStorage];
    }
}
