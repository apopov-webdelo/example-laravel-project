<?php

namespace Tests\Unit\Listeners\User;

use App\Contracts\Utils\Session\SessionStorageContract;
use App\Events\User\SecurityLogEvent;
use App\Listeners\User\SecurityLogListener;
use App\Models\User\SecurityLog;
use Tests\TestCase;

class SecurityLogListenerUnitTest extends TestCase
{
    /** @test */
    public function testWritesDataCorrectly()
    {
        $user = $this->registerUser();

        /* @var SecurityLogListener $listener */
        $listener = app(SecurityLogListener::class);
        $record = $listener->handle(app(SecurityLogEvent::class, [
            'user'           => $user,
            'sessionStorage' => app(SessionStorageContract::class),
            'securityInfo'   => [
                'type'       => 'test-type',
                'status'     => true,
                'statusDesc' => 'test-statusDesc',
            ],
        ]));

        $this->assertInstanceOf(SecurityLog::class, $record);
        $this->assertEquals('test-type', $record->type);
        $this->assertEquals('test-statusDesc', $record->status_desc);
        $this->assertEquals(true, $record->status);
    }
}
