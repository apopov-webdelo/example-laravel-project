<?php

namespace Tests\Unit\Listeners\Blockchain;

use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Listeners\Blockchain\LogErrorStellarRequest;
use App\Models\Blockchain\StellarLog;
use App\Models\User\User;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class LogStellarRequestTest extends TestCase
{
    protected $reqData = [
        'crypto_type'    => 'btc',
        'amount'         => 0.0001,
        'balance_amount' => 0.0001,
        'hash'           => 'bad-hash',
        'transaction_id' => 'tx456666',
    ];

    protected $route;

    /** @test */
    public function testLogSavedOnFromRequest()
    {
        // mock alerter
        $this->app->bind(LogErrorStellarRequest::class, function () {
            return \Mockery::mock(LogErrorStellarRequest::class)->makePartial()->allows([
                'handle' => null,
            ]);
        });

        $user = factory(User::class)->create();
        $count = StellarLog::count();

        $this->route = route('blockchain.transaction.deposit', ['user' => $user]);

        // act
        $response = $this->json('POST', $this->route, $this->reqData);

        // assert
        $this->assertEquals(StellarLog::count(), $count + 1);

        $last = StellarLog::latest()->first();
        $this->assertEquals('from', $last->type);
        $this->assertEquals('POST', $last->method);
        $this->assertEquals('blockchain/user/' . $user->id . '/deposit', $last->route);
        $this->assertEquals($response->decodeResponseJson(), $last->response);
        $this->assertEquals($this->reqData, $last->request);
        $this->assertEquals($response->getStatusCode(), $last->response_code);
        $this->assertNotNull($last->created_at);
    }

    /** @test */
    public function testLogSavedOnToRequestBad()
    {
        // mock alerter
        $this->app->bind(LogErrorStellarRequest::class, function () {
            return \Mockery::mock(LogErrorStellarRequest::class)->makePartial()->allows([
                'handle' => null,
            ]);
        });

        $user = factory(User::class)->make();
        $count = StellarLog::count();

        /* @var WalletContract $service */
        $service = app(WalletContract::class);

        try {
            $service->getWallet($user, $this->getCryptoByCode('btc'));
        } catch (\Exception $e) {
        } finally {
            // assert
            $this->assertEquals(StellarLog::count(), $count + 1);

            $last = StellarLog::latest()->first();
            $this->assertEquals('to', $last->type);
            $this->assertEquals('POST', $last->method);
            $this->assertContains('user/wallet', $last->route);
            $this->assertContains($e->getMessage(), $last->response);
            $this->assertArrayHasKey('hash', $last->request);
            $this->assertArrayHasKey('crypto_type', $last->request);
            $this->assertArrayHasKey('user_id', $last->request);
            $this->assertArrayHasKey('timestamp', $last->request);
            $this->assertEquals($e->getCode(), $last->response_code);
            $this->assertNotNull($last->created_at);
        }
    }

    /**
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function testLogSavedOnToRequestGood()
    {
        $user = factory(User::class)->make();
        $count = StellarLog::count();

        // mock guzzle
        $this->app->bind(ClientInterface::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode(['wallet_id' => '123'])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /* @var WalletContract $service */
        $service = app(WalletContract::class);

        $service->getWallet($user, $this->getCryptoByCode('btc'));

        // assert
        $this->assertEquals(StellarLog::count(), $count + 1);

        $last = StellarLog::latest()->first();
        $this->assertEquals('to', $last->type);
        $this->assertEquals('POST', $last->method);
        $this->assertContains('user/wallet', $last->route);
        $this->assertEquals(['wallet_id' => '123'], $last->response);
        $this->assertArrayHasKey('hash', $last->request);
        $this->assertArrayHasKey('crypto_type', $last->request);
        $this->assertArrayHasKey('user_id', $last->request);
        $this->assertArrayHasKey('timestamp', $last->request);
        $this->assertEquals(200, $last->response_code);
        $this->assertNotNull($last->created_at);
    }
}
