<?php

namespace Tests\Unit\Listeners\Blockchain;

use App\Contracts\Services\Blockchain\From\Transaction\DepositContract;
use App\Contracts\Services\Blockchain\To\Wallet\WalletContract;
use App\Facades\EmergencyAlert;
use App\Models\User\User;
use App\Services\Blockchain\Stellar\From\Transaction\DepositHandler;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class LogErrorStellarRequestTest extends TestCase
{
    protected $reqData = [
        'crypto_type'    => 'btc',
        'amount'         => 0.0001,
        'balance_amount' => 0.0001,
        'hash'           => 'bad-hash',
        'transaction_id' => 'tx456666',
    ];

    protected $route;

    /** @test */
    public function testBadFromRequestHasAlerts()
    {
        // pre assert
        EmergencyAlert::shouldReceive('channel->send')->twice();

        $user = factory(User::class)->create();
        $this->route = route('blockchain.transaction.deposit', ['user' => $user]);

        // act
        $this->json('POST', $this->route, $this->reqData);
    }

    /** @test */
    public function testGoodFromRequestNoAlerts()
    {
        $this->app->bind(DepositContract::class, function () {
            return \Mockery::mock(DepositHandler::class)
                            ->makePartial()
                            ->allows(['deposit' => true]);
        });

        // setup
        $user = factory(User::class)->create();
        $this->route = route('blockchain.transaction.deposit', ['user' => $user]);
        $correctRq = [
            'crypto_type'    => 'btc',
            'amount'         => '0.001',
            'balance_amount' => '0.001',
            'transaction_id' => '9999',
            'hash'           => md5("9999-{$user->id}-0.001-btc-0.001-" . config('app.blockchain.stellar.private_key')),
        ];

        // act
        $response = $this->json('POST', $this->route, $correctRq);

        // assert | bad answer will try to send telegram and fail
        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK);
    }

    /** @test */
    public function testBadToRequestHasAlerts()
    {
        // pre assert
        EmergencyAlert::shouldReceive('channel->send')->twice();

        $user = factory(User::class)->make();

        /* @var WalletContract $service */
        $service = app(WalletContract::class);

        try {
            $service->getWallet($user, $this->getCryptoByCode('btc'));
        } catch (\Exception $e) {
        }
    }

    /**
     * @throws \App\Exceptions\Blockchain\Stellar\StellarException
     */
    public function testGoodToRequestNoAlerts()
    {
        $user = factory(User::class)->make();

        // mock guzzle
        $this->app->bind(ClientInterface::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode(['wallet_id' => '123'])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /* @var WalletContract $service */
        $service = app(WalletContract::class);

        $wallet_id = $service->getWallet($user, $this->getCryptoByCode('btc'));

        // assert | bad answer will try to send telegram and fail
        $this->assertEquals('123', $wallet_id);
    }
}
