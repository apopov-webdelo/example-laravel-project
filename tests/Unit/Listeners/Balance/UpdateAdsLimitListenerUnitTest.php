<?php

namespace Tests\Unit\Listeners\Deal;

use App\Events\Balance\BalanceReplenished;
use App\Events\Balance\BalanceWrittenOff;
use App\Events\Deal\DealPlaced;
use App\Listeners\Ad\SubstractAdLiquidityListener;
use App\Listeners\Balance\UpdateAdLimitsListener;
use App\Models\Directory\CryptoCurrencyConstants;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class UpdateAdsLimitListenerUnitTest extends TestCase
{
    use DealHelpers, AdHelpers, BalanceHelpers;

    protected function setUp()
    {
        parent::setUp();
        Event::fake();
    }

    /** @test */
    public function testMaxAmountDecreased()
    {
        // seller
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 5);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 1000,
            'min'                => 1000,
            'max'                => 10000,
            'original_max'       => 10000,
            'liquidity_required' => false,
            'commission_percent' => 100,
        ]);

        $ad = $this->lastAd($userAd);

        // buyer
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 5000]);
        $this->lastDeal($userDeal);

        $this->actingAs($userAd, 'api');

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff(
            $userAd->getBalance($ad->cryptoCurrency)
        ));

        // assert
        $ad = $ad->fresh();
        $this->assertTrue($ad->is_active);
        $this->assertEquals(4950, $ad->max); // 5000 - 1% commission
    }

    /** @test */
    public function testNotDeactivateAdOnLimitHigherThanMinimum()
    {
        $user = $this->registerUser();
        $this->makeAd($user, [
            'author_id' => $user,
            'is_sale'   => true,
        ]);
        $ad = $this->lastAd($user);

        $user->getBalance($ad->cryptoCurrency)->update(['amount' => CryptoCurrencyConstants::ONE_BTC]);

        $dealuser = $this->registerUser();
        $this->makeDeal($dealuser, $ad, [
            'ad_id'       => $ad,
            'price'       => $ad->price,
            'fiat_amount' => $ad->min,
        ]);

        $this->actingAs($user, 'api');

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff(
            $user->getBalance($ad->cryptoCurrency)
        ));

        $this->assertTrue($ad->is_active);
    }

    /** @test */
    public function testDeactivateAdOnLimitLowerThanMinimum()
    {
        $user = $this->registerUser();
        $this->makeAd($user, [
            'author_id' => $user,
            'is_sale'   => true,
        ]);
        $ad = $this->lastAd($user);

        $user->getBalance($ad->cryptoCurrency)->update(['amount' => 0]);

        $dealuser = $this->registerUser();
        $this->makeDeal($dealuser, $ad, [
            'ad_id'       => $ad,
            'price'       => $ad->price,
            'fiat_amount' => $ad->min,
        ]);

        $this->actingAs($user, 'api');

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceWrittenOff(
            $user->getBalance($ad->cryptoCurrency)
        ));

        $ad = $ad->fresh();
        $this->assertFalse($ad->is_active);
        $this->assertEquals($ad->min, $ad->max);
    }

    /** @test
     * @throws \Exception
     */
    public function testNotActivateAdWithLiquidityGoneOnBalanceReplenished()
    {
        // seller
        $crypto = $this->getCryptoByCode(CryptoCurrencyConstants::BTC_CODE);
        $userAd = $this->registerUser(['testerAd']);
        $this->addBalance($userAd, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);

        $this->makeAd($userAd, [
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'price'              => 3000,
            'min'                => 30,
            'max'                => 90,
            'original_max'       => 90,
            'liquidity_required' => true,
        ]);

        $ad = $this->lastAd($userAd);

        // buyer
        $userDeal = $this->registerUser(['testerDeal']);
        $this->makeDeal($userDeal, $ad, ['fiat_amount' => 90]);
        $deal = $this->lastDeal($userDeal);

        /* @var SubstractAdLiquidityListener $listener */
        $listener = app(SubstractAdLiquidityListener::class);
        $listener->handle(new DealPlaced($deal));

        // asserts
        $ad = $ad->fresh();
        $this->assertEquals(0, $ad->max);
        $this->assertEquals(false, $ad->is_active);

        /* @var UpdateAdLimitsListener $listener */
        $listener = app(UpdateAdLimitsListener::class);
        $listener->handle(new BalanceReplenished(
            $userAd->getBalance($crypto)
        ));

        // asserts
        $ad = $ad->fresh();
        $this->assertEquals(0, $ad->max);
        $this->assertEquals(false, $ad->is_active);
    }
}
