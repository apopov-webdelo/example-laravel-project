<?php

namespace Tests\Unit\Facades;

use App\Facades\EmergencyAlert;
use App\Factories\EmergencyAlertFactory;
use App\Jobs\Utils\LogToChannel;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class EmergencyAlertTest extends TestCase
{
    /** @test */
    public function testCanGetFactory()
    {
        $facade = EmergencyAlert::channel('slack');

        $this->assertInstanceOf(EmergencyAlertFactory::class, $facade);
    }

    /** @test */
    public function testCanGetFactoryWithArray()
    {
        $facade = EmergencyAlert::channel(['slack', 'jira', 'telegram_emergency']);

        $this->assertInstanceOf(EmergencyAlertFactory::class, $facade);
    }

    /** @test */
    public function testCanSendMessage()
    {
        Queue::fake();

        EmergencyAlert::send('hello world');

        Queue::assertPushed(LogToChannel::class);
    }

    /** @test */
    public function testCanSendMultiMessage()
    {
        Queue::fake();

        EmergencyAlert::channel(['slack', 'jira', 'telegram_emergency'])->send('hello world');

        Queue::assertPushed(LogToChannel::class, 3);
    }

    /** @test */
    public function testInvalidChannelSkippedAndLogged()
    {
        Queue::fake();

        EmergencyAlert::channel('invalid')->send('hello world');

        Queue::assertNotPushed(LogToChannel::class);
    }
}
