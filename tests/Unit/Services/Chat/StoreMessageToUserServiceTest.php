<?php

namespace Tests\Unit\Services\Chat;

use App\Models\User\User;
use App\Services\Chat\Message\StoreMessageToDealService;
use App\Services\Chat\Message\StoreMessageToUserService;
use Illuminate\Support\Collection;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class StoreMessageToUserServiceTest extends TestCase
{
    use AdHelpers, DealHelpers;

    /**
     * @var StoreMessageToUserService
     */
    private $service;

    /**
     * @var User
     */
    private $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->registerUser();
        $this->actingAs($this->user);
        $this->service = app(StoreMessageToUserService::class);
    }

    /** @test */
    public function testInstantiate()
    {
        $this->assertInstanceOf(StoreMessageToUserService::class, $this->service);
    }

    /**
     * @test
     * @throws \App\Exceptions\Chat\ChatException
     */
    public function testCreatesPrivateChat()
    {
        $userTo = $this->registerUser();

        // act
        $this->service->store($userTo, 'Hello');

        // assert
        $chat = $userTo->chats->last();
        /* @var Collection $recipients */
        $recipients = $chat->recipients;
        $this->assertEquals($this->user->id, $chat->author_id);
        $this->assertCount(2, $recipients);
        $this->assertTrue($recipients->contains($this->user));
        $this->assertTrue($recipients->contains($userTo));
    }

    /**
     * @test
     * @throws \App\Exceptions\Chat\ChatException
     */
    public function testAfterDealChatCreatePrivateChat()
    {
        $userTo = $this->registerUser();

        $this->makeAd($this->user);
        $ad = $this->lastAd($this->user);
        $this->makeDeal($userTo, $ad);

        // make deal chat
        /* @var StoreMessageToDealService $dealChatService */
        $dealChatService = app(StoreMessageToDealService::class);
        $dealChatService->store($this->lastDeal($userTo), 'Deal message test');

        // make user chat
        $this->service->store($userTo, 'Hello $userTo');

        // assert
        $chat = $userTo->chats->last();
        /* @var Collection $recipients */
        $recipients = $chat->recipients;
        $this->assertEquals($this->user->id, $chat->author_id);
        $this->assertCount(2, $recipients);
        $this->assertTrue($recipients->contains($this->user));
        $this->assertTrue($recipients->contains($userTo));
    }

    /**
     * @test
     * @throws \App\Exceptions\Chat\ChatException
     */
    public function testAfterDealChatFromUser2()
    {
        $user2 = $this->registerUser();
        $this->makeAd($this->user);
        $ad = $this->lastAd($this->user);
        $this->makeDeal($user2, $ad);

        // make deal chat from user2 to test user
        $this->actingAs($user2);
        /* @var StoreMessageToDealService $dealChatService */
        $dealChatService = app(StoreMessageToDealService::class);
        $dealChatService->store($this->lastDeal($user2), 'Deal message test');

        // make private hat from test user to $user2
        $this->actingAs($this->user);
        /* @var StoreMessageToUserService $privateChatService */
        $privateChatService = app(StoreMessageToUserService::class);
        $privateChatService->store($user2, 'Hello $user2');

        // assert
        $chat = $user2->chats->last();
        /* @var Collection $recipients */
        $recipients = $chat->recipients;
        $this->assertEquals($this->user->id, $chat->author_id);
        $this->assertCount(2, $recipients);
        $this->assertTrue($recipients->contains($this->user));
        $this->assertTrue($recipients->contains($user2));
    }
}
