<?php

namespace Tests\Unit\Services\Chat;

use App\Exceptions\Chat\ChatException;
use App\Models\Deal\DealStatusConstants;
use App\Services\Chat\Message\StoreMessageToDealService;
use App\Services\Review\ReviewService;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class StoreMessageToDealServiceTest extends TestCase
{
    use AdHelpers, DealHelpers;

    /**
     * @test
     * @throws \App\Exceptions\Chat\ChatException
     * @throws \App\Exceptions\Review\ReviewException
     * @throws \ReflectionException
     */
    public function testCantSendMessageWhenBlacklisted()
    {
        $user1 = $this->registerUser();
        $user2 = $this->registerUser();
        $this->makeAd($user1);
        $ad = $this->lastAd($user1);
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::FINISHED);

        // blacklist from user2 to user1
        $this->actingAs($user2);
        /* @var ReviewService $reviewService */
        $reviewService = app(ReviewService::class);
        $reviewService->store($deal, ['trust' => 'negative', 'rate' => 1]);

        $this->expectException(ChatException::class);

        // make deal chat from user2 to test user1
        $this->actingAs($user2);
        /* @var StoreMessageToDealService $dealChatService */
        $dealChatService = app(StoreMessageToDealService::class);
        $dealChatService->store($deal, 'Deal message test');
    }
}
