<?php

namespace Tests\Unit\Services\Partnership;

use App\Exceptions\Partnership\InvalidProfitAmountPartnershipException;
use App\Models\Directory\CryptoCurrencyConstants as Crypto;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Services\Partnership\PartnershipProfitStorage;
use Tests\TestCase;

class PartnershipProfitStorageUnitTest extends TestCase
{
    /**
     * @var CryptoCurrencyRepo
     */
    protected $cryptoCurrencyRepo;

    /**
     * Set repo
     */
    protected function setUp()
    {
        parent::setUp();
        /** @var CryptoCurrencyRepo $repo */
        $this->cryptoCurrencyRepo = app(CryptoCurrencyRepo::class);
    }

    /**
     * Valid data provider
     *
     * @return array
     */
    public function validProfitProvider()
    {
        return [
            [Crypto::ONE_BTC, Crypto::BTC_CODE],
            [Crypto::ONE_ETH*2, Crypto::ETH_CODE],
        ];
    }

    /**
     * Test that transmitted amount and crypto type set to storage correctly
     *
     * @param $amount
     * @param $cryptoCode
     *
     * @return $this
     *
     * @dataProvider validProfitProvider
     */
    public function testValidData($amount, $cryptoCode)
    {
        $profitStorage = $this->getStorage($amount, $cryptoCode);
        $this->assertEquals(
            $amount,
            $profitStorage->amount()
        );
        $this->assertEquals(
            $cryptoCode,
            $profitStorage->cryptoCurrency()->getCode()
        );
        return $this;
    }

    /**
     * @param $amount
     * @param $cryptoCode
     *
     * @return PartnershipProfitStorage
     */
    protected function getStorage($amount, $cryptoCode): PartnershipProfitStorage
    {

        return app(
            PartnershipProfitStorage::class,
            [
                'amount' => $amount,
                'cryptoCurrency' => $this->cryptoCurrencyRepo->getByCode($cryptoCode)
            ]
        );
    }

    /**
     * @return void
     */
    public function testInvalidAmountProfit()
    {
        $this->expectException(InvalidProfitAmountPartnershipException::class);
        $this->getStorage(Crypto::ONE_ETH*2*(-1), Crypto::ETH_CODE);
    }
}
