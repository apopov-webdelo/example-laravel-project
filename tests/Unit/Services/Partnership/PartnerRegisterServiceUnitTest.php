<?php

namespace Tests\Unit\Services\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Events\Partnership\PartnerRegistered;
use App\Exceptions\Partnership\PartnerAlreadyRegisteredException;
use App\Models\User\User;
use App\Services\Partnership\PartnerRegisterService;
use Tests\TestCase;

/**
 * Class PartnerRegisterServiceUnitTest
 *
 * @package Tests\Unit\Services\Partnership
 */
class PartnerRegisterServiceUnitTest extends TestCase
{
    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->registerUser(['login' => 'partner']);
    }

    /**
     * Test valid partner registration
     *
     * @throws PartnerAlreadyRegisteredException
     * @throws \Exception
     */
    public function testValidRegistration()
    {
        /** @var PartnerRegisterService $service */
        $service = app(PartnerRegisterService::class);

        $this->expectsEvents(PartnerRegistered::class);
        $this->assertInstanceOf(PartnerContract::class, $service->register($this->user));
        $this->assertTrue(is_user_partner($this->user));
    }

    /**
     * Test duplicate partner registration
     *
     * @throws \App\Exceptions\Partnership\PartnerAlreadyRegisteredException
     */
    public function testInvalidRegistration()
    {
        /** @var PartnerRegisterService $service */
        $service = app(PartnerRegisterService::class);

        $service->register($this->user);
        $this->expectException(PartnerAlreadyRegisteredException::class);
        $this->doesntExpectEvents(PartnerRegistered::class);
        $service->register($this->user);
    }
}
