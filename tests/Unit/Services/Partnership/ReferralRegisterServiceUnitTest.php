<?php

namespace Tests\Unit\Services\Partnership;

use App\Contracts\Partnership\ReferralContract;
use App\Events\Partnership\ReferralRegistered;
use App\Exceptions\Partnership\ReferralAlreadyRegisteredException;
use App\Models\Partnership\Partner;
use App\Models\User\User;
use App\Services\Partnership\PartnerRegisterService;
use App\Services\Partnership\ReferralRegisterService;
use Tests\TestCase;

/**
 * Class PartnerRegisterServiceUnitTest
 *
 * @package Tests\Unit\Services\Partnership
 */
class ReferralRegisterServiceUnitTest extends TestCase
{
    /**
     * User
     *
     * @var Partner $user
     */
    protected $partner;
    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * @throws \App\Exceptions\Partnership\NotFoundPartnerPartnershipException
     */
    protected function setUp()
    {
        parent::setUp();
        $this->user    = $this->registerUser(['login' => 'user']);
        $user          = $this->registerUser(['login' => 'partner']);
        $this->partner = app(PartnerRegisterService::class)->register($user) ? get_partner($user) : null;
    }

    /**
     * Test valid referral registration
     *
     * @throws \App\Exceptions\Partnership\ReferralAlreadyRegisteredException
     * @throws \Exception
     */
    public function testValidRegistration()
    {
        /** @var ReferralRegisterService $service */
        $service = app(ReferralRegisterService::class);

        $this->expectsEvents(ReferralRegistered::class);
        $this->assertInstanceOf(ReferralContract::class, $service->register($this->user, $this->partner));
        $this->assertTrue(is_user_referral($this->user));
    }

    /**
     * Test duplicate referral registration
     *
     * @throws \App\Exceptions\Partnership\ReferralAlreadyRegisteredException
     */
    public function testInvalidRegistration()
    {
        /** @var ReferralRegisterService $service */
        $service = app(ReferralRegisterService::class);

        $service->register($this->user, $this->partner);
        $this->expectException(ReferralAlreadyRegisteredException::class);
        $this->doesntExpectEvents(ReferralRegistered::class);
        $service->register($this->user, $this->partner);
    }
}
