<?php

namespace Tests\Unit\Services\Partnership;

use App\Contracts\Partnership\PartnerContract;
use App\Contracts\Partnership\PartnerRegisterContract;
use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Contracts\Partnership\PartnershipCommissionStrategyContract;
use App\Contracts\Partnership\PartnershipPayoutStrategyContract;
use App\Contracts\Partnership\PartnershipProcessingContract;
use App\Contracts\Partnership\PartnershipProgramContract;
use App\Contracts\Partnership\ReferralContract;
use App\Contracts\Partnership\ReferralRegisterContract;
use App\Events\Partnership\EntityProcessed;
use App\Facades\Robot;
use App\Makers\User\UserMaker;
use App\Models\Balance\Transaction;
use App\Models\Directory\CryptoCurrencyConstants as Crypto;
use App\Services\Partnership\Entities\DealEntity;
use App\Services\Partnership\PartnerRegisterService;
use App\Services\Partnership\PartnershipProcessingService;
use App\Services\Partnership\ReferralRegisterService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;
use Tests\Unit\Services\Partnership\Traits\MockedEntity;
use Tests\Unit\Services\Partnership\Traits\MockedReferral;

/**
 * Class PartnershipProcessingServiceUnitTest
 *
 * @package Tests\Unit\Services\Partnership
 */
class PartnershipProcessingServiceUnitTest extends TestCase
{
    use MockedEntity, MockedReferral, AdHelpers, DealHelpers;

    /**
     * @var PartnershipProcessingService|\Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface
     */
    private $processingService;

    /**
     * @var PartnershipPayoutStrategyContract|\Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface
     */
    private $mockedPayStrategy;

    protected function setUp()
    {
        parent::setUp();

        $this->mockedPayStrategy = \Mockery::mock(PartnershipPayoutStrategyContract::class);
        $this->processingService = app(PartnershipProcessingService::class, [
            'payoutService' => $this->mockedPayStrategy,
        ]);
    }

    /**
     * Testing data provider for processing test
     *
     * @return array
     */
    public function referralsNumbersProvider()
    {
        return [
            'one_Referral'    => [1],
            'two_referrals'   => [2],
            'three_referrals' => [3],
        ];
    }

    /**
     * Test that commission will pay for one referral
     *
     * @param int $referralNumbers
     *
     * @throws \Exception
     *
     * @dataProvider referralsNumbersProvider
     */
    public function testValidEntityProcessingForOneReferral(int $referralNumbers)
    {
        Event::fake();
        $this->expectsEvents(EntityProcessed::class);

        $this->mockedPayStrategy->shouldReceive('pay')->withAnyArgs()->times($referralNumbers);

        $entity = $this->getEntity($referralNumbers);
        $this->assertTrue($this->processingService->process($entity));
    }

    /**
     * Return mocked PartnershipEntityContract with needed referral numbers
     *
     * @param int $referralNumbers
     *
     * @return \App\Contracts\Partnership\PartnershipEntityContract|\Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface
     * @throws \Exception
     */
    protected function getEntity(int $referralNumbers = 1)
    {
        $referrals = [];

        for ($i = 1; $i <= $referralNumbers; $i++) {
            $mockedCommissionStorage = \Mockery::mock(PartnershipCommissionStorageContract::class);

            $mockedCommissionDriver = \Mockery::mock(PartnershipCommissionStrategyContract::class);
            $mockedCommissionDriver->shouldReceive('getCommission')
                                   ->withAnyArgs()
                                   ->andReturn($mockedCommissionStorage);

            $mockedPartnershipProgram = \Mockery::mock(PartnershipProgramContract::class);
            $mockedPartnershipProgram->allows(['getCommissionDriver' => $mockedCommissionDriver]);

            $mockedPartner = \Mockery::mock(PartnerContract::class);
            $mockedPartner->allows([
                'getPartnershipProgram' => $mockedPartnershipProgram,
                'getUser'               => UserMaker::init()->create(),
            ]);

            $mockedReferral = \Mockery::mock(ReferralContract::class);
            $mockedReferral->allows(['getPartner' => $mockedPartner]);

            $referrals[] = $mockedReferral;
        }

        $entity = $this->getMockedEntity(
            1000,
            Crypto::BTC_CODE,
            'MockedEntity',
            '1',
            '',
            '',
            $referrals
        );

        return $entity;
    }

    /**
     * @test
     * @throws \App\Exceptions\Partnership\PartnerAlreadyRegisteredException
     */
    public function testCorrectTransactionsOneReferalOnSale()
    {
        Event::fake();
        $user1 = $this->registerUser();
        $user2 = $this->registerUser();
        $user3 = $this->registerUser();
        $count = Transaction::count();

        // user2 is referal of user1
        /** @var PartnerRegisterContract $partnerReg */
        $partnerReg = app(PartnerRegisterService::class);
        $partner = $partnerReg->register($user1);

        /** @var ReferralRegisterContract $referalReg */
        $referalReg = app(ReferralRegisterService::class);
        $referalReg->register($user2, $partner);

        // user2 sells to user3
        $this->makeAd($user2, ['is_sale' => true]);
        $ad = $this->lastAd($user2);
        $this->makeDeal($user3, $ad);
        $deal = $this->lastDeal($user3);

        // act
        /** @var PartnershipProcessingContract $service */
        $service = app(PartnershipProcessingContract::class);
        $service->process(app(DealEntity::class, ['deal' => $deal]));

        // user1 must receive correct transaction deal amount * reward % (1)
        $line1reward = (1 / 100 * config('app.partnership.first_line.percentage_stage_1'));
        $tx = Transaction::query()->latest()->first();

        // assert
        $this->assertEquals(Transaction::count(), $count + 1);
        $this->assertEquals($tx->amount, $deal->commission * $line1reward);
        $this->assertEquals($tx->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($tx->receiver_balance_id, $user1->getBalance($deal->cryptoCurrency)->id);
    }

    /**
     * @test
     * @throws \App\Exceptions\Partnership\PartnerAlreadyRegisteredException
     */
    public function testCorrectTransactionsOneReferalOnBuy()
    {
        Event::fake();
        $user1 = $this->registerUser();
        $user2 = $this->registerUser();
        $user3 = $this->registerUser();
        $count = Transaction::count();

        // user2 is referal of user1
        /** @var PartnerRegisterContract $partnerReg */
        $partnerReg = app(PartnerRegisterService::class);
        $partner = $partnerReg->register($user1);

        /** @var ReferralRegisterContract $referalReg */
        $referalReg = app(ReferralRegisterService::class);
        $referalReg->register($user2, $partner);

        // user2 sells to user3
        $this->makeAd($user2, ['is_sale' => false]);
        $ad = $this->lastAd($user2);
        $this->makeDeal($user3, $ad);
        $deal = $this->lastDeal($user3);

        // act
        /** @var PartnershipProcessingContract $service */
        $service = app(PartnershipProcessingContract::class);
        $service->process(app(DealEntity::class, ['deal' => $deal]));

        // user1 must receive correct transaction deal amount * reward % (1)
        $line1reward = (1 / 100 * config('app.partnership.first_line.percentage_stage_1'));
        $tx = Transaction::query()->latest()->first();

        // assert
        $this->assertEquals(Transaction::count(), $count + 1);
        $this->assertEquals($tx->amount, $deal->commission * $line1reward);
        $this->assertEquals($tx->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($tx->receiver_balance_id, $user1->getBalance($deal->cryptoCurrency)->id);
    }

    /** @test
     * @throws \App\Exceptions\Partnership\PartnerAlreadyRegisteredException
     */
    public function testCorrectTransactionsTwoReferralsOnSale()
    {
        Event::fake();
        $user1 = $this->registerUser(['login' => 'partner1']);
        $user2 = $this->registerUser(['login' => 'partner2']);
        $user3 = $this->registerUser(['login' => 'referal3']);
        $user4 = $this->registerUser();
        $count = Transaction::count();

        // user3 => $user2 => $user1
        /** @var PartnerRegisterContract $partnerReg */
        $partnerReg = app(PartnerRegisterService::class);
        $partner1 = $partnerReg->register($user1);
        $partner2 = $partnerReg->register($user2);

        /** @var ReferralRegisterContract $referalReg */
        $referalReg = app(ReferralRegisterService::class);
        $referalReg->register($user2, $partner1);
        $referalReg->register($user3, $partner2);

        // user3 sells to user4
        $this->makeAd($user3, ['is_sale' => true]);
        $ad = $this->lastAd($user3);
        $this->makeDeal($user4, $ad);
        $deal = $this->lastDeal($user4);

        // act
        /** @var PartnershipProcessingContract $service */
        $service = app(PartnershipProcessingContract::class);
        $service->process(app(DealEntity::class, ['deal' => $deal]));

        // expects reward for user2 (first line), for user1 (second line)
        $line1reward = (1 / 100 * config('app.partnership.first_line.percentage_stage_1'));
        $line2reward = (1 / 100 * config('app.partnership.second_line.percentage_stage_1'));
        /** @var Transaction|Collection $txs */
        $txs = Transaction::latest()
                          ->take(2)
                          ->get(['id', 'amount', 'receiver_balance_id', 'description', 'remitter_balance_id']);

        // asserts
        $this->assertEquals(Transaction::count(), $count + 2);
        // assert for line 1
        $this->assertEquals($txs->first()->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->first()->receiver_balance_id, $user2->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->first()->amount, $deal->commission * $line1reward);
        // assert for line 2
        $this->assertEquals($txs->last()->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->last()->receiver_balance_id, $user1->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->last()->amount, $deal->commission * $line2reward);
    }

    /** @test
     * @throws \App\Exceptions\Partnership\PartnerAlreadyRegisteredException
     */
    public function testCorrectTransactionsTwoReferralsOnBuy()
    {
        Event::fake();
        $user1 = $this->registerUser(['login' => 'partner1']);
        $user2 = $this->registerUser(['login' => 'partner2']);
        $user3 = $this->registerUser(['login' => 'referal3']);
        $user4 = $this->registerUser();
        $count = Transaction::count();

        // user3 => $user2 => $user1
        /** @var PartnerRegisterContract $partnerReg */
        $partnerReg = app(PartnerRegisterService::class);
        $partner1 = $partnerReg->register($user1);
        $partner2 = $partnerReg->register($user2);

        /** @var ReferralRegisterContract $referalReg */
        $referalReg = app(ReferralRegisterService::class);
        $referalReg->register($user2, $partner1);
        $referalReg->register($user3, $partner2);

        // user3 sells to user4
        $this->makeAd($user3, ['is_sale' => false]);
        $ad = $this->lastAd($user3);
        $this->makeDeal($user4, $ad);
        $deal = $this->lastDeal($user4);

        // act
        /** @var PartnershipProcessingContract $service */
        $service = app(PartnershipProcessingContract::class);
        $service->process(app(DealEntity::class, ['deal' => $deal]));

        // expects reward for user2 (first line), for user1 (second line)
        $line1reward = (1 / 100 * config('app.partnership.first_line.percentage_stage_1'));
        $line2reward = (1 / 100 * config('app.partnership.second_line.percentage_stage_1'));
        /** @var Transaction|Collection $txs */
        $txs = Transaction::latest()
                          ->take(2)
                          ->get(['id', 'amount', 'receiver_balance_id', 'description', 'remitter_balance_id']);

        // asserts
        $this->assertEquals(Transaction::count(), $count + 2);
        // assert for line 1
        $this->assertEquals($txs->first()->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->first()->receiver_balance_id, $user2->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->first()->amount, $deal->commission * $line1reward);
        // assert for line 2
        $this->assertEquals($txs->last()->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->last()->receiver_balance_id, $user1->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->last()->amount, $deal->commission * $line2reward);
    }

    /**
     * @test
     * изначально найденный баг в партнерке
     * юзер 2 реферал юзера 1, а юзер Х реферал 2-го
     * 1 купил у юзера Х
     * я так понимаю 2-ой должен был получить 6666
     * а 16-ый должен был получить за второй уровень партнерки, а не такую же сумму
     * @throws \App\Exceptions\Partnership\PartnerAlreadyRegisteredException
     */
    public function testClosePartner2Lines()
    {
        Event::fake();
        $user1 = $this->registerUser(['login' => 'partner1']);
        $user2 = $this->registerUser(['login' => 'partner2']);
        $user3 = $this->registerUser(['login' => 'referal3']);
        $count = Transaction::count();

        // user3 => $user2 => $user1
        /** @var PartnerRegisterContract $partnerReg */
        $partnerReg = app(PartnerRegisterService::class);
        $partner1 = $partnerReg->register($user1);
        $partner2 = $partnerReg->register($user2);

        /** @var ReferralRegisterContract $referalReg */
        $referalReg = app(ReferralRegisterService::class);
        $referalReg->register($user2, $partner1);
        $referalReg->register($user3, $partner2);

        // user3 sells to user1
        $this->makeAd($user3, ['is_sale' => true]);
        $ad = $this->lastAd($user3);
        $this->makeDeal($user1, $ad);
        $deal = $this->lastDeal($user1);

        // act
        /** @var PartnershipProcessingContract $service */
        $service = app(PartnershipProcessingContract::class);
        $service->process(app(DealEntity::class, ['deal' => $deal]));

        // expects reward for user2 (first line), for user1 (second line)
        $line1reward = (1 / 100 * config('app.partnership.first_line.percentage_stage_1'));
        $line2reward = (1 / 100 * config('app.partnership.second_line.percentage_stage_1'));
        /** @var Transaction|Collection $txs */
        $txs = Transaction::latest()
                          ->take(2)
                          ->get(['id', 'amount', 'receiver_balance_id', 'description', 'remitter_balance_id']);

        // asserts
        $this->assertEquals(Transaction::count(), $count + 2);
        // assert for line 1
        $this->assertEquals($txs->first()->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->first()->receiver_balance_id, $user2->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->first()->amount, $deal->commission * $line1reward);
        // assert for line 2
        $this->assertEquals($txs->last()->remitter_balance_id, Robot::user()->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->last()->receiver_balance_id, $user1->getBalance($deal->cryptoCurrency)->id);
        $this->assertEquals($txs->last()->amount, $deal->commission * $line2reward);
    }
}
