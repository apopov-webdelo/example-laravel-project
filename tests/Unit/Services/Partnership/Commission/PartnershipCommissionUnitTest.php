<?php

namespace Tests\Unit\Services\Partnership\Commission;

use App\Exceptions\Partnership\InvalidCommissionAmountPartnershipException;
use App\Models\Directory\CryptoCurrencyConstants as Crypto;
use App\Repositories\Directory\CryptoCurrencyRepo;
use Tests\TestCase;
use Tests\Unit\Services\Partnership\Traits\CommissionStorage;
use Tests\Unit\Services\Partnership\Traits\MockedEntity;
use Tests\Unit\Services\Partnership\Traits\MockedReferral;

class PartnershipCommissionUnitTest extends TestCase
{
    use MockedReferral, MockedEntity, CommissionStorage;

    /**
     * @var CryptoCurrencyRepo
     */
    protected $cryptoCurrencyRepo;

    /**
     * Set repo
     */
    protected function setUp()
    {
        parent::setUp();
        /** @var CryptoCurrencyRepo $repo */
        $this->cryptoCurrencyRepo = app(CryptoCurrencyRepo::class);
    }

    /**
     * Retrieve test data
     *
     * @return array
     */
    public function validCommissionDataProvider()
    {
        return [
            [0, Crypto::BTC_CODE],
            [1, Crypto::BTC_CODE],
            [Crypto::ONE_BTC, Crypto::BTC_CODE],
            [Crypto::ONE_BTC*2, Crypto::BTC_CODE],
            [Crypto::ONE_ETH, Crypto::ETH_CODE],
            [Crypto::ONE_ETH*2, Crypto::ETH_CODE],
        ];
    }

    /**
     * Test that commission storage work with valid data
     *
     * @param int    $amount
     * @param string $cryptoCode
     *
     * @dataProvider validCommissionDataProvider
     */
    public function testValidPartnershipCommission(int $amount, string $cryptoCode)
    {
        $entity = $this->getMockedEntity();

        $commissionStorage = $this->getCommissionStorage(
            $amount,
            $cryptoCode,
            $entity,
            $this->getMockedReferral()
        );

        $this->assertEquals($amount, $commissionStorage->amount());
        $this->assertEquals($cryptoCode, $commissionStorage->cryptoCurrency()->getCode());
        $this->assertEquals($entity->entityType(), $commissionStorage->entity()->entityType());
        $this->assertEquals($entity->entityTitle(), $commissionStorage->entity()->entityTitle());
        $this->assertEquals($entity->entityDescription(), $commissionStorage->entity()->entityDescription());
    }

    /**
     * Test that commission storage throw exception for invalid commission
     *
     * @return void
     */
    public function testInvalidPartnershipCommission()
    {
        $this->expectException(InvalidCommissionAmountPartnershipException::class);

        $this->getCommissionStorage(
            -1000,
            Crypto::BTC_CODE,
            $this->getMockedEntity(),
            $this->getMockedReferral()
        );
    }
}
