<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-17
 * Time: 09:03
 */

namespace Tests\Unit\Services\Partnership\Traits;

use App\Contracts\Partnership\PartnershipEntityContract;
use App\Models\Deal\Deal;
use App\Models\Directory\CryptoCurrencyConstants as Crypto;
use App\Services\Partnership\Entities\DealEntity;

/**
 * Trait MockedEntity
 *
 * Provide mocked PartnershipEntityContract for testing
 *
 * @package Tests\Unit\Services\Partnership\Traits
 */
trait MockedEntity
{
    use MockedProfitStorage;

    /**
     * Retrieve mocked PartnershipEntityContract
     *
     * @param int    $profitAmount
     * @param string $profitCrypto
     * @param string $type
     * @param int    $id
     * @param string $title
     * @param string $description
     * @param array  $referrals
     *
     * @return \Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface|PartnershipEntityContract
     */
    protected function getMockedEntity(
        int $profitAmount = 10000,
        string $profitCrypto = Crypto::BTC_CODE,
        string $type = DealEntity::TYPE,
        int $id = 1,
        string $title = 'Deal-1',
        string $description = 'Test Mock Entity Description',
        array $referrals = []
    ) {
        $mock = \Mockery::mock(PartnershipEntityContract::class);
        return $mock->allows([
            'entityProfit'      => $this->getMockedProfitStorage($profitAmount, $profitCrypto),
            'entityType'        => $type,
            'entityId'          => $id,
            'entityTitle'       => $title,
            'entityDescription' => $description,
            'entityReferrals'   => $referrals
        ]);
    }
}