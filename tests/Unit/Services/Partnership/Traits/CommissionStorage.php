<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-17
 * Time: 09:06
 */

namespace Tests\Unit\Services\Partnership\Traits;

use App\Contracts\Partnership\PartnershipCommissionStorageContract;
use App\Contracts\Partnership\PartnershipEntityContract;
use App\Contracts\Partnership\ReferralContract;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Services\Partnership\Commission\PartnershipCommission;

/**
 * Trait CommissionStorage
 *
 * Allow quickly instant PartnershipCommissionStorageContract implementation
 *
 * @package Tests\Unit\Services\Partnership\Traits
 */
trait CommissionStorage
{
    /**
     * Instance commission storage
     *
     * @param                           $amount
     * @param                           $cryptoCode
     * @param PartnershipEntityContract $entity
     * @param ReferralContract          $referral
     *
     * @return PartnershipCommissionStorageContract
     */
    protected function getCommissionStorage(
        $amount,
        $cryptoCode,
        PartnershipEntityContract $entity,
        ReferralContract $referral
    ): PartnershipCommissionStorageContract {
        /** @var CryptoCurrencyRepo $repo */
        $repo = app(CryptoCurrencyRepo::class);
        return app(
            PartnershipCommission::class,
            [
                'amount'         => $amount,
                'cryptoCurrency' => $repo->getByCode($cryptoCode),
                'entity'         => $entity,
                'referral'       => $referral,
            ]
        );
    }
}