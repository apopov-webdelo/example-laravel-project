<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-17
 * Time: 08:57
 */

namespace Tests\Unit\Services\Partnership\Traits;

use App\Contracts\Partnership\ReferralContract;
use App\Makers\User\UserMaker;
use App\Models\Partnership\Partner;
use App\Models\User\User;
use Carbon\Carbon;

/**
 * Trait MockedReferral
 *
 * Provide mocked ReferralContract for testing
 *
 * @package Tests\Unit\Services\Partnership\Traits
 */
trait MockedReferral
{
    use MockedPartner;

    /**
     * Retrieve mocked ReferralContract
     *
     * @param Carbon|null $regDate
     * @param int|null    $userId
     * @param int|null    $partnerId
     *
     * @return \Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface|ReferralContract
     */
    protected function getMockedReferral(Carbon $regDate = null, int $userId = null, int $partnerId = null)
    {
        $mock = \Mockery::mock(ReferralContract::class);

        /** @var User $user */
        $user = $userId ? User::findOrFail($userId) : UserMaker::init()->create();

        $partner = $partnerId ? Partner::findOrFail($partnerId) : $this->getMockedPartner();

        return $mock->allows([
            'getUser' => $user,
            'getPartner' => $partner,
            'getRegistrationDate' => $regDate ?? now(),
        ]);
    }
}