<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-17
 * Time: 08:55
 */

namespace Tests\Unit\Services\Partnership\Traits;

use App\Contracts\Partnership\PartnerContract;
use App\Makers\User\UserMaker;
use App\Models\User\User;

/**
 * Trait MockedPartner
 *
 * Provide mocked PartnerContract for testing
 *
 * @package Tests\Unit\Services\Partnership\Traits
 */
trait MockedPartner
{
    /**
     * Retrieve mocked PartnerContract
     *
     * @param int|null $userId
     *
     * @return \Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface|PartnerContract
     */
    protected function getMockedPartner(int $userId = null)
    {
        $mock = \Mockery::mock(PartnerContract::class);

        /** @var User $user */
        $user = $userId ? User::findOrFail($userId) : UserMaker::init()->create();

        return $mock->allows([
            'getUser' => $user,
        ]);
    }
}
