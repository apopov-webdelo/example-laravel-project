<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 2018-12-17
 * Time: 09:00
 */

namespace Tests\Unit\Services\Partnership\Traits;

use App\Contracts\Partnership\PartnershipProfitStorageContract;
use App\Repositories\Directory\CryptoCurrencyRepo;

/**
 * Trait MockedProfitStorage
 *
 * Provide mocked PartnershipProfitStorageContract for testing
 *
 * @package Tests\Unit\Services\Partnership\Traits
 */
trait MockedProfitStorage
{
    /**
     * Retrieve mocked PartnershipProfitStorageContract
     *
     * @param int    $amount
     * @param string $cryptoCode
     *
     * @return \Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface|PartnershipProfitStorageContract
     */
    protected function getMockedProfitStorage(int $amount, string $cryptoCode)
    {
        $mock = \Mockery::mock(PartnershipProfitStorageContract::class);
        $repo = app(CryptoCurrencyRepo::class);

        return $mock->allows(
            [
                'amount'         => $amount,
                'cryptoCurrency' => $repo->getByCode($cryptoCode),
            ]
        );
    }
}