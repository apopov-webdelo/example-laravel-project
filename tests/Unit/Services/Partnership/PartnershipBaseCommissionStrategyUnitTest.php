<?php

namespace Tests\Unit\Services\Partnership;

use App\Services\Partnership\Commission\Strategies\Partnership1stLineCommissionStrategy;
use Tests\TestCase;
use Tests\Unit\Services\Partnership\Traits\MockedEntity;
use Tests\Unit\Services\Partnership\Traits\MockedReferral;

class PartnershipBaseCommissionStrategyUnitTest extends TestCase
{
    use MockedEntity, MockedReferral;

    const ENTITY_PROFIT = 100;

    const CONFIG = [
        'percentage_stage_1'  => 25,
        'percentage_stage_2'  => 12,
        'percentage_stage_3'  => 6,
    ];

    const STAGE_1 = 1;
    const STAGE_2 = 181;
    const STAGE_3 = 367;

    /**
     * Data provider contains valid test combinations
     *
     * @return array
     */
    public function validDataForCommission()
    {
        return [
            'CommissionReferralStage1' => [self::STAGE_1, 25, false],
            'CommissionReferralStage2' => [self::STAGE_2, 12 , false],
            'CommissionReferralStage3' => [self::STAGE_3, 6 , false],
        ];
    }

    /**
     * Test valid commissions variants
     *
     * @param int  $days
     * @param int  $expectedPercent
     * @param bool $isReferralClient
     *
     * @dataProvider validDataForCommission
     *
     * @throws \App\Exceptions\Partnership\InvalidCommissionAmountPartnershipException
     */
    public function testValidCommissionReferral(int $days, int $expectedPercent, bool $isReferralClient = true)
    {
        $entity = $this->getEntity();
        $referral = $this->getMockedReferral(now()->subDays($days));

        $calculatePercent = $this->getBaseCommissionStrategy()
                                 ->getCommission($entity, $referral)->amount();

        $this->assertEquals($expectedPercent, $calculatePercent);
    }

    /**
     * Instant entity with Deal model mocked method
     *
     * @return \App\Contracts\Partnership\PartnershipEntityContract|\Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\MockInterface
     */
    protected function getEntity()
    {
        $entity = $this->getMockedEntity(self::ENTITY_PROFIT);
        return $entity;
    }

    /**
     * Return PartnershipBaseCommissionStrategy with injected DealRepo
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    protected function getBaseCommissionStrategy()
    {
        return app(Partnership1stLineCommissionStrategy::class, [
            'config'   => self::CONFIG,
        ]);
    }
}
