<?php

namespace Tests\Unit\Services\Deal;

use App\Events\Deal\DealCancellation;
use App\Listeners\Blockchain\Deal\CancelDealToStellar;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Services\Deal\CancellationDealService;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class CancellationDealServiceTest extends TestCase
{
    use AdHelpers, DealHelpers;

    protected function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function testInstantiate()
    {
        $user = $this->registerUser();
        $this->actingAs($user);
        /* @var CancellationDealService $service */
        $service = app(CancellationDealService::class);

        $this->assertInstanceOf(CancellationDealService::class, $service);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testSetsCancellationStatus()
    {
        Event::fake();

        // seller
        $user1 = $this->registerUser();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = $this->registerUser();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED);

        // sub assert
        $this->assertEquals(DealStatusConstants::VERIFIED, $deal->status->id);

        $this->actingAs($user2);
        /* @var CancellationDealService $service */
        $service = app(CancellationDealService::class);

        // act
        $service->cancellation($deal);

        // assert
        $this->assertEquals(DealStatusConstants::CANCELLATION, $deal->status->id);
        Event::assertDispatched(DealCancellation::class);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testStellarCancelJobQueued()
    {
        Queue::fake();

        // seller
        $user1 = factory(User::class)->create();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = factory(User::class)->create();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED);

        // sub assert
        $this->assertEquals(DealStatusConstants::VERIFIED, $deal->status->id);

        $this->actingAs($user2);
        /* @var CancellationDealService $service */
        $service = app(CancellationDealService::class);

        // act
        $service->cancellation($deal);

        // assert
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == CancelDealToStellar::class;
        });
    }
}
