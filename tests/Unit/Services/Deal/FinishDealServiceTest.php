<?php

namespace Tests\Unit\Services\Deal;

use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use App\Services\Deal\FinishDealService;
use App\Services\Deal\StoreDealService;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class FinishDealServiceTest extends TestCase
{
    use AdHelpers, DealHelpers, BalanceHelpers;

    /**
     * @var FinishDealService
     */
    private $service;

    /**
     * @var User
     */
    private $userAd;

    /**
     * @var User
     */
    private $userDeal;

    protected function setUp()
    {
        parent::setUp();
        $this->userAd = $this->registerUser();
        $this->actingAs($this->userAd);
        $this->service = app(FinishDealService::class);
    }

    /** @test */
    public function testInstantiate()
    {
        $this->assertInstanceOf(FinishDealService::class, $this->service);
    }

    /** @test
     * @throws \Exception
     */
    public function testSellAdCommissions()
    {
        // setup
        $testFullbalance = CryptoCurrencyConstants::ONE_BTC;
        $testDealAmount = CryptoCurrencyConstants::HALF_BTC;

        $this->userDeal = $this->registerUser(['login' => 'BUYER']);
        $crypto = $this->getCryptoByCode('btc');
        $this->addBalance($this->userAd, $crypto, $testFullbalance);

        $this->makeAd($this->userAd, [
            'is_sale'            => true, // author is SELLER
            'crypto_currency_id' => $crypto,
            'price'              => 100000,
            'min'                => 100000,
            'max'                => 100000,
            'original_max'       => 100000,
            'commission_percent' => 1000 // 10%
        ]);
        $ad = $this->lastAd($this->userAd);

        // make deal
        $this->actingAs($this->userDeal);
        /* @var StoreDealService $storeService */
        $storeService = app(StoreDealService::class, ['ad' => $ad]);
        $deal = $storeService->store(['fiat_amount' => 500, 'ad_id' => $ad->id]); // buys Half BTC
        $deal->statuses()->attach(DealStatusConstants::VERIFIED);

        // assert for escrows
        $balance = $this->userAd->getBalance($crypto);
        $escrow = $deal->crypto_amount + $deal->commission;
        $this->assertEquals($escrow, $balance->escrow);
        $this->assertEquals($testFullbalance - $escrow, $balance->amount);

        // act
        $this->actingAs($this->userDeal);
        $this->service->finish($deal);

        // assert
        $balanceAuthor = $this->userAd->getBalance($crypto);
        $this->assertEquals(0, $balanceAuthor->escrow);
        $this->assertEquals($testFullbalance - $escrow, $balanceAuthor->amount);

        $balanceBuyer = $this->userDeal->getBalance($crypto);
        $this->assertEquals(0, $balanceBuyer->escrow);
        $this->assertEquals($testDealAmount, $balanceBuyer->amount);
    }

    /** @test
     * @throws \Exception
     */
    public function testBuyAdCommissions()
    {
        // setup
        $testFullbalance = CryptoCurrencyConstants::ONE_BTC;
        $testDealAmount = CryptoCurrencyConstants::HALF_BTC;

        $this->userDeal = $this->registerUser(['login' => 'SELLER']);
        $crypto = $this->getCryptoByCode('btc');
        $this->addBalance($this->userDeal, $crypto, $testFullbalance);

        $this->makeAd($this->userAd, [
            'is_sale'            => false, // author is BUYER
            'crypto_currency_id' => $crypto,
            'price'              => 100000,
            'min'                => 100000,
            'max'                => 100000,
            'original_max'       => 100000,
            'commission_percent' => 1000 // 10%
        ]);
        $ad = $this->lastAd($this->userAd);

        // make deal
        $this->actingAs($this->userDeal);
        /* @var StoreDealService $storeService */
        $storeService = app(StoreDealService::class, ['ad' => $ad]);
        $deal = $storeService->store(['fiat_amount' => 500, 'ad_id' => $ad->id]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED);

        // assert for escrows
        $balance = $this->userDeal->getBalance($crypto);
        $this->assertEquals($testDealAmount, $balance->escrow);
        $this->assertEquals($testFullbalance - $testDealAmount, $balance->amount);

        // act
        $this->actingAs($this->userDeal);
        $this->service->finish($deal);

        // assert
        $balanceSeller = $this->userDeal->getBalance($crypto);
        $this->assertEquals(0, $balanceSeller->escrow);
        $this->assertEquals($testFullbalance - $testDealAmount, $balanceSeller->amount);

        $balanceAuthor = $this->userAd->getBalance($crypto);
        $this->assertEquals(0, $balanceAuthor->escrow);
        $this->assertEquals($testDealAmount - $deal->commission, $balanceAuthor->amount);
    }
}
