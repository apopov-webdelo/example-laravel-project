<?php

namespace Tests\Unit\Services\Deal;

use App\Events\Deal\DealVerified;
use App\Models\Deal\DealStatusConstants;
use App\Services\Deal\VerifyDealService;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class VerifyDealServiceTest extends TestCase
{
    use AdHelpers, DealHelpers;

    protected function setUp()
    {
        parent::setUp();
        Event::fake();
    }

    /** @test */
    public function testInstantiate()
    {
        /* @var VerifyDealService $service */
        $service = app(VerifyDealService::class);

        $this->assertInstanceOf(VerifyDealService::class, $service);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testSetsVerifiedForDeal()
    {
        Event::fake();

        // seller
        $user1 = $this->registerUser();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = $this->registerUser();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::VERIFICATION);

        // sub assert
        $this->assertEquals(DealStatusConstants::VERIFICATION, $deal->status->id);

        /* @var VerifyDealService $service */
        $service = app(VerifyDealService::class);

        // act
        $service->verified($deal);

        // assert
        $this->assertEquals(DealStatusConstants::VERIFIED, $deal->status->id);
        Event::assertDispatched(DealVerified::class);
    }
}
