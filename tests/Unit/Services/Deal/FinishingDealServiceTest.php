<?php

namespace Tests\Unit\Services\Deal;

use App\Events\Deal\DealFinishing;
use App\Listeners\Blockchain\Deal\FinishDealToStellar;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Services\Deal\FinishingDealService;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class FinishingDealServiceTest extends TestCase
{
    use AdHelpers, DealHelpers;

    protected function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function testInstantiate()
    {
        $user = $this->registerUser();
        $this->actingAs($user);
        /* @var FinishingDealService $service */
        $service = app(FinishingDealService::class);

        $this->assertInstanceOf(FinishingDealService::class, $service);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testStatusChanged()
    {
        Event::fake();

        // seller
        $user1 = $this->registerUser();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = $this->registerUser();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::PAID);

        // sub assert
        $this->assertEquals(DealStatusConstants::PAID, $deal->status->id);

        // act
        $this->actingAs($user1);
        /* @var FinishingDealService $service */
        $service = app(FinishingDealService::class);
        $service->finishing($deal);

        // assert
        $this->assertEquals(DealStatusConstants::FINISHING, $deal->status->id);
        Event::assertDispatched(DealFinishing::class);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testStellarFinishJobQueued()
    {
        Queue::fake();

        // seller
        $user1 = factory(User::class)->create();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = factory(User::class)->create();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::PAID);

        // sub assert
        $this->assertEquals(DealStatusConstants::PAID, $deal->status->id);

        // act
        $this->actingAs($user1);
        /* @var FinishingDealService $service */
        $service = app(FinishingDealService::class);
        $service->finishing($deal);

        // assert
        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == FinishDealToStellar::class;
        });
    }
}
