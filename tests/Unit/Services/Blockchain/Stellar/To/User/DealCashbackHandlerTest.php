<?php

namespace Tests\Unit\Services\Blockchain\Stellar\To\User;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Contracts\DealCashback\DealCashbackProcessServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Contracts\Services\Blockchain\To\User\ProcessDealCashbackContract;
use App\Exceptions\Blockchain\Stellar\BadRequestStellarException;
use App\Services\Blockchain\Stellar\To\User\ProcessDealCashback;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class DealCashbackHandlerTest extends TestCase
{
    use DealHelpers, AdHelpers;

    /** @test */
    public function testCanInstantiate()
    {
        /** @var ProcessDealCashbackContract $handler */
        $handler = app(ProcessDealCashbackContract::class);

        $this->assertInstanceOf(ProcessDealCashback::class, $handler);
        $this->assertInstanceOf(ProcessDealCashbackContract::class, $handler);
    }

    /** @test */
    public function testCallRewardCashback()
    {
        $dealCount = env('DEAL_CASHBACK_DEAL_QUANTITY') - 1;

        $userAd = $this->registerUser(['login' => 'userAd']);
        $this->makeAd($userAd, ['is_sale' => true]);

        $userDeal = $this->registerUser(['login' => 'userDeal']);
        $this->times($dealCount)->makeDeal($userDeal, $this->lastAd($userAd));

        /* @var DealCashbackProcessServiceContract $service */
        $service = app(DealCashbackProcessServiceContract::class);

        $this->getDeals($userDeal)->each(function ($deal) use ($service) {
            $service->process($deal);
        });

        /* @var DealCashbackRepoContract $cbRepo */
        $cbRepo = app(DealCashbackRepoContract::class);
        $cashback = $cbRepo->getInProgressOrCreate($userAd, $this->lastDeal($userDeal)->cryptoCurrency);

        /** @var ProcessDealCashbackContract $handler */
        $handler = app(ProcessDealCashbackContract::class);

        $commissionStorage = app(DealCashbackCommissionStorageContract::class, ['cashback' => $cashback]);

        // assert tries to connect to Stellar
        $this->expectException(BadRequestStellarException::class);
        $handler->reward($commissionStorage);
    }
}
