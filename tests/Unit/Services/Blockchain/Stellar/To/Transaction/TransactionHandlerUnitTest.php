<?php


namespace Tests\Unit\Services\Blockchain\Stellar\To\Transaction;


use App\Contracts\Services\Blockchain\To\Transaction\TransactionContract;
use App\Services\Blockchain\Stellar\To\Transaction\TransactionHandler;
use Tests\TestCase;

class TransactionHandlerUnitTest extends TestCase
{
    /** @test */
    public function testCanInstantiateService()
    {
        $service = app(TransactionContract::class);

        $this->assertInstanceOf(TransactionHandler::class, $service);
    }

    /** @test */
    public function testUserSendsCryptoWithZeroBalance()
    {
        $this->assertTrue(true);
        return;

        $userFrom = $this->registerUser(['login' => 'testFrom']);
        $userTo = $this->registerUser(['login' => 'testTo']);
        $crypto = $this->getRandomCrypto();

        /* @var TransactionContract $service */
        $service = app(TransactionContract::class);

        $result = $service->transaction($userFrom, $userTo, $crypto, 0);
        $this->assertTrue($result);

        $result = $service->transaction($userFrom, $userTo, $crypto, 100000000);
        $this->assertFalse($result, 'Stellar transfered crypto but users balance expected to be 0');
    }
}
