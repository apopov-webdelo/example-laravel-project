<?php

namespace Tests\Unit\Services\Blockchain\Stellar\From\Deal;

use App\Events\Blockchain\From\Deal\DealVerified;
use App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException;
use App\Exceptions\Event\UndefinedEventException;
use App\Models\Deal\DealStatusConstants;
use App\Services\Blockchain\Stellar\From\Deal\DealService;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class DealVerifyHandlerTest extends TestCase
{
    use AdHelpers, DealHelpers;

    /** @test */
    public function testInstantiate()
    {
        /* @var DealService $service */
        $service = app(DealService::class);

        $this->assertInstanceOf(DealService::class, $service);
    }

    /**
     * @test
     * @throws InvalidControlSumStellarException
     * @throws UndefinedEventException
     * @throws \ReflectionException
     */
    public function testDealStatusChangedToVerified()
    {
        Event::fake();

        // seller
        $user1 = $this->registerUser();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = $this->registerUser();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::VERIFICATION);

        // sub assert
        $this->assertEquals(DealStatusConstants::VERIFICATION, $deal->status->id);

        // act
        /* @var DealService|DealService $service */
        $service = app(DealService::class);
        $timestamp = time();
        $service->controlSum(
            md5($deal->id . '-' . $timestamp . '-' . config('app.blockchain.stellar.private_key')),
            $timestamp
        );
        $service->verified($deal);

        // assert
        $this->assertEquals(DealStatusConstants::VERIFIED, $deal->status->id);
        Event::assertDispatched(DealVerified::class);
    }
}
