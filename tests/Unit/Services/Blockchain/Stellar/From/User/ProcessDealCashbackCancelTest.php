<?php

namespace Tests\Unit\Services\Blockchain\Stellar\From\User;

use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackCancelContract;
use App\Events\DealCashback\DealCashbackCancelState;
use App\Http\Requests\Blockchain\DealCashback\CancelRequest;
use App\Listeners\DealCashback\SendCashbackCancelAlert;
use App\Models\DealCashback\DealCashback;
use App\Services\Blockchain\Stellar\From\User\ProcessDealCashbackCancel;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class ProcessDealCashbackCancelTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Queue::fake();
    }

    /** @test */
    public function testInstantiate()
    {
        $request = new CancelRequest([
            'error'  => 'random',
            'reason' => 'random',
            'hash'   => 'random',
        ]);

        /* @var ProcessDealCashbackCancel $handler */
        $handler = app(ProcessDealCashbackCancelContract::class, ['request' => $request]);

        $this->assertInstanceOf(ProcessDealCashbackCancelContract::class, $handler);
        $this->assertInstanceOf(ProcessDealCashbackCancel::class, $handler);
    }

    /** @test
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function testCancelsCashback()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_PENDING]);

        $request = new CancelRequest([
            'error'  => 'error_text',
            'reason' => 'reason_text',
            'hash'   => md5($cashback->id . '-error_text-reason_text-' . config('app.blockchain.stellar.private_key')),
        ]);

        /* @var ProcessDealCashbackCancel $handler */
        $handler = app(ProcessDealCashbackCancelContract::class, ['request' => $request]);
        $handler->cancel($cashback);

        $cashback = $cashback->fresh();

        $this->assertEquals(DealCashback::STATUS_CANCELED, $cashback->status);
    }

    /** @test
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function testFiresEvent()
    {
        Event::fake();

        $this->testCancelsCashback();

        Event::assertDispatched(DealCashbackCancelState::class);
    }

    /** @test
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function testListenerDispatched()
    {
        $this->testCancelsCashback();

        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == SendCashbackCancelAlert::class;
        });
    }
}
