<?php

namespace Tests\Unit\Services\Blockchain\Stellar\From\User;

use App\Contracts\Services\Blockchain\From\User\ProcessDealCashbackDoneContract;
use App\Events\DealCashback\DealCashbackDoneState;
use App\Http\Requests\Blockchain\DealCashback\DoneRequest;
use App\Listeners\DealCashback\CashbackTransfer;
use App\Models\DealCashback\DealCashback;
use App\Services\Blockchain\Stellar\From\User\ProcessDealCashbackDone;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class ProcessDealCashbackDoneTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Queue::fake();
    }

    /** @test */
    public function testInstantiate()
    {
        $request = new DoneRequest([
            'transaction_id' => 9999,
            'timestamp'      => time(),
            'hash'           => 'random',
        ]);

        /* @var ProcessDealCashbackDone $handler */
        $handler = app(ProcessDealCashbackDoneContract::class, ['request' => $request]);

        $this->assertInstanceOf(ProcessDealCashbackDoneContract::class, $handler);
        $this->assertInstanceOf(ProcessDealCashbackDone::class, $handler);
    }

    /** @test
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function testCashbackIsDone()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_PENDING]);

        $request = new DoneRequest([
            'transaction_id' => 9999,
            'timestamp'      => 12345,
            'hash'           => md5($cashback->id . '-' . 9999 . '-' . 12345 . '-' . config('app.blockchain.stellar.private_key')),
        ]);

        /* @var ProcessDealCashbackDone $handler */
        $handler = app(ProcessDealCashbackDoneContract::class, ['request' => $request]);
        $handler->done($cashback);

        $this->assertEquals(DealCashback::STATUS_DONE, $cashback->status);
    }

    /** @test
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function testFiresEvent()
    {
        Event::fake();

        $this->testCashbackIsDone();

        Event::assertDispatched(DealCashbackDoneState::class);
    }

    /** @test
     * @throws \App\Exceptions\Blockchain\Stellar\InvalidControlSumStellarException
     */
    public function testListenerDispatched()
    {
        $this->testCashbackIsDone();

        Queue::assertPushed(CallQueuedListener::class, function ($job) {
            return $job->class == CashbackTransfer::class;
        });
    }
}
