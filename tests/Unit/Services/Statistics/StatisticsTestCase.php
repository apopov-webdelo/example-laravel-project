<?php

namespace Tests\Unit\Services\Statistics;

use App\Jobs\Deal\UpdateDealCryptoAmountInUsd;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\CreatesApplication;
use Tests\TestCase;

abstract class StatisticsTestCase extends TestCase
{
    use CreatesApplication, DatabaseTransactions;

    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User $user
     */
    protected $dealUser;

    /**
     * Ads created for user
     *
     * @var Ad[]
     */
    protected $ads = [];

    /**
     * Deals created for dealUser
     *
     * @var Deal[]
     */
    protected $deals = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser('user');

        // Create a deal author
        $this->dealUser = $this->createUser('dealUser');
    }

    /**
     * Create Ads for user
     *
     * @param User  $user
     * @param int   $count
     *
     * @param array $options
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1, $options = [])
    {
        for ($i = 0; $i < $count; $i++) {
            if (array_key_exists('payment_system_id', $options)) {
                $paymentSystem = PaymentSystem::find($options['payment_system_id']);
            } else {
                $paymentSystem = PaymentSystem::all()->random(1)->first();
            }

            $newAd = AdMaker::init($options)
                            ->author($user)
                            ->paymentSystem($paymentSystem);

            if (array_key_exists('is_sale', $options)) {
                $newAd->sale($options['is_sale']);
            }

            $this->ads[] = $newAd->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array     $data
     *
     * @param bool      $bSkipUpdateDispatch
     *
     * @return void
     * @throws \Exception
     */
    protected function storeDeals(User $user = null, $data = [], $bSkipUpdateDispatch = false)
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $this->deals[] = DealMaker::init($data)
                                      ->author($user)
                                      ->status(DealStatusConstants::FINISHED)
                                      ->create();
        }

        if (!$bSkipUpdateDispatch) {
            foreach ($this->deals as $deal) {
                UpdateDealCryptoAmountInUsd::dispatch($deal);
            }
        }
    }

}
