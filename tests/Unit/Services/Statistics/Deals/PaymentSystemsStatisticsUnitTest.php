<?php

namespace Tests\Unit\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\PaymentSystemsStatisticsContract;
use App\Models\Directory\PaymentSystem;
use App\Services\Statistics\Deals\PaymentSystemsStatistics;
use Tests\TestApi\Traits\FiatHelpers;
use Tests\Unit\Services\Statistics\StatisticsTestCase;

class PaymentSystemsStatisticsUnitTest extends StatisticsTestCase
{
    use FiatHelpers;

    /** @test */
    public function testItCanInstantiate()
    {
        $paymentStats = app(PaymentSystemsStatisticsContract::class);

        $this->assertInstanceOf(PaymentSystemsStatistics::class, $paymentStats);
    }

    /** @test
     * @throws \Exception
     */
    public function testGetStatsWithoutFiltersOnePayType()
    {
        $paymentSys = PaymentSystem::all()->random(1)->first()->id;

        $this->storeAds($this->user, 1, [
            'author_id'         => $this->user->id,
            'payment_system_id' => $paymentSys,
            'currency_id'       => $this->getUsdCurrency(),
        ]);
        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser->id]);

        /* @var PaymentSystemsStatistics $paymentStats */
        $paymentStats = app(PaymentSystemsStatisticsContract::class);
        $paymentStats->member($this->dealUser);

        $result = $paymentStats->getStatistics();

        $this->assertArrayHasKey($paymentSys, $result);
        $this->assertNotEquals(0, $result[$paymentSys]);
    }

    /** @test
     * @throws \Exception
     */
    public function testGetStatsWithoutFilters2PayTypes()
    {
        $payments = PaymentSystem::all()->random(2);
        $paymentSys1 = $payments->first()->id;
        $paymentSys2 = $payments->last()->id;

        $this->storeAds($this->user, 5, [
            'author_id'         => $this->user->id,
            'payment_system_id' => $paymentSys1,
            'currency_id'       => $this->getUsdCurrency(),
        ]);
        $this->storeAds($this->user, 5, [
            'author_id'         => $this->user->id,
            'payment_system_id' => $paymentSys2,
            'currency_id'       => $this->getUsdCurrency(),
        ]);

        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser->id]);

        /* @var PaymentSystemsStatistics $paymentStats */
        $paymentStats = app(PaymentSystemsStatisticsContract::class);
        $paymentStats->member($this->dealUser);

        $result = $paymentStats->getStatistics();

        $this->assertArrayHasKey($paymentSys1, $result);
        $this->assertNotEquals(0, $result[$paymentSys1]);
        $this->assertArrayHasKey($paymentSys2, $result);
        $this->assertNotEquals(0, $result[$paymentSys2]);
    }

    /** @test
     * @throws \Exception
     */
    public function testGetStatsWithoutFilters3PayTypes()
    {
        $paymentSystems = PaymentSystem::all()->random(3);

        $arExpected = [];
        foreach ($paymentSystems as $paymentSystem) {
            $this->storeAds($this->user, 5, [
                'author_id'         => $this->user->id,
                'payment_system_id' => $paymentSystem->id,
                'currency_id'       => $this->getUsdCurrency(),
            ]);

            $arExpected[$paymentSystem->id] = 0;
        }

        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser->id]);

        /* @var PaymentSystemsStatistics $paymentStats */
        $paymentStats = app(PaymentSystemsStatisticsContract::class);
        $paymentStats->member($this->dealUser);

        $result = $paymentStats->getStatistics();

        foreach ($arExpected as $paySysId => $val) {
            $this->assertArrayHasKey($paySysId, $result);
            $this->assertNotEquals(0, $result[$paySysId]);
        }
    }

    /** @test
     * @throws \Exception
     */
    public function testGetStatsWithoutFiltersManyPayTypes()
    {
        $paymentSystems = PaymentSystem::all()->random(6);

        $arExpected = [];
        foreach ($paymentSystems as $paymentSystem) {
            $this->storeAds($this->user, 5, [
                'author_id'         => $this->user->id,
                'payment_system_id' => $paymentSystem->id,
                'currency_id'       => $this->getUsdCurrency(),
            ]);

            $arExpected[$paymentSystem->id] = 25;
        }

        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser->id]);

        /* @var PaymentSystemsStatistics $paymentStats */
        $paymentStats = app(PaymentSystemsStatisticsContract::class);
        $paymentStats->member($this->dealUser);

        $result = $paymentStats->getStatistics();

        foreach ($arExpected as $paySysId => $val) {
            $this->assertArrayHasKey($paySysId, $result);
            $this->assertNotEquals(0, $result[$paySysId]);
        }
    }
}
