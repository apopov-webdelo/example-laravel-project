<?php

namespace Tests\Unit\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\CurrenciesStatisticsContract;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Services\Statistics\Deals\CurrenciesStatistics;
use Tests\Unit\Services\Statistics\StatisticsTestCase;

class CurrenciesStatisticsUnitTest extends StatisticsTestCase
{
    /** @test */
    public function testItCanInstantiate()
    {
        $currencyStats = app(CurrenciesStatisticsContract::class);

        $this->assertInstanceOf(CurrenciesStatistics::class, $currencyStats);
    }

    /** @test */
    public function testGetStatsWithoutFilter()
    {
        $this->ads = []; // reset
        $this->storeAds($this->user, 2, ['crypto_currency_id' => $this->getRandomCrypto()]);
        $this->storeAds($this->user, 2, ['crypto_currency_id' => $this->getRandomCrypto()]);
        $this->storeAds($this->user, 2, ['crypto_currency_id' => $this->getRandomCrypto()]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var CurrenciesStatistics $currencyStats */
        $currencyStats = app(CurrenciesStatisticsContract::class);
        $currencyStats->member($this->dealUser);

        $result = $currencyStats->getStatistics();

        $this->assertArrayHasKey('fiat', $result);
        $this->assertArrayHasKey('crypto', $result);
    }

    /** @test */
    public function testGetStatsWithCryptoFilter()
    {
        $crypto = CryptoCurrency::all()->random(2);
        $cryptoFirst = $crypto->first();
        $cryptoSecond = $crypto->last();

        $this->ads = []; // reset
        $this->storeAds($this->user, 1, ['crypto_currency_id' => $cryptoFirst->id]);
        $this->storeAds($this->user, 2, ['crypto_currency_id' => $cryptoSecond->id]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var CurrenciesStatistics $currencyStats */
        $currencyStats = app(CurrenciesStatisticsContract::class);
        $currencyStats->member($this->dealUser)
                      ->cryptoCurrency($cryptoSecond);

        $result = $currencyStats->getStatistics();

        $this->assertArrayHasKey('fiat', $result);
        $this->assertArrayHasKey('crypto', $result);
        $this->assertArrayHasKey($cryptoSecond->code, $result['crypto']);
        $this->assertArrayNotHasKey($cryptoFirst->code, $result['crypto']);
    }

    /** @test */
    public function testGetStatsWithCurrencyFilter()
    {
        $currency = Currency::all()->random(2);
        $currencyFirst = $currency->first();
        $currencySecond = $currency->last();

        $this->ads = []; // reset
        $this->storeAds($this->user, 1, ['currency_id' => $currencyFirst->id]);
        $this->storeAds($this->user, 2, ['currency_id' => $currencySecond->id]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var CurrenciesStatistics $currencyStats */
        $currencyStats = app(CurrenciesStatisticsContract::class);
        $currencyStats->member($this->dealUser)
                      ->currency($currencySecond);

        $result = $currencyStats->getStatistics();

        $this->assertArrayHasKey('fiat', $result);
        $this->assertArrayHasKey('crypto', $result);
        $this->assertArrayHasKey($currencySecond->code, $result['fiat']);
        $this->assertArrayNotHasKey($currencyFirst->code, $result['fiat']);
    }
}
