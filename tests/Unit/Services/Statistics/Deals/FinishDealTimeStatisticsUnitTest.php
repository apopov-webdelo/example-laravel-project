<?php

namespace Tests\Unit\Services\Statistics\Deals;

use App\Makers\DealMaker;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Services\Statistics\Deals\FinishDealTimeStatistics;
use Carbon\Carbon;
use Tests\TestCase;

class FinishDealTimeStatisticsUnitTest extends TestCase
{
    /**
     * @var FinishDealTimeStatistics
     */
    private $finishDealTimeStatistics;

    protected function setUp()
    {
        parent::setUp();
        $this->finishDealTimeStatistics = app(FinishDealTimeStatistics::class);
    }

    /**
     * Data provider with valid elements
     *
     * @return array
     */
    public function validFinishDealTimeDataProvider()
    {
        return [
            '900 seconds'                          => [900, now()->subSecond(1000), now()->subSecond(100)],
            '1500 seconds'                         => [1500, now()->subSecond(2000), now()->subSecond(500)],
            '5000 seconds'                         => [5000, now()->subSecond(5000), now()->subSecond(0)],
            '0 seconds - instantly deal\'s finish' => [0, now()->subSecond(0), now()->subSecond(0)],
            '0 seconds - without pay status'       => [0, null, null],
            '0 seconds - without finish status'    => [0, null, null],
        ];
    }

    /**
     * Test valid deal finish time calculation
     *
     * @param Carbon|null $payStatusCreatedDate
     * @param Carbon|null $finishStatusCreatedDate
     * @param int         $validFinishDealTimeInSeconds
     *
     * @throws \Exception
     * @dataProvider validFinishDealTimeDataProvider
     */
    public function testValidFinishDealTimeCalculation(
        int $validFinishDealTimeInSeconds,
        Carbon $payStatusCreatedDate = null,
        Carbon $finishStatusCreatedDate = null
    ) {
        $deal = $this->createDeal($payStatusCreatedDate, $finishStatusCreatedDate);

        $this->assertEquals($validFinishDealTimeInSeconds, $this->finishDealTimeStatistics->timeInSeconds($deal));
    }

    /**
     * Create deal with needed statuses
     *
     * @param Carbon $payStatusCreatedDate
     * @param Carbon $finishStatusCreatedDate
     *
     * @return Deal
     * @throws \Exception
     */
    private function createDeal(Carbon $payStatusCreatedDate = null, Carbon $finishStatusCreatedDate = null): Deal
    {
        $dealMaker = DealMaker::init();

        if ($payStatusCreatedDate) {
            $dealMaker->status(DealStatusConstants::PAID, $payStatusCreatedDate);
        };

        if ($finishStatusCreatedDate) {
            $dealMaker->status(DealStatusConstants::FINISHED, $finishStatusCreatedDate);
        };

        return $dealMaker->create();
    }
}
