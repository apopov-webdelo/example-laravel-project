<?php

namespace Tests\Unit\Services\Statistics\Deals;

use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\PaymentSystem;
use App\Services\Statistics\Deals\DealStatisticsResult;
use Tests\TestApi\Traits\FiatHelpers;
use Tests\Unit\Services\Statistics\StatisticsTestCase;

class DealStatisticsResultUnitTest extends StatisticsTestCase
{
    use FiatHelpers;

    /** @test */
    public function testItCanInstantiate()
    {
        /* @var DealStatisticsResult $statsResult */
        $statsResult = app(DealStatisticsResult::class);

        $this->assertInstanceOf(DealStatisticsResult::class, $statsResult);
    }

    /** @test */
    public function testGetStatsOperations()
    {
        $this->ads = []; // reset
        $this->storeAds($this->user, 5, ['is_sale' => true, 'author_id' => $this->user,]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var DealStatisticsResult $statsResult */
        $statsResult = app(DealStatisticsResult::class);
        $statsResult->member($this->dealUser);

        $result = $statsResult->getStatsOperations();
        $this->assertEquals(5, $result['buy']['count']);
        $this->assertEquals(100, $result['buy']['percentage']);
    }

    /** @test */
    public function testGetStatsPaymentSystems()
    {
        $paySys = PaymentSystem::all()->random(1)->first()->id;

        $this->ads = []; // reset
        $this->storeAds($this->user, 5, [
            'author_id'         => $this->user,
            'is_sale'           => true,
            'payment_system_id' => $paySys,
            'currency_id'       => $this->getUsdCurrency(),
        ]);
        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser]);

        /* @var DealStatisticsResult $statsResult */
        $statsResult = app(DealStatisticsResult::class);
        $statsResult->member($this->dealUser);

        $result = $statsResult->getStatsPaymentSystems();
        $this->assertArrayHasKey($paySys, $result);
        $this->assertEquals(100, $result[$paySys]['percentage']);
    }

    /**
     * @test
     */
    public function testGetStatsPaymentCrypto()
    {
        $crypto = CryptoCurrency::all()->random(1)->first();

        $this->ads = []; // reset
        $this->storeAds($this->user, 5, [
            'author_id'          => $this->user,
            'is_sale'            => true,
            'crypto_currency_id' => $crypto,
            'currency_id'        => $this->getUsdCurrency(),
        ]);
        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser]);

        /* @var DealStatisticsResult $statsResult */
        $statsResult = app(DealStatisticsResult::class);
        $statsResult->member($this->dealUser);

        $result = $statsResult->getStatsCrypto();
        $this->assertArrayHasKey($crypto->getCode(), $result);
        $this->assertEquals(100, $result[$crypto->getCode()]['percentage']);
    }

    /** @test */
    public function testGetStatsPaymentFiat()
    {
        $fiat = $this->getUsdCurrency();

        $this->ads = []; // reset
        $this->storeAds($this->user, 5, [
            'author_id'   => $this->user,
            'is_sale'     => true,
            'currency_id' => $fiat,
        ]);
        $this->storeDeals($this->dealUser, ['author_id' => $this->dealUser]);

        /* @var DealStatisticsResult $statsResult */
        $statsResult = app(DealStatisticsResult::class);
        $statsResult->member($this->dealUser);

        $result = $statsResult->getStatsFiat();
        $this->assertArrayHasKey($fiat->getCode(), $result);
        $this->assertEquals(100, $result[$fiat->getCode()]['percentage']);
    }
}
