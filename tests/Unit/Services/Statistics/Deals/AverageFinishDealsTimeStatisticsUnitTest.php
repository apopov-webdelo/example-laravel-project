<?php

namespace Tests\Unit\Services\Statistics\Deals;

use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Services\Statistics\Deals\AverageFinishDealsTimeStatistics;
use Tests\TestCase;

class AverageFinishDealsTimeStatisticsUnitTest extends TestCase
{
    /**
     * @var AverageFinishDealsTimeStatistics
     */
    private $service;

    /**
     * @var User
     */
    private $user;

    /**
     * Finish Deal Times for generated deals
     *
     * @var int[]
     */
    private $finishTimes;

    protected function setUp()
    {
        parent::setUp();
        $this->service = app(AverageFinishDealsTimeStatistics::class);
    }

    /**
     * Test is service valid calculates average finish deals time
     *
     * @throws \Exception
     */
    public function testAverageDealsFinishTimeInSeconds()
    {
        $user = UserMaker::init()->create();
        $this->createDeals($user);
        $this->assertEquals($this->getAverageFinishDealsTime(), $this->service->averageTimeInSeconds($user));
    }

    /**
     * Create deals where user is seller
     *
     * @param User $seller
     * @param int  $numbers
     *
     * @throws \Exception
     */
    private function createDeals(User $seller, int $numbers = 5)
    {
        for ($i=1; $i<=5; $i++) {
            $this->createDeal($seller);
        }
    }

    /**
     * Create deal where user is seller
     *
     * @param User $seller
     *
     * @return Deal
     * @throws \Exception
     */
    private function createDeal(User $seller): Deal
    {
        $paidStatusSubTime = 10000;
        $finishStatusSubTime = $paidStatusSubTime - $this->generateFinishTime();
        return DealMaker::init(['ad_id' => $this->createAd($seller)->id])
            ->status(DealStatusConstants::PAID, now()->subSecond($paidStatusSubTime))
            ->status(DealStatusConstants::FINISHED, now()->subSecond($finishStatusSubTime))
            ->create();
    }

    /**
     * Create Ad where user is seller
     *
     * @param User $seller
     *
     * @return Ad
     */
    private function createAd(User $seller): Ad
    {
        return AdMaker::init([
            'is_active' => true,
            'is_sale'   => true,
            'author_id' => $seller->getId(),
        ])->create();
    }

    /**
     * Generate random finish time and save that value to array
     *
     * @return int
     */
    private function generateFinishTime(): int
    {
        $this->finishTimes[] = $time = rand(1000, 5000);
        return $time;
    }

    /**
     * Calculate average times for created deals
     *
     * @return int
     */
    private function getAverageFinishDealsTime(): int
    {
        return (int)collect($this->finishTimes)->average();
    }
}
