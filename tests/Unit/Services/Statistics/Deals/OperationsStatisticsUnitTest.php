<?php

namespace Tests\Unit\Services\Statistics\Deals;

use App\Contracts\Services\Statistics\Deals\OperationsStatisticsContract;
use App\Services\Statistics\Deals\CurrenciesStatistics;
use App\Services\Statistics\Deals\OperationsStatistics;
use Tests\Unit\Services\Statistics\StatisticsTestCase;

class OperationsStatisticsUnitTest extends StatisticsTestCase
{
    /** @test */
    public function testItCanInstantiate()
    {
        $operationStats = app(OperationsStatisticsContract::class);

        $this->assertInstanceOf(OperationsStatistics::class, $operationStats);
    }

    /** @test */
    public function testGetStatsEmptyDeals()
    {
        /* @var OperationsStatistics $operationStats */
        $operationStats = app(OperationsStatisticsContract::class);
        $operationStats->member($this->dealUser);

        $result = $operationStats->getStatistics();

        $this->assertArrayHasKey('sale', $result);
        $this->assertArrayHasKey('buy', $result);
        $this->assertEquals(0, $result['buy']['count']);
        $this->assertEquals(0, $result['buy']['percentage']);
        $this->assertEquals(0, $result['sale']['count']);
        $this->assertEquals(0, $result['sale']['percentage']);
    }

    /** @test */
    public function testGetStatsSale()
    {
        $this->storeAds($this->user, 10, ['is_sale' => true]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var OperationsStatistics $operationStats */
        $operationStats = app(OperationsStatisticsContract::class);
        $operationStats->member($this->dealUser);

        $result = $operationStats->getStatistics();

        $this->assertEquals(10, $result['buy']['count']);
        $this->assertEquals(100, $result['buy']['percentage']);
    }

    /** @test */
    public function testGetStatsBuy()
    {
        $this->storeAds($this->user, 10, ['is_sale' => false]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var OperationsStatistics $operationStats */
        $operationStats = app(OperationsStatisticsContract::class);
        $operationStats->member($this->dealUser);

        $result = $operationStats->getStatistics();

        $this->assertEquals(10, $result['sale']['count']);
        $this->assertEquals(100, $result['sale']['percentage']);
    }

    /** @test */
    public function testGetStatsBuyAndSale()
    {
        $this->storeAds($this->user, 10, ['is_sale' => false]);
        $this->storeAds($this->user, 20, ['is_sale' => true]);
        $this->storeDeals($this->dealUser, [], true);

        /* @var OperationsStatistics $operationStats */
        $operationStats = app(OperationsStatisticsContract::class);
        $operationStats->member($this->dealUser);

        $result = $operationStats->getStatistics();

        $this->assertEquals(10, $result['sale']['count']);
        $this->assertEquals(33, $result['sale']['percentage']);
        $this->assertEquals(20, $result['buy']['count']);
        $this->assertEquals(67, $result['buy']['percentage']);
    }
}
