<?php

namespace Tests\Unit\Services\Statistics;

use App\Contracts\Statistics\Ads\AdStatisticsContract;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Models\Ad\Ad;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use Carbon\Carbon;
use Tests\TestCase;

class AdStatsUnitTest extends TestCase
{
    const ROUTE = 'stats.ad';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    const PERIOD_TOTAL = 5;
    const PERIOD_START = 2;
    const PERIOD_END = 4;

    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User $user
     */
    protected $dealUser;

    /**
     * Ads created for user
     *
     * @var Ad[]
     */
    protected $ads = [];

    /**
     * Deals created for dealUser
     *
     * @var Deal[]
     */
    protected $deals = [];

    /**
     * Array for Deals dates period
     *
     * @var array
     */
    protected $arrayPeriod = [];

    /**
     * Fixture for test
     *
     * @throws \Exception
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');

        // Create a deal author
        $this->dealUser = $this->createUser('dealUser');

        // Create 5 Ads for User
        $this->storeAds($this->user, 5);

        // Currents Time
        $timeNow = now();

        // Create 5 Deals for each Ad by deal author
        for ($i = 0; $i < self::PERIOD_TOTAL; $i++) {
            $this->arrayPeriod[$i+1] = $timeNow->copy()->addDays($i)
                ->format('Y-m-d H:i:s');

            $this->storeDeals(
                $this->dealUser,
                [
                    'created_at' => $this->arrayPeriod[$i+1],
                ]
            );
        }
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int $count
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init()
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array     $data
     *
     * @return void
     * @throws \Exception
     */
    protected function storeDeals(User $user = null, $data = [])
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $this->deals[] = DealMaker::init($data)
                ->author($user)
                ->status(DealStatusConstants::FINISHED)
                ->create();
        }
    }

    /**
     * Unit Test If User receive valid calculated data
     */
    public function testUserReceiveValidCalculatedData()
    {
        // Get first Ad to view statistic
        $adForStats = array_shift($this->ads);
        
        $adStatisticsContract = app(
            AdStatisticsContract::class,
            [
                'ad'   => $adForStats,
                'from' => new Carbon($this->arrayPeriod[self::PERIOD_START]),
                'to'   => new Carbon($this->arrayPeriod[self::PERIOD_END]),
            ]
        );

        // Test if deals count match
        $this->assertTrue(
            $adStatisticsContract->deals() == (self::PERIOD_END - self::PERIOD_START + 1),
            'Deals number do not match period! (Deals in response: ['
            . $adStatisticsContract->deals()
            . ']; Deals in Period:['
            . (self::PERIOD_END - self::PERIOD_START + 1)
            . '])'
        );

        // Test if crypto turnover match
        $dealRepo = app(DealRepo::class)->filterByAd($adForStats)
            ->filterByCreatedAtFrom(new Carbon($this->arrayPeriod[self::PERIOD_START]))
            ->filterByCreatedAtTo(new Carbon($this->arrayPeriod[self::PERIOD_END]))
            ->take();

        $this->assertTrue(
            $adStatisticsContract->cryptoTurnover() == $dealRepo->sum('crypto_amount'),
            'cryptoTurnover() number do not match sum("crypto_amount")! [cryptoTurnover()='
            . $adStatisticsContract->cryptoTurnover()
            . '] <> [sum("crypto_amount")='
            . $dealRepo->sum('crypto_amount')
            . ']'
        );

        // Test if fiat turnover match
        $this->assertTrue(
            $adStatisticsContract->fiatTurnover() == $dealRepo->sum('fiat_amount'),
            'fiatTurnover() number do not match sum("fiat_amount")! [fiatTurnover()='
            . $adStatisticsContract->fiatTurnover()
            . '] <> [sum("fiat_amount")='
            . $dealRepo->sum('fiat_amount')
            . ']'
        );

        // Test if wighted price match

        $wieghtedPrice = currencyFromCoins($adStatisticsContract->fiatTurnover(), $adForStats->currency)
            / currencyFromCoins($adStatisticsContract->cryptoTurnover(), $adForStats->cryptoCurrency);

        $this->assertTrue(
            $adStatisticsContract->weightedAveragePrice() == (int) $wieghtedPrice,
            'weightedAveragePrice() number do not match average("price")! [weightedAveragePrice()='
            . $adStatisticsContract->weightedAveragePrice()
            . '] <> [average("price")='
            . (int) $dealRepo->average('price')
            . ']'
        );
    }
}
