<?php

namespace Tests\Unit\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackCommissionStorageContract;
use App\Contracts\DealCashback\DealCashbackProcessServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Services\DealCashback\DealCashbackCommission;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class DealCashbackCommissionTest extends TestCase
{
    use DealHelpers, AdHelpers;

    /** @test */
    public function testCanInstantiate()
    {
        /* @var DealCashbackCommissionStorageContract $service */
        $service = app(DealCashbackCommissionStorageContract::class);

        $this->assertInstanceOf(DealCashbackCommissionStorageContract::class, $service);
        $this->assertInstanceOf(DealCashbackCommission::class, $service);
    }

    /** @test */
    public function testProperties()
    {
        $dealCount = env('DEAL_CASHBACK_DEAL_QUANTITY') - 1;
        $crypto = $this->getRandomCrypto();

        // setup
        $userAd = $this->registerUser(['login' => 'userAd']);
        $this->makeAd($userAd, ['is_sale' => true, 'crypto_currency_id' => $crypto]);

        $userDeal = $this->registerUser(['login' => 'userDeal']);
        $this->times($dealCount)->makeDeal($userDeal, $this->lastAd($userAd));

        /* @var DealCashbackProcessServiceContract $service */
        $service = app(DealCashbackProcessServiceContract::class);

        // act
        $this->getDeals($userDeal)->each(function ($deal) use ($service) {
            $service->process($deal);
        });

        /* @var DealCashbackRepoContract $cbRepo */
        $cbRepo = app(DealCashbackRepoContract::class);
        $cashback = $cbRepo->getInProgressOrCreate($userAd, $crypto);

        /* @var DealCashbackCommissionStorageContract $commissionStorage */
        $commissionStorage = app(DealCashbackCommissionStorageContract::class, ['cashback' => $cashback]);

        // assert
        $this->assertEquals(config('app.cashback.deals.commission'), $commissionStorage->percent());

        $commission = (int)ceil(
            $this->getDeals($userDeal)->sum('commission') * config('app.cashback.deals.commission') / 100
        );
        $this->assertEquals($commission, $commissionStorage->amount());

        $this->assertEquals($crypto, $commissionStorage->cryptoCurrency());
        $this->assertEquals($this->getDeals($userDeal)->pluck('id'), $commissionStorage->deals()->pluck('id'));
    }
}
