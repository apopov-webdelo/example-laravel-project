<?php

namespace Tests\Unit\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackDoneServiceContract;
use App\Exceptions\DealCashback\ExistingsTransactionIdCashbackException;
use App\Exceptions\DealCashback\UnexpectedCashbackStatusException;
use App\Facades\Robot;
use App\Models\DealCashback\DealCashback;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Services\DealCashback\DealCashbackDoneService;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class DealCashbackDoneServiceTest extends TestCase
{
    use AdHelpers, DealHelpers, BalanceHelpers;

    protected $stellarTxId;

    public function setUp()
    {
        parent::setUp();
        $this->stellarTxId = 99999;
    }

    /** @test */
    public function testInstantiate()
    {
        /* @var DealCashbackDoneService $handler */
        $handler = app(DealCashbackDoneServiceContract::class);

        $this->assertInstanceOf(DealCashbackDoneService::class, $handler);
    }

    /** @test
     * @throws \Exception
     */
    public function testDoneCashback()
    {
        // setup
        /* @var DealCashback $cashback */
        $cashback = $this->prepareData();

        /* @var DealCashbackDoneService $service */
        $service = app(DealCashbackDoneServiceContract::class);
        $service->done($cashback, $this->stellarTxId);

        $this->assertEquals(DealCashback::STATUS_DONE, $cashback->status);
    }

    /** @test
     * @throws \Exception
     */
    public function testTransactionIdIsSaved()
    {
        // setup
        /* @var DealCashback $cashback */
        $cashback = $this->prepareData();

        // act
        /* @var DealCashbackDoneService $service */
        $service = app(DealCashbackDoneServiceContract::class);
        $service->done($cashback, $this->stellarTxId);

        // assert
        $this->assertEquals($this->stellarTxId, $cashback->transaction_id);
    }

    /**
     * @test
     * @throws \App\Exceptions\DealCashback\DealCashbackException
     */
    public function testDoubleTransactionIsDenied()
    {
        // setup
        /* @var DealCashback $cashback */
        $cashback = $this->prepareData();

        $this->expectException(ExistingsTransactionIdCashbackException::class);

        // act
        /* @var DealCashbackDoneService $service */
        $service = app(DealCashbackDoneServiceContract::class);
        $service->done($cashback, $this->stellarTxId);
        $service->done($cashback, $this->stellarTxId);
    }

    /**
     * @test
     * @throws \App\Exceptions\DealCashback\DealCashbackException
     * @throws \Exception
     */
    public function testDoneStatusIsDenied()
    {
        // setup
        /* @var DealCashback $cashback */
        $cashback = $this->prepareData();
        $cashback->setStatus(DealCashback::STATUS_DONE);

        $this->expectException(UnexpectedCashbackStatusException::class);

        // act
        /* @var DealCashbackDoneService $service */
        $service = app(DealCashbackDoneServiceContract::class);
        $service->done($cashback, $this->stellarTxId);
    }

    /**
     * @return DealCashback
     */
    protected function prepareData()
    {
        $dealCount = env('DEAL_CASHBACK_DEAL_QUANTITY');

        $user = $this->registerUser();

        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create([
            'status'  => DealCashback::STATUS_PENDING,
            'user_id' => $user,
        ]);

        $this->makeAd($user);
        $ad = $this->lastAd($user);
        $this->addBalance(Robot::user(), $ad->cryptoCurrency, CryptoCurrencyConstants::ONE_BTC * 100);

        $userDeal = $this->registerUser();
        $this->times($dealCount)->makeDeal($userDeal, $this->lastAd($user));

        $this->getDeals($userDeal)->each(function ($deal) use ($cashback) {
            $cashback->dealsRelation()->attach($deal);
        });

        return $cashback;
    }
}
