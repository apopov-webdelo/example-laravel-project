<?php

namespace Tests\Unit\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackProcessServiceContract;
use App\Contracts\DealCashback\DealCashbackRepoContract;
use App\Models\DealCashback\DealCashback;
use App\Repositories\DealCashback\DealCashbackRepo;
use App\Services\DealCashback\DealCashbackProcessService;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class DealCashbackProcessServiceTest extends TestCase
{
    use DealHelpers, AdHelpers;

    public function setUp()
    {
        parent::setUp();
        Event::fake();
    }

    /** @test */
    public function testCanInstantiate()
    {
        /* @var DealCashbackProcessServiceContract $service */
        $service = app(DealCashbackProcessServiceContract::class);

        $this->assertInstanceOf(DealCashbackProcessServiceContract::class, $service);
        $this->assertInstanceOf(DealCashbackProcessService::class, $service);
    }

    /** @test */
    public function testCorrectDealsCount()
    {
        // setup
        $dealCount = env('DEAL_CASHBACK_DEAL_QUANTITY') - 1;

        $userAd = $this->registerUser(['login' => 'userAd']);
        $this->makeAd($userAd, ['is_sale' => true]);
        $ad = $this->lastAd($userAd);

        $userDeal = $this->registerUser(['login' => 'userDeal']);
        $this->times($dealCount)->makeDeal($userDeal, $ad);

        /* @var DealCashbackProcessServiceContract $service */
        $service = app(DealCashbackProcessServiceContract::class);

        // act
        $this->getDeals($userDeal)->each(function ($deal) use ($service) {
            $service->process($deal);
        });

        /* @var DealCashbackRepoContract $cbRepo */
        $cbRepo = app(DealCashbackRepoContract::class);
        $cashback1 = $cbRepo->getInProgressOrCreate($userAd, $this->lastDeal($userDeal)->cryptoCurrency);
        $cashback2 = $cbRepo->getInProgressOrCreate($userDeal, $this->lastDeal($userDeal)->cryptoCurrency);

        // assert
        $this->assertCount($dealCount, $cashback1->deals());
        $this->assertCount($dealCount, $cashback2->deals());
    }

    /** @test */
    public function testDealCashbackGetsPendingStatus()
    {
        // setup
        $dealCount = env('DEAL_CASHBACK_DEAL_QUANTITY');

        $userAd = $this->registerUser(['login' => 'userAd']);
        $this->makeAd($userAd, ['is_sale' => true]);
        $ad = $this->lastAd($userAd);

        $userDeal = $this->registerUser(['login' => 'userDeal']);
        $this->times($dealCount)->makeDeal($userDeal, $ad);

        /* @var DealCashbackProcessServiceContract $service */
        $service = app(DealCashbackProcessServiceContract::class);

        // act
        $this->getDeals($userDeal)->each(function ($deal) use ($service) {
            $service->process($deal);
        });

        /* @var DealCashbackRepo|DealCashbackRepoContract $cbRepo */
        $cbRepo = app(DealCashbackRepoContract::class);
        /* @var DealCashback $cashback1 */
        $cashback1 = $cbRepo->filterByRecipient($userAd)->take()->first();
        /* @var DealCashback $cashback2 */
        $cashback2 = $cbRepo->reset()->filterByRecipient($userDeal)->take()->first();

        // assert
        $this->assertCount($dealCount, $cashback1->deals());
        $this->assertEquals(DealCashback::STATUS_PENDING, $cashback1->status);
        $this->assertCount($dealCount, $cashback2->deals());
        $this->assertEquals(DealCashback::STATUS_PENDING, $cashback2->status);
    }
}
