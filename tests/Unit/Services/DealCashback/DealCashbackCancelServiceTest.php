<?php

namespace Tests\Unit\Services\DealCashback;

use App\Contracts\DealCashback\DealCashbackCancelServiceContract;
use App\Exceptions\DealCashback\UnexpectedCashbackStatusException;
use App\Models\DealCashback\DealCashback;
use App\Services\DealCashback\DealCashbackCancelService;
use Tests\TestCase;

class DealCashbackCancelServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /* @var DealCashbackCancelService $service */
        $service = app(DealCashbackCancelServiceContract::class);

        $this->assertInstanceOf(DealCashbackCancelServiceContract::class, $service);
        $this->assertInstanceOf(DealCashbackCancelService::class, $service);
    }

    /** @test
     * @throws \Exception
     */
    public function testCancelsCashback()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_PENDING]);

        /* @var DealCashbackCancelService $service */
        $service = app(DealCashbackCancelServiceContract::class);
        $service->cancel($cashback);

        $cashback = $cashback->fresh();

        $this->assertEquals(DealCashback::STATUS_CANCELED, $cashback->status);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testCancelsCashbackDoubleIsDenied()
    {
        // setup
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_CANCELED]);

        $this->expectException(UnexpectedCashbackStatusException::class);

        // act
        /* @var DealCashbackCancelService $service */
        $service = app(DealCashbackCancelServiceContract::class);
        $service->cancel($cashback);
    }
}
