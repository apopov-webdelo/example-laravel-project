<?php


namespace Tests\Unit\Services\Auth;

use App\Contracts\Services\Auth\TokenServiceContract;
use App\Models\User\User;
use App\Services\Auth\TokenService;
use Tests\TestCase;

class TokenServiceTest extends TestCase
{
    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->registerUser(['login' => 'user']);
    }

    /** @test */
    public function testCanBeResolved()
    {
        /* @var TokenService $service */
        $service = app(TokenServiceContract::class)->setUser($this->user);

        $this->assertInstanceOf(TokenService::class, $service);
    }

    /** @test */
    public function testDefaultTokenIsEmpty()
    {
        $this->actingAs($this->user);

        $this->assertCount(0, $this->user->tokens()->get());
    }

    /** @test
     * @throws \Exception
     */
    public function testTokenCreate()
    {
        /* @var TokenService $service */
        $service = app(TokenServiceContract::class)->setUser($this->user);

        $token = $service->createNew();

        $this->assertNotEmpty($token);
        $this->assertCount(1, $this->user->tokens()->get());
    }

    /** @test
     * @throws \Exception
     */
    public function testTokenRevokeAll()
    {
        /* @var TokenService $service */
        $service = app(TokenServiceContract::class)->setUser($this->user);

        $service->createNew();
        $service->createNew();
        $this->assertCount(2, $this->user->tokens()->get());

        $service->revokeAll();

        $validTokens = $this->user->tokens()->where('revoked', false);
        $this->assertCount(0, $validTokens->get());
    }

    /** @test
     * @throws \Exception
     */
    public function testTokenRevokeForDevice()
    {
        /* @var TokenService $service */
        $service = app(TokenServiceContract::class)->setUser($this->user);

        $service->createNew();

        $service->revokeForDevice();

        $validTokens = $this->user->tokens()->where('revoked', false);
        $this->assertCount(0, $validTokens->get());
    }
}
