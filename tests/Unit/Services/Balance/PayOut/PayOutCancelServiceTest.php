<?php

namespace Tests\Unit\Services\Balance\PayOut;

use App\Contracts\Balance\PayOut\PayOutContract;
use App\Events\Balance\PayOut\PayOutCanceled;
use App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException;
use App\Makers\User\UserMaker;
use App\Models\Balance\PayOut;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Services\Balance\EscrowService;
use App\Services\Balance\PayOut\PayOutCancelService;
use Mockery\MockInterface;
use Tests\TestCase;

class PayOutCancelServiceTest extends TestCase
{
    const PAYOUT_AMOUNT = 12500;

    const PAYOUT_COMMISSION = 10;

    /**
     * Service for testing
     *
     * @var PayOutCancelService
     */
    private $cancelService;

    /**
     * @var CryptoCurrencyRepo
     */
    private $cryptoCurrencyRepo;

    /**
     * @var EscrowService|MockInterface
     */
    private $escrowService;

    /**
     * @var User
     */
    private $user;

    /**
     * @var CryptoCurrency
     */
    private $crypto;

    protected function setUp()
    {
        parent::setUp();
        $this->escrowService = $this->getMockedEscrowService();
        $this->cancelService = app(PayOutCancelService::class, ['escrowService' => $this->escrowService]);
        $this->cryptoCurrencyRepo = app(CryptoCurrencyRepo::class);
        $this->user = UserMaker::init()->create();
        $this->crypto = $this->cryptoCurrencyRepo->getByCode(CryptoCurrencyConstants::BTC_CODE);
    }

    /**
     * Retrieve mocked EscrowService
     *
     * @return EscrowService|MockInterface
     */
    protected function getMockedEscrowService()
    {
        $mock = \Mockery::mock(EscrowService::class);
        return $mock;
    }

    /**
     * Check is service work correctly for PayOut with valid statuses
     *
     * @throws \App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException
     * @throws \Exception
     */
    public function testCancelWithValidStatus()
    {
        $this->expectsEvents(PayOutCanceled::class);
        $payOut = $this->getMockedPayOut(PayOut::STATUS_PENDING);

        $payOut->shouldReceive('setCanceledStatus')
               ->once()
               ->andReturnTrue();

        $this->escrowService->shouldReceive('decrease')
                            ->withArgs([
                                self::PAYOUT_AMOUNT + self::PAYOUT_COMMISSION,
                                $this->user,
                                $this->crypto
                            ])
                            ->once();

        $result = $this->cancelService->cancel($payOut);

        $this->assertInstanceOf(PayOutContract::class, $result);
    }

    /**
     * Retrieve mocked PayOut with needed status
     *
     * @param string $status
     *
     * @return PayOutContract|MockInterface
     */
    protected function getMockedPayOut(string $status)
    {
        $mock = \Mockery::mock(PayOutContract::class);
        $mock->allows([
            'due'               => self::PAYOUT_AMOUNT + self::PAYOUT_COMMISSION,
            'amount'            => self::PAYOUT_AMOUNT,
            'commission'        => self::PAYOUT_COMMISSION,
            'user'              => $this->user,
            'cryptoCurrency'    => $this->crypto,
            'status'            => $status,
        ]);
        return $mock;
    }

    /**
     * PayOut's invalid statuses
     *
     * @return array
     */
    public function invalidStatusesDataProvider()
    {
        return [
            [PayOut::STATUS_CANCELED],
            [PayOut::STATUS_PROCESSED],
        ];
    }

    /**
     * Check is service throw exception for PayOut with invalid statuses
     *
     * @dataProvider invalidStatusesDataProvider
     *
     * @param string $status
     *
     * @throws \App\Exceptions\Balance\PayOut\UnexpectedStatusPayOutException
     * @throws \Exception
     */
    public function testCancelWithInvalidStatus(string $status)
    {
        $this->doesntExpectEvents(PayOutCanceled::class);

        $payOut = $this->getMockedPayOut($status);
        $payOut->shouldNotReceive('setCanceledStatus');

        $this->escrowService->shouldNotReceive('decrease');

        $this->expectException(UnexpectedStatusPayOutException::class);

        $this->cancelService->cancel($payOut);
    }
}
