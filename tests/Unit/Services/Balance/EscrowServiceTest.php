<?php

namespace Tests\Unit\Services\Balance;

use App\Exceptions\Balance\BalanceAmountException;
use App\Models\Balance\EscrowState;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use App\Services\Balance\EscrowService;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\BalanceHelpers;
use Tests\TestCase;

class EscrowServiceTest extends TestCase
{
    use BalanceHelpers;

    /**
     * @var EscrowService
     */
    private $service;

    /**
     * @var User
     */
    private $user;

    protected function setUp()
    {
        parent::setUp();

        Event::fake();

        $this->user = $this->registerUser();
        $this->actingAs($this->user);
        $this->service = app(EscrowService::class);
    }

    /** @test */
    public function testInstantiate()
    {
        $this->assertInstanceOf(EscrowService::class, $this->service);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testCanNotIncreaseOnZeroBalance()
    {
        $this->expectException(BalanceAmountException::class);

        $this->service->increase(CryptoCurrencyConstants::ONE_BTC, $this->user, $this->getCryptoByCode('btc'));
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testCanNotDecreaseOnZeroEscrow()
    {
        $this->expectException(\Exception::class);

        $this->service->decrease(CryptoCurrencyConstants::ONE_BTC, $this->user, $this->getCryptoByCode('btc'));
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testEscrowStateSaved()
    {
        $crypto = $this->getCryptoByCode('btc');
        $oneBtc = CryptoCurrencyConstants::ONE_BTC;
        $partBtc = round(CryptoCurrencyConstants::HALF_BTC / 3);
        $this->addBalance($this->user, $crypto, $oneBtc);

        // act increase
        $this->service->increase($partBtc, $this->user, $crypto);

        // assert
        /* @var EscrowState $escrowState */
        $escrowState = EscrowState::query()
                                  ->where('balance_id', $this->user->getBalance($crypto)->id)
                                  ->first();

        $this->assertEquals(0, $escrowState->escrow_before);
        $this->assertEquals($partBtc, $escrowState->escrow);
        $this->assertEquals($partBtc, $escrowState->escrow_after);
        $this->assertEquals($oneBtc, $escrowState->balance_before);
        $this->assertEquals($oneBtc - $partBtc, $escrowState->balance_after);
        $this->assertNotNull($escrowState->created_at);
        $this->assertNotNull($escrowState->updated_at);

        // act decrease
        $this->service->decrease($partBtc, $this->user, $crypto);

        // assert
        /* @var EscrowState $escrowState */
        $escrowState = EscrowState::query()
                                  ->where('balance_id', $this->user->getBalance($crypto)->id)
                                  ->latest('id')
                                  ->first();

        $this->assertEquals($partBtc, $escrowState->escrow_before);
        $this->assertEquals($partBtc, $escrowState->escrow);
        $this->assertEquals(0, $escrowState->escrow_after);
        $this->assertEquals($oneBtc - $partBtc, $escrowState->balance_before);
        $this->assertEquals($oneBtc, $escrowState->balance_after);
        $this->assertNotNull($escrowState->created_at);
        $this->assertNotNull($escrowState->updated_at);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testEscrowIncrease()
    {
        $crypto = $this->getCryptoByCode('btc');
        $oneBtc = CryptoCurrencyConstants::ONE_BTC;
        $partBtc = round(CryptoCurrencyConstants::HALF_BTC / 3);
        $this->addBalance($this->user, $crypto, $oneBtc);

        // act increase
        $this->service->increase($partBtc, $this->user, $crypto);

        // assert
        $balance = $this->user->getBalance($crypto);
        $this->assertEquals($partBtc, $balance->escrow);
        $this->assertEquals($oneBtc - $partBtc, $balance->amount);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testEscrowDecrese()
    {
        $crypto = $this->getCryptoByCode('btc');
        $oneBtc = CryptoCurrencyConstants::ONE_BTC;
        $halfBtc = CryptoCurrencyConstants::HALF_BTC;
        $partBtc = round(CryptoCurrencyConstants::HALF_BTC / 3);
        $this->addBalance($this->user, $crypto, $oneBtc);

        // act increase and decrease
        $this->service->increase($halfBtc, $this->user, $crypto); // 500
        $this->service->decrease($partBtc, $this->user, $crypto); // 500-166

        // assert
        $balance = $this->user->getBalance($crypto);
        $this->assertEquals($halfBtc - $partBtc, $balance->escrow);
        $this->assertEquals($oneBtc - ($halfBtc - $partBtc), $balance->amount);
    }
}
