<?php

namespace Tests\Unit\Services\Balance\PayIn;

use App\Contracts\Balance\PayIn\PayInTransactionContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\PayInTransactionRepoContract;
use App\Events\Balance\PayIn\PayInTransactionStored;
use App\Exceptions\Balance\PayIn\AmountPayInException;
use App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException;
use App\Models\Balance\PayIn\PayInRegisterServiceContract;
use App\Models\Directory\CryptoCurrencyConstants as Crypto;
use App\Models\User\User;
use App\Services\Balance\PayIn\PayInRegisterService;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class PayInRegisterServiceTest extends TestCase
{
    /**
     * Test that is binding for testing contract
     *
     * @return void
     */
    public function testCanInstantiateService()
    {
        $service = app(PayInRegisterServiceContract::class);

        $this->assertInstanceOf(PayInRegisterServiceContract::class, $service);
    }

    /**
     * Provide valid date for test
     *
     * @return array
     */
    public function validTransactionDataProvider()
    {
        return [
            [
                $this->getRandomId(),
                $this->getMockedUser(),
                Crypto::ONE_BTC,
                $this->getMockedCrypto(),
            ],
        ];
    }

    /**
     * Generate random transaction ID
     *
     * @return int
     */
    protected function getRandomId()
    {
        return rand(1000, 9000);
    }

    /**
     * Retrive mocked User object
     *
     * @return User|\Mockery\MockInterface
     */
    protected function getMockedUser()
    {
        return \Mockery::mock(User::class);
    }

    /**
     * Test that service works correctly with valid data
     *
     * @param string                 $transactionId
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @throws \App\Exceptions\Balance\PayIn\AmountPayInException
     * @throws \App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException
     *
     * @dataProvider validTransactionDataProvider
     */
    public function testPayInRegisterWithValidData(
        string $transactionId,
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency
    ) {
        Event::fake();
        /** @var PayInRegisterService $service */
        $service = app(PayInRegisterService::class, ['payInTransactionRepo' => $this->getMockedRepo($transactionId)]);
        $result = $service->register($transactionId, $user, $amount, $cryptoCurrency);

        $this->assertInstanceOf(PayInTransactionContract::class, $result);
        Event::assertDispatched(PayInTransactionStored::class);
    }

    /**
     * Retrieve mocked PayInTransactionRepoContract
     *
     * @param      $transactionId
     * @param bool $isExists
     *
     * @return PayInTransactionRepoContract|\Mockery\MockInterface
     */
    protected function getMockedRepo($transactionId, $isExists = false)
    {
         $mock = \Mockery::mock(PayInTransactionRepoContract::class);
         $mock->shouldReceive('isExists')->with($transactionId)->andReturn($isExists);
         $mock->shouldReceive('store');
         return $mock;
    }

    /**
     * Retrieve mocked CryptoCurrencyContract
     *
     * @return CryptoCurrencyContract|\Mockery\MockInterface
     */
    protected function getMockedCrypto()
    {
        $mock = \Mockery::mock(CryptoCurrencyContract::class);
        return $mock;
    }

    /**
     * Data provider with invalid data
     *
     * @return array
     */
    public function invalidTransactionDataProvider()
    {
        return [
            [
                $this->getRandomId(),
                $this->getMockedUser(),
                Crypto::ONE_BTC*(-1),
                $this->getMockedCrypto(),
            ],
            [
                $this->getRandomId(),
                $this->getMockedUser(),
                0,
                $this->getMockedCrypto(),
            ],
        ];
    }

    /**
     * Test that service throws valid exception if amount is invalid
     *
     * @param string                 $transactionId
     * @param User                   $user
     * @param int                    $amount
     * @param CryptoCurrencyContract $cryptoCurrency
     *
     * @throws \App\Exceptions\Balance\PayIn\AmountPayInException
     * @throws \App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException
     *
     * @dataProvider invalidTransactionDataProvider
     */
    public function testPayInRegisterWithInvalidData(
        string $transactionId,
        User $user,
        int $amount,
        CryptoCurrencyContract $cryptoCurrency
    ) {
        Event::fake();

        $this->expectException(AmountPayInException::class);

        /** @var PayInRegisterService $service */
        $service = app(
            PayInRegisterService::class,
            ['payInTransactionRepo' => $this->getMockedRepo($transactionId)]
        );
        $service->register($transactionId, $user, $amount, $cryptoCurrency);

        Event::assertNotDispatched(PayInTransactionStored::class);
    }

    /**
     * Test that service throws valid exception if transaction with same ID is exists
     *
     * @throws AmountPayInException
     * @throws \App\Exceptions\Balance\PayIn\ExistingsTransactionIdPayInException
     */
    public function testPayInRegisterWithExistsTransactionId()
    {
        Event::fake();
        $transactionId = $this->getRandomId();

        $this->expectException(ExistingsTransactionIdPayInException::class);

        /** @var PayInRegisterService $service */
        $service = app(
            PayInRegisterService::class,
            ['payInTransactionRepo' => $this->getMockedRepo($transactionId, true)]
        );
        $service->register(
            $transactionId,
            $this->getMockedUser(),
            Crypto::ONE_BTC,
            $this->getMockedCrypto()
        );

        Event::assertNotDispatched(PayInTransactionStored::class);
    }
}
