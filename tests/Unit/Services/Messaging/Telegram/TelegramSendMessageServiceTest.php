<?php

namespace Tests\Unit\Services\Messaging\Telegram;

use App\Services\Messaging\Telegram\TelegramSendMessageService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Monolog\Logger;
use Tests\TestCase;

class TelegramSendMessageServiceTest extends TestCase
{
    protected function getTestData()
    {
        return [
            config('logging.channels.telegram_emergency'),
            [
                'message'    => 'message body уес',
                'context'    => ['param1' => 'value1', 'param2' => 'value2'],
                'channel'    => 'Test',
                'level_name' => 'INFO',
                'level'      => Logger::INFO,
            ],
        ];
    }

    /** @test */
    public function testInstantiate()
    {
        /** @var TelegramSendMessageService $service */
        list($config, $record) = $this->getTestData();
        $service = app(TelegramSendMessageService::class, ['config' => $config]);

        $this->assertInstanceOf(TelegramSendMessageService::class, $service);
    }

    /**
     * @test
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testSendsMessage()
    {
        // mock guzzle
        $this->app->bind(ClientInterface::class, function () {
            $mock = new MockHandler([
                new Response(200, [], json_encode(['ok' => true])),
            ]);

            $handler = HandlerStack::create($mock);

            return new Client(['handler' => $handler]);
        });

        /** @var TelegramSendMessageService $service */
        list($config, $record) = $this->getTestData();
        $service = app(TelegramSendMessageService::class, ['config' => $config]);

        $result = $service->send($record);

        // assert
        $this->assertTrue($result);
    }
}
