<?php

namespace Tests\Unit\Services\User\Statistics;

use App\Contracts\AuthenticatedContract;
use App\Facades\Robot;
use App\Makers\AdMaker;
use App\Models\Ad\AdStatus;
use App\Models\Ad\AdStatusConstants;
use App\Services\User\Statistics\UpdateReviewsCoefficientService;
use App\Services\User\Statistics\UpdateReviewsCoefficientStorage;
use Tests\TestCase;

class UpdateReviewsCoefficientServiceUnitTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app->bind(AuthenticatedContract::class, function () {
            return Robot::user();
        });
    }

    /**
     * @param       $class
     * @param array $methods
     *
     * @return \Mockery\Expectation|\Mockery\ExpectationInterface|\Mockery\HigherOrderMessage|\Mockery\Mock
     */
    protected function mockClass($class, $methods = [])
    {
        $mock = \Mockery::mock($class)->makePartial();

        return $mock->allows($methods);
    }

    /** @test */
    public function testReviewCoefficientSubZeroPercent()
    {
        $ad       = AdMaker::init()->sale(true)->status(AdStatus::find(AdStatusConstants::ACTIVE))->create();
        $author   = $ad->author;
        $storage  = $this->mockClass(UpdateReviewsCoefficientStorage::class, [
            'dealsCount'    => 20,
            'positiveCount' => 4,
            'negativeCount' => 1,
            'neutralCount'  => 2,
            'model'         => $author->statistic
        ]);
        $service  = app(UpdateReviewsCoefficientService::class, ['storage' => $storage]);
        $expected = 0;

        // 0.2 - 0.05 - 1 = -0.85 | By service logic result is 0.
        $service->update();

        $this->assertEquals($expected, $author->statistic->reviews_coefficient);
    }

    /** @test */
    public function testReviewCoefficientPercent()
    {
        $ad      = AdMaker::init()->sale()->status(AdStatus::find(AdStatusConstants::ACTIVE))->create();
        $author  = $ad->author;
        $storage = $this->mockClass(UpdateReviewsCoefficientStorage::class, [
            'dealsCount'    => 20,
            'positiveCount' => 50,
            'negativeCount' => 1,
            'neutralCount'  => 1,
            'model'         => $author->statistic
        ]);
        $service  = app(UpdateReviewsCoefficientService::class, ['storage' => $storage]);
        $expected = 195000000;

        // 1.1 - 0.05 - 1 = 0.05
        $service->update();

        $this->assertEquals($expected, $author->statistic->reviews_coefficient);
    }
}
