<?php

namespace Tests\Unit\Services\User\Statistics;

use App\Contracts\Services\User\Statistics\UserStatisticsContract;
use App\Models\Deal\DealStatusConstants;
use App\Services\User\Statistics\UserStatisticsService;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class UserStatisticsServiceUnitTest extends TestCase
{
    use AdHelpers, DealHelpers;

    /**
     * @var UserStatisticsService
     */
    private $stats;

    public function setUp()
    {
        parent::setUp();

        $this->stats = app(UserStatisticsContract::class);
    }

    /** @test */
    public function testInstanceCorrect()
    {
        $this->assertInstanceOf(UserStatisticsService::class, $this->stats);
    }

    /** @test */
    public function testCancelCountAndPercent()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = config('app.deal.cancel_min_limit') + 1;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => true,
        ]);

        $ads = $this->allAds($userAd);
        $ads->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()->attach(DealStatusConstants::CANCELED);
        });

        $this->stats->setUser($userAd)->update();
        $this->stats->setUser($userDeal)->update();

        $this->assertEquals(0, $userAd->statistic->deals_canceled_count);
        $this->assertEquals($count, $userDeal->statistic->deals_canceled_count);
        $this->assertEquals(0, $userAd->statistic->deals_cancellation_percent);
        $this->assertEquals(100, $userDeal->statistic->deals_cancellation_percent);
    }

    /** @test */
    public function testCancelCountAndPercentWhenBuying()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = config('app.deal.cancel_min_limit') + 1;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => false,
        ]);

        $ads = $this->allAds($userAd);
        $ads->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()->attach(DealStatusConstants::CANCELED);
        });

        $this->stats->setUser($userAd)->update();
        $this->stats->setUser($userDeal)->update();

        $this->assertEquals($count, $userAd->statistic->deals_canceled_count);
        $this->assertEquals(0, $userDeal->statistic->deals_canceled_count);
        $this->assertEquals(100, $userAd->statistic->deals_cancellation_percent);
        $this->assertEquals(0, $userDeal->statistic->deals_cancellation_percent);
    }

    /** @test */
    public function testFinishCount()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = config('app.deal.cancel_min_limit') + 1;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => true,
        ]);

        $ads = $this->allAds($userAd);
        $ads->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()->attach(DealStatusConstants::FINISHED);
        });

        $this->stats->setUser($userDeal)->update();
        $this->stats->setUser($userAd)->update();

        $this->assertEquals($count, $userAd->statistic->deals_finished_count);
        $this->assertEquals($count, $userDeal->statistic->deals_finished_count);
        $this->assertEquals(0, $userAd->statistic->deals_cancellation_percent);
        $this->assertEquals(0, $userDeal->statistic->deals_cancellation_percent);
    }

    /** @test */
    public function testPaidCount()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = config('app.deal.cancel_min_limit') + 1;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => true,
        ]);

        $ads = $this->allAds($userAd);
        $ads->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()->attach(DealStatusConstants::PAID);
        });

        $this->stats->setUser($userAd)->update();
        $this->stats->setUser($userDeal)->update();

        $this->assertEquals($count, $userAd->statistic->deals_paid_count);
        $this->assertEquals($count, $userDeal->statistic->deals_paid_count);
        $this->assertEquals(0, $userAd->statistic->deals_cancellation_percent);
        $this->assertEquals(0, $userDeal->statistic->deals_cancellation_percent);
    }

    /** @test */
    public function testDisputeCount()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = config('app.deal.cancel_min_limit') + 1;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => true,
        ]);

        $ads = $this->allAds($userAd);
        $ads->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()->attach(DealStatusConstants::IN_DISPUTE);
        });

        $this->stats->setUser($userAd)->update();
        $this->stats->setUser($userDeal)->update();

        $this->assertEquals($count, $userAd->statistic->deals_disputed_count);
        $this->assertEquals($count, $userDeal->statistic->deals_disputed_count);
        $this->assertEquals(0, $userAd->statistic->deals_cancellation_percent);
        $this->assertEquals(0, $userDeal->statistic->deals_cancellation_percent);
    }

    /** @test */
    public function testTotalCountWithoutCanceled()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = 10;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => true,
        ]);

        $ads = $this->allAds($userAd);
        $ads->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()
                 ->attach(array_random([
                     DealStatusConstants::IN_DISPUTE,
                     DealStatusConstants::FINISHED,
                     DealStatusConstants::PAID,
                 ]));
        });

        $this->stats->setUser($userAd)->update();
        $this->stats->setUser($userDeal)->update();

        $this->assertEquals($count, $userAd->statistic->deals_count);
        $this->assertEquals($count, $userDeal->statistic->deals_count);
    }

    /** @test */
    public function testTotalCountWithCancels()
    {
        $userAd = $this->registerUser(['testerAd']);
        $userDeal = $this->registerUser(['testerDeal']);
        $count = 10;

        $this->times($count)->makeAd($userAd, [
            'is_sale' => true,
        ]);

        $chunks = $this->allAds($userAd)->chunk($count / 2);

        $chunks->first()->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()
                 ->attach(DealStatusConstants::FINISHED);
        });

        $chunks->last()->map(function ($ad) use ($userDeal) {
            $this->makeDeal($userDeal, $ad);
            $deal = $this->lastDeal($userDeal);
            $deal->statuses()
                 ->attach(DealStatusConstants::CANCELED);
        });

        $this->stats->setUser($userAd)->update();
        $this->stats->setUser($userDeal)->update();

        $this->assertEquals($count / 2, $userAd->statistic->deals_count);
        $this->assertEquals($count, $userDeal->statistic->deals_count);

        $this->assertEquals(0, $userAd->statistic->deals_canceled_count);
        $this->assertEquals($count / 2, $userDeal->statistic->deals_canceled_count);
    }
}
