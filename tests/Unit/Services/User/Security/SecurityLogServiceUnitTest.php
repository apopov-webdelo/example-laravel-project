<?php

namespace Tests\Unit\Services\User\Security;

use App\Contracts\AuthenticatedContract;
use App\Services\User\Security\SecurityLogService;
use Tests\TestCase;

class SecurityLogServiceUnitTest extends TestCase
{
    /**
     * @var AuthenticatedContract
     */
    protected $user;

    /**
     * @var SecurityLogService
     */
    protected $service;

    /**
     * @param AuthenticatedContract $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getSecurityLogs(AuthenticatedContract $user)
    {
        return $user->securityLogs()->get();
    }

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->registerUser(['login' => 'tester']);
        $this->actingAs($this->user);

        $this->service = app(SecurityLogService::class);
    }

    /** @test */
    public function testCanBeResolved()
    {
        $this->assertInstanceOf(SecurityLogService::class, $this->service);
    }

    /** @test */
    public function testAddSimpleRecord()
    {
        $this->service->addRecord('1', '2', '3');

        $logs = $this->getSecurityLogs($this->user);

        $this->assertCount(1, $logs);
        $this->assertEquals($logs->first()->sessionable_id, $this->user->getId());
        $this->assertEquals($logs->first()->sessionable_type, get_class($this->user));
    }
}
