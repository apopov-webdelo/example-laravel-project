<?php

namespace Tests\Unit\Services\User;

use App\Contracts\Services\User\UserChangesServiceContract;
use App\Models\User\User;
use App\Services\User\SocialRegistrationService;
use App\Services\User\UserChangesService;
use App\Utils\User\UserChangesState;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class UserChangesServiceTest extends TestCase
{
    /**
     * @var UserChangesServiceContract
     */
    private $service;

    /**
     * @var SocialRegistrationService
     */
    private $regService;

    /**
     * @var User
     */
    private $user;

    public function setUp()
    {
        parent::setUp();

        Event::fake();

        $this->regService = app(SocialRegistrationService::class);

        $this->user = $this->regService->store(['login' => 'fb-noname']);
        $this->actingAs($this->user);

        // The tested service
        $this->service = app(UserChangesServiceContract::class);
    }

    /** @test */
    public function testCanInstantiate()
    {
        $this->assertInstanceOf(UserChangesService::class, $this->service);
        $this->assertInstanceOf(UserChangesServiceContract::class, $this->service);
    }

    /** @test */
    public function testCanChangeUserEmail()
    {
        $result = $this->service->emailChange('test@test.com');

        /** @var UserChangesState $changesState */
        $changesState = app(UserChangesState::class, ['state' => $this->user->changesAvailable]);

        $this->assertTrue($result);
        $this->assertEquals('test@test.com', $this->user->email);
        $this->assertEquals(false, $changesState->canSetEmail());
    }

    /** @test */
    public function testCanNotChangeUserEmailTwice()
    {
        $result = $this->service->emailChange('test@test.com');
        $result_bad = $this->service->emailChange('badtest@test.com');

        $this->assertTrue($result);
        $this->assertFalse($result_bad);
        $this->assertEquals('test@test.com', $this->user->email);
    }

    /** @test */
    public function testCanChangeUserLogin()
    {
        $result = $this->service->loginChange('new-login');

        /** @var UserChangesState $changesState */
        $changesState = app(UserChangesState::class, ['state' => $this->user->changesAvailable]);

        $this->assertTrue($result);
        $this->assertEquals('new-login', $this->user->login);
        $this->assertEquals(false, $changesState->canSetLogin());
    }

    /** @test */
    public function testCanNotChangeUserLoginTwice()
    {
        $result = $this->service->loginChange('new-login');
        $result_bad = $this->service->loginChange('bad-login');

        $this->assertTrue($result);
        $this->assertFalse($result_bad);
        $this->assertEquals('new-login', $this->user->login);
    }
}
