<?php

namespace Tests\Unit\Services\User;

use App\Models\User\User;
use App\Models\User\UserChanges;
use App\Services\User\SocialRegistrationService;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class SocialRegistrationServiceTest extends TestCase
{
    /**
     * @var SocialRegistrationService
     */
    private $service;

    public function setUp()
    {
        parent::setUp();

        Event::fake();

        $this->service = app(SocialRegistrationService::class);
    }

    /** @test */
    public function testCanInstantiate()
    {
        $this->assertInstanceOf(SocialRegistrationService::class, $this->service);
    }
    
    /** @test */
    public function testCanRegisterWithService()
    {
        $user = $this->service->store(['login' => 'fb-noname']);

        $this->assertInstanceOf(User::class, $user);
    }

    /** @test */
    public function testUserChangerepoRecordIsAdded()
    {
        $user = $this->service->store(['login' => 'fb-noname']);

        /* @var UserChanges $record */
        $record = UserChanges::where('user_id', $user->id)->get()->first();

        $this->assertEquals(null, $record->login_changed_at);
        $this->assertEquals(null, $record->email_changed_at);
        $this->assertEquals($user->id, $record->user_id);
    }
}
