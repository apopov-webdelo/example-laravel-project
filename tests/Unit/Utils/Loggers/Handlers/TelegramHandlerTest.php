<?php

namespace Tests\Unit\Utils\Loggers\Handlers;

use App\Jobs\Utils\TelegramSendMessageJob;
use App\Utils\Loggers\Handlers\TelegramHandler;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class TelegramHandlerTest extends TestCase
{
    /** @test */
    public function testSendJobIsDispatched()
    {
        Queue::fake();

        $config = config('logging.channels.telegram_deals');

        $record = [
            'message'    => 'message body',
            'context'    => ['some' => 'data', 'more' => 'data etc'],
            'channel'    => 'Deals',
            'level_name' => 'INFO',
            'level'      => 'info',
        ];

        /** @var TelegramHandler $handler */
        $handler = app(TelegramHandler::class, ['config' => $config]);
        $handler->write($record);

        Queue::assertPushed(
            TelegramSendMessageJob::class,
            function (TelegramSendMessageJob $job) use ($record, $config) {
                return $job->record === $record && $job->config === $config;
            }
        );
    }
}
