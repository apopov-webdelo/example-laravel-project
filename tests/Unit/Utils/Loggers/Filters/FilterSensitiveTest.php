<?php

namespace Tests\Unit\Utils\Loggers\Filters;

use App\Utils\Loggers\FilterSensitive;
use Tests\TestCase;

class FilterSensitiveTest extends TestCase
{
    /**
     * @var FilterSensitive
     */
    private $service;

    protected function setUp()
    {
        parent::setUp();
        $this->service = app(FilterSensitive::class);
    }

    /** @test */
    public function testInstantiate()
    {
        $this->assertInstanceOf(FilterSensitive::class, $this->service);
    }

    /** @test */
    public function testHideSensitiveData()
    {
        $data = [
            'public_key'  => '123',
            'private_key' => '321',
            'valid_data'  => 'XXX',
        ];

        $result = $this->service->filter($data);

        $this->assertEquals([
            'public_key'  => FilterSensitive::STUB,
            'private_key' => FilterSensitive::STUB,
            'valid_data'  => 'XXX',
        ], $result);
    }
}
