<?php

namespace Tests\Unit\Utils\User;

use App\Services\User\SocialRegistrationService;
use App\Utils\User\UserChangesState;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class UserChangesStateTest extends TestCase
{
    /**
     * @var SocialRegistrationService
     */
    private $service;

    public function setUp()
    {
        parent::setUp();

        Event::fake();

        $this->service = app(SocialRegistrationService::class);
    }

    /** @test */
    public function testDefaultStateAfterRegistration()
    {
        $user = $this->service->store(['login' => 'fb-noname']);
        $state = app(UserChangesState::class, ['state' => $user->changesAvailable]);

        $this->assertEquals(true, $state->isOAuthRegistration());
        $this->assertEquals(true, $state->canSetEmail());
        $this->assertEquals(true, $state->canSetLogin());
    }

    /** @test */
    public function testNotSocialUserHasFalseStates()
    {
        $user = $this->registerUser(['login' => 'just-user']);

        $state = app(UserChangesState::class, ['state' => $user->changesAvailable]);

        $this->assertEquals(false, $state->isOAuthRegistration());
        $this->assertEquals(false, $state->canSetEmail());
        $this->assertEquals(false, $state->canSetLogin());
    }
}
