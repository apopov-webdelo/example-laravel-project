<?php


namespace Tests\Unit\Traits\Bcrypt;

use App\Models\Ad\Ad;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use Tests\TestCase;

class HasModelBcKeysTest extends TestCase
{
    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->registerUser(['login' => 'user']);
    }

    /**
     * @param User $user
     *
     * @return Ad
     */
    protected function getFirstAd(User $user)
    {
        /* @var AdRepo $repo */
        $repo = app(AdRepo::class);

        return $repo->filterByAuthor($user)->take()->get()->first();
    }

    /**
     * @param User $user
     *
     * @return Ad
     */
    protected function getLastAd(User $user)
    {
        /* @var AdRepo $repo */
        $repo = app(AdRepo::class);

        return $repo->filterByAuthor($user)->take()->get()->last();
    }

    /**
     * @return string
     */
    protected function getRandomKey()
    {
        return str_random(64);
    }

    /**
     * @return string
     */
    protected function getRandomAlias()
    {
        return str_random(20);
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return class_basename($this);
    }

    /** @test */
    public function testHasNoBcKeysBydefault()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $this->assertCount(0, $ad->keys()->get());
    }

    /** @test */
    public function testAddBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getAlias());

        $this->assertCount(1, $ad->keys()->get());
    }

    /** @test */
    public function testGetBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $key = $this->getRandomKey();
        $ad->saveBcKey($key, $this->getAlias());

        $this->assertEquals($key, $ad->getBcKey($this->getAlias()));
    }

    /** @test */
    public function testAddManyBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getRandomAlias());
        $ad->saveBcKey($this->getRandomKey(), $this->getRandomAlias());
        $ad->saveBcKey($this->getRandomKey(), $this->getRandomAlias());

        $this->assertCount(3, $ad->keys()->get());
    }

    /** @test */
    public function testUpdateBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getAlias());
        $ad->saveBcKey('123', $this->getAlias());

        $this->assertCount(1, $ad->keys()->get());
        $this->assertEquals('123', $ad->getBcKey($this->getAlias()));
    }

    /** @test */
    public function testRemoveBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getAlias());

        $this->assertCount(1, $ad->keys()->get());

        $ad->removeBcKey($this->getAlias());

        $this->assertCount(0, $ad->keys()->get());
    }

    /** @test */
    public function testExistsBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getAlias());

        $this->assertTrue($ad->isBcKeyExists($this->getAlias()));
    }

    /** @test */
    public function testNotExistsBcKeys()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $this->assertFalse($ad->isBcKeyExists($this->getAlias()));
    }

    /** @test */
    public function testExistsBcKeysAfterRemove()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getAlias());
        $ad->removeBcKey($this->getAlias());

        $this->assertFalse($ad->isBcKeyExists($this->getAlias()));
    }

    /** @test */
    public function testUpdateBcKeysAfterRemove()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveBcKey($this->getRandomKey(), $this->getAlias());
        $ad->removeBcKey($this->getAlias());
        $ad->saveBcKey('123', $this->getAlias());

        $this->assertFalse($ad->isBcKeyExists($this->getAlias()));
        $this->assertCount(0, $ad->keys()->get());
    }

    /** @test */
    public function testBcKeysMultiple()
    {
        $this->times(6)->makeAd($this->user);
        $adFirst = $this->getFirstAd($this->user);
        $adLast = $this->getLastAd($this->user);

        $adFirst->saveBcKey($this->getRandomKey(), $this->getAlias());
        $adLast->saveBcKey($this->getRandomKey(), $this->getRandomAlias());

        $this->assertCount(1, $adFirst->keys()->get());
        $this->assertCount(1, $adLast->keys()->get());
    }
}
