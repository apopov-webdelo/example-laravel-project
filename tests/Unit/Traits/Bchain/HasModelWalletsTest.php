<?php


namespace Tests\Unit\Traits\Bchain;

use App\Models\Ad\Ad;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use Tests\TestCase;

class HasModelWalletsTest extends TestCase
{
    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->registerUser(['login' => 'user']);
    }

    /**
     * @param User $user
     *
     * @return Ad
     */
    protected function getFirstAd(User $user)
    {
        /* @var AdRepo $repo */
        $repo = app(AdRepo::class);

        return $repo->filterByAuthor($user)->take()->get()->first();
    }

    /**
     * @param User $user
     *
     * @return Ad
     */
    protected function getLastAd(User $user)
    {
        /* @var AdRepo $repo */
        $repo = app(AdRepo::class);

        return $repo->filterByAuthor($user)->take()->get()->last();
    }

    /**
     * @return string
     */
    protected function getRandomWallet()
    {
        return str_random(64);
    }

    /**
     * @return string
     */
    protected function getRandomAlias()
    {
        return str_random(20);
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return class_basename($this);
    }

    /** @test */
    public function testHasNoWalletBydefault()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $this->assertCount(0, $ad->wallets()->get());
    }

    /** @test */
    public function testAddWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getAlias());

        $this->assertCount(1, $ad->wallets()->get());
    }

    /** @test */
    public function testGetWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $key = $this->getRandomWallet();
        $ad->saveWallet($key, $this->getAlias());

        $this->assertEquals($key, $ad->getWallet($this->getAlias()));
    }

    /** @test */
    public function testAddManyWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getRandomAlias());
        $ad->saveWallet($this->getRandomWallet(), $this->getRandomAlias());
        $ad->saveWallet($this->getRandomWallet(), $this->getRandomAlias());

        $this->assertCount(3, $ad->wallets()->get());
    }

    /** @test */
    public function testUpdateWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getAlias());
        $ad->saveWallet('123', $this->getAlias());

        $this->assertCount(1, $ad->wallets()->get());
        $this->assertEquals('123', $ad->getWallet($this->getAlias()));
    }

    /** @test */
    public function testRemoveWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getAlias());

        $this->assertCount(1, $ad->wallets()->get());

        $ad->removeWallet($this->getAlias());

        $this->assertCount(0, $ad->wallets()->get());
    }

    /** @test */
    public function testExistsWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getAlias());

        $this->assertTrue($ad->isWalletExists($this->getAlias()));
    }

    /** @test */
    public function testNotExistsWallet()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $this->assertFalse($ad->isWalletExists($this->getAlias()));
    }

    /** @test */
    public function testExistsWalletAfterRemove()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getAlias());
        $ad->removeWallet($this->getAlias());

        $this->assertFalse($ad->isWalletExists($this->getAlias()));
    }

    /** @test */
    public function testUpdateWalletAfterRemove()
    {
        $this->times(1)->makeAd($this->user);
        $ad = $this->getFirstAd($this->user);

        $ad->saveWallet($this->getRandomWallet(), $this->getAlias());
        $ad->removeWallet($this->getAlias());
        $ad->saveWallet('123', $this->getAlias());

        $this->assertFalse($ad->isWalletExists($this->getAlias()));
        $this->assertCount(0, $ad->wallets()->get());
    }

    /** @test */
    public function testWalletMultiple()
    {
        $this->times(6)->makeAd($this->user);
        $adFirst = $this->getFirstAd($this->user);
        $adLast = $this->getLastAd($this->user);

        $adFirst->saveWallet($this->getRandomWallet(), $this->getAlias());
        $adLast->saveWallet($this->getRandomWallet(), $this->getRandomAlias());

        $this->assertCount(1, $adFirst->wallets()->get());
        $this->assertCount(1, $adLast->wallets()->get());
    }
}
