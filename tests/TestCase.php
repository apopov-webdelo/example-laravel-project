<?php

namespace Tests;

use App\Contracts\AuthenticatedContract;
use App\Jobs\Deal\UpdateDealCryptoAmountInUsd;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\PaymentSystem;
use App\Models\User\User;
use App\Services\User\RegistrationService;
use App\Utils\Json;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\Api\TestLog;
use Tests\TestApi\Traits\BalanceHelpers;

/**
 * Class TestCase
 *
 * @package Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions, TestLog, BalanceHelpers;

    /**
     * @var int
     */
    protected $times = 1;

    public function tearDown()
    {
        $this->beforeApplicationDestroyed(function () {
            DB::disconnect();
        });

        parent::tearDown();

        if ($container = \Mockery::getContainer()) {
            $this->addToAssertionCount($container->mockery_getExpectationCount());
        }

        \Mockery::close();
    }

    /**
     * @param $n
     *
     * @return $this
     */
    public function times($n)
    {
        $this->times = $n;

        return $this;
    }

    /**
     * @param User  $user
     * @param array $options
     */
    public function makeAd(User $user, $options = [])
    {
        while ($this->times--) {
            if (array_key_exists('payment_system_id', $options)) {
                $paymentSystem = PaymentSystem::find($options['payment_system_id']);
            } else {
                $paymentSystem = PaymentSystem::all()->random(1)->first();
            }

            $newAd = AdMaker::init($options)
                            ->author($user)
                            ->paymentSystem($paymentSystem);

            if (array_key_exists('is_sale', $options)) {
                $newAd->sale($options['is_sale']);
            }

            $newAd->create();
        }

        $this->times = 1;
    }

    /**
     * Create Deal for user
     *
     * @param User|null $user
     * @param Ad        $ad
     * @param array     $data
     *
     * @param bool      $bUpdateDispatch
     *
     * @return void
     */
    public function makeDeal(User $user, Ad $ad, $data = [], $bUpdateDispatch = false)
    {
        while ($this->times--) {
            $data['ad_id'] = $ad;

            $deal = DealMaker::init($data)
                             ->author($user)
                             ->create();

            if ($bUpdateDispatch) {
                UpdateDealCryptoAmountInUsd::dispatch($deal);
            }
        }

        $this->times = 1;
    }

    /**
     * Set the currently logged in user for the application.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string|null                                $driver
     *
     * @return $this
     */
    public function actingAs(UserContract $user, $driver = null)
    {
        $this->app->bind(AuthenticatedContract::class, function ($app) use ($user) {
            return $user;
        });

        return $this->be($user, $driver);
    }

    /**
     * @param string $guard
     * @param array  $data
     *
     * @return mixed
     */
    protected function loginUser($guard = 'api', array $data = [])
    {
        $user = factory(User::class)->create($data);

        $this->actingAs(
            $user,
            $guard
        );

        return $user;
    }

    /**
     * Create User
     *
     * @param array $data
     *
     * @return User
     */
    protected function registerUser($data = [])
    {
        Event::fake();

        return $this->registerUserWithEvents($data);
    }

    /**
     * Create User
     *
     * @param array $data
     *
     * @return User
     */
    protected function registerUserWithEvents($data = [])
    {
        $faker = \Faker\Factory::create();
        $password = bcrypt('secret');

        return app(RegistrationService::class)
            ->store([
                'login'                 => isset($data['login']) ? $data['login'] : $faker->unique()->userName,
                'email'                 => isset($data['email']) ? $data['email'] : $faker->unique()->safeEmail,
                'password'              => $password,
                'password_confirmation' => $password,
            ]);
    }

    /**
     * Create User
     *
     * @param string|null $login
     *
     * @return User
     */
    protected function createUser($login = null)
    {
        $userMaker = UserMaker::init()->state(UserMaker::STATE_ACTIVE);

        if ($login) {
            $userMaker->login($login);
        }

        return $userMaker->create();
    }

    /**
     * Test response to the route
     *
     * @param User   $user
     * @param string $route
     * @param string $method
     * @param array  $data
     * @param mixed  $parameters
     * @param bool   $needLog
     *
     * @return TestResponse $response
     */
    protected function response(
        User $user,
        string $route,
        string $method = 'get',
        $data = [],
        $parameters = [],
        $needLog = true
    ) {
        $response = $this->actingAs($user, 'api')
                         ->json($method, route($route, $parameters), $data);

        if ($needLog) {
            $this->log($response, $method, $route);
        }

        return $response;
    }

    /**
     * Test Log
     *
     * @param TestResponse $response
     * @param string       $method
     * @param string       $route
     *
     * @return void
     */
    protected function log(TestResponse $response, string $method, string $route)
    {
        $this->logger()->debug(
            strtoupper($method) . ' ' . $route,
            [
                'code' => $response->status(),
                'json' => $response->json(),
            ]
        );
    }

    /**
     * Asserts for testing
     *
     * @param TestResponse $response
     * @param Int          $status
     * @param array        $jsonAdditionalData
     *
     * @return void
     */
    protected function asserts($response, $status = Response::HTTP_OK, $jsonAdditionalData = [])
    {
        $response->assertStatus($status)
                 ->assertJsonStructure(
                     Json::structure(
                         $this->responseJsonStructure()
                     )
                 );

        if (!empty($jsonAdditionalData)) {
            $response->assertJson(
                [
                    'data' => $jsonAdditionalData,
                ]
            );
        }
    }

    /**
     * Assert if response field value is included in range
     *
     * @param TestResponse $response
     * @param string       $field
     * @param array        $range
     *
     * @return void
     */
    protected function assertsRanges($response, $field, $range)
    {
        $response->assertSuccessful()
                 ->assertJsonStructure(
                     Json::structure($this->responseJsonStructure())
                 );

        $responseData = json_decode($response->getContent())->data;
        $range = array_values($range);

        // Test assertion if response field value is included in range
        foreach ($responseData as $data) {
            $this->assertGreaterThanOrEqual($range[0], $data->{$field});
            $this->assertLessThanOrEqual($range[1], $data->{$field});
        }
    }

    /**
     * Get Response Data
     *
     * @param TestResponse $response
     *
     * @return mixed
     */
    protected function getResponseData(TestResponse $response)
    {
        $responseContent = json_decode($response->getContent());

        return $responseContent->data;
    }

    /**
     * @return mixed
     */
    protected function getRandomCrypto()
    {
        $crypto = CryptoCurrency::all()->random(1)->first();

        return $crypto;
    }

    /**
     * @param $code
     *
     * @return CryptoCurrency
     */
    protected function getCryptoByCode($code)
    {
        $crypto = CryptoCurrency::all()->where('code', $code)->first();

        return $crypto;
    }
}
