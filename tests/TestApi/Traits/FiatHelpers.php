<?php

namespace Tests\TestApi\Traits;

use App\Models\Directory\Currency;
use App\Models\Directory\FiatConstants;

trait FiatHelpers
{
    /**
     * Because some Fiat markets have problems with diff curencies sometimes
     *
     * @return Currency|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    protected function getUsdCurrency()
    {
        return Currency::query()->where('code', FiatConstants::USD)->first();
    }
}