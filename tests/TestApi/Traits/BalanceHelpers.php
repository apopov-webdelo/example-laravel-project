<?php

namespace Tests\TestApi\Traits;

use App\Models\User\User;

trait BalanceHelpers
{
    /**
     * @param User $user
     * @param      $crypto
     * @param int  $amount
     */
    protected function addBalance(User $user, $crypto, int $amount)
    {
        $balance = $user->getBalance($crypto);
        $balance->amount += $amount;
        $balance->save();
    }

    /**
     * @param User $user
     * @param      $crypto
     * @param int  $amount
     */
    protected function addEscrow(User $user, $crypto, int $amount)
    {
        $balance = $user->getBalance($crypto);
        $balance->escrow += $amount;
        $balance->save();
    }

    /**
     * @param User $user
     * @param      $crypto
     * @param int  $amount
     */
    protected function subBalance(User $user, $crypto, int $amount)
    {
        $this->addBalance($user, $crypto, -$amount);
    }

    /**
     * @param User $user
     * @param      $crypto
     * @param int  $amount
     */
    protected function subEscrow(User $user, $crypto, int $amount)
    {
        $this->addEscrow($user, $crypto, -$amount);
    }

    /**
     * @param User $user
     * @param      $crypto
     *
     * @return int|mixed
     */
    protected function getBalanceAmount(User $user, $crypto)
    {
        return $user->getBalance($crypto)->amount;
    }
}
