<?php

namespace Tests\TestApi\Traits;

use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use Illuminate\Support\Collection;

trait DealHelpers
{
    /**
     * @var DealRepo
     */
    private $repoDeal;

    /**
     * @param User $user
     *
     * @return Deal|false
     */
    private function lastDeal(User $user)
    {
        if (!$this->repoDeal) {
            $this->repoDeal = app(DealRepo::class);
        }

        return $this->repoDeal->filterByAuthor($user)->take()->get()->last();
    }

    /**
     * @param User $user
     *
     * @return Collection[Deal]
     */
    private function getDeals(User $user)
    {
        if (!$this->repoDeal) {
            $this->repoDeal = app(DealRepo::class);
        }

        return $this->repoDeal->filterByAuthor($user)->take()->get();
    }
}
