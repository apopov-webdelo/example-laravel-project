<?php

namespace Tests\TestApi\Traits;

use App\Models\Ad\Ad;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use Illuminate\Support\Collection;

trait AdHelpers
{
    /**
     * @var AdRepo
     */
    private $repoAd;

    /**
     * @param User $user
     *
     * @return Ad|false
     */
    private function lastAd(User $user)
    {
        if (!$this->repoAd) {
            $this->repoAd = app(AdRepo::class);
        }

        return $this->repoAd->filterByAuthor($user)->take()->get()->last();
    }

    /**
     * @param User $user
     *
     * @return Collection[Ad]
     */
    private function allAds(User $user)
    {
        if (!$this->repoAd) {
            $this->repoAd = app(AdRepo::class);
        }

        return $this->repoAd->filterByAuthor($user)->take()->get();
    }
}
