<?php

namespace Tests\Feature\Api\Notes;

use App\Http\Resources\Client\Note\NoteCollection;
use App\Makers\User\NoteMaker;
use App\Models\User\User;
use App\Repositories\User\NoteRepo;
use Tests\TestCase;

class NotesDeleteTest extends TestCase
{
    const ROUTE = 'notes.destroy';
    const METHOD = 'delete';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Notes created for user
     *
     * @var array
     */
    protected $notes = [];

    /**
     * Note to be deleted
     *
     * @var \App\Models\User\Note $noteDeleted
     */
    protected $noteDeleted;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');

        // Create a Recipient
        $this->recipient = $this->createUser('recipient');

        // Create 5 Notes
        $this->storeNotes($this->user, $this->recipient, 5);
    }

    /**
     * Test Response If authenticated User can view Notes Index Page
     */
    public function testNotesDeleteTest()
    {
        // Note to be Deleted
        $this->noteDeleted = array_shift($this->notes[$this->user->id]);

        // Get response
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->noteDeleted->id,
            false
        );
        
        // Asserts HTTP_STATUS
        $response->assertStatus(self::HTTP_STATUS);
        
        // Asserts HTTP RESPONSE is true
        $this->assertTrue($response == true);
        
        $noteCollection = app(NoteCollection::class, [
            'resource' => app(NoteRepo::class)
                ->filterByAuthor($this->user)
                ->take()
                ->with('recipient')
                ->paginate(25)
        ]);
        
        // Asserts If Note was Deleted from Repo
        $this->assertNotTrue($noteCollection->contains($this->noteDeleted));
    }

    /**
     * Create Notes for user
     *
     * @param User|null $user
     * @param User|null $recipient
     * @param int $count
     *
     * @return void
     */
    protected function storeNotes(
        User $user,
        User $recipient,
        int $count = 1
    ) {
        for ($i = 0; $i < $count; $i++) {
            $this->notes[$user->id][] = NoteMaker::init()
                ->author($user)
                ->recipient($recipient)
                ->create();
        }
    }

    /**
     * Test Response if authenticated User cannot delete others Notes
     */
    public function testUserDeleteOnlyHisNotesTest()
    {
        // Create a anotherUser
        $anotherUser = $this->createUser('anotherUser');
    
        // Create 5 Notes for anotherUser
        $this->storeNotes($anotherUser, $this->recipient, 5);
        
        // Note to be Deleted
        $this->noteDeleted = array_shift($this->notes[$anotherUser->id]);

        // Get response
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->noteDeleted->id
        );
        
        // Asserts HTTP_STATUS
        $response->assertStatus(403);
    }
}
