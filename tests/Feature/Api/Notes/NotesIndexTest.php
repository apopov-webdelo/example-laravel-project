<?php

namespace Tests\Feature\Api\Notes;

use App\Http\Resources\Client\Note\NoteCollection;
use App\Makers\User\NoteMaker;
use App\Models\User\User;
use App\Repositories\User\NoteRepo;
use Tests\TestCase;

class NotesIndexTest extends TestCase
{
    const ROUTE = 'notes.index';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Recipient for notes
     *
     * @var User|null $recipient
     */
    protected $recipient;

    /**
     * Notes created for user
     *
     * @var array
     */
    protected $notes = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');

        // Create a Recipient
        $this->recipient = $this->createUser('recipient');

        // Create 5 Notes
        $this->storeNotes($this->user, $this->recipient, 5);
    }

    /**
     * Test Response If authenticated User can view Notes Index Page
     */
    public function testUserNotesIndexPage()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD
        );

        // Reviews Array for asserting
        $notesArray = [];

        foreach ($this->notes[$this->user->id] as $note) {
            $notesArray[] = [
                'id' => $note->id,
                'recipient' => [
                    'id' => $this->recipient->id,
                ]
            ];
        }

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $notesArray
        );
    }

    /**
     * Create Notes for user
     *
     * @param User|null $user
     * @param User|null $recipient
     * @param int $count
     *
     * @return void
     */
    protected function storeNotes(
        User $user,
        User $recipient,
        int $count = 1
    ) {
        for ($i = 0; $i < $count; $i++) {
            $this->notes[$user->id][] = NoteMaker::init()
                ->author($user)
                ->recipient($recipient)
                ->create();
        }
    }

    /**
     * Return valid Json structure for NoteCollection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(NoteCollection::class, [
            'resource' => app(NoteRepo::class)
                ->take()
                ->paginate(25)
        ]);
    }

    /**
     * Test Response If authenticated User can view Only his Notes
     */
    public function testUserOnlyHisNotes()
    {
        // Create a anotherUser
        $anotherUser = $this->createUser('anotherUser');
    
        // Create 5 Notes for anotherUser
        $this->storeNotes($anotherUser, $this->recipient, 5);
    
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD
        );
    
        // Reviews Array for asserting
        $notesArray = [];
    
        foreach ($this->notes[$this->user->id] as $note) {
            $notesArray[] = [
                'id' => $note->id,
                'recipient' => [
                    'id' => $this->recipient->id,
                ]
            ];
        }
    
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $notesArray
        );
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
