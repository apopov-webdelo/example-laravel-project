<?php

namespace Tests\Feature\Api\Dashboard;

use App\Http\Resources\Client\Dashboard\DealCollection;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Models\User\User;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\Deal\DealStatusConstants;
use App\Repositories\Deal\DealRepo;
use Tests\TestCase;

class DashboardDealsListTest extends TestCase
{
    const ROUTE = 'dashboard.deals';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    const PAYMENT_SYSTEM_QIWI = 1;
    const COUNTRY_RUSSIA      = 1;
    const CURRENCY_RUB        = 1;
    const BANK_SYSTEM_ID      = 1;
    const DEAL_STATUS         = 1;

    /**
     * Event needed for that ad status
     *
     * @var string
     */
    protected $event;

    /**
     * First user
     *
     * User|null $user
     */
    protected $user;

    /**
     * Another user
     *
     * User|null $anotherUser
     */
    protected $anotherUser;

    /**
     * Array for CryptoCurrencies Ids
     *
     * @var array
     */
    protected $crypto = [];

    /**
     * Deals created for testing Deal model
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Ads created for testing Ad model
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
        
        // Create a User
        $this->anotherUser = $this->createUser('anotherUser');

        // Get Ids for CryptoCurrencies
        $this->crypto['BTC'] = $this->getCryptoCurrency(CryptoCurrencyConstants::BTC_CODE)->id;
        $this->crypto['ETH'] = $this->getCryptoCurrency(CryptoCurrencyConstants::ETH_CODE)->id;
    }

    /**
     * Test Response if User is viewing only his own deals
     *
     * @throws \Exception
     */
    public function testAssertResponseForUser()
    {
        // Create Ads for users
        $this->storeAds([
            'is_sale' => 1,
            'is_active' => 1,
        ]);
        
        // Create Deals for users
        $this->storeDealsForUsers();

        // Test for First user
        $this->assertResponse(
            $this->user,
            [
                'for_sale' => 1,
                'is_active' => 1,
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
            ]
        );
        
        // Test for Another user
        $this->assertResponse(
            $this->anotherUser,
            [
                'for_sale' => 1,
                'is_active' => 1,
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
            ]
        );
    }

    /**
     * Create Users and Ads for First & Another user
     *
     * @param array $data
     * @return void
     */
    protected function storeAds($data = [])
    {
        // Create Ads for First user
        $this->storeAd($this->user, $this->crypto['BTC'], $data);
        $this->storeAd($this->user, $this->crypto['ETH'], $data);

        // Create Ads for Another user
        $this->storeAd($this->anotherUser, $this->crypto['BTC'], $data);
        $this->storeAd($this->anotherUser, $this->crypto['ETH'], $data);
    }

    /**
     * Create Deals for First & Another user
     *
     * @param array $data
     * @return void
     *
     * @throws \Exception
     */
    protected function storeDealsForUsers($data = [])
    {
        // Create Deal for First user
        $this->storeDealsForUser($this->user, $data);
        
        // Create Deal for Another user
        $this->storeDealsForUser($this->anotherUser, $data);
    }

    /**
     * Create One Ad for user
     *
     * @param User|null $user
     * @param int|null $cryptoCurrencyId
     * @param array $data
     * @return void
     */
    protected function storeAd(User $user, $cryptoCurrencyId = null, $data = [])
    {
        $data['crypto_currency_id'] = $cryptoCurrencyId ?? $this->crypto['BTC'];

        $this->ads[$user->id][] = AdMaker::init($data)->author($user)->create();
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array $data
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function storeDealsForUser(User $user = null, $data = [])
    {
        foreach ($this->ads[$user->id] as $adObject) {
            $data['ad_id'] = $adObject->id;
            $this->deals[$user->id][] = DealMaker::init($data)
                ->status(DealStatusConstants::VERIFIED)
                ->author($user)
                ->create();
        }
    }

    /**
     * Return valid Collection structure for response
     *
     * @param string $code
     * @return CryptoCurrency
     */
    protected function getCryptoCurrency($code)
    {
        $cryptoCurrency = app(CryptoCurrencyRepo::class);
        return $cryptoCurrency->getByCode($code);
    }

    /**
     * Test response to DashBoard for specific User with Filter
     *
     * @param User  $user
     * @param array $data
     * @param array $jsonAdditionalData
     *
     * @return void
     */
    protected function assertResponse(User $user, $data = [], $jsonAdditionalData = [])
    {
        // Test if everybody can view Deal details
        $response = $this->response(
            $user,
            self::ROUTE,
            self::METHOD,
            $data
        );

        // Test if data author is current user
        $response->assertJson(
            [
                'data' => [
                    [
                        'author' => [
                            'id'    => $user->id,
                            'login' => $user->login,
                        ],
                    ]
                ],
            ]
        );
        
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [$jsonAdditionalData]
        );
    }

    /**
     * Return valid AdCollection structure for response
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(DealCollection::class, [
            'resource' => app(DealRepo::class)->take()->orderBy('price', 'desc')->paginate(25)
        ]);
    }

    /**
     * Test Response with Filter by CryptoCurrency
     *
     * @throws \Exception
     */
    public function testFilterByCryptoCurrency()
    {
        // Create Ads for users
        $this->storeAds([
            'is_sale' => 1,
            'is_active' => 1,
        ]);

        // Create Deals for users
        $this->storeDealsForUsers();

        // Test for First user with Bitcoin
        $this->assertResponse(
            $this->user,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'crypto_currency_id' => $this->crypto['BTC'],
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'crypto_currency' => [
                    'id' => $this->crypto['BTC'],
                ],
            ]
        );

        // Test for First user with Etherium
        $this->assertResponse(
            $this->user,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'crypto_currency_id' => $this->crypto['ETH'],
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'crypto_currency' => [
                    'id' => $this->crypto['ETH'],
                ],
            ]
        );

        // Test for Another user with Bitcoin
        $this->assertResponse(
            $this->anotherUser,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'crypto_currency_id' => $this->crypto['BTC'],
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'crypto_currency' => [
                    'id' => $this->crypto['BTC'],
                ],
            ]
        );

        // Test for Another user with Etherium
        $this->assertResponse(
            $this->anotherUser,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'crypto_currency_id' => $this->crypto['ETH'],
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'crypto_currency' => [
                    'id' => $this->crypto['ETH'],
                ],
            ]
        );
    }

    /**
     * Test Response with Filter by Currency
     *
     * @throws \Exception
     */
    public function testFilterByCurrency()
    {
        // Create Ads for users
        $this->storeAds([
            'is_sale' => 1,
            'is_active' => 1,
            'currency_id' =>self::CURRENCY_RUB,
        ]);

        // Create Deals for users
        $this->storeDealsForUsers();

        // Test for First user
        $this->assertResponse(
            $this->user,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'currency_id' => self::CURRENCY_RUB
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'currency' => [
                    'id' => self::CURRENCY_RUB,
                ],
            ]
        );

        // Test for Another user
        $this->assertResponse(
            $this->anotherUser,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'currency' => [
                    'id' => self::CURRENCY_RUB,
                ],
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'currency' => [
                    'id' => self::CURRENCY_RUB,
                ],
            ]
        );
    }

    /**
     * Test Response with Filter by Payment System
     *
     * @throws \Exception
     */
    public function testFilterByPaymentSystem()
    {
        // Create Ads for users
        $this->storeAds([
            'is_sale' => 1,
            'is_active' => 1,
            'payment_system_id' => self::PAYMENT_SYSTEM_QIWI,
        ]);

        // Create Deals for users
        $this->storeDealsForUsers();

        // Test for First user
        $this->assertResponse(
            $this->user,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'payment_system_id' => self::PAYMENT_SYSTEM_QIWI
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'payment_system' => [
                    'id' => self::PAYMENT_SYSTEM_QIWI,
                ],
            ]
        );

        // Test for Another user
        $this->assertResponse(
            $this->anotherUser,
            [
                'for_sale' => 1,
                'is_active' => 1,
                'payment_system' => [
                    'id' => self::PAYMENT_SYSTEM_QIWI,
                ],
            ],
            [
                'is_sale' => true,
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
                'payment_system' => [
                    'id' => self::PAYMENT_SYSTEM_QIWI,
                ],
            ]
        );
    }
}
