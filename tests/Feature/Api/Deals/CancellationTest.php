<?php

namespace Tests\Feature\Api\Deals;

use App\Events\Deal\DealCancellation;
use App\Makers\User\UserMaker;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;

class CancellationTest extends DealTest
{
    /**
     * Route for deal status response
     *
     * @var string
     */
    protected $route = 'deal.cancellation';

    /**
     * Deal will be stored with this status id
     *
     * @var int
     */
    protected $storeStatusId = DealStatusConstants::VERIFIED;

    /**
     * Event needed for that deal status
     *
     * @var string
     */
    protected $event = DealCancellation::class;

    protected function setUp()
    {
        parent::setUp();
        Event::fake();
        Queue::fake();
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestFromBuyerNotVerifiedDeal()
    {
        $this->storeDeal(DealStatusConstants::VERIFICATION);

        $buyer = $this->deal->getBuyer();

        $this->getResponse($this->route, 'put', $buyer)
             ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestFromBuyer()
    {
        $this->checkDealChangeStatusResponse(
            DealStatusConstants::CANCELLATION,
            Response::HTTP_OK
        );
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestFromSeller()
    {
        $this->checkFailStatusDealResponse(
            Response::HTTP_FORBIDDEN,
            false
        );
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestFromNotDealMember()
    {
        Event::fake();

        $this->storeDeal(DealStatusConstants::VERIFIED);

        $this->getResponse($this->route, 'put', UserMaker::init()->create())
             ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Test response to deal with fail status for pay request
     *
     * @param int $storeStatusId
     *
     * @return TestResponse
     *
     * @throws \Exception
     */
    protected function checkFailStatusDeal(int $storeStatusId)
    {
        return $this->checkFailStatusDealResponse(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            true,
            $storeStatusId
        );
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestToCanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::CANCELED);
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestToFinishedDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::FINISHED);
    }

    /**
     * @throws \Exception
     */
    public function testCancelRequestToAutocanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::AUTOCANCELED);
    }
}
