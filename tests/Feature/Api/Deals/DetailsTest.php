<?php

namespace Tests\Feature\Api\Deals;

use App\Makers\User\UserMaker;
use App\Models\Deal\Deal;
use App\Models\User\User;
use Tests\Feature\Api\Dealable;
use Tests\TestCase;

class DetailsTest extends TestCase
{
    use Dealable;

    /**
     * Test valid response for DealAuthor
     *
     * @throws \Exception
     */
    public function testValidDealOwner()
    {
        $this->storeDeal();
        $response = $this->getDealResponse($this->dealAuthor, $this->deal);
        $this->assertValidDealResponse($response);
    }

    /**
     * Return response according user and deal
     *
     * @param User $user
     * @param Deal $deal
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function getDealResponse(User $user, Deal $deal)
    {
        $payload = [
            'deal' => $deal->id,
        ];

        return $this->actingAs($user, 'api')->getJson(route('deal.details', $payload));
    }

    /**
     * Test valid response
     *
     * @param $response \Illuminate\Foundation\Testing\TestResponse
     * @return $this
     */
    protected function assertValidDealResponse($response)
    {
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $this->deal->id,
            ],
        ]);
        return $this;
    }

    /**
     * Test is Ad owner get valid data
     *
     * @throws \Exception
     */
    public function testAdOwner()
    {
        /** @var User $user */
        $response = $this->storeDeal()->getDealResponse($this->adAuthor, $this->deal);

        $this->assertValidDealResponse($response);
    }

    /**
     * Test access block for not deal member
     *
     * @throws \Exception
     */
    public function testInvalidDealOwner()
    {
        /** @var User $user */
        $user = UserMaker::init()->create();
        $this->storeDeal()->getDealResponse($user, $this->deal)->assertStatus(403);
    }
}
