<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/23/18
 * Time: 10:35
 */

namespace Tests\Feature\Api\Deals;

use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Http\Resources\Client\Dashboard\DealResource;
use App\Makers\DealMaker;
use App\Models\Deal\DealStatus;
use App\Models\User\User;
use App\Utils\Json;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Event;
use Tests\Feature\Api\Dealable;
use Tests\TestCase;

abstract class DealTest extends TestCase
{
    use Dealable;

    /**
     * Route for deal status response
     *
     * @var string
     */
    protected $route;

    /**
     * Event needed for that deal status
     *
     * @var string
     */
    protected $event;

    /**
     * Deal will be stored with this status id
     *
     * @var int
     */
    protected $storeStatusId;

    /**
     * Test response to deal with valid statuses and users for request
     *
     * @param int $statusId
     * @param int $httpStatus
     * @param bool $fromBuyer
     * @param int|null $storeStatusId
     *
     * @return TestResponse
     *
     * @throws \Exception
     */
    protected function checkDealChangeStatusResponse(
        int $statusId,
        int $httpStatus,
        bool $fromBuyer = true,
        int $storeStatusId = null
    ) {
        Event::fake();

        $storeStatusId = $storeStatusId ?? $this->storeStatusId;

        $this->storeDeal($storeStatusId);

        /** @var User $user */
        $user = $fromBuyer ? $this->deal->getBuyer() : $this->deal->getSeller();

        $response = $this->getResponse($this->route, 'put', $user)
                        ->assertStatus($httpStatus)
                        ->assertJsonStructure($this->getDealResourceStructure())
                        ->assertJson([
                            'data' => [
                                'id'     => $this->deal->id,
                                'author' => [
                                    'id' => $this->dealAuthor->id,
                                ],
                                'status' => [
                                    'id'    => DealStatus::findOrFail($statusId)->id,
                                    'title' => DealStatus::findOrFail($statusId)->title,
                                ]
                            ]
                        ]);

        $this->assertEvent();

        return $response;
    }

    /**
     * Send request by route and return response
     *
     * @param string $route
     * @param string $requestType
     * @param null $user
     * @param array $payload
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     *
     * @throws \Exception
     */
    protected function getResponse(
        string $route,
        string $requestType = 'put',
        $user = null,
        $payload = []
    ) {
        if (empty($route)) {
            throw new \Exception('Route is empty for response!');
        }

        $payload = $payload instanceof Collection
            ? $payload->toArray()
            : $payload;

        $response = $this->actingAs($user ?? $this->dealAuthor, 'api')
                         ->json($requestType, route($route, ['deal' => $this->deal->id]), $payload);

        $this->log($response, $requestType, $route);

        return $response;
    }

    /**
     * Return valid structure for DealResource
     *
     * @return array
     */
    protected function getDealResourceStructure()
    {
        return Json::structure(
            app(DealResource::class, ['resource' => DealMaker::init()->create()])
        );
    }

    /**
     * Check if event was fired
     *
     * @param int $times
     */
    protected function assertEvent(int $times = 1)
    {
        if (class_exists($this->event)) {
            Event::assertDispatched($this->event, $times);
        }
    }

    /**
     * Test response to deal with fail status for request
     *
     * @param int|null $httpStatus
     * @param bool $fromBuyer
     * @param int|null $storeStatusId
     * @return TestResponse
     *
     * @throws \Exception
     */
    protected function checkFailStatusDealResponse(
        int $httpStatus = null,
        bool $fromBuyer = true,
        int $storeStatusId = null
    ) {
        Event::fake();

        $storeStatusId = $storeStatusId ?? $this->storeStatusId;

        $this->storeDeal($storeStatusId);

        $httpStatus = $httpStatus ?? UnexpectedDealStatusException::class;

        $user = $fromBuyer ? $this->deal->getBuyer() : $this->deal->getSeller();

        $response = $this->getResponse($this->route, 'put', $user)
                         ->assertStatus($httpStatus);

        $this->assertEvent(0);

        return $response;
    }
}
