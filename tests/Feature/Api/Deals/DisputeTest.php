<?php

namespace Tests\Feature\Api\Deals;

use App\Events\Deal\DealDisputed;
use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Makers\DealMaker;
use App\Makers\User\UserMaker;
use App\Models\Deal\DealStatusConstants;
use Event;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Carbon\Carbon;

class DisputeTest extends DealTest
{
    /**
     * Route for deal status response
     *
     * @var string
     */
    protected $route = 'deal.dispute';

    /**
     * Deal will be stored with this status id
     *
     * @var int
     */
    protected $storeStatusId = DealStatusConstants::PAID;

    /**
     * Event needed for that deal status
     *
     * @var string
     */
    protected $event = DealDisputed::class;

    /**
     * Store deal with needed status
     *
     * @param int|null $statusId
     * @return $this
     *
     * @throws \Exception
     */
    protected function storeDeal(int $statusId = null)
    {
        $this->storeAd();

        $dealMaker = DealMaker::init([
            'ad_id' => $this->ad->id,
        ]);

        if ($statusId) {
            $dealMaker->status($statusId, now()->subHour());
        }

        $this->deal = $dealMaker->create();
        $this->dealAuthor = $this->deal->author;

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestFromBuyer()
    {
        $this->checkDealChangeStatusResponse(
            DealStatusConstants::IN_DISPUTE,
            200
        );
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestFromSeller()
    {
        $this->checkDealChangeStatusResponse(
            DealStatusConstants::IN_DISPUTE,
            200,
            false
        );
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestFromNotDealMember()
    {
        Event::fake();

        $this->storeDeal(DealStatusConstants::PAID);

        $this->getResponse($this->route, 'put', UserMaker::init()->create())
             ->assertStatus(403);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestToInProcessDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::VERIFIED);
    }

    /**
     * Test response to deal with fail status for pay request
     *
     * @param int $storeStatusId
     * @return TestResponse
     *
     * @throws \Exception
     */
    protected function checkFailStatusDeal(int $storeStatusId)
    {
        return $this->checkFailStatusDealResponse(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            true,
            $storeStatusId
        );
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestToFinishedDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::FINISHED);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestToDisputedDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::IN_DISPUTE);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestToAutocanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::AUTOCANCELED);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestToCanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::CANCELED);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestAfterThirtyMinutes()
    {
        Event::fake();

        $this->storeAd();

        $dealMaker = DealMaker::init([
            'ad_id' => $this->ad->id,
            'created_at' => Carbon::now()->addMinutes(-35)
                ->format('Y-m-d H:i:s'),
        ]);

        $dealMaker->status(
            DealStatusConstants::PAID,
            Carbon::now()->addMinutes(-30)
        );

        $this->deal = $dealMaker->create();
        $this->dealAuthor = $this->deal->author;

        // Test for Deal Author
        $response = $this->getResponse($this->route, 'put', $this->dealAuthor);
        $response->assertStatus(200);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestBeforeThirtyMinutes()
    {
        Event::fake();

        $this->storeAd();

        $dealMaker = DealMaker::init([
            'ad_id' => $this->ad->id,
            'created_at' => Carbon::now()->addMinutes(-30)
                ->format('Y-m-d H:i:s'),
        ]);

        $dealMaker->status(
            DealStatusConstants::PAID,
            Carbon::now()->addMinutes(-29)
        );

        $this->deal = $dealMaker->create();
        $this->dealAuthor = $this->deal->author;

        // Test for Deal Author
        $response = $this->getResponse($this->route, 'put', $this->dealAuthor);
        
        // Try to receive Unprocessable Response
        $response->assertStatus(422);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestByAdAuthorAfterThirtyMinutes()
    {
        Event::fake();

        $this->storeAd();

        $dealMaker = DealMaker::init([
            'ad_id' => $this->ad->id,
            'created_at' => Carbon::now()->addMinutes(-35)
                ->format('Y-m-d H:i:s'),
        ]);

        $dealMaker->status(
            DealStatusConstants::PAID,
            Carbon::now()->addMinutes(-30)
        );

        $this->deal = $dealMaker->create();

        $this->adAuthor = $this->ad->author;

        // Test for Ad Author
        $response = $this->getResponse($this->route, 'put', $this->adAuthor);
        $response->assertStatus(200);
    }

    /**
     * @throws \Exception
     */
    public function testDisputeRequestByAdAuthorBeforeThirtyMinutes()
    {
        Event::fake();

        $this->storeAd();

        $dealMaker = DealMaker::init([
            'ad_id' => $this->ad->id,
            'created_at' => Carbon::now()->addMinutes(-30)
                ->format('Y-m-d H:i:s'),
        ]);

        $dealMaker->status(
            DealStatusConstants::PAID,
            Carbon::now()->addMinutes(-29)
        );

        $this->deal = $dealMaker->create();
        
        $this->adAuthor = $this->ad->author;

        // Test for Ad Author
        $responseAd = $this->getResponse($this->route, 'put', $this->adAuthor);
        
        // Try to receive Unprocessable Response
        $responseAd->assertStatus(422);
    }
}
