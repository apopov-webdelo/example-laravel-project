<?php

namespace Tests\Feature\Api\Deals;

use App\Events\Deal\DealPaid;
use App\Makers\User\UserMaker;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;

class PayTest extends DealTest
{
    /**
     * Route for deal status response
     *
     * @var string
     */
    protected $route = 'deal.paid';

    /**
     * Deal will be stored with this status id
     *
     * @var int
     */
    protected $storeStatusId = DealStatusConstants::VERIFIED;

    /**
     * Event needed for that deal status
     *
     * @var string
     */
    protected $event = DealPaid::class;

    /**
     * @throws \Exception
     */
    public function testPaidRequestFromBuyerNotVerifiedDeal()
    {
        $this->storeDeal(DealStatusConstants::VERIFICATION);

        $buyer = $this->deal->getBuyer();

        $this->getResponse($this->route, 'put', $buyer)
             ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestFromBuyer()
    {
        $this->checkDealChangeStatusResponse(
            DealStatusConstants::PAID,
            200
        );
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestFromSeller()
    {
        $this->checkFailStatusDealResponse(
            403,
            false
        );
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestFromNotDealMember()
    {
        Event::fake();

        $this->storeDeal(DealStatusConstants::VERIFIED);

        $this->getResponse($this->route, 'put', UserMaker::init()->create())
             ->assertStatus(403);
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestToPaidDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::PAID);
    }

    /**
     * Test response to deal with fail status for pay request
     *
     * @param int $storeStatusId
     *
     * @return TestResponse
     *
     * @throws \Exception
     */
    protected function checkFailStatusDeal(int $storeStatusId)
    {
        return $this->checkFailStatusDealResponse(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            true,
            $storeStatusId
        );
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestToFinishedDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::FINISHED);
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestToDisputedDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::IN_DISPUTE);
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestToAutocanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::AUTOCANCELED);
    }

    /**
     * @throws \Exception
     */
    public function testPaidRequestToCanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::CANCELED);
    }
}
