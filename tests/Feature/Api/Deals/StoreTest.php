<?php

namespace Tests\Feature\Api\Deals;

use App\Events\Deal\DealPlaced;
use App\Makers\AdMaker;
use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\Deal\DealStatus;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use Illuminate\Support\Facades\Event;
use Tests\Feature\Api\Authorable;

/**
 * Class StoreTest
 *
 * @package Tests\Feature\Api\Deals
 */
class StoreTest extends DealTest
{
    use Authorable;

    const ROUTE = 'deal.store';

    /**
     * Fiat amount for deal creating
     */
    const DEAL_FIAT_AMOUNT = 250;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStore()
    {
        Event::fake();

        // Configure deal author
        $dealAuthor = $this->setAuthor()->author;

        // Configure ad
        $ad = AdMaker::init()->create();

        $this->storeDealRequest($dealAuthor, $ad)
             ->assertStatus(201)
             ->assertJsonStructure($this->getDealResourceStructure())
             ->assertJson([
                 'data' => [
                     'author'          => [
                         'id'    => $this->author->id,
                         'login' => $this->author->login,
                     ],
                     'payment_system'  => [
                         'id' => $ad->payment_system_id,
                     ],
                     'status'          => [
                         'id'    => DealStatus::find(DealStatusConstants::VERIFICATION)->id,
                         'title' => DealStatus::find(DealStatusConstants::VERIFICATION)->title,
                     ],
                     'currency'        => [
                         'id' => $ad->currency_id,
                     ],
                     'crypto_currency' => [
                         'id'    => $ad->crypto_currency_id,
                         'title' => $ad->cryptoCurrency->title,
                     ],
                     'fiat_amount'     => self::DEAL_FIAT_AMOUNT,
                     'price'           => currencyFromCoins($ad->price, $ad->currency),
                 ],
             ]);

        Event::assertDispatched(DealPlaced::class, 1);
    }

    /**
     * @param User $dealAuthor
     * @param Ad   $ad
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function storeDealRequest(User $dealAuthor, Ad $ad)
    {
        $payload = collect([
            'ad_id'       => $ad->id,
            'price'       => $ad->price,
            'fiat_amount' => self::DEAL_FIAT_AMOUNT,
        ]);

        $response = $this->actingAs($dealAuthor, 'api')
                         ->postJson(route(self::ROUTE), $payload->toArray());

        $this->log($response, 'post', self::ROUTE);

        return $response;
    }

    /**
     * @return array
     */
    protected function getValidJsonStructure(): array
    {
        return [
            'data' => [
                'id',
                'author'          => [
                    'id',
                    'login',
                    'last_seen',
                    'trust_coef',
                    'negative_count',
                    'positive_count',
                    'is_blocked',
                    'is_favorite',
                    'note',
                ],
                'payment_system'  => [
                    'id',
                    'title',
                ],
                'status',
                'currency'        => [
                    'id',
                    'title',
                ],
                'crypto_currency' => [
                    'id',
                    'title',
                ],
                'fiat_amount',
                'crypto_amount',
                'price',
                'created_at',
            ],
        ];
    }

    /**
     * @throws \Exception
     */
    public function testWrongRating()
    {
        Event::fake();

        // Configure deal author
        $dealAuthor = $this->setAuthor()->author;
        $dealAuthor->reputation->fill(['rate' => 10])->save();

        // Configure ad
        $ad = AdMaker::init()->reviewRate(20)->create();

        $this->storeDealRequest($dealAuthor, $ad)
             ->assertStatus(422)
             ->assertJsonStructure([
                 'message',
                 'errors' => [
                     'ad_id',
                 ],
             ]);

        Event::assertDispatched(DealPlaced::class, 0);
    }

    /**
     * @throws \Exception
     */
    public function testWrongTurnover()
    {
        Event::fake();

        // Configure deal author
        $dealAuthor = $this->setAuthor()->author;
        $dealAuthor->statistic
            ->fill(['total_turnover' => 0])
            ->save();

        // Configure ad
        $ad = AdMaker::init()->turnover(10)->create();

        $this->storeDealRequest($dealAuthor, $ad)
             ->assertStatus(422)
             ->assertJsonStructure([
                 'message',
                 'errors' => [
                     'ad_id',
                 ],
             ]);

        Event::assertDispatched(DealPlaced::class, 0);
    }

    /**
     * @throws \Exception
     */
    public function testWrongMinDealFinish()
    {
        Event::fake();

        // Configure deal author
        $dealAuthor = $this->setAuthor()->author;
        $dealAuthor->statistic
            ->fill(['deals_finished_count' => 0])
            ->save();

        // Configure ad
        $ad = AdMaker::init()->minDealFinish(10)->create();

        $this->storeDealRequest($dealAuthor, $ad)
             ->assertStatus(422)
             ->assertJsonStructure([
                 'message',
                 'errors' => [
                     'ad_id',
                 ],
             ]);

        Event::assertDispatched(DealPlaced::class, 0);
    }

    public function testWrongDealerBalance()
    {
        Event::fake();

        // Configure deal author
        $dealAuthor = $this->setAuthor()->author;

        // Configure ad author
        $adAuthor = UserMaker::init()->create();

        // Configure ad
        $ad = AdMaker::init()->author($adAuthor)->create();

        $adAuthor->balance
            ->where('crypto_currency_id', $ad->crypto_currency_id)
            ->first()
            ->fill(['amount' => 0])
            ->save();

        $this->storeDealRequest($dealAuthor, $ad)
             ->assertStatus(422)
             ->assertJsonStructure([
                 'message',
                 'errors' => [
                     'ad_id',
                 ],
             ]);

        Event::assertDispatched(DealPlaced::class, 0);
    }
}
