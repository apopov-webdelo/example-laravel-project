<?php

namespace Tests\Feature\Api\Deals;

use App\Events\Deal\DealFinishing;
use App\Makers\User\UserMaker;
use App\Models\Deal\DealStatusConstants;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;

class FinishingTest extends DealTest
{
    /**
     * Route for deal status response
     *
     * @var string
     */
    protected $route = 'deal.finishing';

    /**
     * Deal will be stored with this status id
     *
     * @var int
     */
    protected $storeStatusId = DealStatusConstants::PAID;

    /**
     * Event needed for that deal status
     *
     * @var string
     */
    protected $event = DealFinishing::class;

    /**
     * @throws \Exception
     */
    public function testCancelRequestFromSellerNotVerifiedDeal()
    {
        $this->storeDeal(DealStatusConstants::VERIFICATION);

        $seller = $this->deal->getSeller();

        $this->getResponse($this->route, 'put', $seller)
             ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @throws \Exception
     */
    public function testFinishRequestFromBuyer()
    {
        $this->checkFailStatusDealResponse(403);
    }

    /**
     * @throws \Exception
     */
    public function testFinishRequestFromSeller()
    {
        $this->checkDealChangeStatusResponse(
            DealStatusConstants::FINISHING,
            200,
            false
        );
    }

    /**
     * @throws \Exception
     */
    public function testFinishRequestFromNotDealMember()
    {
        Event::fake();

        $this->storeDeal($this->storeStatusId);

        $this->getResponse($this->route, 'put', UserMaker::init()->create())
             ->assertStatus(403);
    }

    /**
     * @throws \Exception
     */
    public function testFinishRequestToCanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::CANCELED);
    }

    /**
     * Test response to deal with fail status for pay request
     *
     * @param int $storeStatusId
     *
     * @return TestResponse
     *
     * @throws \Exception
     */
    protected function checkFailStatusDeal(int $storeStatusId)
    {
        return $this->checkFailStatusDealResponse(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            false,
            $storeStatusId
        );
    }

    /**
     * @throws \Exception
     */
    public function testFinishRequestToFinishedDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::FINISHING);
    }

    /**
     * @throws \Exception
     */
    public function testFinishRequestToAutocanceledDeal()
    {
        $this->checkFailStatusDeal(DealStatusConstants::AUTOCANCELED);
    }
}
