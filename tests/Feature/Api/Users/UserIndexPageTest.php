<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\User\UserCollection;
use App\Models\User\User;
use App\Repositories\User\UserRepo;
use Tests\TestCase;

class UserIndexPageTest extends TestCase
{
    const ROUTE = 'user.index';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Users
     *
     * @var array $users
     */
    protected $users = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->registerUser();
        
        // Create another users
        for ($i = 0; $i < 5; $i++) {
            $this->registerUser([
                'login' => 'anotherUser'.$i,
            ]);
        }
    }

    /**
     * Test Response If authenticated User can view All users list without filter
     */
    public function testUsersList()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            ['login' => 'robot']
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS
        );
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(UserCollection::class, [
            'resource' => app(UserRepo::class)->take()->paginate(50)
        ]);
    }

    /**
     * Test Response If authenticated User can view All users list with filter
     */
    public function testUsersListWithFilter()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            ['login' => 'anotherUser0']
        );

        // Test for user with filter
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                ['login' => 'anotherUser0']
            ]
        );
    }

    /**
     * Test Response If Unauthenticated guests can access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}