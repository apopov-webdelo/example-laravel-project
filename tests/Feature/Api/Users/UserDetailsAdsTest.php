<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\Dashboard\AdCollection;
use App\Makers\AdMaker;
use App\Models\User\User;
use App\Repositories\Ad\AdRepo;
use Tests\TestCase;

class UserDetailsAdsTest extends TestCase
{
    const ROUTE = 'user.ads.index';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User|null $adUser
     */
    protected $adUser;

    /**
     * Ads created for testing Ad model
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
        
        // Create a adUser
        $this->adUser = $this->createUser('adUser');

        // Create 5 active Ads for adUser
        $this->storeAds(
            $this->adUser,
            5,
            [
                'is_sale' => 1,
            ]
        );

        // Create 5 Ads for adUser with filter is_sale 0
        $this->storeAds(
            $this->adUser,
            5,
            [
                'is_sale' => 0,
            ]
        );
        
        // Create a anotherAdUser
        $this->anotherAdUser = $this->createUser('anotherAdUser');

        // Create 5 Ads for anotherAdUser
        $this->storeAds($this->anotherAdUser, 5);
    }

    /**
     * Test Response If authenticated User can view Ads list without filter
     */
    public function testUsersAdsList()
    {
        // Test user views deals. Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->adUser->id
        );
        
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->adUser->id,
                ],
            ],]
        );
    }

    /**
     * Test Response If authenticated User can view Ads list with filter
     */
    public function testUsersAdsListWithFilter()
    {
        // Test user views deals. Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'for_sale' => 1,
            ],
            $this->adUser->id
        );
        
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->adUser->id,
                ],
                'is_sale' => true,
            ],]
        );
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int $count
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init()->author($user)->create();
        }
    }

    /**
     * Return valid Json structure for Ad Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(AdCollection::class, [
            'resource' => app(AdRepo::class)->take()->paginate(5)
        ]);
    }

    /**
     * Test Response If Unauthenticated guests can view Ads list
     */
    public function testAssertResponseWithoutUser()
    {
        // Test if Unauthenticated guest can view Ads list
        $response = $this->json(self::METHOD, route(self::ROUTE, $this->adUser->id));

        $this->log($response, self::METHOD, self::ROUTE);

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->adUser->id,
                ],
            ],]
        );
    }
}