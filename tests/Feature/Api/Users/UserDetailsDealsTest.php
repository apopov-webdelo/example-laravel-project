<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\Dashboard\DealCollection;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use App\Repositories\Deal\DealRepo;
use Tests\TestCase;

class UserDetailsDealsTest extends TestCase
{
    const ROUTE = 'user.deals.index';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User|null $dealUser
     */
    protected $dealUser;

    /**
     * User
     *
     * @var User|null $anotherUser
     */
    protected $anotherUser;

    /**
     * User for route
     *
     * @var User|null $anotherDealUser
     */
    protected $anotherDealUser;

    /**
     * Ads created for dealUser
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Deals created for dealUser
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
        
        // Create a User
        $this->dealUser = $this->createUser('dealUser');

        // Create 5 active Ads for User
        $this->storeAds(
            $this->user,
            5,
            [
                'is_sale' => 1,
            ]
        );

        // Create 5 Ads for User with filter 
        $this->storeAds(
            $this->user,
            5,
            [
                'is_sale' => 0,
            ]
        );

        // Create Deals for dealUser with status VERIFIED
        $this->storeDeals($this->dealUser, DealStatusConstants::VERIFIED);
        
        // Create a anotherUser
        $this->anotherUser = $this->createUser('anotherUser');
        
        // Create a anotherDealUser
        $this->anotherDealUser = $this->createUser('anotherDealUser');

        // Create 5 Ads for anotherUser with filter is_sale 1
        $this->storeAds(
            $this->anotherUser,
            5,
            [
                'is_sale' => 1,
            ]
        );

        // Create 5 Ads for User with filter is_sale 0
        $this->storeAds(
            $this->anotherUser,
            5,
            [
                'is_sale' => 0,
            ]
        );

        // Create Deals for anotherDealUser with status FINISHED
        $this->storeDeals($this->anotherDealUser, DealStatusConstants::FINISHED);
    }

    /**
     * Test Response If authenticated User can view Deals list without filter
     */
    public function testUsersDealsList()
    {
        // Test user views deals. Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->dealUser->id
        );
        
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->dealUser->id,
                ],
            ],]
        );

        // Test anotherUser views his deals. Acting route as anotherUser
        $response = $this->response(
            $this->anotherUser,
            self::ROUTE,
            self::METHOD,
            [],
            $this->anotherDealUser->id
        );
        
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->anotherDealUser->id,
                ],
            ]]
        );
    }

    /**
     * Test Response If authenticated User can view Deals list with filter
     */
    public function testUsersDealsListWithFilter()
    {
        // Test user views his deals
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'for_sale' => 1,
                'is_active' => 1,
            ],
            $this->dealUser->id
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->dealUser->id,
                ],
                'ad' => [
                    'is_sale' => false,
                ],
                'status' => [
                    'id' => DealStatusConstants::VERIFIED,
                ],
            ]]
        );

        // Test if another user cannot view another deals
        $response = $this->response(
            $this->anotherUser,
            self::ROUTE,
            self::METHOD,
            [
                'for_buy' => 1,
                'is_blocked' => 1,
            ],
            $this->anotherDealUser->id
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [[
                'author' => [
                    'id' => $this->anotherDealUser->id,
                ],
                'ad' => [
                    'is_sale' => true,
                ],
                'status' => [
                    'id' => DealStatusConstants::FINISHED,
                ],
            ]]
        );
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int $count
     * @param array $data
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1, $data = [])
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init($data)
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param int $status
     * @param array $data
     *
     * @return void
     */
    protected function storeDeals(User $user = null, int $status = 1, $data = [])
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $this->deals[] = DealMaker::init($data)
                ->status($status)
                ->author($user)
                ->create();
        }
    }

    /**
     * Return valid Json structure for DealRepo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(DealCollection::class, [
            'resource' => app(DealRepo::class)->take()->paginate(5)
        ]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE, $this->dealUser->id));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}