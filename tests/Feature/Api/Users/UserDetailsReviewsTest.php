<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\Market\ReviewCollection;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Makers\User\ReviewMaker;
use App\Models\User\User;
use Tests\TestCase;

class UserDetailsReviewsTest extends TestCase
{
    const ROUTE       = 'user.reviews';
    const METHOD      = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User|null $user
     */
    protected $searchUser;

    /**
     * Ads created for User
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Deals created for searchUser
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Deals created for searchUser
     *
     * @var array
     */
    protected $reviews = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser('user');

        // Create a searchUser
        $this->searchUser = $this->createUser('searchUser');

        // Create 5 Ads for User
        $this->storeAds($this->user, 3);

        // Create 5 Deals for searchUser
        $this->storeDeals($this->searchUser);

        // Create 5 Reviews for searchUser
        $this->storeReviews($this->searchUser);
    }

    /**
     * Test Response If authenticated User can view Reviews list without filter
     */
    public function testUsersReviewsList()
    {
        // Test user views Reviews. Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->searchUser->id
        );

        // Reviews Array for asserting
        $reviewsArray = [];

        // filter rate=1 reviews that should be hidden
        $this->reviews = array_filter($this->reviews, function ($review) {
            return $review->rate != 1;
        });

        foreach ($this->reviews as $review) {
            $reviewsArray[] = [
                'id'        => $review->id,
                'from_user' => [
                    'id' => $this->user->id,
                ],
            ];
        }

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $reviewsArray
        );
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int  $count
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init()
                                  ->author($user)
                                  ->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array     $data
     *
     * @return void
     */
    protected function storeDeals(User $user = null, $data = [])
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $this->deals[] = DealMaker::init($data)
                                      ->author($user)
                                      ->create();
        }
    }

    /**
     * Create Reviews for user
     *
     * @param User|null $user
     * @param array     $data
     *
     * @return void
     */
    protected function storeReviews(User $user = null, $data = [])
    {
        foreach ($this->deals as $deal) {
            $data['deal_id'] = $deal->id;
            $this->reviews[] = ReviewMaker::init($data)
                                          ->recipient($user)
                                          ->author($this->user)
                                          ->deal($deal)
                                          ->create();
        }

        $this->reviews = array_reverse($this->reviews);
    }

    /**
     * Return valid Json structure for ReviewCollection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ReviewCollection::class, [
            'resource' => $this->searchUser->reviewsIncome()->paginate(10),
        ]);
    }

    /**
     * Test Response If Unauthenticated guests can access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE, $this->searchUser->id));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(self::HTTP_STATUS);
    }
}