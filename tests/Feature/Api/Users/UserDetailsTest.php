<?php

namespace Tests\Feature\Api\Users;

use App\Models\User\User;
use App\Repositories\User\UserRepo;
use Tests\TestCase;

class UserDetailsTest extends TestCase
{
    const ROUTE = 'user.details';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User|null $searchUser
     */
    protected $searchUser;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
        
        // Create a searchUser
        $this->searchUser = $this->createUser('searchUser');
    }

    /**
     * Test Response If authenticated User can view All users list without filter
     */
    public function testUsersList()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->searchUser->id
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            ['id' => $this->searchUser->id]
        );
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(UserRepo::class)->take();
    }

    /**
     * Test Response If Unauthenticated guests can access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE, $this->searchUser->id));

        $this->log($response, self::METHOD, self::ROUTE);

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            ['id' => $this->searchUser->id]
        );
    }
}