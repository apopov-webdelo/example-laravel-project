<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/20/18
 * Time: 12:17
 */

namespace Tests\Feature\Api;

use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

trait TestLog
{
    /**
     * Return logger for test channel
     *
     * @return LoggerInterface
     */
    public function logger()
    {
        return Log::channel('test');
    }
}
