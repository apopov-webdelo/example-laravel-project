<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/20/18
 * Time: 17:06
 */

namespace Tests\Feature\Api;

use App\Makers\DealMaker;
use App\Models\Deal\Deal;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use Tests\TestApi\Traits\BalanceHelpers;

trait Dealable
{
    use Adable, BalanceHelpers;

    /**
     * @var User
     */
    protected $dealAuthor;

    /**
     * @var Deal
     */
    protected $deal;

    /**
     * Store deal with needed status
     *
     * @param int|null $statusId
     *
     * @return $this
     *
     * @throws \Exception
     */
    protected function storeDeal(int $statusId = null)
    {
        $this->storeAd();

        $dealMaker = DealMaker::init([
            'ad_id' => $this->ad->id,
        ]);

        if ($statusId) {
            $dealMaker->status($statusId);
        }

        $this->deal       = $dealMaker->create();
        $this->dealAuthor = $this->deal->author;

        $crypto = $this->ad->cryptoCurrency;
        $this->addBalance($this->ad->author, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);
        $this->addBalance($this->dealAuthor, $crypto, CryptoCurrencyConstants::ONE_BTC * 10);
        $this->addEscrow($this->ad->author, $crypto, $this->deal->crypto_amount);
        $this->addEscrow($this->dealAuthor, $crypto, $this->deal->crypto_amount);

        return $this;
    }
}
