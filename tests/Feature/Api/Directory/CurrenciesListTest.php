<?php

namespace Tests\Feature\Api\Directory;

use App\Http\Resources\Client\Directory\Currency\CurrencyCollection;
use App\Repositories\Directory\CurrencyRepo;
use Tests\TestCase;

class CurrenciesListTest extends TestCase
{
    const ROUTE = 'directory.currencies';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test if user can access and view Currencies List
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertCurrenciesListTestValidResponse()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE
        );
        
        $this->asserts($response);
    }

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(CurrencyCollection::class, [
                'resource' => app(CurrencyRepo::class)->take()->paginate(25)
            ]);
    }
}
