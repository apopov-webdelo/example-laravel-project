<?php

namespace Tests\Feature\Api\Directory;

use App\Models\Directory\CryptoCurrency;
use Illuminate\Http\Response;
use Tests\TestCase;

class FullDirectoryControllerTest extends TestCase
{
    protected $route = 'directory.all.json';

    protected function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function testStruture()
    {
        $response = $this->json('GET', route($this->route));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'crypto_currencies',
            'countries',
        ]);
    }

    /** @test */
    public function testCryptoData()
    {
        $crypto = factory(CryptoCurrency::class)->create();

        $response = $this->json('GET', route($this->route));

        $response->assertJsonFragment([
            'value'       => $crypto->id,
            'label'       => $crypto->getTitle(),
            'short_title' => $crypto->getCode(),
            'accuracy'    => $crypto->getAccuracy(),
        ]);
    }

    /** @test */
    public function testCountryStructure()
    {
        $response = $this->json('GET', route($this->route));

        $response->assertJsonStructure([
            'countries' => [
                [
                    'country'         => [
                        'label',
                        'value',
                        'short_title',
                    ],
                    'currency'        => [
                        'label',
                        'value',
                        'short_title',
                        'accuracy',
                    ],
                    'currencies'      => [
                        [
                            'label',
                            'value',
                            'short_title',
                            'accuracy',
                        ],
                    ],
                    'payment_systems' => [
                        [
                            'label',
                            'value',
                            'currencies',
                        ],
                    ],
                    'banks'           => [
                        [
                            'label',
                            'value',
                            'icon',
                            'currencies',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
