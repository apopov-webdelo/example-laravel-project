<?php

namespace Tests\Feature\Api\Directory;

use App\Http\Resources\Client\Directory\Country\CountryCollection;
use App\Repositories\Directory\CountryRepo;
use Tests\TestCase;

class CountriesListTest extends TestCase
{
    const ROUTE = 'directory.countries';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test if user can access and view Countries List
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertCountriesListTestValidResponse()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE
        );
        
        $this->asserts($response);
    }

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(CountryCollection::class, [
            'resource' => app(CountryRepo::class)->take()->paginate(25)
        ]);
    }
}
