<?php

namespace Tests\Feature\Api\Directory;

use App\Http\Resources\Client\Directory\PaymentSystem\PaymentSystemCollection;
use App\Repositories\Directory\PaymentSystemRepo;
use Tests\TestCase;

class PaymentSystemListTest extends TestCase
{
    const ROUTE = 'directory.payment-systems';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test if user can access and view Payment Systems List
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertPaymentSystemListTestValidResponse()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE
        );
        
        $this->asserts($response);
    }

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(PaymentSystemCollection::class, [
                'resource' => app(PaymentSystemRepo::class)->take()->paginate(25)
            ]);
    }
}
