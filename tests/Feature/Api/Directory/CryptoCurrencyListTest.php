<?php

namespace Tests\Feature\Api\Directory;

use App\Http\Resources\Client\Directory\CryptoCurrency\CryptoCurrencyCollection;
use App\Repositories\Directory\CryptoCurrencyRepo;
use Tests\TestCase;

class CryptoCurrenciesListTest extends TestCase
{
    const ROUTE = 'directory.crypto-currencies';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test if user can access and view Crypto Currencies List
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertCryptoCurrenciesListTestValidResponse()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE
        );
        
        $this->asserts($response);
    }

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(CryptoCurrencyCollection::class, [
                'resource' => app(CryptoCurrencyRepo::class)->take()->paginate(25)
            ]);
    }
}
