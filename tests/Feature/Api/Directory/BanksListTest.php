<?php

namespace Tests\Feature\Api\Directory;

use App\Http\Resources\Client\Directory\Bank\BankCollection;
use App\Repositories\Directory\BankRepo;
use Tests\TestCase;

class BanksListTest extends TestCase
{
    const ROUTE = 'directory.banks';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test if user can access and view Bank List
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertBanksListTestValidResponse()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE
        );
        
        $this->asserts($response);
    }

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(BankCollection::class, [
            'resource' => app(BankRepo::class)->take()->paginate(25)
        ]);
    }
}
