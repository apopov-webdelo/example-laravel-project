<?php

namespace Tests\Feature\Api\Users;

use App\Contracts\Statistics\Ads\AdStatisticsContract;
use App\Http\Resources\Client\Stats\AdStatsResource;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Models\Deal\DealStatusConstants;
use App\Models\User\User;
use Carbon\Carbon;
use Tests\TestCase;

class AdStatsTest extends TestCase
{
    const ROUTE = 'stats.ad';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    const DEALS_TOTAL = 5;
    const PERIOD_TOTAL = 5;
    const PERIOD_START = 2;
    const PERIOD_END = 4;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User|null $dealUser
     */
    protected $dealUser;

    /**
     * Ads created for user
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Deals created for dealUser
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Array for Deals dates period
     *
     * @var array
     */
    protected $arrayPeriod = [];

    /**
     * Fixture for test
     *
     * @throws \Exception
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
        
        // Create a dealUser
        $this->dealUser = $this->createUser('dealUser');

        // Create 5 Ads for User
        $this->storeAds($this->user, self::DEALS_TOTAL);
        
        // Currents Time
        $timeNow = now();

        // Create 5 Deals for each Ad by deal author
        for ($i = 0; $i < self::PERIOD_TOTAL; $i++) {
            $this->arrayPeriod[$i+1] = $timeNow->copy()->addDays($i);

            $this->storeDeals(
                $this->dealUser,
                [
                    'created_at' => $this->arrayPeriod[$i+1],
                ]
            );
        }
    }

    /**
     * Test Response If authenticated User can view Ad Stats
     */
    public function testUserReceiveValidAdStats()
    {
        // Get first Ad to view statistic
        $adForStats = array_shift($this->ads);

        // Test for User without filter
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $adForStats->id
        );

        $adStatsResource = app(AdStatsResource::class, [
            'resource' => App(
                AdStatisticsContract::class,
                [
                    'ad'   => $adForStats,
                    'from' => null,
                    'to'   => null,
                ]
            )
        ]);

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                "deals"                 => $adStatsResource->resource->deals(),
                "crypto_turnover"       => currencyFromCoins(
                    $adStatsResource->resource->cryptoTurnover(),
                    $adStatsResource->resource->getAd()->cryptoCurrency
                ),
                "fiat_turnover"         => currencyFromCoins(
                    $adStatsResource->resource->fiatTurnover(),
                    $adStatsResource->resource->getAd()->currency
                ),
                "weighted_average_price"=> $adStatsResource->resource->weightedAveragePrice(),
            ]
        );
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int $count
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init(['author_id' => $user->id])
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array     $data
     *
     * @return void
     * @throws \Exception
     */
    protected function storeDeals(User $user = null, $data = [])
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $data['author_id'] = $user->id;
            $this->deals[] = DealMaker::init($data)
                ->author($user)
                ->status(DealStatusConstants::FINISHED)
                ->create();
        }
    }

    /**
     * Return valid Json structure
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(AdStatsResource::class, [
            'resource' => App(
                AdStatisticsContract::class,
                [
                    'ad'   => AdMaker::init()->create(),
                    'from' => null,
                    'to'   => null,
                ]
            )
        ]);
    }

    /**
     * Test Response If authenticated User can view Ad Stats
     */
    public function testUserReceiveOnlyHisAdStats()
    {
        // Create a anotherUser
        $anotherUser = $this->createUser('anotherUser');

        // Get first Ad to view statistic
        $adForStats = array_shift($this->ads);

        $response = $this->response(
            $anotherUser,
            self::ROUTE,
            self::METHOD,
            [],
            $adForStats->id
        );

        // 403 Forbidden
        $response->assertStatus(403);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        // Get first Ad to view statistic
        $adForStats = array_shift($this->ads);

        // Test if Unauthenticated guest has no access
        $response = $this->json(self::METHOD, route(self::ROUTE, $adForStats->id));

        $this->log($response, self::METHOD, self::ROUTE);

        // 401 Unauthorized
        $response->assertStatus(401);
    }

    /**
     * Test Response If authenticated User can view Ad Stats With Filter
     */
    public function testUserReceiveValidAdStatsWithFilter()
    {

        // Get first Ad to view statistic
        $adForStats = array_shift($this->ads);

        // Test for User without filter
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'from' => $this->arrayPeriod[self::PERIOD_START],
                'to'   => $this->arrayPeriod[self::PERIOD_END],
            ],
            $adForStats->id
        );
        
        $adStatsResource = app(AdStatsResource::class, [
            'resource' => App(
                AdStatisticsContract::class,
                [
                    'ad'   => $adForStats,
                    'from' => new Carbon($this->arrayPeriod[self::PERIOD_START]),
                    'to'   => new Carbon($this->arrayPeriod[self::PERIOD_END]),
                ]
            )
        ]);

        // Test if response match JSON
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                "deals"                 => $adStatsResource->resource->deals(),
                "crypto_turnover"       => currencyFromCoins(
                    $adStatsResource->resource->cryptoTurnover(),
                    $adStatsResource->resource->getAd()->cryptoCurrency
                ),
                "fiat_turnover"         => currencyFromCoins(
                    $adStatsResource->resource->fiatTurnover(),
                    $adStatsResource->resource->getAd()->currency
                ),
                "weighted_average_price"=> $adStatsResource->resource->weightedAveragePrice(),
            ]
        );

        // Test if deals count in response match deals count in period
        $this->assertTrue(
            $adStatsResource->resource->deals() == (self::PERIOD_END - self::PERIOD_START + 1),
            'Deals number do not match period! (Deals in response: ['
            . $adStatsResource->resource->deals()
            . ']; Deals in Period:['
            . (self::PERIOD_END - self::PERIOD_START + 1)
            . '])'
        );
    }
}
