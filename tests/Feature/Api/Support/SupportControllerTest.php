<?php

namespace Tests\Feature\Api\Support;

use App\Notifications\Support\ApiAccessRequestNotification;
use Illuminate\Http\Response;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SupportControllerTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function testAcceptsCorrectApiAccessRequests()
    {
        Notification::fake();

        $route = route('support.api.access.request');

        $req = [
            'email' => 'test@test.ru',
            'name'  => 'some name',
            'login' => 'some login',
            'text'  => 'some text',
        ];

        $response = $this->json('POST', $route, $req);

        // assert
        $response->assertStatus(Response::HTTP_OK);

        Notification::assertSentTo(
            new AnonymousNotifiable,
            ApiAccessRequestNotification::class,
            function (ApiAccessRequestNotification $notification, $channels) use ($req) {
                $data = $notification->toArray();

                return $data === $req;
            }
        );
    }
}
