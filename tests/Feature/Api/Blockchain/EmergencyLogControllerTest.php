<?php

namespace Tests\Feature\Api\Blockchain;

use App\Facades\EmergencyAlert;
use App\Jobs\Utils\LogToChannel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class EmergencyLogControllerTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function testEmergencyTelegramAccepted()
    {
        // pre assert
        EmergencyAlert::shouldReceive('channel->send')->once();

        // act
        $timestamp = time();
        $response = $this->json(
            'POST',
            route('blockchain.emergency.telegram'),
            [
                'hash'      => md5($timestamp . '-' . config('app.blockchain.stellar.private_key')),
                'timestamp' => $timestamp,
            ]
        );

        // assert
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function testEmergencyTelegramQueue()
    {
        Queue::fake();

        // act
        $timestamp = time();
        $this->json(
            'POST',
            route('blockchain.emergency.telegram'),
            [
                'hash'      => md5($timestamp . '-' . config('app.blockchain.stellar.private_key')),
                'timestamp' => $timestamp,
            ]
        );

        // assert
        Queue::assertPushed(LogToChannel::class);
    }
}
