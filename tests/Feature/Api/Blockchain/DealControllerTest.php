<?php

namespace Tests\Feature\Api\Blockchain;

use App\Models\Deal\DealStatusConstants;
use Illuminate\Http\Response;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestApi\Traits\DealHelpers;
use Tests\TestCase;

class DealControllerTest extends TestCase
{
    use AdHelpers, DealHelpers;

    protected function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function testVerifiedFromRoute()
    {
        // seller
        $user1 = $this->registerUser();
        $this->makeAd($user1, ['is_sale' => true]);
        $ad = $this->lastAd($user1);

        // buyer
        $user2 = $this->registerUser();
        $this->makeDeal($user2, $ad);
        $deal = $this->lastDeal($user2);
        $deal->statuses()->attach(DealStatusConstants::VERIFICATION);

        // sub assert
        $this->assertEquals(DealStatusConstants::VERIFICATION, $deal->status->id);

        // stellar from
        $timestamp = time();
        $response = $this->json(
            'POST',
            route('blockchain.deal.verified', ['deal' => $deal->id]),
            [
                'hash'      => md5($deal->id . '-' . $timestamp . '-' . config('app.blockchain.stellar.private_key')),
                'timestamp' => $timestamp,
            ]
        );

        // asserts
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals(DealStatusConstants::VERIFIED, $deal->status->id);
    }
}
