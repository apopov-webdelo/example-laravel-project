<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/21/18
 * Time: 06:42
 */

namespace Tests\Feature\Api;

use App\Makers\AdMaker;
use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\User\User;

trait Adable
{
    /**
     * @var User
     */
    protected $adAuthor;

    /**
     * @var User
     */
    protected $notAdAuthor;

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * Store ad to private fields
     *
     * @param bool $active
     * @return self
     */
    protected function storeAd(bool $active = true)
    {
        $this->notAdAuthor = UserMaker::init()->create();

        /** @var AdMaker $maker */
        $maker = AdMaker::init();

        $active ? $maker->active() : $maker->inactive();

        $this->ad = $maker->create();

        $this->adAuthor = $this->ad->author;

        return $this;
    }
}
