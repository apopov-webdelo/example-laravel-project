<?php

namespace Tests\Feature\Api\Ads;

use App\Events\Ad\AdPlaced;
use App\Makers\User\UserMaker;
use App\Models\User\User;
use Event;
use Tests\TestCase;

class UserBlockedTest extends TestCase
{
    const ROUTE = 'ad.store';
    const METHOD = 'post';
    const HTTP_STATUS = 403;

    const PAYMENT_SYSTEM_QIWI = 1;
    const COUNTRY_RUSSIA      = 1;
    const CURRENCY_RUB        = 1;
    const BANK_SYSTEM_ID      = 1;
    const CRYPTO_CURRENCY_BTC = 1;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Data for testing Ad
     *
     * @var array
     */
    protected $adData = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create One Ad for user
        $this->adData = [
            'payment_system_id' => self::PAYMENT_SYSTEM_QIWI,
            'notes' => 'Test ad store',
            'country_id'  => self::COUNTRY_RUSSIA,
            'is_sale'     => true,
            'currency_id' => self::CURRENCY_RUB,
            'crypto_currency_id' => self::CRYPTO_CURRENCY_BTC,
            'balance' => 100,
            'price' => 1000,
            'min' => 1,
            'max' => 2000,
            'turnover' => 0,
            'min_deal_finished_count' => 0,
            'deal_cancellation_max_percent' => 50,
            'time' => 60,
            'liquidity_required' => true,
            'email_confirm_required' => true,
            'phone_confirm_required' => true,
            'trust_required' => true,
            'tor_denied' => true,
            'review_rate' => 50,
            'is_active' => true,
            'conditions' => 'Test conditions',
            'banks' => [1,3,5],
        ];
    }

    /**
     * Test Response Blocked User cannot create Ads
     */
    public function testUserBlocked()
    {
        // Create a User with Blocked Status
        $this->user = UserMaker::init([])->state(UserMaker::STATE_INACTIVE)->create();

        // Test for user
        $this->assertResponse();
    }

    /**
     * Test response
     *
     * @return void
     */
    protected function assertResponse()
    {
        Event::fake();

        $response = $this->actingAs($this->user, 'api')
            ->json(self::METHOD, route(self::ROUTE), $this->adData);
            
        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(self::HTTP_STATUS);
        
        Event::assertDispatched(AdPlaced::class, 0);
    }

    /**
     * Test Response for Ad store if User has status TradeBlocked
     */
    public function testUserTradeBlocked()
    {
        // Create a User with Trade Blocked Status
        $this->user = UserMaker::init([])->tradeLock()->create();

        // Test for user
        $this->assertResponse();
    }
}
