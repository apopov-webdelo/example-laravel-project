<?php

namespace Tests\Feature\Api\Ads;

use App\Events\Ad\AdPriceChanged;
use App\Events\Ad\AdUpdated;
use \Illuminate\Foundation\Testing\TestResponse;
use Event;

class UpdatePriceTest extends AdTest
{
    /**
     * Route for response
     *
     * @var string
     */
    protected $route = 'ad.update_price';

    /**
     * Payload for request
     *
     * @var array
     */
    protected $payload = [
        'price' => 500,
    ];

    /**
     * Event must be fired
     *
     * @var array
     */
    protected $event = [
        AdPriceChanged::class,
        AdUpdated::class,
    ];

    /**
     * @throws \Exception
     */
    public function testUpdatePriceForActiveAdByAuthor()
    {
        $this->assertAdValidResponse(200, true);
    }

    /**
     * Return JSON for ad according transmitted state
     *
     * @param bool $active
     * @return array
     */
    protected function responseJson(bool $active = true)
    {
        return [
            'data' => [
                'id'     => $this->ad->id,
                'author' => [
                    'id' => $this->adAuthor->id,
                ],
                'is_active' => !$active,
                'price'  => $this->payload['price'],
            ]
        ];
    }

    /**
     * @throws \Exception
     */
    public function testUpdatePriceForActiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, true, false);
    }

    /**
     * @throws \Exception
     */
    public function testUpdatePriceForInactiveAdByAuthor()
    {
        $this->assertAdValidResponse(200, false);
    }

    /**
     * @throws \Exception
     */
    public function testUpdatePriceForInactiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, false, false);
    }
}
