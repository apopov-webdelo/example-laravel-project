<?php

namespace Tests\Feature\Api\Ads;

use App\Events\Ad\AdActivated;
use App\Events\Ad\AdDeactivated;
use Event;

class DeactivateTest extends AdTest
{
    /**
     * Route for response
     *
     * @var string
     */
    protected $route = 'ad.deactivate';

    /**
     * Event must be fired
     *
     * @var string
     */
    protected $event = AdDeactivated::class;

    /**
     * Event NOT needed for firing
     *
     * @var string
     */
    protected $failEvent = AdActivated::class;

    /**
     * @throws \Exception
     */
    public function testDeactivationForActiveAdByAuthor()
    {
        $this->assertAdValidResponse(200, true);
    }

    /**
     * @throws \Exception
     */
    public function testDeactivationForActiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, true, false);
    }

    /**
     * @throws \Exception
     */
    public function testDeactivationForInactiveAdByAuthor()
    {
        $this->assertAdFailResponse(422, false, true);
    }

    /**
     * @throws \Exception
     */
    public function testDeactivationForInactiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, false, false);
    }
}
