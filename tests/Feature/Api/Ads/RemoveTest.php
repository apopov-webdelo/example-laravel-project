<?php

namespace Tests\Feature\Api\Ads;

use App\Events\Ad\AdActivated;
use App\Events\Ad\AdDeactivated;
use App\Events\Ad\AdDeleted;
use Event;

class RemoveTest extends AdTest
{
    /**
     * Route for response
     *
     * @var string
     */
    protected $route = 'ad.destroy';

    /**
     * REST response type
     *
     * @var string
     */
    protected $request = 'delete';

    /**
     * Event must be fired
     *
     * @var string
     */
    protected $event = AdDeleted::class;

    /**
     * Return JSON for ad according transmitted state
     *
     * @param bool $active
     * @return array
     */
    protected function responseJson(bool $active = true)
    {
        return [
            'data' => [
                'id'     => $this->ad->id,
                'author' => [
                    'id' => $this->adAuthor->id,
                ],
                'is_active' => false,
            ]
        ];
    }

    /**
     * @throws \Exception
     */
    public function testRemovingForActiveAdByAuthor()
    {
        $this->assertAdValidResponse(200, true);
    }

    /**
     * Test is response is valid
     *
     * @param int $httpStatus
     * @param bool $activeAd
     * @param bool $isAuthor
     * @param int $eventTimes
     * @param array|null $payload
     *
     * @throws \Exception
     */
    protected function assertAdValidResponse(
        int $httpStatus = 200,
        bool $activeAd = false,
        bool $isAuthor = true,
        int $eventTimes = 1,
        array $payload = null
    ) {
        parent::assertAdValidResponse($httpStatus, $activeAd, $isAuthor, $eventTimes, $payload);
        $this->ad->refresh();
        $this->assertTrue($this->ad->trashed());
    }

    /**
     * @throws \Exception
     */
    public function testRemovingForActiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, true, false);
    }

    /**
     * Test is response is failed
     *
     * @param int $httpStatus
     * @param bool $activeAd
     * @param bool $isAuthor
     * @param int $eventTimes
     * @param array|null $payload
     *
     * @throws \Exception
     */
    protected function assertAdFailResponse(
        int $httpStatus = 422,
        bool $activeAd = true,
        bool $isAuthor = true,
        int $eventTimes = 0,
        array $payload = null
    ) {
        parent::assertAdFailResponse($httpStatus, $activeAd, $isAuthor, $eventTimes, $payload);
        $this->ad->refresh();
        $this->assertFalse($this->ad->trashed());
    }

    /**
     * @throws \Exception
     */
    public function testRemovingForInactiveAdByAuthor()
    {
        $this->assertAdValidResponse(200, false);
    }

    /**
     * @throws \Exception
     */
    public function testRemovingForInactiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, false, false);
    }
}
