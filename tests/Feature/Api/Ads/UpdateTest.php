<?php

namespace Tests\Feature\Api\Ads;

use App\Makers\AdMaker;
use App\Makers\User\UserMaker;
use App\Models\Ad\Ad;
use App\Models\User\User;
use Event;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    /**
     * Route for response
     *
     * @var string
     */
    protected $route = 'ad.update';

    /**
     * REST response type
     *
     * @var string
     */
    protected $requestType = 'put';

    const PAYMENT_SYSTEM_QIWI = 1;
    const COUNTRY_RUSSIA      = 1;
    const CURRENCY_RUB        = 1;
    const CRYPTO_CURRENCY_BTC = 1;
    const UPDATED_NOTE        = 'That note must be updated';

    /**
     * Test for adding a row to ads table.
     *
     * @return void
     */
    public function testUpdate()
    {
        $response = $this->updateAd([
            'min' => 10000,
            'max' => 20000,
        ]);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'data' => [
                         'id',
                         'author' => [
                             'id',
                             'login',
                             'email',
                             'last_seen',
                             'user_locked_at',
                             'user_locked_to',
                             'trade_locked_at',
                             'trade_locked_to',
                             'one_signal_player_id',
                             'one_signal_player_id',
                             'telegram_id',
                         ],
                         'payment_system_id',
                         'country_id',
                         'is_sale',
                         'currency_id',
                         'crypto_currency_id',
                         'price',
                         'min',
                         'max',
                         'is_active',
                         'created_at'
                     ],
                 ]);

        $updatedAd = Ad::find($response->json()['data']['id']);
        $this->assertTrue($updatedAd->notes === self::UPDATED_NOTE);
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function updateAd(array $data = [])
    {
        Event::fake();

        /** @var Ad $ad */
        $ad = $this->getAd();

        $response = $this->actingAs($ad->author, 'api')
                         ->json(
                             $this->requestType,
                             route($this->route, ['ad' => $ad->id]),
                             array_merge($ad->toArray(), $data)
                         );

        $this->log($response, $this->requestType, $this->route);

        return $response;
    }

    /**
     * Create new Ad using maker
     *
     * @return Ad
     */
    protected function getAd() : Ad
    {
        return AdMaker::init([
            'author_id' => $this->getUser()->id,
        ])->notes(self::UPDATED_NOTE)->create();
    }

    /**
     * Create new User using maker
     *
     * @return User
     */
    protected function getUser() : User
    {
        return UserMaker::init()->active()->tradeAvailable()->login('test@ersec.net')->create();
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testMinMaxPeriod()
    {
        $this->updateAd([
            'min' => 200,
            'max' => 199,
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'min',
                    'max'
                ],
            ]);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testMinPrice()
    {
        $this->updateAd([
            'price' => 0
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'price'
                ],
            ]);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testTimePeriod()
    {
        $this->updateAd([
            'time' => 10
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'time'
                ],
            ]);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testReviewRatePeriod()
    {
        $this->updateAd([
            'review_rate' => 130
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'review_rate'
                ],
            ]);
    }
}
