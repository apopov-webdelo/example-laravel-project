<?php

namespace Tests\Feature\Api\Ads;

use App\Events\Ad\AdPlaced;
use App\Models\User\User;
use Event;
use Tests\Feature\Api\Authorable;
use Tests\TestCase;

class StoreTest extends TestCase
{
    use Authorable;

    /**
     * REST response type
     *
     * @var string
     */
    protected $requestType = 'post';

    /**
     * Route for response
     *
     * @var string
     */
    protected $route = 'ad.store';

    const PAYMENT_SYSTEM_QIWI = 1;
    const COUNTRY_RUSSIA      = 1;
    const CURRENCY_RUB        = 1;
    const CRYPTO_CURRENCY_BTC = 1;

    /**
     * Test for adding a row to ads table.
     *
     * @return void
     */
    public function testStore()
    {
        Event::fake();

        $response = $this->storeAd();

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'payment_system_id',
                    'country_id',
                    'is_sale',
                    'currency_id',
                    'author_id',
                    'crypto_currency_id',
                    'price',
                    'min',
                    'max',
                    'turnover',
                    'min_deal_finished_count',
                    'deal_cancellation_max_percent',
                    'time',
                    'liquidity_required',
                    'email_confirm_required',
                    'phone_confirm_required',
                    'trust_required',
                    'tor_denied',
                    'review_rate',
                    'conditions',
                    'conditions',
                    'original_max',
                    'deals_count',
                    'crypto_turnover',
                    'fiat_turnover',
                    'average_rate',
                    'is_active',
                    'created_at'
                ],
            ])
            ->assertJson([
                'data' => [
                    'author_id' => $this->author->id,
                ]
            ]);

        Event::assertDispatched(AdPlaced::class, 1);
    }

    /**
     * @param array $data
     * @param User|null $author
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function storeAd(array $data = [], User $author = null)
    {
        if ($author instanceof User) {
            $this->author = $author;
        } else {
            $this->setAuthor();
        }

        $data    = collect($data);
        $payload = collect([
            'payment_system_id' => self::PAYMENT_SYSTEM_QIWI,
            'notes' => 'Test ad store',
            'country_id'  => self::COUNTRY_RUSSIA,
            'is_sale'     => true,
            'currency_id' => self::CURRENCY_RUB,
            'crypto_currency_id' => self::CRYPTO_CURRENCY_BTC,
            'balance' => 100,
            'price' => 1000,
            'min' => 1,
            'max' => 2000,
            'turnover' => 0,
            'min_deal_finished_count' => 0,
            'deal_cancellation_max_percent' => 50,
            'time' => 60,
            'liquidity_required' => true,
            'email_confirm_required' => true,
            'phone_confirm_required' => true,
            'trust_required' => true,
            'tor_denied' => true,
            'review_rate' => 50,
            'is_active' => true,
            'conditions' => 'Test conditions',
            'banks' => [1,3,5],
        ]);

        if ($data->isNotEmpty()) {
            $payload = $payload->merge($data);
        }

        $response = $this->actingAs($this->author, 'api')
                         ->json($this->requestType, route($this->route), $payload->toArray());

        $this->log($response, $this->requestType, $this->route);

        return $response;
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testDuplicate()
    {
        Event::fake();

        $this->storeAd();
        $this->storeAd([], $this->author)->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'payment_system_id',
                    'is_sale',
                    'currency_id',
                    'crypto_currency_id',
                ],
            ]);

        Event::assertDispatched(AdPlaced::class, 1);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testMinMaxPeriod()
    {
        Event::fake();

        $this->storeAd([
            'min' => 200,
            'max' => 199,
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'min',
                    'max'
                ],
            ]);

        Event::assertNotDispatched(AdPlaced::class);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testMinPrice()
    {
        Event::fake();

        $this->storeAd([
            'price' => 0
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'price'
                ],
            ]);

        Event::assertNotDispatched(AdPlaced::class);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testTimePeriod()
    {
        Event::fake();

        $this->storeAd([
            'time' => 10
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'time'
                ],
            ]);

        Event::assertNotDispatched(AdPlaced::class);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testReviewRatePeriod()
    {
        Event::fake();

        $this->storeAd([
            'review_rate' => 130
        ])->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'review_rate'
                ],
            ]);

        Event::assertNotDispatched(AdPlaced::class);
    }

    /**
     * Test for duplicate adding row to ads table
     *
     * @return void
     */
    public function testCountryNotExists()
    {
        Event::fake();

        $this->storeAd([
            'country_id' => -1
        ])->assertStatus(404);

        Event::assertNotDispatched(AdPlaced::class);
    }
}
