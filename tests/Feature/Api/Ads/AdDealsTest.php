<?php

namespace Tests\Feature\Api\Ads;

use App\Http\Resources\Client\Market\DealCollection;
use App\Makers\DealMaker;
use App\Repositories\Deal\DealRepo;
use App\Utils\Json;
use Event;
use Tests\Feature\Api\Adable;

class AdDealsTest extends AdTest
{
    use Adable {
        storeAd as adableStoreAd;
    }

    /**
     * Deals created for testing Ad model
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Route for deal status response
     *
     * @var string
     */
    protected $route = 'ad.deals';

    /**
     * REST response type
     *
     * @var string
     */
    protected $request = 'get';

    /**
     * @param bool $active
     * @return AdDealsTest
     */
    protected function storeAd(bool $active = true)
    {
        $this->adableStoreAd($active);
        for ($i=1; $i<=15; $i++) {
            $this->deals[] = DealMaker::init([
                'ad_id' => $this->ad->id,
            ])->create();
        };

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function testAdDealsForActiveAdByAuthor()
    {
        $this->storeAd(true);
        $response = $this->actingAs($this->adAuthor, 'api')->get(route('ad.deals', ['ad'=>$this->ad]));
        $response->assertJsonStructure($this->responseJsonStructure());
    }

    /**
     * Return valid structure for response
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return Json::structure($this->getCollectionResource());
    }

    protected function getCollectionResource()
    {
        $deals = app(DealRepo::class)->filterByAd($this->ad)->take()->paginate(10);
        return app(DealCollection::class, ['resource' => $deals]);
    }

    /**
     * Return JSON for ad according transmitted state
     *
     * @param bool $active
     * @return array
     */
    protected function responseJson(bool $active = true)
    {
        //TODO: Complete assert by json content
        return $this->getCollectionResource();
    }

    /**
     * @throws \Exception
     */
    public function testAdDealsForActiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, true, false);
    }

    /**
     * @throws \Exception
     */
    public function testAdDealsForInactiveAdByAuthor()
    {
        $this->assertAdValidResponse(200, false);
    }

    /**
     * @throws \Exception
     */
    public function testAdDealsForInactiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, false, false);
    }
}
