<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/23/18
 * Time: 10:35
 */

namespace Tests\Feature\Api\Ads;

use App\Http\Resources\Client\Ad\AdResource;
use App\Makers\AdMaker;
use App\Models\User\User;
use App\Utils\Json;
use Event;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Feature\Api\Adable;
use Tests\TestCase;

abstract class AdTest extends TestCase
{
    use Adable;

    /**
     * Route for response
     *
     * @var string
     */
    protected $route;

    /**
     * REST response type
     *
     * @var string
     */
    protected $request = 'put';

    /**
     * Payload for request
     *
     * @var array
     */
    protected $payload = [];

    /**
     * Event must be fired
     *
     * @var string|array
     */
    protected $event;

    /**
     * Event NOT needed for firing
     *
     * @var string
     */
    protected $failEvent;

    /**
     * Test is response is valid
     *
     * @param int $httpStatus
     * @param bool $activeAd
     * @param bool $isAuthor
     * @param int $eventTimes
     * @param array|null $payload
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     *
     * @throws \Exception
     */
    protected function assertAdValidResponse(
        int $httpStatus = 200,
        bool $activeAd = false,
        bool $isAuthor = true,
        int $eventTimes = 1,
        array $payload = null
    ) {
        Event::fake();

        $this->storeAd($activeAd);

        $user = $isAuthor ? $this->adAuthor : $this->notAdAuthor;
        $payload = $payload ?? $this->payload;

        $response = $this->getResponse($this->route, $this->request, $user, $payload)
                         ->assertStatus($httpStatus)
                         ->assertJsonStructure($this->responseJsonStructure());
        //TODO: Complete assert by json content
//                         ->assertJson($this->responseJson(!$activeAd));

        $this->assertEvent($eventTimes);
        if (isset($this->failEvent)) {
            $this->assertEvent(0, $this->failEvent);
        }

        return $response;
    }

    /**
     * Send request by route and return response
     *
     * @param string $route
     * @param string $requestType
     * @param User $user
     * @param array $payload
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     *
     * @throws \Exception
     */
    protected function getResponse(
        string $route,
        string $requestType = 'put',
        User $user = null,
        array $payload = []
    ) {
        if (empty($route)) {
            throw new \Exception('Route is empty for response!');
        }

        $payload = $payload instanceof Collection
            ? $payload->toArray()
            : $payload;

        $user = $user ?? $this->adAuthor;

        /** @var TestResponse $response */
        $response = $this->actingAs($user, 'api')
                         ->json($requestType, route($route, ['ad' => $this->ad->id]), $payload);

        $this->log($response, $requestType, $route);

        return $response;
    }

    /**
     * Return valid structure for response
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return Json::structure(
            app(AdResource::class, ['resource' => $this->ad])
        );
    }

    /**
     * Return valid json for response
     *
     * @param bool $active
     * @return array
     */
    protected function responseJson(bool $active = true)
    {
        return [
            'data' => [
                'id'     => $this->ad->id,
                'author' => [
                    'id' => $this->adAuthor->id,
                ],
                'is_active' => $active,
                'price'  => $this->ad->price,
            ]
        ];
    }

    /**
     * Test is response is failed
     *
     * @param int $httpStatus
     * @param bool $activeAd
     * @param bool $isAuthor
     * @param int $eventTimes
     * @param array|null $payload
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     *
     * @throws \Exception
     */
    protected function assertAdFailResponse(
        int $httpStatus = 422,
        bool $activeAd = true,
        bool $isAuthor = true,
        int $eventTimes = 0,
        array $payload = null
    ) {
        Event::fake();

        $this->storeAd($activeAd);

        $user = $isAuthor ? $this->adAuthor : $this->notAdAuthor;
        $payload = $payload ?? $this->payload;

        $response = $this->getResponse($this->route, $this->request, $user, $payload)
                         ->assertStatus($httpStatus);

        $this->assertEvent($eventTimes);
        if (isset($this->failEvent)) {
            $this->assertEvent(0, $this->failEvent);
        }

        return $response;
    }

    /**
     * Check if event was fired
     *
     * @param int $times
     * @param string|array|null $event
     * @return $this
     *
     * @throws \Exception
     */
    protected function assertEvent(int $times = 1, $event = null)
    {
        $events = $event ?? $this->event ?? [];

        if (is_string($events)) {
            $events = [$event];
        }

        foreach ($events as $event) {
            if ($event) {
                if (class_exists($event)) {
                    Event::assertDispatched($event, $times);
                } else {
                    throw new \Exception('Event class "'.$event.'" not exists!');
                }
            }
        }

        return $this;
    }
}