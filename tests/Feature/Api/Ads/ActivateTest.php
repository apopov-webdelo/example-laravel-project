<?php

namespace Tests\Feature\Api\Ads;

use App\Events\Ad\AdActivated;
use App\Events\Ad\AdDeactivated;
use Event;

class ActivateTest extends AdTest
{
    /**
     * Route for response
     *
     * @var string
     */
    protected $route = 'ad.activate';

    /**
     * Event must be fired
     *
     * @var string
     */
    protected $event = AdActivated::class;

    /**
     * Event NOT needed for firing
     *
     * @var string
     */
    protected $failEvent = AdDeactivated::class;

    /**
     * @throws \Exception
     */
    public function testActivationForInactiveAdByAuthor()
    {
        $this->assertAdValidResponse();
    }

    /**
     * @throws \Exception
     */
    public function testActivationForInactiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, false, false);
    }

    /**
     * @throws \Exception
     */
    public function testActivationForActiveAdByAuthor()
    {
        $this->assertAdFailResponse(422, true, true);
    }

    /**
     * @throws \Exception
     */
    public function testActivationForActiveAdByAnotherUser()
    {
        $this->assertAdFailResponse(403, true, false);
    }
}
