<?php

namespace Tests\Feature\Api\Auth;

use App\Http\Resources\Client\User\SignInResource;
use App\Models\User\User;
use App\Utils\Json;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthSignOutTest extends TestCase
{
    const ROUTE_SIGNIN      = 'auth.signIn';
    const ROUTE_SIGNOUT     = 'auth.signOut';
    const METHOD            = 'post';
    const HTTP_STATUS       = Response::HTTP_OK;
    const HTTP_STATUS_BAD   = Response::HTTP_UNPROCESSABLE_ENTITY;
    
    const LOGIN             = 'login';
    const PASSWORD          = 'Password1*';

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser(self::LOGIN);

        // Set manual password to user
        $this->user->setPassword(self::PASSWORD);

        // Unset Google2FA
        $this->user->security->google2fa_status = 'deactivated';
        $this->user->security->save();
    }

    /**
     * Test Response If authenticated User can Update Email
     */
    public function testUserSignIn()
    {
        // Get response
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE_SIGNIN, []),
            [
                'login'     => self::LOGIN,
                'password'  => self::PASSWORD,
            ]
        );


        // Get response
        $responseSignOut = $this->response(
            $this->user,
            self::ROUTE_SIGNOUT,
            self::METHOD
        );
        
        $responseSignOut->assertSuccessful();
    }
}
