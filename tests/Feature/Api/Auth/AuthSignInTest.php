<?php

namespace Tests\Feature\Api\Auth;

use App\Http\Resources\Client\User\SignInResource;
use App\Models\User\User;
use App\Utils\Json;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthSignInTest extends TestCase
{
    const ROUTE             = 'auth.signIn';
    const METHOD            = 'post';
    const HTTP_STATUS       = Response::HTTP_OK;
    const HTTP_STATUS_BAD   = Response::HTTP_UNPROCESSABLE_ENTITY;
    
    const LOGIN             = 'login';
    const PASSWORD          = 'Password1*';

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser(self::LOGIN);

        // Set manual password to user
        $this->user->setPassword(self::PASSWORD);

        // Unset Google2FA
        $this->user->security->google2fa_status = 'deactivated';
        $this->user->security->save();
    }

    /**
     * Test Response If authenticated User can Update Email
     */
    public function testUserSignIn()
    {
        // Get response
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE, []),
            [
                'login'     => self::LOGIN,
                'password'  => self::PASSWORD,
            ]
        );

        $responseData = $this->getResponseData($response);

        $responseJsonStructure = $this->responseJsonStructure(
            $this->user,
            $responseData->token
        );

        // Test response
        $this->assertsResponse(
            $response,
            self::HTTP_STATUS,
            $responseJsonStructure,
            [
                'id' => $this->user->id,
            ]
        );
    }

    /**
     * Return valid Json structure
     *
     * @return array
     */
    protected function responseJsonStructure($user, $token)
    {
        return app(SignInResource::class, ['resource' => $user, 'token' => $token]);
    }

    /**
     * Asserts for testing
     *
     * @param TestResponse $response
     * @param int $httpStatus
     * @param array $responseJsonStructure
     * @param array $jsonAdditionalData
     * @return void
     */
    protected function assertsResponse($response, int $httpStatus, $responseJsonStructure = [], $jsonAdditionalData = [])
    {
        $response->assertStatus($httpStatus)
            ->assertJsonStructure(
                Json::structure(
                    $responseJsonStructure
                )
            );

        if (!empty($jsonAdditionalData)) {
            $response->assertJson(
                [
                    'data' => $jsonAdditionalData,
                ]
            );
        }
    }

    /**
     * Provider for testUserWrongLogin: login, password
     */
    public function providerUserWrongLogin()
    {
        return [
            ['', ''],
            [self::LOGIN, ''],
            [self::LOGIN, 'aa'],
            ['aa', 'aa'],
            ['', self::PASSWORD],
        ];
    }

    /**
     * Test Response for wrong Login
     *
     * @dataProvider providerUserWrongLogin
     */
    public function testUserWrongLogin($login, $password)
    {
        // Get response
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE, []),
            [
                'login'    => $login,
                'password' => $password,
            ]
        );

        // Test wrong response status
        $response->assertStatus(self::HTTP_STATUS_BAD);
    }
}
