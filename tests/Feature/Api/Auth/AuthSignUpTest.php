<?php

namespace Tests\Feature\Api\Auth;

use App\Http\Resources\Client\User\SignInResource;
use App\Models\Partnership\Partner;
use App\Models\Partnership\Referral;
use App\Models\User\User;
use App\Services\Partnership\PartnerRegisterService;
use App\Utils\Json;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Event;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthSignUpTest extends TestCase
{
    const ROUTE             = 'auth.signUp';
    const METHOD            = 'post';
    const HTTP_STATUS       = Response::HTTP_OK;
    const HTTP_STATUS_BAD   = Response::HTTP_UNPROCESSABLE_ENTITY;
    
    const LOGIN             = 'loginunittest';
    const LOGIN_WRONG       = 'parent';
    const PASSWORD          = 'Password1*';
    const PASSWORD_WRONG    = 'password';
    const EMAIL             = 'email@email.com';
    const EMAIL_WRONG       = 'email-wrong.com';

    const INT_MAX           = 9223372036854775807;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        Event::fake();

        $this->user = $this->createUser('parent');
    }

    /**
     * Test Response If guest can register
     */
    public function testUserRegistration()
    {
        // Get response
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE, []),
            [
                'login'     => self::LOGIN,
                'email'     => self::EMAIL,
                'password'  => self::PASSWORD,
                'password_confirmation'  => self::PASSWORD,
            ]
        );

        $user = User::query()->where('login', self::LOGIN)->first();

        $responseData = $this->getResponseData($response);

        $responseJsonStructure = $this->responseJsonStructure(
            $user,
            $responseData->token
        );

        // Test response
        $this->assertsResponse(
            $response,
            self::HTTP_STATUS,
            $responseJsonStructure,
            [
                'id' => $user->id,
            ]
        );

        // Test if database was changed
        $this->assertTrue($user->email == self::EMAIL);
    }

    /**
     * Return valid Json structure
     *
     * @return array
     */
    protected function responseJsonStructure($user, $token)
    {
        return app(SignInResource::class, ['resource' => $user, 'token' => $token]);
    }

    /**
     * Asserts for testing
     *
     * @param TestResponse $response
     * @param int $httpStatus
     * @param array $responseJsonStructure
     * @param array $jsonAdditionalData
     * @return void
     */
    protected function assertsResponse($response, int $httpStatus, $responseJsonStructure = [], $jsonAdditionalData = [])
    {
        $response->assertStatus($httpStatus)
            ->assertJsonStructure(
                Json::structure(
                    $responseJsonStructure
                )
            );

        if (!empty($jsonAdditionalData)) {
            $response->assertJson(
                [
                    'data' => $jsonAdditionalData,
                ]
            );
        }
    }

    /**
     * Provider for testUserWrongRegistration: login, email, password, password_confirmation, partner_id
     */
    public function providerUserWrongRegistration()
    {
        return [
            [self::LOGIN_WRONG, self::EMAIL, self::PASSWORD, self::PASSWORD, false],
            [self::LOGIN, self::EMAIL_WRONG, self::PASSWORD, self::PASSWORD, false],
            [self::LOGIN, self::EMAIL, self::PASSWORD_WRONG, self::PASSWORD, false],
            [self::LOGIN, self::EMAIL, self::PASSWORD, self::PASSWORD_WRONG, false],
            [self::LOGIN, self::EMAIL, self::PASSWORD, self::PASSWORD, self::INT_MAX],
            ['', '', '', '', false],
        ];
    }

    /**
     * Test Response for wrong registration
     *
     * @dataProvider providerUserWrongRegistration
     */
    public function testUserWrongRegistration($login, $email, $password, $passwordConfirmation, $partnerId)
    {
        $dataProvider = [
            'login'     => $login,
            'email'     => $email,
            'password'  => $password,
            'password_confirmation'  => $passwordConfirmation,
        ];

        if ($partnerId) {
            $dataProvider['partner_id'] = $partnerId;
        }

        // Get response
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE, []),
            $dataProvider
        );

        // Test wrong response status
        $response->assertStatus(self::HTTP_STATUS_BAD);
    }

    /**
     * Test Response If guest can register with Referral Partner Id
     */
    public function testUserRegistrationReferralPartnerId()
    {
        $partner = $this->partnerRegisterService($this->user);

        // Get response
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE, []),
            [
                'login'     => self::LOGIN,
                'email'     => self::EMAIL,
                'password'  => self::PASSWORD,
                'password_confirmation'  => self::PASSWORD,
                'partner_id'  => $partner->id,
            ]
        );

        $user = User::query()->where('login', self::LOGIN)->first();

        $responseContent = json_decode($response->getContent());
        
        $responseJsonStructure = $this->responseJsonStructure(
            $user,
            $responseContent->data->token
        );

        // Test response
        $this->assertsResponse(
            $response,
            self::HTTP_STATUS,
            $responseJsonStructure,
            [
                'id' => $user->id,
            ]
        );

        $referral = Referral::query()->where('user_id', $user->id)->first();

        // Test if referral was created in database
        $this->assertTrue($referral->partner_id == $partner->id);
    }

    /**
     * PartnerShip Register Service
     *
     * @param User $user
     *
     * @return Partner
     */
    protected function partnerRegisterService(User $user)
    {
        // App\Services\Partnership\PartnerRegisterService
        $partnerRegisterService = app(PartnerRegisterService::class);
        $partnerRegisterService->register($user);

        // Get Partner Model
        return Partner::query()
            ->where('user_id', $user->id)
            ->first();
    }
}
