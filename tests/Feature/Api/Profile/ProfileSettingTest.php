<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\User\SettingsResource;
use App\Models\User\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class ProfileSettingTest extends TestCase
{
    const ROUTE = 'profile.settings';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test Response If authenticated User can view Settings page
     */
    public function testUsersSettings()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                'email_notification_required'   => $this->user->settings->email_notification_required,
                'phone_notification_required'   => $this->user->settings->phone_notification_required,
                'telegram_notification_required'=> $this->user->settings->telegram_notification_required,
                'deal_notification_required'    => $this->user->settings->deal_notification_required,
                'message_notification_required' => $this->user->settings->message_notification_required,
                'push_notification_required'    => $this->user->settings->push_notification_required,
            ]
        );
    }

    /**
     * Return valid Json structure for User Settings
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(SettingsResource::class, ['resource' => $this->user->settings]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
