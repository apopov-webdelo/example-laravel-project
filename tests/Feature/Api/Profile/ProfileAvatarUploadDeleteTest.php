<?php

namespace Tests\Feature\Api\Profile;

use App\Models\Image\Image;
use App\Models\User\User;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ProfileAvatarUploadDeleteTest extends TestCase
{
    const ROUTE             = 'profile.avatar.update';
    const ROUTE_REMOVE      = 'profile.avatar.remove';
    const METHOD            = 'post';
    const METHOD_REMOVE     = 'delete';
    const HTTP_STATUS       = 200;
    const HTTP_STATUS_BAD   = 422;
    const PATH_AVATAR       = __DIR__ . '/avatar/';

    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
    }

    /**
     * Provider for real avatars List: $filename, $validateStatus
     */
    public function avatarsListValidationProvider()
    {
        return [
            ['avatar.gif', true],
            ['avatar.jpeg', true],
            ['avatar.jpg', true],
            ['avatar.png', true],
            ['avatar_1920.jpg', true],
            ['avatar_1920.png', true],
            ['avatar_2048.png', true],
            ['avatar_bad_docx.docx', false],
            ['avatar_bad_docx.png', false],
            ['avatar_bad_pdf.jpg', false],
            ['avatar_bad_pdf.pdf', false],
            ['avatar_bad_tar.tar', false],
            ['avatar_bad_zip.zip', false],
        ];
    }

    /**
     * Provider for fake avatars List: $width, $height, $size, $validateStatus
     */
    public function avatarsFakeListValidationProvider()
    {
        return [
            [100, 100, 100, true],
            [200, 100, 100, true],
            [100, 300, 100, true],
            [100, 100, 300, true],
            [900, 900, 1999, true],
            [10000, 10000, 10000, false],
        ];
    }

    /**
     * Test If User can upload real images to Profile adn they are validated
     *
     * @dataProvider avatarsListValidationProvider
     */
    public function testRealFileAvatarUpload(string $image, bool $validateStatus)
    {
        Storage::fake('public');

        $avatar = $this->getAvatar($image);
        
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'image' => $avatar,
            ]
        );

        $image = Image::query()->where('filename', $avatar->getClientOriginalName())->first();

        if ($validateStatus == true) {
            Storage::disk('public')->assertExists('/images/' . $image->id . '.' . $image->ext);
            $response->assertStatus(self::HTTP_STATUS);
        } else {
            $response->assertStatus(self::HTTP_STATUS_BAD);
        }
    }

    /**
     * Test Response If authenticated User can view update Profile Image
     *
     * @param string $image
     *
     * @return UploadedFile
     */
    protected function getAvatar(string $image)
    {
        $path = self::PATH_AVATAR . $image;
        $file = new File($path);

        return new UploadedFile(
            $file->path(),
            $file->hashName(),
            $file->getMimeType(),
            null,
            true
        );
    }

    /**
     * Test If User can update Fake Profile Image
     *
     * @dataProvider avatarsFakeListValidationProvider
     */
    public function testFakeAvatarUpload(int $width, int $height, int $size, bool $validateStatus)
    {
        Storage::fake('public');

        $avatar = UploadedFile::fake()->image('avatar.jpg', $width, $height)->size($size);

        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'image' => $avatar,
            ]
        );

        $image = Image::query()->where('filename', $avatar->getClientOriginalName())->first();

        if ($validateStatus == true) {
            Storage::disk('public')->assertExists('/images/' . $image->id . '.' . $image->ext);
            $response->assertStatus(self::HTTP_STATUS);
        } else {
            $response->assertStatus(self::HTTP_STATUS_BAD);
        }
    }

    /**
     * Test Response if Unauthenticated guest has no access
     */
    public function testUnauthenticatedCannotAccess()
    {
        // Test if Unauthenticated guest has no access
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }

    

    /**
     * Test If user can delete avatar
     */
    public function testAvatarDelete()
    {
        Storage::fake('public');

        $avatar = $this->getAvatar('avatar.gif');

        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'image' => $avatar,
            ]
        );

        $image = Image::query()->where('filename', $avatar->getClientOriginalName())->first();
        
        Storage::disk('public')->assertExists('/images/' . $image->id . '.' . $image->ext);
        
        // Delete response
        $response = $this->response(
            $this->user,
            self::ROUTE_REMOVE,
            self::METHOD_REMOVE,
            []
        );

        Storage::disk('public')->assertMissing('/images/' . $image->id . '.' . $image->ext);
    }
}
