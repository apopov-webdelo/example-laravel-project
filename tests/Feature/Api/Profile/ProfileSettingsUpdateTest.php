<?php

namespace Tests\Feature\Api\Profile;

use App\Http\Resources\Client\User\ProfileResource;
use App\Models\User\User;
use App\Repositories\Balance\BalanceRepo;
use Tests\TestCase;

class ProfileSettingsUpdateTest extends TestCase
{
    const ROUTE = 'profile.settings';
    const METHOD = 'put';
    const HTTP_STATUS = 201;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser();

        // bind fake BalanceRepo to not pull network data
        $this->app->bind(BalanceRepo::class, function () {
            $mockedRepo = \Mockery::mock(BalanceRepo::class)->makePartial();
            return $mockedRepo->allows(['getTotalAmountInFiat' => 0]);
        });
    }

    /**
     * Provider for Settings List & data
     */
    public function settingsListDataProvider()
    {
        return [
            ['enableEmailNotifications', 'email_notification_required', true],
            ['disableEmailNotifications', 'email_notification_required', false],
            ['enablePhoneNotifications', 'phone_notification_required', true],
            ['disablePhoneNotifications', 'phone_notification_required', false],
            ['enableTelegramNotifications', 'telegram_notification_required', true],
            ['disableTelegramNotifications', 'telegram_notification_required', false],
            ['enableDealNotifications', 'deal_notification_required', true],
            ['disableDealNotifications', 'deal_notification_required', false],
            ['enableMessageNotifications', 'message_notification_required', true],
            ['disableMessageNotifications', 'message_notification_required', false],
            ['enablePushNotifications', 'push_notification_required', true],
            ['disablePushNotifications', 'push_notification_required', false],
        ];
    }

    /**
     * Test Response If authenticated User can update Settings
     *
     * @dataProvider settingsListDataProvider
     */
    public function testProfileSettingsUpdate($route, $field, $value)
    {
        // Get response
        $response = $this->response(
            $this->user,
            self::ROUTE . '.' . $route,
            self::METHOD,
            []
        );

        // Test for user without filter
        $this->asserts(
            $response,
            self::HTTP_STATUS
        );

        // Test if data was changed
        $this->assertTrue($this->user->settings->{$field} == $value);

        return $this;
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ProfileResource::class, ['resource' => $this->user]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     *
     * @dataProvider settingsListDataProvider
     */
    public function testAssertResponseWithoutUser($route, $field, $value)
    {
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE . '.' . $route)
        );

        $this->log($response, self::METHOD, self::ROUTE . '.' . $route);

        $response->assertStatus(401);
    }
}
