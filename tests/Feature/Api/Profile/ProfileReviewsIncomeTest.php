<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\User\Review\ReviewCollection;
use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Makers\User\ReviewMaker;
use App\Models\User\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class ProfileReviewsIncomeTest extends TestCase
{
    const ROUTE = 'profile.reviews.income';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * User for route
     *
     * @var User|null $user
     */
    protected $dealUser;

    /**
     * Ads created for dealUser
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Deals created for dealUser
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Reviews created for deals
     *
     * @var array
     */
    protected $reviews = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('adUser');

        // Create a deal author
        $this->dealUser = $this->createUser('dealUser');

        // Create 5 Ads for User
        $this->storeAds($this->user, 5);

        // Create 5 Deals for deal author
        $this->storeDeals($this->dealUser);

        // Create 5 Reviews for deals
        $this->storeReviews($this->user);
    }

    /**
     * Test Response If authenticated User can view Income Reviews list
     */
    public function testUserViewIncomeReviewsList()
    {
        // Acting route as dealUser
        $response = $this->response(
            $this->dealUser,
            self::ROUTE,
            self::METHOD
        );

        // Reviews Array for asserting
        $reviewsArray = [];

        foreach ($this->reviews as $review) {
            $reviewsArray[] = [
                'id' => $review->id,
                'author_id' => $this->user->id,
                'recipient_id' => $this->dealUser->id,
            ];
        }

        // Test if Deal User views reviews
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $reviewsArray
        );
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int $count
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init()
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array $data
     *
     * @return void
     */
    protected function storeDeals(User $user = null, $data = [])
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $this->deals[] = DealMaker::init($data)
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Reviews for user
     *
     * @param User|null $user
     * @param array $data
     *
     * @return void
     */
    protected function storeReviews(User $user = null, $data = [])
    {
        foreach ($this->deals as $deal) {
            $data['deal_id'] = $deal->id;
            $this->reviews[] = ReviewMaker::init($data)
                ->recipient($this->dealUser)
                ->author($user)
                ->deal($deal)
                ->create();
        }
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ReviewCollection::class, [
            'resource' => $this->dealUser->reviewsIncome()->paginate(10)
        ]);
    }

    /**
     * Test Response If authenticated User can view only his Reviews list
     */
    public function testUserViewOnlyHisReviewsList()
    {
        // Reviews Array for asserting
        $reviewsArray = [];

        foreach ($this->reviews as $review) {
            $reviewsArray[] = [
                'id' => $review->id,
                'author_id' => $this->user->id,
                'recipient_id' => $this->dealUser->id,
            ];
        }

        // Reload arrays for new data
        $ads = [];
        $deals = [];
    
        // Create a another User
        $anotherUser = $this->createUser('anotherUser');

        // Create a another Deal User
        $anotherDealUser = $this->createUser('anotherDealUser');

        // Create 5 Ads for anotherUser
        $this->storeAds($anotherUser, 5);

        // Create 5 Deals for anotherDealUser
        $this->storeDeals($anotherDealUser);

        // Create 5 Reviews for deals
        $this->storeReviews($anotherUser);

        // Acting route as dealUser
        $response = $this->response(
            $this->dealUser,
            self::ROUTE,
            self::METHOD
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $reviewsArray
        );
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
