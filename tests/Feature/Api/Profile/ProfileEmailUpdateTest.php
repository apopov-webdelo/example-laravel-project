<?php

namespace Tests\Feature\Api\Profile;

use App\Facades\CryptoExchange;
use App\Http\Resources\Client\User\ContactResource;
use App\Http\Resources\Client\User\ProfileResource;
use App\Models\User\Google2faConstants;
use App\Models\User\User;
use App\Utils\Json;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ProfileEmailUpdateTest extends TestCase
{
    const ROUTE         = 'profile.emailUpdate';
    const METHOD        = 'put';
    const HTTP_STATUS   = 201;
    
    const FIELD         = 'email';
    const NEW_VALUE     = 'emailnew@mail.new';
    const PASSWORD      = 'password';

    const ROUTE_CONFIRM = 'profile.security.confirmEmailUpdate';
    const ROUTE_UPDATE  = 'profile.security.confirmEmail';
    
    const HTTP_STATUS_CONFIRM = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
        $this->user->password = Hash::make(self::PASSWORD);

        // Setup a User Security data
        $this->user->security->google2fa_status = Google2faConstants::STATUS_ACTIVATED;
        $this->google2fa = app('pragmarx.google2fa');
        $this->user->google2fa_secret = $this->google2fa->generateSecretKey();
    }

    /**
     * Test Response If authenticated User can Update Email
     */
    public function testUserProfileEmailUpdate()
    {
        CryptoExchange::shouldReceive('convert')->andReturn(999);

        // Get response for ROUTE profile.emailUpdate
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'email'     => self::NEW_VALUE,
                'password'  => self::PASSWORD,
                'google2fa_secret'  => $this->google2fa->getCurrentOtp($this->user->google2fa_secret),
            ]
        );

        // Test response for ROUTE profile.emailUpdate
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                'phone'           => $this->user->phone,
                'phone_confirmed' => $this->user->security->phone_confirmed,
    
                'email'           => $this->user->email,
                'email_confirmed' => $this->user->security->email_confirmed,
    
                'telegram_id' => $this->user->telegram_id,
                'telegram_id_confirmed' => $this->user->security->telegram_id_confirmed,
            ]
        );

        // Get response for ROUTE profile.security.confirmEmailUpdate
        $responseConfirm = $this->response(
            $this->user,
            self::ROUTE_CONFIRM,
            self::METHOD,
            [
                'token' => $this->user->security->email_access_token,
            ]
        );

        $responseData = $this->getResponseData($responseConfirm);

        $jsonData = [
            'user'      => new ProfileResource($this->user),
            'api_token' => $responseData->api_token
        ];

        $responseJson = response()->json($jsonData);

        // Test response for ROUTE profile.security.confirmEmailUpdate
        $this->assertsOtherRoutes(
            $responseConfirm,
            self::HTTP_STATUS_CONFIRM,
            Json::structure($jsonData),
            $this->objectToArray($responseJson->getData())
        );

        // Get response from for ROUTE profile.security.confirmEmail
        $this->response(
            $this->user,
            self::ROUTE_UPDATE,
            self::METHOD,
            [
                'token' => $this->user->api_token,
            ]
        );

        // Test if data was changed
        $this->assertTrue($this->user->email == self::NEW_VALUE);
    }

    /**
     * Return valid Json structure
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ContactResource::class, ['resource' => $this->user]);
    }

    /**
     * Asserts for testing
     *
     * @param TestResponse $response
     * @param int $httpStatus
     * @param array $responseJsonStructure
     * @param array $jsonAdditionalData
     * @return void
     */
    protected function assertsOtherRoutes($response, int $httpStatus, $responseJsonStructure = [], $jsonAdditionalData = [])
    {
        $response->assertStatus($httpStatus)
            ->assertJsonStructure($responseJsonStructure);

        if (!empty($jsonAdditionalData)) {
            $response->assertJson(
                [
                    'data' => $jsonAdditionalData,
                ]
            );
        }
    }

    protected function objectToArray($d)
    {
        if (is_object($d)) {
            // Gets the properties of the given object
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            // Return array converted to object Using __FUNCTION__ (Magic constant)
            return array_map(__METHOD__, $d);
        } else {
            // Return array
            return $d;
        }
    }
}
