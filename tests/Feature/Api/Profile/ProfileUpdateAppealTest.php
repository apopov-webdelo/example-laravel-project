<?php

namespace Tests\Feature\Api\Profile;

use App\Http\Resources\Client\User\ProfileResource;
use App\Models\User\User;
use App\Repositories\Balance\BalanceRepo;
use Tests\TestCase;

class ProfileUpdateAppealTest extends TestCase
{
    const ROUTE = 'profile.updateAppeal';
    const METHOD = 'put';
    const HTTP_STATUS = 201;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser();

        // bind fake BalanceRepo to not pull network data
        $this->app->bind(BalanceRepo::class, function () {
            $mockedRepo = \Mockery::mock(BalanceRepo::class)->makePartial();
            return $mockedRepo->allows(['getTotalAmountInFiat' => 0]);
        });
    }

    /**
     * Test Response If authenticated User can Update Appeal
     */
    public function testUserProfile()
    {
        // Test for user without filter
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'appeal' => 'Reason',
            ]
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                'id' => $this->user->id,
                'appeal' => 'Reason',
            ]
        );
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ProfileResource::class, ['resource' => $this->user]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     *
     */
    public function testAssertResponseWithoutUser()
    {
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE)
        );

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
