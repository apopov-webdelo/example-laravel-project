<?php

namespace Tests\Feature\Api\Profile;

use App\Facades\CryptoExchange;
use App\Http\Resources\Client\User\ProfileResource;
use App\Models\User\User;
use Tests\TestCase;

class ProfileSettingsUpdateDealCancellationMaxPercentTest extends TestCase
{
    const HTTP_STATUS   = 201;
    const ROUTE         = 'profile.settings.updateDealCancellationMaxPercent';
    const METHOD        = 'put';
    const VALUE         = 70;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test Response If authenticated User can update Settings
     */
    public function testProfileSettingsUpdate()
    {
        CryptoExchange::shouldReceive('convert')->andReturn(999);

        // Get response
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            ['deal_cancellation_max_percent' => self::VALUE]
        );
        
        // Test for user without filter
        $this->asserts(
            $response,
            self::HTTP_STATUS
        );
        
        // Test if data was changed
        $this->assertTrue($this->user->settings->deal_cancellation_max_percent == self::VALUE);
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ProfileResource::class, ['resource' => $this->user]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     *
     */
    public function testAssertResponseWithoutUser()
    {
        $response = $this->json(
            self::METHOD,
            route(self::ROUTE)
        );

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
