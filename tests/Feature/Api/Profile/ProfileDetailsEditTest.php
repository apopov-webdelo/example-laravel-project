<?php

namespace Tests\Feature\Api\Profile;

use App\Facades\CryptoExchange;
use App\Http\Resources\Client\User\ProfileResource;
use App\Models\User\User;
use Tests\TestCase;

class ProfileDetailsEditTest extends TestCase
{
    const ROUTE = 'profile.update';
    const METHOD = 'put';
    const HTTP_STATUS = 201;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test Response If authenticated User can edit his Profile
     */
    public function testUserProfile()
    {
        CryptoExchange::shouldReceive('convert')->andReturn(999);

        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [
                'login' => 'newlogin',
                'email' => 'newemail@test.com',
            ]
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                'id' => $this->user->id,
                //TODO: не обновляется профиль в контроллере
                //'login' => 'test',
                //'email' => 'test@test.com',
            ]
        );
    }

    /**
     * Return valid Json structure for User Repo Collection
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ProfileResource::class, ['resource' => $this->user]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
