<?php

namespace Tests\Feature\Api\Users;

use App\Makers\AdMaker;
use App\Makers\DealMaker;
use App\Makers\User\ReviewMaker;
use App\Models\User\User;
use App\Repositories\User\UserRepo;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class ProfileFavoritelistTest extends TestCase
{
    const ROUTE = 'profile.favoritelist';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Users for deals
     *
     * @var array
     */
    protected $dealUsers = [];

    /**
     * Ads created for user
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Deals created for dealUser
     *
     * @var array
     */
    protected $deals = [];

    /**
     * Reviews created for deals
     *
     * @var array
     */
    protected $reviews = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
        
        // Create a deals authors
        $this->dealUsers = $this->createUsers(6);

        // Create 6 Ads for User
        $this->storeAds($this->user, 6);

        // Create 36 Deals for 6 ads
        foreach ($this->dealUsers as $dealUser) {
            $this->storeDeals($dealUser);
        }
    }

    /**
     * Test Response If authenticated User can view Favoritelist page
     */
    public function testUsersFavoritelist()
    {
        // Make 3 favorite Users
        $favoriteUsers = array_slice($this->dealUsers, 3);

        $favoriteUsersIds = [];

        foreach ($favoriteUsers as $favoriteUser) {
            $favoriteUsersIds [] = $favoriteUser->id;
        }

        // Create 5 Reviews for User to be author
        foreach ($this->deals as $deal) {
            $this->storeReview(
                $deal,
                $this->user,
                [
                    'rate' => (
                        in_array($deal->author_id, $favoriteUsersIds)
                            ? 4
                            : 1
                        ),
                ]
            );
        }

        // Test if user can view favorite Users his Favoritelist
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD
        );

        // Reviews Array for User
        $assertArray = [];

        foreach ($favoriteUsers as $favoriteUser) {
            $assertArray[] = [
                'id'    => $favoriteUser->id,
                'login' => $favoriteUser->login,
            ];
        }

        // Test route
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $assertArray
        );
    }

    /**
     * Create Users
     *
     * @param int $count
     *
     * @return void
     */
    protected function createUsers(int $count = 1)
    {
        $users = [];

        for ($i = 0; $i < $count; $i++) {
            $users[] = $this->createUser();
        }

        return $users;
    }

    /**
     * Create Ads for user
     *
     * @param User $user
     * @param int $count
     *
     * @return void
     */
    protected function storeAds(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->ads[] = AdMaker::init()
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Deals for user
     *
     * @param User|null $user
     * @param array $data
     *
     * @return void
     */
    protected function storeDeals(User $user = null, $data = [])
    {
        foreach ($this->ads as $ad) {
            $data['ad_id'] = $ad->id;
            $this->deals[] = DealMaker::init($data)
                ->author($user)
                ->create();
        }
    }

    /**
     * Create Reviews for user
     *
     * @param User|null $user
     * @param array $data
     *
     * @return void
     */
    protected function storeReviews(User $user = null, $data = [])
    {
        foreach ($this->deals as $deal) {
            $this->storeReview($deal, $user, $data);
        }
    }

    /**
     * Create one Review
     *
     * @param Deal $deal
     * @param User|null $user
     * @param array $data
     *
     * @return void
     */
    protected function storeReview($deal, User $user = null, $data = [])
    {
        $data['deal_id'] = $deal->id;
        $this->reviews[] = ReviewMaker::init($data)
            ->recipient($deal->author()->get()->first())
            ->author($user)
            ->deal($deal)
            ->create();
    }

    /**
     * Return Resource for Json structure
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(UserRepo::class)->take()->paginate(50);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
