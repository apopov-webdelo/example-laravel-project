<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\User\Session\SessionCollection;
use App\Makers\User\SessionMaker;
use App\Models\User\User;
use Tests\TestCase;

class ProfileSessionTest extends TestCase
{
    const ROUTE = 'profile.sessions';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Second User
     *
     * @var User|null $userTwo
     */
    protected $userTwo;

    /**
     * Array with Users Sessions
     *
     * @var array $sessions
     */
    protected $sessions = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->createUser('user');

        // Create a second User
        $this->userTwo = $this->createUser('userTwo');

        // Create a User Session data
        $this->storeSessions($this->user, 5);

        // Create a Session data for second User
        $this->storeSessions($this->userTwo, 5);
    }

    /**
     * Test Response If authenticated User can view sessions page
     */
    public function testUsersSession()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD
        );

        // Reviews Array for first User
        $sessionsArray = [];

        foreach (array_reverse($this->sessions[$this->user->id]) as $session) {
            $sessionsArray[] = [
                'ip'      => $session->ip,
                'country' => $session->country,
                'city'    => $session->city,
                'os'      => $session->os,
                'browser' => $session->browser,
                'type'    => $session->type,
                'status'  => $session->status,
            ];
        }

        // Test route
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            $sessionsArray
        );

        // Test for second user to view its sessions
        $responseTwo = $this->response(
            $this->userTwo,
            self::ROUTE,
            self::METHOD
        );

        // Reviews Array for first User
        $sessionsArrayTwo = [];

        foreach (array_reverse($this->sessions[$this->userTwo->id]) as $sessionTwo) {
            $sessionsArrayTwo[] = [
                'ip'      => $sessionTwo->ip,
                'country' => $sessionTwo->country,
                'city'    => $sessionTwo->city,
                'os'      => $sessionTwo->os,
                'browser' => $sessionTwo->browser,
                'status'  => $sessionTwo->status,
                'type'    => $sessionTwo->type,
            ];
        }

        $this->asserts(
            $responseTwo,
            self::HTTP_STATUS,
            $sessionsArrayTwo
        );
    }

    /**
     * Create Sessions for user
     *
     * @param User $user
     * @param int  $count
     *
     * @return void
     */
    protected function storeSessions(User $user, int $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->sessions[$user->id][] = SessionMaker::init(['sessionable_id' => $user])
                                                       ->sessionable($user)
                                                       ->create();
        }
    }

    /**
     * Return valid Json structure for User Session
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(SessionCollection::class, ['resource' => $this->user->sessions()->paginate(25)]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
