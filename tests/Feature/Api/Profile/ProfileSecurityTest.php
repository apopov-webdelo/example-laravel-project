<?php

namespace Tests\Feature\Api\Users;

use App\Http\Resources\Client\User\SecurityResource;
use App\Makers\User\SecurityMaker;
use App\Models\User\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class ProfileSecurityTest extends TestCase
{
    const ROUTE = 'profile.security';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
        
        // Create a User security data
        SecurityMaker::init()
            ->user($this->user)
            ->create();
    }

    /**
     * Test Response If authenticated User can view security page
     */
    public function testUsersSecurity()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD
        );
        
        // Test route
        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [
                "email_confirmed"   => $this->user->security->email_confirmed,
                "phone_confirmed"   => $this->user->security->phone_confirmed,
                "google2fa_status"  => $this->user->security->google2fa_status,
            ]
        );
    }

    /**
     * Return valid Json structure for User Security
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(SecurityResource::class, ['resource' => $this->user->security]);
    }

    /**
     * Test Response If Unauthenticated guests cannot access route
     */
    public function testAccessUnauthenticatedGuests()
    {
        $response = $this->json(self::METHOD, route(self::ROUTE));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
