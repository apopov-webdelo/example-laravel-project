<?php

namespace Tests\Feature\Api\Market;

use App\Http\Resources\Client\Market\AdCollection;
use App\Models\Directory\Currency;
use App\Repositories\Ad\AdRepo;
use App\Repositories\Directory\CryptoCurrencyRepo;
use App\Makers\AdMaker;
use App\Models\User\User;
use App\Models\Directory\Country;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\CryptoCurrencyConstants;
use Tests\TestCase;

class MarketAdsForSaleTest extends TestCase
{
    const ROUTE = 'market.sale';
    const METHOD = 'get';
    const HTTP_STATUS = 200;

    // Locale: Россия
    const COUNTRY_ALPHA2      = 'RU';
    const CURRENCY_RUB        = 1;
    const BANK_SYSTEM_ID      = 1;
    const PAYMENT_SYSTEM_QIWI = 1;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Another user
     *
     * User|null $anotherUser
     */
    protected $anotherUser;

    /**
     * Array for CryptoCurrencies Ids
     *
     * @var array
     */
    protected $crypto = [];

    /**
     * Ads created for testing Ad model
     *
     * @var array
     */
    protected $ads = [];

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser('user');
        
        // Create a User
        $this->anotherUser = $this->createUser('anotherUser');

        // Get Ids for CryptoCurrencies
        $this->crypto['BTC'] = $this->getCryptoCurrency(CryptoCurrencyConstants::BTC_CODE)->id;
        $this->crypto['ETH'] = $this->getCryptoCurrency(CryptoCurrencyConstants::ETH_CODE)->id;
    }

    /**
     * Test Response for User is viewing only his own ads
     */
    public function testMarketAdsForSale()
    {
        // Create One Ad for user
        $this->storeAds();

        // Test for First user
        $this->assertResponse($this->user);
        
        // Test for Another user
        $this->assertResponse($this->anotherUser);
    }

    /**
     * Create Users and Ads for First & Another user
     *
     * @param array $data
     * @return void
     */
    protected function storeAds($data = [])
    {
        // Create Ads for First user
        $this->storeAd($this->user, 1, $data);
        $this->storeAd($this->user, 0, $data);

        // Create Ads for Another user
        $this->storeAd($this->anotherUser, 1, $data);
        $this->storeAd($this->anotherUser, 0, $data);
    }

    /**
     * Create One Ad for user
     *
     * @param User $user
     * @param int|null $is_sale
     * @param array $data
     * @return void
     */
    protected function storeAd(User $user, $is_sale = null, $data = [])
    {
        $data['is_sale'] = $is_sale;

        $this->ads[$user->id][] = AdMaker::init($data)->author($user)->create();
    }

    /**
     * Return cryptoCurrency code by Id
     *
     * @param string $code
     * @return CryptoCurrency
     */
    protected function getCryptoCurrency($code)
    {
        $cryptoCurrency = app(CryptoCurrencyRepo::class);
        return $cryptoCurrency->getByCode($code);
    }

    /**
     * Return Country name by Alpha2
     *
     * @param string $code
     * @return Country
     */
    protected function getCountry($code)
    {
        $country = Country::where('alpha2', '=', $code)->get();
        return $country[0];
    }

    /**
     * Test response for specific User with Filter
     *
     * @param User  $user
     * @param array $data
     * @param array $jsonAdditionalData
     *
     * @return void
     */
    protected function assertResponse(
        User $user,
        $data = [],
        $jsonAdditionalData = []
    ) {
        // Test route
        $response = $this->response(
            $user,
            self::ROUTE,
            self::METHOD,
            $data
        );

        // Test if data is for Sale
        $response->assertJson(
            [
                'data' => [
                    ['is_sale' => true]
                ],
            ]
        );

        $this->asserts(
            $response,
            self::HTTP_STATUS,
            [$jsonAdditionalData]
        );
    }

    /**
     * Test response for specific User with Filter & Data Range
     *
     * @param User   $user
     * @param array  $data
     * @param string $field
     *
     * @return void
     */
    protected function assertResponseRange(
        User $user,
        $data,
        $field
    ) {
        // Test route
        $response = $this->response(
            $user,
            self::ROUTE,
            self::METHOD,
            $data
        );

        // Test if data is for Sale
        $response->assertJson(
            [
                'data' => [
                    ['is_sale' => true]
                ],
            ]
        );

        $this->assertsRanges(
            $response,
            $field,
            $data
        );
    }

    /**
     * Return valid AdCollection structure for response
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(AdCollection::class, [
            'resource' => app(AdRepo::class)->take()->paginate(5)
        ]);
    }

    /**
     * Test Response with Filter by CryptoCurrency
     */
    public function testFilterByCryptoCurrency()
    {
        // Create One Ad for user
        $this->storeAds(['crypto_currency_id' => $this->crypto['BTC']]);

        // Test for First user with Filter
        $this->assertResponse(
            $this->user,
            ['crypto_currency_id' => $this->crypto['BTC']],
            [
                'crypto_currency' => [
                    'id' => $this->crypto['BTC'],
                ],
            ]
        );
    }

    /**
     * Test Response with Filter by Currency
     */
    public function testFilterByCurrency()
    {
        // Create One Ad for user
        $this->storeAds(['currency_id' => self::CURRENCY_RUB]);

        // Test for First user with Filter currency
        $this->assertResponse(
            $this->user,
            ['currency_id' => self::CURRENCY_RUB],
            [
                'currency' => [
                    'id' => self::CURRENCY_RUB,
                ],
            ]
        );
        
        // Test for Another user with Filter currency
        $this->assertResponse(
            $this->anotherUser,
            ['currency_id' => self::CURRENCY_RUB],
            [
                'currency' => [
                    'id' => self::CURRENCY_RUB,
                ],
            ]
        );
    }

    /**
     * Test Response with Filter by Payment System
     */
    public function testFilterByPaymentSystem()
    {
        // Create One Ad for user
        $this->storeAds(['payment_system_id' => self::PAYMENT_SYSTEM_QIWI]);

        // Test for First user with Filter payment
        $this->assertResponse(
            $this->user,
            ['payment_system_id' => self::PAYMENT_SYSTEM_QIWI],
            [
                'payment_system' => [
                    'id' => self::PAYMENT_SYSTEM_QIWI,
                ],
            ]
        );
        
        // Test for Another user with Filter payment
        $this->assertResponse(
            $this->anotherUser,
            ['payment_system_id' => self::PAYMENT_SYSTEM_QIWI],
            [
                'payment_system' => [
                    'id' => self::PAYMENT_SYSTEM_QIWI,
                ],
            ]
        );
    }

    /**
     * Test Response with Filter by Country
     */
    public function testFilterByCountry()
    {
        // Get Country by Alpha2
        $country = $this->getCountry(self::COUNTRY_ALPHA2);

        // Create One Ad for user
        $this->storeAds(['country_id' => $country->id]);

        // Test for First user with Filter "Россия"
        $this->assertResponse(
            $this->user,
            ['country_id' => $country->id],
            ['country' => $country->title]
        );
        
        // Test for Another user with Filter "Россия"
        $this->assertResponse(
            $this->anotherUser,
            ['country_id' => $country->id],
            ['country' => $country->title]
        );
    }

    /**
     * Test Response with Filter by Price
     */
    public function testFilterByPrice()
    {
        // Test price value
        $value = 1500;
        $range = [1000, 2000];
        $currency = Currency::find(1);

        // Create One Ad for user
        $this->storeAds(['price' => currencyToCoins($value, $currency)]);

        // Test for First user with Filter
        $this->assertResponseRange(
            $this->user,
            ['price_from' => $range[0], 'price_to' => $range[1]],
            'price'
        );

        // Test for Another user with Filter
        $this->assertResponseRange(
            $this->anotherUser,
            ['price_from' => $range[0], 'price_to' => $range[1]],
            'price'
        );
    }

    /**
     * Test Response with Filter by Min
     */
    public function testFilterByMin()
    {
        // Test min value
        $value = 250;
        $range = [200, 300];
        $currency = Currency::find(1);

        // Create One Ad for user
        $this->storeAds(['min' => currencyToCoins($value, $currency)]);

        // Test for First user with Filter
        $this->assertResponseRange(
            $this->user,
            ['min_from' => $range[0], 'min_to' => $range[1]],
            'min'
        );

        // Test for Another user with Filter
        $this->assertResponseRange(
            $this->anotherUser,
            ['min_from' => $range[0], 'min_to' => $range[1]],
            'min'
        );
    }

    /**
     * Test Response with Filter by Max
     */
    public function testFilterByMax()
    {
        // Test value
        $value = 400;
        $range = [390, 410];
        $currency = Currency::find(1);

        // Create One Ad for user
        $this->storeAds(['max' => currencyToCoins($value, $currency)]);

        // Test for First user with Filter
        $this->assertResponseRange(
            $this->user,
            ['max_from' => $range[0], 'max_to' => $range[1]],
            'max'
        );

        // Test for Another user with Filter
        $this->assertResponseRange(
            $this->anotherUser,
            ['max_from' => $range[0], 'max_to' => $range[1]],
            'max'
        );
    }

    /**
     * Test Response with Filter by ReviewRate
     */
    public function testFilterByReviewRate()
    {
        // Create One Ad for user
        $this->storeAds(['review_rate' => 3]);

        // Test for First user with Filter
        $this->assertResponse(
            $this->user,
            ['review_rate_from' => 2, 'review_rate_to' => 4]
        );

        // Test for Another user with Filter
        $this->assertResponse(
            $this->anotherUser,
            ['review_rate_from' => 2, 'review_rate_to' => 4]
        );
    }

    /**
     * Test Response with Filter by Turnover
     */
    public function testFilterByTurnover()
    {
        // Create One Ad for user
        $this->storeAds(['turnover' => 1]);

        // Test for First user with Filter
        $this->assertResponse(
            $this->user,
            ['turnover_from' => 0, 'turnover_to' => 2]
        );

        // Test for Another user with Filter
        $this->assertResponse(
            $this->anotherUser,
            ['turnover_from' => 0, 'turnover_to' => 2]
        );
    }

    /**
     * Test Response with Filter by Time
     */
    public function testFilterByTime()
    {
        // Test value
        $value = 64;
        $range = [63, 65];

        // Create One Ad for user
        $this->storeAds(['time' => $value]);

        // Test for First user with Filter
        $this->assertResponseRange(
            $this->user,
            ['time_from' => $range[0], 'time_to' => $range[1]],
            'time'
        );

        // Test for Another user with Filter
        $this->assertResponseRange(
            $this->anotherUser,
            ['time_from' => $range[0], 'time_to' => $range[1]],
            'time'
        );
    }

    /**
     * Test Response with Filter by Bank
     */
    public function testFilterByBank()
    {
        // Create Ads for user
        $this->storeAdWithBank(0);
        $this->storeAdWithBank(1);

        // Test for user with Filter
        $this->assertResponse($this->user, ['bank_id' => self::BANK_SYSTEM_ID]);
    }

    /**
     * Create One Ad with Bank
     *
     * @param int $is_sale
     */
    protected function storeAdWithBank($is_sale)
    {
        AdMaker::init(['is_sale' => $is_sale])
            ->author($this->user)
            ->bank(self::BANK_SYSTEM_ID, $this->user->id)
            ->create();
    }
}
