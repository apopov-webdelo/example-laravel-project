<?php

namespace Tests\Feature\Api\Market;

use App\Repositories\Ad\AdRepo;
use Tests\Feature\Api\Adable;
use Tests\TestCase;

class MarketAdDetailsTest extends TestCase
{
    use Adable;

    const ROUTE = 'market.ad.details';
    const METHOD = 'get';

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User & Ad
        $this->storeAd();
    }

    /**
     * Test market.ad.details response is valid
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertMarketAdDetailsValidResponse()
    {
        // Test if everybody can view Ad details. Acting route as notAdAuthor
        $response = $this->response(
            $this->notAdAuthor,
            self::ROUTE,
            self::METHOD,
            [],
            $this->ad->id
        );

        $this->asserts($response);
    }

    

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(AdRepo::class)->filterForBuy()->take()->orderBy('price', 'desc')->paginate(25);
    }
}
