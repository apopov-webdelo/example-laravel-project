<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 8/20/18
 * Time: 17:23
 */

namespace Tests\Feature\Api;

use App\Contracts\AuthenticatedContract;
use App\Makers\User\UserMaker;
use App\Models\User\User;

trait Authorable
{
    /**
     * Author of model
     *
     * @var User
     */
    protected $author;

    /**
     * Instance author model
     *
     * @return $this
     */
    protected function setAuthor()
    {
        $author = $this->author = $this->getUser();
        app()->bind(AuthenticatedContract::class, function () use ($author) {
            return $author;
        });

        return $this;
    }

    /**
     * Return User using Maker
     *
     * @param array $attributes
     * @return User
     */
    protected function getUser(array $attributes = []) : User
    {
        $user = UserMaker::init($attributes)->create();

        return $user;
    }
}