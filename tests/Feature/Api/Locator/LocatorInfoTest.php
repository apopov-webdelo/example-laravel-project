<?php

namespace Tests\Feature\Api\Locator;

use App\Facades\GeoLocator;
use App\Http\Resources\Client\User\GeoLocatorResource;
use Tests\TestCase;

class LocatorInfoTest extends TestCase
{
    const ROUTE = 'locator.info';
    const HTTP_STATUS = 200;

    /**
     * User
     *
     * @var User|null $user
     */
    protected $user;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
    }

    /**
     * Test if user can access and view Locator Info
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAssertLocatorInfoTestValidResponse()
    {
        // Acting route as user
        $response = $this->response(
            $this->user,
            self::ROUTE
        );
        
        $this->asserts($response);
    }

    /**
     * Return valid Json structure for GeoLocatorResource
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(GeoLocatorResource::class, ['resource' => GeoLocator::getIp()]);
    }
}
