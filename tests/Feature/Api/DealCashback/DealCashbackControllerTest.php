<?php

namespace Tests\Feature\Api\DealCashback;

use App\Models\DealCashback\DealCashback;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class DealCashbackControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        Event::fake();
    }

    /** @test */
    public function testCancelRoute()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_PENDING]);

        $data = [
            'error'  => 'error_text',
            'reason' => 'reason_text',
            'hash'   => md5($cashback->id . '-error_text-reason_text-' . config('app.blockchain.stellar.private_key')),
        ];

        $response = $this->json(
            'POST',
            route('blockchain.cashback.cancel', ['cashback' => $cashback->id]),
            $data
        );

        $response->assertStatus(Response::HTTP_OK);

        $cashback = $cashback->fresh();
        $this->assertEquals(DealCashback::STATUS_CANCELED, $cashback->status);
    }

    /** @test */
    public function testDoneRoute()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_PENDING]);

        $data = [
            'transaction_id' => '9999',
            'timestamp'      => 12345,
            'hash'           => md5($cashback->id . '-' . 9999 . '-' . 12345 . '-' . config('app.blockchain.stellar.private_key')),
        ];

        $response = $this->json(
            'POST',
            route('blockchain.cashback.done', ['cashback' => $cashback->id]),
            $data
        );

        $response->assertStatus(Response::HTTP_OK);

        $cashback = $cashback->fresh();
        $this->assertEquals(DealCashback::STATUS_DONE, $cashback->status);
        $this->assertEquals(9999, $cashback->transaction_id);
    }

    /** @test */
    public function testDoneRouteDoubleSameTX()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_PENDING]);

        $data = [
            'transaction_id' => '9999',
            'timestamp'      => 12345,
            'hash'           => md5($cashback->id . '-' . 9999 . '-' . 12345 . '-' . config('app.blockchain.stellar.private_key')),
        ];

        $this->json(
            'POST',
            route('blockchain.cashback.done', ['cashback' => $cashback->id]),
            $data
        );
        $response = $this->json(
            'POST',
            route('blockchain.cashback.done', ['cashback' => $cashback->id]),
            $data
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function testDoneRouteDoubleStatusIsDone()
    {
        /* @var DealCashback $cashback */
        $cashback = factory(DealCashback::class)->create(['status' => DealCashback::STATUS_DONE]);

        $data = [
            'transaction_id' => '9999',
            'timestamp'      => 12345,
            'hash'           => md5($cashback->id . '-' . 9999 . '-' . 12345 . '-' . config('app.blockchain.stellar.private_key')),
        ];

        $response = $this->json(
            'POST',
            route('blockchain.cashback.done', ['cashback' => $cashback->id]),
            $data
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
