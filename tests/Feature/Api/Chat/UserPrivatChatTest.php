<?php

namespace Tests\Feature\Api\Chat;

use App\Makers\Chat\ChatMaker;
use App\Models\Chat\Chat;
use App\Models\User\User;
use App\Http\Resources\Client\Chat\ChatResource;
use App\Repositories\Chat\ChatRepo;
use Tests\TestCase;

class UserPrivatChatTest extends TestCase
{
    const ROUTE = 'chat.user';
    const METHOD = 'get';
    const HTTP_STATUS = 200;
    const HTTP_STATUS_BAD = 404;

    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * User for chat
     *
     * @var User $user
     */
    protected $chatUser;

    /**
     * Chats created for chatUser
     *
     * @var Chat
     */
    protected $chat;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();

        // Create a User
        $this->user = $this->registerUser(['login' => 'user']);

        // Create a chat User
        $this->chatUser = $this->registerUser(['login' => 'chatUser']);

        // Create a chat for User
        $this->chat = $this->storeChat($this->user, $this->chatUser, ['author_id' => $this->user]);

        $this->chat->recipients()->attach($this->chatUser);
    }

    /**
     * Create Chat for user
     *
     * @param User  $user
     * @param User  $recepient
     * @param array $data
     *
     * @return Chat
     */
    protected function storeChat(User $user, User $recepient, $data = [])
    {
        return ChatMaker::init($data)
            ->author($user)
            ->withRecepient($recepient)
            ->withRecepient($user)
            ->withMessages($user, $recepient)
            ->create();
    }

    /**
     * Test Response If authenticated User can view PrivateChat
     */
    public function testUserViewPrivateChat()
    {
        $response = $this->response(
            $this->user,
            self::ROUTE,
            self::METHOD,
            [],
            $this->chatUser->id
        );

        $this->asserts(
            $response
        );
    }

    /**
     * Return valid Json structure
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return app(ChatResource::class, [
            'resource' => app(ChatRepo::class)->getChatBetweenUsers($this->chatUser, $this->user)
        ]);
    }

    /**
     * Test Response If chat does not exist
     */
    public function testNonexistentChat()
    {
        // Create another not chat User
        $notChatUser = $this->createUser('notChatUser');

        // Test if Unauthenticated guest has no access to Users list
        $response = $this->json(self::METHOD, route(self::ROUTE, $notChatUser->id));

        $this->log($response, self::METHOD, self::ROUTE);

        $response->assertStatus(401);
    }
}
