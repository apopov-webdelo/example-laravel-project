<?php

namespace Tests\Feature\Api\Chat;

use App\Http\Resources\Client\Message\MessageCollection;
use App\Makers\Chat\MessageMaker;
use App\Makers\DealMaker;
use App\Models\Chat\Message;
use App\Models\User\User;
use App\Repositories\Chat\MessageRepo;
use Tests\TestCase;
use Event;

class StoreTest extends TestCase
{
    const ROUTE_USER    = 'chat.user.messages.store';
    const ROUTE_DEAL    = 'chat.deal.messages.store';
    const METHOD        = 'post';
    const HTTP_STATUS   = 201;
    const HTTP_STATUS_BAD = 403;

    /**
     * User
     *
     * @var User $user
     */
    protected $user;

    /**
     * Message
     *
     * @var Message $message
     */
    protected $message;

    /**
     * Fixture for test
     */
    protected function setUp()
    {
        // Call laravel fixture setUp() for enviroment dependencies
        parent::setUp();
        
        // Create a User
        $this->user = $this->createUser();
        
        $this->message = MessageMaker::init()
            ->author($this->user)
            ->make();
    }

    /**
     * Store User Message
     *
     * @return void
     */
    public function testStoreUserMessage()
    {
        $recipient = $this->createUser();

        $this->responseAssert(
            self::ROUTE_USER,
            self::HTTP_STATUS,
            [ 'user' => $recipient ],
            $this->responseJsonStructure()
        );
    }

    /**
     * Store Deal Message
     *
     * @return void
     */
    public function testStoreDealMessage()
    {
        $deal = $this->storeDeal($this->user);

        $this->responseAssert(
            self::ROUTE_DEAL,
            self::HTTP_STATUS,
            [ 'deal' => $deal ],
            $this->responseJsonStructure()
        );
    }

    /**
     * Store Wrong Deal Message
     *
     * @return void
     */
    public function testStoreWrongDealMessage()
    {
        $dealAuthor = $this->createUser();
        
        $deal = $this->storeDeal($dealAuthor);

        $this->responseAssert(
            self::ROUTE_DEAL,
            self::HTTP_STATUS_BAD,
            [ 'deal' => $deal ]
        );
    }

    /**
     * Test response to the route
     *
     * @param string $route
     * @param int $status
     * @param mixed $parameters
     * @param array $data
     *
     * @return TestResponse $response
     */
    protected function responseAssert(string $route, int $status, $parameters = [], $data = [] )
    {
        Event::fake();
        
        $response = $this->actingAs($this->user, 'api')
                         ->postJson(route($route, $parameters), $this->message->toArray());
            
        $this->log($response, self::METHOD, $route);

        $response->assertStatus($status);
    }

    /**
     * Create Deal for user
     *
     * @param User|null $user
     * @param array $data
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function storeDeal(User $user = null, $data = [])
    {
        return DealMaker::init($data)
            ->author($user)
            ->create();
    }

    /**
     * Return valid AdCollection structure for response
     *
     * @return array
     */
    protected function responseJsonStructure()
    {
        return [
            'id',
            'message',
            'chat_id',
            'created_at',
            'updated_at',
            'author_id',
        ];
    }
}
