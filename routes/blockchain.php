<?php

Route::group([
    'prefix'     => 'blockchain',
    'namespace'  => 'Blockchain',
    'middleware' => ['bindings', 'robot_binding', 'stellar_from_log']
], function () {
    Route::group([
        'prefix'     => 'user',
    ], function () {
        Route::post('{user}/deposit', 'TransactionController@deposit')->name('blockchain.transaction.deposit');
        Route::post('{user}/withdraw', 'TransactionController@withdraw')->name('blockchain.transaction.withdraw');
    });

    Route::group([
        'prefix'     => 'deal',
    ], function () {
        Route::post('{deal}/finish', 'DealController@finish')->name('blockchain.deal.finish');
        Route::post('{deal}/cancel', 'DealController@cancel')->name('blockchain.deal.cancel');
        Route::post('{deal}/verified', 'DealController@verified')->name('blockchain.deal.verified');
    });

    Route::group([
        'prefix'     => 'error',
    ], function () {
        Route::post('deal/{deal}', 'ErrorController@deal')->name('blockchain.error.deal');
        Route::post('user/{user}', 'ErrorController@user')->name('blockchain.error.user');
        Route::post('out/{payOut}', 'ErrorController@out')->name('blockchain.error.out');
    });

    Route::group([
        'prefix'     => 'cashback',
    ], function () {
        Route::post('{cashback}/done', 'DealCashbackController@done')->name('blockchain.cashback.done');
        Route::post('{cashback}/cancel', 'DealCashbackController@cancel')->name('blockchain.cashback.cancel');
    });

    Route::group([
        'prefix'     => 'emergency',
    ], function () {
        Route::post('telegram', 'EmergencyLogController@telegram')->name('blockchain.emergency.telegram');
    });
});
