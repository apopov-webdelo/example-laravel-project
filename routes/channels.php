<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('market.ads', \App\Broadcasting\GuestChannel::class);
Broadcast::channel('market.ads.{ad}', \App\Broadcasting\GuestChannel::class);

Broadcast::channel('ads', \App\Broadcasting\AuthenticatedChannel::class);
Broadcast::channel('ads.{ad}', \App\Broadcasting\AdChannel::class);
Broadcast::channel('ads.{ad}.deals', \App\Broadcasting\AdChannel::class);

Broadcast::channel('chat.{chat}', \App\Broadcasting\ChatChannel::class);

Broadcast::channel('deals.{deal}', \App\Broadcasting\DealChannel::class);

Broadcast::channel('balance.{user}', \App\Broadcasting\BalanceChannel::class);

Broadcast::channel('users.{user}', \App\Broadcasting\UserChannel::class);

Broadcast::channel('reviews.{review}', \App\Broadcasting\GuestChannel::class);
