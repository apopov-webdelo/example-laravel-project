<?php

Route::get('/', 'Client\HomeController@index')->name('index');
Route::match(['get', 'post'], '/botman', 'Client\BotManController@handle')->name('botman');
