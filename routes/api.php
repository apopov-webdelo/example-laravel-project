<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require __DIR__ . '/api/client/auth.php';

/*
 * Client routes - no auth
 */

Route::group([
    'prefix'     => 'v1',
    'namespace'  => 'Client\Directory',
], function () {
    require __DIR__ . '/api/client/directory.php';
});

Route::group([
    'prefix'     => 'v1',
    'namespace'  => 'Client\User',
], function () {
    require __DIR__ . '/api/client/users_for_guest.php';
});

Route::group([
    'prefix'     => 'v1',
    'namespace'  => 'Client\Market',
], function () {
    require __DIR__ . '/api/client/market.php';
});

Route::group([
    'prefix'     => 'v1',
    'namespace'  => 'Client',
], function () {
    require __DIR__ . '/api/client/locator.php';
    require __DIR__ . '/api/client/services.php';
});

/*
 * Client routes - with auth
 */
Route::group([
    'prefix'     => 'v1',
    'middleware' => ['auth:api', 'client', 'prolong_token']
], function () {
    Route::group([
        'namespace'  => 'Client\Chat'
    ], function () {
        require __DIR__ . '/api/client/chats.php';
    });

    Route::group([
        'namespace'  => 'Client\User'
    ], function () {
        require __DIR__ . '/api/client/profile.php';
        require __DIR__ . '/api/client/users.php';
        require __DIR__ . '/api/client/balance.php';
    });

    Route::group([
        'namespace'  => 'Client\Market'
    ], function () {
        require __DIR__ . '/api/client/ads.php';
        require __DIR__ . '/api/client/dashboard.php';
        require __DIR__ . '/api/client/deals.php';
        require __DIR__ . '/api/client/stats.php';
    });
});


Route::group([
    'prefix'     => 'admin/v1',
    'namespace'  => 'Admin',
    'middleware' => ['auth:api-admin', 'admin']
], function () {
    require __DIR__ . '/api/admin/activityLogs.php';
    require __DIR__ . '/api/admin/transactions.php';
    require __DIR__ . '/api/admin/stellarLogs.php';
    require __DIR__ . '/api/admin/ads.php';
    require __DIR__ . '/api/admin/deals.php';
    require __DIR__ . '/api/admin/dashboard.php';
    require __DIR__ . '/api/admin/clients.php';
    require __DIR__ . '/api/admin/chats.php';
    require __DIR__ . '/api/admin/profile.php';
    require __DIR__ . '/api/admin/countries.php';
    require __DIR__ . '/api/admin/cryptoCurrencies.php';
    require __DIR__ . '/api/admin/banks.php';
    require __DIR__ . '/api/admin/paymentSystems.php';
    require __DIR__ . '/api/admin/currencies.php';
    require __DIR__ . '/api/admin/roles.php';
    require __DIR__ . '/api/admin/permissions.php';
    require __DIR__ . '/api/admin/employees.php';
    require __DIR__ . '/api/admin/reviews.php';
});

Route::group([
    'prefix'     => '/v1/partnership',
    'namespace'  => 'Partnership',
    'middleware' => ['auth:api', 'client', 'prolong_token', config('app.throttle.default_api')]
], function () {
    require __DIR__ . '/api/partnership/auth.php';
});

Route::group([
    'prefix'     => '/v1',
    'namespace'  => 'Client',
    'middleware' => ['market', config('app.throttle.mailing_api')],
], function () {
    require __DIR__ . '/api/client/idea.php';
});

Route::group([
    'prefix'     => '/v1',
    'namespace'  => 'Support',
    'middleware' => ['market', config('app.throttle.mailing_api')],
], function () {
    require __DIR__ . '/api/support.php';
});

Route::fallback('FallbackController@notFound');
