<?php

Route::group(['prefix' => 'support'], function () {
    Route::post('api-access-request', 'SupportController@apiAccessRequest')->name('support.api.access.request');
});
