<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */

Route::post('/sign-up', 'RegisterController@register')->name('partnership.signUp');
Route::get('/profile', 'ProfileController@index')->name('partnership.profile');
