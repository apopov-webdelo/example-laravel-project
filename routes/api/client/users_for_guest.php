<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */

Route::group(['middleware' => ['market', 'last_seen', config('app.throttle.default_api')]], function () {
    Route::get('/user/{user}', 'UserController@details')->name('user.details');
    Route::get('/user/login/{login}', 'UserController@detailsLogin')->name('user.details.login');
    Route::get('/user/{user}/reviews', 'UserController@reviews')->name('user.reviews');
    Route::get('/user/{user}/ads', 'UserController@ads')->name('user.ads.index');
    Route::get('/user/{user}/turnover/summary', 'UserController@turnoverSummary')->name('user.turnover.summary');
});
