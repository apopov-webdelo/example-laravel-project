<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */

Route::group(['middleware' => config('app.throttle.default_api')], function () {
    Route::get('user', 'UserController@index')->name('user.index');
    Route::get('user/{user}/deals', 'UserController@deals')->name('user.deals.index');
    Route::put('user/{user}/notes', 'NoteController@store')->name('user.notes.store');
    Route::delete('user/{user}/notes', 'NoteController@destroyByUser')->name('user.notes.delete');
    Route::put('user/{login}/review', '\App\Http\Controllers\Client\Market\ReviewController@storeForLogin')
         ->name('user.review.storeForLogin');
});
