<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */

Route::group(['prefix' => 'dashboard', 'middleware' => config('app.throttle.default_api')], function () {
    Route::get('/ads', 'DashboardController@ads')->name('dashboard.ads');
    Route::get('/deals', 'DashboardController@deals')->name('dashboard.deals');
});
