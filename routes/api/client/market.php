<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */

Route::group([
    'prefix'     => 'market',
    'middleware' => ['market', 'last_seen', config('app.throttle.default_api')],
], function () {
    Route::get('/buy/best', 'MarketController@bestBuy')->name('market.ad.buy.best');
    Route::get('/sale/best', 'MarketController@bestSale')->name('market.ad.sale.best');
    Route::get('/buy', 'MarketController@buy')->name('market.buy');
    Route::get('/sale', 'MarketController@sale')->name('market.sale');
    Route::get('/{ad}', 'MarketController@details')->name('market.ad.details');
    Route::get('/{ad}', 'MarketController@details')->name('market.ad.details');
});
