<?php

Route::group([
    'prefix' => 'balance',
], function () {
    Route::get('commissions', 'BalanceController@commissions')->name('balance.commissions');

    Route::get('{cryptoCurrency}/transactions', 'BalanceController@transactions')->name('balance.transactions');

    Route::post('withdraw', 'BalanceController@withdraw')->name('balance.withdraw');

    Route::get('{cryptoCode}/wallet', 'BalanceController@wallet')->name('balance.wallet');

    Route::post('emergencyWithdraw', 'BalanceController@emergencyWithdraw')
         ->name('balance.emergencyWithdraw');

    Route::post('emergency/wallet', 'BalanceController@addEmergencyWallet')->name('balance.addEmergencyWallet');

    Route::get('amount', 'BalanceController@amount')->name('balance.amount');

    Route::post('emergency', 'BalanceController@createEmergencyWallet')->name('balance.createEmergencyWallet');
});
