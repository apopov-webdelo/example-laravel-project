<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */

Route::group(['middleware' => config('app.throttle.default_api')], function () {
    Route::get('locator/info', 'LocatorController@info')->name('locator.info');
});
