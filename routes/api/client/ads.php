<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */

Route::group(['middleware' => config('app.throttle.default_api')], function () {
    Route::get('ad', 'AdController@index')->name('ad.index');
    Route::post('ad', 'AdController@store')->name('ad.store');
    Route::get('ad/{ad}', 'AdController@details')->name('ad.details');
    Route::get('ad/{ad}/deals', 'AdController@deals')->name('ad.deals');
    Route::put('ad/{ad}', 'AdController@update')->name('ad.update');
    Route::put('ad/{ad}/price', 'AdController@updatePrice')->name('ad.update_price');
    Route::put('ad/{ad}/activate', 'AdController@activate')->name('ad.activate');
    Route::put('ad/{ad}/deactivate', 'AdController@deactivate')->name('ad.deactivate');
    Route::delete('ad/{ad}', 'AdController@destroy')->name('ad.destroy');
});
