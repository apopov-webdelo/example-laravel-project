<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:50
 */
Route::group(['prefix' => 'chat', 'middleware' => config('app.throttle.default_api')], function () {
    Route::get('/', 'ChatController@index')->name('chat.index');
    Route::get('/user/{user}', 'ChatController@user')->name('chat.user');
    Route::post('/deal/{deal}', 'MessageController@storeToDeal')->name('chat.deal.messages.store');
    Route::get('/deal/recent_messages', 'MessageController@getRecentDealMessages')
         ->name('chat.deal.recent_messages.get');
    Route::get('/deal/{deal}', 'MessageController@getDealChat')->name('chat.deal.messages.get');
    Route::post('/user/{user}', 'MessageController@storeToUser')->name('chat.user.messages.store');
    Route::get('/{chat}', 'MessageController@index')->name('chat.messages');
    Route::post('/{chat}', 'MessageController@store')->name('chat.messages.store');
});
