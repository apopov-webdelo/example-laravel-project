<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */

Route::group(['prefix' => 'profile', 'middleware' => config('app.throttle.default_api')], function () {
    Route::get('/', 'ProfileController@details')->name('profile.details');
    Route::put('/', 'ProfileController@update')->name('profile.update');

    Route::get('/reviews/income', 'ProfileController@reviewsIncome')->name('profile.reviews.income');
    Route::get('/reviews/outcome', 'ProfileController@reviewsOutcome')->name('profile.reviews.outcome');

    Route::get('/settings', 'ProfileController@settings')->name('profile.settings');
    Route::get('/security', 'ProfileController@security')->name('profile.security');
    Route::get('/sessions', 'ProfileController@sessions')->name('profile.sessions');

    Route::get('/blacklist', 'ProfileController@blacklist')->name('profile.blacklist');
    Route::get('/favoritelist', 'ProfileController@favoritelist')->name('profile.favoritelist');

    Route::put('/updateAppeal', 'ProfileController@updateAppeal')->name('profile.updateAppeal');
    Route::put('/social/setLogin', 'ProfileController@setLogin')->name('profile.setLogin');
    Route::put('/social/setEmail', 'ProfileController@setEmail')->name('profile.setEmail');

    Route::get('/notes', 'NoteController@index')->name('notes.index');
    Route::delete('/notes/{note}', 'NoteController@destroy')->name('notes.destroy');

    Route::put('/settings/deal-cancellation-max-percent', 'SettingsController@updateDealCancellationMaxPercent')
         ->name('profile.settings.updateDealCancellationMaxPercent');

    Route::put('/settings/enable-email-notifications', 'SettingsController@enableEmailNotifications')
         ->name('profile.settings.enableEmailNotifications');

    Route::put('/settings/disable-email-notifications', 'SettingsController@disableEmailNotifications')
         ->name('profile.settings.disableEmailNotifications');

    Route::put('/settings/enable-phone-notifications', 'SettingsController@enablePhoneNotifications')
         ->name('profile.settings.enablePhoneNotifications');

    Route::put('/settings/disable-phone-notifications', 'SettingsController@disablePhoneNotifications')
         ->name('profile.settings.disablePhoneNotifications');

    Route::put('/settings/enable-telegram-notifications', 'SettingsController@enableTelegramNotifications')
         ->name('profile.settings.enableTelegramNotifications');

    Route::put('/settings/disable-telegram-notifications', 'SettingsController@disableTelegramNotifications')
         ->name('profile.settings.disableTelegramNotifications');

    Route::put('/settings/enable-deal-notifications', 'SettingsController@enableDealNotifications')
         ->name('profile.settings.enableDealNotifications');

    Route::put('/settings/disable-deal-notifications', 'SettingsController@disableDealNotifications')
         ->name('profile.settings.disableDealNotifications');

    Route::put('/settings/enable-message-notifications', 'SettingsController@enableMessageNotifications')
         ->name('profile.settings.enableMessageNotifications');

    Route::put('/settings/disable-message-notifications', 'SettingsController@disableMessageNotifications')
         ->name('profile.settings.disableMessageNotifications');

    Route::put('/settings/enable-push-notifications', 'SettingsController@enablePushNotifications')
         ->name('profile.settings.enablePushNotifications');

    Route::put('/settings/disable-push-notifications', 'SettingsController@disablePushNotifications')
         ->name('profile.settings.disablePushNotifications');

    Route::post('/avatar', 'ProfileImageController@upload')
         ->name('profile.avatar.update');
    Route::delete('/avatar', 'ProfileImageController@remove')
         ->name('profile.avatar.remove');

    Route::get('/spoofing-protection', 'ProfileController@spoofingProtection')->name('user.spoofing-protection');
});

Route::group(['prefix' => 'profile', 'middleware' => config('app.throttle.profile_security_api')], function () {
    Route::get('/security/logs', 'ProfileController@securityLogs')->name('profile.security.logs');

    Route::put('/security/change-password', 'ProfileController@changePassword')
         ->name('profile.security.changePassword');

    Route::put('/set-telegram-id', 'TelegramAddController@telegramAdd')->name('profile.telegramAdd');

    Route::put('/security/confirm-telegram-id', 'TelegramAddController@confirmTelegramId')
         ->name('profile.security.confirmTelegramId');

    Route::put('/email-update', 'EmailChangeController@emailUpdate')->name('profile.emailUpdate');

    Route::put('/security/confirm-email-update', 'EmailChangeController@confirmEmailUpdate')
         ->name('profile.security.confirmEmailUpdate');

    Route::put('/security/start-confirm-email', 'EmailChangeController@startConfirmEmail')
         ->name('profile.security.confirmEmail');
    Route::put('/security/confirm-email', 'EmailChangeController@confirmEmail')->name('profile.security.confirmEmail');

    Route::put('/phone/add', 'PhoneAddController@phoneAdd')->name('profile.phone.add');
    Route::put('/phone/confirm', 'PhoneAddController@confirm')->name('profile.phone.confirm');
    Route::put('/phone/delete', 'PhoneDeleteController@delete')->name('profile.phone.delete');
    Route::put('/phone/complete-delete', 'PhoneDeleteController@complete')->name('profile.phone.complete-delete');

    Route::get('/balance', 'BalanceController@index')->name('profile.balance.index');
    Route::get('/balance/{cryptoCode}', 'BalanceController@balanceCrypto')->name('profile.balance.crypto');

    Route::put('/security/2fa/connect', 'Google2FaController@connect')->name('profile.security.connect');
    Route::put('/security/2fa/disconnect', 'Google2FaController@disconnect')->name('profile.security.disconnect');
    Route::put('/security/2fa/enable', 'Google2FaController@enable')->name('profile.security.enable');
    Route::put('/security/2fa/disable', 'Google2FaController@disable')->name('profile.security.disable');
});
