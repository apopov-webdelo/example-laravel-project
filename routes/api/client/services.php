<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */

Route::group(['middleware' => config('app.throttle.default_api')], function () {
    Route::get('/ping', 'ServiceController@ping')->name('service.ping');
});
