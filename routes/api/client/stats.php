<?php
/**
 * Created by PhpStorm.
 * User: a.popov
 * Date: 9/27/18
 * Time: 16:30
 */
Route::group(['prefix' => 'stats', 'middleware' => config('app.throttle.default_api')], function () {
    Route::get('/ad/{ad}', 'StatisticsController@ad')->name('stats.ad');
    Route::get('/deals/', 'StatisticsController@deals')->name('stats.deals');
});
