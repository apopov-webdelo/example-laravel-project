<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */
Route::group(['prefix' => 'deal', 'middleware' => config('app.throttle.default_api')], function () {
    Route::get('/', 'DealController@index')->name('deal.index');
    Route::post('/', 'DealController@store')->name('deal.store');
    Route::get('/{deal}', 'DealController@details')->name('deal.details');
    Route::put('/{deal}/cancel', 'DealController@cancellation')->name('deal.cancellation');
    Route::put('/{deal}/autocancel', 'DealController@autocancel')->name('deal.autocancel');
    Route::put('/{deal}/dispute', 'DealController@dispute')->name('deal.dispute');
    Route::put('/{deal}/paid', 'DealController@paid')->name('deal.paid');
    Route::put('/{deal}/finish', 'DealController@finishing')->name('deal.finishing');
    Route::put('/{deal}/review', 'ReviewController@store')->name('deal.review.store');
});
