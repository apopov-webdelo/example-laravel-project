<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */

Route::group(['middleware' => config('app.throttle.default_api')], function () {
    Route::group(['prefix'=>'v1/auth', 'namespace'=>'Client'], function () {
        Route::post('/sign-up', 'RegisterController@register')->name('auth.signUp');
        Route::post('/sign-in', 'LoginController@login')->name('auth.signIn');
    });
    Route::group(['prefix'=>'v1/auth', 'namespace'=>'Client', 'middleware'=>['auth:api']], function () {
        Route::post('/sign-out', 'LoginController@logout')->name('auth.signOut');
    });

    Route::group(['prefix'=>'v1', 'namespace'=>'Client\User'], function () {
        Route::post('reset-link', 'RecoveryPasswordController@sendResetLinkEmail')->name('auth.reset-link');
        Route::post('reset-password', 'RecoveryPasswordController@reset')->name('auth.reset-password');
    });


    Route::group(['prefix'=>'admin/v1/auth', 'namespace'=>'Admin', 'middleware'=>['admin']], function () {
        Route::post('/sign-in', 'LoginController@login')->name('admin.auth.signIn');
    });

    Route::group(['prefix'=>'admin/v1/auth', 'namespace'=>'Admin', 'middleware'=>['auth:api-admin', 'admin']], function () {
        Route::post('/sign-out', 'LoginController@logout')->name('admin.auth.signOut');
    });

    Route::group(['prefix'=>'v1/auth', 'namespace' => 'Client'], function () {
        Route::get('list', 'SocialController@index')->name('social.auth.list');
    });

    Route::group(['prefix'=>'v1/auth', 'namespace' => 'Client', 'middleware' => ['web']], function () {
        Route::get('{provider}', 'SocialController@redirectToProvider')->name('social.auth.login');
        Route::get('{provider}/callback', 'SocialController@handleProviderCallback');
    });
});
