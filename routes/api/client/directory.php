<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/31/18
 * Time: 11:33
 */

Route::group(['middleware' => config('app.throttle.default_api')], function () {
    Route::get('countries', 'CountryController@index')->name('directory.countries');
    Route::get('crypto-currencies', 'CryptoCurrencyController@index')->name('directory.crypto-currencies');
    Route::get('currencies', 'CurrencyController@index')->name('directory.currencies');
    Route::get('payment-systems', 'PaymentSystemController@index')->name('directory.payment-systems');
    Route::get('banks', 'BankController@index')->name('directory.banks');

    Route::get('directory/all', 'FullDirectoryController@all')->name('directory.all.view')
                                                            ->middleware('etag');

    Route::get('directory/all/json', 'FullDirectoryController@index')->name('directory.all.json');
});
