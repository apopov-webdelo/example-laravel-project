<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/12/18
 * Time: 15:20
 */

Route::get('payment-systems', 'PaymentSystemController@index')
    ->name('admin.directory.payment-systems');
Route::get('payment-systems/{paymentSystem}', 'PaymentSystemController@details')
    ->name('admin.directory.payment-systems.details');
Route::get('payment-systems/{paymentSystem}/currencies', 'PaymentSystemController@currencies')
    ->name('admin.directory.payment-systems.details.currencies');
Route::put('payment-systems/{paymentSystem}/currencies/{currency}/attach', 'PaymentSystemController@attachCurrency')
    ->name('admin.directory.payment-systems.details.currencies.attach');
Route::put('payment-systems/{paymentSystem}/currencies/{currency}/detach', 'PaymentSystemController@detachCurrency')
    ->name('admin.directory.payment-systems.details.currencies.detach');
Route::post('payment-systems', 'PaymentSystemController@store')
    ->name('admin.directory.payment-systems.store');
Route::put('payment-systems/{paymentSystem}', 'PaymentSystemController@update')
    ->name('admin.directory.payment-systems.update');
Route::delete('payment-systems/{paymentSystem}', 'PaymentSystemController@delete')
    ->name('admin.directory.payment-systems.delete');
