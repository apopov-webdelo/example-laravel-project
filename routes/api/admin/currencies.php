<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/12/18
 * Time: 16:08
 */

Route::get('currencies', 'CurrencyController@index')->name('admin.directory.currencies');
Route::post('currencies', 'CurrencyController@store')->name('admin.directory.currencies.store');
Route::get('currencies/{currency}', 'CurrencyController@details')->name('admin.directory.currencies.details');
Route::put('currencies/{currency}', 'CurrencyController@update')->name('admin.directory.currencies.update');
Route::delete('currencies/{currency}', 'CurrencyController@delete')->name('admin.directory.currencies.delete');
