<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */
Route::get('ads', 'AdController@index')->name('admin.ads.index');
Route::get('ads/{ad}', 'AdController@details')->name('admin.ads.details');
Route::put('ads/{ad}/update-conditions', 'AdController@updateConditions')->name('admin.ads.update-conditions');
Route::put('ads/{ad}/activate', 'AdController@activate')->name('admin.ads.activate');
Route::put('ads/{ad}/deactivate', 'AdController@deactivate')->name('admin.ads.deactivate');
Route::put(
    'ads/{ad}/update-commission-percent',
    'AdController@updateCommissionPercent'
)->name('admin.ads.update-commission-percent');
