<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:50
 */
Route::group(['prefix'=>'chat'], function () {
    Route::post('/deal/{deal}', 'MessageController@storeToDeal')->name('admin.chat.messages.storeToDeal');
    Route::get('/{chat}', 'MessageController@index')->name('admin.chat.messages');
});
