<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 07.11.2018
 * Time: 14:56
 */

Route::get(
    'employees',
    'EmployeeController@index'
)->name('admin.employees');

Route::post(
    'employees',
    'EmployeeController@store'
)->name('admin.employees.store');

Route::get(
    'employees/{employee}',
    'EmployeeController@details'
)->name('admin.employees.employee');

Route::put(
    'employees/{employee}',
    'EmployeeController@update'
)->name('admin.employees.employee.update');

Route::delete(
    'employees/{employee}',
    'EmployeeController@delete'
)->name('admin.employees.employee.delete');

Route::put(
    'employees/{employee}/lock/{days?}',
    'EmployeeController@lock'
)->name('admin.employees.employee.lock');

Route::put(
    'employees/{employee}/unlock',
    'EmployeeController@unlock'
)->name('admin.employees.employee.unlock');

Route::put(
    'employees/{employee}/reset-password',
    'EmployeeController@resetPassword'
)->name('admin.employees.employee.resetPassword');
