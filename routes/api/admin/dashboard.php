<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 2/25/19
 * Time: 16:35
 */

Route::group(['prefix'=>'dashboard', 'namespace'=>'Dashboard'], function () {
    Route::get('/balance', 'BalanceController@index')->name('admin.dashboard.index');
    Route::get('/balance/summary', 'BalanceController@summary')->name('admin.dashboard.summary');
});
