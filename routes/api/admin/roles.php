<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */
Route::group(['prefix'=>'roles'], function () {
    Route::get('/', 'RoleController@index')->name('admin.roles.index');
    Route::post('/', 'RoleController@store')->name('admin.roles.store');
    Route::get('/{role}', 'RoleController@details')->name('admin.roles.details');
    Route::put('/{role}', 'RoleController@update')->name('admin.roles.update');
    Route::delete('/{role}', 'RoleController@delete')->name('admin.roles.delete');
});
