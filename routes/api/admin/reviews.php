<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */
Route::get('reviews/negatives', 'ReviewController@negatives')
     ->name('admin.reviews.negatives');
Route::get('reviews/{review}', 'ReviewController@details')
     ->name('admin.reviews.details');
Route::put('reviews/{review}', 'ReviewController@update')
     ->name('admin.reviews.update');
Route::put('reviews/{review}/delete', 'ReviewController@delete')
     ->name('admin.reviews.delete');
Route::put('reviews/{review}/reset-with-block', 'ReviewController@resetWithBlock')
     ->name('admin.reviews.reset-with-block');
Route::get('reviews', 'ReviewController@index')
     ->name('admin.reviews.index');
