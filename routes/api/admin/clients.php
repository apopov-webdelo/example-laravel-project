<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:45
 */
Route::group(['namespace'=>'Client'], function () {
    Route::put(
        'clients/balance/{balance}/update-commission',
        'BalanceController@updateCommissionPercent'
    )->name('admin.clients.client.balance.update-commission');

    Route::get('clients/balance/{balance}', 'BalanceController@details')->name('admin.clients.client.balance.details');
    Route::get(
        'clients/balance/{balance}/transactions',
        'TransactionController@index'
    )->name('admin.clients.client.balance.transactions');
    Route::get('clients/{client}/balance', 'BalanceController@index')->name('admin.clients.client.balance');

    Route::get('clients', 'ClientController@index')->name('admin.clients');
    Route::get('clients/{client}', 'ClientController@details')->name('admin.clients.client');
    Route::put(
        'clients/{client}/update-email',
        'ClientController@updateEmail'
    )->name('admin.clients.client.update-email');
    Route::put('clients/{client}/unlock', 'ClientController@unlock')->name('admin.clients.client.unlock');
    Route::put('clients/{client}/lock/{days?}', 'ClientController@lock')->name('admin.clients.client.lock');
    Route::put(
        'clients/{client}/unlock-trade',
        'ClientController@unlockTrade'
    )->name('admin.clients.client.unlockTrade');
    Route::put(
        'clients/{client}/lock-trade/{days?}',
        'ClientController@lockTrade'
    )->name('admin.clients.client.lockTrade');
    Route::get('clients/{client}/sessions', 'SessionController@index')->name('admin.clients.client.sessions');
    Route::get('clients/{client}/reviews', 'ClientController@reviews')->name('admin.clients.client.reviews');
    Route::get('clients/{client}/reviews/in', 'ReviewController@in')->name('admin.clients.client.reviews.in');
    Route::get('clients/{client}/reviews/out', 'ReviewController@out')->name('admin.clients.client.reviews.out');
    Route::get('clients/{client}/deals', 'ClientController@deals')->name('admin.clients.client.deals');
    Route::get('clients/{client}/notes/in', 'NoteController@in')->name('admin.clients.client.notes.in');
    Route::get('clients/{client}/notes/out', 'NoteController@out')->name('admin.clients.client.notes.out');
    Route::get('clients/{client}/favorites', 'ClientController@favorites')->name('admin.clients.client.favorites');
    Route::get('clients/{client}/blacklist', 'ClientController@blacklist')->name('admin.clients.client.blacklist');
    Route::put(
        'clients/{client}/security/2fa/disconnect',
        'Google2FaController@disconnect'
    )->name('admin.clients.client.security.2fa.disconnect');

    Route::put(
        'clients/{client}/security/reset-password',
        'SecurityController@resetPassword'
    )->name('admin.clients.client.security.resetPassword');


});
