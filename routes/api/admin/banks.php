<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 11/12/18
 * Time: 15:07
 */

Route::post('banks/{bank}/icon', 'BankImageController@upload')->name('admin.directory.banks.icon.upload');
Route::delete('banks/{bank}/icon', 'BankImageController@remove')->name('admin.directory.banks.icon.remove');
Route::get('banks', 'BankController@index')->name('admin.directory.banks');
Route::get('banks/{bank}', 'BankController@details')->name('admin.directory.banks.details');
Route::get('banks/{bank}/currencies', 'BankController@currencies')
    ->name('admin.directory.banks.details.currencies');
Route::put('banks/{bank}/currencies/{currency}/attach', 'BankController@attachCurrency')
    ->name('admin.directory.banks.details.currencies.attach');
Route::put('banks/{bank}/currencies/{currency}/detach', 'BankController@detachCurrency')
    ->name('admin.directory.banks.details.currencies.detach');
Route::post('banks', 'BankController@store')->name('admin.directory.banks.store');
Route::put('banks/{bank}', 'BankController@update')->name('admin.directory.banks.update');
Route::delete('banks/{bank}', 'BankController@delete')->name('admin.directory.banks.delete');
