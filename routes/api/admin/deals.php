<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/28/18
 * Time: 13:51
 */
Route::group(['prefix'=>'deal'], function () {
    Route::get('/', 'DealController@index')->name('admin.deal.index');
    Route::put('/{deal}/cancel', 'DealController@cancellation')->name('admin.deal.cancellation');
    Route::put('/{deal}/finish', 'DealController@finishing')->name('admin.deal.finishing');
    Route::get('/statuses', 'DealController@statuses')->name('admin.deal.statuses');
    Route::get('/{deal}', 'DealController@details')->name('admin.deal.details');
});
