<?php
/**
 * Created by PhpStorm.
 * User: dmitricercel
 * Date: 5/31/18
 * Time: 11:33
 */
Route::get('countries', 'CountryController@index')->name('admin.directory.countries');
Route::get('countries/{country}', 'CountryController@details')->name('admin.directory.country.details');
Route::put('countries/{country}', 'CountryController@update')->name('admin.directory.country.update');
Route::delete('countries/{country}', 'CountryController@delete')->name('admin.directory.country.delete');
Route::post('countries', 'CountryController@store')->name('admin.directory.country.store');
