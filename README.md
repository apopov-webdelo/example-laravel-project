# ERSEC_PROJECT

## Artisan команды

#### php artisan dbdemo:fresh

сброс демо данных в базе, управление в [database/seeds/Fresh/FreshDatabaseSeeder.php](database/seeds/Fresh/FreshDatabaseSeeder.php)

#### php artisan dbdemo:seed

сид демо данных в базу, управление в [database/seeds/Demo/DemoDatabaseSeeder.php](database/seeds/Demo/DemoDatabaseSeeder.php)

#### php artisan deal:usd_amount

заполнит поле crypto_amount_in_usd во всех сделках где оно равно 0

--queue - поставить все задачи по заполнению crypto_amount_in_usd в очередь  
--force - принудительно пересчитать все сделки

#### php artisan balance:verify --login=user1

ручная сверка баланса юзера с балансом на боксе [app/Jobs/Blockchain/CompareBalanceJob.php](app/Jobs/Blockchain/CompareBalanceJob.php)

#### php artisan balance:deposit --login=user1

вручную пополняет баланс криптовалюты пользователя

--login=... или --userId=... - кому добавить (логин или ID надо указать обязательно)  
--crypto=btc - код валюты которую добавить (по умолчанию btc)   
--amount=1 - кол-во которое добавить (по умолчанию 1)
--coins - флаг если amount написали в сатоши (опционально)
--description='Some text' - причина пополнения из консоли (опционально)

#### php artisan api:generate --routePrefix="api/v1/*" |  | 

генерация apiDoc

--actAsUserId=1 - ходить под юзером с ID  
--header="Authorization: Bearer FO48S3zu9IuYkOqOJQ9EocWdYV9NFcgesDHwJVrtOU6WZPW6h4y5OBelXWDp" - отправлять хедер авторизации

#### php artisan ide-helper:generate

пересоздать хелпер по синтаксису для Laravel, файл результата должен быть в .gitignore

### apiDoc 2.x

Пример запуска: php artisan api:generate --routePrefix="api/v1/*"


С авторизацией: php artisan api:generate --routePrefix="api/v1/*" --actAsUserId=1 --header="Authorization: Bearer FO48S3zu9IuYkOqOJQ9EocWdYV9NFcgesDHwJVrtOU6WZPW6h4y5OBelXWDp"

У нас стоит версия 2.*.

Версия 2.* ходит толко по GET маршрутам, конкретные ресурсы не может получить.

Версия 3.* ходит на все маршруты, их коробки авторизация по токену и т.д. (с ресурсами думаю проблема та же будет). Еще - там убрали автопарсинг правил валидации реквеста и получение необходимых методу параметров. Поэтому пока эта версия нам не полезна.

Документация по 2.*: https://github.com/mpociot/laravel-apidoc-generator/blob/2.x/README.md

Альтернативы:  
http://labs.infyom.com/laravelgenerator/  
http://apiato.io/  
swagger